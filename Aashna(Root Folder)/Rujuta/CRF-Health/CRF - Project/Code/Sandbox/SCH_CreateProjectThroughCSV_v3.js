// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_CreatesProjectThroughCSV.js
     Author: Rujuta Karandikar
     Company: Aashna Cloud Tech
     Date: 17/04/2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    try 
	{
		var b_nextScheduleFlag = false;
        var contextObj = nlapiGetContext();
        var ProjectStatus = '';
        var statusArray = new Array();
        var updateStatus = new Array();
        var status = new Array();
        var flag = '';
        var fileresource = ''
        var taskresourcearray = ''
        var UpdateError=new Array();
		var add_action =new Array()
        //total usage
        var beginUsage = contextObj.getRemainingUsage();
        nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
        
        //getting the Parameter value
        var fileId_Param = contextObj.getSetting('SCRIPT', 'custscript_sprojectfileid_v3');
        nlapiLogExecution('DEBUG', 'Search Results', 'fileId_Param :' + fileId_Param);
        
        //var lineno = contextObj.getSetting('SCRIPT', 'custscriptcustscript_lineno');
        //nlapiLogExecution('DEBUG', 'Search Results', 'lineno :' + lineno);
        
        /*
         if(!_logValidation(lineno))
         {
         lineno=0;
         }
         */
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var filter = new Array();
        //filter.push(new nlobjSearchFilter('folder',null,'is','CSV'));
        //filter.push(new nlobjSearchFilter('filetype',null,'is','EXCEL'));
        
        if (_logValidation(fileId_Param)) 
		{
            filter.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', fileId_Param));
        }
        
        //Searching the records from the Saved Search "CSV Upload"
        var results = nlapiSearchRecord(null, 'customsearch_csvupload', filter, columns);
        
        if (_logValidation(results)) 
		{
            nlapiLogExecution('DEBUG', 'Search Results', 'Results Length :' + results.length);
            
            for (var i = 0; i < results.length; i++) //line no
            {
            
                //Getting the File Internal ID
                var fileId = results[i].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Search Results', 'fileId :' + fileId);
                
                var beginUsage = contextObj.getRemainingUsage();
                nlapiLogExecution('DEBUG', 'Search Results', 'In Loop Remaining Usage :' + beginUsage);
                
                //Loading the File
                var fileObj = nlapiLoadFile(fileId);
                
                var fileName = fileObj.getName();
                nlapiLogExecution('DEBUG', 'Search Results', 'fileName :' + fileName);
                
                //Checking the Record Type whether it is Estimate or Project or Sales Order 
                //Process only if it is Project
                var isProject = fileName.toString().match(/Project/);
                nlapiLogExecution('DEBUG', 'Search Results', 'isProject :' + isProject);
                
                var isCIS = fileName.toString().match(/CIS/);
                nlapiLogExecution('DEBUG', 'Search Results', 'is file name :' + isCIS);
                
                if ((isProject == 'Project' || isProject == 'project') && !_logValidation(isCIS)) 
				{
                    var records = GetFileContents(fileObj);
                    nlapiLogExecution('DEBUG', 'Search Results', 'records==== :' + records);
                    if (_logValidation(records)) 
					{
                        var tranID = '';
                        var split = records.toString().split('#$');
                        nlapiLogExecution('DEBUG', 'Search Project', 'split Length :' + split.length);
                        
                        if (_logValidation(split)) 
						{
                            //Usage to execute the Current file with respect to the line items
                            var cal_Usage = parseInt(parseInt([parseInt(split.length) * parseInt(25)]) + parseInt(20) + parseInt(60) + parseInt(100) + parseInt(150));
                            nlapiLogExecution('DEBUG', 'Search Results', 'cal_Usage-' + i + ' :' + cal_Usage);
                            
                            //current remaining usage
                            var remainingUsage = contextObj.getRemainingUsage();
                            nlapiLogExecution('DEBUG', 'Search Results', 'remainingUsage-' + i + ' :' + remainingUsage);
                            
                            if (remainingUsage > 6200) 
							{
                                fileObj.setFolder(1315);
                                nlapiSubmitFile(fileObj);
                                //Create Custom Record					
                                var CustRecID = CreateCustomRecord(fileId, fileName);
                                //Get the File Contents and Create/Update the Project
                                
                                var splitArray = split[0].toString().split('|');
                                tranID = splitArray[2];
                                nlapiLogExecution('DEBUG', 'Search Project', 'tranID:' + tranID);
                                //Search Project Record Using the TranID		
                                if (_logValidation(tranID)) 
								{
                                    //Search Project
                                    var ProjectID = searchProject(tranID);
                                    nlapiLogExecution('DEBUG', 'Search Project', 'ProjectID :' + ProjectID);
                                    if (!_logValidation(ProjectID))
									{
                                        //Create Project
                                        nlapiLogExecution('DEBUG', 'Create Project', '************** Creation of Project *******');
                                        
                                        ProjectStatus = CreateProject(records);
                                        nlapiLogExecution('DEBUG', 'Create Task', 'Project Status==== :' + ProjectStatus);
                                        
                                        var custommessage = ProjectStatus.toString().split('#');
                                        nlapiLogExecution('DEBUG', 'Create Task', 'custommessage==== :' + custommessage);
                                        
                                        if (custommessage[3] == '') 
										{
                                            //Create Project Task
                                            nlapiLogExecution('DEBUG', 'Create Project', '************** Creation of Task ********');
                                            
                                            var ProjectID = custommessage[2];
                                            nlapiLogExecution('DEBUG', 'Create Task', 'ProjectID==== :' + ProjectID);
											
                                            var TaskStatus = CreateTask(records, ProjectID);
                                            nlapiLogExecution('DEBUG', 'Create Task', 'Task Status==== :' + TaskStatus);
                                            
                                            var beginUsage = contextObj.getRemainingUsage();
                                            nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
											
                                            var UpdateTaskStatus = UpdateTask(records, ProjectID)
                                            nlapiLogExecution('DEBUG', 'Create Task', 'Update Task Status==== :' + UpdateTaskStatus);
                                            
                                            nlapiLogExecution('DEBUG', 'Create Project', '************** Project ' + tranID + ' Created Suceessfully *******');
                                            
                                            ProjectStatus = ProjectStatus + "$" + TaskStatus + "$" + UpdateTaskStatus;
                                            nlapiLogExecution('DEBUG', 'Update Project', 'ProjectStatus' + ProjectStatus);
											
                                           UpdateCustomRecord(ProjectStatus, CustRecID, fileName);
                                            
                                        }
                                        else 
										{
											ProjectStatus = "Create" + "#" + tranID + "#" + "The tranID is blank in CSV file.";
                                            updateerrorCustomRecord(ProjectStatus, CustRecID, fileName)
                                        }
                                        
                                    }
                                    else 
									{
                                        ProjectStatus = "Create" + "#" + tranID + "#" + "The Project Id " + tranID + " is already created.";
                                        updateerrorCustomRecord(ProjectStatus, CustRecID, fileName)
                                    }
                                    
                                }
								else
								{
									updateerrorCustomRecord(ProjectStatus, CustRecID, fileName)
								}
                                
                            }
                            else 
							{
								b_nextScheduleFlag = true;
                                Schedulescriptafterusageexceeded(fileId);
                                break;
                            }
                            
                        }
                        var endUsage = contextObj.getRemainingUsage();
                        nlapiLogExecution('DEBUG', 'Search Results', 'endUsage :' + endUsage);
                    }
                    //------------------------------------ Update The Project------------------------------//
                }
                else 
                    if ((isProject == 'Project' || isProject == 'project') && (isCIS == 'CIS' || isCIS == 'cis')) 
					{
						
                        //Update Project
                        var records = GetFileContents(fileObj);
                        nlapiLogExecution('DEBUG', 'Search Results', 'records==== :' + records);
                        if (_logValidation(records)) 
						{
                            var tranID = '';
							
                            var split = records.toString().split('#$');
                            nlapiLogExecution('DEBUG', 'Search Project', 'split Length :' + split.length);
                            
                            if (_logValidation(split)) 
							{
                                //Usage to execute the Current file with respect to the line items
                                var cal_Usage = parseInt(parseInt([parseInt(split.length) * parseInt(5)]) + parseInt(82) + parseInt(60) + parseInt(100));
                                nlapiLogExecution('DEBUG', 'Search Results', 'cal_Usage-' + i + ' :' + cal_Usage);
                                
                                //current remaining usage
                                var remainingUsage = contextObj.getRemainingUsage();
                                nlapiLogExecution('DEBUG', 'Search Results', 'remainingUsage-' + i + ' :' + remainingUsage);
                                
                                if (remainingUsage > 6200) 
								{
                                    fileObj.setFolder(1315);
                                    nlapiSubmitFile(fileObj);
                                    //Create Custom Record					
                                    var CustRecID = CreateCustomRecord(fileId, fileName);
                                    //Get the File Contents and Create/Update the Project				
                                    
                                    var splitArray = split[0].toString().split('|');
									nlapiLogExecution('DEBUG', 'Search Project', 'splitArray:' + splitArray);
                                    tranID = splitArray[2];
									var EndusageforCIS=''
                                    nlapiLogExecution('DEBUG', 'Search Project', 'tranID:' + tranID);
                                    var remainingUsage1 = contextObj.getRemainingUsage();
                                    nlapiLogExecution('DEBUG', 'Search Results', 'remainingUsage-' + i + ' :' + remainingUsage1);
                                    //Search Project Record Using the TranID		
                                    if (_logValidation(tranID)) 
									{
                                        //Search Project
                                        var ProjectID = searchProject(tranID);
                                        nlapiLogExecution('DEBUG', 'Search Project', 'ProjectID :' + ProjectID);
                                        if (_logValidation(ProjectID)) 
										{
										
											var BeginusageforCIS = contextObj.getRemainingUsage();
                       						nlapiLogExecution('DEBUG', 'Search Results', 'BeginusageforCIS :' + BeginusageforCIS);
											var finalstatus = new Array();
											var resourceservice = CheckResourcesService(records)
											nlapiLogExecution('DEBUG', 'Create Project', 'resourceservice :' + resourceservice);
											
											var message = resourceservice.toString().split("#")
											nlapiLogExecution('DEBUG', 'Create Project', 'message :' + message);
											
											if (message[2] == '') 
											{
											
												for (var j = 0; j < split.length; j++) {
													var splitArray = split[j].toString().split('|');
													nlapiLogExecution('DEBUG', 'Search Project', 'splitArray ===' + splitArray);
													var action = splitArray[26]
													nlapiLogExecution('DEBUG', 'j', 'j========== :' + j);
													
													if (action == "No Change") {
														nlapiLogExecution('DEBUG', 'action==', 'action :' + action);
														continue;
													}
													else 
														if (action == "Add") {
															nlapiLogExecution('DEBUG', 'action==', 'action :' + action);
															nlapiLogExecution('DEBUG', 'Add Action Function', "*****************In Add Function*************");
															if (_logValidation(splitArray[7])) {
																add_action[j] = addChildTask(splitArray, ProjectID);
																nlapiLogExecution('DEBUG', 'addChildTask', 'addChildTask== :' + add_action[j]);
																finalstatus.push(add_action[j])
																nlapiLogExecution('DEBUG', 'addChildTask', 'finalstatus== :' + finalstatus[j]);
																var add_precedessor = addprecedessor(splitArray, ProjectID)
																nlapiLogExecution('DEBUG', 'addChildTask', 'add_precedessor :' + add_precedessor);
																finalstatus.push(add_precedessor[j])
																continue;
															}
															else {
																add_action[j] = addParentTask(splitArray, ProjectID)
																nlapiLogExecution('DEBUG', 'addParentTask', 'addParentTask :' + add_action[j]);
																finalstatus.push(add_action[j])
																continue;
															}
														}
														else 
															if (action == "Delete") {
																nlapiLogExecution('DEBUG', 'action==', 'action :' + action);
																var deleteaction = DeleteAction(splitArray)
																nlapiLogExecution('DEBUG', 'Delete action==', 'deleteaction :' + deleteaction);
																continue;
															}
															else // if (action == "Modify") 
															{
																fileresource = new Array()
																
																taskresourcearray = new Array()
																
																var errorStatus = new Array()
																
																var Parenttask = splitArray[1]
																nlapiLogExecution('DEBUG', 'Modify Action Function', "Parenttask" + Parenttask);
																// var ProjectID = splitArray[2];
																// nlapiLogExecution('DEBUG', 'Modify Action Function', "ProjectID" + ProjectID);
																
																var taskStatus = SearchTask(Parenttask, ProjectID)
																nlapiLogExecution('DEBUG', 'Modify Action Function', "taskStatus===" + taskStatus);
																if (_logValidation(taskStatus)) {
																	var taskrecord = nlapiLoadRecord('projectTask', taskStatus)
																	nlapiLogExecution('DEBUG', 'Modify Action Function', "taskrecord" + taskrecord);
																	var status = taskrecord.getFieldValue('status')
																	nlapiLogExecution('DEBUG', 'Modify Action Function', "status===" + status);
																	if (status != 'COMPLETE') {
																		nlapiLogExecution('DEBUG', 'action==', 'action :' + action);
																		nlapiLogExecution('DEBUG', 'Modify Action Function', "*****************In Modify Function*************");
																		
																		var ResourceName = splitArray[7]
																		nlapiLogExecution('DEBUG', 'Modify Action Function', "ResourceName==" + ResourceName);
																		
																		if (_logValidation(ResourceName)) {
																			//var ProjStatus = searchProject(ProjectID)
																			// nlapiLogExecution('DEBUG', 'Modify Action Function', "ProjStatus=== " + ProjStatus);
																			
																			if (_logValidation(ProjectID)) {
																				var Project = nlapiLoadRecord('job', ProjectID)
																				nlapiLogExecution('DEBUG', 'Modify Action Function', " Load Project=== " + Project);
																				var ProjectResources = Project.getLineItemCount('jobresources')
																				nlapiLogExecution('DEBUG', 'Modify Action Function', "ProjectResources=== " + ProjectResources);
																				for (var i = 1; i <= ProjectResources; i++) {
																					var ProjectResourceID = Project.getLineItemText('jobresources', 'jobresource', i);
																					nlapiLogExecution('DEBUG', 'Modify Action Function', "ProjectResourceID=== " + ProjectResourceID);
																					if (ProjectResourceID == ResourceName) {
																						flag = false;
																						break;
																					}
																					else {
																						flag = true;
																					}
																				}
																				if (flag == true) {
																					if (_logValidation(ResourceName)) {
																						var resouceid = getResourseID(ResourceName)
																						nlapiLogExecution('DEBUG', 'Modify Action Function', "resouceid=== " + resouceid);
																						if (_logValidation(resouceid)) {
																							Project.selectNewLineItem('jobresources');
																							
																							Project.setCurrentLineItemValue('jobresources', 'jobresource', resouceid)
																							try {
																								Project.commitLineItem('jobresources');
																							} 
																							catch (e) {
																								var message = e.message;
																								nlapiLogExecution('DEBUG', 'Create Project', 'message=' + message);
																								
																								if (message == 'Resource must be unique') {
																									errorStatus.push("For this Task " + splitArray[1] + " Same Resource can not be added in the list. ")
																								}
																							}
																							
																						}
																						else {
																							errorStatus.push("<ol> Resource " + ResourceName + " is not present in netsuite.")
																						}
																					}
																					else {
																						errorStatus.push("<ol> Resource is blank in CSV file</ol>")
																					}
																					
																				}
																				var id = nlapiSubmitRecord(Project, true, true);
																				nlapiLogExecution('DEBUG', 'Modify Action Function', "id==" + id);
																				
																				
																				//nlapiLogExecution('DEBUG', 'Modify Action Function', "*****************In Modify Function*************");
																				var taskname = splitArray[1]
																				nlapiLogExecution('DEBUG', 'Modify Action Function', 'taskname ===' + taskname);
																				if (_logValidation(splitArray[1])) {
																					if (_logValidation(id)) {
																						var search_task = SearchTask(taskname, id)
																						nlapiLogExecution('DEBUG', 'Modify Action Function', 'search_task ===' + search_task);
																						if (_logValidation(search_task)) {
																							for (var j1 = 0; j1 < split.length; j1++) {
																								split_Array = split[j1].toString().split('|');
																								var filetaskname = split_Array[1]
																								nlapiLogExecution('DEBUG', 'Modify Action Function', 'filetaskname ===' + filetaskname);
																								
																								if (taskname == filetaskname) {
																									fileresource.push(split_Array[7])
																									nlapiLogExecution('DEBUG', 'Modify Action Function', 'fileresource ===' + fileresource);
																								}
																							}
																							
																							var loadtask = nlapiLoadRecord('projectTask', search_task)
																							nlapiLogExecution('DEBUG', 'Modify Action Function', 'loadtask ===' + loadtask);
																							
																							
																							var linecount = loadtask.getLineItemCount('assignee')
																							nlapiLogExecution('DEBUG', 'Modify Action Function', 'linecount ===' + linecount);
																							
																							for (var k = 1; k <= linecount; k++) {
																								var taskresource = loadtask.getLineItemText('assignee', 'resource', k)
																								nlapiLogExecution('DEBUG', 'Modify Action Function', 'taskresource ===' + taskresource);
																								taskresourcearray.push(taskresource)
																								
																							}
																							nlapiLogExecution('DEBUG', 'Modify Action Function', 'fileresource ===' + fileresource);
																							nlapiLogExecution('DEBUG', 'Modify Action Function', 'taskresourcearray ===' + taskresourcearray);
																							
																							var number = 0;
																							//  nlapiLogExecution('DEBUG', 'Modify Action Function', 'fileresource length ===' + fileresource.length);
																							//  nlapiLogExecution('DEBUG', 'Modify Action Function', 'taskresourcearray length ===' + taskresourcearray.length);
																							
																							for (var p = 0; p < fileresource.length; p++) {
																							
																								var replace_resource = fileresource[p]
																								nlapiLogExecution('DEBUG', 'Modify Action Function', 'replace_resource ===' + replace_resource);
																								nlapiLogExecution('DEBUG', 'Modify Action Function', 'number ===' + number);
																								for (var q = number; q < taskresourcearray.length; q++) {
																									var task_resource = taskresourcearray[q]
																									nlapiLogExecution('DEBUG', 'Modify Action Function', 'task_resource ===' + task_resource);
																									if (replace_resource == task_resource) {
																										nlapiLogExecution('DEBUG', 'Modify Action Function', 'In side this replace_resource == task_resource');
																										number = number + 1;
																										break;
																									}
																									if (replace_resource != task_resource) {
																										nlapiLogExecution('DEBUG', 'Modify Action Function', 'In side this replace_resource != task_resource');
																										//nlapiLogExecution('DEBUG', 'Modify Action Function', 'replace_resource ===' + replace_resource);
																										nlapiLogExecution('DEBUG', 'Modify Action Function', 'task_resource ===' + task_resource);
																										for (var m = 1; m <= linecount; m++) {
																											var resource = loadtask.getLineItemText('assignee', 'resource', m)
																											nlapiLogExecution('DEBUG', 'Modify Action Function', 'resource ===' + resource);
																											
																											if (task_resource == resource) {
																												nlapiLogExecution('DEBUG', 'Modify Action Function', 'In side task_resource == resource');
																												loadtask.selectLineItem('assignee', m)
																												var estimatedwork = ''
																												var unitcost = ''
																												var resourceID = getResourseID(replace_resource)
																												nlapiLogExecution('DEBUG', 'Modify Action Function', 'resourceID ===' + resourceID);
																												if (_logValidation(resourceID)) {
																													loadtask.setCurrentLineItemValue('assignee', 'resource', resourceID)
																													if (_logValidation(splitArray[8])) {
																														var ServiceID = getServiceItemID(splitArray[8])
																														nlapiLogExecution('DEBUG', 'Add Assignee', 'ServiceID :' + ServiceID);
																														if (_logValidation(ServiceID)) {
																															loadtask.setCurrentLineItemValue('assignee', 'serviceitem', ServiceID)
																															if (_logValidation(splitArray[9])) {
																																estimatedwork = splitArray[9]
																																nlapiLogExecution('DEBUG', 'Modify Action Function', 'estimatedwork ===' + estimatedwork);
																																loadtask.setCurrentLineItemValue('assignee', 'estimatedwork', estimatedwork)
																																if (_logValidation(splitArray[6])) {
																																	unitcost = splitArray[6]
																																	loadtask.setCurrentLineItemValue('assignee', 'unitcost', unitcost)
																																	loadtask.commitLineItem('assignee');
																																	break;
																																}
																																else {
																																	errorStatus.push("<ol> The Unit Cost is blank for this task :=" + taskname + "</ol>")
																																}
																															}
																															else {
																																errorStatus.push("<ol>The estimated work is blank for task" + taskname + "</ol>")
																															}
																														}
																														else {
																															errorStatus.push("<ol>The Service item" + splitArray[8] + " is not Present in Netsuite </ol>")
																														}
																													}
																													else {
																														errorStatus.push("The Service Item is blank for task" + splitArray[1])
																													}
																												}
																												else {
																													errorStatus.push("<ol> Resource" + splitArray[7] + "is not present in netsuite</ol>")
																												}
																												
																											}
																											
																											
																										}
																										number = number + 1;
																										nlapiLogExecution('DEBUG', 'Modify Action Function', "number==" + number);
																										break;
																									}
																									
																								}
																							}
																							
																							
																							//=========================== Set the Task Values=======================//
																							
																							if (_logValidation(splitArray[1])) {
																								loadtask.setFieldValue('title', splitArray[1])
																								nlapiLogExecution('DEBUG', 'Modify Action Function', "Task Name==" + splitArray[1]);
																								if (_logValidation(splitArray[2])) {
																									var projid = searchProject(splitArray[2])
																									if (_logValidation(splitArray[10])) {
																										var Parentsearch = SearchTask(splitArray[10], projid)
																										nlapiLogExecution('DEBUG', 'Modify Action Function', "Project Name==" + projid);
																										if (_logValidation(Parentsearch)) {
																											loadtask.setFieldValue('parent', Parentsearch)
																											nlapiLogExecution('DEBUG', 'Modify Action Function', "parent Name==" + splitArray[10]);
																											if (_logValidation(splitArray[4])) {
																												loadtask.setFieldText('priority', splitArray[4])
																												nlapiLogExecution('DEBUG', 'Modify Action Function', "priority==" + splitArray[4]);
																												if (_logValidation(splitArray[5])) {
																													loadtask.setFieldText('status', splitArray[5])
																													nlapiLogExecution('DEBUG', 'Modify Action Function', "status==" + splitArray[5]);
																													if (_logValidation(splitArray[17])) {
																														loadtask.setFieldText('constrainttype', splitArray[17])
																														nlapiLogExecution('DEBUG', 'Modify Action Function', "constrainttype" + splitArray[17]);
																														if (_logValidation(splitArray[25])) {
																															var setvalue = loadtask.getFieldValue('custevent_cisinformation')
																															nlapiLogExecution('DEBUG', 'Modify Action Function', "setvalue===" + setvalue);
																															if (!_logValidation(setvalue)) {
																																loadtask.setFieldValue('custevent_cisinformation', splitArray[25])
																															}
																															else {
																																setvalue = setvalue + "," + splitArray[25]
																																loadtask.setFieldValue('custevent_cisinformation', setvalue)
																															}
																															loadtask.setFieldValue('custevent_cisinformation', splitArray[25])
																															nlapiLogExecution('DEBUG', 'Modify Action Function', "CIS Information==" + splitArray[25]);
																															var taskid = nlapiSubmitRecord(loadtask);
																															nlapiLogExecution('DEBUG', 'Modify Action Function', "taskid==" + taskid);
																														}
																														else {
																															errorStatus.push("<ol> CIS Information is blank for task " + splitArray[1] + "</ol>")
																														}
																													}
																													else {
																														errorStatus.push("<ol> Constaint type is blank for task " + splitArray[1] + "</ol>")
																													}
																												}
																												else {
																													errorStatus.push("<ol> Status is blank for task " + splitArray[1] + "</ol>")
																												}
																											}
																											
																											else {
																												errorStatus.push("<ol> Priority is blank for task " + splitArray[1] + "</ol>")
																											}
																										}
																										else {
																											errorStatus.push("<ol> Parent Task name is not created </ol>")
																										}
																										
																									}
																									
																									else {
																										errorStatus.push("<ol> Parent Task name is blank for task " + splitArray[1] + "</ol>")
																									}
																								}
																								else {
																									errorStatus.push("<ol> Project name is blank for task " + splitArray[1] + "</ol>")
																								}
																							}
																							
																							else {
																								errorStatus.push("<ol> Task name is blank in given CSV file</ol>")
																							}
																							
																							
																							var taskid = nlapiSubmitRecord(loadtask);
																							nlapiLogExecution('DEBUG', 'Modify Action Function', "taskid==" + taskid);
																							var estimatedtask = nlapiLoadRecord('projectTask', taskid)
																							nlapiLogExecution('DEBUG', 'Modify Action Function', "estimatedtask==" + estimatedtask);
																							var estimatedwork_total = estimatedtask.getFieldValue('estimatedwork')
																							nlapiLogExecution('DEBUG', 'Modify Action Function', "estimatedwork_total" + estimatedwork_total);
																							estimatedtask.setFieldValue('custevent_budget_hours', estimatedwork_total)
																							
																							var estimatedtaskid = nlapiSubmitRecord(estimatedtask);
																							nlapiLogExecution('DEBUG', 'Modify Action Function', "estimatedtaskid" + estimatedtaskid);
																							continue;
																						}
																						else {
																							errorStatus.push("<ol> The Task " + taskname + " is not present</ol>")
																						}
																					}
																					else {
																						errorStatus.push("<ol> The Project " + splitArray[1] + " is not present in Netsuite </ol>")
																					}
																				}
																				else {
																					errorStatus.push("<ol> The Project is blank in CSV file </ol>")
																				}
																				
																				
																				
																			}
																			else {
																				errorStatus.push("<ol>Task status is completed so can not Modify task " + splitArray[1])
																			}
																			
																		}
																		
																	}
																}
																else {
																	errorStatus.push("The Task " + splitArray[1] + " is not present in Project.")
																}
															}
												}
													//ProjectStatus = errorStatus;
													EndusageforCIS = contextObj.getRemainingUsage();
													nlapiLogExecution('DEBUG', 'Search Results', 'EndusageforCIS :' + EndusageforCIS);
													
													ProjectStatus = "Update" + "#" + tranID + "#" + finalstatus + "#" + errorStatus;
													UpdateCustomRecordCIS(ProjectStatus, CustRecID, fileName)
												
											}
											else 
											{
												EndusageforCIS = contextObj.getRemainingUsage();
												nlapiLogExecution('DEBUG', 'Search Results', 'EndusageforCIS :' + EndusageforCIS);
												ProjectStatus = "Update" + "#" + tranID + "#" + message[2]
												UpdateErrorCustomRecordCIS(ProjectStatus, CustRecID, fileName)
											}
                                        }
                                        else 
										{
											EndusageforCIS = contextObj.getRemainingUsage();
                       						nlapiLogExecution('DEBUG', 'Search Results', 'EndusageforCIS :' + EndusageforCIS);
                                            ProjectStatus = "Update" + "#" + tranID + "#" + "The Project Id " + tranID + " is not present in Netsuite." 
                                            UpdateErrorCustomRecordCIS(ProjectStatus, CustRecID, fileName)
                                        }
                                    }
									else
									{
										EndusageforCIS = contextObj.getRemainingUsage();
                       					nlapiLogExecution('DEBUG', 'Search Results', 'EndusageforCIS :' + EndusageforCIS);
										ProjectStatus = "Update" + "#" + tranID + "#" + "The Project Id " + tranID + " is blank in given CSV file." 
                                        UpdateErrorCustomRecordCIS(ProjectStatus, CustRecID, fileName)
									}
                                }
                                else 
								{
									b_nextScheduleFlag = true;
                                    Schedulescriptafterusageexceeded(fileId);
                                    break;
                                }
                            }
                        }
                    }
            }
        }
    } 
    catch (e) 
	{
        nlapiLogExecution('DEBUG', 'Catch Exception :===' + e.getDetails());
		var message = e.message;
		var error=new Array()
		nlapiLogExecution('DEBUG', 'Create Project', 'message=' + message);
	     if (message == 'Resource must be unique') 
		 {
			error.push("For this Task " + splitArray[1] + " Same Resource can not be added in the list.")
		 }
		 var errmsg="Update"+"#"+tranID+"#"+error;
		 UpdateErrorCustomRecordCIS(errmsg,CustRecID,fileName)
		
														
    }
	if(b_nextScheduleFlag != true)
	{
		 ////Define all parameters to schedule the script for voucher generation.
	     var params=new Array();
	     params['status']='scheduled';
	     params['runasadmin']='T';
	     var startDate = new Date();
	     params['startdate']=startDate.toUTCString();//     
	     var status=nlapiScheduleScript('customscript_createestimatethroughcsv_v3', 'customdeploy1', params);
	}
}



// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
//Function To validate the v
function _logValidation(value){
    if (value != null && value != '' && value != undefined) {
        return true;
    }
    else {
        return false;
    }
}

function UpdateCustomRecordCIS(status, CustRecId, fileName)
{
    nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'status:' + status);
	var errorMsg = "";
    if (_logValidation(status) && _logValidation(CustRecId)) 
	{
        var statusSplit = status.toString().split('#');
        nlapiLogExecution('DEBUG', 'UpdateCustomRecord CIS', 'statusSplit 0:' + statusSplit[0]);
		nlapiLogExecution('DEBUG', 'UpdateCustomRecord CIS', 'statusSplit 1:' + statusSplit[1]);
		nlapiLogExecution('DEBUG', 'UpdateCustomRecord CIS', 'statusSplit 2:' + statusSplit[2]);
        
        if (statusSplit.length != 1 && statusSplit.length != 0) 
		{

            	nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
            var custObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', CustRecId[0]);
			
            
            if (_logValidation(statusSplit[0])) 
			{
                if (statusSplit[0] == 'Update') 
				{
						nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
                    custObj.setFieldValue('custrecord_updatedrecords', 'Project : ' + statusSplit[1]);
                }
                
            }
			statusSplit[2]=statusSplit[2].toString().replace(/,/g, '')
			nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
			statusSplit[3]=statusSplit[3].toString().replace(/,/g, '')
			nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
			
             
			
                if (statusSplit[2]=='' && statusSplit[3] == '') 
				{
                    //1 means Success
                    custObj.setFieldValue('custrecord_status', 3);
					custObj.setFieldValue('custrecord_errorlog', ' ');
                    nlapiLogExecution('DEBUG', 'Updated CustomRecord CIS', 'succsful condition==');
                }
                else 
				{
                    if (statusSplit[0] == 'Update') 
					{
						 if (statusSplit[2] == '' && statusSplit[3] != '') 
						 {
						 	nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
						 	errorMsg +=  statusSplit[3] ;
						 	nlapiLogExecution('DEBUG', 'UpdateCustomRecord CIS', 'errorMsg:' + statusSplit[3]);
						 	custObj.setFieldValue('custrecord_errorlog', statusSplit[3]);
						 	nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'errorMsg:' + statusSplit[3]);
						 	custObj.setFieldValue('custrecord_status',6);
						 }
						 else
						  if (statusSplit[2]!='' && statusSplit[3] == '') 
						  {
						  	nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
						  	errorMsg +=  statusSplit[2] 
						 	nlapiLogExecution('DEBUG', 'Update CustomRecord CIS', 'errorMsg:' + statusSplit[2]);
						 	custObj.setFieldValue('custrecord_errorlog', statusSplit[2]);
						 	//nlapiLogExecution('DEBUG', 'Update CustomRecordCIS', 'errorMsg:' + errorMsg);
						 	custObj.setFieldValue('custrecord_status',6);
						  }
						  else
						  if (statusSplit[2]!=''&&statusSplit[3] != '') 
						  {
						  	nlapiLogExecution('DEBUG', 'UpdateCustomRecordCIS', 'Updating error');
						  	errorMsg +=  statusSplit[2] + "<br/>" + statusSplit[3];
						 	nlapiLogExecution('DEBUG', 'Update CustomRecord CIS', 'errorMsg:' + errorMsg);
						 	custObj.setFieldValue('custrecord_errorlog', errorMsg);
						 	nlapiLogExecution('DEBUG', 'Update CustomRecordCIS', 'errorMsg:' + errorMsg);
						 	custObj.setFieldValue('custrecord_status',6);
						  }                      
                    }
                    
                }
            
            var id = nlapiSubmitRecord(custObj, false, true);
            nlapiLogExecution('DEBUG', 'Updated CustomRecord CIS', 'CustomRecord= ' + id);
        }
    }
}
function UpdateErrorCustomRecordCIS(status, CustRecId, fileName)
{
    nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'status:' + status);
	
    if (_logValidation(status) && _logValidation(CustRecId)) 
	{
        var statusSplit = status.toString().split('#');
        nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'statusSplit 0:' + statusSplit[0]);
		nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'statusSplit 1:' + statusSplit[1]);
		nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'statusSplit 2:' + statusSplit[2]);          
        var custObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', CustRecId[0]);
		statusSplit[2]=statusSplit[2].replace(/,/g, '')
            if (_logValidation(statusSplit[0])) 
			{
                if (statusSplit[0] == 'Update') 
				{
                    custObj.setFieldValue('custrecord_updatedrecords', 'Project : ' + statusSplit[1]);
					
                }
                
            }
					custObj.setFieldValue('custrecord_errorlog', statusSplit[2]);
					
					nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'errorMsg:' +  statusSplit[2]);
					
					custObj.setFieldValue('custrecord_status', 6);
            var id = nlapiSubmitRecord(custObj, false, true);
            nlapiLogExecution('DEBUG', 'UpdateErrorCustomRecordCIS', 'CustomRecord= ' + id);
        
    }
}


function updateerrorCustomRecord(status, CustRecId, fileName)
{
    nlapiLogExecution('DEBUG', 'updateerrorCustomRecord ', 'status:' + status);
    var errorMsg = "";
    if (_logValidation(status) && _logValidation(CustRecId)) 
	{
        var statusSplit = status.toString().split('#');
        nlapiLogExecution('DEBUG', 'updateerrorCustomRecord', 'statusSplit:' + statusSplit);
        
        statusSplit[1] = statusSplit[1].toString().replace(/,/g, '')
		statusSplit[2]=statusSplit[2].replace(/,/g, '')
        
        var custObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', CustRecId[0]);
        
        if (statusSplit[2] == '') 
		{
            if (_logValidation(statusSplit[0])) 
			{
                if (statusSplit[0] == 'Create') 
				{
                    custObj.setFieldValue('custrecord_createdrecords', 'Project : ' + statusSplit[1]);
                }
				
            }
            //1 means Success
            custObj.setFieldValue('custrecord_status', 1);
            
            nlapiLogExecution('DEBUG', 'Updated CustomRecord ', 'succsful condition==');
        }
        else  if(_logValidation(statusSplit[2]))
		{
			if (_logValidation(statusSplit[3])) 
			{
				errorMsg += statusSplit[2] + statusSplit[3];
				custObj.setFieldValue('custrecord_status',2)
				 custObj.setFieldValue('custrecord_errorlog', errorMsg);
			}
			else
			{
				errorMsg += statusSplit[2]
				custObj.setFieldValue('custrecord_status',2)
				custObj.setFieldValue('custrecord_errorlog', errorMsg);
			}
            //2 means Error
          //  custObj.setFieldValue('custrecord_errorlog', 2);
            
         
            
        }
        var id = nlapiSubmitRecord(custObj, false, true);
        nlapiLogExecution('DEBUG', 'updateerrorCustomRecord', 'CustomRecord= ' + id);
    }
    
    
    
    
    
    
}

//Function to update the custom record
function UpdateCustomRecord(status, CustRecId, fileName)
{
    nlapiLogExecution('DEBUG', 'UpdateCustomRecord', 'status:' + status);
    
    if (_logValidation(status) && _logValidation(CustRecId)) 
	{
        var statusSplit = status.toString().split('$');
        nlapiLogExecution('DEBUG', 'UpdateCustomRecord', 'statusSplit:' + statusSplit);
		
		var spilt = statusSplit[0].toString().split('#');
        nlapiLogExecution('DEBUG', 'UpdateCustomRecord', 'spilt:' + spilt);
        
		var spiltarray2=statusSplit[2];
		nlapiLogExecution('DEBUG', 'Updated CustomRecord', 'statusSplit[1]= ' + statusSplit[1])
		
		statusSplit[1]=statusSplit[1].replace(/,/g, '')
		statusSplit[2]=statusSplit[2].replace(/,/g, '')
		statusSplit[1]=statusSplit[1].replace(/#/g, '')
		statusSplit[2]=statusSplit[2].replace(/#/g, '')
		
        
            var errorMsg = "";
            
            var custObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', CustRecId[0]);
            
            if (_logValidation(spilt[0])) 
			{
                 if (spilt[0] == 'Create') 
				{
				
                    custObj.setFieldValue('custrecord_createdrecords', spilt[1]);
                }
               
            }
            
            if (!_logValidation(statusSplit[1]) && !_logValidation(statusSplit[2]))
			{
                //1 means Success
				
                custObj.setFieldValue('custrecord_createdrecords', spilt[1]);
                custObj.setFieldValue('custrecord_status', 1);
				  custObj.setFieldValue('custrecord_createdrecords', spilt[1]);
                nlapiLogExecution('DEBUG', 'Updated CustomRecord 1', 'succsful condition==');
                custObj.setFieldValue('custrecord_errorlog', '');
            }
            else 
			{
				custObj.setFieldValue('custrecord_status', 2);
				
                errorMsg += statusSplit[1]+"<br>"+statusSplit[2]
				
                custObj.setFieldValue('custrecord_errorlog',errorMsg);
            }
        var id = nlapiSubmitRecord(custObj, false, true);
        nlapiLogExecution('DEBUG', 'Updated CustomRecord', 'CustomRecord= ' + id);
    }
}

//function to Create Custom record
function CreateCustomRecord(fileId, filename)
{
    nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'fileId :' + fileId);
    nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'filename :' + filename);
    
    if (_logValidation(fileId)) 
	{
        var id = '';
        
        if (_logValidation(filename)) 
		{
            id = searchCustomRecord(filename, fileId)
            nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'id :' + id);
        }
        
        var fileObj = nlapiLoadFile(fileId);
        
        if (!_logValidation(id)) {
            nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'id :' + id);
            fileObj.setFolder(1315);
            var newID = nlapiSubmitFile(fileObj);
            nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'newID :' + newID);
            
            var custRecordObj = nlapiCreateRecord('customrecord_csvuploadreferencerecord');
            nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'custRecordObj :' + custRecordObj);
            
            custRecordObj.setFieldValue('custrecord_file', fileId);
            custRecordObj.setFieldValue('custrecord_status', 5);
            custRecordObj.setFieldValue('custrecord_recordtype', 'Project');
            
            if (_logValidation(filename)) 
			{
                custRecordObj.setFieldValue('custrecord_filename', filename);
            }
            
            var id = nlapiSubmitRecord(custRecordObj, false, true);
            nlapiLogExecution('DEBUG', 'New Custom Record', 'New Record ID :' + id);
            
            if (_logValidation(id)) {
                return [id, false];
            }
            else {
                return [];
            }
        }
        else { //return results[0].getValue('internalid');
            var CustRecObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', id);
            nlapiLogExecution('DEBUG', 'New Custom Record', 'CustRecObj :' + CustRecObj);
            var PrevFileId = CustRecObj.getFieldValue('custrecord_file');
            nlapiLogExecution('DEBUG', 'searchCustomRecord', 'Previous File ID :' + PrevFileId);
            nlapiLogExecution('DEBUG', 'searchCustomRecord', 'New File ID :' + fileId);
            if (_logValidation(PrevFileId)) {
                nlapiDeleteFile(PrevFileId);
            }
            CustRecObj.setFieldValue('custrecord_status', 3);
            fileObj.setFolder(1315);
            var newID = nlapiSubmitFile(fileObj);
            CustRecObj.setFieldValue('custrecord_file', newID);
            var UpdatedRec = nlapiSubmitRecord(CustRecObj);
            nlapiLogExecution('DEBUG', 'Update Custom Record', 'Updated Custom Record ID :' + UpdatedRec);
            return [id, true];
        }
        
    }
    
    else {
        return [];
    }
}

function searchCustomRecord(fileName, fileId){
    if (_logValidation(fileName)) {
        var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_filename', null, 'is', fileName));
        
        var columns = new Array();
        columns[columns.length] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('customrecord_csvuploadreferencerecord', null, filters, columns);
        
        if (_logValidation(results)) {
            return results[0].getValue('internalid');
        }
        else {
            return '';
        }
    }
    
    else {
        return '';
    }
}


//Function to get the file contents and create/update the Project Record
function GetFileContents(FileObj){
    if (_logValidation(FileObj)) {
        var filedecodedrows = '';
        
        var fileContents = FileObj.getValue();
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'fileContents=' + fileContents + '# Type =' + FileObj.getType());
        
        // Begin Code : To parse The data.
        
        if (FileObj.getType() == 'EXCEL' || (FileObj.getType() == 'MISCTEXT')) {
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ excel file');
            filedecodedrows = decode_base64(fileContents);
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After filedecodedrows=' + filedecodedrows);
        }
        else {
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ other than excel file');
            filedecodedrows = fileContents;
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After fileContents=' + fileContents);
        }
        
        // End Code : To parse The data.
        
        var arr = filedecodedrows.split(/\n/g);
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'base64Ecodedstring=' + arr.length);
        
        var Length = parseInt(arr.length) - parseInt(1);
        
        var FullContents = '';
        
        for (var i = 1; i < Length; i++) {
            //Begin Code : to get the data from the CSV Row wise
            
            arr[i] = parseCSV(arr[i]);
            
            if (i != (Length - 1)) {
                FullContents += arr[i] + '#$';
            }
            else {
                FullContents += arr[i];
            }
            
        }
        
        return FullContents.toString();
    }
}

//search the Project using the Tran Id
function searchProject(tranid){
    if (_logValidation(tranid)) {
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var filters = new Array();
        filters.push(new nlobjSearchFilter('entityid', null, 'is', tranid));
        //filters.push(new nlobjSearchFilter('mainline',null,'is','T'));
        
        var results = nlapiSearchRecord('job', null, filters, columns);
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'searchProject', 'Results Length :' + results.length);
            
            return results[0].getValue('internalid');
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

function decode_base64(s){
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
    
    for (z in n) {
        for (i = n[z][0]; i < n[z][1]; i++) {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++) {
        e[v[i]] = i;
    }
    
    for (i = 0; i < s.length; i += 72) {
        var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
    
}

//function to parse the CSV String	
function parseCSV(csvString){
    var fieldEndMarker = /([,\015\012] *)/g; /* Comma is assumed as field separator */
    var qFieldEndMarker = /("")*"([,\015\012] *)/g; /* Double quotes are assumed as the quote character */
    var startIndex = 0;
    var records = [], currentRecord = [];
    do {
        // If the to-be-matched substring starts with a double-quote, use the qFieldMarker regex, otherwise use fieldMarker.
        var endMarkerRE = (csvString.charAt(startIndex) == '"') ? qFieldEndMarker : fieldEndMarker;
        endMarkerRE.lastIndex = startIndex;
        var matchArray = endMarkerRE.exec(csvString);
        if (!matchArray || !matchArray.length) {
            break;
        }
        var endIndex = endMarkerRE.lastIndex - matchArray[matchArray.length - 1].length;
        var match = csvString.substring(startIndex, endIndex);
        if (match.charAt(0) == '"') // The matching field starts with a quoting character, so remove the quotes
        {
            match = match.substring(1, match.length - 1).replace(/""/g, '"');
        }
        currentRecord.push(match);
        var marker = matchArray[0];
        if (marker.indexOf(',') < 0) // Field ends with newline, not comma
        {
            records.push(currentRecord);
            currentRecord = [];
        }
        startIndex = endMarkerRE.lastIndex;
    }
    while (true);
    if (startIndex < csvString.length) { // Maybe something left over?
        var remaining = csvString.substring(startIndex).trim();
        if (remaining) 
            currentRecord.push(remaining);
    }
    if (currentRecord.length > 0) { // Account for the last record
        records.push(currentRecord);
    }
    ////nlapiLogExecution('DEBUG','parseCSV','records :'+records);
    return records;
}

//function get ResourseID	
function getResourseID(resource){
    if (_logValidation(resource)) {
    
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('entityid', null, 'is', resource);
        filter[1] = new nlobjSearchFilter('isjobresource', null, 'is', 'T');
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('employee', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'getResourseID', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'getResourseID', 'Results[0] :' + results[0].getValue('internalid'));
            
            return results[0].getValue('internalid');
            
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

function getResourse(){
    var Resource = new Array();
    var ResourceName = ''
    var ResourceID = ''
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('isjobresource', null, 'is', 'T');
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('entityid');
    var results = nlapiSearchRecord('employee', null, filter, columns);
    if (_logValidation(results)) {
        nlapiLogExecution('DEBUG', 'getResourse', 'Results Length :' + results.length);
        for (var i = 0; i < results.length; i++) {
            ResourceName = results[i].getValue('entityid');
            nlapiLogExecution('DEBUG', 'getResourse', 'Resource Name :' + ResourceName);
            ResourceID = results[i].getValue('internalid');
            nlapiLogExecution('DEBUG', 'getResourse', 'Resource ID :' + ResourceID);
            //Resource.push(ResourceName + "#" + ResourceID)
            Resource[ResourceName] = ResourceName + "#" + ResourceID
        }
        //nlapiLogExecution('DEBUG', 'getResourse', 'Results[0] :' + results[0].getValue('internalid')); 
        nlapiLogExecution('DEBUG', 'getResourse', 'Resource :' + Resource);
        return Resource;
    }
    else {
        return "";
    }
}

function getCustomerID(Customername)
{
    if (_logValidation(Customername)) 
	{
    nlapiLogExecution('DEBUG', 'getCustomerID', 'Customername:' + Customername);
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('entityid', null, 'is', Customername);
        
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('customer', null, filter, columns);
        
        if (_logValidation(results)) 
		{
            nlapiLogExecution('DEBUG', 'getCustomerID', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'getCustomerID', 'Results[0] :' + results[0].getValue('internalid'));
            
            return results[0].getValue('internalid');
            
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

//function get ServiceItemID
function getServiceItemID(serviceItem){
    if (_logValidation(serviceItem)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('itemid', null, 'is', serviceItem);
        
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('serviceitem', null, filter, columns);
        
        if (_logValidation(results)) {
            //nlapiLogExecution('DEBUG','getServiceItemID','Results Length :'+results.length);
            //nlapiLogExecution('DEBUG','getServiceItemID','Results[0] :'+results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

function getService()
{
    var Service = new Array();
    var ServiceName = ''
    var ServiceID = ''
    var filter = new Array();
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('itemid');
    var results = nlapiSearchRecord('serviceitem', null, filter, columns);
    if (_logValidation(results)) 
	{
		nlapiLogExecution('DEBUG', 'getServiceItem', 'results length :' + results.length);
        for (var i = 0; i < results.length; i++) 
		{
            ServiceName = results[i].getValue('itemid');
            nlapiLogExecution('DEBUG', 'getServiceItem', 'Service Name :' + ServiceName);
            ServiceID = results[i].getValue('internalid');
            nlapiLogExecution('DEBUG', 'getServiceItem', 'Service ID :' + ServiceID);
            Service[i] = ServiceName + "#" + ServiceID
        }
        
        return Service;
    }
    else {
        return "";
    }
    
    
    
}

//function Search Task

function SearchPredecessor(parentid, projectid){
    nlapiLogExecution('DEBUG', 'Search Task', 'parentid :' + parentid);
    
    nlapiLogExecution('DEBUG', 'Search Task', 'projectid :' + projectid);
    
    if (_logValidation(parentid) && _logValidation(projectid)) {
    
        var filter = new Array();
        // filter[0] = new nlobjSearchFilter('title', null, 'is', parentid);
        filter[0] = new nlobjSearchFilter('company', null, 'is', projectid)
        filter[1] = new nlobjSearchFilter('predecessor', null, 'is', parentid)
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('projectTask', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'SearchTask', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'SearchTask', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

function SearchTask(parentid, projectid)
{
    nlapiLogExecution('DEBUG', 'Search Task', 'Task Name :' + parentid);
    
    nlapiLogExecution('DEBUG', 'Search Task', 'projectid :' + projectid);
    
    if (_logValidation(parentid) && _logValidation(projectid))
	{
    
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('title', null, 'is', parentid);
        filter[1] = new nlobjSearchFilter('company', null, 'is', projectid)
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('projectTask', null, filter, columns);
        
        if (_logValidation(results)) 
		{
            nlapiLogExecution('DEBUG', 'SearchTask', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'SearchTask', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else 
		{
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

//function Delete Task
function DeleteTask(parentid){

    if (_logValidation(parentid)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('company', null, 'is', parentid);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        columns[1] = new nlobjSearchColumn('parent');
        //Search record Project Task
        var results = nlapiSearchRecord('projectTask', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'Delete Task', 'Task Line Count :' + results.length);
            var ChildCount = 0;
            var ParentCount = 0;
            var ChildTask = new Array();
            var ParentTask = new Array();
            
            for (var i = 0; i < results.length; i++) {
            
                var internalid = results[i].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Delete Task', 'internalid :' + internalid);
                var checkparent = results[i].getValue('parent')
                nlapiLogExecution('DEBUG', 'Delete Task', 'checkparent :' + checkparent);
                
                if (_logValidation(checkparent)) {
                    ChildTask.push(internalid);
                    ChildTask[ChildCount++] = internalid;
                }
                if (!_logValidation(checkparent)) {
                    ParentTask.push(internalid);
                    ParentTask[ParentCount++] = internalid;
                }
            }
            for (var j = 0; j < ChildTask.length; j++) {
                nlapiLogExecution('DEBUG', 'Delete Task', 'ChildTask length :' + ChildTask.length);
                try {
                    var id = nlapiDeleteRecord('projectTask', ChildTask[j]);
                    nlapiLogExecution('DEBUG', 'Delete Task', 'id :' + id);
                } 
                catch (e) {
                    nlapiLogExecution('DEBUG', 'Delete  Task Catch Block :' + e);
                    
                }
            }
            for (var k = 0; k < ParentTask.length; k++) {
                nlapiLogExecution('DEBUG', 'Delete Task', 'ParentTask  length :' + ParentTask.length);
                try {
                    var id = nlapiDeleteRecord('projectTask', ParentTask[k]);
                    nlapiLogExecution('DEBUG', 'Delete Task', 'id :' + id);
                } 
                catch (e) {
                    nlapiLogExecution('DEBUG', 'Delete  Task Catch Block :' + e);
                }
            }
            
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
    
}

//function Create Task
function CreateTask(records, ProjectID)
{

    nlapiLogExecution('DEBUG', 'Create Task', '**********  Creation of Task started ************');
   // nlapiLogExecution('DEBUG', 'Create Task', 'ProjectID=====' + ProjectID);
	
    var contextObj = nlapiGetContext();
    nlapiLogExecution('DEBUG', 'Add Assignee', 'contextObj=====' + contextObj);
	
    var nonItems=new Array();
    var id = '';
    var status = '';
    var ResourceTab = new Array();
    var ResourseItems = new Array();
    var cnt = 0;
    var tranID = '';
    var precearry = new Array();
	var Resource = new Array()
	var NewServiceItems = new Array();
    var NewResourseItems = new Array();
    
    var createParent = CreateParentTask(records, ProjectID);
    nlapiLogExecution('DEBUG', 'Add Assignee', 'createParent=====' + createParent);
    
    var createassignee = CreateChildTask(records, ProjectID);
    nlapiLogExecution('DEBUG', 'Add Assignee', 'createassignee=====' + createassignee);
	
    var createassigneelength = createassignee.length
    nlapiLogExecution('DEBUG', 'Add Assignee', 'createassigneelength=====' + createassigneelength);
	
    if (_logValidation(createassigneelength))
	 {
        var ResourseID = getResourse();
        nlapiLogExecution('DEBUG', 'Add Assignee', 'ResourseID :' + ResourseID);
		
        for (var i = 0; i < createassigneelength; i++) 
		{
            var addAssignee = nlapiLoadRecord('projectTask', createassignee[i])
            nlapiLogExecution('DEBUG', 'Add Assignee', 'addAssignee=====' + addAssignee);
            
            var ProjectTaskname = addAssignee.getFieldValue('title')
            nlapiLogExecution('DEBUG', 'Add Assignee', 'ProjectTaskname=====' + ProjectTaskname);
            
            var split = records.toString().split('#$');
            nlapiLogExecution('DEBUG', 'Add Assignee', 'split Length :' + split.length);
            
            var splitArray = split[0].toString().split('|');
            
            tranID = splitArray[2];
            nlapiLogExecution('DEBUG', 'Add Assignee', 'FileProjname :' + FileProjname);
            
            for (var j = 0; j < split.length; j++) 
			{
                var beginUsage = contextObj.getRemainingUsage();
                nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
                var FileProjname = '';
                nlapiLogExecution('DEBUG', 'Add Assignee', 'j=== :' + j);
                var splitArray = split[j].toString().split('|');
                
                if (!_logValidation(splitArray[7])) 
				{
                    continue;
                }
                else 
				{
                    FileProjname = splitArray[1];
					
						
						if (_logValidation(ResourseID[splitArray[7]])) 
						{
							var Resourceinternalid = ResourseID[splitArray[7]].toString().split("#");
							nlapiLogExecution('DEBUG', 'Add Assignee', 'Resourceinternalid :' + Resourceinternalid);
							
							var internalid = Resourceinternalid[1];
							nlapiLogExecution('DEBUG', 'Add Assignee', 'internalid :' + internalid);
							
							if (_logValidation(splitArray[8])) 
							{
								nlapiLogExecution('DEBUG', 'Add Assignee', 'Before For Loop' + ProjectTaskname == FileProjname);
								nlapiLogExecution('DEBUG', 'Add Assignee', 'ProjectTaskname == FileProjname' + ProjectTaskname + " == " + FileProjname);
								if (_logValidation(internalid)) 
								{
									if (ProjectTaskname == FileProjname) 
									{
										nlapiLogExecution('DEBUG', 'Add Assignee', 'After For Loop' + ProjectTaskname == FileProjname);
										nlapiLogExecution('DEBUG', 'Add Assignee', 'ProjectTaskname == FileProjname:===========' + ProjectTaskname + "==" + FileProjname);
										//nlapiLogExecution('DEBUG', 'Add Assignee', 'ResourseID :' + ResourseID);
										//Set assignee Line Item
										addAssignee.selectNewLineItem('assignee');
										//Assign  Resource to line item
										addAssignee.setCurrentLineItemValue('assignee', 'resource', internalid)
										//Assign  Service Item to line item
										
										//Set Set Estimated Work
										if (_logValidation(splitArray[9])) 
										{
											addAssignee.setCurrentLineItemValue('assignee', 'estimatedwork', splitArray[9])
											nlapiLogExecution('DEBUG', 'Add Assignee', 'Set Estimated Work :' + splitArray[9]);
											//Set UnitCost
											if (_logValidation(splitArray[6])) 
											{
												addAssignee.setCurrentLineItemValue('assignee', 'unitcost', splitArray[6])
												nlapiLogExecution('DEBUG', 'Add Assignee', 'Set UnitCost :' + splitArray[6]);
												if (_logValidation(splitArray[8])) 
												{
													var serviceInternalid = getServiceItemID(splitArray[8])
													nlapiLogExecution('DEBUG', 'Add Assignee', 'serviceInternalid :' + serviceInternalid);
													addAssignee.setCurrentLineItemValue('assignee', 'serviceitem', serviceInternalid)
												}
												
												//Submit the line.
												
												addAssignee.commitLineItem('assignee');
												
												try 
												{
													id = nlapiSubmitRecord(addAssignee, true);
													nlapiLogExecution('DEBUG', 'Add Assignee', '**********  Assignee ' + id + ' Created ************');
													
													var loadtask=nlapiLoadRecord('projectTask',id)
													nlapiLogExecution('DEBUG', 'Add Assignee', 'loadtask=== :' + loadtask);
													
													var estimatedhours=loadtask.getFieldValue('estimatedwork')
													nlapiLogExecution('DEBUG', 'Add Assignee', 'estimatedhours=== :' + estimatedhours);													
													
													loadtask.setFieldValue('custevent_budget_hours',estimatedhours)
													nlapiLogExecution('DEBUG', 'Add Assignee', 'Budgeted hours=== :' + estimatedhours);
													
													var taskid = nlapiSubmitRecord(loadtask, true);
													nlapiLogExecution('DEBUG', 'Add Assignee', 'taskid=='+taskid);
												} 
												catch (e) 
												{
												
													var message = e.message;
													nlapiLogExecution('DEBUG', 'Create Project', 'message=' + message);
													
													if (message == 'Resource must be unique') 
													{
														nonItems.push("For this Task " + splitArray[1] + " Same Resource can not be added in the list. ")
													}
													
												}
												
											}
											else 
											{
												ResourseItems.push("For this task :-" + FileProjname + "For this Resourse :-" + splitArray[6] + " Please enter the Unit Cost")
											}
										}
									}
									else 
									{
										continue
									}
								}
								else 
									{
										ResourseItems.push("<ol>For this task :- " + splitArray[1] + " Resource " + splitArray[7] + " is not present</ol>")
									}
							}
							else
							{
								NewServiceItems.push("<ol>For this task :- " + splitArray[1] + " Service Item  is not present in given CSV File</ol>")
							}
						}
						else 
						{
							continue;
						}
                    
                }
                
            }
        }
    }
    else 
	{
    
    }
    
    
    for (var p = 0; p < ResourceTab.length; p++) 
	{
        if (_logValidation(ResourceTab[p])) 
		{
            NewServiceItems.push(ResourceTab[p])
        }
    }
    
    for (var h = 0; h < ResourseItems.length; h++) 
	{
        if (_logValidation(ResourseItems[h])) 
		{
            NewResourseItems.push(ResourseItems[h])
        }
    }
    
   
    
    Resource = removearrayduplicate(NewResourseItems);
    nlapiLogExecution('DEBUG', 'Resource', 'Resource====' + Resource);
    if (nonItems.length > 0) {
		status = NewServiceItems + "#" + Resource + "#" + createParent + "#" + nonItems
	}
	else	
	{
		status = NewServiceItems + "#" + Resource + "#" + createParent 
	}
    //nlapiLogExecution('DEBUG', 'Create Project Catch Block status:=====' + status);
    
    return status.toString();
    
}

function CreateParentTask(records, ProjectID)
{
    var NonItems = new Array();
    var status = '';
    nlapiLogExecution('DEBUG', 'Create Task', '************Creation Of Parent Task Started *****************');
    //Create Task				
    if (_logValidation(records)) 
	{
        var nonItems = new Array();
        var cnt = 0;
        var tranID = '';
        var parentID = '';
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Create Parent Task', 'split Length :' + split.length);
        
        var splitArray = split[0].toString().split('|');
        tranID = splitArray[2];
        nlapiLogExecution('DEBUG', 'Create Parent Task', 'tranID:' + tranID);
        
        
        
        if (_logValidation(ProjectID)) {
            //Loop for take all task details from spilt variable
            
            for (var j = 0; j < split.length; j++) {
                if (_logValidation(split)) {
                    nlapiLogExecution('DEBUG', 'Create Parent Task', 'j ===========:' + j);
                    
                    var splitArray = split[j].toString().split('|');
                    
                    //Create Record for Project Task
                    var record = nlapiCreateRecord('projectTask');
                    
                    //Set the Parent task value
                    
                    nlapiLogExecution('DEBUG', 'Create Parent Task', 'splitArray[7] :' + splitArray[7]);
                    
                    // var ResourseID = getResourseID(splitArray[7]);
                    // nlapiLogExecution('DEBUG', 'Create Parent Task', 'ResourseID :' + ResourseID);
                    
                    if (_logValidation(splitArray[7])) {
                        continue;
                    }
                    else {
                        //Set Project Name 
                        if (_logValidation(ProjectID)) {
                            record.setFieldValue('company', ProjectID);
                            nlapiLogExecution('DEBUG', 'Create Parent Task', 'Set Project Name :' + ProjectID);
                            
                            
                            //Set Task Name
                            if (_logValidation(splitArray[1])) {
                                var taskname = record.setFieldValue('title', splitArray[1]);
                                nlapiLogExecution('DEBUG', 'Create Parent Task', 'Set Task Name :' + splitArray[1]);
                                
                                
                                //Set Priority
                                if (_logValidation(splitArray[4])) {
                                    record.setFieldText('priority', splitArray[4]);
                                    nlapiLogExecution('DEBUG', 'Create Paren tTask', 'Set Priority :' + splitArray[4]);
                                    
                                    //Set Status
                                    if (_logValidation(splitArray[5])) {
                                        record.setFieldText('status', splitArray[5]);
                                        nlapiLogExecution('DEBUG', 'Create Parent Task', 'Set Status :' + splitArray[5]);
                                        
                                        if (_logValidation(splitArray[17])) {
                                            record.setFieldText('constrainttype', splitArray[17]);
                                            nlapiLogExecution('DEBUG', 'Create Parent Task', 'Set Constraint Type :' + splitArray[17]);
                                            
                                            if (_logValidation(splitArray[10])) {
                                                var parentId = SearchTask(splitArray[10], ProjectID);
                                                nlapiLogExecution('DEBUG', 'Create Parent Task', 'Parent ID :' + parentId);
                                                if (_logValidation(parentId)) 
												{
                                                    record.setFieldValue('parent', parentId);
                                                    nlapiLogExecution('DEBUG', 'Create Parent Task', 'Set Parent Task :' + splitArray[10]);
													
                                                }
                                                else {
                                                    nonItems.push("<ol> Parent Task not found for Task " + splitArray[1] + " so this will create as parent task.</ol>")
                                                }
                                                
                                            }
                                            else {
                                                nonItems.push("<ol> Parent Task is blank for Task " + splitArray[1] + "</ol>")
                                            }
                                        }
                                        else {
                                            nonItems.push("<ol> Constraint Type is blank for Task " + splitArray[1] + "</ol>")
                                        }
                                    }
                                    else {
                                        nonItems.push("<ol>Status is blank for Task " + splitArray[1] + "</ol>")
                                    }
                                }
                                else {
                                    nonItems.push("<ol>Priority is blank for task " + splitArray[1] + "</ol>")
                                }
                            }
                            else {
                                nonItems.push("<ol>Task Name is Blank for Project " + splitArray[2] + "</ol>")
                                
                            }
                            
                        }
                        else {
                            nonItems.push("<ol>Project " + splitArray[2] + " is not present in Netsuite</ol>")
                            
                        }
                    }
                    
                }
                try 
				{
                    id = nlapiSubmitRecord(record, true);
					
					
                } 
                catch (e) 
				{
                    nlapiLogExecution('DEBUG', 'Catch Exception', 'Error Message==' + e.getDetails());
                    nonItems.push("<ol>" + e.getDetails() + "</ol>")
                }
            }
        }
        
        else {
            nonItems.push("<ol>Project " + tranID + " is Blank</ol>")
        }
        
    }
    else 
	{
        NonItems.push("<ol>The Records are not present.</ol>")
    }
    
    status = NonItems ;
    return status.toString();
}

function CreateChildTask(records, ProjectID)
{
    nlapiLogExecution('DEBUG', 'Create Child Task', '************Creation Of Child  Task Started *****************');
    //Create Task				
    if (_logValidation(records)) {
        var tranID = '';
        var parentID = '';
        var ChildCount = 0;
        var nonItems = new Array();
        var cnt = 0;
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Create Task', 'split Length :' + split.length);
        var splitArray = split[0].toString().split('|');
        tranID = splitArray[2];
        var ChildArray = new Array();
        nlapiLogExecution('DEBUG', 'Create Child Task', 'ChildArray :' + ChildArray);
        
        nlapiLogExecution('DEBUG', 'Create Child Task', 'tranID:' + tranID);
        
        
        if (_logValidation(tranID)) 
		{
            //Loop for take all task details from spilt variable                
            //var ProjectID=ProjectID;
            for (var j = 0; j < split.length; j++) 
			{
                if (_logValidation(split)) 
				{
                
                    nlapiLogExecution('DEBUG', 'Create Child Task', 'j ===========:' + j);
                    nlapiLogExecution('DEBUG', 'Create Child Task', 'ProjectID===========:' + ProjectID);
                    var splitArray = split[j].toString().split('|');
                    
                    //Create Record for Project Task
                    var record = nlapiCreateRecord('projectTask');
                    
                    //Set the Parent task value
                    
                    
                    nlapiLogExecution('DEBUG', 'Create Child Task', 'splitArray[7] :' + splitArray[7]);
                    //var ResourseID = getResourseID(splitArray[7]);
                    //nlapiLogExecution('DEBUG', 'Create Child Task', 'ResourseID :' + ResourseID);
                    
                    if (!_logValidation(splitArray[7])) 
					{
                        continue;
                    }
                    else 
					{
                        var Childtask = splitArray[1];
                        nlapiLogExecution('DEBUG', 'Create Child Task', 'Childtask :' + Childtask);
                        
                        var Search_Task = SearchTask(Childtask, ProjectID)
                        nlapiLogExecution('DEBUG', 'Create Child Task', 'Search_Task :' + Search_Task);
                        
                        if (_logValidation(Search_Task)) 
						{
                            ChildArray[ChildCount++] = Search_Task;
                            nlapiLogExecution('DEBUG', 'Create Project', 'ChildArray :' + ChildArray);
                            continue;
                        }
                        else { //Set Project Name 
                            record.setFieldValue('company', ProjectID);
                            nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Project Name :' + ProjectID);
                            
                            //Set Task Name
                            if (_logValidation(splitArray[1])) 
							{
                                record.setFieldValue('title', splitArray[1]);
                                nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Task Name :' + splitArray[1]);
                                
                                //Set Priority
                                if (_logValidation(splitArray[4])) 
								{
                                    record.setFieldText('priority', splitArray[4]);
                                    nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Priority :' + splitArray[4]);
                                    
                                    //Set Status
                                    if (_logValidation(splitArray[5])) 
									{
                                        record.setFieldText('status', splitArray[5]);
                                        nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Status :' + splitArray[5]);
                                        
                                        if (_logValidation(splitArray[17])) {
                                            record.setFieldText('constrainttype', splitArray[17]);
                                            nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Constraint Type :' + splitArray[17]);
                                            
                                            if (_logValidation(splitArray[10])) {
                                                var parentId = SearchTask(splitArray[10], ProjectID);
                                                nlapiLogExecution('DEBUG', 'Create Child Task', 'Parent ID :' + parentId);
                                                if (_logValidation(parentId)) {
                                                    record.setFieldValue('parent', parentId);
                                                    nlapiLogExecution('DEBUG', 'Create Child Task', 'Set Parent Task :' + splitArray[10]);
                                                    
                                                }
                                                else {
                                                    nonItems.push("Parent Name " + splitArray[10] + " not found in Netsuite")
                                                }
                                            }
                                            else {
                                                nonItems.push("The Parent ID is Blank for this task " + splitArray[1] + "in the given CSV file")
                                            }
                                            /*
                                             }
                                             else
                                             {
                                             nonItems.push("Date format is not in proper format")
                                             }
                                             */
                                        }
                                        else {
                                            nonItems.push("The Constraint Type is Blank for this task " + splitArray[1] + "in the given CSV file")
                                        }
                                    }
                                    else {
                                        record.setFieldText('status', 'Not Started');
                                        //nonItems.push("The Status is Blank for this task " +splitArray[1]+"in the given CSV file")
                                    }
                                }
                                else {
                                    record.setFieldText('priority', 'Medium');
                                    //nonItems.push("The Priority is Blank for this task " +splitArray[1]+"in the given CSV file")
                                }
                            }
                            else {
                                nonItems.push("The Task Name is Blank in the given CSV file")
                            }
                            
                        }
                        
                    }
                }
                
                id = nlapiSubmitRecord(record, true);
                nlapiLogExecution('DEBUG', 'Create Child Task', '********** Task ' + id + ' Created ************');
                ChildArray[ChildCount++] = id;
            }
            var removeduplicatechild = new Array();
            removeduplicatechild = removearrayduplicate(ChildArray);
            nlapiLogExecution('DEBUG', 'Create Project', 'remove duplicate array :' + removeduplicatechild);
            
        }
        else 
		{
            nonItems.push("The Project id is blank in the given CSV file")
        }
    }
    return removeduplicatechild;
}


//function Create Project
function CreateProject(records)
{
    var nonItems = new Array();
    var cnt = 0;
    var resourseCount = 0;
    var DuplicateArray = new Array();
    
    nlapiLogExecution('DEBUG', 'Create Project', '************Creation Of Project Started *****************');
    
    if (_logValidation(records)) 
	{
        var tranID = '';
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Create Project', 'split Length :' + split.length);
        
        var splitArray = split[0].toString().split('|');
        tranID = splitArray[2];
        nlapiLogExecution('DEBUG', 'Create Project', 'tranID:' + tranID);
        
        if (_logValidation(splitArray[2])) 
		{
         //   var resourceservice = CheckResourcesService(records)
         //   nlapiLogExecution('DEBUG', 'Create Project', 'resourceservice :' + resourceservice);
            
          //  var message = resourceservice.toString().split("#")
          //  nlapiLogExecution('DEBUG', 'Create Project', 'message :' + message);
            
           // if (message[2] == '') 
			//{
                //Create Project
                var ProjectObj = nlapiCreateRecord('job');
                var disablename = ProjectObj.getFieldValue('custentity_generate_crf_name');
                nlapiLogExecution('DEBUG', 'Create Project', 'disablename :' + disablename);
				
                
                if (disablename == 'T') 
				{
                    var disablestatus = ProjectObj.setFieldValue('custentity_generate_crf_name', 'F')
                    nlapiLogExecution('DEBUG', 'Create Project', 'disablestatus :' + disablestatus);
                }
                
                var autoname = ProjectObj.getFieldValue('autoname')
                nlapiLogExecution('DEBUG', 'Create Project', 'autoname :' + autoname);
                if (autoname == 'T') 
				{
                    //Set Value Auto check box to false 
                    var checkstatus = ProjectObj.setFieldValue('autoname', 'F')
                    nlapiLogExecution('DEBUG', 'Create Project', 'checkstatus :' + checkstatus);
                }
                
                for (var j = 0; j < split.length; j++) 
				{
                    //Setting values for Project
                    var splitArray = split[j].toString().split('|');
                    var new_customer="";
                    if (j == 0) 
					{
                        nlapiLogExecution('DEBUG', 'Create Project', 'j ===========:' + j);
                        
                        //Set Project Id
                        if (_logValidation(splitArray[2])) 
						{
                            ProjectObj.setFieldValue('entityid', splitArray[2]);
                            nlapiLogExecution('DEBUG', 'Create Project', 'Project Id :' + splitArray[2]);
                            
                            //Set Requested Service Level
                            if (_logValidation(splitArray[13])) 
							{
                                ProjectObj.setFieldText('custentity_proj_requested_service_level', splitArray[13]);
                                nlapiLogExecution('DEBUG', 'Create Project', 'requested_service_level :' + splitArray[13]);
                                
                                //Set Customer Name
                                if (_logValidation(splitArray[14])) 
								{
                                    var customerid =splitArray[14]; 
                                    nlapiLogExecution('DEBUG', 'Create Project', 'customerid :' + customerid);
									var checkcomma=customerid.indexOf(',');
									nlapiLogExecution('DEBUG', 'Create Project', 'checkcomma :==' + checkcomma);
									if(checkcomma!=-1)
									{
										var customer_new=customerid.toString().split(',')
										nlapiLogExecution('DEBUG', 'Create Project', 'checkcomma :==' + checkcomma);
										new_customer=customer_new[0]+","+" "+customer_new[1];
										nlapiLogExecution('DEBUG', 'Create Project', 'new_customer :==' + new_customer);
										var newcustomerid=getCustomerID(new_customer);
									    nlapiLogExecution('DEBUG', 'Create Project', 'newcustomerid :==' + new_customer);
                                        ProjectObj.setFieldValue('parent', newcustomerid);
                                       nlapiLogExecution('DEBUG', 'Create Project', 'Customer Name :' + splitArray[14]);
									}
									else
									{
										var oldcustomerid=getCustomerID(customerid);
									    nlapiLogExecution('DEBUG', 'Create Project', 'oldcustomerid :==' + oldcustomerid);
									    ProjectObj.setFieldValue('parent', oldcustomerid);
                                       nlapiLogExecution('DEBUG', 'Create Project', 'Customer Name :' + splitArray[14]);
									}
									
                                    
                                    // Set Project Name
                                    if (_logValidation(splitArray[15])) 
									{
                                        ProjectObj.setFieldValue('companyname', splitArray[15]);
                                        nlapiLogExecution('DEBUG', 'Create Project', 'Project Name :' + splitArray[15]);
                                        
                                        //Set Kick Off Date 
                                        if (_logValidation(splitArray[16])) 
										{
                                            ProjectObj.setFieldValue('startdate', splitArray[16]);
                                            nlapiLogExecution('DEBUG', 'Create Project', 'Kick Off Date :' + splitArray[16]);
                                            
                                            nlapiLogExecution('DEBUG', 'Create Project', 'nonItems=====' + nonItems);
                                        }
                                        
                                        else 
										{
                                            var date = "Date is not proper format"
                                            nonItems.push(date)
                                        }
                                    }
                                    
                                    else 
									{
                                        var companyname = "Project name is blank"
                                        nonItems.push(companyname)
                                    }
                                }
                                else {
                                    var customer = "Customer is blank or Customer not found in netsuite"
                                    nlapiLogExecution('DEBUG', 'Create Project', 'customer=====' + customer);
                                    nonItems.push(customer)
                                    nlapiLogExecution('DEBUG', 'Create Project', 'nonItems=====' + nonItems);
                                }
                            }
                            else 
							{
                                var service_level = "Requested service_level is blank or  not found"
                                nonItems.push(service_level)
                            }
                        }
                        else {
                            nonItems.push(splitArray[2])
                        }
                    }
                    
                    nlapiLogExecution('DEBUG', 'Create Project', 'j ===========:' + j);
                    if (_logValidation(splitArray[7])) {
						
                        nlapiLogExecution('DEBUG', 'Create Project', 'Resource :' + splitArray[7]);
                        
                        var Resource = getResourse();
                        nlapiLogExecution('DEBUG', 'Create Project', 'Resource == :' + Resource);
                        
                        
                        if (_logValidation(Resource[splitArray[7]]))
						 {
                            var ResourceInternalid = Resource[splitArray[7]].toString().split("#")
                            nlapiLogExecution('DEBUG', 'Create Project', 'ResourceInternalid :' + ResourceInternalid);
                            
                            var Internalid = ResourceInternalid[1];
                            nlapiLogExecution('DEBUG', 'Create Project', 'Internalid :' + Internalid);
                            
                            
                            DuplicateArray[resourseCount++] = Internalid;
                            nlapiLogExecution('DEBUG', 'Create Project', 'DuplicateArray :' + DuplicateArray);
                        }
                        else {
                            var resourse = "Resourse" + splitArray[7] + " not found in netsuite."
                            nonItems.push(resourse)
                        }
                    }
                    else {
                        continue;
                    }
                  
                }
				
				
                var removeduplicatearray = new Array();
                
                removeduplicatearray = removearrayduplicate(DuplicateArray);
                
                nlapiLogExecution('DEBUG', 'Create Project', 'remove duplicate array :' + removeduplicatearray);
                
                if (_logValidation(removeduplicatearray)) 
				{
                    for (var k = 0; k < removeduplicatearray.length; k++) 
					{
                        ProjectObj.selectNewLineItem('jobresources');
                        
                        ProjectObj.setCurrentLineItemValue('jobresources', 'jobresource', removeduplicatearray[k])
                        
                        ProjectObj.commitLineItem('jobresources');
                    }
                }
                var id = '';
                
                var status = "";
                try 
				{
                    id = nlapiSubmitRecord(ProjectObj, true, true);
                    nlapiLogExecution('DEBUG', 'Create Project', 'Project id==' + id);
                } 
                catch (e) 
				{
                
                    var message = e.message;
                    nlapiLogExecution('DEBUG', 'Create Project', 'message=' + message);
                    if (message == 'Please enter value(s) for: Subsidiary') 
					{
                        nonItems.push("Customer is not found in Netsuite")
                    }
                    if (message == 'Please enter value(s) for: Requested Service Level') 
					{
                        nonItems.push("Service_level not found in Netsuite");
                    }
                    if (message == 'Please enter value(s) for: Requested Service Level, Subsidiary') 
					{
                        nonItems.push("<ol>Customer is not found in Netsuite</ol>")
                        nonItems.push("<ol>service_level not found in Netsuite</ol>");
                        
                    }
                }
          //  }
          //  else 
			//{
           //     nonItems.push(message[2])
           // }
        }
        else 
		{
            nonItems.push("Project" + tranID + " is blank in CSV File.")
        }
    }
    if (_logValidation(id)) 
	{
		status = "Create" + "#" + tranID + "#" + id + "#" + nonItems;
	}
	else
	{
		status = "Create" + "#" + tranID + "#" + nonItems;
	}
    nlapiLogExecution('DEBUG', 'Create Project Catch Block status:=====' + status);
    return status.toString();
}

//function Update Project

function removearrayduplicate(array){
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}

function UpdateProject(records){
    ////nlapiLogExecution('DEBUG', 'Update Project', '************ In Update Project **************');
    
    
    if (_logValidation(records)) {
        var nonItems = new Array();
        var cnt = 0;
        var tranID = '';
        var split = records.toString().split('#$');
        ////nlapiLogExecution('DEBUG', 'Search Results', 'split Length :' + split.length);
        
        if (_logValidation(split)) {
            //for(var j=0;j<split.length;j++)
            
            var splitArray = split[0].toString().split('|');
            
            tranID = splitArray[2];
            ////nlapiLogExecution('DEBUG', 'Search Results', 'tranID :' + tranID);
            
            //Search Project Record Using the TranID
            //Search Project
            if (_logValidation(tranID)) {
                var ProjectID = searchProject(tranID);
                nlapiLogExecution('DEBUG', 'Update Project', 'ProjectID :' + ProjectID);
                if (_logValidation(ProjectID)) {
                
                    var ProjectObj = nlapiLoadRecord('job', ProjectID);
                    ////nlapiLogExecution('DEBUG', 'Update Project', 'ProjectObj :' + ProjectObj);
                }
                else {
                    nonItems[cnt++] = ProjectID
                }
                
                var ResourseLineCount = ProjectObj.getLineItemCount('jobresources');
                ////nlapiLogExecution('DEBUG', 'Resourse Line Count', 'ResourseLineCount :' + ResourseLineCount);
                
                for (var i = ResourseLineCount; i > 0; i--) {
                
                    ProjectObj.removeLineItem('jobresources', i);
                    
                    ////nlapiLogExecution('DEBUG', 'Task Line Count', 'Removed Previous Resources Line Items :');
                }
                
                
                
                
                var deletetask = DeleteTask(ProjectID);
                ////nlapiLogExecution('DEBUG', 'Delete Task', 'deletetask :' + deletetask);
                
                
                for (var k = 0; k < split.length; k++) {
                    var splitArray = split[k].toString().split(',');
                    
                    if (k == 0) {
                        ////nlapiLogExecution('DEBUG', 'Update Project', 'k ===========:' + k);
                        if (_logValidation(splitArray[13])) {
                            ProjectObj.setFieldText('custentity_proj_requested_service_level', splitArray[13]);
                            ////nlapiLogExecution('DEBUG', 'Update Project', 'requested_service_level :' + splitArray[13]);
                        }
                        if (_logValidation(splitArray[16])) {
                            ProjectObj.setFieldValue('startdate', splitArray[16]);
                            ////nlapiLogExecution('DEBUG', 'Update Project', 'Kick Off Date :' + splitArray[16]);
                        }
                    }
                    
                    ////nlapiLogExecution('DEBUG', 'Update Project', 'k ===========:' + k);
                    
                    if (_logValidation(splitArray[7])) {
                        ////nlapiLogExecution('DEBUG', 'Update Project', 'Resource :' + splitArray[7]);
                        var ResourceId = getResourseID(splitArray[7]);
                        ////nlapiLogExecution('DEBUG', 'Update Project', 'ResourceId :' + ResourceId);
                        
                        if (_logValidation(ResourceId)) {
                            ////nlapiLogExecution('DEBUG', 'Update Project', 'k ===========:' + k);
                            ProjectObj.selectNewLineItem('jobresources');
                            ProjectObj.setCurrentLineItemValue('jobresources', 'jobresource', ResourceId)
                            ProjectObj.commitLineItem('jobresources');
                        }
                    }
                    
                    else {
                        nonItems[cnt++] = splitArray[7];
                    }
                }
                
                var id = '';
                
                var status = "";
                try {
                    ////nlapiLogExecution('DEBUG', 'UpdateProject', '**********In side try block************ :');
                    id = nlapiSubmitRecord(ProjectObj);
                    ////nlapiLogExecution('DEBUG', 'UpdateProject', 'Project id==' + id);
                    
                    if (cnt > 0) {
                        ////nlapiLogExecution('DEBUG', 'UpdateProject', 'tranID :' + tranID);
                        
                        
                        status = "Update" + "#" + +tranID + "#" + "" + "#" + nonItems;
                    }
                    else 
                        if (cnt == 0) {
                            ////nlapiLogExecution('DEBUG', 'UpdateProject', 'tranID :' + tranID);
                            status = "Update" + "#" + +tranID + "#" + "" + "#" + "";
                        }
                    return status.toString();
                } 
                catch (e) {
                    ////nlapiLogExecution('DEBUG', 'UpdateProject', 'tranID :' + tranID);
                    ////nlapiLogExecution('DEBUG', 'Create Project Catch Block :' + exception);
                    
                    status = "Update" + "#" + +tranID + "#" + e.getDetails();
                    ////nlapiLogExecution('DEBUG', 'Create Project Catch Block status:=====' + status);
                    
                    return status.toString();
                }
                
                //End of Resourse array
            
            }//end of split array
            //end of For loop
        
        }//end of if(_logValidation(tranID))
        else {
            return "" + "#" + "No Project Found with this External ID :" + tranID;
        }
    }//end of if(_logValidation(split))
}//end of if(_logValidation(records))			
//Function to Reschedule script
function Schedulescriptafterusageexceeded(fileId, i){
    ////Define all parameters to schedule the script for voucher generation.
    var params = new Array();
    params['status'] = 'scheduled';
    params['runasadmin'] = 'T';
    params['custscript_projectfileid'] = fileId;
    params['custscriptcustscript_lineno'] = i;
    var startDate = new Date();
    params['startdate'] = startDate.toUTCString();
    
    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
    nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    
    ////If script is scheduled then successfuly then check for if status=queued
    if (status == 'QUEUED') {
        nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
    }
}

////function Update Task
function UpdateTask(records, projectID)
{
	var status=new Array()
    nlapiLogExecution('DEBUG', 'Update Task', 'projectID :' + projectID);
    
    var split = records.toString().split('#$');
    nlapiLogExecution('DEBUG', 'Update Task', 'split Length :' + split.length);
    
    var splitArray = split[0].toString().split('|');
    tranID = splitArray[2];
    
    var Project_ID = searchProject(tranID);
    nlapiLogExecution('DEBUG', 'Search Project', 'ProjectID :' + Project_ID);
    
    for (var j = 0; j < split.length; j++) 
	{
        nlapiLogExecution('DEBUG', 'Update Task', 'j=== :' + j);
		
        var splitArray = split[j].toString().split('|');
        nlapiLogExecution('DEBUG', 'Update Task', 'splitArray :' + splitArray);
        
        if (_logValidation(splitArray[1])) 
		{
            nlapiLogExecution('DEBUG', 'Update Task', 'splitArray[1] :' + splitArray[1]);
            nlapiLogExecution('DEBUG', 'Search Project', 'ProjectID :' + Project_ID);
            
            var taskid = SearchTask(splitArray[1], Project_ID);
            nlapiLogExecution('DEBUG', 'Update Task', 'taskid :' + taskid);
            
			if (_logValidation(taskid)) 
			{
				var Taskrecord = nlapiLoadRecord('projectTask', taskid)
				nlapiLogExecution('DEBUG', 'Update Task', 'Taskrecord :' + Taskrecord);
				
				var TaskPredecessorCount = Taskrecord.getLineItemCount('predecessor')
				nlapiLogExecution('DEBUG', 'Update Task', 'Taskrecord :' + TaskPredecessorCount);
				
				if (TaskPredecessorCount == 0) 
				{
					var SplitPredecessor = splitArray[11]
					nlapiLogExecution('DEBUG', 'Update Task', 'SplitPredecessor :' + SplitPredecessor);
					
					if (_logValidation(SplitPredecessor)) 
					{
						var precearry = new Array();
						
						precearry.push(SplitPredecessor)
						
						nlapiLogExecution('DEBUG', 'Update Task', 'precearry :' + precearry);
						var SplitPredecessorlength = precearry.toString();
						
						nlapiLogExecution('DEBUG', 'Update Task', 'SplitPredecessorlength :' + SplitPredecessorlength);
						
						var predecessor = SplitPredecessorlength.split(':');
						nlapiLogExecution('DEBUG', 'Update Task', 'predecessor :' + predecessor);
						
						var lengthprece = predecessor.length;
						nlapiLogExecution('DEBUG', 'Update Task', 'lengthprece :' + lengthprece);
						
						var pretask = lengthprece - 1;
						nlapiLogExecution('DEBUG', 'Update Task', 'pretask :' + predecessor[pretask]);
						
						var searchtask = predecessor[pretask];
						nlapiLogExecution('DEBUG', 'Update Task', 'searchtask :' + searchtask);
						
						nlapiLogExecution('DEBUG', 'Create Predecessor', '******** Create Predecessor *************');
						
						var updatetask = SearchTask(searchtask.trim(), Project_ID);
						nlapiLogExecution('DEBUG', 'Update Task', 'updatetask :' + updatetask);
						
						if (_logValidation(updatetask)) 
						{
							Taskrecord.selectNewLineItem('predecessor');
							
							Taskrecord.setCurrentLineItemValue('predecessor', 'task', updatetask)
							
							if (_logValidation(splitArray[12])) 
							{
								if (splitArray[12] == 'Finish-To-Start') 
								{
									nlapiLogExecution('DEBUG', 'set Type Predecessor', '******** Finish-To-Start Set *************');
									Taskrecord.setCurrentLineItemValue('predecessor', 'type', 'FS')
								}
								else 
								{
									nlapiLogExecution('DEBUG', 'set Type Predecessor', '******** Start-To-Start Set *************');
									Taskrecord.setCurrentLineItemValue('predecessor', 'type', 'SS')
								}
							}
							else 
							{
								Taskrecord.setCurrentLineItemValue('predecessor', 'type', 'FS')
							}
							
							Taskrecord.commitLineItem('predecessor');
							
							try 
							{
								id = nlapiSubmitRecord(Taskrecord);
							////nlapiLogExecution('DEBUG', 'Update Task', 'Task id==' + id);
							} 
							catch (e) 
							{
								nlapiLogExecution('DEBUG', 'UpdateProject', 'tranID :' + tranID);
								nlapiLogExecution('DEBUG', 'Update Task :' + e);
								
								status = "Update" + "#" + +tranID + "#" + e.getDetails();
								////nlapiLogExecution('DEBUG', 'Update Task:=====' + status);
								
								return status.toString();
								
							}
						}
						
						else 
						{
							status.push("<ol>The Predecessor " + searchtask + " not present in Netsuite.</ol>")
							nlapiLogExecution('DEBUG', 'Update Task:=====' + status);
							continue;
						}
					}
					else 
					{
						continue;
					}
				}
				else 
				{
					continue;
				}
			}
			else
			{
				status.push("<ol>The Task "+splitArray[1]+" is not Found in Netsuite</ol>")
			}
        }
		else
		{
			status.push("<ol>Project Task is blank</ol>")
		}
    }
    
    return status.toString();
}

function addParentTask(records,ProjectID)
{
	nlapiLogExecution('DEBUG', 'Add Parent Function', "*****************In Add Parent Function*************");
	
	var updateStatus = new Array();
	
	var status = new Array();
    
    var spliarray = records;
	
	var Proj_ID = ProjectID;
   // nlapiLogExecution('DEBUG', 'Add Parent Function', "ProjectID" + ProjectID);
	
	var search_task=SearchTask(spliarray[1],ProjectID)
	nlapiLogExecution('DEBUG', 'Add Parent Function', "ProjectID===" + ProjectID);
	
	if(!_logValidation(search_task))
	{
		 nlapiLogExecution('DEBUG', 'Add Parent Function', "***********Create Task************* ");
		 
                    var task = nlapiCreateRecord('projectTask')
                    nlapiLogExecution('DEBUG', 'Add Parent Function', "task" + task);
					
                    if (_logValidation(spliarray[1])) 
					{
						task.setFieldValue('title', spliarray[1])
						nlapiLogExecution('DEBUG', 'Add Parent Function', "Task Name" + spliarray[1]);
						
						if (_logValidation(spliarray[2])) 
						{
							task.setFieldValue('company', Proj_ID)
							nlapiLogExecution('DEBUG', 'Add Parent Function', "Project Name" + Proj_ID);
								
									var Parentsearch = SearchTask(spliarray[10], Proj_ID)
									nlapiLogExecution('DEBUG', 'Add Parent Function', "Project Name" + ProjectID);
									
									if (_logValidation(Parentsearch)) 
									{
										task.setFieldValue('parent', Parentsearch)
										nlapiLogExecution('DEBUG', 'Add Parent Function', "parent Name" + spliarray[10]);
										}
									
										if (_logValidation(spliarray[4])) 
										{
											task.setFieldText('priority', spliarray[4])
											nlapiLogExecution('DEBUG', 'Add Parent Function', "priority" + spliarray[4]);
											
											if (_logValidation(spliarray[5])) 
											{
												task.setFieldText('status', spliarray[5])
												nlapiLogExecution('DEBUG', 'Add Parent Function', "status" + spliarray[5]);
												
												if (_logValidation(spliarray[17])) 
												{
													task.setFieldText('constrainttype', spliarray[17])
													nlapiLogExecution('DEBUG', 'Add Parent Function', "constrainttype" + spliarray[17]);
													
													if (_logValidation(spliarray[25])) 
													{
														task.setFieldValue('custevent_cisinformation', spliarray[25])
														nlapiLogExecution('DEBUG', 'Add Parent Function', "CIS Information" + spliarray[25]);
														
	 													var taskid = nlapiSubmitRecord(task);
     													nlapiLogExecution('DEBUG', 'Add Action Function', "taskid==" + taskid);
													}
													else 
													{
														status.push("<ol> CIS Information is blank for task " + spliarray[1] + "</ol>")
													}
												}
												else {
													status.push("<ol> Constaint type is blank for task " + spliarray[1] + "</ol>")
												}
											}
											else {
												status.push("<ol> Status is blank for task " + spliarray[1] + "</ol>")
											}
										}
										
										else {
											status.push("<ol> Priority is blank for task " + spliarray[1] + "</ol>")
										}
									
								
						}
						else 
						{
							status.push("<ol> Project name is blank for project " + ProjectID + "</ol>")
						}
					}					
					else 
					{
						status.push("<ol> Task name is blank for project " + ProjectID + "</ol>")
					}
	}
	else
	{
		status.push("The Task "+spliarray[1]+" is already created")
	}
	
	updateStatus = status ;
    
    nlapiLogExecution('DEBUG', 'Add Parent Function', "updateStatus=== " + updateStatus);
    return updateStatus.toString();
}
function addChildTask(records,ProjectID)
{
    nlapiLogExecution('DEBUG', 'Add Action Function', "*****************In Add Child Function*************");
    var updateStatus = new Array();
    
    var status = new Array();
    var flag = '';
    var spliarray = records;
    
    nlapiLogExecution('DEBUG', 'Add Action Function', "spliarray=== " + spliarray);
	
    var Proj_ID = ProjectID;
    nlapiLogExecution('DEBUG', 'Add Action Function', "ProjectID" + ProjectID);
	
    var ResourceName = spliarray[7]
    nlapiLogExecution('DEBUG', 'Add Action Function', "ResourceName==" + ResourceName);
	
    if (_logValidation(spliarray[1])) 
	{
        var searchtaskName = spliarray[1];
        nlapiLogExecution('DEBUG', 'Add Action Function', "searchtaskName=== " + searchtaskName);
		
        if (_logValidation(spliarray[10])) 
		{
            var parentTaskName = spliarray[10]
            nlapiLogExecution('DEBUG', 'Add Action Function', "parentTaskName=== " + parentTaskName);
			
                var Project = nlapiLoadRecord('job', Proj_ID)
                nlapiLogExecution('DEBUG', 'Add Action Function', " Load Project=== " + Project);
				
                var ProjectResources = Project.getLineItemCount('jobresources')
                nlapiLogExecution('DEBUG', 'Add Action Function', "ProjectResources=== " + ProjectResources);
				
                for (var i = 1; i <= ProjectResources; i++) 
				{
                    var ProjectResourceID = Project.getLineItemText('jobresources', 'jobresource', i);
                    nlapiLogExecution('DEBUG', 'Add Action Function', "ProjectResourceID=== " + ProjectResourceID);
                    if (ProjectResourceID == ResourceName) 
					{
                        flag = false;
                        break;
                    }
                    else 
					{
                        flag = true;
                    }
                }
                if (flag == true) 
				{
                    if (_logValidation(ResourceName)) 
					{
                        var resouceid = getResourseID(ResourceName)
                        nlapiLogExecution('DEBUG', 'Add Action Function', "resouceid=== " + resouceid);
						
                        if (_logValidation(resouceid)) 
						{
                            Project.selectNewLineItem('jobresources');
                            
                            Project.setCurrentLineItemValue('jobresources', 'jobresource', resouceid)
                            
                            Project.commitLineItem('jobresources');
                        }
                        else 
						{
                            status.push("<ol> Resource " + ResourceName + " is not present in netsuite.")
                        }
                    }
                    else 
					{
                        status.push("<ol> Resource is blank in CSV file</ol>")
                    }
                    
                }
                
                var id = nlapiSubmitRecord(Project,true,true);
                nlapiLogExecution('DEBUG', 'Add Action Function', "id" + id);
                
                var taskStatus = SearchTask(searchtaskName, id)
                nlapiLogExecution('DEBUG', 'Add Action Function', "taskStatus=== " + taskStatus);
				
                //=========================================Updation Of Task=====================================//
                if (_logValidation(taskStatus)) 
				{
                    nlapiLogExecution('DEBUG', 'Add Action Function', "***********Update Task************* ");
					
                    var task = nlapiLoadRecord('projectTask', taskStatus)
                    nlapiLogExecution('DEBUG', 'Add Action Function', "task=== " + task);
					
                    var appendCISInfo = task.getFieldValue('custevent_cisinformation')
					
					if(!_logValidation(appendCISInfo))
					{
						task.setFieldValue('custevent_cisinformation', spliarray[25])
					}
					else
					{
						appendCISInfo = appendCISInfo + "," + spliarray[25]
                    	task.setFieldValue('custevent_cisinformation', appendCISInfo)
					
					}
                    nlapiLogExecution('DEBUG', 'Add Action Function', "appendCISInfo" + appendCISInfo);
					
                   
                    nlapiLogExecution('DEBUG', 'Add Action Function', "CIS Information" + spliarray[25]);
                
					if (_logValidation(spliarray[7])) 
					{
						var resouceid = getResourseID(spliarray[7])
						nlapiLogExecution('DEBUG', 'Add Assignee', 'resouceid :' + resouceid);
						
						if (_logValidation(resouceid)) 
						{
							task.selectNewLineItem('assignee');
							//Assign  Resource to line item
							
							task.setCurrentLineItemValue('assignee', 'resource', resouceid)
							
							if (_logValidation(spliarray[8])) 
							{
								var ServiceID = getServiceItemID(spliarray[8])
								nlapiLogExecution('DEBUG', 'Add Assignee', 'ServiceID :' + ServiceID);
								
								if (_logValidation(ServiceID)) 
								{
									task.setFieldValue('serviceitem', ServiceID)
									
									if (_logValidation(spliarray[9])) 
									{
										task.setCurrentLineItemValue('assignee', 'estimatedwork', spliarray[9])
										nlapiLogExecution('DEBUG', 'Add Assignee', 'Estimatedwork=== :' + spliarray[9]);
										if (_logValidation(spliarray[6])) 
										{
											task.setCurrentLineItemValue('assignee', 'unitcost', spliarray[6])
											nlapiLogExecution('DEBUG', 'Add Assignee', 'unitcost=== :' + spliarray[6]);
											task.commitLineItem('assignee');
											
										}
										else 
										{
											status.push("<ol> The Unit Cost is blank for this task :=" + searchtaskName + "</ol>")
										}
									}
									else 
									{
										status.push("<ol>The estimated work is blank for task" + searchtaskName + "</ol>")
									}
								}
								else 
								{
									status.push("<ol>The Service item " + spliarray[8] + " is not Present in Netsuite </ol>")
								}
							}
							else 
							{
								status.push("<ol>The Service Item is blank for task" + searchtaskName+"</ol>")
							}
							
						}
						else
						{
							status.push("<ol>The Resource  " + spliarray[7] + " is not Present in Netsuite </ol>")
						}
					}
					else 
					{
						status.push("<ol>The Resource is blank for task" + searchtaskName+"</ol>")
					}
					try 
					{
						var taskid = nlapiSubmitRecord(task);
						nlapiLogExecution('DEBUG', 'Add Action Function', "taskid==" + taskid);
					} 
					catch (e) 
					{
						var message = e.message;
						nlapiLogExecution('DEBUG', 'Create Project', 'message=' + message);
						
						if (message == 'Resource must be unique') 
						{
							status.push("<ol>For this Task " + spliarray[1] + " Same Resource can not be added in the list. </ol>")
						}
						else 
						{
							status.push(message)
						}
						
					}
					
                }
                //=========================================Creation Of Task=====================================//
                else 
				{
                    nlapiLogExecution('DEBUG', 'Add Action Function', "***********Create Task************* ");
                    var task = nlapiCreateRecord('projectTask')
                    nlapiLogExecution('DEBUG', 'Add Action Function', "task" + task);
                    if (_logValidation(spliarray[1])) 
					{
                        task.setFieldValue('title', spliarray[1])
                        nlapiLogExecution('DEBUG', 'Add Action Function', "Task Name" + spliarray[1]);
                        if (_logValidation(spliarray[2])) 
						{
                            //var projid = searchProject(ProjectID)
                            
                                task.setFieldValue('company', Proj_ID)
                                nlapiLogExecution('DEBUG', 'Add Action Function', "Project Name" + Proj_ID);
                                if (_logValidation(spliarray[10])) 
								{
                                    //var Parentsearch = SearchTask(spliarray[10], spliarray[15])
									 var Parentsearch = SearchTask(spliarray[10], Proj_ID)
                                    nlapiLogExecution('DEBUG', 'Add Action Function', "Project Name" + ProjectID);
                                    if (_logValidation(Parentsearch)) 
									{
                                        task.setFieldValue('parent', Parentsearch)
                                        nlapiLogExecution('DEBUG', 'Add Action Function', "parent Name" + spliarray[10]);
                                        if (_logValidation(spliarray[4])) 
										{
                                            task.setFieldText('priority', spliarray[4])
                                            nlapiLogExecution('DEBUG', 'Add Action Function', "priority" + spliarray[4]);
                                            if (_logValidation(spliarray[5])) 
											{
                                                task.setFieldText('status', spliarray[5])
                                                nlapiLogExecution('DEBUG', 'Add Action Function', "status" + spliarray[5]);
                                                if (_logValidation(spliarray[17])) 
												{
                                                    task.setFieldText('constrainttype', spliarray[17])
                                                    nlapiLogExecution('DEBUG', 'Add Action Function', "constrainttype" + spliarray[17]);
                                                    if (_logValidation(spliarray[25])) 
													{
                                                        task.setFieldValue('custevent_cisinformation', spliarray[25])
                                                        nlapiLogExecution('DEBUG', 'Add Action Function', "CIS Information" + spliarray[25]);
                                                    }
                                                    else 
													{
                                                        status.push("<ol> CIS Information is blank for task " + spliarray[1] + "</ol>")
                                                    }
                                                }
                                                else 
												{
                                                    status.push("<ol> Constaint type is blank for task " + spliarray[1] + "</ol>")
                                                }
                                            }
                                            else 
											{
                                                status.push("<ol> Status is blank for task " + spliarray[1] + "</ol>")
                                            }
                                        }
                                        
                                        else 
										{
                                            status.push("<ol> Priority is blank for task " + spliarray[1] + "</ol>")
                                        }
                                    }
                                    else 
									{
                                        status.push("<ol> Parent Task name is not created </ol>")
                                    }
                                    
                                }
                                
                                else 
								{
                                    status.push("<ol> Parent Task name is blank for task " + spliarray[1] + "</ol>")
                                }
                          
                            
                        }
                        
                        else {
                            status.push("<ol> Project name is blank for project " + ProjectID + "</ol>")
                        }
                    }
                    
                    else {
                        status.push("<ol> Task name is blank for project " + ProjectID + "</ol>")
                        
                    }
                    
                    task.selectNewLineItem('assignee');
					
                    var resouceid = getResourseID(spliarray[7])
                    nlapiLogExecution('DEBUG', 'Add Action Function', "resouceid=== " + resouceid);
                    
                    if (_logValidation(resouceid)) 
					{
                    
                        //Assign  Resource to line item
                        task.setCurrentLineItemValue('assignee', 'resource', resouceid)
                        if (_logValidation(spliarray[9])) 
						{
                            task.setCurrentLineItemValue('assignee', 'estimatedwork', spliarray[9])
                            if (_logValidation(spliarray[6])) 
							{
                                task.setCurrentLineItemValue('assignee', 'unitcost', spliarray[6])
                            }
                            else 
							{
                                status.push("<ol> The Unit Cost is blank for this task :=" + searchtaskName + "</ol>")
                            }
                            
                        }
                        else {
                            status.push("<ol>The estimated work is blank for task" + searchtaskName + "</ol>")
                        }
                    }
                    else 
					{
                        status.push("<ol>The Resource is blank for task" + searchtaskName + "</ol>")
                    }
                    
                    
                    task.commitLineItem('assignee');
					
                    var taskid = nlapiSubmitRecord(task);
                    nlapiLogExecution('DEBUG', 'Add Action Function', "taskid==" + taskid);
                }
           
            
        }
        else {
            status.push("<ol>The Task name is blank</ol>")
        }
    }
    else 
	{
        status.push("<ol>The Parent Task is blank</ol>")
    }
    updateStatus = status 
    
    nlapiLogExecution('DEBUG', 'Add Action Function', "updateStatus=== " + updateStatus);
	
    return updateStatus.toString();
    
}

function addprecedessor(records,ProjectID)
{
    nlapiLogExecution('DEBUG', 'Create Predecessor', '******** Create Predecessor *************');
    var updateStatus = new Array();
    var status = new Array();
    var flag = '';
    var spliarray = records;
    nlapiLogExecution('DEBUG', 'add precedessor', "spliarray=== " + spliarray);
    var Proj_ID = ProjectID;
    nlapiLogExecution('DEBUG', 'add precedessor', "ProjectID" + Proj_ID);
    var taskid = SearchTask(spliarray[1], ProjectID);
    nlapiLogExecution('DEBUG', 'add precedessor', "taskid" + taskid);
    if (_logValidation(taskid)) 
	{
        var Taskrecord = nlapiLoadRecord('projectTask', taskid)
        nlapiLogExecution('DEBUG', 'Update Task', 'Taskrecord :' + Taskrecord);
        
        var SplitPredecessor = spliarray[11]
        nlapiLogExecution('DEBUG', 'Update Task', 'SplitPredecessor :' + SplitPredecessor);
        
        var precearry = new Array();
        precearry.push(SplitPredecessor)
        nlapiLogExecution('DEBUG', 'Update Task', 'precearry :' + precearry);
        var SplitPredecessorlength = precearry.toString();
        nlapiLogExecution('DEBUG', 'Update Task', 'SplitPredecessorlength :' + SplitPredecessorlength);
        var predecessor = SplitPredecessorlength.split(':');
        nlapiLogExecution('DEBUG', 'Update Task', 'predecessor :' + predecessor);
        var lengthprece = predecessor.length;
        nlapiLogExecution('DEBUG', 'Update Task', 'lengthprece :' + lengthprece);
        var pretask = lengthprece - 1;
        nlapiLogExecution('DEBUG', 'Update Task', 'pretask :' + predecessor[pretask]);
        //nlapiLogExecution('DEBUG', 'Update Task', 'spiltprece :' + spiltprece);	
        var searchtask = predecessor[pretask];
        nlapiLogExecution('DEBUG', 'Update Task', 'searchtask :' + searchtask);
        
        var updatetask = SearchTask(searchtask.trim(), Proj_ID);
        nlapiLogExecution('DEBUG', 'Update Task', 'updatetask :' + updatetask);
        if (_logValidation(updatetask)) {
            Taskrecord.selectNewLineItem('predecessor');
            
            Taskrecord.setCurrentLineItemValue('predecessor', 'task', updatetask)
            
            if (_logValidation(spliarray[12])) {
                if (spliarray[12] == 'Finish-To-Start') {
                    nlapiLogExecution('DEBUG', 'set Type Predecessor', '******** Finish-To-Start Set *************');
                    Taskrecord.setCurrentLineItemValue('predecessor', 'type', 'FS')
                }
                else {
                
                    nlapiLogExecution('DEBUG', 'set Type Predecessor', '******** Start-To-Start Set *************');
                    Taskrecord.setCurrentLineItemValue('predecessor', 'type', 'SS')
                }
            }
            
            Taskrecord.commitLineItem('predecessor');
            var estimatedwork_total = Taskrecord.getFieldValue('estimatedwork')
            nlapiLogExecution('DEBUG', 'Modify Action Function', "estimatedwork_total" + estimatedwork_total);
            var actualwork = Taskrecord.getFieldValue('actualwork')
            nlapiLogExecution('DEBUG', 'Modify Action Function', "actualwork" + actualwork);
            if (actualwork > estimatedwork_total) {
                Taskrecord.setFieldValue('custevent_budget_hours', actualwork)
            }
            else {
                Taskrecord.setFieldValue('custevent_budget_hours', estimatedwork_total)
            }
            
            id = nlapiSubmitRecord(Taskrecord);
        }
        else 
		{
            status = "<ol> Task" + searchtask + " is not present</ol>"
        }
    }
    else {
        status.push("<ol> The task is not present</ol>")
    }
	if(status.length!=0)
	{
		return status.toString();
	}
	return '';
	
	
}



function CheckResourcesService(records)
{
    var nonItems = new Array();
    var cnt = 0;
    var resourseCount = new Array();
    var serviceCount = new Array();
    if (_logValidation(records)) {
        var tranID = '';
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Create Project', 'split Length :' + split.length);
        if (_logValidation(split)) {
            var splitArray = split[0].toString().split('|');
            tranID = splitArray[2];
            
            if (_logValidation(tranID)) 
			{
				var Resource = getResourse();
    			nlapiLogExecution('DEBUG', 'Create Project', 'Resource == :' + Resource);
				 var ServiceItem = getService()
				nlapiLogExecution('DEBUG', 'Create Project', 'ServiceItem == :' + ServiceItem);
                for (var j = 0; j < split.length; j++) 
				{
                    var splitArray = split[j].toString().split('|');
                    var UnitCost = splitArray[6]
                    var EstimatedWork = splitArray[9]
                    var Resource = splitArray[7]
                    var ServiceItem_name = splitArray[8]
                    if (!_logValidation(UnitCost) && !_logValidation(EstimatedWork) && !_logValidation(Resource) && !_logValidation(ServiceItem_name)) 
					{
                    
                    }
                    else 
                    
                        if (_logValidation(splitArray[6])) 
						{
                            nlapiLogExecution('DEBUG', 'Create Project', 'Unit Cost == :' + splitArray[6]);
                            
                            if (_logValidation(splitArray[9])) 
							{
                                nlapiLogExecution('DEBUG', 'Create Project', 'EstimatedCost == :' + splitArray[9]);
                                
                                if (_logValidation(splitArray[7])) 
								{
                                    
									
                                    if (_logValidation(splitArray[8])) 
									{
                                        if (!_logValidation(Resource[splitArray[7]]))
										{
                                            if (!_logValidation(ServiceItem[splitArray[8]])) 
											{
												nlapiLogExecution('DEBUG', 'Create Project', 'Resource :' + splitArray[7]);
												 nlapiLogExecution('DEBUG', 'Create Project', 'ServiceItemID == :' + splitArray[8]);
                                                resourseCount.push(splitArray[7]);
                                                serviceCount.push(splitArray[8]);
                                            }
                                            else 
											{
                                                var ServiceItemname = "<ol>ServiceItem:-- " + splitArray[8] + " not found in netsuite.</ol>"
                                                nonItems.push(ServiceItemname)
                                            }
                                        }
                                        else 
										{
                                            var resourse = "<ol> Resourse :--" + splitArray[7] + " not found in netsuite.</ol>"
                                            nonItems.push(resourse)
                                        }
                                    }
                                    else 
									{
                                        nonItems.push("<ol> For this " + splitArray[1] + " Task Service Item is Blank in CSV file.</ol>")
                                    }
                                }
                                else 
								{
                                    nonItems.push("<ol> For this " + splitArray[1] + " Task Resource is Blank in CSV file.</ol>")
                                }
                            }
                            
                            else 
							{
                                nonItems.push("<ol>For this Task " + splitArray[1] + "  Estimated Work is Blank in CSV File.</ol>")
                            }
                            
                        }
                        else 
						{
                            nonItems.push("<ol> For this Task " + splitArray[1] + " Unit Cost is Blank in CSV File.</ol>")
                        }
                }
                
            }
            else {
                nonItems.push("The Project Id is blank in CSV file.")
            }
        }
        else {
            nonItems.push("Data is not present in CSV file")
        }
    }
	
    status = resourseCount + "#" + serviceCount + "#" + nonItems;
    nlapiLogExecution('DEBUG', 'Create Project', 'status ==' + status);
    
    return status.toString()
}

function DeleteAction(records)
{
    nlapiLogExecution('DEBUG', 'Delete Action Function', "*****************In Delete Function*************");
    var updateStatus = new Array();
    var status = new Array();
	var taskresourcearray=new Array();
    var flag = '';
    var splitarray = records;
    nlapiLogExecution('DEBUG', 'Delete Action Function', 'splitarray :' + splitarray);
    var Searchtask = splitarray[1]
	var FileResource=splitarray[7]
    nlapiLogExecution('DEBUG', 'Delete Action Function', 'SearchTask :' + Searchtask);
    if (splitarray[2]) 
	{
        var projectID = searchProject(splitarray[2])
        nlapiLogExecution('DEBUG', 'Delete Action Function', 'projectID :' + projectID);
        if (_logValidation(projectID)) 
		{
            if (_logValidation(Searchtask)) 
			{
                var deleteTask = SearchTask(Searchtask, projectID)
                nlapiLogExecution('DEBUG', 'Delete Action Function', 'deleteTask :' + deleteTask);
				
                if (_logValidation(deleteTask)) 
				{
                    //percenttimecomplete
                    var loadtask = nlapiLoadRecord('projectTask', deleteTask)
                    nlapiLogExecution('DEBUG', 'Delete Action Function', 'deleteprojecttask :' + deleteprojecttask);
					var percentcomplete = loadtask.getFieldValue('percenttimecomplete')
					nlapiLogExecution('DEBUG', 'Delete Action Function', 'deleteprojecttask :' + deleteprojecttask);
					if (percentcomplete == '0.0%') 
					{
						if (_logValidation(loadtask)) 
						{
							var linecount = loadtask.getLineItemCount('assignee')
							nlapiLogExecution('DEBUG', 'Delete Action Function', 'linecount== :' + linecount);
							
							for (var k = 1; k <= linecount; k++) 
							{
								nlapiLogExecution('DEBUG', 'Delete Action Function', 'k ===' + k);
								var taskresource = loadtask.getLineItemText('assignee', 'resource', k)
								nlapiLogExecution('DEBUG', 'Delete Action Function', 'taskresource ===' + taskresource);
								if (taskresource == FileResource) 
								{
									loadtask.removeLineItem('assignee', k)
								}
								else 
								{
									continue;
								}
							}
							
							var Repeatlinecount = loadtask.getLineItemCount('assignee')
							nlapiLogExecution('DEBUG', 'Delete Action Function', 'Repeatlinecount== :' + Repeatlinecount);
							var id = nlapiSubmitRecord(loadtask, true, true)
							nlapiLogExecution('DEBUG', 'Delete Action Function', 'id :===' + id);
							
							if (Repeatlinecount == 0) 
							{
								var deleteprojecttask = nlapiDeleteRecord('projectTask', deleteTask)
								nlapiLogExecution('DEBUG', 'Delete Action Function', 'deleteprojecttask :' + deleteprojecttask);
							}
							
						}
					}
					else 
					{
						loadtask.setFieldText('status', 'Completed')
						var id = nlapiSubmitRecord(loadtask, true, true)
						nlapiLogExecution('DEBUG', 'Delete Action Function', 'id :===' + id);
					}
									
                }
				else 
					{
						status.push("<ol>The Delete Task "+splitarray[1]+" is not present in Netsuite</ol>")
					}             
            }
			 else 
				{
                    status.push("<ol>The Task is " + Searchtask + " is blank in Given CSV file.</ol>")
                }
        }
        else 
		{
            status.push("<ol>The Project ID " + splitarray[2] + " is not present in netsuite</ol>")
        }
    }
    else 
	{    
        status.push("<ol>The Project ID is blank in CSV file</ol>")
    }
	
    return status.toString();
}


// END FUNCTION =====================================================
