// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_CreateCustomerDeposit.js
     Author: Rujuta K.
     Company: Aashna Cloud Tech
     Date: 16/09/2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type){

    var contextObj = nlapiGetContext();
    var beginUsage = contextObj.getRemainingUsage();
    nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecordstatus', null, 'is', '');
    
    
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    nlapiLogExecution('DEBUG', 'Search Results', "columns");
    
    
   
	
    var nonItems = new Array();
    var results = nlapiSearchRecord('customrecord_customcustomerdeposits', null, filter, columns);
    nlapiLogExecution('DEBUG', 'Search Results', 'results== :' + results.length);
    
    if (results != null) 
	{
        //for (var i_i = 0; i_i < results.length; i_i++) 
        for (var i_i = 0; i_i < results.length; i_i++) {
            var id = results[i_i].getValue('internalid')
            nlapiLogExecution('DEBUG', 'schedulerFunction', 'id== :' + id);
            var o_custobj = nlapiLoadRecord('customrecord_customcustomerdeposits', id);
            nlapiLogExecution('DEBUG', 'schedulerFunction', 'o_custobj== :' + o_custobj);
            var status = o_custobj.getFieldValue('custrecordstatus');
            nlapiLogExecution('DEBUG', 'schedulerFunction', 'status== :' + status);
           // if (status == 'Pass' || status != '' || status != 'undefined' || status != null) {
          //   nlapiLogExecution('DEBUG', 'Search Results', "i am in if");
          //  }
           // else {
				  nlapiLogExecution('DEBUG', 'Search Results', "i am in else");
                var status_id = CreateDeposit(id);
                nlapiLogExecution('DEBUG', 'schedulerFunction', 'status_id== :' + status_id);
                var internalid = status_id.toString().split("#");
                nlapiLogExecution('DEBUG', 'schedulerFunction', 'internalid== :' + internalid);
                var cust_id = internalid[0];
                nlapiLogExecution('DEBUG', 'schedulerFunction', 'cust_id== :' + cust_id);
                var status2 = internalid[1];
                nlapiLogExecution('DEBUG', 'schedulerFunction', 'status2== :' + status2);
                if (status2 != '' && status2 != null) {
                    nlapiLogExecution('DEBUG', 'updateCustomrecord', 'I am in if loop');
                    o_custobj.setFieldValue('custrecord_errorlog', status2);
                    o_custobj.setFieldValue('custrecordstatus', 'Fail')
                }
                else {
                    nlapiLogExecution('DEBUG', 'updateCustomrecord', 'I am in else loop');
                    o_custobj.setFieldValue('custrecord_errorlog', status2);
                    o_custobj.setFieldValue('custrecord_transactiontype', cust_id);
                    o_custobj.setFieldValue('custrecordstatus', 'Pass');
                    
                }
                var newid = nlapiSubmitRecord(o_custobj, true, true);
                nlapiLogExecution('DEBUG', 'updateCustomrecord', 'newid== :' + newid);
                
                var EndUsage = contextObj.getRemainingUsage();
                nlapiLogExecution('DEBUG', 'Search Results', 'EndUsage :' + EndUsage);
                if (EndUsage < 500) {
                    Schedulescriptafterusageexceeded(id);
                }
         //   }
        }
    }
    
    
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined) {
        return true;
    }
    else {
        return false;
    }
}

function updateCustomrecord(error, id){
    var id = nlapiLoadRecord('customrecord_customcustomerdeposits', id);
    nlapiLogExecution('DEBUG', 'updateCustomrecord', 'results== :' + id);
    if (error == '') {
        id.setFieldValue('custrecord_errorlog', error);
        id.setFieldValue('custrecordstatus', 'Pass')
    }
    else {
        id.setFieldValue('custrecord_errorlog', error);
        id.setFieldValue('custrecordstatus', 'Fail')
        
    }
    var newid = nlapiSubmitRecord(id, true, true);
    nlapiLogExecution('DEBUG', 'updateCustomrecord', 'newid== :' + newid);
}

function CreateDeposit(id){
    var nonItems = new Array();
    try {
    
        //nlapiLogExecution('DEBUG', 'CreateDeposit', 'id== :' + id);
        var o_custobj = nlapiLoadRecord('customrecord_customcustomerdeposits', id);
        nlapiLogExecution('DEBUG', 'CreateDeposit', 'o_custobj== :' + o_custobj);
        var s_customername = o_custobj.getFieldValue('custrecord_customer')
        nlapiLogExecution('DEBUG', 'Search Results', 'customer_name== :' + s_customername);
        var s_deposit = o_custobj.getFieldValue('custrecord_deposit')
        nlapiLogExecution('DEBUG', 'Search Results', 's_deposit== :' + s_deposit);
        var s_date = o_custobj.getFieldValue('custrecord_date')
        nlapiLogExecution('DEBUG', 'Search Results', 's_date== :' + s_date);
        var s_postingperiod = o_custobj.getFieldValue('custrecord_postingperiod')
        nlapiLogExecution('DEBUG', 'Search Results', 's_postingperiod== :' + s_postingperiod);
        var s_amount = o_custobj.getFieldValue('custrecord_amount')
        nlapiLogExecution('DEBUG', 'Search Results', 's_amount== :' + s_amount);
        var s_currency = o_custobj.getFieldValue('custrecord_currency')
        nlapiLogExecution('DEBUG', 'Search Results', 's_currency== :' + s_currency);
        var s_account = o_custobj.getFieldValue('custrecord_account')
        nlapiLogExecution('DEBUG', 'Search Results', 's_account== :' + s_account);
        var s_exchangerate = o_custobj.getFieldValue('custrecord_exchangerate')
        nlapiLogExecution('DEBUG', 'Search Results', 's_exchangerate== :' + s_exchangerate);
        var s_class = o_custobj.getFieldValue('custrecord_class')
        nlapiLogExecution('DEBUG', 'Search Results', 's_class== :' + s_class);
        var s_location = o_custobj.getFieldValue('custrecord_location')
        nlapiLogExecution('DEBUG', 'Search Results', 's_location== :' + s_location);
        var s_department = o_custobj.getFieldValue('custrecord1392')
        nlapiLogExecution('DEBUG', 'Search Results', 's_department== :' + s_department);
        var s_memo = o_custobj.getFieldValue('custrecord_memo')
        nlapiLogExecution('DEBUG', 'Search Results', 's_memo== :' + s_memo);
        var s_subsidiary = o_custobj.getFieldValue('custrecord_subsidiary')
        nlapiLogExecution('DEBUG', 'Search Results', 's_subsidiary== :' + s_subsidiary);
        var s_paymentmethod = o_custobj.getFieldValue('custrecord_paymentmethod')
        nlapiLogExecution('DEBUG', 'Search Results', 's_paymentmethod== :' + s_paymentmethod);
        
        //************************** Create Customer Deposite**********************//
        var s_CustDepositID = nlapiCreateRecord('customerdeposit')
        nlapiLogExecution('DEBUG', 'Search Results', 's_CustDepositID== :' + s_CustDepositID);
        //************* set Customer*********//
        
        s_CustDepositID.setFieldValue('customer', s_customername)
        s_CustDepositID.setFieldValue('trandate', s_date)
        //s_CustDepositID.setFieldValue('postingperiod',s_postingperiod)
        s_CustDepositID.setFieldValue('payment', s_amount)
        //s_CustDepositID.setFieldValue('currency', s_currency)
        s_CustDepositID.setFieldValue('exchangerate', s_exchangerate)
        s_CustDepositID.setFieldValue('account', s_account)
        s_CustDepositID.setFieldValue('location', s_location)
        s_CustDepositID.setFieldValue('class', s_class)
        s_CustDepositID.setFieldValue('memo', s_memo)
        s_CustDepositID.setFieldValue('department', s_department)
        s_CustDepositID.setFieldValue('paymentmethod', s_paymentmethod)
        if (s_paymentmethod == 2) {
            var Checknumber = o_custobj.getFieldValue('custrecord_check')
            nlapiLogExecution('DEBUG', 'Search Results', 'Checknumber== :' + Checknumber);
            s_CustDepositID.setFieldValue('checknum', Checknumber)
        }
        var s_CustID = nlapiSubmitRecord(s_CustDepositID, true, true)
        nlapiLogExecution('DEBUG', 'Search Results', 's_CustID== :' + s_CustID);
        nonItems.push(s_CustID + "#" + "")
    } 
    catch (e) {
        nlapiLogExecution('DEBUG', 'Catch Exception :===' + e.getDetails());
        var message = e.message;
        if (message.toString().match(/Invalid class reference key/)) {
            nonItems.push("" + "#" + "Customer Subsidiary is not matching with class Subsidiary")
        }
        if (message.toString().match(/Invalid account reference/)) {
            nonItems.push("" + "#" + "Customer Subsidiary is not matching with Account Subsidiary")
        }
    }
    
    return nonItems.toString();
    //updateCustomrecord(nonItems, id)
}

function Schedulescriptafterusageexceeded(custId){
    ////Define all parameters to schedule the script for voucher generation.
    var params = new Array();
    params['status'] = 'scheduled';
    params['runasadmin'] = 'T';
    params['custscript_custid'] = custId;
    var startDate = new Date();
    params['startdate'] = startDate.toUTCString();
    
    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
    nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    
    ////If script is scheduled then successfuly then check for if status=queued
    if (status == 'QUEUED') {
        nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
    }
}


// END FUNCTION =====================================================
