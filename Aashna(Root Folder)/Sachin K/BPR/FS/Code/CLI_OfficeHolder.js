// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_Organisation.js
	Author:
	Company:
	Date:
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...

var opertaionType
}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function offhold_pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
opertaionType=type
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function offhold_saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
 if(opertaionType=='create')
   {
   		nlapiSetFieldValue('custrecord_fs_portal_oh_office_h_status',8,true,true)
   }
 //alert('opertaionType->'+opertaionType)
   if(opertaionType=='edit')
   {
    	var orgstatus=nlapiGetFieldValue('custrecord_fs_portal_oh_office_h_status')
    	//alert('orgstatus->'+orgstatus)
    	if(orgstatus==3)
    	{
    		nlapiSetFieldValue('custrecord_fs_portal_oh_office_h_status',4,true,true)
    	}

   }

	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function callSubmit()
{
    var flag=0
	var First_Name=nlapiGetFieldValue('custrecord_fs_portal_oh_first_name')
	//alert(Organisation_Type)
	if(First_Name==''||First_Name==null)
	{
		alert('Please enter the value for First Name')
		flag=1
	}

	var Preferred_Name=nlapiGetFieldValue('custrecord_fs_portal_oh_preferred_name')
	//alert('Preferred_Name'+Preferred_Name)
	if(Preferred_Name==''||Preferred_Name==null)
	{
		alert('Please enter the value for Preferred Name')
		flag=1
	}

	var Last_Name=nlapiGetFieldValue('custrecord_fs_portal_oh_last_name')
	//alert('Last_Name'+Last_Name)
	if(Last_Name==''||Last_Name==null)
	{
		alert('Please enter the value for Last Name')
		flag=1
	}


	var RoleIn_Organisation=nlapiGetFieldValue('custrecord_fs_portal_oh_role_in_org')
	//alert('RoleIn_Organisation'+RoleIn_Organisation)
	if(RoleIn_Organisation==''||RoleIn_Organisation==null)
	{
		alert('Please enter the value for Role In Organisation')
		flag=1
	}



	var Email=nlapiGetFieldValue('custrecord_fs_portal_oh_email')
	//alert('Street_Address'+Street_Address)
	if(Email==''||Email==null)
	{
		alert('Please enter the value for Email')
		flag=1
	}




	var Phone_Number=nlapiGetFieldValue('custrecord_fs_portal_oh_phone_number')
	//alert('Phone_Number'+Phone_Number)
	if(Phone_Number==''||Phone_Number==null)
	{
		alert('Please enter the value for Phone Number')
		flag=1
	}

	var Funds=nlapiGetFieldValue('custrecord_fs_portal_oh_authorised_funds')
	//alert('Funds'+Funds)
	if(Funds==''||Funds==null)
	{
		alert('Please enter the value for Authorised to Apply for Funds?')
		flag=1
	}

	var Bank_Signatory=nlapiGetFieldValue('custrecord_fs_portal_oh_bank_signatory')
	//alert('Bank_Signatory'+Bank_Signatory)
	if(Bank_Signatory==''||Bank_Signatory==null)
	{
		alert('Please enter the value for Bank Signatory')
		flag=1
	}

	var Address_Line1=nlapiGetFieldValue('custrecord_fs_portal_oh_address_line_1')
	//alert('Address_Line1'+Address_Line1)
	if(Address_Line1==''||Address_Line1==null)
	{
		alert('Please enter the value for Address Line 1')
		flag=1
	}

	var Suburb=nlapiGetFieldValue('custrecord_fs_portal_oh_suburb')
	//alert('Suburb'+Suburb)
	if(Suburb==''||Suburb==null)
	{
		alert('Please enter the value for Suburb')
		flag=1
	}

	var Town_City=nlapiGetFieldValue('custrecord_fs_portal_oh_town_city')
	//alert('Town_City'+Town_City)
	if(Town_City==''||Town_City==null)
	{
		alert('Please enter the value for Town/City ')
		flag=1
	}



	if(flag==0)
	{
		try
		{
			var recId=nlapiGetRecordId()
			var recType=nlapiGetRecordType()
			var recObj=nlapiLoadRecord(recType,recId)
			recObj.setFieldValue('custrecord_fs_portal_oh_office_h_status',2)
			var id=nlapiSubmitRecord(recObj,true,true)
			location.reload()
		}
		catch(e)
		{

		}

		//nlapiSetFieldValue('custrecord_fs_portal_oh_office_h_status',2,true,true)
	}

}


// END FUNCTION =====================================================
