/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       06 Aug 2013     Kevin Zhang
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function upload(request, response) {

    if (request.getMethod() == 'GET') {
        /* create a form */
        var form = nlapiCreateForm('Upload your file and attch to your Organisation');

        /* build the fields */
        var organizationField = form.addField('custrecord_organization', 'select', 'Select your organization', null);
        organizationField.setMandatory(true);
        setList(organizationField);

        var fileField = form.addField('custrecord_file', 'file', 'Select File');
        fileField.setLayoutType('outsidebelow', 'startrow');
        fileField.setMandatory(true);

        /* add the "Upload" button */
        form.addSubmitButton('Upload');

        /* write the form */
        response.writePage(form);
    }
    else {

        var organization = request.getParameter('custrecord_organization');
        var file = request.getFile('custrecord_file');

        /* rename the file */
        file.setName(file.getName());

        /* Organisation folder id: 171 */
        var folder_id = 171;

        /* set the folder where this file will be added.*/
        file.setFolder(folder_id);

        /* create and upload the file on the file cabinet using the nlapiSubmitFile */
        var id = nlapiSubmitFile(file);

        /* now attach file to organisation record */
        nlapiAttachRecord('file', id, 'customrecord_portal_organisation', organization);

        /* now write the successful message */
        response.write("Thank You. Your file is uploaded and attached to your Organisation.");
    }
}

// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
    function setList(Field)
    {

        var cureent_User=nlapiGetUser()
        nlapiLogExecution('DEBUG', 'Orginatzation', 'cureent_User  = '+cureent_User );


        /* specify all columns associated with this search */
        var column = new Array();
        column[0] = new nlobjSearchColumn('internalid');
        column[1] = new nlobjSearchColumn('name');

        /* specify the record type and the saved search ID */
        var searchResults = nlapiSearchRecord('customrecord_portal_organisation', 'customsearch93', null, column);

        /* create the drop-down list using the name and internalid from the saved search result */
        Field.addSelectOption('-1', '', true);
        //Field.addSelectOption('100', searchResults.length, false);
        if (searchResults != null && searchResults != '')
        {
            for (var i = 0; i < searchResults.length; i++) {
                Field.addSelectOption(searchResults[i].getValue('internalid'), searchResults[i].getValue('name'), false);
            }
        }
    }

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
