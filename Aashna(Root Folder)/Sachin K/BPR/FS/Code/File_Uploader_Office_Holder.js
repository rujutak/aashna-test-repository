function upload(request, response) {

    if (request.getMethod() == 'GET') {
        /* create a form */
        var form = nlapiCreateForm('Upload your file and attach to your Office Holders');

        /* build the fields */
        var holderField = form.addField('custrecord_holder', 'select', 'Select your office holder', null);
        holderField.setMandatory(true);
        setList(holderField);

        var fileField = form.addField('custrecord_file', 'file', 'Select File');
        fileField.setLayoutType('outsidebelow', 'startrow');
        fileField.setMandatory(true);

        /* add the "Upload" button */
        form.addSubmitButton('Upload');

        /* write the form */
        response.writePage(form);
    }
    else {

        var holder = request.getParameter('custrecord_holder');
        var file = request.getFile('custrecord_file');

        /* rename the file */
        file.setName(file.getName());

        /* Office Holder folder id: 178 */
        var folder_id = 178;

        /* set the folder where this file will be added.*/
        file.setFolder(folder_id)

        /* create and upload the file on the file cabinet using the nlapiSubmitFile */
        var id = nlapiSubmitFile(file)

        /* now attach file to Office Holder record */
        nlapiAttachRecord('file', id, 'customrecord_fs_portal_office_holders', holder)

        /* now write the successful message */
        response.write("Thank You. Your file is uploaded and attached to your Office Holders.");
    }
}

// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
    function setList(Field) {
        var column = new Array();

        column[0] = new nlobjSearchColumn('internalid')
        column[1] = new nlobjSearchColumn('name')
        var searchResults = nlapiSearchRecord("customrecord_fs_portal_office_holders", null, null, column);
        
        Field.addSelectOption('-1', '', true);
        if (searchResults != null && searchResults != '') {
            for (i = 0; i < searchResults.length; i++) {
                Field.addSelectOption(searchResults[i].getValue('internalid'),
                                        searchResults[i].getValue('name'), false);
            }
        }
    }

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================