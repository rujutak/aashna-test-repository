// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
        Script Name:CLI_Organisation.js
        Author:
        Company:
        Date:
        Description:


        Script Modification Log:

        -- Date --			-- Modified By --				--Requested By--				-- Description --




        Below is a summary of the process controls enforced by this script file.  The control logic is described
        more fully, below, in the appropriate function headers and code blocks.

         PAGE INIT
            - pageInit(type)


         SAVE RECORD
            - saveRecord()


         VALIDATE FIELD
            - validateField(type, name, linenum)


         FIELD CHANGED
            - fieldChanged(type, name, linenum)


         POST SOURCING
            - postSourcing(type, name)


        LINE INIT
            - lineInit(type)


         VALIDATE LINE
            - validateLine()


         RECALC
            - reCalc()


         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:





    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...

    var opertaionType
}
// END GLOBAL VARIABLE BLOCK  =======================================
{
	var organisationfieldHasChanged = false;
}




// BEGIN PAGE INIT ==================================================

function org_pageInit(type) {
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
    opertaionType = type;

    nlapiDisableLineItemField('recmachcustrecord_fs_oh_organisation', 'name', true);
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function org_saveRecord() {
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	//var gmApprovalCompleted = nlapiGetFieldValue('custrecord_ga_gm_approval_completed');
  //  if (gmApprovalCompleted=='F') {
  //      nlapiSetFieldValue('custrecord_ga_organisation_status', 2, true, true)
 //   }
    //alert('opertaionType->'+opertaionType)




    //--------------------------------------------------------------------------------------------------
    if(type=='create')
    {
    var authCounter = 0
    var bankCounter = 0
    var lineCount = nlapiGetLineItemCount('recmachcustrecord_fs_oh_organisation')
   // alert('lineCount->'+lineCount)
    for (var i = 1; i <= lineCount; i++)
    {

        var authForfund = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_authorised_funds', i)
        //alert('authForfund->'+authForfund)
        var bankSignatory = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_bank_signatory', i)
        //alert('bankSignatory->'+bankSignatory)
        if (authForfund == 1)
        {
            authCounter = parseInt(authCounter) + 1
            if (bankSignatory == 1)
            {
                bankCounter = parseInt(bankCounter) + 1
            }
        }


    }


//alert("authCounter: " + authCounter + " bankCounter: " + bankCounter)
    if (authCounter >= 3 && bankCounter >= 2) {
        //alert('condition Match')
    }
    else
    {
        alert('Please add 3 Authorised to Apply for Funds  and 2 for Bank signatory')
    }
    }
//--------------------------------------------------------------------------------------------------
    var authCounter = 0
    var bankCounter = 0
    var id=nlapiGetRecordId()
       var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_fs_oh_organisation', null, 'is', id);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_fs_portal_oh_authorised_funds');
		columns[2] = new nlobjSearchColumn('custrecord_fs_portal_oh_bank_signatory');
		var searchresults = nlapiSearchRecord('customrecord_fs_portal_office_holders', null, filters, columns);
		//alert(searchresults)
		if (searchresults != null)
		{
			for (i = 0; i < searchresults.length; i++)
			{


					var internalid = searchresults[i].getValue('internalid');
					var authForfund = searchresults[i].getValue('custrecord_fs_portal_oh_authorised_funds');
					var bankSignatory = searchresults[i].getValue('custrecord_fs_portal_oh_bank_signatory');
					//alert('authForfund->'+authForfund)
					//alert('bankSignatory->'+bankSignatory)
					if (authForfund == 1)
			        {
			            authCounter = parseInt(authCounter) + 1
			            if (bankSignatory == 1)
			            {
			                bankCounter = parseInt(bankCounter) + 1
			            }
			        }


			}

		}
		 if (authCounter >= 3 && bankCounter >= 2)
		 {
        	//alert('condition Match')
    	 }
	    else
	    {
	        alert('Please add 3 Authorised to Apply for Funds  and 2 for Bank signatory')
	    }


//--------------------------------------------------------------------------------------------------
    var flag = 0
    var Organisation_Type = nlapiGetFieldValue('custrecord_ga_organisation_type')
    //alert(Organisation_Type)
    if (Organisation_Type == '' || Organisation_Type == null) {
        alert('Please enter the value for Organisation Type')
        flag = 1
    }

    var Coverage = nlapiGetFieldValue('custrecord_ga_coverage')
    //alert('Coverage'+Coverage)
    if (Coverage == '' || Coverage == null) {
        alert('Please enter the value for Coverage')
        flag = 1
    }

    var Phone_Number = nlapiGetFieldValue('custrecord_ga_phone_number')
    //alert('Phone_Number'+Phone_Number)
    if (Phone_Number == '' || Phone_Number == null) {
        alert('Please enter the value for Phone Number')
        flag = 1
    }

    /*
    var WebSite = nlapiGetFieldValue('custrecord_ga_web_site')
    //alert('WebSite'+WebSite)
    if (WebSite == '' || WebSite == null) {
        alert('Please enter the value for Web Site')
        flag = 1
    }
    */

    /*var Answer=nlapiGetFieldValue('custrecord_ga_description_answer')
	//alert('Answer'+Answer)
	if(Answer==''||Answer==null)
	{
		alert('Please enter the value for Answer')
		flag=1
	}*/


    var Street_Address = nlapiGetFieldValue('custrecord_ga_street_address')
    //alert('Street_Address'+Street_Address)
    if (Street_Address == '' || Street_Address == null) {
        alert('Please enter the value for Street Address')
        flag = 1
    }

    var Town_City = nlapiGetFieldValue('custrecord_ga_town_city')
    //alert('Town_City'+Town_City)
    if (Town_City == '' || Town_City == null) {
        alert('Please enter the value for Town/City')
        flag = 1
    }


    var Postal_Code = nlapiGetFieldValue('custrecord_ga_postal_code')
    //alert('Postal_Code'+Postal_Code)
    if (Postal_Code == '' || Postal_Code == null) {
        alert('Please enter the value for Postal Code')
        flag = 1
    }

    var gst = nlapiGetFieldValue('custrecord_ga_org_register_gst')
    //alert('gst'+gst)
    if (gst == '' || gst == null) {
        alert('Please enter the value for Is your organisation registered for GST?')
        flag = 1
    }

    var incorporated = nlapiGetFieldValue('custrecord_ga_org_incorporated')
    //alert('incorporated'+incorporated)
    if (incorporated == '' || incorporated == null) {
        alert('Please enter the value for Is your organisation incorporated')
        flag = 1
    }

    var charity = nlapiGetFieldValue('custrecord_ga_org_register_charity')
    //alert('charity'+charity)
    if (charity == '' || charity == null) {
        alert('Please enter the value for Your organisation a registered charity?')
        flag = 1
    }

    var BankAccount_Name = nlapiGetFieldValue('custrecord_ga_bank_account_name')
    //alert('BankAccount_Name'+BankAccount_Name)
    if (BankAccount_Name == '' || BankAccount_Name == null) {
        alert('Please enter the value for Bank Account Name')
        flag = 1
    }

    var Bank = nlapiGetFieldValue('custrecord_ga_bank')
    //alert('Bank'+Bank)
    if (Bank == '' || Bank == null) {
        alert('Please enter the value for Bank ')
        flag = 1
    }

    var Branch = nlapiGetFieldValue('custrecord_ga_branch')
    //alert('Branch'+Branch)
    if (Branch == '' || Branch == null) {
        alert('Please enter the value for Branch ')
        flag = 1
    }

    var Account = nlapiGetFieldValue('custrecord_ga_account')
    //alert('Account'+Account)
    if (Account == '' || Account == null) {
        alert('Please enter the value for Account ')
        flag = 1
    }

    var Suffix = nlapiGetFieldValue('custrecord_ga_suffix')
    //alert('Suffix'+Suffix)
    if (Suffix == '' || Suffix == null) {
        alert('Please enter the value for Suffix ')
        flag = 1
    }

    if (flag == 0) {
        //nlapiSetFieldValue('custrecord_ga_organisation_status',2,true,true)
        try {

       //     var recId = nlapiGetRecordId();
        //    var recType = nlapiGetRecordType();
          //  alert('recId'+recId);
           // alert('recType'+recType);
        //    var recObj = nlapiLoadRecord(recType, recId);
      //      var currentStatus = recObj.getFieldValue('custrecord_ga_organisation_status');
		//	alert(currentStatus);

			var currentStatus = nlapiGetFieldValue('custrecord_ga_organisation_status');

            if (currentStatus == 8) {
                nlapiSetFieldValue('custrecord_ga_organisation_status', 2);
             //  recObj.setFieldValue('custrecord_ga_organisation_status', 2);
            }
			//var ohapproved = nlapiGetFieldValue('custrecord_ga_gm_approval_completed');


		//	if(currentStatus == 2){
		//		 nlapiSetFieldValue('custrecord_ga_organisation_status', 3, true, true)
		//	}



			// Jason uncommented this because we'll create a user field changed UES script.


			/*
			else if (currentStatus == 3) {
                nlapiSetFieldValue('custrecord_ga_organisation_status', 4, true, true)
                recObj.setFieldValue('custrecord_ga_organisation_status', 4)
            }
			*/


          //  var id = nlapiSubmitRecord(recObj, true, true)

			//alert(id)
            location.reload()
        }
        catch (e) {

        }
    }
    else {
        return false
    }



    return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum) {

    /*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES



    //  VALIDATE FIELD CODE BODY


    return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function org_fieldChanged(type, name, linenum) {
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */
	var organisationStatus = nlapiGetFieldValue('custrecord_ga_organisation_status');

	// Issue here was that when you hit save as draft, it the organisation status should stay as draft but it changes to pending certified.

	/*
	     if (organisationStatus == 8) {
                nlapiSetFieldValue('custrecord_ga_organisation_status', 2, true, true)
              //  recObj.setFieldValue('custrecord_ga_organisation_status', 2)
         }
		var ohapproved = nlapiGetFieldValue('custrecord_ga_gm_approval_completed');


		if(organisationStatus == 2 && ohapproved =='T'){
				 nlapiSetFieldValue('custrecord_ga_organisation_status', 3, true, true)
		}

	*/











    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

    //alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
    if (type == 'recmachcustrecord_fs_oh_organisation' && (name == 'custrecord_fs_portal_oh_first_name' || name == 'custrecord_fs_portal_oh_last_name')) {
        var firstName = nlapiGetCurrentLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_first_name')
        var lastNAme = nlapiGetCurrentLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_last_name')
        if (firstName == null) {
            firstName = ''
        }
        if (lastNAme == null) {
            lastNAme = ''
        }
        var fullName = firstName + ' ' + lastNAme
        nlapiSetCurrentLineItemValue('recmachcustrecord_fs_oh_organisation', 'name', fullName)
    }

	/*
		Organisation field changes
		Sets the field to pending confirmation and the status to pending re certification
	*/
	if(opertaionType=='edit' && (organisationStatus=='3' || organisationStatus=='4')){



		if(name=='custrecord_ga_organisation_type'){

			organisationfieldHasChanged = true;
			nlapiSetFieldValue('custrecord_ga_grants_manager_1',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_coverage'){
			organisationfieldHasChanged = true;
			nlapiSetFieldValue('custrecord_ga_grants_manager_1',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_phone_number'){
			organisationfieldHasChanged = true;
			nlapiSetFieldValue('custrecord_ga_grants_manager_1',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_web_site'){
			organisationfieldHasChanged = true;
			nlapiSetFieldValue('custrecord_ga_grants_manager_1',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

	/*
		Physical Address changes
		Sets the field to pending confirmation and the status to pending re certification
	*/


		else if(name=='custrecord_ga_street_address'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_2',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_address_line_2'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_2',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_town_city'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_2',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_state_province'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_2',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_postal_code'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_2',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

	/*
		Postal Address changes
		Sets the field to pending confirmation and the status to pending re certification
	*/


		else if(name=='custrecord_ga_street_address_postal_chec'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_same_as_physical_address'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_post_box'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_street_address_postal_addr'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_address_line_2_postal_addr'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_town_city_postal_address'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_postal_code_postal_address'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_post_box_number'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_address_line_2_pos_box'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_town_city_post_box'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_postal_code_post_box'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_3',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

	/*
		Additional information changes
		Sets the field to pending confirmation and the status to pending re certification
	*/
		else if(name=='custrecord_ga_org_register_gst'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_org_incorporated'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_org_register_charity'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_gst_number'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_incorporation_number'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_charities_number'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_4',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

	/*
		Bank information changes
		Sets the field to pending confirmation and the status to pending re certification
	*/
		else if(name=='custrecord_ga_bank_account_name'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_5',2);
			nlapiSetFieldValue('custrecord_finance_manager',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_bank'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_5',2);
			nlapiSetFieldValue('custrecord_finance_manager',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_branch'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_5',2);
			nlapiSetFieldValue('custrecord_finance_manager',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}

		else if(name=='custrecord_ga_account'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_5',2);
			nlapiSetFieldValue('custrecord_finance_manager',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}
		else if(name=='custrecord_ga_suffix'){
			nlapiSetFieldValue('custrecord_ga_grant_manager_5',2);
			nlapiSetFieldValue('custrecord_finance_manager',3);
			nlapiSetFieldValue('custrecord_ga_gm_approval_completed','F');
			nlapiSetFieldValue('custrecord_ga_grant_manager_approval',1);
			nlapiSetFieldValue('custrecord_ga_organisation_status',4);
		}






	}
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name) {

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type) {

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type) {

    /*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  VALIDATE LINE CODE BODY


    return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type) {

    /*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function callSubmit() {




    //alert('opertaionType-->'+opertaionType)
    var recType = nlapiGetRecordType()
    if (opertaionType == 'create') {
        //alert('in create')
        var ga_organisation_legal_name = nlapiGetFieldValue('custrecord_ga_organisation_legal_name')
        var ga_organisation_type = nlapiGetFieldValue('custrecord_ga_organisation_type')
        var ga_coverage = nlapiGetFieldValue('custrecord_ga_coverage')
        var ga_phone_number = nlapiGetFieldValue('custrecord_ga_phone_number')
        var ga_web_site = nlapiGetFieldValue('custrecord_ga_web_site')
        var ga_description = nlapiGetFieldValue('custrecord_ga_description')
        var add_text = nlapiGetFieldValue('custrecord_add_text')
        var remainingcharacter = nlapiGetFieldValue('custrecord_remainingcharacter')
        var ga_street_address = nlapiGetFieldValue('custrecord_ga_street_address')
        var ga_address_line_2 = nlapiGetFieldValue('custrecord_ga_address_line_2')
        var ga_town_city = nlapiGetFieldValue('custrecord_ga_town_city')
        var ga_state_province = nlapiGetFieldValue('custrecord_ga_state_province')
        var ga_postal_code = nlapiGetFieldValue('custrecord_ga_postal_code')
        var ga_street_address_postal_chec = nlapiGetFieldValue('custrecord_ga_street_address_postal_chec')
        var ga_same_as_physical_address = nlapiGetFieldValue('custrecord_ga_same_as_physical_address')
        var ga_post_box = nlapiGetFieldValue('custrecord_ga_post_box')
        var ga_street_address_postal_addr = nlapiGetFieldValue('custrecord_ga_street_address_postal_addr')
        var ga_address_line_2_postal_addr = nlapiGetFieldValue('custrecord_ga_address_line_2_postal_addr')
        var ga_town_city_postal_address = nlapiGetFieldValue('custrecord_ga_town_city_postal_address')
        var ga_state_province_postal_addr = nlapiGetFieldValue('custrecord_ga_state_province_postal_addr')
        var ga_postal_code_postal_address = nlapiGetFieldValue('custrecord_ga_postal_code_postal_address')
        var ga_post_box_number = nlapiGetFieldValue('custrecord_ga_post_box_number')
        var ga_address_line_2_pos_box = nlapiGetFieldValue('custrecord_ga_address_line_2_pos_box')
        var ga_town_city_post_box = nlapiGetFieldValue('custrecord_ga_town_city_post_box')
        var ga_postal_code_post_box = nlapiGetFieldValue('custrecord_ga_postal_code_post_box')
        var ga_nz_post_code_website = nlapiGetFieldValue('custrecord_ga_nz_post_code_website')
        var ga_org_register_gst = nlapiGetFieldValue('custrecord_ga_org_register_gst')
        var ga_gst_number = nlapiGetFieldValue('custrecord_ga_gst_number')
        var ga_org_incorporated = nlapiGetFieldValue('custrecord_ga_org_incorporated')
        var ga_incorporation_number = nlapiGetFieldValue('custrecord_ga_incorporation_number')
        var ga_org_register_charity = nlapiGetFieldValue('custrecord_ga_org_register_charity')
        var ga_charities_number = nlapiGetFieldValue('custrecord_ga_charities_number')
        var ga_bank_account_name = nlapiGetFieldValue('custrecord_ga_bank_account_name')
        var ga_bank = nlapiGetFieldValue('custrecord_ga_bank')
        var ga_branch = nlapiGetFieldValue('custrecord_ga_branch')
        var ga_account = nlapiGetFieldValue('custrecord_ga_account')
        var ga_suffix = nlapiGetFieldValue('custrecord_ga_suffix')
        var ga_organisation_status = nlapiGetFieldValue('custrecord_ga_organisation_status')
        var ga_grants_manager_1 = nlapiGetFieldValue('custrecord_ga_grants_manager_1')
        var ga_grant_manager_comments_1 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_1')
        var ga_grant_manager_2 = nlapiGetFieldValue('custrecord_ga_grant_manager_2')
        var ga_grant_manager_comments_2 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_2')
        var ga_grant_manager_3 = nlapiGetFieldValue('custrecord_ga_grant_manager_3')
        var ga_grant_manager_comments_3 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_3')
        var ga_grant_manager_4 = nlapiGetFieldValue('custrecord_ga_grant_manager_4')
        var ga_grant_manager_comments_4 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_4')
        var ga_grant_manager_5 = nlapiGetFieldValue('custrecord_ga_grant_manager_5')
        var ga_grant_manager_comments_5 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_5')
        var ga_organisation_cert_reg_char = nlapiGetFieldValue('custrecord_ga_organisation_cert_reg_char')
        var ga_organisation_cert_incorpor = nlapiGetFieldValue('custrecord_ga_organisation_cert_incorpor')
        var ga_organisation_meeting_mins = nlapiGetFieldValue('custrecord_ga_organisation_meeting_mins')
        var a_organisation_bank_deposit = nlapiGetFieldValue('custrecordga_organisation_bank_deposit')
        var ga_grant_manager_comments = nlapiGetFieldValue('custrecord_ga_grant_manager_comments')
        var ga_certified_date = nlapiGetFieldValue('custrecord_ga_certified_date')
        var ga_grant_manager_approver = nlapiGetFieldValue('custrecord_ga_grant_manager_approver')
        var org_email_contact_1 = nlapiGetFieldValue('custrecord_org_email_contact_1')
        var org_email_contact_2 = nlapiGetFieldValue('custrecord_org_email_contact_2')
        var org_email_contact_3 = nlapiGetFieldValue('custrecord_org_email_contact_3')
        var name = nlapiGetFieldValue('name')

        var newRecord = nlapiCreateRecord(recType)
        newRecord.setFieldValue('name', name)
        newRecord.setFieldValue('custrecord_ga_organisation_legal_name', ga_organisation_legal_name)
        newRecord.setFieldValue('custrecord_ga_organisation_type', ga_organisation_type)
        newRecord.setFieldValue('custrecord_ga_coverage', ga_coverage)
        newRecord.setFieldValue('custrecord_ga_phone_number', ga_phone_number)
        newRecord.setFieldValue('custrecord_ga_web_site', ga_web_site)
        newRecord.setFieldValue('custrecord_ga_description', ga_description)
        newRecord.setFieldValue('custrecord_add_text', add_text)
        newRecord.setFieldValue('custrecord_remainingcharacter', remainingcharacter)
        newRecord.setFieldValue('custrecord_ga_street_address', ga_street_address)
        newRecord.setFieldValue('custrecord_ga_address_line_2', ga_address_line_2)
        newRecord.setFieldValue('custrecord_ga_town_city', ga_town_city)
        newRecord.setFieldValue('custrecord_ga_state_province', ga_state_province)
        newRecord.setFieldValue('custrecord_ga_postal_code', ga_postal_code)
        newRecord.setFieldValue('custrecord_ga_street_address_postal_chec', ga_street_address_postal_chec)
        newRecord.setFieldValue('custrecord_ga_same_as_physical_address', ga_same_as_physical_address)
        newRecord.setFieldValue('custrecord_ga_post_box', ga_post_box)
        newRecord.setFieldValue('custrecord_ga_street_address_postal_addr', ga_street_address_postal_addr)
        newRecord.setFieldValue('custrecord_ga_address_line_2_postal_addr', ga_address_line_2_postal_addr)
        newRecord.setFieldValue('custrecord_ga_town_city_postal_address', ga_town_city_postal_address)
        newRecord.setFieldValue('custrecord_ga_state_province_postal_addr', ga_state_province_postal_addr)
        newRecord.setFieldValue('custrecord_ga_postal_code_postal_address', ga_postal_code_postal_address)
        newRecord.setFieldValue('custrecord_ga_post_box_number', ga_post_box_number)
        newRecord.setFieldValue('custrecord_ga_address_line_2_pos_box', ga_address_line_2_pos_box)
        newRecord.setFieldValue('custrecord_ga_town_city_post_box', ga_town_city_post_box)
        newRecord.setFieldValue('custrecord_ga_postal_code_post_box', ga_postal_code_post_box)
        newRecord.setFieldValue('custrecord_ga_nz_post_code_website', ga_nz_post_code_website)
        newRecord.setFieldValue('custrecord_ga_org_register_gst', ga_org_register_gst)
        newRecord.setFieldValue('custrecord_ga_gst_number', ga_gst_number)
        newRecord.setFieldValue('custrecord_ga_org_incorporated', ga_org_incorporated)
        newRecord.setFieldValue('custrecord_ga_incorporation_number', ga_incorporation_number)
        newRecord.setFieldValue('custrecord_ga_org_register_charity', ga_org_register_charity)
        newRecord.setFieldValue('custrecord_ga_charities_number', ga_charities_number)
        newRecord.setFieldValue('custrecord_ga_bank_account_name', ga_bank_account_name)
        newRecord.setFieldValue('custrecord_ga_bank', ga_bank)
        newRecord.setFieldValue('custrecord_ga_branch', ga_branch)
        newRecord.setFieldValue('custrecord_ga_account', ga_account)
        newRecord.setFieldValue('custrecord_ga_suffix', ga_suffix)
        newRecord.setFieldValue('custrecord_ga_organisation_status', ga_organisation_status)
        newRecord.setFieldValue('custrecord_ga_grants_manager_1', ga_grants_manager_1)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_1', ga_grant_manager_comments_1)
        newRecord.setFieldValue('custrecord_ga_grant_manager_2', ga_grant_manager_2)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_2', ga_grant_manager_comments_2)
        newRecord.setFieldValue('custrecord_ga_grant_manager_3', ga_grant_manager_3)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_3', ga_grant_manager_comments_3)
        newRecord.setFieldValue('custrecord_ga_grant_manager_4', ga_grant_manager_4)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_4', ga_grant_manager_comments_4)
        newRecord.setFieldValue('custrecord_ga_grant_manager_5', ga_grant_manager_5)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_5', ga_grant_manager_comments_5)
        newRecord.setFieldValue('custrecord_ga_organisation_cert_reg_char', ga_organisation_cert_reg_char)
        newRecord.setFieldValue('custrecord_ga_organisation_cert_incorpor', ga_organisation_cert_incorpor)
        newRecord.setFieldValue('custrecord_ga_organisation_meeting_mins', ga_organisation_meeting_mins)
        newRecord.setFieldValue('custrecordga_organisation_bank_deposit', a_organisation_bank_deposit)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments', ga_grant_manager_comments)
        newRecord.setFieldValue('custrecord_ga_certified_date', ga_certified_date)
        newRecord.setFieldValue('custrecord_ga_grant_manager_approver', ga_grant_manager_approver)
        newRecord.setFieldValue('custrecord_org_email_contact_1', org_email_contact_1)
        newRecord.setFieldValue('custrecord_org_email_contact_2', org_email_contact_2)
        newRecord.setFieldValue('custrecord_org_email_contact_3', org_email_contact_3)
        var id = nlapiSubmitRecord(newRecord, true, true)




        //alert('createdid->'+id)
    }
    if (opertaionType == 'edit') {
        //alert('In edit')
        var ga_organisation_legal_name = nlapiGetFieldValue('custrecord_ga_organisation_legal_name')
        var ga_organisation_type = nlapiGetFieldValue('custrecord_ga_organisation_type')
        var ga_coverage = nlapiGetFieldValue('custrecord_ga_coverage')
        var ga_phone_number = nlapiGetFieldValue('custrecord_ga_phone_number')
        var ga_web_site = nlapiGetFieldValue('custrecord_ga_web_site')
        var ga_description = nlapiGetFieldValue('custrecord_ga_description')
        var add_text = nlapiGetFieldValue('custrecord_add_text')
        var remainingcharacter = nlapiGetFieldValue('custrecord_remainingcharacter')
        var ga_street_address = nlapiGetFieldValue('custrecord_ga_street_address')
        var ga_address_line_2 = nlapiGetFieldValue('custrecord_ga_address_line_2')
        var ga_town_city = nlapiGetFieldValue('custrecord_ga_town_city')
        var ga_state_province = nlapiGetFieldValue('custrecord_ga_state_province')
        var ga_postal_code = nlapiGetFieldValue('custrecord_ga_postal_code')
        var ga_street_address_postal_chec = nlapiGetFieldValue('custrecord_ga_street_address_postal_chec')
        var ga_same_as_physical_address = nlapiGetFieldValue('custrecord_ga_same_as_physical_address')
        var ga_post_box = nlapiGetFieldValue('custrecord_ga_post_box')
        var ga_street_address_postal_addr = nlapiGetFieldValue('custrecord_ga_street_address_postal_addr')
        var ga_address_line_2_postal_addr = nlapiGetFieldValue('custrecord_ga_address_line_2_postal_addr')
        var ga_town_city_postal_address = nlapiGetFieldValue('custrecord_ga_town_city_postal_address')
        var ga_state_province_postal_addr = nlapiGetFieldValue('custrecord_ga_state_province_postal_addr')
        var ga_postal_code_postal_address = nlapiGetFieldValue('custrecord_ga_postal_code_postal_address')
        var ga_post_box_number = nlapiGetFieldValue('custrecord_ga_post_box_number')
        var ga_address_line_2_pos_box = nlapiGetFieldValue('custrecord_ga_address_line_2_pos_box')
        var ga_town_city_post_box = nlapiGetFieldValue('custrecord_ga_town_city_post_box')
        var ga_postal_code_post_box = nlapiGetFieldValue('custrecord_ga_postal_code_post_box')
        var ga_nz_post_code_website = nlapiGetFieldValue('custrecord_ga_nz_post_code_website')
        var ga_org_register_gst = nlapiGetFieldValue('custrecord_ga_org_register_gst')
        var ga_gst_number = nlapiGetFieldValue('custrecord_ga_gst_number')
        var ga_org_incorporated = nlapiGetFieldValue('custrecord_ga_org_incorporated')
        var ga_incorporation_number = nlapiGetFieldValue('custrecord_ga_incorporation_number')
        var ga_org_register_charity = nlapiGetFieldValue('custrecord_ga_org_register_charity')
        var ga_charities_number = nlapiGetFieldValue('custrecord_ga_charities_number')
        var ga_bank_account_name = nlapiGetFieldValue('custrecord_ga_bank_account_name')
        var ga_bank = nlapiGetFieldValue('custrecord_ga_bank')
        var ga_branch = nlapiGetFieldValue('custrecord_ga_branch')
        var ga_account = nlapiGetFieldValue('custrecord_ga_account')
        var ga_suffix = nlapiGetFieldValue('custrecord_ga_suffix')
        var ga_organisation_status = nlapiGetFieldValue('custrecord_ga_organisation_status')
        var ga_grants_manager_1 = nlapiGetFieldValue('custrecord_ga_grants_manager_1')
        var ga_grant_manager_comments_1 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_1')
        var ga_grant_manager_2 = nlapiGetFieldValue('custrecord_ga_grant_manager_2')
        var ga_grant_manager_comments_2 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_2')
        var ga_grant_manager_3 = nlapiGetFieldValue('custrecord_ga_grant_manager_3')
        var ga_grant_manager_comments_3 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_3')
        var ga_grant_manager_4 = nlapiGetFieldValue('custrecord_ga_grant_manager_4')
        var ga_grant_manager_comments_4 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_4')
        var ga_grant_manager_5 = nlapiGetFieldValue('custrecord_ga_grant_manager_5')
        var ga_grant_manager_comments_5 = nlapiGetFieldValue('custrecord_ga_grant_manager_comments_5')
        var ga_organisation_cert_reg_char = nlapiGetFieldValue('custrecord_ga_organisation_cert_reg_char')
        var ga_organisation_cert_incorpor = nlapiGetFieldValue('custrecord_ga_organisation_cert_incorpor')
        var ga_organisation_meeting_mins = nlapiGetFieldValue('custrecord_ga_organisation_meeting_mins')
        var a_organisation_bank_deposit = nlapiGetFieldValue('custrecordga_organisation_bank_deposit')
        var ga_grant_manager_comments = nlapiGetFieldValue('custrecord_ga_grant_manager_comments')
        var ga_certified_date = nlapiGetFieldValue('custrecord_ga_certified_date')
        var ga_grant_manager_approver = nlapiGetFieldValue('custrecord_ga_grant_manager_approver')
        var org_email_contact_1 = nlapiGetFieldValue('custrecord_org_email_contact_1')
        var org_email_contact_2 = nlapiGetFieldValue('custrecord_org_email_contact_2')
        var org_email_contact_3 = nlapiGetFieldValue('custrecord_org_email_contact_3')

        var newRecord = nlapiLoadRecord(recType, nlapiGetRecordId())
        newRecord.setFieldValue('custrecord_ga_organisation_legal_name', ga_organisation_legal_name)
        newRecord.setFieldValue('custrecord_ga_organisation_type', ga_organisation_type)
        newRecord.setFieldValue('custrecord_ga_coverage', ga_coverage)
        newRecord.setFieldValue('custrecord_ga_phone_number', ga_phone_number)
        newRecord.setFieldValue('custrecord_ga_web_site', ga_web_site)
        newRecord.setFieldValue('custrecord_ga_description', ga_description)
        newRecord.setFieldValue('custrecord_add_text', add_text)
        newRecord.setFieldValue('custrecord_remainingcharacter', remainingcharacter)
        newRecord.setFieldValue('custrecord_ga_street_address', ga_street_address)
        newRecord.setFieldValue('custrecord_ga_address_line_2', ga_address_line_2)
        newRecord.setFieldValue('custrecord_ga_town_city', ga_town_city)
        newRecord.setFieldValue('custrecord_ga_state_province', ga_state_province)
        newRecord.setFieldValue('custrecord_ga_postal_code', ga_postal_code)
        newRecord.setFieldValue('custrecord_ga_street_address_postal_chec', ga_street_address_postal_chec)
        newRecord.setFieldValue('custrecord_ga_same_as_physical_address', ga_same_as_physical_address)
        newRecord.setFieldValue('custrecord_ga_post_box', ga_post_box)
        newRecord.setFieldValue('custrecord_ga_street_address_postal_addr', ga_street_address_postal_addr)
        newRecord.setFieldValue('custrecord_ga_address_line_2_postal_addr', ga_address_line_2_postal_addr)
        newRecord.setFieldValue('custrecord_ga_town_city_postal_address', ga_town_city_postal_address)
        newRecord.setFieldValue('custrecord_ga_state_province_postal_addr', ga_state_province_postal_addr)
        newRecord.setFieldValue('custrecord_ga_postal_code_postal_address', ga_postal_code_postal_address)
        newRecord.setFieldValue('custrecord_ga_post_box_number', ga_post_box_number)
        newRecord.setFieldValue('custrecord_ga_address_line_2_pos_box', ga_address_line_2_pos_box)
        newRecord.setFieldValue('custrecord_ga_town_city_post_box', ga_town_city_post_box)
        newRecord.setFieldValue('custrecord_ga_postal_code_post_box', ga_postal_code_post_box)
        newRecord.setFieldValue('custrecord_ga_nz_post_code_website', ga_nz_post_code_website)
        newRecord.setFieldValue('custrecord_ga_org_register_gst', ga_org_register_gst)
        newRecord.setFieldValue('custrecord_ga_gst_number', ga_gst_number)
        newRecord.setFieldValue('custrecord_ga_org_incorporated', ga_org_incorporated)
        newRecord.setFieldValue('custrecord_ga_incorporation_number', ga_incorporation_number)
        newRecord.setFieldValue('custrecord_ga_org_register_charity', ga_org_register_charity)
        newRecord.setFieldValue('custrecord_ga_charities_number', ga_charities_number)
        newRecord.setFieldValue('custrecord_ga_bank_account_name', ga_bank_account_name)
        newRecord.setFieldValue('custrecord_ga_bank', ga_bank)
        newRecord.setFieldValue('custrecord_ga_branch', ga_branch)
        newRecord.setFieldValue('custrecord_ga_account', ga_account)
        newRecord.setFieldValue('custrecord_ga_suffix', ga_suffix)
        newRecord.setFieldValue('custrecord_ga_organisation_status', ga_organisation_status)
        newRecord.setFieldValue('custrecord_ga_grants_manager_1', ga_grants_manager_1)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_1', ga_grant_manager_comments_1)
        newRecord.setFieldValue('custrecord_ga_grant_manager_2', ga_grant_manager_2)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_2', ga_grant_manager_comments_2)
        newRecord.setFieldValue('custrecord_ga_grant_manager_3', ga_grant_manager_3)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_3', ga_grant_manager_comments_3)
        newRecord.setFieldValue('custrecord_ga_grant_manager_4', ga_grant_manager_4)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_4', ga_grant_manager_comments_4)
        newRecord.setFieldValue('custrecord_ga_grant_manager_5', ga_grant_manager_5)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments_5', ga_grant_manager_comments_5)
        newRecord.setFieldValue('custrecord_ga_organisation_cert_reg_char', ga_organisation_cert_reg_char)
        newRecord.setFieldValue('custrecord_ga_organisation_cert_incorpor', ga_organisation_cert_incorpor)
        newRecord.setFieldValue('custrecord_ga_organisation_meeting_mins', ga_organisation_meeting_mins)
        newRecord.setFieldValue('custrecordga_organisation_bank_deposit', a_organisation_bank_deposit)
        newRecord.setFieldValue('custrecord_ga_grant_manager_comments', ga_grant_manager_comments)
        newRecord.setFieldValue('custrecord_ga_certified_date', ga_certified_date)
        newRecord.setFieldValue('custrecord_ga_grant_manager_approver', ga_grant_manager_approver)
        newRecord.setFieldValue('custrecord_org_email_contact_1', org_email_contact_1)
        newRecord.setFieldValue('custrecord_org_email_contact_2', org_email_contact_2)
        newRecord.setFieldValue('custrecord_org_email_contact_3', org_email_contact_3)


        var orgstatus = nlapiGetFieldValue('custrecord_ga_organisation_status')
        //alert('orgstatus->'+orgstatus)
        /*if (orgstatus == 3) {
            newRecord.setFieldValue('custrecord_ga_organisation_status', 4)
        }*/
        var id = nlapiSubmitRecord(newRecord, true, true)
        //alert('updatedid->'+id)
        //-----------------------------------------------------------------------------------------------------------------------------------------------
        var lineCount = nlapiGetLineItemCount('recmachcustrecord_fs_oh_organisation')
        for (var i = 1; i <= lineCount; i++) {
            //var a=nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation','',i)
            var oh_id = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'id', i)
            //alert(id)
            var portal_oh_primary_office = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord__fs_portal_oh_primary_office', i)
            //alert('portal_oh_primary_office->'+portal_oh_primary_office)
            var portal_oh_name = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'name', i)
            var portal_oh_first_name = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_first_name', i)
            var portal_oh_preferred_name = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_preferred_name', i)
            var portal_oh_last_name = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_last_name', i)
            var portal_oh_role_in_org = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_role_in_org', i)
            var portal_oh_email = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_email', i)
            var portal_oh_login_password = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_login_password', i)
            var portal_oh_phone_number = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_phone_number', i)
            var portal_oh_authorised_funds = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_authorised_funds', i)
            var portal_oh_bank_signatory = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_bank_signatory', i)
            var portal_oh_address_line_1 = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_address_line_1', i)
            var portal_oh_address_line_2 = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_address_line_2', i)
            var portal_oh_suburb = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_suburb', i)
            var portal_oh_town_city = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_town_city', i)
            var portal_oh_postal_code = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_postal_code', i)
            var portal_oh_office_h_status = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_office_h_status', i)
            var oh_organisation = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_oh_organisation', i)
            var oh_organisation = oh_id
            //var office_holder_linked_contact = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_office_holder_linked_contact', i)
            var portal_oh_grant_manager_app = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecordfs_portal_oh_grant_manager_app', i)
            var portal_oh_gm_comments = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_gm_comments', i)
            var portal_oh_certified_date = nlapiGetLineItemValue('recmachcustrecord_fs_oh_organisation', 'custrecord_fs_portal_oh_certified_date', i)
            if (oh_id != null && oh_id != '') {
                var childRec = nlapiLoadRecord('customrecord_fs_portal_office_holders', oh_id)
                childRec.setFieldValue('custrecord__fs_portal_oh_primary_office', portal_oh_primary_office)
                childRec.setFieldValue('name', portal_oh_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_first_name', portal_oh_first_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_preferred_name', portal_oh_preferred_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_last_name', portal_oh_last_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_role_in_org', portal_oh_role_in_org)
                childRec.setFieldValue('custrecord_fs_portal_oh_email', portal_oh_email)
                childRec.setFieldValue('custrecord_fs_portal_oh_login_password', portal_oh_login_password)
                childRec.setFieldValue('custrecord_fs_portal_oh_phone_number', portal_oh_phone_number)
                childRec.setFieldValue('custrecord_fs_portal_oh_authorised_funds', portal_oh_authorised_funds)
                childRec.setFieldValue('custrecord_fs_portal_oh_bank_signatory', portal_oh_bank_signatory)
                childRec.setFieldValue('custrecord_fs_portal_oh_address_line_1', portal_oh_address_line_1)
                childRec.setFieldValue('custrecord_fs_portal_oh_address_line_2', portal_oh_address_line_2)
                childRec.setFieldValue('custrecord_fs_portal_oh_suburb', portal_oh_suburb)
                childRec.setFieldValue('custrecord_fs_portal_oh_town_city', portal_oh_town_city)
                childRec.setFieldValue('custrecord_fs_portal_oh_postal_code', portal_oh_postal_code)
                childRec.setFieldValue('custrecord_fs_portal_oh_office_h_status', portal_oh_office_h_status)
                //childRec.setFieldValue('custrecord_fs_oh_organisation',oh_organisation)
                //childRec.setFieldValue('custrecord_office_holder_linked_contact', office_holder_linked_contact)
                childRec.setFieldValue('custrecordfs_portal_oh_grant_manager_app', portal_oh_grant_manager_app)
                childRec.setFieldValue('custrecord_fs_portal_oh_gm_comments', portal_oh_gm_comments)
                childRec.setFieldValue('custrecord_fs_portal_oh_certified_date', portal_oh_certified_date)
                var childId = nlapiSubmitRecord(childRec, true, true)
                //alert(childId)


            }
            else {
                var oh_organisation = nlapiGetRecordId()
                var childRec = nlapiCreateRecord('customrecord_fs_portal_office_holders')
                childRec.setFieldValue('custrecord__fs_portal_oh_primary_office', portal_oh_primary_office)
                childRec.setFieldValue('name', portal_oh_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_first_name', portal_oh_first_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_preferred_name', portal_oh_preferred_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_last_name', portal_oh_last_name)
                childRec.setFieldValue('custrecord_fs_portal_oh_role_in_org', portal_oh_role_in_org)
                childRec.setFieldValue('custrecord_fs_portal_oh_email', portal_oh_email)
                childRec.setFieldValue('custrecord_fs_portal_oh_login_password', portal_oh_login_password)
                childRec.setFieldValue('custrecord_fs_portal_oh_phone_number', portal_oh_phone_number)
                childRec.setFieldValue('custrecord_fs_portal_oh_authorised_funds', portal_oh_authorised_funds)
                childRec.setFieldValue('custrecord_fs_portal_oh_bank_signatory', portal_oh_bank_signatory)
                childRec.setFieldValue('custrecord_fs_portal_oh_address_line_1', portal_oh_address_line_1)
                childRec.setFieldValue('custrecord_fs_portal_oh_address_line_2', portal_oh_address_line_2)
                childRec.setFieldValue('custrecord_fs_portal_oh_suburb', portal_oh_suburb)
                childRec.setFieldValue('custrecord_fs_portal_oh_town_city', portal_oh_town_city)
                childRec.setFieldValue('custrecord_fs_portal_oh_postal_code', portal_oh_postal_code)
                childRec.setFieldValue('custrecord_fs_portal_oh_office_h_status', portal_oh_office_h_status)
                childRec.setFieldValue('custrecord_fs_oh_organisation', oh_organisation)
                //childRec.setFieldValue('custrecord_office_holder_linked_contact', office_holder_linked_contact)
                childRec.setFieldValue('custrecordfs_portal_oh_grant_manager_app', portal_oh_grant_manager_app)
                childRec.setFieldValue('custrecord_fs_portal_oh_gm_comments', portal_oh_gm_comments)
                childRec.setFieldValue('custrecord_fs_portal_oh_certified_date', portal_oh_certified_date)
                var newchildid = nlapiSubmitRecord(childRec, true, true)
                //alert(newchildid)
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------

    }

    window.open('/app/common/custom/custrecordentry.nl?rectype=5&id='+id+'&e=T', '_self')
}


// END FUNCTION =====================================================