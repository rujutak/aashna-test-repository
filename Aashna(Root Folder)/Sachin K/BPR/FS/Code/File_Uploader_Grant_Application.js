function upload(request, response) {

    if (request.getMethod() == 'GET') {
        /* create a form */
        var form = nlapiCreateForm('Upload your file and attach to your Grant Application');

        /* build the fields */
        var applicationField = form.addField('custrecord_application', 'select', 'Select your application', null);
        applicationField.setMandatory(true);
        setList(applicationField);

        var fileField = form.addField('custrecord_file', 'file', 'Select File');
        fileField.setLayoutType('outsidebelow', 'startrow');
        fileField.setMandatory(true);

        /* add the "Upload" button */
        form.addSubmitButton('Upload');

        /* write the form */
        response.writePage(form);
    }
    else {

        var application = request.getParameter('custrecord_application');
        var file = request.getFile('custrecord_file');

        /* rename the file */
        file.setName(file.getName());

        /* Grant Application folder id: 179 */
        var folder_id = 179;

        /* set the folder where this file will be added.*/
        file.setFolder(folder_id)

        /* create and upload the file on the file cabinet using the nlapiSubmitFile */
        var id = nlapiSubmitFile(file)

        /* now attach file to Grant Application record */
        nlapiAttachRecord('file', id, 'customrecord_fs_portal_online_grant_app', application)

        /* now write the successful message */
        response.write("Thank You. Your file is uploaded and attached to your Grant Application.");
    }
}

// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
    function setList(Field) {
        var column = new Array();

        column[0] = new nlobjSearchColumn('internalid')
        column[1] = new nlobjSearchColumn('name')
        var searchResults = nlapiSearchRecord("customrecord_fs_portal_online_grant_app", null, null, column);
        
        Field.addSelectOption('-1', '', true);
        if (searchResults != null && searchResults != '') {
            for (i = 0; i < searchResults.length; i++) {
                Field.addSelectOption(searchResults[i].getValue('internalid'),
                                        searchResults[i].getValue('name'), false);
            }
        }
    }

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================