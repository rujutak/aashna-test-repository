// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
        Script Name:CLI_Grant_Application.js
        Author:
        Company:
        Date:
        Description:


        Script Modification Log:

        -- Date --			-- Modified By --				--Requested By--				-- Description --




        Below is a summary of the process controls enforced by this script file.  The control logic is described
        more fully, below, in the appropriate function headers and code blocks.

         PAGE INIT
            - pageInit(type)


         SAVE RECORD
            - saveRecord()


         VALIDATE FIELD
            - validateField(type, name, linenum)


         FIELD CHANGED
            - fieldChanged(type, name, linenum)


         POST SOURCING
            - postSourcing(type, name)


        LINE INIT
            - lineInit(type)


         VALIDATE LINE
            - validateLine()


         RECALC
            - reCalc()


         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:





    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...

    var opertaionType
}
// END GLOBAL VARIABLE BLOCK  =======================================


function hideBtn() {

        var orgstatus = document.getElementById("custrecord_fs_ga_organisation_status").value;
        var submitBtn = document.getElementById("tdbody_submitter");
        var submitBtn2 = document.getElementById("tdbody_secondarysubmitter");

        if (orgstatus != "Certified") {
            submitBtn.style.display = "none";
            submitBtn2.style.display = "none";
        } else {
            submitBtn.style.display = "inline";
            submitBtn2.style.display = "inline";
        }
}


// BEGIN PAGE INIT ==================================================

function app_pageInit(type) {
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
    opertaionType = type

   hideBtn();

}

// END PAGE INIT ====================================================



// BEGIN SAVE RECORD ================================================

function app_saveRecord() {
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY

    if (opertaionType == 'create') {
        nlapiSetFieldValue('custrecord_fs_ga_application_status', 1, true, true)
    }
    //alert('opertaionType->'+opertaionType)




    //--------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------
    var flag = 0
    var Main_Purpose = nlapiGetFieldValue('custrecord_fs_ga_main_purpose')
    //alert('Main_Purpose'+Main_Purpose)
    if (Main_Purpose == '' || Main_Purpose == null) {
        alert('Please enter the value for Main Purpose of Application')
        flag = 1
    }

    var Category = nlapiGetFieldValue('custrecord_fs_ga_grant_category')
    //alert('Category'+Category)
    if (Category == '' || Category == null) {
        alert('Please enter the value for Grant Category')
        flag = 1
    }

    var Region = nlapiGetFieldValue('custrecord_fs_ol_grant_region')
    //alert('Region'+Region)
    if (Region == '' || Region == null) {
        alert('Please enter the value for Region')
        flag = 1
    }


    var When = nlapiGetFieldValue('custrecord_fs_ga_when_do_you_require_fun')
    //alert('When'+When)
    if (When == '' || When == null) {
        alert('Please enter the value for When do you require the funds?')
        flag = 1
    }

    var Full_Amount = nlapiGetFieldValue('custrecord_fs_ga_do_you_require_full_amt')
    //alert('Full_Amount'+Full_Amount)
    if (Full_Amount == '' || Full_Amount == null) {
        alert('Please enter the value for Do you require the full amount?')
        flag = 1
    }

    var Answer = nlapiGetFieldValue('custrecord_fs_ga_answer')
    //alert('Answer'+Answer)
    if (Answer == '' || Answer == null) {
        alert('Please enter the value for Answer')
        flag = 1
    }


    var Signature_1 = nlapiGetFieldValue('custrecord_fs_ga_signature_1')
    //alert('Signature_1'+Signature_1)
    if (Signature_1 == '' || Signature_1 == null) {
        alert('Please enter the value for Signature 1')
        flag = 1
    }

    var Signature_2 = nlapiGetFieldValue('custrecord_fs_ga_signature_2')
    //alert('Signature_2'+Signature_2)
    if (Signature_2 == '' || Signature_2 == null) {
        alert('Please enter the value for Signature 2')
        flag = 1
    }

    var relate_to_event = nlapiGetFieldValue('custrecord_fs_ol_grant_relate_to_event')
    //alert('relate_to_event'+relate_to_event)
    if (relate_to_event == '' || relate_to_event == null) {
        alert('Please enter the value for This grant relate to a specific event?')
        flag = 1
    }

    var checklist_a1 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a1')
    //alert('checklist_a1'+checklist_a1)
    if (checklist_a1 == '' || checklist_a1 == null) {
        alert('Please enter the value for Check List 1')
        flag = 1
    }

    var checklist_a2 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a2')
    //alert('checklist_a2'+checklist_a2)
    if (checklist_a1 == '' || checklist_a1 == null) {
        alert('Please enter the value for Check List 2')
        flag = 1
    }

    var checklist_a3 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a3')
    //alert('checklist_a3'+checklist_a3)
    if (checklist_a3 == '' || checklist_a3 == null) {
        alert('Please enter the value for Check List 3')
        flag = 1
    }


    if (flag == 0) {
        //nlapiSetFieldValue('custrecord_ga_organisation_status',2,true,true)
        try {
            nlapiSetFieldValue('custrecord_fs_ga_application_status', 2, true, true)
            var recId = nlapiGetRecordId()
            var recType = nlapiGetRecordType()
            //alert('recId'+recId)
            //alert('recType'+recType)
            var recObj = nlapiLoadRecord(recType, recId)
            recObj.setFieldValue('custrecord_fs_ga_application_status', 2)
            var id = nlapiSubmitRecord(recObj, true, true)
            //alert(id)
            location.reload()

        }
        catch (e) {

        }
    }
    else {
        return false
    }



    return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum) {

    /*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES



    //  VALIDATE FIELD CODE BODY
    //alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

    return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function app_fieldChanged(type, name, linenum) {
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

if (name == 'custrecord_fs_organisation_link') {

setTimeout(hideBtn,1000);

}

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name) {

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY

}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type) {

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type) {

    /*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  VALIDATE LINE CODE BODY


    return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type) {

    /*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function callSubmit() {
    //alert('opertaionType-->'+opertaionType)
    var recType = nlapiGetRecordType()
    var id=''
    if (opertaionType == 'create') {
        //alert('in create')
        var ga_application_status = nlapiGetFieldValue('custrecord_fs_ga_application_status')
        var ga_title_of_app = nlapiGetFieldValue('custrecord_fs_ga_title_of_app')
        var ga_main_purpose = nlapiGetFieldValue('custrecord_fs_ga_main_purpose')
        var ga_grant_category = nlapiGetFieldValue('custrecord_fs_ga_grant_category')
        var ga_supplier_upload_1 = nlapiGetFieldValue('custrecord_fs_ga_supplier_upload_1')
        var organisation_link = nlapiGetFieldValue('custrecord_fs_organisation_link')
        var ol_grant_region = nlapiGetFieldValue('custrecord_fs_ol_grant_region')
        var ga_when_do_you_require_fun = nlapiGetFieldValue('custrecord_fs_ga_when_do_you_require_fun')
        var ga_do_you_require_full_amt = nlapiGetFieldValue('custrecord_fs_ga_do_you_require_full_amt')
        var ga_benefit_community = nlapiGetFieldValue('custrecord_fs_ga_benefit_community')
        var ga_specify_details_of_othe = nlapiGetFieldValue('custrecord_fs_ga_specify_details_of_othe')
        var ga_organisation_one = nlapiGetFieldValue('custrecord_fs_ga_organisation_one')
        var ga_organisation_two = nlapiGetFieldValue('custrecord_fs_ga_organisation_two')
        var ga_answer = nlapiGetFieldValue('custrecord_fs_ga_answer')
        var ga_gst_number = nlapiGetFieldValue('custrecord_fs_ga_gst_number')
        var ol_grant_relate_to_event = nlapiGetFieldValue('custrecord_fs_ol_grant_relate_to_event')
        var ga_event_name = nlapiGetFieldValue('custrecord_fs_ga_event_name')
        var ga_event_start_date = nlapiGetFieldValue('custrecord_fs_ga_event_start_date')
        var ga_event_end_date = nlapiGetFieldValue('custrecord_fs_ga_event_end_date')
        var ga_signature_1 = nlapiGetFieldValue('custrecord_fs_ga_signature_1')
        var ga_signature_2 = nlapiGetFieldValue('custrecord_fs_ga_signature_2')
        var ga_amount_applied = nlapiGetFieldValue('custrecord_fs_ga_amount_applied')
        var ga_grants_manager_notes = nlapiGetFieldValue('custrecord_fs_ga_grants_manager_notes')
        var ga_grant_manager_approval = nlapiGetFieldValue('custrecord_fs_ga_grant_manager_approval')
        var ga_approved_date = nlapiGetFieldValue('custrecord_fs_ga_approved_date')
        var ga_trustees_approval_compl = nlapiGetFieldValue('custrecord_fs_ga_trustees_approval_compl')
        var ga_final_list_signed_off = nlapiGetFieldValue('custrecord_fs_ga_final_list_signed_off')
        var ga_trustees_status = nlapiGetFieldValue('custrecord_fs_ga_trustees_status')
        var ga_total_grantable_amount = nlapiGetFieldValue('custrecord_fs_ga_total_grantable_amount')
        var ga_organisation_acceptance = nlapiGetFieldValue('custrecord_fs_ga_organisation_acceptance')
        var ga_reason = nlapiGetFieldValue('custrecord_fs_ga_reason')
        var ga_checklist_q1 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q1')
        var ga_checklist_a1 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a1')
        var ga_checklist_q2 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q2')
        var ga_checklist_a2 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a2')
        var ga_checklist_q3 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q3')
        var ga_checklist_a3 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a3')
        var ga_submitted_by = nlapiGetFieldValue('custrecord_fs_ga_submitted_by')
        var ga_resolution_example = nlapiGetFieldValue('custrecord_fs_ga_resolution_example')
        var ga_final_payments = nlapiGetFieldValue('custrecord_fs_ga_final_payments')
        var ga_payment_date = nlapiGetFieldValue('custrecord_fs_ga_payment_date')
        var name = nlapiGetFieldValue('name')

        var newRecord = nlapiCreateRecord(recType)
        newRecord.setFieldValue('name', name)
        newRecord.setFieldValue('custrecord_fs_ga_application_status', ga_application_status)
        newRecord.setFieldValue('custrecord_fs_ga_title_of_app', ga_title_of_app)
        newRecord.setFieldValue('custrecord_fs_ga_main_purpose', ga_main_purpose)
        newRecord.setFieldValue('custrecord_fs_ga_grant_category', ga_grant_category)
        newRecord.setFieldValue('custrecord_fs_ga_supplier_upload_1', ga_supplier_upload_1)
        newRecord.setFieldValue('custrecord_fs_organisation_link', organisation_link)
        newRecord.setFieldValue('custrecord_fs_ol_grant_region', ol_grant_region)
        newRecord.setFieldValue('custrecord_fs_ga_when_do_you_require_fun', ga_when_do_you_require_fun)
        newRecord.setFieldValue('custrecord_fs_ga_do_you_require_full_amt', ga_do_you_require_full_amt)
        newRecord.setFieldValue('custrecord_fs_ga_benefit_community', ga_benefit_community)
        newRecord.setFieldValue('custrecord_fs_ga_specify_details_of_othe', ga_specify_details_of_othe)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_one', ga_organisation_one)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_two', ga_organisation_two)
        newRecord.setFieldValue('custrecord_fs_ga_answer', ga_answer)
        newRecord.setFieldValue('custrecord_fs_ga_gst_number', ga_gst_number)
        newRecord.setFieldValue('custrecord_fs_ol_grant_relate_to_event', ol_grant_relate_to_event)
        newRecord.setFieldValue('custrecord_fs_ga_event_name', ga_event_name)
        newRecord.setFieldValue('custrecord_fs_ga_event_start_date', ga_event_start_date)
        newRecord.setFieldValue('custrecord_fs_ga_event_end_date', ga_event_end_date)
        newRecord.setFieldValue('custrecord_fs_ga_signature_1', ga_signature_1)
        newRecord.setFieldValue('custrecord_fs_ga_signature_2', ga_signature_2)
        newRecord.setFieldValue('custrecord_fs_ga_amount_applied', ga_amount_applied)
        newRecord.setFieldValue('custrecord_fs_ga_grants_manager_notes', ga_grants_manager_notes)
        newRecord.setFieldValue('custrecord_fs_ga_grant_manager_approval', ga_grant_manager_approval)
        newRecord.setFieldValue('custrecord_fs_ga_approved_date', ga_approved_date)
        newRecord.setFieldValue('custrecord_fs_ga_trustees_approval_compl', ga_trustees_approval_compl)
        newRecord.setFieldValue('custrecord_fs_ga_final_list_signed_off', ga_final_list_signed_off)
        newRecord.setFieldValue('custrecord_fs_ga_trustees_status', ga_trustees_status)
        newRecord.setFieldValue('custrecord_fs_ga_total_grantable_amount', ga_total_grantable_amount)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_acceptance', ga_organisation_acceptance)
        newRecord.setFieldValue('custrecord_fs_ga_reason', ga_reason)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q1', ga_checklist_q1)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a1', ga_checklist_a1)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q2', ga_checklist_q2)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a2', ga_checklist_a2)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q3', ga_checklist_q3)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a3', ga_checklist_a3)
        newRecord.setFieldValue('custrecord_fs_ga_submitted_by', ga_submitted_by)
        newRecord.setFieldValue('custrecord_fs_ga_resolution_example', ga_resolution_example)
        newRecord.setFieldValue('custrecord_fs_ga_final_payments', ga_final_payments)
        newRecord.setFieldValue('custrecord_fs_ga_payment_date', ga_payment_date)

       id = nlapiSubmitRecord(newRecord, true, true)
        //alert('createdid->'+id)

    }
    if (opertaionType == 'edit') {
        //alert('In edit')
        var ga_application_status = nlapiGetFieldValue('custrecord_fs_ga_application_status')
        var ga_title_of_app = nlapiGetFieldValue('custrecord_fs_ga_title_of_app')
        var ga_main_purpose = nlapiGetFieldValue('custrecord_fs_ga_main_purpose')
        var ga_grant_category = nlapiGetFieldValue('custrecord_fs_ga_grant_category')
        var ga_supplier_upload_1 = nlapiGetFieldValue('custrecord_fs_ga_supplier_upload_1')
        var organisation_link = nlapiGetFieldValue('custrecord_fs_organisation_link')
        var ol_grant_region = nlapiGetFieldValue('custrecord_fs_ol_grant_region')
        var ga_when_do_you_require_fun = nlapiGetFieldValue('custrecord_fs_ga_when_do_you_require_fun')
        var ga_do_you_require_full_amt = nlapiGetFieldValue('custrecord_fs_ga_do_you_require_full_amt')
        var ga_benefit_community = nlapiGetFieldValue('custrecord_fs_ga_benefit_community')
        var ga_specify_details_of_othe = nlapiGetFieldValue('custrecord_fs_ga_specify_details_of_othe')
        var ga_organisation_one = nlapiGetFieldValue('custrecord_fs_ga_organisation_one')
        var ga_organisation_two = nlapiGetFieldValue('custrecord_fs_ga_organisation_two')
        var ga_answer = nlapiGetFieldValue('custrecord_fs_ga_answer')
        var ga_gst_number = nlapiGetFieldValue('custrecord_fs_ga_gst_number')
        var ol_grant_relate_to_event = nlapiGetFieldValue('custrecord_fs_ol_grant_relate_to_event')
        var ga_event_name = nlapiGetFieldValue('custrecord_fs_ga_event_name')
        var ga_event_start_date = nlapiGetFieldValue('custrecord_fs_ga_event_start_date')
        var ga_event_end_date = nlapiGetFieldValue('custrecord_fs_ga_event_end_date')
        var ga_signature_1 = nlapiGetFieldValue('custrecord_fs_ga_signature_1')
        var ga_signature_2 = nlapiGetFieldValue('custrecord_fs_ga_signature_2')
        var ga_amount_applied = nlapiGetFieldValue('custrecord_fs_ga_amount_applied')
        var ga_grants_manager_notes = nlapiGetFieldValue('custrecord_fs_ga_grants_manager_notes')
        var ga_grant_manager_approval = nlapiGetFieldValue('custrecord_fs_ga_grant_manager_approval')
        var ga_approved_date = nlapiGetFieldValue('custrecord_fs_ga_approved_date')
        var ga_trustees_approval_compl = nlapiGetFieldValue('custrecord_fs_ga_trustees_approval_compl')
        var ga_final_list_signed_off = nlapiGetFieldValue('custrecord_fs_ga_final_list_signed_off')
        var ga_trustees_status = nlapiGetFieldValue('custrecord_fs_ga_trustees_status')
        var ga_total_grantable_amount = nlapiGetFieldValue('custrecord_fs_ga_total_grantable_amount')
        var ga_organisation_acceptance = nlapiGetFieldValue('custrecord_fs_ga_organisation_acceptance')
        var ga_reason = nlapiGetFieldValue('custrecord_fs_ga_reason')
        var ga_checklist_q1 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q1')
        var ga_checklist_a1 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a1')
        var ga_checklist_q2 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q2')
        var ga_checklist_a2 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a2')
        var ga_checklist_q3 = nlapiGetFieldValue('custrecord_fs_ga_checklist_q3')
        var ga_checklist_a3 = nlapiGetFieldValue('custrecord_fs_ga_checklist_a3')
        var ga_submitted_by = nlapiGetFieldValue('custrecord_fs_ga_submitted_by')
        var ga_resolution_example = nlapiGetFieldValue('custrecord_fs_ga_resolution_example')
        var ga_final_payments = nlapiGetFieldValue('custrecord_fs_ga_final_payments')
        var ga_payment_date = nlapiGetFieldValue('custrecord_fs_ga_payment_date')

        var newRecord = nlapiLoadRecord(recType, nlapiGetRecordId())
        newRecord.setFieldValue('name', name)
        newRecord.setFieldValue('custrecord_fs_ga_application_status', ga_application_status)
        newRecord.setFieldValue('custrecord_fs_ga_title_of_app', ga_title_of_app)
        newRecord.setFieldValue('custrecord_fs_ga_main_purpose', ga_main_purpose)
        newRecord.setFieldValue('custrecord_fs_ga_grant_category', ga_grant_category)
        newRecord.setFieldValue('custrecord_fs_ga_supplier_upload_1', ga_supplier_upload_1)
        newRecord.setFieldValue('custrecord_fs_organisation_link', organisation_link)
        newRecord.setFieldValue('custrecord_fs_ol_grant_region', ol_grant_region)
        newRecord.setFieldValue('custrecord_fs_ga_when_do_you_require_fun', ga_when_do_you_require_fun)
        newRecord.setFieldValue('custrecord_fs_ga_do_you_require_full_amt', ga_do_you_require_full_amt)
        newRecord.setFieldValue('custrecord_fs_ga_benefit_community', ga_benefit_community)
        newRecord.setFieldValue('custrecord_fs_ga_specify_details_of_othe', ga_specify_details_of_othe)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_one', ga_organisation_one)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_two', ga_organisation_two)
        newRecord.setFieldValue('custrecord_fs_ga_answer', ga_answer)
        newRecord.setFieldValue('custrecord_fs_ga_gst_number', ga_gst_number)
        newRecord.setFieldValue('custrecord_fs_ol_grant_relate_to_event', ol_grant_relate_to_event)
        newRecord.setFieldValue('custrecord_fs_ga_event_name', ga_event_name)
        newRecord.setFieldValue('custrecord_fs_ga_event_start_date', ga_event_start_date)
        newRecord.setFieldValue('custrecord_fs_ga_event_end_date', ga_event_end_date)
        newRecord.setFieldValue('custrecord_fs_ga_signature_1', ga_signature_1)
        newRecord.setFieldValue('custrecord_fs_ga_signature_2', ga_signature_2)
        newRecord.setFieldValue('custrecord_fs_ga_amount_applied', ga_amount_applied)
        newRecord.setFieldValue('custrecord_fs_ga_grants_manager_notes', ga_grants_manager_notes)
        newRecord.setFieldValue('custrecord_fs_ga_grant_manager_approval', ga_grant_manager_approval)
        newRecord.setFieldValue('custrecord_fs_ga_approved_date', ga_approved_date)
        newRecord.setFieldValue('custrecord_fs_ga_trustees_approval_compl', ga_trustees_approval_compl)
        newRecord.setFieldValue('custrecord_fs_ga_final_list_signed_off', ga_final_list_signed_off)
        newRecord.setFieldValue('custrecord_fs_ga_trustees_status', ga_trustees_status)
        newRecord.setFieldValue('custrecord_fs_ga_total_grantable_amount', ga_total_grantable_amount)
        newRecord.setFieldValue('custrecord_fs_ga_organisation_acceptance', ga_organisation_acceptance)
        newRecord.setFieldValue('custrecord_fs_ga_reason', ga_reason)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q1', ga_checklist_q1)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a1', ga_checklist_a1)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q2', ga_checklist_q2)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a2', ga_checklist_a2)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_q3', ga_checklist_q3)
        newRecord.setFieldValue('custrecord_fs_ga_checklist_a3', ga_checklist_a3)
        newRecord.setFieldValue('custrecord_fs_ga_submitted_by', ga_submitted_by)
        newRecord.setFieldValue('custrecord_fs_ga_resolution_example', ga_resolution_example)
        newRecord.setFieldValue('custrecord_fs_ga_final_payments', ga_final_payments)
        newRecord.setFieldValue('custrecord_fs_ga_payment_date', ga_payment_date)


        var orgstatus = nlapiGetFieldValue('custrecord_fs_ga_application_status')
        //alert('orgstatus->'+orgstatus)
        if (orgstatus == 3) {
            newRecord.setFieldValue('custrecord_fs_ga_application_status', 2)
        }
        id = nlapiSubmitRecord(newRecord, true, true)
        //alert('updatedid->'+id)
    }

    window.open('/app/common/custom/custrecordentry.nl?rectype=3&id='+id+'&e=T', '_self')
}


// END FUNCTION =====================================================
