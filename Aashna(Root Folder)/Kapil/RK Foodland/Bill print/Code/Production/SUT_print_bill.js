//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_print_bill.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:21-08-13
	 Version:0.1
	 Description:Print bill with required data
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  bill_print(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET

function bill_print(request, response)
{
	/* Suitelet:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	//var form=nlapiCreateForm('Sales Order Summary');
	//var id=nlapiGetRecordId();
	
	//===========Getting the body fields of Customer Payment==============
	
	var param = request.getParameter('custscript_print_bill');
	nlapiLogExecution('DEBUG', 'Vendor bill', 'bill Id=' + param);
	
	var o_bill_rec = nlapiLoadRecord('vendorbill', param); //Loading vendor bill record
	nlapiLogExecution('DEBUG', 'Vendor bill', 'bill rec=' + o_bill_rec);
	
	var s_bill_no = o_bill_rec.getFieldValue('tranid');
	s_bill_no=nullcheckingfield(s_bill_no)
	nlapiLogExecution('DEBUG', 'Vendor bill', 's_bill_no=' + s_bill_no);
	
	var d_date = o_bill_rec.getFieldValue('trandate');
	if(d_date !=null && d_date!= 'undefined' && d_date !='')
	{
		d_date = getDateFromFormat(d_date)
	}
	else
	{
		d_date = ''
	}
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'd_date=' + d_date);
	
	var s_vendor = o_bill_rec.getFieldText('entity')
	s_vendor=nullcheckingfield(s_vendor)
	nlapiLogExecution('DEBUG', 'Vendor bill', 's_vendor=' + s_vendor);
	
	var s_narration = o_bill_rec.getFieldValue('custbody_narration');
	s_narration=nullcheckingfield(s_narration)
	nlapiLogExecution('DEBUG', 'Vendor bill', 'narration=' + s_narration);
	
	var f_amount_total = o_bill_rec.getFieldValue('usertotal');
	nlapiLogExecution('DEBUG', 'Vendor bill', 'narration=' + s_narration);
	
		
	var top_img_url = 'https://system.na1.netsuite.com/core/media/media.nl?id=112&c=3424475&h=90ec1964b357049e37b4';
	var img = nlapiEscapeXML(top_img_url);
	
	//Table design for image and top data
	
	var strVar="";
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\">";
	strVar += "		<img  width=\"100px\" height=\"50px\" align=\"left\" src=\"" + img + " \" \/><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\"><b>Radhakrishna Foodland Pvt. Ltd.<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\" style =\"font-size:9;\">Radhakrishna House, Majiwada Village Road, Majiwade<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\" style =\"font-size:9;\">Thane(W), Maharashtra 400601<\/td>";
	strVar += "	<\/tr>";
	
	/*
strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\">400601<\/td>";
	strVar += "	<\/tr>";
*/
	
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\"><b>Purchase Voucher<\/b><\/td>";
	strVar += "	<\/tr>";
	
	for (var k = 0; k <= 3; k++) 
	{
		strVar += "	<tr>";
		strVar += "		<td width=\"100%\"><\/td>";
		strVar += "	<\/tr>";
	}
	strVar += "<\/table>";
	//strVar += "	<br/>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"35%\"><b>No.&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+s_bill_no+"<\/b><\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	//strVar += "		<td width=\"15%\"><b>Dated&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+d_date+"<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"85%\"><b>Party's Name&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+s_vendor+"<\/b><\/td>";
	//strVar += "		<td width=\"40%\">&nbsp;<\/td>";
	strVar += "		<td width=\"15%\"><b>Dated&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+d_date+"<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table border=\"0.1\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"85%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;\"><b>Particulars<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;\"><b>Amount<\/b><\/td>";
	//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;\"><b>Credit<\/b><\/td>";
	strVar += "	<\/tr>";
	
	
	var i_line_count=o_bill_rec.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'Vendor bill', 'i_line_count=' + i_line_count);
	
	if (i_line_count != null && i_line_count != '' && i_line_count != 'undefined') 
	{
		for (var i = 1; i <= i_line_count; i++) 
		{
			var i_itemID = o_bill_rec.getLineItemText('item', 'item', i);
			if (i_itemID != null && i_itemID != '' && i_itemID != 'undefined') 
			{
				i_itemID = formatAndReplaceMessageForAnd(nlapiEscapeXML(i_itemID))
			}
			else 
			{
				i_itemID = ''
			}
			nlapiLogExecution('DEBUG', 'Vendor bill', 'i_itemID=' + i_itemID);
			
			var f_amount = o_bill_rec.getLineItemValue('item', 'grossamt', i);
			f_amount = nullcheckingfield(f_amount)
			nlapiLogExecution('DEBUG', 'Vendor bill', 'gross amt=' + f_amount);
			
			
			strVar += "	<tr>";
			strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"> " + i_itemID + "<\/td>";
			strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\">" + f_amount + "<\/td>";
			//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\">"+f_credit+"<\/td>";
			strVar += "	<\/tr>";
			
			strVar += "	<tr>";
			strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			strVar += "	<\/tr>";
			
			strVar += "	<tr>";
			strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
			strVar += "	<\/tr>";		
		}
	}
		var i_ex_line_count=o_bill_rec.getLineItemCount('expense');
	    nlapiLogExecution('DEBUG', 'Vendor bill', 'i_ex_line_count=' + i_ex_line_count);
		
		if (i_ex_line_count != null && i_ex_line_count != '' && i_ex_line_count != 'undefined') 
		{
			for (var i = 1; i <= i_line_count; i++) {
				var i_ex_itemID = o_bill_rec.getLineItemText('expense', 'account', i);
				if (i_ex_itemID != null && i_ex_itemID != '' && i_ex_itemID != 'undefined') 
				{
					i_ex_itemID = formatAndReplaceMessageForAnd(nlapiEscapeXML(i_ex_itemID))
				}
				else {
					i_ex_itemID = ''
				}
				
				nlapiLogExecution('DEBUG', 'Vendor bill', 'i_ex_itemID=' + i_ex_itemID);
				
				
				var f_ex_amount = o_bill_rec.getLineItemValue('expense', 'grossamt', i);
				f_ex_amount = nullcheckingfield(f_ex_amount)
				nlapiLogExecution('DEBUG', 'Vendor bill', 'gross amt expense=' + f_ex_amount);
				
				
				strVar += "	<tr>";
				strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"> " + i_ex_itemID + "<\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\">" + f_ex_amount + "<\/td>";
				//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\">"+f_credit+"<\/td>";
				strVar += "	<\/tr>";
				
				
			}
		}
		var amt_in_words = toWordsFunc(f_amount_total)
		//strVar += "<\/table>";
		
		//strVar += "<table width=\"100%\">";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
				
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><b>On Account of :<\/b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+s_narration+"<\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><b>Amount(in words) :<\/b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INR&nbsp;"+amt_in_words+"&nbsp;Only<\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"85%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-left-style:none;border-bottom-style:none;\"><b>"+f_amount_total+"<\/b><\/td>";
		//strVar += "		<td width=\"25%\" align=\"center\" border=\"0.1\" style=\"border-left-style:none;border-right-style:none;\"><b>"+f_total_credit+"<\/b><\/td>";
		strVar += "	<\/tr>";
	
	
	strVar += "<\/table>";
	
	strVar += "	<br/>";
	strVar += "	<br/>";
	strVar += "	<br/>";
	
	strVar += "<table width=\"85%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align=\"right\"><b>Authorised Signatory<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "";

	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
    var file = nlapiXMLToPDF(xml);
    response.setContentType('PDF','Vendor Bill.pdf', 'inline');
    response.write(file.getValue());
	
}

function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	

function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Messgae with and =====" + messgaeToBeSendParaAnd);
 
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
 
  return messgaeToBeSendParaAnd;
} 

function getDateFromFormat(date_val)
{
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Param : " + param);
	//var date = new Date(param);
	//var convertedDate = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
	var months = new Array();
	months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var dateArray = new Array();
	dateArray = date_val.split('/');
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Date : " + dateArray[0]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Month : " + months[dateArray[1]-1]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Year : " + dateArray[2]);
	var convertedDate = dateArray[0] + "-" + months[dateArray[1]-1] + "-" + dateArray[2];
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Converted Date : " + convertedDate);

	return convertedDate;
}	

function toWordsFunc(s)
{
   // alert('In conversion function')
    var str=''

    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}	 

