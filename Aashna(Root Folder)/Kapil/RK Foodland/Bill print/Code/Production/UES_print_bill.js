//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_printJV.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:21-08-13
	 Version:0.1
	 Description:Print pdf on Vendor Bill
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 bill_beforeload(type,form)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY
function bill_beforeload(type,form)
{
	var s_cust_form = nlapiGetFieldValue('customform')
	nlapiLogExecution('DEBUG','Vendor bill','s_cust_form=='+s_cust_form)
	if (type == 'view') 
	{
		var i_bill_id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'PDF', 'Record Id=' + i_bill_id);
		
		
		var o_load_bill = nlapiLoadRecord('vendorbill', i_bill_id)
		nlapiLogExecution('DEBUG', 'PDF', 'o_loadPO=' + o_load_bill);
		
		var cust_form = o_load_bill.getFieldValue('customform');
		nlapiLogExecution('DEBUG', 'PDF', 'custom form=' + cust_form);
		if (cust_form == 101) 
		{
			form.setScript('customscript_cli_print_bill');
			form.addButton('custpage_button', 'Print', 'call_bill(\'' + i_bill_id + '\');');
			//form.addButton('custpage_button', 'Create ASN', nlapiCopyRecord('purchaseorder',i_po_id));
			//nlapiLogExecution('DEBUG', 'PDF', 'field value=' + fld);
			return true;
		}
	}			return true;	
	
}
    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
	 
	 /* On After Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN AFTER SUBMIT CODE BODY

	
    //END AFTER SUBMIT