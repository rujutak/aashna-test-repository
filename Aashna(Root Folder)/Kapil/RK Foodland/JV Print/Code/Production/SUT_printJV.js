//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_printJV.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:20-08-13
	 Version:0.1
	 Description:Print JV
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  jv_print(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET

function jv_print(request, response)
{
	/* Suitelet:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	//var form=nlapiCreateForm('Sales Order Summary');
	//var id=nlapiGetRecordId();
	
	//===========Getting the body fields of Customer Payment==============
	
	var param = request.getParameter('custscript_print_journal');
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'JV Id=' + param);
	
	var o_jv_rec = nlapiLoadRecord('journalentry', param); //Loading JV record
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'JV rec=' + o_jv_rec);
	
	var s_jv_no = o_jv_rec.getFieldValue('tranid');
	s_jv_no=nullcheckingfield(s_jv_no)
	nlapiLogExecution('DEBUG', 'Journal Voucher', 's_jv_no=' + s_jv_no);
	
	var d_date = o_jv_rec.getFieldValue('trandate');
	if(d_date !=null && d_date!= 'undefined' && d_date !='')
	{
		d_date = getDateFromFormat(d_date)
	}
	else
	{
		d_date = ''
	}
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'd_date=' + d_date);
	
	var s_narration = o_jv_rec.getFieldValue('custbody_narration');
	s_narration=nullcheckingfield(s_narration)
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'narration=' + s_narration);
	
	var debit_values=new Array();
	var i_debit_cnt=0;
	
	var credit_values=new Array();
	var i_credit_cnt=0;
	
	
	
	
	var top_img_url = 'https://system.na1.netsuite.com/core/media/media.nl?id=112&c=3424475&h=90ec1964b357049e37b4';
	var img = nlapiEscapeXML(top_img_url);
	
	//Table design for image and top data
	
	var strVar="";
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\">";
	strVar += "		<img  width=\"100px\" height=\"50px\" align=\"left\" src=\"" + img + " \" \/><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\"><b>Radhakrishna Foodland Pvt. Ltd.<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\" style =\"font-size:9;\">Radhakrishna House, Majiwada Village Road, Majiwade<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\" style =\"font-size:9;\">Thane(W), Maharashtra 400601<\/td>";
	strVar += "	<\/tr>";
	
	/*
strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\">400601<\/td>";
	strVar += "	<\/tr>";
*/
	
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align =\"center\"><b>Journal Voucher<\/b><\/td>";
	strVar += "	<\/tr>";
	
	for (var k = 0; k <= 3; k++) 
	{
		strVar += "	<tr>";
		strVar += "		<td width=\"100%\"><\/td>";
		strVar += "	<\/tr>";
	}
	strVar += "<\/table>";
	//strVar += "	<br/>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"35%\"><b>No.&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+s_jv_no+"<\/b><\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td width=\"15%\"><b>Dated&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;"+d_date+"<\/b><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "<\/table>";
	
	strVar += "<table border=\"0.1\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"70%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;\"><b>Particulars<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;\"><b>Debit<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;\"><b>Credit<\/b><\/td>";
	strVar += "	<\/tr>";
	
	
	var i_line_count=o_jv_rec.getLineItemCount('line');
	nlapiLogExecution('DEBUG', 'Journal Voucher', 'i_line_count=' + i_line_count);
	
	if(i_line_count!=null && i_line_count!='')
	{
		var f_total_debit = 0
		var f_total_credit = 0
		for(var i=1;i<=i_line_count;i++)
		{
			var i_accountID=o_jv_rec.getLineItemText('line','account',i);
			i_accountID=formatAndReplaceMessageForAnd(nlapiEscapeXML(i_accountID))
			nlapiLogExecution('DEBUG', 'Journal Voucher', 'acc id=' + i_accountID);
			
			
			
			var f_debit=o_jv_rec.getLineItemValue('line','debit',i);
	          var f_tdebit=0
			  
			  if(f_debit ==null || f_debit=='' || f_debit=='undefined')
		       {
			     f_debit='';
				 f_tdebit=0
		       }
			   
			   else
			   {
			   	 f_tdebit=f_debit
			   }
			   
			  /*
if(f_debit !=null && f_debit!='' && f_debit!='undefined')
			   {
				 debit_values[i_debit_cnt++]=i_accountID+'##'+f_debit+'##';
			   }
*/
			   nlapiLogExecution('DEBUG', 'Journal Voucher', 'debit values=' + f_debit);
			   
		   
			
			
	        var f_credit=o_jv_rec.getLineItemValue('line','credit',i);
	          var f_tcredit=0
			  if(f_credit ==null || f_credit=='' || f_credit=='undefined')
		       {
			     f_credit='';
				 f_tcredit=0
		       }
			   
			   else
			   {
			   	f_tcredit=f_credit
			   }
		
		      /*
if(f_credit !=null && f_credit!='' && f_credit!='undefined')
		       {
			     credit_values[i_credit_cnt++]=i_accountID+'##'+f_credit+'##';
		       }
*/
			   nlapiLogExecution('DEBUG', 'Journal Voucher', 'credit values=' + f_credit);
			    var to;
				if(f_credit>0)
				{
				  to='&nbsp;&nbsp;&nbsp;&nbsp;To'
				}
				else
				{
					to=''
				}
				
				strVar += "	<tr height=\"0.5\">";
				strVar += "		<td width=\"70%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\">"+to+" "+i_accountID+"<\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\">"+f_debit+"<\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\">"+f_credit+"<\/td>";
				strVar += "	<\/tr>";
				
				strVar += "	<tr>";
				strVar += "		<td width=\"70%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "	<\/tr>";
				
				strVar += "	<tr>";
				strVar += "		<td width=\"70\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
				strVar += "	<\/tr>";
						
				f_total_debit = parseFloat(f_total_debit) + parseFloat(f_tdebit)
				if (f_total_debit != null && f_total_debit != '' && f_total_debit != 'undefined' ) 
					{
						f_total_debit = f_total_debit.toFixed(2);
					}
			    else 
					{
						f_total_debit = '';
					}
				nlapiLogExecution('DEBUG', 'Journal Voucher', 'f_total_debit=' + f_total_debit);
				
				f_total_credit = parseFloat(f_total_credit) + parseFloat(f_tcredit)
				if (f_total_credit != null && f_total_credit != '' && f_total_credit != 'undefined' ) 
					{
						f_total_credit = f_total_credit.toFixed(2)
					}
			    else 
					{
						f_total_credit = 0;
					}
				nlapiLogExecution('DEBUG', 'Journal Voucher', 'f_total_credit=' + f_total_credit);
		}
		//strVar += "<\/table>";
		
		//strVar += "<table width=\"100%\">";
		strVar += "	<tr>";
		strVar += "		<td width=\"70%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><b>On Account of :<\/b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+s_narration+"<\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"70%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-top-style:none;border-right-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr>";
		strVar += "		<td width=\"70%\" border=\"0.1\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-left-style:none;\"><b>"+f_total_debit+"<\/b><\/td>";
		strVar += "		<td width=\"15%\" align=\"center\" border=\"0.1\" style=\"border-left-style:none;border-right-style:none;\"><b>"+f_total_credit+"<\/b><\/td>";
		strVar += "	<\/tr>";
	}
	
	strVar += "<\/table>";
	
	strVar += "	<br/>";
	strVar += "	<br/>";
	strVar += "	<br/>";
	
	strVar += "<table width=\"85%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" align=\"right\"><b>Authorised Signatory<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "";

	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
    var file = nlapiXMLToPDF(xml);
    response.setContentType('PDF','Jounal Voucher.pdf', 'inline');
    response.write(file.getValue());
	
}

function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	

function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Messgae with and =====" + messgaeToBeSendParaAnd);
 
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
 
  return messgaeToBeSendParaAnd;
} 

function getDateFromFormat(date_val)
{
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Param : " + param);
	//var date = new Date(param);
	//var convertedDate = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
	var months = new Array();
	months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var dateArray = new Array();
	dateArray = date_val.split('/');
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Date : " + dateArray[0]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Month : " + months[dateArray[1]-1]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Year : " + dateArray[2]);
	var convertedDate = dateArray[0] + "-" + months[dateArray[1]-1] + "-" + dateArray[2];
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Converted Date : " + convertedDate);

	return convertedDate;
}	