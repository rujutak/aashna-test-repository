//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:CLI_autonumbering_payment.js
	 Author:Kapil
	 Company:Aashna
	 Date:03-09-13
	 Version:0.1
	 Description:This script will autonumber the tranid on payment
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 PAGE INIT
	 PageInit(type)
	 
	 SAVE RECORD
	 autonumber_payment_SaveRecord()
	 
	 FIELD CHANGED
	 fieldchanged(type,name,linenum)
	 
	 VALIDATE FIELD
	 validatefield(type,name,linenum)
	 
	 POSTSOURCING
	 postsourcing(type,name)
	 
	 LINE INIT
	 firstlineinit(type)
	 
	 VALIDATE LINE
	 validateline(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
var returntype;	
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
		
		
	 //BEGIN PAGE INIT
		
function PageInit(type)
{
	/* On Page Init:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
 	
	//END LOCAL VARIABLES
	
	//BEGIN PAGE INIT CODE BODY
returntype = type;
//alert('returntype-->'+returntype)
	
}

     //END PAGE INIT
		
	 //BEGIN SAVE RECORD
	 /* On Save Record:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	 
	 //BEGIN SAVE RECORD CODE BODY
function autonumber_deposit_SaveRecord()
{
	var i_user = nlapiGetUser();
		
		if (returntype == 'create' || returntype == 'edit') 
		{
			var s_recordType = nlapiGetRecordType();
			
			//alert('s_recordType'+s_recordType)
			var i_recordId = nlapiGetRecordId();
			//alert('i_recordId'+i_recordId)
			
			
			
			//var o_currentRecord = nlapiLoadRecord(s_recordType, i_recordId);
			
			
			//var i_department = o_currentRecord.getFieldValue('department');
			var i_department = nlapiGetFieldValue('department');
			//alert('i_department'+i_department)
			
			//if (i_department == 1 || i_department == 2) 
			//{
				var o_dept_obj = nlapiLoadRecord('department', i_department);
				//alert('o_dept_obj'+o_dept_obj)
				//nlapiLogExecution('DEBUG', 'aftersubmit', 'department record = ' + o_dept_obj);
				
				var chkbx_autonumber = o_dept_obj.getFieldValue('custrecord_isautonumberingrequired');
				//alert('chkbx_autonumber'+chkbx_autonumber)
				//nlapiLogExecution('DEBUG', 'aftersubmit', 'chkbx_autonumber = ' + chkbx_autonumber);
				//var s_tran_type = o_dept_obj.getFieldValue('custrecord_transactiontype');
				if ((chkbx_autonumber == 'T') && (s_recordType == 'deposit')) 
				{
					var a_trans_type = new Array();
					a_trans_type = o_dept_obj.getFieldValues('custrecord_transactiontype');
					//alert('a_trans_type'+a_trans_type)
					//var a_getsingle_trantype = new Array();
					var i_len = a_trans_type.length;
					
					var i_tranId='';
					
				   for (var j = 0; j < i_len; j++) 
				   {					 
					 if (s_recordType == 'deposit') 
					 {
					 	if(4==a_trans_type[j])
						{
							i_tranId=4
						} 
					 }
					 			 
				   }
				   //alert('a_trans_type'+i_tranId)
						
						var s_dept_intls = o_dept_obj.getFieldValue('custrecord_initialonautonumber');
						//alert('s_dept_intls'+s_dept_intls)
						
						
						var s_recordtypeprefix = '';
						
						//var d_date = o_currentRecord.getFieldValue('trandate')
						var d_date =nlapiGetFieldValue('trandate')
						//date=nlapiDateToString('trandate')
						
						
						var a_DateArray = new Array()
						a_DateArray = d_date.split("/");
						var Day = a_DateArray[0]
						var monthNo = a_DateArray[1]
						var year = a_DateArray[2]//getYear(trandate)
						var d_fnYear = getfnyear(monthNo, year)
						//alert('d_fnYear'+d_fnYear)
						
						
						//var year_twodigit = currentyear.substring(2,4);
						
						
						s_recordtypeprefix = getrecordtypeprefix(s_recordType);
						//alert('s_recordtypeprefix'+s_recordtypeprefix)
						
						/*
alert('s_recordType'+s_recordType)
						alert('d_fnYear'+d_fnYear)
						alert('i_department'+i_department)
						alert('s_dept_intls'+s_dept_intls)
						alert('i_tranId'+i_tranId)
						alert('s_recordtypeprefix'+s_recordtypeprefix)
*/
						var s_get_fn_year = searchfinanacialyear_value(d_fnYear)
						//alert('s_get_fn_year====>>>>'+s_get_fn_year)
						var i_currentno = getcurrentno(s_recordType, d_fnYear, i_department, s_dept_intls,i_tranId,s_recordtypeprefix,s_get_fn_year);
						//alert('i_currentno sk====='+i_currentno)
						
						
						if (i_currentno == 'undefined') 
						{
							var i_nextno = parseInt(0) + parseInt(1);
							//alert('i_nextno before if'+i_nextno)
							
						}
						else 
						{
							var i_nextno = parseInt(i_currentno) + parseInt(1);
							//alert('i_nextno before if'+i_nextno)
							
						}
						
						var i_newno = i_nextno;
						
						if (i_newno.toString().length == 1) 
						{
							i_newno = '00000' + i_newno;
						}
						if (i_newno.toString().length == 2) 
						{
							i_newno = '0000' + i_newno;
						}
						if (i_newno.toString().length == 3) 
						{
							i_newno = '000' + i_newno;
						}
						//alert('i_nextno after if'+i_newno)
						
						var s_noString = '';
						
						//s_recordtypeprefix = getrecordtypeprefix(s_recordType);
						
						
						
						s_noString = s_recordtypeprefix + '/' + s_dept_intls + '/' + i_newno + '/' + d_fnYear;
						//alert('s_noString'+s_noString)
						
						
						//var s_tranid = o_currentRecord.setFieldValue('tranid', s_noString);
						var s_tranid = nlapiSetFieldValue('tranid', s_noString);
						//var s_tranid = o_currentRecord.setFieldValue('memo', s_noString);
						//alert('s_tranid'+s_tranid)
						
						
						//var o_currentrec_submit = nlapiSubmitRecord(o_currentRecord, false, false);						
						
						
						if (i_currentno != null) 
						{
							//var updatecustomrec = updateAutonomaster(i_tranId, i_newno, s_dept_intls);
							var updatecustomrec = updateAutonomaster(i_tranId,i_nextno,i_newno,s_dept_intls,s_recordtypeprefix,d_fnYear,i_department,s_get_fn_year)
							
						}
						
					}
				
			
		}
		
		return true;
	
/*
catch(e)
{
nlapiLogExecution('DEBUG','error =',e.getDetails());	
}
*/
}

    //END SAVE RECORD
		 
	 //BEGIN FIELD CHANGED	
function fieldchanged(type, name, linenum)
{
	/* On FIELD CHANGED:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES

	//END LOCAL VARIABLES

	//BEGIN FIELD CHANGED CODE BODY
}	
	//END FIELD CHANGED
	
	
	
	//BEGIN VALIDATE FIELD

	/* On Validate Field:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN VALIDATE FIELD CODE BODY
	
	//END VALIDATE FIELD
	
	
	//BEGIN POST SOURCING

	/* On Post Sourcing:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN POST SOURCING CODE BODY
	
	//END POST SOURCING
	
		
	 //BEGIN LINE INIT 
		

	/* On Line Init:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN LINE INIT CODE BODY
	


     //END LINE INIT
	   
	   
	   
	   //BEGIN VALIDATE LINE

	/* On Validate Line:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN VALIDATE LINE CODE BODY
	
	//END VALIDATE LINE
//*******************Function for record type prefix************

function getrecordtypeprefix(s_recordType)
{
	if(s_recordType == 'deposit')
	{
		prefix = 'DE';
	}
	return prefix;
}
	
	
function getcurrentno(s_recordType,d_fnYear,i_department,s_dept_intls,i_tranId,s_recordtypeprefix,s_get_fn_year)
{
	/*
alert('getcurrentno s_recordType in get no->'+s_recordType)
	alert('getcurrentno d_fnYear in get no->'+d_fnYear)
	alert('getcurrentno i_department in get no->'+i_department)
	alert('getcurrentno s_dept_intls in get no->'+s_dept_intls)
	alert('getcurrentno i_tranId in get no->'+i_tranId)
	alert('getcurrentno s_recordtypeprefix in get no->'+s_recordtypeprefix)
*/
	
	var filters = new Array();
	var columns = new Array();
	
	//filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', s_recordType);
	filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', i_tranId);
	filters[1] = new nlobjSearchFilter('custrecord_departmentinitials', null, 'is', s_dept_intls);
	//filters[2] = new nlobjSearchFilter('custrecord_financialyear', null, 'is', d_fnYear);
	filters[2] = new nlobjSearchFilter('custrecord_financialyear', null, 'is', s_get_fn_year);
	//filters[3] = new nlobjSearchFilter('custrecord_department', null, 'is', i_department);
	
	columns[0] = new nlobjSearchColumn('custrecord_counternumber');
	columns[1] = new nlobjSearchColumn('custrecord_financialyear');
	columns[2] = new nlobjSearchColumn('custrecord_departmentinitials');
	columns[3] = new nlobjSearchColumn('custrecord_department');
	columns[4] = new nlobjSearchColumn('custrecord_transaction');
	
	var o_result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	//alert('o_result in get no->'+o_result)
	var i_transactionNo = 0;
	var d_year = '';
	var a_trans_custom = '';
		
	
	
	if(o_result != null && o_result != 0 && o_result != 'undefined')
	{
		i_transactionNo = o_result[0].getValue('custrecord_counternumber');
		//alert('i_transactionNo in get no->'+i_transactionNo)
		
		d_year = o_result[0].getText('custrecord_financialyear');
		
		a_trans_custom = o_result[0].getValue('custrecord_transaction');
		
		
	}
	
	else
	{
		//alert('ELSE in get no sk->')
		
		
		var o_newAutonomaster = nlapiCreateRecord('customrecord_autonumbering');
		//alert('o_newAutonomaster in get no->'+o_newAutonomaster)
		var i_serial = o_newAutonomaster.setFieldValue('custrecord_counternumber',1);
		//alert('i_serial in get no->'+i_serial)
		var i_custom_sno = '000001';
		//alert('i_custom_sno in get no->============================='+i_custom_sno)
		
		
		if (d_fnYear != null && d_fnYear != '' && d_fnYear != 'undefined') 
		{
			//o_newAutonomaster.setFieldText('custrecord_financialyear', d_fnYear);
			o_newAutonomaster.setFieldValue('custrecord_financialyear', s_get_fn_year);
		}
		alert('d_fnYear in get no->sk->'+d_fnYear)
		
		
		if(s_recordType == 'deposit')
		//alert('s_recordType in get no before if->'+s_recordType)
		{
			//alert('s_recordType in get no in if->'+s_recordType)
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			
			//var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			var s_dept_in = o_newAutonomaster.setFieldValue('custrecord_departmentinitials',s_dept_intls);
			
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			//alert('s_nostring_in_custom in get no111->'+s_nostring_in_custom)
			
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			
		}
		var i_id=nlapiSubmitRecord(o_newAutonomaster,true,true);
		//alert('i_id in get no->--------------------'+i_id)
		
	}
	return i_transactionNo;
}		
	
function updateAutonomaster(i_tranId,i_nextno,i_newno,s_dept_intls,s_recordtypeprefix,d_fnYear,i_department,s_get_fn_year)
{
	nlapiLogExecution('DEBUG', 'In Update record', 'UPDATION');
	nlapiLogExecution('DEBUG', 'In Update record', 'recordType='+i_tranId);
	nlapiLogExecution('DEBUG', 'In Update record', 'i_newno='+i_newno);
	
	nlapiLogExecution('DEBUG', 'In Update record', 's_recordtypeprefix='+s_recordtypeprefix);
	nlapiLogExecution('DEBUG', 'In Update record', 'i_next no='+i_nextno);
	nlapiLogExecution('DEBUG', 'In Update record', 's_dept_intls='+s_dept_intls);
	
	var filters = new Array();
	var columns = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', i_tranId);
	filters[1] = new nlobjSearchFilter('custrecord_counternumber', null, 'is', i_nextno);
	filters[2] = new nlobjSearchFilter('custrecord_departmentinitials', null, 'is', s_dept_intls);
	//filters[1] = new nlobjSearchFilter('custrecord_serialno', null, 'is', newno);
	
	columns[0] = new nlobjSearchColumn('internalid');
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var internalid = 0;
	
	if(result !=null)
	{
		internalid = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'In Update record', " internalid = " + internalid);
		
		var o_autoNomasterrec = nlapiLoadRecord('customrecord_autonumbering',internalid);
		var i_update_sno1 = o_autoNomasterrec.getFieldValue('custrecord_counternumber');
		nlapiLogExecution('DEBUG', 'In Update record', " get updated serial no1 = " + i_update_sno1);
		//i_update_sno1 = parseInt(i_update_sno1) + 1;
		var i_update_sno = o_autoNomasterrec.setFieldValue('custrecord_counternumber',i_nextno);
		nlapiLogExecution('DEBUG', 'In Update record', " set updated serial no = " + i_update_sno);
		
		/*
var s_dept_in = o_autoNomasterrec.setFieldText('custrecord_departmentinitials',s_dept_intls);
		nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
*/
		
		var s_dept_name = o_autoNomasterrec.setFieldValue('custrecord_department',i_department);
		nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
		
		var i_last_number = o_autoNomasterrec.getFieldValue('custrecord_lastnumbergeneration');
		nlapiLogExecution('DEBUG', 'In Update record', " i_last_number = " + i_last_number);
		
		var s_split_lastno = i_last_number.split('/')
		nlapiLogExecution('DEBUG', 'In Update record', " s_split_lastno = " + s_split_lastno);
		var i_custom_sno = s_split_lastno[2];
		nlapiLogExecution('DEBUG', 'In Update record', " i_custom_sno = " + i_nextno);
		//var i_custom_sno = i_update_sno1;
		
		
		
		//var i_set_custom_sno = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
		var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  i_newno + '/' + d_fnYear;
		var i_set_last_number = o_autoNomasterrec.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom);
		nlapiLogExecution('DEBUG', 'In Update record', " i_set_last_number = " + i_set_last_number);
		
		var i_updatedId1=nlapiSubmitRecord(o_autoNomasterrec);
		//alert('i_updatedId1=='+i_updatedId1);
		//nlapiLogExecution('DEBUG', 'In Update record', " i_updatedId1 = " + i_updatedId1);
	}
}	
	
function getfnyear(month,yearvalue)
{
 yearvalue=yearvalue.substr(2,4)
 nlapiLogExecution('DEBUG', 'getfnyear', " yearvalue-> " + yearvalue);
 var fnYear=''
 if(month ==4 || month==5 || month == 6 )
 {
  //alert("inside")
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==7 || month==8 || month == 9 )
 {
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==10 || month==11 || month ==12 )
 {
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==1 || month==2 || month ==3 )
 {
  var lastYear=parseInt(yearvalue)-1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=lastYear +'-'+yearvalue
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }
 return fnYear
}		

function searchfinanacialyear_value(financeyear)
{
//alert('financeyear==='+financeyear)
 var filtersItem = new Array();
  var columnsItem = new Array();
  var valueId;
  filtersItem[0] = new nlobjSearchFilter('internalid', null, 'is', financeyear);
  
  columnsItem[0] = new nlobjSearchColumn('name');
  columnsItem[1] = new nlobjSearchColumn('internalid');
    
  var searchResultItem = nlapiSearchRecord('customlist_financialyear', null, filtersItem, columnsItem);
 //alert('searchResultItem========'+searchResultItem)
  if (searchResultItem != null) 
  {
   
   var id = searchResultItem[0].getValue('internalid');
   var fn_year_value = searchResultItem[0].getValue('valueid');
  }
  return id;
}	   