//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_autonumbering.js
	 Author:Kapil
	 Date:26-07-13
	 Version:0.1
	 Description:Autonumbering on multiple records
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 02,03/09/13					Kapil								Mayuri								Script edited and deployed on remaining records
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 beforeload(type,form)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY


    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
function aftersubmit_autonumber(type)
{
	/* On After Submit:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN AFTER SUBMIT CODE BODY
	//try 
	//{
		var i_user = nlapiGetUser();
		
		if (type == 'create') 
		{
			var s_recordType = nlapiGetRecordType();
			nlapiLogExecution('DEBUG', 'aftersubmit', 'recordType' + s_recordType);
			var i_recordId = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'aftersubmit', 'recordId' + i_recordId);
			
			
			var o_currentRecord = nlapiLoadRecord(s_recordType, i_recordId);
			nlapiLogExecution('DEBUG', 'aftersubmit', 'current record = ' + o_currentRecord);
			
			var i_department = o_currentRecord.getFieldValue('department');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'current record = ' + i_department);
			//if (i_department == 1 || i_department == 2) 
			//{
				var o_dept_obj = nlapiLoadRecord('department', i_department);
				nlapiLogExecution('DEBUG', 'aftersubmit', 'department record = ' + o_dept_obj);
				
				var chkbx_autonumber = o_dept_obj.getFieldValue('custrecord_isautonumberingrequired');
				nlapiLogExecution('DEBUG', 'aftersubmit', 'chkbx_autonumber = ' + chkbx_autonumber);
				//var s_tran_type = o_dept_obj.getFieldValue('custrecord_transactiontype');
				if ((chkbx_autonumber == 'T') && (s_recordType == 'salesorder' || s_recordType == 'purchaseorder' || s_recordType == 'vendorbill' || s_recordType == 'itemreceipt' || s_recordType == 'invoice' || s_recordType == 'itemfulfillment' || s_recordType == 'inventorytransfer'
				|| s_recordType == 'creditmemo' || s_recordType == 'customerdeposit' || s_recordType == 'estimate' || s_recordType == 'expensereport' || s_recordType == 'inventoryadjustment' || s_recordType == 'journalentry' || s_recordType == 'opportunity' || s_recordType == 'returnauthorization' || s_recordType == 'vendorreturnauthorization' || s_recordType == 'transferorder')) 
				{
					var a_trans_type = new Array();
					a_trans_type = o_dept_obj.getFieldValues('custrecord_transactiontype');
					
					//var a_getsingle_trantype = new Array();
					var i_len = a_trans_type.length;
					
					var i_tranId='';
					nlapiLogExecution('DEBUG', 'aftersubmit', 'transaction_type length = ' + i_len);
				   for (var j = 0; j < i_len; j++) 
				   {
				     /*
if (s_recordType == 'purchaseorder') //a_trans_type[j] = 15;
					 {
						if(15==a_trans_type[j])
						{
							i_tranId=15
							nlapiLogExecution('DEBUG', 'aftersubmit', 'transaction_type in if = ' + i_tranId);
						} 
						
					 }
*/
					 
					 if (s_recordType == 'salesorder') 
					 {
					 	if(31==a_trans_type[j])
						{
							i_tranId=31
						} 
					 }
					 
					 if (s_recordType == 'invoice') 
					 {
					 	if(7==a_trans_type[j])
						{
							i_tranId=7
						} 
					 }
					 
					 if (s_recordType == 'itemfulfillment')
					 {
					 	if(32==a_trans_type[j])
						{
							i_tranId=32
						} 
					 }
					 
					 if (s_recordType == 'inventorytransfer') 
					 {
					 	if(12==a_trans_type[j])
						{
							i_tranId=12
						} 
					 }
					 
					 if (s_recordType == 'vendorbill') 
					 {
					 	if(17==a_trans_type[j])
						{
							i_tranId=17
						} 
					 }
					 
					 if (s_recordType == 'itemreceipt')
					 {
					 	if(16==a_trans_type[j])
						{
							i_tranId=16
						} 
					 }
					 
					 if (s_recordType == 'creditmemo')
					 {
					 	if(10==a_trans_type[j])
						{
							i_tranId=10
						} 
					 }
					 
					 if (s_recordType == 'customerdeposit')
					 {
					 	if(40==a_trans_type[j])
						{
							i_tranId=40
						} 
					 }
					 
					 if (s_recordType == 'estimate')
					 {
					 	if(6==a_trans_type[j])
						{
							i_tranId=6
						} 
					 }
					 
					 if (s_recordType == 'expensereport')
					 {
					 	if(28==a_trans_type[j])
						{
							i_tranId=28
						} 
					 }
					 
					 if (s_recordType == 'inventoryadjustment')
					 {
					 	if(11==a_trans_type[j])
						{
							i_tranId=11
						} 
					 }
					 
					 if (s_recordType == 'journalentry')
					 {
					 	if(1==a_trans_type[j])
						{
							i_tranId=1
						} 
					 }
					 if (s_recordType == 'opportunity')
					 {
					 	if(37==a_trans_type[j])
						{
							i_tranId=37
						} 
					 }
					 if (s_recordType == 'returnauthorization')
					 {
					 	if(33==a_trans_type[j])
						{
							i_tranId=33
						} 
					 }
					 
					 if (s_recordType == 'vendorreturnauthorization')
					 {
					 	if(43==a_trans_type[j])
						{
							i_tranId=43
						} 
					 }
					 
					 if (s_recordType == 'transferorder')
					 {
					 	if(48==a_trans_type[j])
						{
							i_tranId=48
						} 
					 }
				   }
					nlapiLogExecution('DEBUG', 'aftersubmit', 'a_trans_type = ' + i_tranId);
					
						//if (a_trans_type[3] == 7 && a_trans_type[4] == 12 && a_trans_type[6] == 15 && a_trans_type[5] == 16 && a_trans_type[2] == 17 && a_trans_type[0] == 31 && a_trans_type[1] == 32) 
						//{
						//nlapiLogExecution('DEBUG', 'aftersubmit', 'transaction_type in if= ' + a_trans_type);
						
						var s_dept_intls = o_dept_obj.getFieldValue('custrecord_initialonautonumber');
						nlapiLogExecution('DEBUG', 'aftersubmit', 'dept_intls = ' + s_dept_intls);
						
						
						var s_recordtypeprefix = '';
						
						var d_date = o_currentRecord.getFieldValue('trandate')
						//date=nlapiDateToString('trandate')
						nlapiLogExecution('DEBUG', 'Bi Weekly', 'date= ' + d_date);
						
						var a_DateArray = new Array()
						a_DateArray = d_date.split("/");
						var Day = a_DateArray[0]
						var monthNo = a_DateArray[1]
						var year = a_DateArray[2]//getYear(trandate)
						var d_fnYear = getfnyear(monthNo, year)
						nlapiLogExecution('DEBUG', 'Bi Weekly', 'monthNo =' + monthNo);
						nlapiLogExecution('DEBUG', 'Bi Weekly', 'fnYear =' + d_fnYear);
						
						//var year_twodigit = currentyear.substring(2,4);
						//nlapiLogExecution('DEBUG', 'aftersubmit', 'currentyear after split=' + year_twodigit);
						
						s_recordtypeprefix = getrecordtypeprefix(s_recordType);
						nlapiLogExecution('DEBUG', 'aftersubmit', 'recordtypeprefix =' + s_recordtypeprefix);
						
						var i_currentno = getcurrentno(s_recordType, d_fnYear, i_department, s_dept_intls,i_tranId,s_recordtypeprefix);
						nlapiLogExecution('DEBUG', 'aftersubmit', 'currentno =' + i_currentno);
						
						if (i_currentno == 'undefined') 
						{
							var i_nextno = parseInt(0) + parseInt(1);
							nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo before if=' + i_nextno);
						}
						else 
						{
							var i_nextno = parseInt(i_currentno) + parseInt(1);
							nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo before if=' + i_nextno);
						}
						
						var i_newno = i_nextno;
						
						if (i_newno.toString().length == 1) 
						{
							i_newno = '00000' + i_newno;
						}
						if (i_newno.toString().length == 2) 
						{
							i_newno = '0000' + i_newno;
						}
						if (i_newno.toString().length == 3) 
						{
							i_newno = '000' + i_newno;
						}
						nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo after if=' + i_newno);
						var s_noString = '';
						
						//s_recordtypeprefix = getrecordtypeprefix(s_recordType);
						//nlapiLogExecution('DEBUG', 'aftersubmit', 'recordtypeprefix =' + s_recordtypeprefix);
						
						
						s_noString = s_recordtypeprefix + '/' + s_dept_intls + '/' + i_newno + '/' + d_fnYear;
						nlapiLogExecution('DEBUG', 'aftersubmit', 'noString =' + s_noString);
						
						var s_tranid = o_currentRecord.setFieldValue('tranid', s_noString);
						//var s_tranid = o_currentRecord.setFieldValue('memo', s_noString);
						nlapiLogExecution('DEBUG', 'aftersubmit', 'entityid =' + s_tranid);
						
						var o_currentrec_submit = nlapiSubmitRecord(o_currentRecord, false, false);
						nlapiLogExecution('DEBUG', 'aftersubmit', 'currentrecsubmit =' + o_currentrec_submit);
						
						
						
						if (i_currentno != null) 
						{
							//var updatecustomrec = updateAutonomaster(i_tranId, i_newno, s_dept_intls);
							var updatecustomrec = updateAutonomaster(i_tranId,i_nextno,i_newno,s_dept_intls,s_recordtypeprefix,d_fnYear,i_department)
							nlapiLogExecution('DEBUG', 'aftersubmit', 'update custom record =' + updatecustomrec);
						}
						//}
					}
				//}
			//}
		}
		
		return true;
	//}
/*
catch(e)
{
nlapiLogExecution('DEBUG','error =',e.getDetails());	
}
*/
}

    //END AFTER SUBMIT
	
//*******************Function for record type prefix************

function getrecordtypeprefix(s_recordType)
{
    var prefix ='';
	if(s_recordType == 'vendorbill')
	{
		prefix = 'VB';
	}
	if(s_recordType == 'inventorytransfer')
	{
		prefix = 'IT';
	}
	if(s_recordType == 'invoice')
	{
		prefix = 'INV';
	}
	if(s_recordType == 'itemfulfillment')
	{
		prefix = 'IF';
	}
	if(s_recordType == 'itemreceipt')
	{
		prefix = 'IR';
	}
	if(s_recordType == 'creditmemo')
	{
		prefix = 'CM';
	}
	if(s_recordType == 'salesorder')
	{
		prefix = 'SO';
	}
	if(s_recordType == 'customerdeposit')
	{
		prefix = 'CD';
	}
	
	if(s_recordType == 'estimate')
	{
		prefix = 'ES';
	}
	
	if(s_recordType == 'expensereport')
	{
		prefix = 'EXR';
	}
	
	if(s_recordType == 'inventoryadjustment')
	{
		prefix = 'IA';
	}	
	
	if(s_recordType == 'journalentry')
	{
		prefix = 'JV';
	}
	if(s_recordType == 'opportunity')
	{
		prefix = 'OP';
	}
	if(s_recordType == 'returnauthorization')
	{
		prefix = 'RA';
	}
	if(s_recordType == 'vendorreturnauthorization')
	{
		prefix = 'VRA';
	}
	if(s_recordType == 'transferorder')
	{
		prefix = 'TO';
	}
	return prefix;
}
function getcurrentno(s_recordType,d_fnYear,i_department,s_dept_intls,i_tranId,s_recordtypeprefix)
{
	nlapiLogExecution('DEBUG', 'In getCurrentno', " s_recordtypeprefix  =" + s_recordtypeprefix);
	nlapiLogExecution('DEBUG', 'In getCurrentno', " a_trans_type  =" + i_tranId);
	nlapiLogExecution('DEBUG', 'In getCurrentno', " d_fnYear  =" + d_fnYear);
	nlapiLogExecution('DEBUG', 'In getCurrentno', " s_recordType  =" + s_recordType);
	nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls  =" + s_dept_intls);
	var filters = new Array();
	var columns = new Array();
	
	//filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', s_recordType);
	filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', i_tranId);
	filters[1] = new nlobjSearchFilter('custrecord_departmentinitials', null, 'is', s_dept_intls);
	filters[2] = new nlobjSearchFilter('custrecord_financialyear', null, 'is', d_fnYear);
	//filters[3] = new nlobjSearchFilter('custrecord_department', null, 'is', i_department);
	
	columns[0] = new nlobjSearchColumn('custrecord_counternumber');
	columns[1] = new nlobjSearchColumn('custrecord_financialyear');
	columns[2] = new nlobjSearchColumn('custrecord_departmentinitials');
	columns[3] = new nlobjSearchColumn('custrecord_department');
	columns[4] = new nlobjSearchColumn('custrecord_transaction');
	
	var o_result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var i_transactionNo = 0;
	var d_year = '';
	var a_trans_custom = '';
		
	nlapiLogExecution('DEBUG', 'In getcurrentno', " result " + o_result);
	
	if(o_result != null && o_result != 0 && o_result != 'undefined')
	{
		i_transactionNo = o_result[0].getValue('custrecord_counternumber');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " transaction no =" + i_transactionNo);
		d_year = o_result[0].getText('custrecord_financialyear');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " year =" + d_year);
		a_trans_custom = o_result[0].getValue('custrecord_transaction');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " a_trans_custom =" + a_trans_custom);
		
	}
	
	else
	{
		nlapiLogExecution('DEBUG', 'In getCurrentno', " transaction no =" + i_transactionNo);
		nlapiLogExecution('DEBUG', 'In getCurrentno', " year =" + d_year);
		var o_newAutonomaster = nlapiCreateRecord('customrecord_autonumbering');
		var i_serial = o_newAutonomaster.setFieldValue('custrecord_counternumber',1);
		var i_custom_sno = '000001';
		nlapiLogExecution('DEBUG', 'In getcurrentno', "i_custom_sno =" + i_custom_sno);
		
		if (d_fnYear != null && d_fnYear != '' && d_fnYear != 'undefined') 
		{
			o_newAutonomaster.setFieldText('custrecord_financialyear', d_fnYear);
		}
		nlapiLogExecution('DEBUG', 'In getcurrentno', "set currentyear =" + d_fnYear);
		
		if(s_recordType == 'vendorbill')
		{
			/*
var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',17);
			var s_dept_in = o_newAutonomaster.setFieldValue('custrecord_departmentinitials',s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			var s_nostring_in_custom = s_rec + '/' + s_dept_in +'/' + '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
*/
            var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		if(s_recordType == 'inventorytransfer')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		if(s_recordType == 'invoice')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		if(s_recordType == 'itemfulfillment')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		if(s_recordType == 'itemreceipt')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'salesorder')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'creditmemo')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'customerdeposit')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'estimate')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'expensereport')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'inventoryadjustment')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'journalentry')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'opportunity')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'returnauthorization')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'vendorreturnauthorization')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		
		if(s_recordType == 'transferorder')
		{
			var s_rec = o_newAutonomaster.setFieldValue('custrecord_transaction',i_tranId);  //7
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_rec =" + s_rec);
			var s_dept_in = o_newAutonomaster.setFieldText('custrecord_departmentinitials',s_dept_intls);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
			var s_dept_name = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
			nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
			var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  '000001' + '/' + d_fnYear;
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
			var set_s_nostring_in_custom = o_newAutonomaster.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom)
			nlapiLogExecution('DEBUG', 'In getCurrentno', " s_nostring_in_custom =" + s_nostring_in_custom);
		}
		var i_id=nlapiSubmitRecord(o_newAutonomaster,true,true);
		nlapiLogExecution('DEBUG', 'In getCurrentno', ' Newly Created Id =' + i_id);
	}
	return i_transactionNo;
}	


function updateAutonomaster(i_tranId,i_nextno,i_newno,s_dept_intls,s_recordtypeprefix,d_fnYear,i_department)
{
	nlapiLogExecution('DEBUG', 'In Update record', 'UPDATION');
	nlapiLogExecution('DEBUG', 'In Update record', 'recordType='+i_tranId);
	nlapiLogExecution('DEBUG', 'In Update record', 'i_newno='+i_newno);
	
	nlapiLogExecution('DEBUG', 'In Update record', 's_recordtypeprefix='+s_recordtypeprefix);
	nlapiLogExecution('DEBUG', 'In Update record', 'i_next no='+i_nextno);
	nlapiLogExecution('DEBUG', 'In Update record', 's_dept_intls='+s_dept_intls);
	
	var filters = new Array();
	var columns = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_transaction', null, 'is', i_tranId);
	filters[1] = new nlobjSearchFilter('custrecord_counternumber', null, 'is', i_nextno);
	filters[2] = new nlobjSearchFilter('custrecord_departmentinitials', null, 'is', s_dept_intls);
	//filters[1] = new nlobjSearchFilter('custrecord_serialno', null, 'is', newno);
	
	columns[0] = new nlobjSearchColumn('internalid');
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var internalid = 0;
	
	if(result !=null)
	{
		internalid = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'In Update record', " internalid = " + internalid);
		
		var o_autoNomasterrec = nlapiLoadRecord('customrecord_autonumbering',internalid);
		var i_update_sno1 = o_autoNomasterrec.getFieldValue('custrecord_counternumber');
		nlapiLogExecution('DEBUG', 'In Update record', " get updated serial no1 = " + i_update_sno1);
		//i_update_sno1 = parseInt(i_update_sno1) + 1;
		var i_update_sno = o_autoNomasterrec.setFieldValue('custrecord_counternumber',i_nextno);
		nlapiLogExecution('DEBUG', 'In Update record', " set updated serial no = " + i_update_sno);
		
		var s_dept_in = o_autoNomasterrec.setFieldText('custrecord_departmentinitials',s_dept_intls);
		nlapiLogExecution('DEBUG', 'In getCurrentno', " s_dept_intls =" + s_dept_intls);
		
		var s_dept_name = o_autoNomasterrec.setFieldValue('custrecord_department',i_department);
		nlapiLogExecution('DEBUG', 'In getCurrentno', " i_department =" + i_department);
		
		var i_last_number = o_autoNomasterrec.getFieldValue('custrecord_lastnumbergeneration');
		nlapiLogExecution('DEBUG', 'In Update record', " i_last_number = " + i_last_number);
		
		var s_split_lastno = i_last_number.split('/')
		nlapiLogExecution('DEBUG', 'In Update record', " s_split_lastno = " + s_split_lastno);
		var i_custom_sno = s_split_lastno[2];
		nlapiLogExecution('DEBUG', 'In Update record', " i_custom_sno = " + i_nextno);
		//var i_custom_sno = i_update_sno1;
		
		
		
		//var i_set_custom_sno = o_newAutonomaster.setFieldValue('custrecord_department',i_department);
		var s_nostring_in_custom = s_recordtypeprefix + '/' + s_dept_intls +'/' +  i_newno + '/' + d_fnYear;
		var i_set_last_number = o_autoNomasterrec.setFieldValue('custrecord_lastnumbergeneration',s_nostring_in_custom);
		nlapiLogExecution('DEBUG', 'In Update record', " i_set_last_number = " + i_set_last_number);
		
		var i_updatedId1=nlapiSubmitRecord(o_autoNomasterrec);
		nlapiLogExecution('DEBUG', 'In Update record', " i_updatedId1 = " + i_updatedId1);
	}
}

function getfnyear(month,yearvalue)
{
 yearvalue=yearvalue.substr(2,4)
 nlapiLogExecution('DEBUG', 'getfnyear', " yearvalue-> " + yearvalue);
 var fnYear=''
 if(month ==4 || month==5 || month == 6 )
 {
  //alert("inside")
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==7 || month==8 || month == 9 )
 {
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==10 || month==11 || month ==12 )
 {
  var nextYear=parseInt(yearvalue)+1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=yearvalue+'-'+nextYear
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }

 if(month ==1 || month==2 || month ==3 )
 {
  var lastYear=parseInt(yearvalue)-1
  nlapiLogExecution('DEBUG', 'getfnyear', " nextYear-> " + nextYear);
  fnYear=lastYear +'-'+yearvalue
  nlapiLogExecution('DEBUG', 'getfnyear', " fnYear-> " + fnYear);
 }
 return fnYear
}