function cliPO_submit(i_po_id) 	
{
	try 
	{
		//alert('PO_id =' + i_po_id)
		
		var o_copy_PO_rec = nlapiCopyRecord('purchaseorder', i_po_id);
		//alert('PO_obj copy=' + o_copy_PO_rec)
		o_copy_PO_rec.setFieldValue('custbody_asnparentpo', i_po_id)
		
		var cust_form = o_copy_PO_rec.getFieldValue('customform');
		//alert('cust_form=' + cust_form)
		o_copy_PO_rec.setFieldValue('customform', 127)
		
		var o_po_obj = nlapiLoadRecord('purchaseorder',i_po_id);
		//alert('o_po_obj =' + o_po_obj)
		
		var i_linecount = o_copy_PO_rec.getLineItemCount('item');
		//alert('i_linecount='+i_linecount)
		if (i_linecount != null && i_linecount != 'undefined' && i_linecount != '') 
			{
				//for (var i = 1; i <= i_linecount; i++) 
				for (var i = i_linecount; i >0 ; i--)
				{
					//var i_qty = o_copy_PO_rec.getLineItemValue('item', 'quantity', i)
					//alert('i_qty from copy=' + i_qty)
					
															
					var i_parent_pending_qty = o_po_obj.getLineItemValue('item', 'custcolpendingqtyforasn', i);
					
					var i_qty = o_po_obj.getLineItemValue('item', 'quantity', i);
					//alert('parent quantity'+i_qty)
					
                    //var f_pending_qty = parseFloat(i_parent_qty) - parseFloat(i_qty);
					//alert('i_parent_pending_qty=' + i_parent_pending_qty)
                    var f_set_pending_qty = ''
					if (i_parent_pending_qty !=null && i_parent_pending_qty != '' && i_parent_pending_qty!= 'undefined' && parseInt(i_parent_pending_qty) != 0) 
					{
						//alert('f_pending_qty='+ i_parent_pending_qty); 
						
						o_copy_PO_rec.setLineItemValue('item', 'quantity', i, i_parent_pending_qty);
					    //alert('Qty Set')
					}
					else if(parseInt(i_parent_pending_qty) == 0)
					{
						//alert('i_parent_pending_qty in else='+ parseInt(i_parent_pending_qty));
						
						var s_remove = o_copy_PO_rec.removeLineItem('item',i)
						//alert('remove item'+s_remove)
					
						//var s_commit = o_copy_PO_rec.commitLineItem('item');
						//alert('commit item'+s_commit)
					}
					else if(parseInt(i_parent_pending_qty) == '')
					{
						o_copy_PO_rec.setLineItemValue('item', 'quantity', i, i_qty);
					}
					//var s_commit = o_copy_PO_rec.commitLineItem('item');
				    //alert('commit item'+s_commit)
				}
				
			}
		
		var id = nlapiSubmitRecord(o_copy_PO_rec, true, true)
		//alert('PO_id copy submit=' + id)
		
		
	}	
catch(e)
{
	alert('Error'+e.getDetails())
}	
window.open("https://system.na1.netsuite.com/app/accounting/transactions/purchord.nl?id="+id+"&whence=&e=T")
}