//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_PO_to_ASN_setfields.js
	 Date:12-08-13
	 Version:0.1
	 Description:Get all values of PO ,and set those values to ASN record
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 po_submit_beforeLoad(type)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 po_submit_afterSubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY

//END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
function po_submit_afterSubmit(type)
{
	/* On After Submit:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES

	//END LOCAL VARIABLES

	//BEGIN AFTER SUBMIT CODE BODY
	nlapiLogExecution('DEBUG','after submit','type='+type)
	var contextObj = nlapiGetContext();
    var newType = contextObj.getExecutionContext();
    nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Excustion type##############=' + newType);
    if(newType!='userevent')//scheduled
    //{
     //if(newType!='scheduled')
     //{
	
	//if (type == 'create' || type == 'edit') && (s_current_context.getExecutionContext() == 'userinterface')) 
	{
		var cust_form = nlapiGetFieldValue('customform')
		nlapiLogExecution('DEBUG', 'after submit', 'cust_form=' + cust_form)
		if (cust_form == 127) 
		{
			var s_po_id = nlapiGetFieldValue('custbody_asnparentpo');
			nlapiLogExecution('DEBUG', 'after submit', 'PO id=' + s_po_id)
			
			var o_po_obj = nlapiLoadRecord('purchaseorder', s_po_id)
			nlapiLogExecution('DEBUG', 'after submit', 'load parent po=' + o_po_obj)
			
			var i_linecount_parent_PO = o_po_obj.getLineItemCount('item')
			nlapiLogExecution('DEBUG', 'after submit', 'i_linecount_parent_PO=' + i_linecount_parent_PO)
			
			var i_linecount = nlapiGetLineItemCount('item');
			nlapiLogExecution('DEBUG', 'after submit', 'i_linecount copy PO=' + i_linecount)
			
			if (i_linecount_parent_PO != null && i_linecount_parent_PO != 'undefined' && i_linecount_parent_PO != '') 
			{
				for (var i = 1; i <= i_linecount_parent_PO; i++) 
				{
					var i_parent_item = o_po_obj.getLineItemValue('item', 'item', i);
					nlapiLogExecution('DEBUG','after submit','i_parent_item'+i_parent_qty)
					
					var i_parent_qty = o_po_obj.getLineItemValue('item', 'quantity', i);
					nlapiLogExecution('DEBUG','after submit','i_parent_qty'+i_parent_qty)
					
					var i_parent_pending_qty = o_po_obj.getLineItemValue('item', 'custcolpendingqtyforasn', i);
				    nlapiLogExecution('DEBUG','after submit','i_parent_pending_qty'+i_parent_pending_qty)
					
					
					for(var j= 1;j<=i_linecount;j++)
					{
						var i_copy_item = nlapiGetLineItemValue('item', 'item', j)
					    nlapiLogExecution('DEBUG', 'after submit', 'i_qty=' + i_qty)
						
						if(i_copy_item == i_parent_item)
						{
							var i_qty = nlapiGetLineItemValue('item', 'quantity', j)
					        nlapiLogExecution('DEBUG', 'after submit', 'i_qty=' + i_qty)
							
							if (i_parent_pending_qty != '' && i_parent_pending_qty != 'undefined' && i_parent_pending_qty != null) 
					        {
					
							var f_pending = parseFloat(i_parent_pending_qty) - parseFloat(i_qty);
							//var f_pending = parseFloat(i_parent_qty) - parseFloat(i_qty);
							nlapiLogExecution('DEBUG', 'after submit', 'i_pending before if' + f_pending)
								if (f_pending >= 0) 
								{
									nlapiLogExecution('DEBUG', 'after submit', 'i_pending in if' + f_pending)
									
									var f_set_pending_qty = o_po_obj.setLineItemValue('item', 'custcolpendingqtyforasn', i, f_pending);
									//nlapiLogExecution('DEBUG', 'after submit', 's_set_qty' + f_set_pending_qty)
								}
					        }
							else
					        {
						
					         if (i_parent_pending_qty == '' || i_parent_pending_qty == 'undefined' || i_parent_pending_qty == null) 
					         {
					
								var f_pending = parseFloat(i_parent_qty) - parseFloat(i_qty);
								nlapiLogExecution('DEBUG', 'after submit', 'i_pending before if' + f_pending)
								if (f_pending >= 0) 
								{
									nlapiLogExecution('DEBUG', 'after submit', 'i_pending in if' + f_pending)
									
									var f_set_pending_qty = o_po_obj.setLineItemValue('item', 'custcolpendingqtyforasn', i, f_pending);
									//nlapiLogExecution('DEBUG', 'after submit', 's_set_qty' + f_set_pending_qty)
								}
					         }
					        }
							
						}
						
					}
					
					
					/*
if (i_parent_pending_qty != '' && i_parent_pending_qty != 'undefined' && i_parent_pending_qty != null) 
					{
					
						var f_pending = parseFloat(i_parent_pending_qty) - parseFloat(i_qty);
						//var f_pending = parseFloat(i_parent_qty) - parseFloat(i_qty);
						nlapiLogExecution('DEBUG', 'after submit', 'i_pending before if' + f_pending)
						if (f_pending >= 0) 
						{
							nlapiLogExecution('DEBUG', 'after submit', 'i_pending in if' + f_pending)
							
							var f_set_pending_qty = o_po_obj.setLineItemValue('item', 'custcolpendingqtyforasn', i, f_pending);
							nlapiLogExecution('DEBUG', 'after submit', 's_set_qty' + f_set_pending_qty)
						}
					}
					else
					{
						
					if (i_parent_pending_qty == '' || i_parent_pending_qty == 'undefined' || i_parent_pending_qty == null) 
					  {
					
						var f_pending = parseFloat(i_parent_qty) - parseFloat(i_qty);
						nlapiLogExecution('DEBUG', 'after submit', 'i_pending before if' + f_pending)
						if (f_pending >= 0) 
						{
							nlapiLogExecution('DEBUG', 'after submit', 'i_pending in if' + f_pending)
							
							var f_set_pending_qty = o_po_obj.setLineItemValue('item', 'custcolpendingqtyforasn', i, f_pending);
							nlapiLogExecution('DEBUG', 'after submit', 's_set_qty' + f_set_pending_qty)
						}
					   }
					}
*/
					
				}
			}
			var id = nlapiSubmitRecord(o_po_obj,false,false)
		    nlapiLogExecution('DEBUG','after submit','submit id=='+id)
		}
		
	}
	
}	
    //END AFTER SUBMIT