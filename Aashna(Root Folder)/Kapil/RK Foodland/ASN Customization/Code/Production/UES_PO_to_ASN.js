//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_PO_to_ASN.js
	 Date:12-08-13
	 Version:0.1
	 Description:Add button on Purchase order record 
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 buttonCreateASN(type)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY
function buttonCreateASN(type,form)
{
	nlapiLogExecution('DEBUG', 'PDF', 'type=' + type);
	if(type=='view')
	{
		var i_po_id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'PDF', 'Record Id=' + i_po_id);
				
		
		var o_loadPO = nlapiLoadRecord('purchaseorder',i_po_id)
		nlapiLogExecution('DEBUG', 'PDF', 'o_loadPO=' + o_loadPO);
		
		var cust_form = o_loadPO.getFieldValue('customform');
		nlapiLogExecution('DEBUG', 'PDF', 'custom form=' + cust_form);
		if (cust_form == 111) 
		{
			form.setScript('customscript_cli_po_to_asn');
			form.addButton('custpage_button', 'Create ASN', 'cliPO_submit(\'' + i_po_id + '\');');
			//form.addButton('custpage_button', 'Create ASN', nlapiCopyRecord('purchaseorder',i_po_id));
			//nlapiLogExecution('DEBUG', 'PDF', 'field value=' + fld);
			return true;
		}
	}
		
	
}
    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
	 
	 /* On After Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN AFTER SUBMIT CODE BODY

	
    //END AFTER SUBMIT