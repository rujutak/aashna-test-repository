// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_restrict_qty_on_ASN.js
	Author:Kapil Srivastava
	Company:Aashna
	Date:13/08/13
    Description:Restrict the quantity to be changed on ASN


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
                       


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	var operationtype;
	var s_item_Array = new Array();
	var s_qty_Array = new Array();
	var a_qty_pending_array = new Array();
		
    //var originaltrandate;
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================
function pageInit_restrictqty(type)
{

	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES


	//  PAGE INIT CODE BODY
	var s_form = nlapiGetFieldValue('customform')
	//alert('form=='+s_form)
	if (s_form == 127) 
	{
		var s_parent_po_id = nlapiGetFieldValue('custbody_asnparentpo');
		//alert('parent PO id ='+s_parent_po_id)
		
		var s_load_PO = nlapiLoadRecord('purchaseorder', s_parent_po_id);
		//alert('parent PO obj ='+s_load_PO)
		
		var k_linecount = s_load_PO.getLineItemCount('item');
		//alert('parent PO linecount ='+k_linecount)
		var j = 0
		if (k_linecount != null && k_linecount != '' && k_linecount != 'undefined') 
		{
			for (var k = 1; k <= k_linecount; k++) 
			{
				s_item_Array[j] = s_load_PO.getLineItemValue('item', 'item', k)
				//alert('s_item_Array=='+s_item_Array);
				//var j = parseInt(j) + 1
				
				s_qty_Array[j] = s_load_PO.getLineItemValue('item', 'quantity', k)
				
				a_qty_pending_array[j] = s_load_PO.getLineItemValue('item', 'custcolpendingqtyforasn', k)
				
				var j = parseInt(j) + 1
			}
		//alert('s_item_Array=='+s_item_Array);
		}
	}

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================
function saveRecord()
{
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	    Date                 trandate
	 */
	//  LOCAL VARIABLES



	//  SAVE RECORD CODE BODY

	return true;
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{
	/*  On validate field:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY

return true;
}	



// END VALIDATE FIELD ===============================================


var s_blank ='';


// BEGIN FIELD CHANGED ==============================================

function fieldChanged_restrictqty(type, name)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY
 
 
 
 var s_cust_form = nlapiGetFieldValue('customform');
 if (s_cust_form == 127) 
 {
 		
 	var s_parent_po_id = nlapiGetFieldValue('custbody_asnparentpo');
	//alert('s_po_id=' + s_parent_po_id)
	
    var o_load_PO = nlapiLoadRecord('purchaseorder',s_parent_po_id);
    //alert('o_load_PO=' + o_load_PO)
	
 	if (name == 'quantity') 
	{
		
		var s_items_copy_PO =''
		var i_qty_copy_PO = ''
		var flag=0
					
					s_items_copy_PO = nlapiGetCurrentLineItemValue('item', 'item')
					var text_s_items_copy_PO = nlapiGetCurrentLineItemText('item', 'item')
					//alert('text_s_items_copy_PO=='+text_s_items_copy_PO)
					
					
					i_qty_copy_PO = nlapiGetCurrentLineItemValue('item', 'quantity')
					//alert('s_items_copy_PO=='+s_items_copy_PO);
					//alert('length=='+s_item_Array.length);
					for (var i = 0; i < s_item_Array.length; i++) 
					{
						//alert('s_item_Array parent =='+s_item_Array );
						//alert('s_item_Array[i] =='+s_item_Array[i] );
						//if (s_item_Array[i] == s_items_copy_PO) 
						if (s_item_Array [i]== s_items_copy_PO)
						{
							//alert('i_qty_copy_PO parent =='+i_qty_copy_PO +'--s_qty_Array[i]-->'+s_qty_Array[i]);
							if(parseInt(i_qty_copy_PO) > parseInt(s_qty_Array[i]))
							{
								alert('You can not enter quantity more than parent PO quantity')
								nlapiSetCurrentLineItemValue('item','quantity',0,false,false);
								return false;
							}
							/*
else
							{
								flag=1
							}
*/
							
						}
						if(s_item_Array [i] == s_items_copy_PO)
						{
							//alert('s_item_Array parent =='+s_item_Array[i] +'--s_items_copy_PO-->'+s_items_copy_PO);
							//alert('a_qty_pending_array-->'+a_qty_pending_array[i])
							if(parseInt(a_qty_pending_array[i]) == 0)
							{
								alert('You can not add item with 0 pending quantity on parent PO')
								nlapiSetCurrentLineItemValue('item','quantity',0,false,false);
								return false;
							}
						}
					}
					
										
		
		/*
var i_index = nlapiGetCurrentLineItemIndex('item');
		//alert('index=' + i_index);
		
		var i_qty_copy_PO = nlapiGetCurrentLineItemValue('item', 'quantity')
 		//alert('quantity from copied PO=' + i_qty_copy_PO);
		
				
		if(i_qty_copy_PO != null && i_qty_copy_PO != '' && i_qty_copy_PO != 'undefined')
		{
			var i_qty_parent_PO = o_load_PO.getLineItemValue('item', 'quantity',i_index)
 			//alert('quantity from parent PO=' + i_qty_parent_PO);
			
			if (parseInt(i_qty_copy_PO) > parseInt(i_qty_parent_PO)) 
				{
 					alert('You can not enter quantity more than parent PO quantity')
					nlapiSetCurrentLineItemValue('item','quantity',0,false,false);
					//return false;
					//break;
 				}
				else
				{
					//alert('in else block')
					return true;
				}
		}
*/

 		
 	}
	
	
 }

	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing_restrictqty(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY
if (type == 'item' && name == 'item') 
	{
		var s_cust_form = nlapiGetFieldValue('customform');
		//alert('s_cust_form' + s_cust_form);
		if (s_cust_form == 127) 
		{
		
			var s_parent_po_id = nlapiGetFieldValue('custbody_asnparentpo');
			//alert('s_parent_po_id='+s_parent_po_id);
			var s_items = new Array();
			
			var j_linecount = nlapiGetLineItemCount('item');
			//alert('linecount copy='+j_linecount);
			
			var s_items_copy_PO =''
			var flag=0
					
					s_items_copy_PO = nlapiGetCurrentLineItemValue('item', 'item')
					//alert('s_items_copy_PO=='+s_items_copy_PO);
					//alert('length=='+s_item_Array.length);
					for (var i = 0; i < s_item_Array.length; i++) 
					{
						//alert('s_item_Array parent =='+s_item_Array );
						//alert('s_item_Array[i] =='+s_item_Array[i] );
						//if (s_item_Array[i] == s_items_copy_PO) 
						if (s_item_Array [i]== s_items_copy_PO)
						{
							flag=1
						}
						
					}
					
					if(flag==0)
					{
						alert('You can not enter items other than parent PO')
						return false
					}						
						
		}
		
	
	}
	

}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY
    


}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_restrictqty(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY
	
if (type == 'item') 
	{
		var s_cust_form = nlapiGetFieldValue('customform');
		//alert('s_cust_form' + s_cust_form);
		if (s_cust_form == 127) 
		{
		
			var s_parent_po_id = nlapiGetFieldValue('custbody_asnparentpo');
			
			var s_items = new Array();
			
			var j_linecount = nlapiGetLineItemCount('item');
			//alert('linecount copy='+j_linecount);
			
			var s_items_copy_PO = new Array();
			var flag=0
					
					s_items_copy_PO = nlapiGetCurrentLineItemValue('item', 'item')
					//alert('s_items_copy_PO=='+s_items_copy_PO);
					//alert('length=='+s_item_Array.length);
					for (var i = 0; i < s_item_Array.length; i++) 
					{
						//alert('s_item_Array[i] =='+s_item_Array[i] );
						if (s_item_Array[i] == s_items_copy_PO) 
						{
							flag=1
						}
						
					}
					
					if(flag==0)
					{
						alert('You can not enter items other than parent PO')
						return false
					}						
						
		}
		
	
	}


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type,action)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY
	
    


	return true;
}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
/*
function validateDelete(type)
{
	var s_cust_form = nlapiGetFieldValue('customform');
	//alert('s_cust_form in val delete'+s_cust_form);
	if (s_cust_form == 127) 
	{
		alert('You are not allowed to remove items from ASN');
		return false;
	}
}
*/


// END FUNCTION =====================================================






















