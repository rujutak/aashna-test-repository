//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_add_ASN_details.js
	 Author:Kapil Srivastava
	 Date:20-08-13
	 Version:0.1
	 Description:Get the values of ASN ,and set those values to ASN custom record
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 beforeLoad(type)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 asn_details_afterSubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY

//END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
function asn_details_afterSubmit(type)
{
	/* On After Submit:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES

	//END LOCAL VARIABLES

	//BEGIN AFTER SUBMIT CODE BODY
	nlapiLogExecution('DEBUG','after submit','type='+type)
	//var contextObj = nlapiGetContext();
    //var newType = contextObj.getExecutionContext();
    //nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Excustion type##############=' + newType);
    //if(newType!='userevent')//scheduled
	if(type == 'create')
    {
		var s_get_form = nlapiGetFieldValue('customform');
		nlapiLogExecution('DEBUG','After submit','Custom form=='+s_get_form)
		
		if (s_get_form == 127) 
		{
		
			var s_get_parent_PO = nlapiGetFieldValue('custbody_asnparentpo')
			nlapiLogExecution('DEBUG', 'After submit', 'Parent PO==' + s_get_parent_PO)
			
			var s_get_ASN_no = nlapiGetRecordId()
			//var s_get_ASN_no = nlapiGetFieldValue('tranid')
			nlapiLogExecution('DEBUG', 'After submit', 'ASN no==' + s_get_ASN_no)
			
			var s_get_ASN_value = nlapiGetFieldValue('total')
			nlapiLogExecution('DEBUG', 'After submit', 'ASN value==' + s_get_ASN_value)
			
			var d_get_ASN_date = nlapiGetFieldValue('trandate')
			nlapiLogExecution('DEBUG', 'After submit', 'ASN date==' + d_get_ASN_date)
			
			var o_create_cust_rec = nlapiCreateRecord('customrecord_customrecordasndetails')
			nlapiLogExecution('DEBUG', 'After submit', 'Custom rec created==' + o_create_cust_rec)
			
			if(o_create_cust_rec != null && o_create_cust_rec != 'undefined' && o_create_cust_rec != '')
			{
				if(s_get_parent_PO != null && s_get_parent_PO != '' && s_get_parent_PO != 'undefined')
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnno',s_get_parent_PO);
				}
				else
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnno','');
				}
				
				
                if(s_get_ASN_no != null && s_get_ASN_no != '' && s_get_ASN_no != 'undefined')
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnnoact',s_get_ASN_no);
				}
				else
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnnoact','');
				}
				

				if(s_get_ASN_value != null && s_get_ASN_value != '' && s_get_ASN_value != 'undefined')
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnvalue',s_get_ASN_value);
				}
				else
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasnvalue','');
				}
				
				if(d_get_ASN_date != null && d_get_ASN_date != '' && d_get_ASN_date != 'undefined')
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasndate',d_get_ASN_date);
				}
				else
				{
					o_create_cust_rec.setFieldValue('custrecord_customrecordasndate','');
				}
				
			}
			
			var id = nlapiSubmitRecord(o_create_cust_rec, false, false)
			nlapiLogExecution('DEBUG', 'after submit', 'submit id==' + id)
		}
			
			
	}
			
}	
    //END AFTER SUBMIT