//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_printIR.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:31-07-13
	 Version:0.1
	 Description:Print manual on item receipt
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 	22-08-13					   Kapil 							Ganesh								internal id of acc. qty and rec. qty is changed,tax code is fetched from IR instead of PO
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  itemreceipt_print(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET
	 
function itemreceipt_print(request,response)
{
	var param=request.getParameter('custscript_itemreceiptprint');
	nlapiLogExecution('DEBUG','IR','IR id ='+param);
	
	var o_IR_obj = nlapiLoadRecord('itemreceipt',param);       //Loading  IR record
	nlapiLogExecution('DEBUG','IR','IR object='+o_IR_obj);
	
	var s_dept = o_IR_obj.getFieldText('department');
	s_dept = nullcheckingfield(s_dept);
	nlapiLogExecution('DEBUG','IR','Dept='+s_dept);
	
	var s_supplier = o_IR_obj.getFieldText('entity');
	s_supplier = nullcheckingfield(s_supplier);
	nlapiLogExecution('DEBUG','IR','Supplier='+s_supplier);
	
	var s_supplier_doc_ref = o_IR_obj.getFieldValue('custbody_suffdocref');
	s_supplier_doc_ref = nullcheckingfield(s_supplier_doc_ref);
	nlapiLogExecution('DEBUG','IR','Supplier doc ref='+s_supplier_doc_ref);
	
	var d_supplier_doc_date = o_IR_obj.getFieldValue('custbody_suppdocdate');
	d_supplier_doc_date = nullcheckingfield(d_supplier_doc_date);
	nlapiLogExecution('DEBUG','IR','Supplier doc date='+d_supplier_doc_date);
	
	var s_GRN_no = o_IR_obj.getFieldValue('tranid');
	s_GRN_no = nullcheckingfield(s_GRN_no);
	nlapiLogExecution('DEBUG','IR','GRN no='+s_GRN_no);
	
	var s_order_no = o_IR_obj.getFieldText('createdfrom');
	s_order_no = nullcheckingfield(s_order_no);
	s_order_no = s_order_no.substring(16);
	nlapiLogExecution('DEBUG','IR','Order no='+s_order_no);
	
	
	
	var d_GRN_date = o_IR_obj.getFieldValue('trandate');
	d_GRN_date = nullcheckingfield(d_GRN_date);
	nlapiLogExecution('DEBUG','IR','GRN date='+d_GRN_date);
	
	var d_PO_date = o_IR_obj.getFieldValue('custbody_podate');
	d_PO_date = nullcheckingfield(d_PO_date);
	nlapiLogExecution('DEBUG','IR','Order date='+d_PO_date);
	
	/*
var i_order_no_val = o_IR_obj.getFieldValue('createdfrom');
	var d_order_date = '';
	if(i_order_no_val != null && i_order_no_val != '' && i_order_no_val != 'undefined')
	{
		var o_PO_obj = nlapiLoadRecord('purchaseorder',i_order_no_val)
		d_order_date = o_PO_obj.getFieldValue('trandate');
		nlapiLogExecution('DEBUG','IR','Order date='+d_order_date);
	}
	else
	{
		d_order_date = '';
	}
*/
	
	var s_tall_sheet_no = o_IR_obj.getFieldValue('custbody_tallysheetno');
	s_tall_sheet_no = nullcheckingfield(s_tall_sheet_no);
	nlapiLogExecution('DEBUG','IR','Tally sheet no='+s_tall_sheet_no);
	
	var s_GRN_val = o_IR_obj.getFieldValue('custbody_grnvalue');
	s_GRN_val = nullcheckingfield(s_GRN_val);
	nlapiLogExecution('DEBUG','IR','GRN val ='+s_GRN_val);
	
	var d_due_date = o_IR_obj.getFieldValue('custbody_duedate');
	d_due_date = nullcheckingfield(d_due_date);
	nlapiLogExecution('DEBUG','IR','Due date ='+d_due_date);
	
	
	var strVar="";
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td align=\"center\"><b>RADHAKRISHNA FOODLAND PVT. LTD.<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td align=\"center\"><b>KALAMBOLI LOCATION<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td align=\"center\"><b>"+s_dept+"<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td align=\"center\"><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td align=\"center\"><b>GOODS RECEIVED NOTE<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\" border=\"0.1\" border-color=\"#6666660\" style =\"font-size:7; border-left-style:none;border-bottom-style:none;border-right-style:none;\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"9%\"><b>Supplier&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"18%\"><b>"+s_supplier+"<\/b><\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "		<td width=\"14%\"><b>Supplier Doc Ref.&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"9%\"><b>"+s_supplier_doc_ref+"<\/b><\/td>";
	strVar += "		<td width=\"11%\">&nbsp;<\/td>";
	strVar += "		<td width=\"9%\"><b>GRN No.&nbsp;&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"10%\"><b>"+s_GRN_no+"<\/b><\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"9%\"><b>Order No.&nbsp;&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"18%\">"+s_order_no+"<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "		<td width=\"14%\"><b>Supplier Doc Date&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"9%\"><b>"+d_supplier_doc_date+"<\/b><\/td>";
	strVar += "		<td width=\"11%\">&nbsp;<\/td>";
	strVar += "		<td width=\"9%\"><b>GRN Date&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"10%\">"+d_GRN_date+"<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"9%\"><b>Order Date&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"18%\">"+d_PO_date+"<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "		<td width=\"14%\"><b>Tally Sheet No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"9%\"><b>"+s_tall_sheet_no+"<\/b><\/td>";
	strVar += "		<td width=\"11%\">&nbsp;<\/td>";
	strVar += "		<td width=\"9%\"><b>GRN Value&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"10%\"><b>"+s_GRN_val+"<\/b><\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"9%\"><b>Due Date&nbsp;&nbsp;&nbsp;&nbsp;:<\/b><\/td>";
	strVar += "		<td width=\"18%\">&nbsp;<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "		<td width=\"14%\">&nbsp;<\/td>";
	strVar += "		<td width=\"9%\">&nbsp;<\/td>";
	strVar += "		<td width=\"11%\">&nbsp;<\/td>";
	strVar += "		<td width=\"9%\">&nbsp;<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "		<td width=\"10%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
	strVar += "<table border=\"0.1\" width=\"100%\" style =\"font-size:5;border-left-style:none;border-right-style:none;border-bottom-style:none;\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"6%\">Product Code<\/td>";
	strVar += "		<td width=\"15%\">Product Description<\/td>";
	strVar += "		<td width=\"5%\">UOM<\/td>";
	strVar += "		<td width=\"8%\">MRP/MFG Date<\/td>";
	strVar += "		<td width=\"6%\">Expiry Date<\/td>";
	strVar += "		<td width=\"5%\">Rec.Qty<\/td>";
	strVar += "		<td width=\"5%\">Acc.Qty<\/td>";
	strVar += "		<td width=\"10%\">Warehouse/Zone/Bin<\/td>";
	strVar += "		<td width=\"6%\">Basic Rate<\/td>";
	strVar += "		<td width=\"10%\">Basic Value<\/td>";
	strVar += "		<td width=\"8%\">%<\/td>";
	strVar += "		<td width=\"8%\">Value<\/td>";
	strVar += "		<td width=\"8%\">Total Value<\/td>";
	strVar += "	<\/tr>";
	
	
    var tot_rec_qty = 0;
	var tot_acc_qty = 0;
	var tot_basic_val = 0.00;
	var tot_val = 0;
	var tot_total_val = 0;
	var i_linecount = o_IR_obj.getLineItemCount('item')
	if(i_linecount !=null && i_linecount != '' && i_linecount !='undefined')
	{
		for (var i = 1; i <= i_linecount; i++) 
		{
			var s_pr_code = o_IR_obj.getLineItemValue('item', 'itemname', i);
			s_pr_code = nullcheckingfield(s_pr_code);
			nlapiLogExecution('DEBUG', 'IR', 'Prod Code=' + s_pr_code);
			
			var s_pr_desc = o_IR_obj.getLineItemValue('item', 'description', i);
			s_pr_desc = nullcheckingfield(s_pr_desc);
			nlapiLogExecution('DEBUG', 'IR', 'Prod Desc=' + s_pr_desc);
			
			var s_units = o_IR_obj.getLineItemValue('item', 'unitsdisplay', i);
			s_units = nullcheckingfield(s_units);
			nlapiLogExecution('DEBUG', 'IR', 'Units=' + s_units);
			
			var s_mrp = o_IR_obj.getLineItemValue('item', 'custcol_mrpmfgdate', i);
			s_mrp = nullcheckingfield(s_mrp);
			nlapiLogExecution('DEBUG', 'IR', 'MRP/MFG=' + s_mrp);
			
			var d_expiry_date = o_IR_obj.getLineItemValue('item', 'custcol_expirydate', i);
			d_expiry_date = nullcheckingfield(d_expiry_date);
			nlapiLogExecution('DEBUG', 'IR', 'expiry_date=' + d_expiry_date);
			
			var i_rec_qty = o_IR_obj.getLineItemValue('item', 'quantityremaining', i);//onhand
			//i_rec_qty = nullcheckingfield(i_rec_qty);
			if(i_rec_qty != null && i_rec_qty != '' && i_rec_qty != 'undefined')
			{
				i_rec_qty = i_rec_qty;
			}
			else
			{
				i_rec_qty = 0.00
			}
			nlapiLogExecution('DEBUG', 'IR', 'rec qty=' + i_rec_qty);
			
			var i_acc_qty = o_IR_obj.getLineItemValue('item', 'custcol_accqty', i);//itemquantity
			//i_acc_qty = nullcheckingfield(i_acc_qty);
			if(i_acc_qty != null && i_acc_qty != '' && i_acc_qty != 'undefined')
			{
				i_acc_qty = i_acc_qty;
			}
			else
			{
				i_acc_qty = 0.00
			}
			nlapiLogExecution('DEBUG', 'IR', 'acc qty=' + i_acc_qty);
			
			var s_warehouse_zone_bin = o_IR_obj.getLineItemValue('item', 'custcol_warezonebin', i);
			s_warehouse_zone_bin = nullcheckingfield(s_warehouse_zone_bin);
			nlapiLogExecution('DEBUG', 'IR', 'warehouse=' + s_warehouse_zone_bin);
			
			var i_basic_rate = o_IR_obj.getLineItemValue('item', 'custcol_basicrate', i);
			i_basic_rate = nullcheckingfield(i_basic_rate);
			nlapiLogExecution('DEBUG', 'IR', 'basic_rate=' + i_basic_rate);
			
			var i_basic_value = o_IR_obj.getLineItemValue('item', 'custcol_basicvalue', i);
			//i_basic_value = nullcheckingfield(i_basic_value);
			if(i_basic_value != null && i_basic_value != '' && i_basic_value != 'undefined')
			{
				i_basic_value = i_basic_value;
			}
			else
			{
				i_basic_value = 0.00
			}
			nlapiLogExecution('DEBUG', 'IR', 'basic_value=' + i_basic_value);
			
			
            var i_rate = o_IR_obj.getLineItemText('item', 'custcol_taxrate', i);
			//nlapiLogExecution('DEBUG', 'IR', 'RATE=' + i_rate);
			i_rate = nullcheckingfield(i_rate);
			nlapiLogExecution('DEBUG', 'IR', 'percentage=' + i_rate);

            //var i_rate = gettaxcode(s_pr_code);
			//nlapiLogExecution('DEBUG', 'IR', 'rate=' + i_rate);
			
			var i_value = o_IR_obj.getLineItemValue('item', 'custcol_value', i);
			//i_value = nullcheckingfield(i_value);
			if(i_value != null && i_value != '' && i_value != 'undefined')
			{
				i_value = i_value;
			}
			else
			{
				i_value = 0.00
			}
			nlapiLogExecution('DEBUG', 'IR', 'value=' + i_value);
			
			var i_total_value = o_IR_obj.getLineItemValue('item', 'custcol_totalvalue', i);
			//i_total_value = nullcheckingfield(i_total_value);
			if(i_total_value != null && i_total_value != '' && i_total_value != 'undefined')
			{
				i_total_value = i_total_value;
			}
			else
			{
				i_total_value = 0.00
			}
			nlapiLogExecution('DEBUG', 'IR', 'Total value=' + i_total_value);
			
			tot_rec_qty = parseFloat(tot_rec_qty) + parseFloat(i_rec_qty) ;
			if (tot_rec_qty != null && tot_rec_qty != '' && tot_rec_qty != 'undefined' && !isNaN(tot_rec_qty)) 
			{
				tot_rec_qty = tot_rec_qty.toFixed(2);
			}
			else 
			{
				tot_rec_qty = '';
			}
			nlapiLogExecution('DEBUG', 'IR', 'tot_rec_qty=' + tot_rec_qty);
			
			tot_acc_qty = parseFloat(tot_acc_qty) + parseFloat(i_acc_qty) ;
			if (tot_acc_qty != null && tot_acc_qty != '' && tot_acc_qty != 'undefined' && !isNaN(tot_acc_qty)) 
			{
				tot_acc_qty = tot_acc_qty.toFixed(2);
			}
			else 
			{
				tot_acc_qty = '';
			}
			nlapiLogExecution('DEBUG', 'IR', 'tot_acc_qty=' + tot_acc_qty);
			
			tot_basic_val = parseFloat(tot_basic_val) + parseFloat(i_basic_value) ;
			//tot_basic_val = tot_basic_val + i_basic_value ;
			nlapiLogExecution('DEBUG', 'IR', 'i_basic_value=' + i_basic_value);
			nlapiLogExecution('DEBUG', 'IR', 'tot_basic_val=' + tot_basic_val);
			//nlapiLogExecution('DEBUG', 'IR', 'tot_basic_val=' + tot_basic_val);
			if (tot_basic_val != null && tot_basic_val != '' && tot_basic_val != 'undefined' && !isNaN(tot_basic_val)) 
			{
				//nlapiLogExecution('DEBUG', 'IR', 'In print basic value');
				tot_basic_val = tot_basic_val.toFixed(2);
			}
			else 
			{
				tot_basic_val = '';
			}
			//nlapiLogExecution('DEBUG', 'IR', 'tot_basic_val=' + tot_basic_val);
			
			tot_val = parseFloat(tot_val) + parseFloat(i_value) ;
			if (tot_val != null && tot_val != '' && tot_val != 'undefined' && !isNaN(tot_val)) 
			{
				tot_val = tot_val.toFixed(2);
			}
			else 
			{
				tot_val = '';
			}
			nlapiLogExecution('DEBUG', 'IR', 'tot_val=' + tot_val);
			
			tot_total_val = parseFloat(tot_total_val) + parseFloat(i_total_value) ;
			if (tot_total_val != null && tot_total_val != '' && tot_total_val != 'undefined' && !isNaN(tot_total_val)) 
			{
				tot_total_val = tot_total_val.toFixed(2);
			}
			else 
			{
				tot_total_val = '';
			}
			nlapiLogExecution('DEBUG', 'IR', 'tot_total_val=' + tot_total_val);
			
			
			strVar += "	<tr border=\"0.1\" style =\"border-left-style:none;border-right-style:none;border-bottom-style:none;\">";
			strVar += "		<td width=\"6%\">" + s_pr_code + "<\/td>";
			strVar += "		<td width=\"15%\">"+s_pr_desc+"<\/td>";
			strVar += "		<td width=\"5%\">"+s_units+"<\/td>";
			strVar += "		<td width=\"8%\">"+s_mrp+"<\/td>";
			strVar += "		<td width=\"6%\">"+d_expiry_date+"<\/td>";
			strVar += "		<td width=\"5%\">"+i_rec_qty+"<\/td>";
			strVar += "		<td width=\"5%\">"+i_acc_qty+"<\/td>";
			strVar += "		<td width=\"10%\">"+s_warehouse_zone_bin+"<\/td>";
			strVar += "		<td width=\"6%\">"+i_basic_rate+"<\/td>";
			strVar += "		<td width=\"10%\">"+i_basic_value+"<\/td>";
			strVar += "		<td width=\"8%\">"+i_rate+"<\/td>";
			strVar += "		<td width=\"8%\">"+i_value+"<\/td>";
			strVar += "		<td width=\"8%\">"+i_total_value+"<\/td>";
			strVar += "	<\/tr>";
		}
	}
	strVar += "	<tr border=\"0.1\" style =\"border-left-style:none;border-right-style:none;border-bottom-style:none;\">";
	strVar += "		<td width=\"6%\"><\/td>";
	strVar += "		<td width=\"15%\"><b>Total-<\/b><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"8%\"><\/td>";
	strVar += "		<td width=\"6%\"><\/td>";
	strVar += "		<td width=\"5%\"><b>"+tot_rec_qty+"<\/b><\/td>";
	strVar += "		<td width=\"5%\"><b>"+tot_acc_qty+"<\/b><\/td>";
	strVar += "		<td width=\"10%\"><\/td>";
	strVar += "		<td width=\"6%\"><\/td>";
	strVar += "		<td width=\"10%\"><b>"+tot_basic_val+"<\/b><\/td>";
	strVar += "		<td width=\"8%\"><\/td>";
	strVar += "		<td width=\"8%\"><b>"+tot_val+"<\/b><\/td>";
	strVar += "		<td width=\"8%\"><b>"+tot_total_val+"<\/b><\/td>";
	strVar += "	<\/tr>";
	
    /*
strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
*/

	
	
    strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";

	/*
strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
*/
	strVar += "<\/table>";
	
	
	
	var i_total_lines = 115;
	var blank_lines = '';
	if(i_linecount < i_total_lines)
	{
		blank_lines = i_total_lines - i_linecount
	}
	else if(i_linecount > i_total_lines)
	{
		blank_lines = 2;
	}
	else if(i_linecount==i_total_lines)
	{
		 blank_lines = 0;
	}
    nlapiLogExecution('DEBUG','Purchase order','blank_lines='+blank_lines);
	
	
	
	strVar += "<table width=\"100%\" style =\"font-size:6;\">";
	
	for (var k = 1; k <= blank_lines; k++) 
		{
			//nlapiLogExecution('DEBUG', 'Purchase order item', 'blank_lines in for=' + blank_lines);
			strVar += "	<tr>";
			strVar += "		<td width=\"10%\"><b><\/b><\/td>";
	        strVar += "		<td width=\"15%\"><\/td>";
	        strVar += "		<td width=\"12%\"><b><\/b><\/td>";
	        strVar += "		<td width=\"63%\"><\/td>";
			strVar += "	<\/tr>";
		}
	
	/*
strVar += "	<tr>";
	strVar += "		<td width=\"10%\"><b>Received By<\/b><\/td>";
	strVar += "		<td width=\"15%\"><\/td>";
	strVar += "		<td width=\"12%\"><b>Warehouse Supervisor<\/b><\/td>";
	strVar += "		<td width=\"63%\"><\/td>";
*/
	/*
    strVar += "		<td width=\"6%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"10%\"><\/td>";
	strVar += "		<td width=\"6%\"><\/td>";
	strVar += "		<td width=\"10%\"><\/td>";
	strVar += "		<td width=\"8%\"><\/td>";
	strVar += "		<td width=\"8%\"><\/td>";
	strVar += "		<td width=\"8%\"><\/td>";
*/
    //strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"10%\"><b>Received By<\/b><\/td>";
	strVar += "		<td width=\"15%\"><\/td>";
	strVar += "		<td width=\"12%\"><b>Warehouse Supervisor<\/b><\/td>";
	strVar += "		<td width=\"63%\"><\/td>";
    strVar += "	<\/tr>";
	
	strVar += "<\/table>";
	
	strVar += "";
	
	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
    var file = nlapiXMLToPDF(xml);
    response.setContentType('PDF','ASN.pdf', 'inline');
    response.write(file.getValue());
}
	 
function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	 

function formatAndReplaceCommaofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/,/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
//&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 return messgaeToBeSendPara;
}

/*
function gettaxcode(s_pr_code)
{
	nlapiLogExecution('DEBUG', 'In taxcode search', 's_pr_code=' + s_pr_code);
	var filters = new Array();
	var columns = new Array();
	
	filters[0] = new nlobjSearchFilter('item', null, 'is', s_pr_code);
	columns[0] = new nlobjSearchColumn('taxcode');
	//columns[1] = new nlobjSearchColumn('taxcode_display');
	
	var result = nlapiSearchRecord('purchaseorder', null, filters, columns);
	nlapiLogExecution('DEBUG', 'In taxcode search', 'result=' + result);
	if (result != null) 
	{
		var tax_code = result[0].getText('taxcode');
		nlapiLogExecution('DEBUG', 'In taxcode search', 'taxcode=' + tax_code);
		
		return tax_code;
	}
	else
	{
		return null;
	}
	
}
*/
