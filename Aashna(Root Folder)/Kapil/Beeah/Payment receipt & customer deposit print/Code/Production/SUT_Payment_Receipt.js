//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_Payment_Receipt.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:29-04-13
	 Version:0.1
	 Description:Print payment receipt on customer payment
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	   13-09-13						Kapil								Parag								Script deployed for customer deposit  record also
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  payment_receipt(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET

function payment_receipt(request, response)
{
	/* Suitelet:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	//var form=nlapiCreateForm('Sales Order Summary');
	//var id=nlapiGetRecordId();
	
//===========Getting the body fields of Customer Payment==============
	
	var param_rec_type = request.getParameter('custscript_payment_rec_type');
	nlapiLogExecution('DEBUG','Payment receipt','Payment type='+param_rec_type);
	
	var param=request.getParameter('custscript_payment_param');
	nlapiLogExecution('DEBUG','Payment receipt','Payment Id='+param);
	
	var customer_pay = nlapiLoadRecord(param_rec_type,param); 
	//var customer_pay = nlapiLoadRecord('customerpayment',param);       //Loading customer record
	nlapiLogExecution('DEBUG','Payment receipt','customer payment='+customer_pay);
	
	var date = customer_pay.getFieldValue('trandate');                 //Getting Date
	nlapiLogExecution('DEBUG','Payment receipt','date='+date);
	
	if (date!= null && date!= '' && date!= 'undefined' ) 
    {
     date = date;
    }
    else 
    {
     date= '';
    }
	if (param_rec_type == 'customerpayment') 
	{
		var payment_no = customer_pay.getFieldValue('custbody_payment');
		//var payment_no = customer_pay.getFieldValue('custbody_payment_number');
		if (payment_no != null && payment_no != '' && payment_no != 'undefined') 
		{
			//receive_from = receive_from;  only_name
			payment_no = payment_no;
		}
		else 
		{
			payment_no = '';
		}
	}
	
	else
	{
		var payment_no = customer_pay.getFieldValue('tranid');
		if (payment_no != null && payment_no != '' && payment_no != 'undefined') 
		{
			//receive_from = receive_from;  only_name
			payment_no = payment_no;
		}
		else 
		{
			payment_no = '';
		}
	}
	
	nlapiLogExecution('DEBUG','Payment receipt','payment_no='+payment_no);
	
	var receive_from = customer_pay.getFieldText('customer');         //Getting customer name
	nlapiLogExecution('DEBUG','Payment receipt','receive_from='+receive_from);
	
	var only_cust_name = getname(nlapiEscapeXML(receive_from));
	nlapiLogExecution('DEBUG','Payment receipt','only_name='+only_cust_name);
	
	if (receive_from!= null && receive_from!= '' && receive_from!= 'undefined' ) 
    {
     //receive_from = receive_from;  only_name
	 receive_from=only_cust_name;
	 receive_from=formatAndReplaceMessageForAnd(receive_from)
	 nlapiLogExecution('DEBUG','Payment receipt','receive from in if='+receive_from);
	 
    }
    else 
    {
     receive_from= '';
    }
	
	var sum_Dhs = customer_pay.getFieldValue('payment');
	nlapiLogExecution('DEBUG','Payment receipt','sum_Dhs='+sum_Dhs);
	
	if (sum_Dhs!= null && sum_Dhs!= '' && sum_Dhs!= 'undefined' ) 
    {
     sum_Dhs = sum_Dhs;
	 var Rs_with_comma = numberWithCommas(sum_Dhs)
    }
    else 
    {
     sum_Dhs= '';
    }
	var amt = sum_Dhs.split('.');
	//var Rs_amt = amt[0];
	var Paise_amt = amt[1];
	nlapiLogExecution('DEBUG','Payment receipt','Paise_amt='+Paise_amt);
	
	var amt_in_words = toWordsFunc(sum_Dhs);
	
	var cheque_no = customer_pay.getFieldValue('checknum');             //Getting cheque number
	nlapiLogExecution('DEBUG','Payment receipt','cheque_no='+cheque_no);
	
	if (cheque_no != null && cheque_no != '' && cheque_no != 'undefined') 
	 {
		cheque_no = cheque_no;
	 }
	else
	   {
		cheque_no = '';
	   }
	
	var chkbox_cheque = customer_pay.getFieldValue('custbody_checkpaymentreceipt');
	var chkbox_cash = customer_pay.getFieldValue('custbody_cashpaymentreceipt');
	var chkmode = '';
	if (chkbox_cheque == 'T') 
	{
	  chkmode = 'Cheque';
	}
	else if(chkbox_cash =='T')
	{
	  chkmode = 'Cash';
	}
	
	var bank_name = customer_pay.getFieldValue('custbody_bankname');               //Getting bank name
	nlapiLogExecution('DEBUG','Payment receipt','Bank name='+bank_name);
	if (bank_name!= null && bank_name!= '' && bank_name!= 'undefined' ) 
    {
     bank_name = bank_name;
    }
    else 
    {
     bank_name= '';
    }
	var cheque_date = customer_pay.getFieldValue('custbody_chequedatepaymentreceipt');
	if (cheque_date!= null && cheque_date!= '' && cheque_date!= 'undefined' ) 
    {
     cheque_date = cheque_date;
    }
    else 
    {
     cheque_date= '';
    }
	//var account = customer_pay.getFieldText('account');               //Getting account
	//nlapiLogExecution('DEBUG','Payment receipt','account='+account);
	
	//var only_bank_name = getname(account);
	//nlapiLogExecution('DEBUG','Payment receipt','only_name='+only_bank_name);
	
	/*
if (account!= null && account!= '' && account!= 'undefined' ) 
    {
     account = only_bank_name;
    }
    else 
    {
     account= '';
    }
*/
	
	var being = customer_pay.getFieldValue('memo');                    //Getting memo
	nlapiLogExecution('DEBUG','Payment receipt','being='+being);
	
	if (being!= null && being!= '' && being!= 'undefined') 
    {
     being = being;
    }
    else 
    {
     being= '';
    }
	
	var top_img_url = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=84&c=3591874&h=c68406a5abf72e7c2ca2';
	var URL = nlapiEscapeXML(top_img_url);
	
	var bottom_img_url = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=111&c=3591874&h=c6175274c08ed72f4309';
	var bottom_URL = nlapiEscapeXML(bottom_img_url);
	
	var strVar="";
	
	//*****************Table design for top image***************//
	
	strVar += "<table align=\"center\" width=\"68%\">";
	strVar += "		<tr>";
	strVar += "		<td>";
    strVar += "		<img  width=\"400px\" height=\"90px\" align=\"center\" src=\"" + URL + " \" \/><\/td>";
	strVar += "		<\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "	<\/table>";
    


    //*****************Table design for date and serial no***************//
	
	strVar += "<table width=\"100%\">";
	strVar += "		<tr>";
	strVar += "			<td width=\"668\"><b style=\"font-size:12\">Date :&nbsp;&nbsp;&nbsp;"+date+"<\/b><\/td>";
	//strVar += "			<td width=\"668\" style=\"font-size:9\"><b>Date&nbsp;&nbsp;&nbsp;<\/b>"+date+"<\/td>";
	strVar += "			<td align=\"center\" width=\"668\" style=\"font-size:12\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Serial No. :&nbsp;&nbsp;&nbsp;<\/b><b style=\"font-size:14\">"+payment_no+"<\/b><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<\/table>";
	
	
    //*****************Table design for main table and sub-tables***************//
	
	strVar += "<table border=\"0.1\" corner-radius=\"1%\" width=\"100%\" >";
	strVar += " <tr>";
	strVar += "  <td>";
	strVar += "  <table border=\"0\" width=\"100%\">";
	strVar += "   <tr background-color =\"#666666\">";
	//strVar += "    <td height=\"28\" width=\"4%\" align=\"center\" style=\" solid #666666 ; \">Receipt<\/td>";
	strVar += "    <td align=\"center\" style=\"color:white;font-size:16\"><b>Receipt Voucher<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
	

    strVar += "<table width=\"100%\">";
    strVar += "   <tr>";
    strVar += "    <td>&nbsp;<\/td>";
    strVar += "   <\/tr>";
    strVar += "  <\/table>";

	
	strVar += "  <table border=\"0.1\" width=\"54%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"35%\" height=\"10\" border=\"0.1\" style= \"font-size:12; border-bottom-style:none;border-top-style:none;\"><b>Dhs. :&nbsp;&nbsp;&nbsp;"+Rs_with_comma+"<\/b><\/td>";
	strVar += "		<td width=\"20%\" height=\"10\" style =\"font-size:12;\"><b>Fil(s) :&nbsp;&nbsp;&nbsp;"+Paise_amt+"<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "  <\/table>";
	
	strVar += "	<br/>";
	
    /*
strVar += "<table width=\"100%\">";
    strVar += "   <tr>";
    strVar += "    <td>&nbsp;<\/td>";
    strVar += "   <\/tr>";
    strVar += "  <\/table>";
*/

		
	
	strVar += "  <table  width=\"100%\">";
	strVar += "   <tr>";
	strVar += "    <td colspan=\"2\" ><b style =\"font-weight:500;font-size:12;\">Received From :&nbsp;&nbsp;&nbsp;<\/b>";
	strVar += "  <b style =\"font-size:12;\">"+receive_from+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
	
	strVar += "  <table  width=\"100%\">";
	if(Paise_amt>0)
	{
	strVar += "   <tr>";
	strVar += "    <td colspan=\"2\" style =\"font-size:12;\"><b style =\"font-size:12;\">Amount in Words :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+amt_in_words+" Fil(s) Only<\/b><\/td>";
	strVar += "   <\/tr>";
	}
	else
	{
	strVar += "   <tr>";
	strVar += "    <td colspan=\"2\" style =\"font-size:12;\"><b style =\"font-size:12;\">Amount in Words :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+amt_in_words+" Only<\/b><\/td>";
	strVar += "   <\/tr>";
	}
	
    strVar += "   <tr>";
	strVar += "    <td colspan=\"2\">&nbsp;<\/td>";
	strVar += "   <\/tr>";
    strVar += "  <\/table>";
	
	
	strVar += "  <table  width=\"100%\">";
	strVar += "   <tr>";
	strVar += "    <td width=\"50%\" style =\"font-size:12;\"><b>Mode of Payment :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+chkmode+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td align=\"left\" width=\"48%\" style =\"font-size:12;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cheque No. :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+cheque_no+"<\/b><\/td>";
	strVar += "    <td align=\"left\" width=\"62%\" style =\"font-size:12;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cheque Date :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+cheque_date+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td align=\"left\" width=\"68%\" style =\"font-size:12;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bank Name :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+bank_name+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	
	strVar += "   <tr>";
	strVar += "    <td colspan=\"2\" style =\"font-size:12;\"><b>Payment For :<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+being+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
	

    strVar += "<table width=\"100%\">";
    strVar += "   <tr>";
    strVar += "    <td>&nbsp;<\/td>";
    strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
    strVar += "  <\/table>";

	
	strVar += "  <table width=\"100%\">";
	strVar += "   <tr>";
	strVar += "    <td style=\"font-size:12\"><b>Received by :<span class=\"hps\"><\/span><\/b><\/td>";
	strVar += "    <td align=\"center\" style= \"font-size:12\"><b>Checked by :<span class=\"hps\"><\/span><\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
	
	
	strVar += "  <\/td>";
	//strVar += "  <p>&nbsp;<\/td>";
	strVar += " <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "<\/table>";
	
    //*****************Main table ends***************//
	
	//*****************Table design for dots and bottom image***************//
	
	strVar += "  <table width=\"100%\">";
	strVar += " <tr>";
	strVar += "  <td><b style=\"font-size:9\">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .<\/b> <\/td>";
	//strVar += "  . . .<\/b><img  width=\"30px\" height=\"35px\" align=\"right\" src=\"" + bottom_URL + " \" \/> <\/td>";
	strVar += "  <td> <img  width=\"20px\" height=\"25px\" align=\"right\" src=\"" + bottom_URL + " \" \/> <\/td>";
	strVar += " <\/tr>";
	strVar += " <tr>";
	strVar += "  <td><b style=\"font-size:12\">beeah.ae<\/b><\/td>";
	strVar += " <\/tr>";
	strVar += "  <\/table>";
	
    /*
strVar += "<table width=\"100%\">";
	strVar += " <tr>";
	strVar += "  <td><b style=\"font-size:10\">beeah.ae<\/b><\/td>";
	strVar += " <\/tr>";
	strVar += "<\/table>";

*/

	strVar += "";

//================Converting XML to PDF===============

	   var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
       var file = nlapiXMLToPDF(xml);
       response.setContentType('PDF','Payment receipt.pdf', 'inline');
       response.write(file.getValue());
}

     //END SUITELET
	 
//================Function to show only string on fields=============
	 
function getname(receive_from)
{
	var name = receive_from.split(' ');
	var newname='';
	nlapiLogExecution('DEBUG','function body','name'+name[0]);
	
	if(name.length!=0)
	{
		for(var i=1;i< name.length;i++)
		{
			newname = newname +' '+ name[i];
		}
	}
	
	return newname;
}
	
function numberWithCommas(sum_Dhs) 
{
	var amt = sum_Dhs.split('.');
	var Rs_amt = amt[0];
	
    Rs_amt = Rs_amt.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(Rs_amt))
    Rs_amt = Rs_amt.replace(pattern, "$1,$2");
    return Rs_amt;
}
	 


function toWordsFunc(s)
{
   // alert('In conversion function')
    var str=''

    var th  = new Array ('Crore ','Hundred ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}	 

function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Messgae with and =====" + messgaeToBeSendParaAnd);
 
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
 
  return messgaeToBeSendParaAnd;
} 	 