// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI discount calculate
	Author:Kapil Srivastava
	Company:Aashna Cloudtech
	Date:20/05/13
    Description:Calculation of discount


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
                       
     20/05/13              Kapil                          Parag                            Functionality added for expense sublist also

	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- disc_fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================
function pageInit(type)
{

	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES


	//  PAGE INIT CODE BODY

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================
function saveRecord()
{
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	    Date                 trandate
	 */
	//  LOCAL VARIABLES



	//  SAVE RECORD CODE BODY

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{
	/*  On validate field:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY

return true;
}	



// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function disc_fieldChanged(type, name,linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY
if (name == 'custcol_discount' && type == 'item')  
 {
	var pre_gross_amt = nlapiGetCurrentLineItemValue('item', 'grossamt');
	//alert('pre_gross_amt' + pre_gross_amt)
	
	var disc_in_percent = nlapiGetCurrentLineItemValue('item', 'custcol_discount');
	//alert('disc_in_percent' + disc_in_percent);
	
	var disc_fig_only = disc_in_percent.split('%');
	//alert('disc_fig_only' + disc_fig_only[0]);
	
	var disc_amt_calculate = (disc_fig_only[0] / 100) * pre_gross_amt;
	//alert('disc_amt_calculate' + disc_amt_calculate)
	
	var disc_amt_set = nlapiSetCurrentLineItemValue('item', 'custcol_discamount', disc_amt_calculate,false);
	//alert('disc_amt_set==' + disc_amt_set)
	
	var disc_amt_get = nlapiGetCurrentLineItemValue('item','custcol_discamount');
	//alert('disc_amt_get==' + disc_amt_get);
	
	var post_gross_amt = pre_gross_amt - disc_amt_calculate;
	//alert('post_gross_amt' + post_gross_amt)
	
	var post_gross_amt_set = nlapiSetCurrentLineItemValue('item', 'grossamt', post_gross_amt, false);
	//alert('post_gross_amt_set' + post_gross_amt_set);
		

 }	
 
 if (name == 'custcol_discount' && type == 'expense')  
 {
	var pre_gross_amt = nlapiGetCurrentLineItemValue('expense', 'grossamt');
	//alert('pre_gross_amt' + pre_gross_amt)
	
	var disc_in_percent = nlapiGetCurrentLineItemValue('expense', 'custcol_discount');
	//alert('disc_in_percent' + disc_in_percent);
	
	var disc_fig_only = disc_in_percent.split('%');
	//alert('disc_fig_only' + disc_fig_only[0]);
	
	var disc_amt_calculate = (disc_fig_only[0] / 100) * pre_gross_amt;
	//alert('disc_amt_calculate' + disc_amt_calculate)
	
	var disc_amt_set = nlapiSetCurrentLineItemValue('expense', 'custcol_discamount', disc_amt_calculate,false);
	//alert('disc_amt_set==' + disc_amt_set)
	
	var disc_amt_get = nlapiGetCurrentLineItemValue('expense','custcol_discamount');
	//alert('disc_amt_get==' + disc_amt_get);
	
	var post_gross_amt = pre_gross_amt - disc_amt_calculate;
	//alert('post_gross_amt' + post_gross_amt)
	
	var post_gross_amt_set = nlapiSetCurrentLineItemValue('expense', 'grossamt', post_gross_amt, false);
	//alert('post_gross_amt_set' + post_gross_amt_set);
		
 }	
 
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
    var total_discount = 0;

	//  RECALC CODE BODY
	if(type == 'item')
	var disc_amt_get = nlapiGetCurrentLineItemValue('item','custcol_discamount');
	var disc_amt='';
	{
		var linecount = nlapiGetLineItemCount('item');
		//alert('linecount' + linecount);
		
		
		//alert('current_disc' + current_disc);
		
		for (var i = 1; i <= linecount; i++) 
		{
			disc_amt = nlapiGetLineItemValue('item', 'custcol_discamount', i)
			//alert('disc_amt' + disc_amt);
			
			if (disc_amt != null && disc_amt != '' && disc_amt != 'undefined') //&& total_discount != null && total_discount != '' && total_discount != 'undefined') 
			{
				total_discount = parseFloat(disc_amt) + parseFloat(total_discount);
			    //alert('total_discount in loop=' + total_discount);
			}
			
		}
		
		if (disc_amt_get != null && disc_amt_get != '' && disc_amt_get != 'undefined') 
  		  {
   			nlapiSetFieldValue('custbody_discounttotal', total_discount, false);
	        //alert('total discount from all items after= ' + total_discount);
  		  }
  	    else if(disc_amt != null && disc_amt != '' && disc_amt != 'undefined')
  		  {
   			nlapiSetFieldValue('custbody_discounttotal', total_discount, false);
	        //alert('total discount from all items after= ' + total_discount);
  		  }	
		
		
	}
	
	if(type == 'expense')
	var disc_amt_get = nlapiGetCurrentLineItemValue('expense','custcol_discamount');
	var disc_amt='';
	{
		var linecount = nlapiGetLineItemCount('expense');
		//alert('linecount expense===' + linecount);
		//var total_discount = 0;
		
		//alert('current_disc' + current_disc);
		
		for (var i = 1; i <= linecount; i++) 
		{
			disc_amt = nlapiGetLineItemValue('expense', 'custcol_discamount', i)
			//alert('disc_amt' + disc_amt);
			
			if (disc_amt != null && disc_amt != '' && disc_amt != 'undefined') //&& total_discount != null && total_discount != '' && total_discount != 'undefined') 
			{
				total_discount = parseFloat(disc_amt) + parseFloat(total_discount);
			    //alert('total_discount in loop=' + total_discount);
			}
			
		}
		
		if (disc_amt_get != null && disc_amt_get != '' && disc_amt_get != 'undefined') 
  		  {
   			nlapiSetFieldValue('custbody_discounttotal', total_discount, false);
	        //alert('total discount from all items after= ' + total_discount);
  		  }
  	    else if(disc_amt != null && disc_amt != '' && disc_amt != 'undefined')
  		  {
   			nlapiSetFieldValue('custbody_discounttotal', total_discount, false);
	       //alert('total discount from all items after= ' + total_discount);
  		  }	
		
		
	}

	
	
	

}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================





















