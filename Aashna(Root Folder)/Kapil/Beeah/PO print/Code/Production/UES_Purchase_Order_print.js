//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_Purchase_order_manual.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:23-05-13
	 Version:0.1
	 Description:Print manual on Purchase Order
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 purchase_order_beforeload(type,form)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY
function purchase_order_beforeload(type,form)
{
	if (type == 'view') 
	{
		var po_id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'PDF', 'Record Id=' + po_id);
		if (po_id != null) 
		{
			nlapiLogExecution('DEBUG', 'PDF', 'If rec id is not null,rec Id=' + po_id)
			var po_record = nlapiLoadRecord('purchaseorder', po_id);		
		}
		form.setScript('customscript_cli_purchase_order_print');
		form.addButton('custpage_button_print', 'Print', 'callPurchaseorder(\'' + po_id + '\');');
		nlapiLogExecution('DEBUG', 'PDF', 'Button added')
	}
			return true;	
	
}
    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
	 
	 /* On After Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN AFTER SUBMIT CODE BODY

	
    //END AFTER SUBMIT