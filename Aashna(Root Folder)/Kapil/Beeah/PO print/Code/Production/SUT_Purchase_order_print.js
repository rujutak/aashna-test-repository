//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_Purchase_order_print.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:23-05-13
	 Version:0.1
	 Description:Print manual on purchase order
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 27/05/13                      Kapil                             Parag                                 Line item name AED is changed to Line item, same for Amount field
	 28/05/13                      Kapil							 Parag								   HS code name is changed to Item No./Part No.,Line total to Total and Total is calculating instead of getting from gross amount.	
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  purchase_order_manual(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET

function purchase_order_manual(request, response)
{
	/* Suitelet:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	
	//nlapiLogExecution('DEBUG','Purchase order','Purchase order id=');
	var param=request.getParameter('custscript_purchaseorder_param');
	nlapiLogExecution('DEBUG','Purchase order','Purchase order id ='+param);
	
	var img_url = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=85&c=3591874&h=725a2bad29253944e736';
	var URL = nlapiEscapeXML(img_url);
	
	
	var PO_obj = nlapiLoadRecord('purchaseorder',param);       //Loading  purchaseorder record
	nlapiLogExecution('DEBUG','Purchase order','purchaseorder='+PO_obj);
	
	var PO_no= PO_obj.getFieldValue('tranid');
	nlapiLogExecution('DEBUG','Purchase order','PO_no='+PO_no);
	
	var actual_PO_no = nullcheckingfield(PO_no);
	
	
	
	var vendor= PO_obj.getFieldValue('entity');
	nlapiLogExecution('DEBUG','Purchase order','vendor='+vendor);
	
	var actual_vendor = nullcheckingfield(vendor);
	
	
	
	var Vendor_Obj = nlapiLoadRecord('vendor',actual_vendor);       //Loading  vendor record
	nlapiLogExecution('DEBUG','Purchase order','Vendor_Obj='+Vendor_Obj);
	
	
	var vendor_Address = Vendor_Obj.getFieldValue('defaultaddress');
	nlapiLogExecution('DEBUG','Purchase order','vendor_Address='+vendor_Address);
	
	var actual_vendor_Address = nullcheckingfield(vendor_Address);
	
	
	var format_vendor_Address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(actual_vendor_Address));
	
	var vendor_phone = Vendor_Obj.getFieldValue('phone');
	nlapiLogExecution('DEBUG','Purchase order','vendor_phone='+vendor_phone);
	
	var actual_vendor_phone = nullcheckingfield(vendor_phone);
	
	
	
	
	var vendor_fax = Vendor_Obj.getFieldValue('fax');
	nlapiLogExecution('DEBUG','Purchase order','vendor_fax='+vendor_fax);
	
	var actual_vendor_fax = nullcheckingfield(vendor_fax);
	
	
	
	var vendor_mail = Vendor_Obj.getFieldValue('email');
	nlapiLogExecution('DEBUG','Purchase order','vendor_mail='+vendor_mail);
	
	var actual_vendor_mail = nullcheckingfield(vendor_mail);
	
	
	
	var loc = PO_obj.getFieldValue('location');
	nlapiLogExecution('DEBUG','Purchase order','vendor='+loc);
	
	if (loc != null && loc != '' && loc != 'undefined') 
	{
		loc = loc;
		var location_obj = nlapiLoadRecord('location', loc); //Loading  location record
		nlapiLogExecution('DEBUG', 'Purchase order', 'location=' + location_obj);
		
		
		var shipping_add = location_obj.getFieldValue('addrtext');
		nlapiLogExecution('DEBUG', 'Purchase order', 'ship_address=' + shipping_add);
		
		var actual_shipping_add = nullcheckingfield(shipping_add);
		
		
		
		var format_shipping_add = formatAndReplaceSpacesofMessage(nlapiEscapeXML(actual_shipping_add));
		
		var shipp_phone = location_obj.getFieldValue('addrphone');
		nlapiLogExecution('DEBUG', 'Purchase order', 'shipp_phone=' + shipp_phone);
		
		
		var actual_shipp_phone = nullcheckingfield(shipp_phone);
		
		
	}
	else 
	{
		loc = '';
	}
	/*
if (loc != null && loc != '' && loc != 'undefined') 
	{
		var location_obj = nlapiLoadRecord('location', loc); //Loading  location record
		nlapiLogExecution('DEBUG', 'Purchase order', 'location=' + location_obj);
		
		
		var shipping_add = location_obj.getFieldValue('addrtext');
		nlapiLogExecution('DEBUG', 'Purchase order', 'ship_address=' + shipping_add);
		
		if (shipping_add != null && shipping_add != '' && shipping_add != 'undefined') 
		{
			shipping_add = shipping_add;
		}
		else {
			shipping_add = '';
		}
		
		var format_shipping_add = formatAndReplaceSpacesofMessage(nlapiEscapeXML(shipping_add));
		
		var shipp_phone = location_obj.getFieldValue('addrphone');
		nlapiLogExecution('DEBUG', 'Purchase order', 'shipp_phone=' + shipp_phone);
		
		if (shipp_phone != null && shipp_phone != '' && shipp_phone != 'undefined') 
		{
			shipp_phone = 'T: ' + shipp_phone;
		}
		else {
			shipp_phone = '';
		}
	}
*/
	var notify_upon = PO_obj.getFieldValue('custbody_notifyuponarrival');
	nlapiLogExecution('DEBUG','Purchase order','notify_upon='+notify_upon);
	
	var actual_notify_upon = nullcheckingfield(notify_upon);
	
	
	
	
	//var shipp_fax = location_obj.getFieldValue('addrtext');
	//nlapiLogExecution('DEBUG','Purchase order','shipp_phone='+shipp_phone);
	
	var PO_date = PO_obj.getFieldValue('trandate');
	nlapiLogExecution('DEBUG','Purchase order','PO_date='+PO_date);
	
	var actual_PO_date = nullcheckingfield(PO_date);
	
	
	
	var requisitioner = PO_obj.getFieldText('employee');
	nlapiLogExecution('DEBUG','Purchase order','requisitioner='+requisitioner);
	
	var actual_requisitioner = nullcheckingfield(requisitioner);
	
	
	
	var delivery_date = PO_obj.getFieldValue('duedate');
	nlapiLogExecution('DEBUG','Purchase order','delivery_date='+delivery_date);
	
	var actual_delivery_date = nullcheckingfield(delivery_date);
	
	
	
	var FOB = PO_obj.getFieldValue('custbody_fobpoint');
	nlapiLogExecution('DEBUG','Purchase order','FOB='+FOB);
	
	var actual_FOB = nullcheckingfield(FOB);
	
	
	
	
	var terms = PO_obj.getFieldText('terms');
	nlapiLogExecution('DEBUG','Purchase order','terms='+terms);
	
	var actual_terms = nullcheckingfield(terms);
	
	
	
	var terms_and_con = PO_obj.getFieldValue('custbody_termsandconditions');
	nlapiLogExecution('DEBUG','Purchase order','terms_and_con='+terms_and_con);
	
	
	if (terms_and_con != null && terms_and_con != '' && terms_and_con != 'undefined') 
  		{
   			//terms_and_con = terms_and_con;
			terms_and_con = formatAndReplaceSpacesofMessage(nlapiEscapeXML(terms_and_con));
	        nlapiLogExecution('DEBUG','Purchase order','format_terms_and_con='+terms_and_con);
  		}
  	else 
  		{
   			terms_and_con = '';
  		}
	
	
	var total_discount = PO_obj.getFieldValue('custbody_discounttotal');
	nlapiLogExecution('DEBUG','Purchase order','total_discount='+total_discount);
	
	var actual_total_discount = nullcheckingfield(total_discount);
	
	
	
	var total = PO_obj.getFieldValue('total');
	nlapiLogExecution('DEBUG','Purchase order','total='+total);
	
	var actual_total = nullcheckingfield(total);
	
	
	var sub_total = '';
	if (total_discount != null && total_discount != '' && total_discount != 'undefined') 
	{
		sub_total = parseFloat(total) + parseFloat(total_discount);		
	}
	else
	{
		sub_total = parseFloat(total);
	}
	if (sub_total != null && sub_total != '' && sub_total != 'undefined') 
  		{
   			sub_total = sub_total;
			var sub_total_with_decimal = sub_total.toFixed(2);
	        nlapiLogExecution('DEBUG','Purchase order','sub_total_with_decimal='+sub_total_with_decimal);
			//nlapiLogExecution('DEBUG','Purchase order','terms_and_con='+sub_total);
  		}
  	else 
  		{
   			sub_total = '';
  		}
		
	//*********************Getting  line items*****************
	
	var linecount = PO_obj.getLineItemCount('item');
	nlapiLogExecution('DEBUG','Purchase order','linecount='+linecount);
	
	var linecount_ex = PO_obj.getLineItemCount('expense');
	nlapiLogExecution('DEBUG','Purchase order','linecount_ex='+linecount_ex);
	
	
	
	//var img_url = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=85&c=3591874&h=725a2bad29253944e736';
	//var URL = nlapiEscapeXML(img_url);
	

	//var img_url = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=84&c=3591874&h=c68406a5abf72e7c2ca2';
	//var URL = nlapiEscapeXML(img_url);
	
	var strVar="";
	
	strVar += "<table align=\"left\" width=\"32%\">";
	strVar += "		<tr>";
	strVar += "		<td>";
    strVar += "		<img  width=\"200px\" height=\"90px\" align=\"left\" src=\"" + URL + " \" \/><\/td>";
	strVar += "		<\/tr>";
	strVar += "<\/table>";

	strVar += "<table width=\"100%\" height=\"66\">";
    strVar += "	<tr>";
    strVar += "		<td width=\"313\">BEE'AH<p>The Sharjah Environmental Co.<\/p><\/td>";
    //strVar += "		<td>&nbsp;<\/td>";
    strVar += "		<td width=\"393\" align=\"right\">L.P.O. #"+actual_PO_no+"<p>Date:&nbsp;"+actual_PO_date+"<\/p><\/td>";
    strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>P.O.BOX 20248<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>SHARJAH,UAE<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>Phone 06 572 9000 Fax 06 572 9333<\/td>";
	strVar += "	<\/tr>";
    strVar += "<\/table>";

	//strVar += "<table width=\"100%\">";
	/*
strVar += "	<tr>";
	strVar += "		<td>P.O.BOX 20248<\/td>";
	strVar += "	<\/tr>";
*/
	/*
strVar += "	<tr>";
	strVar += "		<td>SHARJAH,UAE<\/td>";
	strVar += "	<\/tr>";
*/
	/*
strVar += "	<tr>";
	strVar += "		<td>Phone 06 572 9000 Fax 06 572 9333<\/td>";
	strVar += "	<\/tr>";
*/
	//strVar += "<\/table>";
	
	strVar += "<p><\/p>";
	//strVar += "<br/>";
	
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"40%\"><b style =\"font-weight:700;font-size:12;\">VENDOR<\/b><br/>"+format_vendor_Address+"<br/>TEL:"+actual_vendor_phone+",FAX:"+actual_vendor_fax+"<br/>EMAIL:<b><u>"+actual_vendor_mail+"<\/u><\/b><\/td>";
	strVar += "		<td  width=\"20%\"><\/td>";
	strVar += "		<td  width=\"20%\"><\/td>";
	strVar += "		<td width=\"40%\"><b>SHIP TO<\/b><br/>"+format_shipping_add+"<br/>"+actual_shipp_phone+"<br/><b>NOTIFY UPON ARRIVAL:<\/b><br/>"+actual_notify_upon+"<\/td>";
	//strVar += "		<td border=\"0.1\" width=\"20%\"><b>Terms<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	/*
strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"207\"><b>VENDOR<\/b><br/><br/>"+format_vendor_Address+"<br/>TEL:"+vendor_phone+",FAX:"+vendor_fax+"<br/>EMAIL:<b>"+vendor_mail+"<\/b><\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"245\"><b>Ship to<\/b><br/>"+format_shipping_add+"<br/>T:"+shipp_phone+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
*/
	
	
	strVar += "<table border=\"0.1\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\"><b>PO Date<\/b><\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\"><b>Requisitioner<\/b><\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\"><b>Delivery date<\/b><\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\"><b>FOB point<\/b><\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\"><b>Terms<\/b><\/td>";
	strVar += "	<\/tr>";
	
	
	strVar += "	<tr>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\">"+actual_PO_date+"<\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\">"+actual_requisitioner+"<\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\">"+actual_delivery_date+"<\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\">"+actual_FOB+"<\/td>";
	strVar += "		<td border=\"0.1\" width=\"20%\" style=\"border-right-style:none;border-bottom-style:none;\">"+actual_terms+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";


	
	
	strVar += "<p><\/p>";
	strVar += "<table border=\"0.1\" width=\"100%\" style=\"border-bottom-style:none;border-left-style:none;\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"5%\" border=\"0.1\"><b>QTY<\/b><\/td>";
	strVar += "		<td width=\"20%\" border=\"0.1\" style=\"border-left-style:none;\"><b>Item No./Part No.<\/b><\/td>";
	strVar += "		<td width=\"40%\" border=\"0.1\" style=\"border-right-style:none;border-left-style:none;\"><b>Description<\/b><\/td>";
	strVar += "		<td width=\"5%\" border=\"0.1\" style=\"border-right-style:none;\"><b>Unit<\/b><\/td>";
	strVar += "		<td width=\"18%\" align=\"right\" border=\"0.1\" style=\"border-right-style:none;\"><b>Unit Price<\/b><\/td>";
	strVar += "		<td width=\"12%\" align=\"right\" border=\"0.1\"><b>Total<\/b><\/td>";
	strVar += "	<\/tr>";
	
for (var i = 1; i <= linecount_ex; i++) 
{
	if (linecount_ex != '' && linecount_ex != null && linecount_ex != 'undefined') 
	{
	
		//nlapiLogExecution('DEBUG', 'Purchase order inside', 'linecount_ex=' + linecount_ex);
		var qty_ex = PO_obj.getLineItemValue('expense', 'custcol_quantity', i);
		nlapiLogExecution('DEBUG', 'Purchase order expense', 'qty=' + qty_ex);
		
		var actual_qty_ex = nullcheckingfield(qty_ex);
		
		
		
		var HS_code_ex = PO_obj.getLineItemValue('expense', 'custcol_mpn', i);
		nlapiLogExecution('DEBUG', 'Purchase order expense', 'HS_code=' + HS_code_ex);
		
		var actual_HS_code_ex = nullcheckingfield(HS_code_ex);
		
		
		
		
		var description_ex = PO_obj.getLineItemValue('expense', 'memo', i);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'description=' + description_ex);
		
		var actual_description_ex = nullcheckingfield(description_ex);
		var format_description_ex = formatAndReplaceSpacesofMessage(nlapiEscapeXML(actual_description_ex));
		
		
		
		
		var UOM = PO_obj.getLineItemText('expense', 'custcol_uom', i);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'UOM=' + UOM);
		
		var actual_UOM = nullcheckingfield(UOM);
		
		
		
		
		var rate = PO_obj.getLineItemValue('expense', 'custcol_rate', i);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'rate=' + rate);
		
		var actual_rate = nullcheckingfield(rate);
		
		
		
		var line_total_USD_ex = actual_qty_ex * actual_rate;
		line_total_USD_ex = line_total_USD_ex.toFixed(2);
		//var line_total_USD_ex = PO_obj.getLineItemValue('expense', 'grossamt', i);
		nlapiLogExecution('DEBUG', 'Purchase order expense', 'line_total_USD_ex=' + line_total_USD_ex);
		
		var actual_line_total_USD_ex = nullcheckingfield(line_total_USD_ex);
		
		
		
	strVar += "	<tr>";	
	strVar += "		<td width=\"5%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;\">" + actual_qty_ex + "<\/td>";
	strVar += "		<td width=\"20%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-left-style:none;\">"+ actual_HS_code_ex + "<\/td>";
	strVar += "		<td width=\"40%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;\">"+format_description_ex+"<\/td>";
	strVar += "		<td width=\"5%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;\">"+actual_UOM+"<\/td>";
	strVar += "		<td width=\"18%\" align=\"right\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;\">"+actual_rate+"<\/td>";
	strVar += "		<td width=\"12%\" align=\"right\" border=\"0.1\" style=\"border-top-style:none;border-bottom-style:none;\">"+actual_line_total_USD_ex+"<\/td>";
	strVar += "	<\/tr>";	
		
	}
	
}


for (var j = 1; j <= linecount; j++) 
{
	if (linecount != '' && linecount != null && linecount != 'undefined') 
	{
		var qty_item = PO_obj.getLineItemValue('item', 'quantity', j);
		nlapiLogExecution('DEBUG', 'Purchase order expense', 'qty=' + qty_item);
		
		var actual_qty_item = nullcheckingfield(qty_item);
		
		
		
		var HS_code_item = PO_obj.getLineItemValue('item', 'custcol_mpn', j);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'HS_code=' + HS_code_item);
		
		var actual_HS_code_item = nullcheckingfield(HS_code_item);
		
		
			
		
		
		var description = PO_obj.getLineItemValue('item', 'description', j);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'description=' + description);
		
		var actual_description = nullcheckingfield(description);
		var format_description = formatAndReplaceSpacesofMessage(nlapiEscapeXML(actual_description));
		
		
		
		var units = PO_obj.getLineItemValue('item', 'units_display', j);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'units=' + units);
		
		var actual_units = nullcheckingfield(units);
		
		
		
		var unitprice = PO_obj.getLineItemValue('item', 'origrate', j);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'unitprice=' + unitprice);
		
		var actual_unitprice = nullcheckingfield(unitprice);
		
		
		var line_total_USD = actual_qty_item * actual_unitprice;
		line_total_USD = line_total_USD.toFixed(2);
		//var line_total_USD = PO_obj.getLineItemValue('item', 'grossamt', j);
		nlapiLogExecution('DEBUG', 'Purchase order item', 'line_total item=' + line_total_USD);
		
		var actual_line_total_USD = nullcheckingfield(line_total_USD);
		
		
		

		
	strVar += "	<tr>";	
	strVar += "		<td width=\"5%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;\">"+actual_qty_item+"<\/td>";
	strVar += "		<td width=\"20%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-left-style:none;\">"+actual_HS_code_item+"<\/td>";
	strVar += "		<td width=\"40%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;\">"+format_description+"<\/td>";
	strVar += "		<td width=\"5%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;\">"+actual_units+"<\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;\">"+actual_unitprice+"<\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\" style=\"border-top-style:none;border-bottom-style:none;\">"+actual_line_total_USD+"<\/td>";
	strVar += "	<\/tr>";
	
	/*
strVar += "	<tr>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"20%\"><\/td>";
	strVar += "		<td width=\"40%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"20.67%\" align=\"right\"><b>SUBTOTAL<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\">"+sub_total_with_decimal+"<\/td>";
	strVar += "	<\/tr>";
*/
	
	
	
	}
}

    //strVar += "<\/table>";


	
    //strVar += "<table width=\"100%\">";
	
	//strVar += "<\/table>";
	
	
	
	//strVar += "<table width=\"100%\">";
	strVar += "	<tr border=\"0.1\" style=\"border-bottom-style:none;\">";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"20%\"><\/td>";
	strVar += "		<td width=\"40%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;\"><b>SUBTOTAL<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\">"+sub_total_with_decimal+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"20%\"><\/td>";
	strVar += "		<td width=\"40%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;\"><b>DISCOUNT<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\">"+actual_total_discount+"<\/td>";
	
	
	/*
strVar += "		<td width=\"10%\" align=\"right\"><b>DISCOUNT<\/b><\/td>";
	strVar += "		<td width=\"13.5%\" border=\"0.1\" align=\"right\" style=\"border-bottom-style:none;\">&nbsp;"+actual_total_discount+"<\/td>";
*/
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"20%\"><\/td>";
	strVar += "		<td width=\"40%\"><\/td>";
	strVar += "		<td width=\"5%\"><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;\"><b>TOTAL<\/b><\/td>";
	strVar += "		<td width=\"15%\" align=\"right\" border=\"0.1\">"+actual_total+"<\/td>";
	
	/*
strVar += "		<td width=\"10%\" align=\"right\"><b>TOTAL<\/b><\/td>";
	strVar += "		<td width=\"13.5%\" border=\"0.1\" align=\"right\" >&nbsp;"+actual_total+"<\/td>";
*/
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "	<td width=\"75%\" rowspan=\"3\"><b>Terms and conditions:<\/b><p>"+terms_and_con+"<\/p><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	//strVar += "<p>&nbsp;<\/p>";
	//strVar += "<p>&nbsp;<\/p>";
	//strVar += "<p>&nbsp;<\/p>";
    strVar += "<p>&nbsp;<\/p>";
	
	
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"35%\">&nbsp;<\/td>";
	strVar += "		<td width=\"35%\">&nbsp;<\/td>";
	strVar += "		<td width=\"30%\">&nbsp;____________________________________<br/>Authorized by<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
  
  	strVar += "";
	
//================Converting XML to PDF===============

	   //var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
       
	  // var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
  //xml += "<pdf>\n<body>";
  //xml += "<pdf>";
  //-----------------------------------------------------
   
  //var xml="";
  /*
var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>";
  xml += "<head>";
  xml += "  <style>";
  
  for(var l=1;l<10000;l++)
  {
   xml += "    #page"+l+" { header:smallheader; header-height:70pt; }";
  }
  xml += "  <\/style>";
  xml += "  <macrolist>";
  
  
  xml += " <macro id=\"myfooter\">";
  xml += "      <p align=\"center\">";
  
  xml += "<i font-size=\"8\">";
  xml +=" Page: <pagenumber\/>"
  xml += "<\/i>";
  xml += "      <\/p>";
  xml += "    <\/macro>";
  
  xml += "  <\/macrolist>";
  xml += "<\/head>";

  xml += "<body footer=\"myfooter\" footer-height=\"2em\">"+strVar+"</body>\n</pdf>";
*/
	
//================Code to add page no.===============	
	
	   
  var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";  
  xml += "<pdf>"; 
  xml += "<head>";
  xml += "  <style>";  
  for(var l=1;l<10000;l++)
  {
   xml += "    #page"+l+" { header:smallheader; header-height:0pt; }";
  }
  xml += "  <\/style>";
  xml += "  <macrolist>";
  xml += "    <macro id=\"smallheader\">";  
  xml += "<table>";
  xml += "<tr>";  
  xml += "<td><\/td>";  
  xml += "<\/tr>";
  xml += "<\/table>";   
  xml += "    <\/macro>";
  
  xml += " <macro id=\"myfooter\">";
  xml += "      <p align=\"center\">"; 
  xml += "<i font-size=\"8\">";
  xml +=" Page: <pagenumber\/>"
  xml += "<\/i>";
  xml += "      <\/p>";
  xml += "    <\/macro>";
  
  xml += "  <\/macrolist>";
  xml += "<\/head>";

  xml += "<body margin-top=\"0pt\" header=\"smallheader\" footer=\"myfooter\" footer-height=\"2em\">"; 
  xml += strVar;
  xml += "</body>\n</pdf>"; 
	   
	   
	   
	   var file = nlapiXMLToPDF(xml);
       response.setContentType('PDF','Payment receipt.pdf', 'inline');
       response.write(file.getValue());

}


function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
//&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 return messgaeToBeSendPara;
}

function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	 