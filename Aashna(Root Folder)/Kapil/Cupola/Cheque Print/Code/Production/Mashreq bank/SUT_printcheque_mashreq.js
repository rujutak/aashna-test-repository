//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_printcheque_mashreq.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:21-06-13
	 Version:0.1
	 Description:Print cheque data
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 	
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  cheque_msq_print(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET
function cheque_msq_print(request,response)
{
	/* Suitelet:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	var param=request.getParameter('custscript_mashreq');
	nlapiLogExecution('DEBUG','Cheque','cheque id ='+param);
	
	var chk_obj = nlapiLoadRecord('vendorpayment',param);       //Loading  vendor payment record
	nlapiLogExecution('DEBUG','Cheque','chk_obj ='+chk_obj);
	
	var chk_date= chk_obj.getFieldValue('trandate');
	chk_date = nullcheckingfield(chk_date);
	nlapiLogExecution('DEBUG','Cheque','chk_date ='+chk_date);
	
	chk_date = getDateFromFormat(chk_date);
	nlapiLogExecution('DEBUG','Cheque','chk_date after format='+chk_date);
	
	//var chk_entity= chk_obj.getFieldText('entity');
	var chk_entity= chk_obj.getFieldValue('custbody_customernameoncheck');
	chk_entity=formatAndReplaceMessageForAnd(nlapiEscapeXML(chk_entity))
	nlapiLogExecution('DEBUG','Cheque','chk_entity before if else='+chk_entity);
	/*
var index = chk_entity.lastIndexOf('-')
	if(index != -1)
	{
	  var req_string = chk_entity.substring(0,index);               
	  chk_entity = req_string ;
	}
	else
	{
	  chk_entity = chk_entity;
	} 
*/
	nlapiLogExecution('DEBUG','Cheque','chk_entity after if else='+chk_entity);
	
	
	var chk_amount= chk_obj.getFieldValue('total');
	chk_amount = nullcheckingfield(chk_amount);
	chk_amount = parseFloat(chk_amount);
	chk_amount = chk_amount.toFixed(2);
	nlapiLogExecution('DEBUG','Cheque','chk_amount ='+chk_amount);
	
	//var amount_with_comma = numberWithCommas(chk_amount);
	//nlapiLogExecution('DEBUG','Cheque','amount_with_comma ='+amount_with_comma);
	
	var amount_in_words = toWordsFunc(chk_amount);
	nlapiLogExecution('DEBUG','Cheque','amount_in_words ='+amount_in_words);
	
	//var chk_no= chk_obj.getFieldValue('tranid');
	//chk_no = nullcheckingfield(chk_no);
	//nlapiLogExecution('DEBUG','Cheque','chk_no ='+chk_no);
	
	//var chk_currency= chk_obj.getFieldText('currency');
	//chk_currency = nullcheckingfield(chk_currency);
	//nlapiLogExecution('DEBUG','Cheque','chk_currency ='+chk_currency);
	
//**************Html design****************
     
	var strVar="";

    /*
strVar += "<table width=\"100%\">";
	strVar += "		<tr>";
	strVar += "			<td width=\"0%\" align=\"left\"><b style=\"font-size:12\"><\/b><\/td>";
	//strVar += "			<td width=\"668\" style=\"font-size:9\"><b>Date&nbsp;&nbsp;&nbsp;<\/b>"+date+"<\/td>";
	strVar += "			<td align=\"left\" width=\"50%\" style=\"font-size:12\"><b style=\"font-size:10\"><\/b><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
*/
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "			<td width=\"80%\"><b style=\"font-size:12\"><\/b><\/td>";
	strVar += "			<td align=\"left\" width=\"20%\" style=\"font-size:12\"><b style=\"font-size:10\"><\/b><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "			<td width=\"80%\"><b style=\"font-size:12\"><\/b><\/td>";
	strVar += "			<td align=\"left\" width=\"20%\" style=\"font-size:12\"><b style=\"font-size:10\"><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;"+chk_date+"<\/b><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<\/table>";
	
	/*
strVar += "  <table  width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<\/table>";
*/

    //strVar += "	<br/>";
	
	strVar += "  <table  width=\"100%\">";
	
	
    strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "   <tr>";
	strVar += "    <td width=\"100%\" >";//align=\"right\"
	strVar += "  <b style =\"font-size:12;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+chk_entity+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td>&nbsp;<\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
	
	
    strVar += "  <table  width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\"><\/td>";
	strVar += "		<\/tr>";
	strVar += "   <tr>";
	strVar += "    <td align=\"left\" width=\"75%\" style =\"font-size:12;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+amount_in_words+" Only<\/b><\/td>";
	//strVar += "    <td border=\"0.1\" align=\"left\" width=\"25%\" style =\"font-size:12;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/b>&nbsp;&nbsp;&nbsp;<b style =\"font-weight:450;\">"+amount_with_comma+"<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td width=\"65%\"><\/td>";
	//strVar += "    <td border=\"0.1\" align=\"left\" width=\"35%\" style =\"font-size:12;\"><b style =\"font-weight:450;\">"+amount_with_comma+"<\/b><\/td>";
	strVar += "			<td align=\"left\" width=\"35%\" style=\"font-size:12\"><b style=\"font-size:10\"><\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td width=\"65%\"><\/td>";
	//strVar += "    <td border=\"0.1\" align=\"left\" width=\"35%\" style =\"font-size:12;\"><b style =\"font-weight:450;\">"+amount_with_comma+"<\/b><\/td>";
	strVar += "			<td align=\"right\" width=\"35%\" style=\"font-size:12\"><b style=\"font-size:10\"><\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "   <tr>";
	strVar += "    <td width=\"65%\"><\/td>";
	//strVar += "    <td border=\"0.1\" align=\"left\" width=\"35%\" style =\"font-size:12;\"><b style =\"font-weight:450;\">"+amount_with_comma+"<\/b><\/td>";
	strVar += "			<td align=\"right\" width=\"35%\" style=\"font-size:12\"><b style=\"font-size:10\">**"+chk_amount+"**<\/b><\/td>";
	strVar += "   <\/tr>";
	strVar += "  <\/table>";
		
	strVar += "";
	
	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
    var file = nlapiXMLToPDF(xml);
    response.setContentType('PDF','Payment receipt.pdf', 'inline');
    response.write(file.getValue());

}	
function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	

function numberWithCommas(sum_Dhs) 
{
	var amt = sum_Dhs.split('.');
	var Rs_amt = amt[0];
	
    Rs_amt = Rs_amt.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(Rs_amt))
    Rs_amt = Rs_amt.replace(pattern, "$1,$2");
    return Rs_amt;
}

function getDateFromFormat(param)
	{
		//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Param : " + param);
		//var date = new Date(param);
		//var convertedDate = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
		var months = new Array();
		months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var dateArray = new Array();
		dateArray = param.split('/');
		//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Date : " + dateArray[0]);
		//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Month : " + months[dateArray[1]-1]);
		//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Year : " + dateArray[2]);
		var convertedDate = dateArray[0] + "-" + months[dateArray[1]-1] + "-" + dateArray[2];
		//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Converted Date : " + convertedDate);

		return convertedDate;
	}

function toWordsFunc(s)
{
   // alert('In conversion function')
    var str=''

    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}	 
 
function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Messgae with and =====" + messgaeToBeSendParaAnd);
 
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
 
  return messgaeToBeSendParaAnd;
} 	   