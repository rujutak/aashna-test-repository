//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:SUT_print_invoice.js
	 Author:Kapil
	 Company:Aashna Cloudtech
	 Date:27-06-13
	 Version:0.1
	 Description:Print invoice details
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 	6-8-13				        Kapil                             Sandesh                               Alignment issue resolved
	 	7-8-13                      Kapil                             Sandesh                               Bank details edited
	 	17-9-13						Kapil							  Sandesh								Terms field taken instead of custom payment field,logo and company name added as per subsidiary
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  invoice_print(request,response)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN SUITELET

function invoice_print(request, response)
{
	/* Suitelet:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SUITELET CODE BODY
	
	//nlapiLogExecution('DEBUG','Purchase order','Purchase order id=');
	var param = request.getParameter('custscript_invoice_print');
	nlapiLogExecution('DEBUG', 'Invoice', 'Invoice id =' + param);
	
	
	
	var Inv_obj = nlapiLoadRecord('invoice', param); //Loading  Invoice record
	nlapiLogExecution('DEBUG', 'Invoice', 'Inv_obj=' + Inv_obj);
	
	var Inv_no = Inv_obj.getFieldValue('tranid');
	Inv_no = formatAndReplaceSpacesofMessage(Inv_no);
	nlapiLogExecution('DEBUG', 'Invoice', 'Inv_no=' + Inv_no);
	
	var PO_no = Inv_obj.getFieldValue('otherrefnum');
	PO_no = nullcheckingfield(PO_no);
	//PO_no= formatAndReplaceSpacesofMessage(PO_no);
	nlapiLogExecution('DEBUG', 'Invoice', 'PO_no=' + PO_no);
	
	var payment = Inv_obj.getFieldText('terms');
	payment = nullcheckingfield(payment);
	payment = formatAndReplaceSpacesofMessage(nlapiEscapeXML(payment));
	nlapiLogExecution('DEBUG', 'Invoice', 'payment=' + payment);
	
	var subsidary = Inv_obj.getFieldValue('subsidiary');
	nlapiLogExecution('DEBUG', 'Invoice', 'subsidary=' + subsidary);
	var img_url = ''
	var company_name;
	if (subsidary != null && subsidary != '' && subsidary != 'undefined')
	{
		if(subsidary == 2)
		{
			img_url = 'https://system.na1.netsuite.com/core/media/media.nl?id=48&c=3651106&h=cc91790cc7db2e55f35c';
		}
		
		else if(subsidary == 3)
		{
			img_url = 'https://system.na1.netsuite.com/core/media/media.nl?id=49&c=3651106&h=bfa4a978d51893f94910';
		}
		
		//var subsidary_rec = nlapiLoadRecord('subsidiary',subsidary);
		//var subsidary_logo = subsidary_rec.getFieldValue('pagelogo');
		//nlapiLogExecution('DEBUG', 'Invoice', 'subsidary_logo=' + subsidary_logo);
	}
	
	var customer_msg = Inv_obj.getFieldValue('message');
	//customer_msg = nullcheckingfield(customer_msg);
	if (customer_msg != null && customer_msg != '' && customer_msg != 'undefined') 
	{
		customer_msg = formatAndReplaceSpacesofMessage(nlapiEscapeXML(customer_msg))
	//customer_msg = nlapiEscapeXML(customer_msg);
		
	}
	else
	{
		customer_msg = ''
	}
	
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_msg=' + customer_msg);
	
	/*
var split_cust_msg = customer_msg.split(',');
	var bank_details = split_cust_msg[0] + split_cust_msg[1] + split_cust_msg[2];
	nlapiLogExecution('DEBUG', 'Invoice', 'bank details before=' + bank_details)
	bank_details = nullcheckingfield(bank_details);
	nlapiLogExecution('DEBUG', 'Invoice', 'bank details after=' + bank_details)
	var swift_code = split_cust_msg[3];
	swift_code = nullcheckingfield(swift_code);
	var comp_name = split_cust_msg[4];
	comp_name = nullcheckingfield(comp_name);
	var other_details = split_cust_msg[5] + split_cust_msg[6];
	other_details = nullcheckingfield(other_details);
*/
	
	var Inv_date = Inv_obj.getFieldValue('trandate');
	Inv_date = nullcheckingfield(Inv_date);
	Inv_date = getDateFromFormat(Inv_date);
	nlapiLogExecution('DEBUG', 'Invoice', 'PO_date=' + Inv_date);
	
	var currency = Inv_obj.getFieldText('currency');
	currency = nullcheckingfield(currency);
	nlapiLogExecution('DEBUG', 'Invoice', 'currency=' + currency);
	
	var Inv_total = Inv_obj.getFieldValue('total');
	Inv_total = nullcheckingfield(Inv_total);
	//var formatted_Inv_total = Inv_total.split('.');
	//var decimal_Inv_total = formatted_Inv_total[1];
	//nlapiLogExecution('DEBUG', 'Invoice', 'Inv_total with decimal=' + Inv_total);
	var Inv_total_with_comma = numberWithCommas(Inv_total);
	nlapiLogExecution('DEBUG', 'Invoice', 'Inv_total with comma=' + Inv_total_with_comma);
	
	var total_inwords = currency + ' ' + toWordsFunc(Inv_total);
	
	
	var customerID = Inv_obj.getFieldValue('entity');
	nlapiLogExecution('DEBUG', 'Invoice', 'customerID before if=' + customerID);
	if(customerID !=null && customerID != '' && customerID != 'undefined')
	{
		customerID=formatAndReplaceMessageForAnd(nlapiEscapeXML(customerID))
	}
	else
	{
		customerID = ''
	}
	nlapiLogExecution('DEBUG', 'Invoice', 'customerID after if=' + customerID);
	var customer_rec = nlapiLoadRecord('customer', customerID);
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_rec=' + customer_rec);
	
	var customer_name = customer_rec.getFieldValue('companyname');
	nlapiLogExecution('DEBUG', 'Invoice before if else', 'customer_name=' + customer_name);
	var index = customer_name.lastIndexOf('-')
	if(index != -1)
	{
	  var req_string = customer_name.substring(0,index);               
	  customer_name = req_string ;
	  customer_name=formatAndReplaceMessageForAnd(nlapiEscapeXML(customer_name))
	}
	else
	{
	  customer_name = customer_name;
	} 
	nlapiLogExecution('DEBUG', 'Invoice after if else', 'customer_name=' + customer_name);
	
		
	//nlapiLogExecution('DEBUG', 'Invoice after if', 'customer_name=' + customer_name);
	
	var customer_address = customer_rec.getFieldValue('defaultaddress');
	//customer_address = nullcheckingfield(customer_address);
	if (customer_address != null && customer_address != '' && customer_address != 'undefined') 
	{
		customer_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(customer_address));
	}
	else
	{
		customer_address = ''
	}
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_address=' + customer_address);
	
	var customer_phone = customer_rec.getFieldValue('phone');
	customer_phone = nullcheckingfield(customer_phone);
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_phone=' + customer_phone);
	
	var customer_fax = customer_rec.getFieldValue('fax');
	customer_fax = nullcheckingfield(customer_fax);
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_fax=' + customer_fax);
	
	var customer_email = customer_rec.getFieldValue('email');
	customer_email = nullcheckingfield(customer_email);
	nlapiLogExecution('DEBUG', 'Invoice', 'customer_email=' + customer_email);
	
	var getcontactdetails = getcontact(customerID);
	var entity_id = '';
	var jobtitle = '';
	if (getcontactdetails != null) 
	{
		//entity_id = getcontactdetails[0].getText('entityid');
		entity_id = getcontactdetails[0].getValue('entityid');
		//entity_id=formatAndReplaceMessageForAnd(nlapiEscapeXML(entity_id))
		nlapiLogExecution('DEBUG', 'after contact search', 'entityid=' + entity_id);
		
		jobtitle = getcontactdetails[0].getValue('title');
		nlapiLogExecution('DEBUG', 'after contact search', 'jobtitle=' + jobtitle);
	}
	else
	{
		entity_id = ''
		nlapiLogExecution('DEBUG', 'after contact search in else', 'entityid=' + entity_id);
		jobtitle = ''
		nlapiLogExecution('DEBUG', 'after contact search in else', 'jobtitle=' + jobtitle);
	}
	var linecount = Inv_obj.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'Invoice', 'linecount=' + linecount);
	
	//var linecount_ex = PO_obj.getLineItemCount('expense');
	//nlapiLogExecution('DEBUG','Invoice','linecount_ex='+linecount_ex);
	
	
	//var total_linecount = parseInt(linecount)+parseInt(linecount_ex);
	//nlapiLogExecution('DEBUG','Purchase order','total_linecount='+total_linecount);
	
	
	
	//var img_url = '	https://system.na1.netsuite.com/core/media/media.nl?id=11&c=3651106&h=f9abd0c52cb72188db1c';
	
	var URL = nlapiEscapeXML(img_url);
	//var URL = nlapiEscapeXML(subsidary_logo)
	
	
	
	var strVar = "";
	strVar += "<table border=\"0.1\" width=\"100%\" style=\"border-bottom-style:none;border-left-style:none;border-top-style:none;border-right-style:none;\">";
	if(subsidary == 2)
	{
		strVar += "	<tr>";
	    strVar += "		<td>";
	    strVar += "<img  width=\"150px\" height=\"80px\" align=\"left\" src=\"" + URL + " \" \/><\/td>";
	    //strVar += "<img  width=\"150px\" align=\"left\" src=\"" + URL + " \" \/>Cupola Teleservices Ltd<br/><b style =\"font-weight:700;font-size:10;\">INVOICE<\/b><\/td>";
	    strVar += "		<td align=\"right\"><b style =\"font-weight:400;font-size:20;\">Cupola Teleservices Ltd<\/b><br/><b style =\"font-weight:700;font-size:10;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INVOICE<\/b><\/td>";
	    strVar += "	<\/tr>";
	}
	else if(subsidary == 3)
	{
		strVar += "	<tr>";
	    strVar += "		<td>";
	    strVar += "<img  width=\"150px\" height=\"80px\" align=\"left\" src=\"" + URL + " \" \/><\/td>";
	    //strVar += "<img  width=\"150px\" align=\"left\" src=\"" + URL + " \" \/>Cupola Teleservices Ltd<br/><b style =\"font-weight:700;font-size:10;\">INVOICE<\/b><\/td>";
	    strVar += "		<td align=\"right\"><b style =\"font-weight:400;font-size:20;\">MEA RESOURCE SOLUTIONS<\/b><br/><b style =\"font-weight:700;font-size:10;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INVOICE<\/b><\/td>";
	    strVar += "	<\/tr>";
	}
	
	strVar += "<\/table>";
	
	strVar += "<table border=\"0.1\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"70%\">Attn:"+entity_id+" <br/> "+jobtitle+"<\/td>";
	strVar += "		<td border=\"0.1\" width=\"30%\" background-color=\"#000000\" color=\"#FFFFFF\" style=\"border-bottom-style:none;border-top-style:none;\">Invoice&nbsp;No:" + Inv_no + "<\/td>";
	strVar += "	<\/tr>";
	
    strVar += "	<tr>";
	strVar += "		<td width=\"70%\" ><\/td>";
	strVar += "		<td border=\"0.1\" width=\"30%\" background-color=\"#FFFFFF\" style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
	strVar += "	<\/tr>";

	strVar += "	<tr>";
	strVar += "		<td width=\"70%\"><b> "+customer_name+"<\/b><\/td>";
	//strVar += "		<td width=\"50%\" border=\"0.1\" style=\"border-bottom-style:none;\">Date:" + Inv_date + "<\/td>";
	strVar += "<td width=\"30%\" background-color=\"#000000\" color=\"#FFFFFF\">Date:&nbsp;&nbsp;" + Inv_date + "<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"70%\" ><\/td>";
	strVar += "		<td border=\"0.1\" width=\"30%\" background-color=\"#FFFFFF\" style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"70%\">" + customer_address + "<\/td>";
	strVar += "		<td width=\"30%\" border=\"0.1\" background-color=\"#000000\" color=\"#FFFFFF\">P.O. No:&nbsp;&nbsp;" + PO_no + " <br/>Currency:&nbsp;&nbsp;" + currency + " <\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"70%\">TEL:&nbsp;&nbsp;" + customer_phone + " FAX:&nbsp;&nbsp;" + customer_fax + "<br/><u>" + customer_email + "<\/u><\/td>";
	strVar += "		<td width=\"30%\" border=\"0.1\" background-color=\"#000000\" color=\"#FFFFFF\" style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	//strVar += "<br/>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr height=\"1%\">";
	strVar += "		<td><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr height=\"1%\">";
	strVar += "		<td><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table border=\"0.1\" style=\"border-top-style:none;\" width=\"100%\">";
	strVar += "	<tr background-color=\"#000000\" color=\"#FFFFFF\">";
	strVar += "		<td align=\"center\" border=\"0.1\" width=\"70%\" style=\"border-bottom-style:none;\"><b>Description<\/b><\/td>";
	strVar += "		<td align=\"center\" border=\"0.1\" width=\"30%\" style=\"border-bottom-style:none;\"><b>Amount<\/b><\/td>";
	strVar += "	<\/tr>";
	
	//*****for items
	
	//for (var i = 1; i <= linecount; i++) 
	//{
	var description = ''
	var gross_amt = ''
	if (linecount == 1) 
	{
		description = Inv_obj.getLineItemValue('item', 'item_display', 1)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 1);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\" style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		/*
strVar += "	<tr height=\"1%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"1%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
*/
		
		
	}
	if(linecount == 2)
	{
		description = Inv_obj.getLineItemValue('item', 'item_display', 1)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 1);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\" style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-bottom-style:none;border-top-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";
		

		
		description = Inv_obj.getLineItemValue('item', 'item_display', 2)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 2);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		
	}
	
	if(linecount == 3)
	{
		description = Inv_obj.getLineItemValue('item', 'item_display', 1)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 1);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
		
		description = Inv_obj.getLineItemValue('item', 'item_display', 2)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 2);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		
        strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";

		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
	

		
		description = Inv_obj.getLineItemValue('item', 'item_display', 3)
		nlapiLogExecution('DEBUG', 'Invoice', 'description=' + description);
		
		gross_amt = Inv_obj.getLineItemValue('item', 'grossamt', 3);
		gross_amt = numberWithCommas(gross_amt)
		//gross_amt = gross_amt.toFixed(2);
		//nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with decimal=' + gross_amt);
		//gross_amt=numberWithCommas(gross_amt);
		nlapiLogExecution('DEBUG', 'Invoice', 'gross_amt with comma=' + gross_amt);
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + description + "<\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\">" + gross_amt + "<\/td>";
		strVar += "	<\/tr>";
		
		strVar += "	<tr height=\"3%\">";
		strVar += "		<td border=\"0.1\"  width=\"70%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\"  style=\"border-top-style:none;border-bottom-style:none;\"><\/td>";
		strVar += "	<\/tr>";
	

	}
	
	
	//}
	strVar += "	<tr>";
    strVar += "		<td border=\"0.1\" width=\"70%\">TOTAL<\/td>";
	strVar += "		<td align=\"right\" border=\"0.1\" width=\"30%\">" + Inv_total_with_comma + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table border=\"0.1\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td><b>AMOUNT IN WORDS: " + total_inwords + " Only.<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr height=\"1%\">";
	strVar += "		<td><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	//strVar += "<br/>";
	
	strVar += "<table width=\"100%\" style =\"font-size:8;\">";
	strVar += "	<tr>";
	strVar += "	<td width=\"75%\" ><b style =\"font-weight:700;font-size:12;\">Payment:<\/b><p>" + payment + "<\/p><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"75%\">";
	strVar += "	<tr>";
	strVar += "	<td width=\"20%\" style =\"font-size:9;\"><b><u>USD Bank Transfer<\/u><\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	//strVar += "<table width=\"100%\" style =\"font-size:8;\">";
    /*
strVar += "	<tr>";
	strVar += "	<td width=\"25%\">Bank Details:<\/td>";
	strVar += "	<td width=\"75%\">&nbsp;<b style =\"font-weight:700;font-size:12;\"><\/b>" + bank_details + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "	<td width=\"25%\">SWIFT CODE:<\/td>";
	strVar += "	<td width=\"75%\"><b style =\"font-weight:700;font-size:12;\"><\/b>" + swift_code + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "	<td width=\"25%\">Beneficiary:<\/td>";
	strVar += "	<td width=\"75%\"><b style =\"font-weight:700;font-size:12;\"><\/b>" + comp_name + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "	<td width=\"25%\">Account No.:<\/td>";
	strVar += "	<td width=\"75%\"><b style =\"font-weight:700;font-size:12;\"><\/b>" + other_details + "<\/td>";
	strVar += "	<\/tr>";
*/
	//strVar += "<\/table>";
	strVar += "<table width=\"100%\" style =\"font-size:8;\">";
    strVar += "	<tr>";
	strVar += "	<td width=\"18%\">Pay To:<\/td>";
	strVar += "	<td width=\"82%\">" + customer_msg + "<br/><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "	<td width=\"18%\"><\/td>";
	strVar += "	<td width=\"82%\"><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "	<td width=\"18%\"><\/td>";
	strVar += "	<td width=\"82%\"><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "	<td width=\"18%\"><\/td>";
	strVar += "	<td width=\"82%\"><\/td>";
	strVar += "	<\/tr>";

	strVar += "<\/table>";
	//strVar += "<br/>";
	
	strVar += "<table width=\"100%\" style =\"font-size:9;\">";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td align=\"left\">CIRCULATION<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td align=\"left\">1.Customer<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td align=\"left\">2.CTS Client Services<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td align=\"left\">3.CTS Finance<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<table width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\">&nbsp;____________________________________<br/>Authorized by<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	
	var footer_text = 'P.O. BOX 500220 DUBAI OUTSOURCE ZONE DUBAI UNITED ARAB EMIRATES'
	var footer2 = 'TEL: (971-4) 3662000 FAX: (971-4) 3662002';
	var footer3 = 'www.cupolagroup.com';
	//footer_text = footer_text.replace("ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½","#8226;");
	//footer_text = escape(footer_text).replace("ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¢","&#8226;");
	
	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
	xml += "<pdf>";
	xml += "<head>";
	xml += "  <style>";
	for (var l = 1; l < 10000; l++) 
	{
		xml += "    #page" + l + " { header:smallheader; header-height:0pt; }";
		//xml += "    #page" + footer_text + " { header:smallheader; header-height:0pt; }";
	}
	xml += "  <\/style>";
	xml += "  <macrolist>";
	xml += "    <macro id=\"smallheader\">";
	xml += "<table>";
	xml += "<tr>";
	xml += "<td><\/td>";
	xml += "<\/tr>";
	xml += "<\/table>";
	xml += "    <\/macro>";
	
	xml += " <macro id=\"myfooter\">";
  xml += "      <p align=\"center\">"; 
  //xml += "<i font-size=\"7\">";
  xml += "<table>";
	xml += "<tr>";
	xml += "<td align=\"center\" font-size=\"8\">"+footer_text+"<\/td>";
	xml += "<\/tr>";
	xml += "<tr>";
	xml += "<td align=\"center\" font-size=\"8\">"+footer2+"<\/td>";	
	xml += "<\/tr>";
	xml += "<tr>";
	xml += "<td align=\"center\" font-size=\"8\">"+footer3+"<\/td>";	
	xml += "<\/tr>";
	xml += "<\/table>";
  //xml +=" Page: <pagenumber\/>"
  //xml += "<\/i>";
  xml += "      <\/p>";
  xml += "    <\/macro>";
	
	xml += "  <\/macrolist>";
	xml += "<\/head>";
	
	xml += "<body margin-top=\"0pt\" header=\"smallheader\" footer=\"myfooter\" footer-height=\"2em\">";
	xml += strVar;
	xml += "</body>\n</pdf>";
	
	
	
	var file = nlapiXMLToPDF(xml);
	response.setContentType('PDF', 'Invoice.pdf', 'inline');
	response.write(file.getValue());
	
}
	
    /*
var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">"+strVar+"</body>\n</pdf> ";
    var file = nlapiXMLToPDF(xml);
    response.setContentType('PDF','Payment receipt.pdf', 'inline');
    response.write(file.getValue());
*/



function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
//&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 return messgaeToBeSendPara;
}


function getDateFromFormat(date_val)
{
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Param : " + param);
	//var date = new Date(param);
	//var convertedDate = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
	var months = new Array();
	months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var dateArray = new Array();
	dateArray = date_val.split('/');
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Date : " + dateArray[0]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Month : " + months[dateArray[1]-1]);
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " DateArray Year : " + dateArray[2]);
	var convertedDate = dateArray[0] + "-" + months[dateArray[1]-1] + "-" + dateArray[2];
	//nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Converted Date : " + convertedDate);

	return convertedDate;
}	

/*
function numberWithCommas(sum_Dhs) 
{
	var amt = sum_Dhs.split('.');
	var Rs_amt = amt[0];
	
    Rs_amt = Rs_amt.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(Rs_amt))
    Rs_amt = Rs_amt.replace(pattern, "$1,$2");
    return Rs_amt;
}
*/

function getcontact(customerID)
{
	var filters = new Array();
	var columns = new Array();
	filters[0] = new nlobjSearchFilter('company',null,'is',customerID)
	//columns[0] = new nlobjSearchColumn('company');
	//columns[1] = new nlobjSearchColumn('firstname');
	//columns[2] = new nlobjSearchColumn('internalid');
	columns[0] = new nlobjSearchColumn('entityid');
	columns[1] = new nlobjSearchColumn('title');
	
	var result = nlapiSearchRecord('contact',null,filters,columns);
	nlapiLogExecution('DEBUG', 'In contact search', 'result=' + result);
	if (result != null) 
	{
		//var contactget = result[0].getValue('company')
		//nlapiLogExecution('DEBUG', 'In contact search', 'contactget=' + contactget);
		
		//var name = result[0].getValue('firstname')
		//nlapiLogExecution('DEBUG', 'In contact search', 'name=' + name);
		
		//var int_id = result[0].getValue('internalid');
		//nlapiLogExecution('DEBUG', 'In contact search', 'int_id=' + int_id);
		
		return result;
	}
	
	
}	

function toWordsFunc(s)
{
   // alert('In conversion function')
    var str=''

    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}	 

function nullcheckingfield(value)
{
  if (value != null && value != '' && value != 'undefined') 
	{
		value = value;
	}
  else 
	{
		value = '';
	}

return value;
}	

function numberWithCommas(nStr) 
{
	
    nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) 
	{
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
} 
function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Messgae with and =====" + messgaeToBeSendParaAnd);
 
  messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
 
  return messgaeToBeSendParaAnd;
} 	 
//msg += "<td  border=\"0.1\"  width=\"5%\" height=\"15\"  style=\"border-right-style:none;border-top-style:none;border-bottom-style:none;font-size:11;\"  >" + Particuar + "<br/><p style=\"font-size:8\" align=\"left\">"+itemdescription+"<\/p> <img width=\"116px\" height=\"87px\" align=\"right\" src=\"" + itemimage + " \" \/><\/td> ";

