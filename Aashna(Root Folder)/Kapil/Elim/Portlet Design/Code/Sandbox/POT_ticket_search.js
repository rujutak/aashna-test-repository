//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:POT_ticket_search.js
	 Author:Kapil
	 Company:Aashna
	 Date:29-08-13
	 Version:0.1
	 Description:Portlet on ticket search
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	  ticket_search(portlet,column)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 


     //BEGIN PORTLET
function ticket_search(portlet, column)
{
	var contextObj = nlapiGetContext();
	var param = contextObj.getSetting('SCRIPT', 'custscript_search_id');
	
	portlet.setTitle("Ticket List")
	
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('mainline',null,'is','T');
	
	var results = nlapiSearchRecord('supportcase', param, filters ,null);
    nlapiLogExecution('DEBUG','Portlet','results=='+results)
	if (results != null) 
	{
			var res = results[0];
			nlapiLogExecution('DEBUG','Portlet','res=='+res)
			var returncolumns = res.getAllColumns();
			//nlapiLogExecution('DEBUG','Portlet','returncolumns=='+returncolumns)
			var columnLen = returncolumns.length;
			//nlapiLogExecution('DEBUG','Portlet','columnLen=='+columnLen)
			var label = new Array();
			var formula = new Array();
			var value = new Array();
			var type = new Array();
			var name = new Array();
			var a=1
			for(var j=0;j<columnLen;j++)
			{
				var column = returncolumns[j];
				nlapiLogExecution('DEBUG','Portlet','column in for=='+column)
				
				name = column.getName();
				nlapiLogExecution('DEBUG','Portlet','name=='+name)
				
				label = column.getLabel();
				//var label = column.getLabel();
				nlapiLogExecution('DEBUG','Portlet','label=='+label)
				
			    formula = column.getFormula();
				nlapiLogExecution('DEBUG','Portlet','formula=='+formula)
				
			    value = res.getValue(column);
				nlapiLogExecution('DEBUG','Portlet','value=='+value)
				
				type = column.getType();
				nlapiLogExecution('DEBUG','Portlet','type=='+type)
				
				if(type=='select')
				{
					type='text'
					name=name+'_display'
				}
				if(type=='datetime')
				{
					type='date'
					name=name;
				}
				if(type=='datetimetz')
				{
					type='date'
					name=name;
				}
				if(type=='checkbox')
				{
					type='text'
					name=name;
				}
				portlet.addColumn(name,type,label)
				a=parseInt(a)+1
				nlapiLogExecution('DEBUG','Portlet','a=='+a)
			}
			
			
		//----------------------------------------------------------------
		
	     //var row = portlet.addRows(results)
		
		//----------------------------------------------------------------------
		 var count=0
		  if(results.length>=50)
		  {
		  	count=50
		  }
		  else
		  {
		  	count=results.length
		  }
		
		
			for (var i = 0; i < count; i++) 
			{
				var row = portlet.addRow(results[i])
				//nlapiLogExecution('DEBUG','Portlet','row=='+row)
				//break;
			}
			//nlapiLogExecution('DEBUG','Portlet','i=='+i)
	    
	}
	
}

