//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_tendersubmit_copy.js
	 Date:24-06-13
	 Version:0.1
	 Description:Get all values of tender prep submission ,and set that value on copied record
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 tender_submit_beforeLoad(type)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY
function tender_submit_beforeLoad(type,form,request)
{
  var Flag = 0;
  if (type == 'create') 
  {
  	//Begin Code : for getting tender prep submission data from the url
	nlapiLogExecution('DEBUG','In IF','inside if')
	var param = request.getParameter('custscript_tender_submit');
	nlapiLogExecution('DEBUG','In IF','param='+param);
	
	
			//var urlStr = window.location.href.toString(param).toLowerCase();
			//nlapiLogExecution('DEBUG','In IF','urlStr='+urlStr);
			//var urlStr = window.location.href
			//alert('urlStr='+urlStr)
			//if ((urlStr.indexOf("?")) > -1) 
			{
				//alert('urlStr.indexOf='+urlStr.indexOf("?"))
				//var parameterList = urlStr.substr((urlStr.indexOf("?") + 1), urlStr.length);
				//alert('parameterList='+parameterList)
				//var paramArray = parameterList.split("&");
				
				//if (paramArray != 'undefined' && paramArray != null && paramArray != '') 
				{
					//alert('paramArray length='+paramArray.length)
					
					/*//for (var i = 0; i < paramArray.length; i++) 
					{
						//alert('paramArray []='+ paramArray[i])	
						var Temp = paramArray[i].split("=");
						//alert('Temp[0]='+ Temp[0]);
						//alert('Temp[1]='+ Temp[1]);
						if (Temp[0] == 'custscriptchklist') 
						{
							//alert('if Temp[0]='+ Temp[0])
							//alert('if Temp[1]='+ Temp[1])
							Flag = 1
							//alert('Flag='+ Flag)
							break;
						}
						
					}*/
					
					//if (Flag == 1) 
					{
						//alert('Flag==' + Flag);
						//if (Temp[1] != null && Temp[1] != 'undefined' && Temp[1] != '') 
						{
						//alert('Temp==' + Temp);
							
                            //**********Getting Field data********************
							
						if (param != null && param != '' && param != 'undefined') 
						{
							//var tender_rec = nlapiLoadRecord('customrecord_tenderprepsubmissionchklist', Temp[1]);
							var tender_rec = nlapiLoadRecord('customrecord_tenderprepsubmissionchklist', param);
							nlapiLogExecution('DEBUG', 'Make Copy', 'tender_rec=' + tender_rec)
							//alert('tender_rec==' + tender_rec);
							
							var opportunity_no = tender_rec.getFieldValue('custrecord_tp_opportunityno');
							nlapiLogExecution('DEBUG', 'Make Copy', 'opportunity_no=' + opportunity_no)
							//alert('opportunity_no==' + opportunity_no);
							
							var title = tender_rec.getFieldValue('custrecord_title');
							nlapiLogExecution('DEBUG', 'Make Copy', 'title=' + title)
							//alert('title==' + title);
							
							var Tender_Submission_Date = tender_rec.getFieldValue('custrecord_tendersubmissiondate');
							nlapiLogExecution('DEBUG', 'Make Copy', 'Tender_Submission_Date=' + Tender_Submission_Date)
							//alert('Tender_Submission_Date==' + Tender_Submission_Date);
							
							var Prepared_By = tender_rec.getFieldValue('custrecord_preparedby');
							//alert('Prepared_By==' + Prepared_By);
							
							var Location = tender_rec.getFieldValue('custrecord_location');
							//alert('Location==' + Location);
							
							var Client = tender_rec.getFieldValue('custrecord_client');
							//alert('Client==' + Client);
							
							var Consultant = tender_rec.getFieldValue('custrecord_consultant');
							//alert('Consultant==' + Consultant);
							
							var Quantity_Surveyor = tender_rec.getFieldValue('custrecord_quantitysurveyor');
							//alert('Quantity_Surveyor==' + Quantity_Surveyor);
							
							var Decision_To_Proceed = tender_rec.getFieldValue('custrecord_decisiontoproceed');
							//alert('Decision_To_Proceed==' + Decision_To_Proceed);
							
							var Decided_By = tender_rec.getFieldValue('custrecord_decidedby');
							//alert('Decided_By==' + Decided_By);
							
							var Status = tender_rec.getFieldValue('custrecord_statustenprepsubchklist');
							//alert('Status==' + Status);
							
							
							//**********Setting Field data********************
							
							
							var set_opportunity_no = nlapiSetFieldValue('custrecord_tp_opportunityno', opportunity_no, false, false);
							//alert('set_opportunity_no==' + set_opportunity_no);
							
							var set_title = nlapiSetFieldValue('custrecord_title', title, false, false);
							//alert('set_title==' + set_title);
							
							var set_Tender_Submission_Date = nlapiSetFieldValue('custrecord_tendersubmissiondate', Tender_Submission_Date, false, false);
							//alert('Tender_Submission_Date==' + Tender_Submission_Date);
							
							var set_Prepared_By = nlapiSetFieldValue('custrecord_preparedby', Prepared_By, false, false);
							//alert('set_Prepared_By==' + set_Prepared_By);
							
							var set_Location = nlapiSetFieldValue('custrecord_location', Location, false, false);
							//alert('set_Location==' + set_Location);
							
							var set_Client = nlapiSetFieldValue('custrecord_client', Client, false, false);
							//alert('set_Client==' + set_Client);
							
							var set_Consultant = nlapiSetFieldValue('custrecord_consultant', Consultant, false, false);
							//alert('set_Consultant==' + set_Consultant);
							
							var set_Quantity_Surveyor = nlapiSetFieldValue('custrecord_quantitysurveyor', Quantity_Surveyor, false, false);
							//alert('set_Quantity_Surveyor==' + set_Quantity_Surveyor);
							
							var set_Decision_To_Proceed = nlapiSetFieldValue('custrecord_decisiontoproceed', Decision_To_Proceed, false, false);
							//alert('set_Decision_To_Proceed==' + set_Decision_To_Proceed);
							
							var set_Decided_By = nlapiSetFieldValue('custrecord_decidedby', Decided_By, false, false);
							//alert('set_Decided_By==' + set_Decided_By);
							
							var set_Status = nlapiSetFieldValue('custrecord_statustenprepsubchklist', Status, false, false);
							//alert('set_Status==' + set_Status);
							
							
							//**********Getting and Setting Line items********************
							
							var p_linecount = tender_rec.getLineItemCount('recmachcustrecord_tpscl_p_reference');
							//alert('linecount==' + p_linecount);
							
							
							//var Temp = 1;
							
							for (var i = 1; i <= tender_rec.getLineItemCount('recmachcustrecord_tpscl_p_reference'); i++) 
							{
							
								var sno = tender_rec.getLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_serialno', i);
								nlapiLogExecution('DEBUG', 'sno', 'sno=' + sno);
								
								var particulars = tender_rec.getLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_particulars', i);
								//alert('particulars==' + particulars);
								
								var target_date = tender_rec.getLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_targetdate', i);
								//alert('target_date==' + target_date);
								
								var remarks = tender_rec.getLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_remarks', i);
								//alert('remarks==' + remarks);
								
								var reference = tender_rec.getLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_reference', i);
								//alert('reference==' + reference);		
								
								//alert('i==' + i);
								//nlapiSelectLineItem('recmachcustrecord_tpscl_p_reference',parseInt(i))
								//alert('reference==' + reference);	
								nlapiSelectNewLineItem('recmachcustrecord_tpscl_p_reference')
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_serialno', sno, true, true);
								//nlapiLogExecution('DEBUG','Client script','serialno')
								//nlapiLogExecution('DEBUG','Client script','serialno')
								
								
								
								
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_particulars', particulars, true, true);
								//nlapiLogExecution('DEBUG','Client script','particulars')
								//nlapiLogExecution('DEBUG','Client script','particulars')
								
								
								
								
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_targetdate', target_date, true, true);
								//nlapiLogExecution('DEBUG','Client script','targetdate')
								//nlapiLogExecution('DEBUG','Client script','targetdate')
								
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_remarks', remarks, true, true);
								//nlapiLogExecution('DEBUG','Client script','remarks')
								//nlapiLogExecution('DEBUG','Client script','remarks')
								
								
								
								
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_p_reference', 'custrecord_pd_reference', reference, true, true);
								//nlapiLogExecution('DEBUG','Client script','reference')
								//nlapiLogExecution('DEBUG','Client script','reference')
								
								
								
								
								
								
								nlapiCommitLineItem('recmachcustrecord_tpscl_p_reference');
								
							//Temp++;
							}
							
							//nlapiSetCurrentLineItemValue()
							
							var d_linecount = tender_rec.getLineItemCount('recmachcustrecord_tpscl_d_reference')
							//alert('d_linecount==' + d_linecount);
							
							
							
							//var Temp2= 1;
							
							
							for (var j = 1; j <= tender_rec.getLineItemCount('recmachcustrecord_tpscl_d_reference'); j++) 
							{
								var d_sno = tender_rec.getLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_serialno', j);
								//alert('d_sno==' + d_sno);
								var d_documentname = tender_rec.getLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_documentname', j);
								//alert('d_documentname==' + d_documentname);
								var d_yesno = tender_rec.getLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_yesno', j);
								//alert('d_yesno==' + d_yesno);
								var d_remarks = tender_rec.getLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_remarks', j);
								//alert('d_remarks==' + d_remarks);
								
								//nlapiSelectLineItem('recmachcustrecord_tpscl_d_reference',j);
								nlapiSelectNewLineItem('recmachcustrecord_tpscl_d_reference');
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_serialno', d_sno, true, true);
								//nlapiLogExecution('DEBUG','Client script','serialno')
								//nlapiLogExecution('DEBUG','Client script','serialno')
								
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_documentname', d_documentname, true, true);
								//nlapiLogExecution('DEBUG','Client script','documentname')
								//nlapiLogExecution('DEBUG','Client script','documentname')
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_yesno', d_yesno, true, true);
								//nlapiLogExecution('DEBUG','Client script','yesno')
								//nlapiLogExecution('DEBUG','Client script','yesno')
								
								
								
								
								
								nlapiSetCurrentLineItemValue('recmachcustrecord_tpscl_d_reference', 'custrecord_da_remarks', d_remarks, true, true);
								//nlapiLogExecution('DEBUG','Client script','remarks')
								//nlapiLogExecution('DEBUG','Client script','remarks')
								
								
								
								
								nlapiCommitLineItem('recmachcustrecord_tpscl_d_reference');
								
							//Temp2++;
							}
							
							
						}
							 
						}
						
					}
					
				}
				
			}
			
	}
	
		
	
}
    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
	 
	 /* On After Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN AFTER SUBMIT CODE BODY

	
    //END AFTER SUBMIT