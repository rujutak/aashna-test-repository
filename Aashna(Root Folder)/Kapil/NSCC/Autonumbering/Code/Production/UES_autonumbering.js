//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_autonumbering.js
	 Date:05-06-13
	 Version:0.1
	 Description:Autonumbering on multiple records
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 purchase_order_beforeload(type,form)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY


    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
function aftersubmit_autonumber(type)
{
	/* On After Submit:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES

	//END LOCAL VARIABLES

	//BEGIN AFTER SUBMIT CODE BODY
	var user = nlapiGetUser();
	
	if (type == 'create') 
	{
		var recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'recordType' + recordType);
		var recordId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'recordId' + recordId);
		
		var currentRecord = nlapiLoadRecord(recordType, recordId);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'current record = ' + currentRecord);
		
		
		var recordtypeprefix = '';
		
		var date = new Date();
		var currentyear = date.getFullYear();
		currentyear = currentyear.toString();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentyear=' + currentyear);
		
		var year_twodigit = currentyear.substring(2,4);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentyear after split=' + year_twodigit);
		
		var currentno = getcurrentno(recordType,year_twodigit);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentno =' + newno);
		
		var newno = parseInt(currentno) + parseInt(1);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo before if=' + newno);
		
		if (newno.toString().length == 1) 
		{
			newno = '000' + newno;
		}
		if (newno.toString().length == 2) 
		{
			newno = '00' + newno;
		}
		nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo after if=' + newno);
		var noString = '';
		
		if (recordType == 'lead' || recordType == 'prospect')  
		{
			//var currentRecord = nlapiLoadRecord(recordType, recordId);
			//var location = currentRecord.getFieldValue('custentity_location');
			var location = currentRecord.getFieldValue('custentity_custom_location');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'location' + location);
			
			recordtypeprefix = getrecordtypeprefix(recordType);
			nlapiLogExecution('DEBUG', 'aftersubmit', 'recordtypeprefix =' + recordtypeprefix);
			
			var locRec = nlapiLoadRecord('customrecord_location',location);
			
			var transactionprefix = locRec.getFieldValue('custrecord_location_record_prefixsuffix');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'transactionprefix =' + transactionprefix);
			
			noString = recordtypeprefix + '-' + year_twodigit + '-' + newno + '-' + transactionprefix;
			nlapiLogExecution('DEBUG', 'aftersubmit', 'noString in lead,prospect =' + noString);
			
			var tranid = currentRecord.setFieldValue('entityid', noString);
		    nlapiLogExecution('DEBUG', 'aftersubmit', 'entityid in others =' + tranid);
			
			var currentrec_submit_others = nlapiSubmitRecord(currentRecord,false,false);
	        nlapiLogExecution('DEBUG', 'aftersubmit', 'currentrecsubmit =' + currentrec_submit_others);

		}
		else if(recordType == 'customer')
		{
			//var location = currentRecord.getFieldValue('custentity_location');
			var location = currentRecord.getFieldValue('custentity_custom_location');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'location in customer' + location);
			
			var loc_Rec = nlapiLoadRecord('customrecord_location', location);
			
			var transactionprefix = loc_Rec.getFieldValue('custrecord_location_record_prefixsuffix');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'transactionprefix in customer =' + transactionprefix);
			
			noString = newno.slice(-3) + '-' + transactionprefix;
			nlapiLogExecution('DEBUG', 'aftersubmit', 'noString in customer =' + noString);
			
			var tranid_cust = currentRecord.setFieldValue('entityid', noString);
		    nlapiLogExecution('DEBUG', 'aftersubmit', 'entityid in customer =' + tranid_cust);
			
			var currentrec_submit_customer = nlapiSubmitRecord(currentRecord,false,false);
	        nlapiLogExecution('DEBUG', 'aftersubmit', 'currentrecsubmit customer =' + currentrec_submit_customer);
		}
		
		
        if (currentno != null) 
		{
			var updatecustomrec = updateAutonomaster(recordType, newno);
			nlapiLogExecution('DEBUG', 'aftersubmit', 'update custom record =' + updatecustomrec);
		}	

		
		//nlapiLogExecution('DEBUG', 'aftersubmit', 'noString =' + noString);
		
			}

	return true;
}	
    //END AFTER SUBMIT
	

function getrecordtypeprefix(recordType)
{
    var prefix ='';
	if(recordType == 'lead')
	{
		prefix = 'L';
	}
	if(recordType == 'prospect')
	{
		prefix = 'X';
	}
	return prefix;
}
function getcurrentno(recordType,year_twodigit)
{
	var filters = new Array();
	var columns = new Array();
	
	filters[0] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recordType);
	columns[0] = new nlobjSearchColumn('custrecord_serialno');
	columns[1] = new nlobjSearchColumn('custrecord_year');
	
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var transactionNo = 0;
	var year = '';
	nlapiLogExecution('DEBUG', 'In getcurrentno', " result " + result);
	
	if(result != null)
	{
		transactionNo = result[0].getValue('custrecord_serialno');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " transaction no =" + transactionNo);
		year = result[0].getValue('custrecord_year');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " year =" + year);
	}
	
	else
	{
		var newAutonomaster = nlapiCreateRecord('customrecord_autonumbering');
		newAutonomaster.setFieldValue('custrecord_serialno',1);
		if (year_twodigit != null && year_twodigit != '' && year_twodigit != 'undefined') 
		{
			newAutonomaster.setFieldValue('custrecord_year', year_twodigit);
		}
		nlapiLogExecution('DEBUG', 'In getcurrentno', "set currentyear =" + year_twodigit);
		if(recordType == 'lead')
		{
			newAutonomaster.setFieldValue('custrecord_recordtype','lead');
		}
		if(recordType == 'prospect')
		{
			newAutonomaster.setFieldValue('custrecord_recordtype','prospect');
		}
		if(recordType == 'customer')
		{
			newAutonomaster.setFieldValue('custrecord_recordtype','customer');
		}
		var id=nlapiSubmitRecord(newAutonomaster,true,true);
		nlapiLogExecution('DEBUG', 'In getCurrentno', ' Newly Created Id =' + id);
	}
	return transactionNo;
}	


function updateAutonomaster(recordType,newno)
{
	nlapiLogExecution('DEBUG', 'In Update record', 'UPDATION');
	nlapiLogExecution('DEBUG', 'In Update record', 'recordType='+recordType);
	nlapiLogExecution('DEBUG', 'In Update record', 'newno='+newno);
	//nlapiLogExecution('DEBUG', 'In Update record', 'year_twodigit='+year_twodigit);
	
	var filters = new Array();
	var columns = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recordType);
	//filters[1] = new nlobjSearchFilter('custrecord_serialno', null, 'is', newno);
	
	columns[0] = new nlobjSearchColumn('internalid');
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var internalid = 0;
	
	if(result !=null)
	{
		internalid = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'In Update record', " internalid = " + internalid);
		
		var autoNomasterrec = nlapiLoadRecord('customrecord_autonumbering',internalid);
		var update_sno = autoNomasterrec.setFieldValue('custrecord_serialno',newno);
		nlapiLogExecution('DEBUG', 'In Update record', " updated serial no = " + update_sno);
		
		var updatedId=nlapiSubmitRecord(autoNomasterrec,true,true);
		nlapiLogExecution('DEBUG', 'In Update record', " updatedId = " + updatedId);
	}
}

