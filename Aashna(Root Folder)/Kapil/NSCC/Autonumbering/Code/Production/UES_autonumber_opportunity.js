//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:UES_autonumber_opportunity.js
	 Date:06-06-13
	 Version:0.1
	 Description:Autonumbering on opportunity
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 BEFORE LOAD
	 purchase_order_beforeload(type,form)
	 
	 BEFORE SUBMIT
	 beforesubmit(type)
	 
	 AFTER SUBMIT
	 aftersubmit(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
	 
	 //BEGIN BEFORE LOAD
	 
	 /* On Before Load:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE LOAD CODE BODY


    //END BEFORE LOAD
	
	
	//BEGIN BEFORE SUBMIT
	 
	 /* On Before Submit:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN BEFORE SUBMIT CODE BODY

	
    //END BEFORE SUBMIT
	
	
	
	//BEGIN AFTER SUBMIT
function aftersubmit_autonumber_opp(type)
{
	//var user = nlapiGetUser();
	//nlapiLogExecution('DEBUG', 'aftersubmit', 'user' + user);
	var formname = nlapiGetFieldValue('customform');
	//if (user == 15 && (type == 'edit' || type == 'create')) 
	if (type == 'create' && (formname == 100 || formname == 101))
	{
		var recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'recordType' + recordType);
		var recordId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'recordId' + recordId);
		
		var opp_rec = nlapiLoadRecord(recordType, recordId);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'current record = ' + opp_rec);
		
		
		//var par_opp = '';
		var recordtypeprefix = '';
		
		var date = new Date();
		var currentyear = date.getFullYear();
		currentyear = currentyear.toString();
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentyear=' + currentyear);
		
		var year_twodigit = currentyear.substring(2,4);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentyear after split=' + year_twodigit);
		
		var currentno = getcurrentno(recordType,year_twodigit);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'currentno =' + currentno);
		
		var newno = parseInt(currentno) + parseInt(1);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo before if=' + newno);
		
		if (newno.toString().length == 1) 
		{
		    newno = '000' + newno;
		}
		if (newno.toString().length == 2) 
		{
			newno = '00' + newno;
		}
		nlapiLogExecution('DEBUG', 'aftersubmit', 'newNo after if=' + newno);
		
		//var opp_rec = nlapiLoadRecord(recordType,recordId);
		//nlapiLogExecution('DEBUG', 'aftersubmit', 'opp_rec =' + opp_rec);
		
		var opp_location = opp_rec.getFieldValue('custbody_custom_location');
		var opp_loc_rec = nlapiLoadRecord('customrecord_location',opp_location);
		
		var opp_tranprefix = opp_loc_rec.getFieldValue('custrecord_location_record_prefixsuffix');
		nlapiLogExecution('DEBUG', 'aftersubmit', 'opp_tranprefix =' + opp_tranprefix);
		
		recordtypeprefix = 'E';
		nlapiLogExecution('DEBUG', 'aftersubmit', 'recordtypeprefix =' + recordtypeprefix);
		
	    var par_opp = opp_rec.getFieldValue('custbody_parentopportunity');
		var par_opp_txt = opp_rec.getFieldText('custbody_parentopportunity');
		//par_opp_txt = opp_rec.getFieldValue('tranid');
		var parent_opp = opp_rec.getFieldValue('tranid');
		nlapiLogExecution('DEBUG', 'aftersubmit', 'parent_opp =' + parent_opp);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'par_opp_txt =' + par_opp_txt);
		//par_opp_txt = par_opp_txt.substring(1);
		nlapiLogExecution('DEBUG', 'aftersubmit', 'par_opp =' + par_opp);
		
		
		
		if(par_opp != '' && par_opp != 'undefined' && par_opp != null)
		{
			
			var getpar_text = opp_rec.getFieldText('custbody_parentopportunity');
			nlapiLogExecution('DEBUG', 'aftersubmit', 'getpar_text before=' + getpar_text);
			
		    getpar_text = getpar_text.substr(1);
			nlapiLogExecution('DEBUG', 'aftersubmit', 'getpar_text after =' + getpar_text);
			
			
				var total_nostring = '';
				var i=0;
				var child_text = search_child_opp(par_opp,par_opp_txt);
				nlapiLogExecution('DEBUG', 'aftersubmit', 'child_text =' + child_text);
				
				var load_par_opp = nlapiLoadRecord('opportunity',par_opp);
				nlapiLogExecution('DEBUG', 'aftersubmit', 'load_par_opp =' + load_par_opp);
				
				var get_par_tranid = load_par_opp.getFieldValue('tranid')
				nlapiLogExecution('DEBUG', 'aftersubmit', 'get_par_tranid =' + get_par_tranid);
				
				//total_nostring = recordtypeprefix + '-' + year_twodigit + '-' + get_par_sno + '-' + opp_tranprefix + '-' + child_text;
				//nlapiLogExecution('DEBUG', 'aftersubmit', 'total_nostring in  opportunity =' + total_nostring);
				var total_nostring = get_par_tranid + '-' + child_text ;
				nlapiLogExecution('DEBUG', 'aftersubmit', 'total_nostring =' + total_nostring);
				
			//}
			
			//nostring_opp = recordtypeprefix + '-' + year_twodigit + '-' + newno + '-' + opp_tranprefix + '-' + total_parent_rec[i++];
			
			var tranid = opp_rec.setFieldValue('tranid', total_nostring);
	        nlapiLogExecution('DEBUG', 'aftersubmit', 'entityid in if opportunity =' + tranid);
			
			
			
		}
		else 
		{
			nostring_opp = recordtypeprefix + '-' + year_twodigit + '-' + newno + '-' + opp_tranprefix;
			nlapiLogExecution('DEBUG', 'aftersubmit', 'noString in else opportunity =' + nostring_opp);
			
			var tran_id = opp_rec.setFieldValue('tranid', nostring_opp);
	        nlapiLogExecution('DEBUG', 'aftersubmit', 'entityid in else opportunity =' + tran_id);
			
			
		}
		
		
		var currentoppsubmit = nlapiSubmitRecord(opp_rec,false,false);
	    nlapiLogExecution('DEBUG', 'aftersubmit', 'currentoppsubmit =' + currentoppsubmit);
			
		if (par_opp == null) 
		{
			var updatecustomrec = updateAutonomaster(recordType, newno);
			nlapiLogExecution('DEBUG', 'aftersubmit', 'update custom record =' + updatecustomrec);
		}	

	}
	return true;
}

function getcurrentno(recordType, year_twodigit)
{
	var filters = new Array();
	var columns = new Array();
	
	filters[0] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recordType);
	columns[0] = new nlobjSearchColumn('custrecord_serialno');
	columns[1] = new nlobjSearchColumn('custrecord_year');
	
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var transactionNo = 0;
	var year = '';
	nlapiLogExecution('DEBUG', 'In getcurrentno', " result " + result);
	
	if (result != null) 
	{
		transactionNo = result[0].getValue('custrecord_serialno');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " transaction no =" + transactionNo);
		year = result[0].getValue('custrecord_year');
		nlapiLogExecution('DEBUG', 'In getCurrentno', " year =" + year);
	}
	else 
	{
		var newAutonomaster = nlapiCreateRecord('customrecord_autonumbering');
		newAutonomaster.setFieldValue('custrecord_serialno', 1);
		if (year_twodigit != null && year_twodigit != '' && year_twodigit != 'undefined') 
		{
			newAutonomaster.setFieldValue('custrecord_year', year_twodigit);
		}
		nlapiLogExecution('DEBUG', 'In getcurrentno', "set currentyear =" + year_twodigit);
		
		if (recordType == 'opportunity') 
		{
			newAutonomaster.setFieldValue('custrecord_recordtype', 'opportunity');
		}
		var id = nlapiSubmitRecord(newAutonomaster, true, true);
		nlapiLogExecution('DEBUG', 'In getCurrentno', ' Newly Created Id =' + id);
	}
	return transactionNo;
}

function updateAutonomaster(recordType,newno)
{
	nlapiLogExecution('DEBUG', 'In Update record', 'UPDATION');
	nlapiLogExecution('DEBUG', 'In Update record', 'recordType='+recordType);
	nlapiLogExecution('DEBUG', 'In Update record', 'newno='+newno);
	//nlapiLogExecution('DEBUG', 'In Update record', 'year_twodigit='+year_twodigit);
	
	var filters = new Array();
	var columns = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recordType);
	//filters[1] = new nlobjSearchFilter('custrecord_serialno', null, 'is', newno);
	
	columns[0] = new nlobjSearchColumn('internalid');
	var result = nlapiSearchRecord('customrecord_autonumbering', null, filters, columns);
	var internalid = 0;
	
	if(result !=null)
	{
		internalid = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'In Update record', " internalid = " + internalid);
		
		var autoNomasterrec = nlapiLoadRecord('customrecord_autonumbering',internalid);
		var update_sno = autoNomasterrec.setFieldValue('custrecord_serialno',newno);
		nlapiLogExecution('DEBUG', 'In Update record', " updated serial no = " + update_sno);
		
		var updatedId=nlapiSubmitRecord(autoNomasterrec,true,true);
		nlapiLogExecution('DEBUG', 'In Update record', " updatedId = " + updatedId);
	}
}
/*
function searchopp(par_opp)
{
	//nlapiLogExecution('DEBUG', 'In searchopp', 'nostring_opp='+nostring_opp);
	nlapiLogExecution('DEBUG', 'In searchopp', 'par_opp='+par_opp);
	var filters = new Array();
	var columns = new Array();
	//filters[0] = new nlobjSearchFilter('tranid', null, 'is', nostring_opp);
	filters[0] = new nlobjSearchFilter('custbody_parentopportunity', null, 'is', par_opp);
	columns[0] = new nlobjSearchColumn('internalid');
	
	
	var result = nlapiSearchRecord('opportunity', null, filters, columns);
	nlapiLogExecution('DEBUG', 'In search opp', " result = " + result);
	var internal_id = 0;
	if (result != null)
    {
		internal_id = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'In search opp', " internal_id = " + internal_id);
		
	}
	return internal_id;
}
*/

/*
function search_par_sno(par_opp_txt,par_opp)
{
	nlapiLogExecution('DEBUG', 'In search_par_sno', 'par_opp_txt='+par_opp_txt);
	nlapiLogExecution('DEBUG', 'In search_par_sno', 'par_opp='+par_opp)
	var filters = new Array();
	var columns = new Array();
	
	//nlapiLogExecution('DEBUG', 'In search_par_sno', 'par_opp='+par_opp);
	
	filters[0] = new nlobjSearchFilter('internalid', null, 'is', par_opp);
	//filters[1] = new nlobjSearchFilter('custbody_parentopportunity', null, 'anyof', '');
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('tranid');
	
	var result = nlapiSearchRecord('opportunity', null, filters, columns);
	nlapiLogExecution('DEBUG', 'In search_par_sno', " result = " + result);
	
	if (result != null)
	{
		var sno = result[0].getValue('tranid');
		sno = sno.split('-');
		var child_number = sno[2];
		nlapiLogExecution('DEBUG', 'In search_par_sno', " child_number = " + child_number);
	}
	return child_number;
}
*/

function search_child_opp(par_opp,par_opp_txt)
{
    nlapiLogExecution('DEBUG', 'In search_par_sno', " par_opp = " + par_opp);
	nlapiLogExecution('DEBUG', 'In search_par_sno', " par_opp_txt = " + par_opp_txt);	
	var filters = new Array();
	var columns = new Array();
	
	//filters[0] = new nlobjSearchFilter('internalid', null, 'is', par_opp);
	//filters[0] = new nlobjSearchFilter('tranid', null, 'contains', par_opp_txt,null);
	
    filters[0] = new nlobjSearchFilter('custbody_parentopportunity', null, 'is', par_opp);
	columns[0] = new nlobjSearchColumn('internalid');
	//var result = nlapiSearchRecord('opportunity', null, filters, columns);
	var result = nlapiSearchRecord('opportunity', null, filters, columns);
	
	
	nlapiLogExecution('DEBUG', 'In search_child_opp', " result = " + result);
	var child_opp_text = '';
	/*
if(result == null)
	{
		child_opp_text = 'a';
	}
*/
	if (result != null)
	{
		//var int_id = result[0].getValue('internalid');
		//nlapiLogExecution('DEBUG', 'In search_child_opp', " int_id = " + int_id);
		var res_length = result.length;
		nlapiLogExecution('DEBUG', 'In search_child_opp', " result = " + res_length);
		for(var j = 0; j<res_length; j++)
		{
			
			//par_opp_txt = par_opp_txt.split('-');
			//nlapiLogExecution('DEBUG', 'In search_child_opp', " result = " + res_length);
			//var child_opp_text = par_opp_txt[4];
			
            if(res_length == 1)
			{
				child_opp_text = 'a';
			}

			else if(res_length ==2 )
			{
				child_opp_text = 'b';
			}
			
			else if(res_length == 3)
			{
				child_opp_text = 'c';
			}
			else if(res_length == 4)
			{
				child_opp_text = 'd';
			}
			else if(res_length == 5)
			{
				child_opp_text = 'e';
			}
			else if(res_length == 6)
			{
				child_opp_text = 'f';
			}
			else if(res_length == 7)
			{
				child_opp_text = 'g';
			}
			else if(res_length == 8)
			{
				child_opp_text = 'h';
			}
			else if(res_length == 9)
			{
				child_opp_text = 'i';
			}
			else if(res_length == 10)
			{
				child_opp_text = 'j';
			}
			else if(res_length == 11)
			{
				child_opp_text = 'k';
			}
			else if(res_length == 12)
			{
				child_opp_text = 'l';
			}
			else if(res_length == 13)
			{
				child_opp_text = 'm';
			}
			else if(res_length == 14)
			{
				child_opp_text = 'n';
			}
			else if(res_length == 15)
			{
				child_opp_text = 'o';
			}
			else if(res_length == 16)
			{
				child_opp_text = 'p';
			}
			else if(res_length == 17)
			{
				child_opp_text = 'q';
			}
			else if(res_length == 18)
			{
				child_opp_text = 'r';
			}
			else if(res_length == 19)
			{
				child_opp_text = 's';
			}
			else if(res_length == 20)
			{
				child_opp_text = 't';
			}
			else if(res_length == 21)
			{
				child_opp_text = 'u';
			}
			else if(res_length == 22)
			{
				child_opp_text = 'v';
			}
			else if(res_length == 23)
			{
				child_opp_text = 'w';
			}
			else if(res_length == 24)
			{
				child_opp_text = 'x';
			}
			else if(res_length == 25)
			{
				child_opp_text = 'y';
			}
			else if(res_length == 26)
			{
				child_opp_text = 'z';
			}
		}
	}
	return child_opp_text;
}
