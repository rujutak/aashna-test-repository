﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data_Migration_Automation.NetSuiteWebReference;
//using Data_Migration_Automation.ServiceReference2012;
using System.Configuration;


namespace Data_Migration_Automation
{
    class Utility
    {
        public Passport GetSourcePassport()
        {
            Passport passportobj = new Passport();
            passportobj.account = ConfigurationManager.AppSettings["Account"];
            passportobj.email = ConfigurationManager.AppSettings["Email"];
            passportobj.password = ConfigurationManager.AppSettings["Password"];

            return passportobj;
        }

        public Passport GetDestinationePassport()
        {
            Passport passportobj = new Passport();
            passportobj.account = ConfigurationManager.AppSettings["DestAccount"];
            passportobj.email = ConfigurationManager.AppSettings["DestEmail"];
            passportobj.password = ConfigurationManager.AppSettings["DestPassword"];

            return passportobj;
        }

        public Preferences GetPreferences()
        {
            Preferences prefs = new Preferences();
            prefs.ignoreReadOnlyFields = true;
            prefs.ignoreReadOnlyFieldsSpecified = true;
            prefs.warningAsError = true;
            prefs.warningAsErrorSpecified = true;
            prefs.disableMandatoryCustomFieldValidation = true;
            prefs.disableMandatoryCustomFieldValidationSpecified = true;

            return prefs;
        }
    }
}
