﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibApi;
//using LibApi.NetSuitePortType;
using Data_Migration_Automation.NetSuiteWebReference;
//using Data_Migration_Automation.ServiceReference2012;
using System.Configuration;
//using Data_Migration_Automation.ServiceReference2012;

namespace Data_Migration_Automation
{
    class Program
    {
        static NetSuitePortTypeClient _client = new NetSuitePortTypeClient("NetSuitePort");
        public static Logger logger = new Logger("Data Migration-SO", "info");
        public static Logger error_logger = new Logger("Data Migration-Error", "info");
        public static Utility US = new Utility();
        public static bool error_flag = false;
        public static bool lot_flag = false;

        static void Main(string[] args)
        {
            string recordID = "", soid = "", sointernalID = "", itemFulfilmentInternalID = "", invoiceID = "", customerPaymentID = "";
            int i = 1;

            logger.info("Application Started !");
            logger.info("*******************************");
            error_logger.info("Application Started !");
            error_logger.info("*******************************");
            logger.info("Getting data from saved search ID : " + ConfigurationManager.AppSettings["SavedSearchID"]);

            logger.info(string.Format("Using SNA userID : {0}\n Using DNA usrID : {1}"
                , ConfigurationManager.AppSettings["Email"]
                , ConfigurationManager.AppSettings["DestEmail"]));

            migrateSO("3210");
            migrateItemFulfillment("3210");
            //migrateInvoice("6655");
            //migrate_NON_SO_Invoice("4367");
            //migrate_All_CustomerPayment("7894");

            //GetUnitSearch("B5");
            //GetPostingPeriod();
            //migrateInvoice("18024");
            //get_taxCodeInternalID_DNA("");
            //GetUnitSearch("B5");
            //getSelectoptionsForUnit("3091", "4162");
            //getSelectoptions_exp("3091", "4162");

            #region Deletetion code

            //            //string x = "02-015579,02-015639,02-015711,IF-AZS-0000000026,IF-AZS-0000000028,IF-AZS-0000000029,IF-AZS-0000000031,IF-AZS-0000000193,IF-AZS-0000000199,IF-AZS-0000000201,IF-AZS-0000000207,IF-AZS-0000000291,IF-AZS-0000000312,IF-AZS-0000000410,IF-AZS-0000000439,IF-AZS-0000000447,IF-AZS-0000000453,IF-AZS-0000000501,IF-AZS-0000000540,IF-AZS-0000000543,IF-AZS-0000000551,IF-AZS-0000000553,IF-AZS-0000000567,IF-AZS-0000000570,IF-AZS-0000000579,IF-AZS-0000000584,IF-AZS-0000000587,IF-AZS-0000000589,IF-AZS-0000000592,IF-AZS-0000000594,IF-AZS-0000000619,IF-AZS-0000000622,IF-AZS-0000000650,IF-AZS-0000000702,IF-AZS-0000000786,IF-AZS-0000000828,IF-AZS-0000000835,IF-AZS-0000000836,IF-AZS-0000000838,IF-AZS-0000000901,IF-AZS-0000000924,IF-AZS-0000000927,IF-AZS-0000000973,IF-AZS-0000001041,IF-AZS-0000001050,IF-AZS-0000001140,IF-AZS-0000001166,IF-AZS-0000001195,IF-AZS-0000001263,IF-AZS-0000001326,IF-AZS-0000001359,IF-AZS-0000001409,IF-AZS-0000001497,IF-AZS-0000001506,IF-AZS-0000001528";
            //            //string x = "02-015799,IF-AZS-0000001479,IF-AZS-0000001464,IF-AZS-0000001524,IF-AZS-0000000726,IF-AZS-0000001484,IF-AZS-0000001484,02-015579,02-015639";
            //string x = "IF-AZS-0000001071,IF-AZS-0000000641,IF-AZS-0000001160,IF-AZS-0000000850,IF-AZS-0000001141,IF-AZS-0000000728,IF-AZS-0000000093,02-015579,02-015639,IF-AZS-0000000578,IF-AZS-0000000607,IF-AZS-0000000603,IF-AZS-0000001142,IF-AZS-0000000409";
            //string internalidofIF = GetItemfulfillment(x,"D");
            ////string x = "02-015639,02-015579";

            //////Boolean ismigratedbutnotcheck = GetItemfulfillmentfromDNa(x,);

            ////string[] irs = x.Split(',');
            ////foreach (string y in irs)
            ////{
            ////    string internalid = GetItemfulfillmentfromDNa(y);
            ////    string snainternalid = GetItemfulfillmentfromSNA(y);
            ////    if (internalid != null)
            ////    {
            ////        Boolean flag = DeleteIF(internalid);
            ////        if (flag != true)
            ////        {
            ////            UpdateMigrationStatusUncheck(snainternalid);
            ////        }
            ////        else
            ////        {
            ////            continue;
            ////        }

            ////    }
            ////}

            #endregion

            var result = getAllSourceSO(_client, logger, US);   // Fetching all Sales order

            try
            {
                string record_type = "";
                var searchData = (from p in result.searchRowList
                                  select p).ToList();

                logger.info(string.Format("{0} Searched records found", searchData.Count));
                logger.info("*******************************!");

                string error_transaction = "";

                foreach (var item in searchData)
                {
                    logger.info(string.Format("Record {0} is processing ",
                                               ((TransactionSearchRow)item).basic.internalId[0].searchValue.internalId));

                    recordID = ((TransactionSearchRow)item).basic.internalId[0].searchValue.internalId;        // get record ID of saved search

                    if (error_transaction == recordID)
                    {
                        logger.info("Aleady processed this transaction, ID : " + recordID);
                        continue;
                    }

                    error_transaction = recordID;

                    record_type = ((TransactionSearchRow)item).basic.type[0].searchValue.ToString();    // get record type of saved search
                    lot_flag = false;
                    string tran_type = "";
                    if (record_type == "_itemFulfillment" || record_type == "_invoice")
                    {
                        var search_Item = (TransactionSearchRow)item;

                        if (search_Item.createdFromJoin != null)
                        {
                            tran_type = ((TransactionSearchRow)item).createdFromJoin.type[0].searchValue.ToString();    // get record type of saved search
                        }
                    }

                    if (record_type.Trim() == "_salesOrder")
                    {
                        #region Start from SalesOrder

                        sointernalID = recordID;
                        logger.info("\n");

                        migrateSO(sointernalID);
                        logger.info("\n\n");

                        if (error_flag == true)
                        {
                            error_flag = false;
                            error_transaction = sointernalID;
                            i++;
                            continue;
                        }

                        migrateItemFulfillment(sointernalID);       // migration of Item Fulfillment
                        logger.info("\n\n");

                        if (error_flag == true)
                        {
                            error_flag = false;
                            error_transaction = sointernalID;
                            i++;
                            continue;
                        }

                        migrateInvoice(sointernalID);       // migration of Invoice
                        logger.info("\n\n");

                        if (error_flag == true)
                        {
                            error_flag = false;
                            error_transaction = sointernalID;
                            i++;
                            continue;
                        }

                        #endregion
                    }
                    else
                    {
                        if (record_type == "_itemFulfillment" && tran_type == "_salesOrder")
                        {
                            #region Start from Item Fulfillment

                            string SO_ID = ((TransactionSearchRow)item).basic.createdFrom[0].searchValue.internalId;

                            SalesOrder SNA_SO = getSOFromDNA(SO_ID);

                            if (SNA_SO != null)
                            {
                                //if SO is present.

                                migrateItemFulfillment(SO_ID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }

                                continue;

                                migrateInvoice(SO_ID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }

                                ////migrateCustomerPayment(invoiceID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }
                            }
                            else
                            {
                                //if SO is not present.

                                migrateSO(SO_ID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }

                                migrateItemFulfillment(SO_ID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }

                                migrateInvoice(SO_ID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }

                                ////migrateCustomerPayment(invoiceID);

                                if (error_flag == true)
                                {
                                    error_flag = false;
                                    error_transaction = sointernalID;
                                    i++;
                                    continue;
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            if (record_type == "_invoice")
                            {
                                #region Start from Invoice

                                string SO_ID = null;

                                try
                                {
                                    var search_Item = (TransactionSearchRow)item;

                                    if (search_Item.basic.createdFrom != null)
                                        SO_ID = ((TransactionSearchRow)item).basic.createdFrom[0].searchValue.internalId;
                                }
                                catch (Exception ex)
                                {

                                }

                                if (SO_ID == null)      // Invoice without Sales Order Migration.
                                {
                                    string invoice_ID = SO_ID = ((TransactionSearchRow)item).basic.internalId[0].searchValue.internalId;

                                    invoiceID = migrate_NON_SO_Invoice(invoice_ID);

                                    if (error_flag == true)
                                    {
                                        error_flag = false;
                                        error_transaction = sointernalID;
                                        i++;
                                        continue;
                                    }

                                    //migrateCustomerPayment(invoiceID);

                                    if (error_flag == true)
                                    {
                                        error_flag = false;
                                        error_transaction = sointernalID;
                                        i++;
                                        continue;
                                    }
                                }
                                else                // Invoice With Sales Order
                                {
                                    SalesOrder SNA_SO = getSOFromDNA(SO_ID);

                                    if (SNA_SO != null)     //if SO is present.
                                    {
                                        logger.info(string.Format("SO({0}) is present.", SO_ID));

                                        ItemFulfillment SNA_IF = getDNA_IF(SO_ID);

                                        if (SNA_IF != null)
                                        {
                                            // if Item fulfillment is present

                                            migrateInvoice(SO_ID);

                                            if (error_flag == true)
                                            {
                                                error_flag = false;
                                                error_transaction = sointernalID;
                                                i++;
                                                continue;
                                            }

                                            ////migrateCustomerPayment(invoiceID);

                                            if (error_flag == true)
                                            {
                                                error_flag = false;
                                                error_transaction = sointernalID;
                                                i++;
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            // if Item fulfillment is not present 

                                            //migrateItemFulfillment(SO_ID);

                                            if (error_flag == true)
                                            {
                                                error_flag = false;
                                                error_transaction = sointernalID;
                                                i++;
                                                continue;
                                            }

                                            migrateInvoice(SO_ID);

                                            if (error_flag == true)
                                            {
                                                error_flag = false;
                                                error_transaction = sointernalID;
                                                i++;
                                                continue;
                                            }

                                            ////migrateCustomerPayment(invoiceID);

                                            if (error_flag == true)
                                            {
                                                error_flag = false;
                                                error_transaction = sointernalID;
                                                i++;
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //if SO is not present.

                                        migrateInvoice(SO_ID);

                                        if (error_flag == true)
                                        {
                                            error_flag = false;
                                            error_transaction = sointernalID;
                                            i++;
                                            continue;
                                        }
                                        continue;



                                        migrateSO(SO_ID);

                                        if (error_flag == true)
                                        {
                                            error_flag = false;
                                            error_transaction = sointernalID;
                                            i++;
                                            continue;
                                        }

                                        migrateItemFulfillment(SO_ID);

                                        if (error_flag == true)
                                        {
                                            error_flag = false;
                                            error_transaction = sointernalID;
                                            i++;
                                            continue;
                                        }

                                        migrateInvoice(SO_ID);

                                        if (error_flag == true)
                                        {
                                            error_flag = false;
                                            error_transaction = sointernalID;
                                            i++;
                                            continue;
                                        }

                                        ////migrateCustomerPayment(invoiceID);

                                        if (error_flag == true)
                                        {
                                            error_flag = false;
                                            error_transaction = sointernalID;
                                            i++;
                                            continue;
                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                if (record_type == "_customerPayment")
                                {
                                    #region Start from Customer Payment

                                    string Cust_pay_ID = ((TransactionSearchRow)item).basic.internalId[0].searchValue.internalId;

                                    migrate_All_CustomerPayment(Cust_pay_ID);
                                    logger.info("\n");
                                    #region OLD Flow
                                    ////migrateCustomerPayment(Cust_pay_ID);

                                    //SalesOrder SNA_SO = getSOFromDNA(SO_ID);

                                    //if (SNA_SO != null)
                                    //{
                                    //    //if SO is present.

                                    //    ItemFulfillment SNA_IF = getDNA_IF(SO_ID);

                                    //    if (SNA_IF != null)
                                    //    {
                                    //        // if Item fulfillment is present

                                    //        Invoice SNA_Invoice = getDNAInvoice(SO_ID);

                                    //        if (SNA_Invoice != null)
                                    //        {
                                    //            // if Invoice is present
                                    ////migrateCustomerPayment(SNA_Invoice.internalId);

                                    //            if (error_flag == true)
                                    //                return;
                                    //        }
                                    //        else
                                    //        {
                                    //            // if Invoice is not present
                                    //            invoiceID = migrateInvoice(SO_ID);

                                    //            if (error_flag == true)
                                    //                return;

                                    //            //migrateCustomerPayment(invoiceID);

                                    //            if (error_flag == true)
                                    //                return;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        // if Item fulfillment is not present 

                                    //        migrateItemFulfillment(SO_ID);

                                    //        if (error_flag == true)
                                    //            return;

                                    //        invoiceID = migrateInvoice(SO_ID);

                                    //        if (error_flag == true)
                                    //            return;

                                    //        //migrateCustomerPayment(invoiceID);

                                    //        if (error_flag == true)
                                    //            return;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    //if SO is not present.

                                    //    migrateSO(SO_ID);

                                    //    if (error_flag == true)
                                    //        return;

                                    //    migrateItemFulfillment(SO_ID);

                                    //    if (error_flag == true)
                                    //        return;

                                    //    invoiceID = migrateInvoice(SO_ID);

                                    //    if (error_flag == true)
                                    //        return;

                                    //    //migrateCustomerPayment(invoiceID);

                                    //    if (error_flag == true)
                                    //        return;
                                    //} 
                                    #endregion

                                    #endregion
                                }
                            }
                        }
                    }
                    logger.info(string.Format("Record number {0} is processed.", i));
                    logger.info("*-*-*-*-*-*-*-*-*-*-*.\n\n");
                    i++;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error in main() : " + ex.Message);
            }

            logger.info("All records are processed...!!!");
            Console.Read();
        }

        #region Migration Of SO

        private static string migrateSO(SearchRow item, string sonum)
        {
            Preferences pref = new Preferences() { disableMandatoryCustomFieldValidation = true, disableMandatoryCustomFieldValidationSpecified = true, ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
            WriteResponse wr = new WriteResponse();
            string transferId = "";
            SalesOrder SO, SNA_SO = null;

            try
            {
                logger.info(string.Format("Reading Sales order record..."));
                SNA_SO = GetSOFromWebService(sonum);

                if (SNA_SO == null)
                {
                    return null;
                }
                else
                {
                    SO = SNA_SO;
                }

                bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_SO.customFieldList
                                         where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                         select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                if (IsMigrated == true)
                {
                    logger.info("Sales Order Already Migrated in DNA.");
                    return SNA_SO.internalId;
                }

                SO = SNA_SO;
                var soid = SO.internalId;

                #region Trial Code

                //SalesOrder sonew = new SalesOrder();
                //sonew.externalId = soid;
                //sonew.customForm = new RecordRef() { internalId = "68", type = RecordType.salesOrder, typeSpecified = true };
                //sonew.startDate = SO.startDate;
                //sonew.endDate = SO.endDate;
                //sonew.job = SO.job;
                //sonew.orderStatus = SO.orderStatus;
                //sonew.otherRefNum = SO.otherRefNum;
                //sonew.memo = SO.memo;
                //sonew.salesRep = SO.salesRep;
                //sonew.opportunity = SO.opportunity;
                //sonew.salesEffectiveDate = SO.salesEffectiveDate;
                //sonew.excludeCommission = SO.excludeCommission;
                //sonew.leadSource = SO.leadSource;
                //sonew.partner = SO.partner;
                ////sonew.location = SO.location;
                //sonew.department = SO.department;
                //sonew.billAddress = SO.billAddress;
                //sonew.createdDate = SO.createdDate;
                //sonew.createdDateSpecified = true;
                //sonew.email = SO.email;
                //sonew.entity = SO.entity;
                //sonew.exchangeRate = SO.exchangeRate;
                //sonew.exchangeRateSpecified = true;
                //sonew.fax = SO.fax;

                //sonew.total = SO.total;
                //sonew.totalSpecified = true;
                //sonew.tranDate = SO.tranDate;
                //sonew.tranDateSpecified = true;
                //sonew.tranId = SO.tranId;
                //sonew.transactionShipAddress = SO.transactionShipAddress;
                //sonew.itemList = SO.itemList;

                #endregion

                if (SO.entity != null)
                    SO.entity = new RecordRef() { internalId = get_EntityInternalID_DNA(SO.entity.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                if (SO.job != null)
                    SO.job = new RecordRef() { internalId = get_JobInternalID_DNA(SO.job.internalId, ConfigurationManager.AppSettings["CustomProjectField"]), type = RecordType.job, typeSpecified = true };

                if (SO.salesRep != null)
                    SO.salesRep = new RecordRef() { internalId = get_SalesrepInternalID_DNA(SO.salesRep.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]), type = RecordType.salesRole, typeSpecified = true };

                //if (SO.leadSource != null)
                //SO.leadSource = new RecordRef() { internalId = get_EntityInternalID_DNA(SO.leadSource.internalId, ConfigurationManager.AppSettings["CustomLeadSourceField"]), type = RecordType.leadSource, typeSpecified = true };

                if (SO.partner != null)
                    SO.partner = new RecordRef() { internalId = get_PartnerInternalID_DNA(SO.partner.internalId, ConfigurationManager.AppSettings["CustomPartnerField"]), type = RecordType.partner, typeSpecified = true };

                if (SO.location != null)
                    SO.location = new RecordRef() { internalId = get_LocationInternalID_DNA(SO.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                if (SO.department != null)
                    SO.department = new RecordRef() { internalId = get_DepartmentInternalID_DNA(SO.department.internalId, ConfigurationManager.AppSettings["CustomDepartmentField"]), type = RecordType.department, typeSpecified = true };

                if (SO.subsidiary != null)
                    SO.subsidiary = new RecordRef() { internalId = get_subsidiaryInternalID_DNA(SO.subsidiary.internalId, ConfigurationManager.AppSettings["CustomsubsidiaryField"]), type = RecordType.subsidiary, typeSpecified = true };

                if (SO.@class != null)
                    SO.@class = new RecordRef() { internalId = get_classInternalID_DNA(SO.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                #region Set Custom Fields

                // set initiator field

                List<CustomFieldRef> customref = new List<CustomFieldRef>();
                StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = soid };
                customref.Add(customField);

                var custfld_initiator = (from f in SO.customFieldList
                                         where typeof(SelectCustomFieldRef) == f.GetType() &&
                                         ((SelectCustomFieldRef)f).internalId == "custbody_initiator"
                                         select f).FirstOrDefault();

                string initiator = get_SalesrepInternalID_DNA(((SelectCustomFieldRef)custfld_initiator).value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                customref.Add(customField_initiator);

                // set LPO date
                var custfld_LPODate = (from f in SO.customFieldList
                                       where typeof(DateCustomFieldRef) == f.GetType() &&
                                       ((DateCustomFieldRef)f).internalId == "custbody_lpo_date"
                                       select f).FirstOrDefault();

                DateCustomFieldRef customField_LPO_Date = new DateCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomLPODateField"], value = ((DateCustomFieldRef)custfld_LPODate).value };
                customref.Add(customField_LPO_Date);

                SO.customFieldList = customref.ToArray();

                #endregion

                logger.info("Reading Sales order Item record...");
                List<SalesOrderItem> soitem = new List<SalesOrderItem>();
                foreach (var itemn in SO.itemList.item)
                {
                    #region Trial Code
                    //var sitem = new SalesOrderItem();
                    //sitem.amount = itemn.amount;
                    //sitem.amountSpecified = true;
                    //sitem.description = itemn.description;
                    //sitem.grossAmt = itemn.grossAmt;
                    //sitem.grossAmtSpecified = true;
                    //itemn.item = itemn.item;
                    //sitem.quantity = itemn.quantity;
                    //sitem.quantityReceived = itemn.quantityReceived;
                    //sitem.quantityReceivedSpecified = true;
                    //sitem.rate = itemn.rate;
                    //sitem.poVendor = itemn.poVendor; 
                    #endregion

                    string SNA_itemID = itemn.item.internalId;
                    //string pricename = itemn.price.name;

                    //itemn.price = new RecordRef() { name = pricename, type = RecordType.priceLevel, typeSpecified = true };

                    string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                    itemn.item.internalId = DNA_itemID;  // assign item internal id.

                    if (itemn.@class != null)
                        itemn.@class = new RecordRef() { internalId = get_classInternalID_DNA(itemn.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                    if (itemn.location != null)
                        itemn.location = new RecordRef() { internalId = get_LocationInternalID_DNA(itemn.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                    if (itemn.units != null)
                        itemn.units = new RecordRef() { internalId = GetUnitSearch(itemn.units.name), type = RecordType.unitsType, typeSpecified = true };

                    if (itemn.customFieldList != null)
                    {
                        var custfld = (from f in itemn.customFieldList
                                       where typeof(SelectCustomFieldRef) == f.GetType() &&
                                       ((SelectCustomFieldRef)f).internalId == "custcol_salesrep"
                                       select f).FirstOrDefault();
                        if (custfld != null)
                        {
                            string id = get_SalesrepInternalID_DNA(((SelectCustomFieldRef)custfld).value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);
                            ((SelectCustomFieldRef)custfld).value.internalId = id;
                        }
                        else
                        {
                            //_logger.info(string.Format("custom field 'itemtoclose' is not found on SO."));
                            //return false;
                        }

                        var custUnitType_Name = (from f in itemn.customFieldList
                                                 where typeof(SelectCustomFieldRef) == f.GetType() &&
                                                 ((SelectCustomFieldRef)f).internalId == "custcol_unitsforprinting"
                                                 select f).FirstOrDefault();
                        if (custUnitType_Name != null)
                        {
                            string Unit_internalID = GetUnitSearch(((SelectCustomFieldRef)custfld).value.name);
                            ((SelectCustomFieldRef)custfld).value = new ListOrRecordRef() { internalId = Unit_internalID };
                        }
                        else
                        {
                            //_logger.info(string.Format("custom field 'itemtoclose' is not found on SO."));
                            //return false;
                        }
                    }

                    soitem.Add(itemn);
                }

                SO.itemList = new SalesOrderItemList();
                SO.itemList.item = soitem.ToArray();

                foreach (var teamItem in SO.salesTeamList.salesTeam)
                {
                    if (teamItem.employee != null)
                    {
                        teamItem.employee.internalId = get_SalesrepInternalID_DNA(teamItem.employee.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);
                    }
                }

                // setting internal ID of SNA So to DNA so's external ID
                soid = SO.internalId;
                SO.externalId = SO.internalId;
                SO.internalId = null;

                SO.orderStatus = SalesOrderOrderStatus._pendingFulfillment;
                //adding the PO to destination account
                logger.info("Adding to DNA Sales order record...");

                _client.add(US.GetDestinationePassport(), null, null, pref, SO, out wr);

                if (!wr.status.isSuccess)
                {
                    logger.info(2, string.Format("Error at migrateSO()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                    var error = string.Format("Error at migrateSO()\n Sales order transfer is failed.\nDetail :{0}", wr.status.statusDetail[0].message);
                    logger.error(2, error);
                    error_flag = true;
                }
                else
                {
                    transferId = ((RecordRef)(wr.baseRef)).internalId;
                    var msg = string.Format("SO is transferred with internal id {0}.", transferId);

                    SNA_SO.internalId = soid;
                    UpdateMigrationStatus(SNA_SO);
                }
                logger.info(string.Format("Transfer Sales order record completed."));

                return soid;
            }
            catch (Exception ex)
            {
                var orderNo = ((TransactionSearchRow)item).basic.tranId[0].searchValue;
                var error = string.Format("Error at migrateSO()\nProcessPendingOrders Exception while reading Order No: {0}\nDetail: {1}\n{2}",
                                          orderNo, ex.Message, ex.StackTrace);
                logger.error(error);
                error_flag = true;
                return null;
            }
        }

        private static string migrateSO(string sonum)
        {
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
            WriteResponse wr = new WriteResponse();
            string transferId = "", irtransferid = "";
            SalesOrder SO, SNA_SO = null;
            try
            {
                logger.info(string.Format("Reading Sales order record..."));

                SNA_SO = GetSOFromWebServiceForIF(sonum);

                if (SNA_SO == null)
                {
                    return null;
                }
                else
                {
                    SO = SNA_SO;
                }

                bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_SO.customFieldList
                                         where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                         select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                if (IsMigrated == true)
                {
                    logger.info("Sales Order Already Migrated in DNA.");
                    return SNA_SO.internalId;
                }

                GetSelectFilterByFieldValue val = new GetSelectFilterByFieldValue() { field = "SNA Taxcode ID", internalId = "custrecord_snataxcodeid" };
                GetSelectValueFieldDescription SalesDes = new GetSelectValueFieldDescription { recordType = RecordType.salesTaxItem, recordTypeSpecified = true, field = "Tax Code" };

                GetSelectValueResult wres = new GetSelectValueResult();
                _client.getSelectValue(US.GetDestinationePassport(), null, null, pref, SalesDes, 1, out wres);

                var soid = SO.internalId;

                #region Trial Code

                //SalesOrder sonew = new SalesOrder();
                //sonew.externalId = soid;
                //sonew.customForm = new RecordRef() { internalId = "68", type = RecordType.salesOrder, typeSpecified = true };
                //sonew.startDate = SO.startDate;
                //sonew.endDate = SO.endDate;
                //sonew.job = SO.job;
                //sonew.orderStatus = SO.orderStatus;
                //sonew.otherRefNum = SO.otherRefNum;
                //sonew.memo = SO.memo;
                //sonew.salesRep = SO.salesRep;
                //sonew.opportunity = SO.opportunity;
                //sonew.salesEffectiveDate = SO.salesEffectiveDate;
                //sonew.excludeCommission = SO.excludeCommission;
                //sonew.leadSource = SO.leadSource;
                //sonew.partner = SO.partner;
                ////sonew.location = SO.location;
                //sonew.department = SO.department;
                //sonew.billAddress = SO.billAddress;
                //sonew.createdDate = SO.createdDate;
                //sonew.createdDateSpecified = true;
                //sonew.email = SO.email;
                //sonew.entity = SO.entity;
                //sonew.exchangeRate = SO.exchangeRate;
                //sonew.exchangeRateSpecified = true;
                //sonew.fax = SO.fax;

                //sonew.total = SO.total;
                //sonew.totalSpecified = true;
                //sonew.tranDate = SO.tranDate;
                //sonew.tranDateSpecified = true;
                //sonew.tranId = SO.tranId;
                //sonew.transactionShipAddress = SO.transactionShipAddress;
                //sonew.itemList = SO.itemList;

                //logger.info("Reading Sales order Item record...");
                //List<SalesOrderItem> soitem = new List<SalesOrderItem>();
                //foreach (var itemn in SO.itemList.item)
                //{
                //    var sitem = new SalesOrderItem();
                //    sitem.amount = itemn.amount;
                //    sitem.amountSpecified = true;
                //    sitem.description = itemn.description;
                //    sitem.grossAmt = itemn.grossAmt;
                //    sitem.grossAmtSpecified = true;
                //    sitem.item = itemn.item;
                //    sitem.quantity = itemn.quantity;
                //    //sitem.quantityReceived = itemn.quantityReceived;
                //    //sitem.quantityReceivedSpecified = true;
                //    sitem.rate = itemn.rate;
                //    sitem.poVendor = itemn.poVendor;
                //    soitem.Add(sitem);
                //}

                //sonew.itemList = new SalesOrderItemList();
                //sonew.itemList.item = soitem.ToArray();
                //sonew.entity = SO.entity; 

                #endregion

                if (SO.entity != null)
                    SO.entity = new RecordRef() { internalId = get_EntityInternalID_DNA(SO.entity.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                if (SO.job != null)
                    SO.job = new RecordRef() { internalId = get_JobInternalID_DNA(SO.job.internalId, ConfigurationManager.AppSettings["CustomProjectField"]), type = RecordType.job, typeSpecified = true };

                if (SO.salesRep != null)
                    SO.salesRep = new RecordRef() { internalId = get_SalesrepInternalID_DNA(SO.salesRep.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]), type = RecordType.salesRole, typeSpecified = true };

                //if (SO.leadSource != null)
                //    SO.leadSource = new RecordRef() { internalId = get_EntityInternalID_DNA(SO.leadSource.internalId, ConfigurationManager.AppSettings["CustomLeadSourceField"]), type = RecordType.leadSource, typeSpecified = true };

                if (SO.partner != null)
                    SO.partner = new RecordRef() { internalId = get_PartnerInternalID_DNA(SO.partner.internalId, ConfigurationManager.AppSettings["CustomPartnerField"]), type = RecordType.partner, typeSpecified = true };

                if (SO.location != null)
                    SO.location = new RecordRef() { internalId = get_LocationInternalID_DNA(SO.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                if (SO.department != null)
                    SO.department = new RecordRef() { internalId = get_DepartmentInternalID_DNA(SO.department.internalId, ConfigurationManager.AppSettings["CustomDepartmentField"]), type = RecordType.department, typeSpecified = true };

                if (SO.@class != null)
                    SO.@class = new RecordRef() { internalId = get_classInternalID_DNA(SO.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                if (SO.subsidiary != null)
                    SO.subsidiary = new RecordRef() { internalId = get_subsidiaryInternalID_DNA(SO.subsidiary.internalId, ConfigurationManager.AppSettings["CustomsubsidiaryField"]), type = RecordType.subsidiary, typeSpecified = true };



                #region Set Custom Fields

                List<CustomFieldRef> customList = SO.customFieldList.ToList();

                //var custbody_ordertype = (from f in SO.customFieldList
                //                          where typeof(SelectCustomFieldRef) == f.GetType() &&
                //                          ((SelectCustomFieldRef)f).internalId == "custbody_ordertype"
                //                          select f).FirstOrDefault();
                //if (custbody_ordertype != null)
                //{
                //    SelectCustomFieldRef customField_custbody_ordertype = new SelectCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["custbody_ordertype"],
                //        value = new ListOrRecordRef() { name = ((SelectCustomFieldRef)custbody_ordertype).value.name }
                //    };
                //    //customref.Add(custbody_ordertype);
                //}

                if (SO.customFieldList.Count() > 0)
                {
                    for (int c = 0; c < SO.customFieldList.Count(); c++)
                    {
                        if (SO.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                        {
                            SelectCustomFieldRef customRec = (SelectCustomFieldRef)SO.customFieldList[c];
                            if (customRec.internalId == "custbody_initiator")
                            {
                                customRec.value.internalId = get_SalesrepInternalID_DNA(customRec.value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                                SO.customFieldList[c] = customRec;
                                continue;
                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                            }
                        }


                        if (SO.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                        {
                            SelectCustomFieldRef customRec = (SelectCustomFieldRef)SO.customFieldList[c];
                            if (customRec.internalId == "custbody_last_itemfulfillment")
                            {
                                customRec = null;

                                SO.customFieldList[c] = customRec;
                                continue;
                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                            }
                        }

                        if (SO.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                        {
                            SelectCustomFieldRef customRec = (SelectCustomFieldRef)SO.customFieldList[c];
                            if (customRec.internalId == "custbody_last_purchaseorder")
                            {
                                customRec = null;

                                SO.customFieldList[c] = customRec;
                                continue;
                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                            }
                        }

                        if (SO.customFieldList[c].GetType() == typeof(MultiSelectCustomFieldRef))
                        {
                            MultiSelectCustomFieldRef customRec = (MultiSelectCustomFieldRef)SO.customFieldList[c];
                            if (customRec.internalId == "custbody_linked_pos")
                            {
                                customRec = null;

                                SO.customFieldList[c] = customRec;
                                continue;
                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                            }
                        }

                        if (SO.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                        {
                            SelectCustomFieldRef customRec = (SelectCustomFieldRef)SO.customFieldList[c];
                            if (customRec.internalId == "custbody_last_invoice")
                            {
                                customRec = null;

                                SO.customFieldList[c] = customRec;
                                continue;
                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                            }
                        }
                    }
                }

                List<CustomFieldRef> customref = SO.customFieldList.ToList();
                StringCustomFieldRef customField = new StringCustomFieldRef()
                {
                    internalId = ConfigurationManager.AppSettings["CustomTransactionField"],
                    value = soid
                };
                customref.Add(customField);
                SO.customFieldList = customref.ToArray();

                // set initiator field
                #region Seperate Assigning

                //var custfld_initiator = (from f in SO.customFieldList
                //                         where typeof(SelectCustomFieldRef) == f.GetType() &&
                //                         ((SelectCustomFieldRef)f).internalId == "custbody_initiator"
                //                         select f).FirstOrDefault();

                //string initiator = get_SalesrepInternalID_DNA(((SelectCustomFieldRef)custfld_initiator).value.internalId,
                //    ConfigurationManager.AppSettings["CustomSalesRepField"]);
                //if (custfld_initiator != null)
                //{
                //    SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["CustomInitiatorField"],
                //        value = new ListOrRecordRef() { internalId = initiator }
                //    };
                //    customref.Add(customField_initiator);
                //}
                //// set LPO date
                //var custfld_LPODate = (from f in SO.customFieldList
                //                       where typeof(DateCustomFieldRef) == f.GetType() &&
                //                       ((DateCustomFieldRef)f).internalId == "custbody_lpo_date"
                //                       select f).FirstOrDefault();
                //if (custfld_LPODate != null)
                //{
                //    DateCustomFieldRef customField_LPO_Date = new DateCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["CustomLPODateField"],
                //        value = ((DateCustomFieldRef)custfld_LPODate).value
                //    };
                //    customref.Add(customField_LPO_Date);
                //}
                //// set custbody_total_inwords
                //var custbody_total_inwords = (from f in SO.customFieldList
                //                              where typeof(StringCustomFieldRef) == f.GetType() &&
                //                              ((StringCustomFieldRef)f).internalId == "custbody_total_inwords"
                //                              select f).FirstOrDefault();
                //if (custbody_total_inwords != null)
                //{
                //    StringCustomFieldRef customField_custbody_total_inwords = new StringCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["custbody_total_inwords"],
                //        value = ((StringCustomFieldRef)custbody_total_inwords).value
                //    };
                //    customref.Add(customField_custbody_total_inwords);
                //}

                //// set custbody_ordertype

                //// set custbody_requires_training
                //var custbody_requires_training = (from f in SO.customFieldList
                //                                  where typeof(BooleanCustomFieldRef) == f.GetType() &&
                //                                  ((BooleanCustomFieldRef)f).internalId == "custbody_requires_training"
                //                                  select f).FirstOrDefault();

                //BooleanCustomFieldRef customField_custbody_requires_training = new BooleanCustomFieldRef()
                //{
                //    internalId = ConfigurationManager.AppSettings["custbody_requires_training"],
                //    value = ((BooleanCustomFieldRef)custbody_requires_training).value
                //};
                //customref.Add(customField_custbody_requires_training);

                //// set custbody_requires_installation
                //var custbody_requires_installation = (from f in SO.customFieldList
                //                                      where typeof(BooleanCustomFieldRef) == f.GetType() &&
                //                                      ((BooleanCustomFieldRef)f).internalId == "custbody_requires_installation"
                //                                      select f).FirstOrDefault();

                //BooleanCustomFieldRef customField_custbody_requires_installation = new BooleanCustomFieldRef()
                //{
                //    internalId = ConfigurationManager.AppSettings["custbody_requires_installation"],
                //    value = ((BooleanCustomFieldRef)custbody_requires_installation).value
                //};
                //customref.Add(customField_custbody_requires_installation);

                //// set custbody_instalcompldate
                //var custbody_instalcompldate = (from f in SO.customFieldList
                //                                where typeof(DateCustomFieldRef) == f.GetType() &&
                //                                      ((DateCustomFieldRef)f).internalId == "custbody_instalcompldate"
                //                                select f).FirstOrDefault();

                //if (custbody_instalcompldate != null)
                //{
                //    DateCustomFieldRef customField_custbody_instalcompldate = new DateCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["custbody_instalcompldate"],
                //        value = ((DateCustomFieldRef)custbody_instalcompldate).value
                //    };
                //    customref.Add(customField_custbody_instalcompldate);
                //}
                //// set custbody_instalcompldate
                //var custbody_trngcompldate = (from f in SO.customFieldList
                //                              where typeof(DateCustomFieldRef) == f.GetType() &&
                //                                    ((DateCustomFieldRef)f).internalId == "custbody_trngcompldate"
                //                              select f).FirstOrDefault();

                //if (custbody_trngcompldate != null)
                //{
                //    DateCustomFieldRef customField_custbody_trngcompldate = new DateCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["custbody_trngcompldate"],
                //        value = ((DateCustomFieldRef)custbody_trngcompldate).value
                //    };
                //    customref.Add(customField_custbody_trngcompldate);
                //}
                //// set custbody_order_weight
                //var custbody_order_weight = (from f in SO.customFieldList
                //                             where typeof(StringCustomFieldRef) == f.GetType() &&
                //                                   ((StringCustomFieldRef)f).internalId == "custbody_order_weight"
                //                             select f).FirstOrDefault();

                //if (custbody_order_weight != null)
                //{
                //    StringCustomFieldRef customField_custbody_order_weight = new StringCustomFieldRef()
                //    {
                //        internalId = ConfigurationManager.AppSettings["custbody_order_weight"],
                //        value = ((StringCustomFieldRef)custbody_order_weight).value
                //    };
                //    customref.Add(customField_custbody_order_weight);
                //}
                //SO.customFieldList = customref.ToArray(); 
                #endregion

                #endregion

                List<SalesOrderItem> soitem = new List<SalesOrderItem>();
                List<SalesTaxItem> asd = GetAllCurrencies();

                foreach (var itemn in SO.itemList.item)
                {
                    #region Trial Code
                    //var sitem = new SalesOrderItem();
                    //sitem.amount = itemn.amount;
                    //sitem.amountSpecified = true;
                    //sitem.description = itemn.description;
                    //sitem.grossAmt = itemn.grossAmt;
                    //sitem.grossAmtSpecified = true;
                    //itemn.item = itemn.item;
                    //sitem.quantity = itemn.quantity;
                    //sitem.quantityReceived = itemn.quantityReceived;
                    //sitem.quantityReceivedSpecified = true;
                    //sitem.rate = itemn.rate;
                    //sitem.poVendor = itemn.poVendor; 
                    #endregion

                    string SNA_itemID = itemn.item.internalId;
                    string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                    itemn.item.internalId = DNA_itemID;  // assign item internal id.

                    if (itemn.taxCode != null)
                    {
                        foreach (var taxname in asd)
                        {
                            if (taxname.itemId == itemn.taxCode.name.Substring(7))
                            {
                                itemn.taxCode.internalId = taxname.internalId;
                            }
                        }
                    }

                    if (itemn.@class != null)
                        itemn.@class = new RecordRef() { internalId = get_classInternalID_DNA(itemn.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                    if (itemn.location != null)
                        itemn.location = new RecordRef() { internalId = get_LocationInternalID_DNA(itemn.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                    if (itemn.units != null)
                    {
                        BaseRef[] bas = getSelectoptions_exp(itemn.item.internalId, SO.entity.internalId);

                        if (bas != null)
                        {
                            foreach (var b in bas)
                            {
                                if (b.name == itemn.units.name)
                                {
                                    RecordRef new_rec = (RecordRef)b;

                                    itemn.units = new_rec;
                                }
                            }
                        }
                    }

                    if (itemn.customFieldList != null)
                    {
                        var custfld = (from f in itemn.customFieldList
                                       where typeof(SelectCustomFieldRef) == f.GetType() &&
                                       ((SelectCustomFieldRef)f).internalId == "custcol_salesrep"
                                       select f).FirstOrDefault();
                        if (custfld != null)
                        {
                            string id = get_SalesrepInternalID_DNA(((SelectCustomFieldRef)custfld).value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);
                            var selected = ((SelectCustomFieldRef)custfld).value = new ListOrRecordRef() { internalId = id };
                        }
                    }

                    soitem.Add(itemn);
                }

                SO.itemList = new SalesOrderItemList();
                SO.itemList.item = soitem.ToArray();

                if (SO.salesTeamList != null)
                {
                    foreach (var teamItem in SO.salesTeamList.salesTeam)
                    {
                        if (teamItem.employee != null)
                        {
                            teamItem.employee.internalId = get_SalesrepInternalID_DNA(teamItem.employee.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);
                        }
                    }
                }

                //if (SO.shipMethod!=null && SO.shipMethod.internalId == "2")
                //{
                //    SO.shipMethod = new RecordRef() { internalId = "6037" };
                //}

                //adding the PO to destination account
                logger.info("Adding to DNA Sales order record...");

                soid = SO.internalId;
                SO.externalId = SO.internalId;
                SO.internalId = null;

                _client.add(US.GetDestinationePassport(), null, null, pref, SO, out wr);

                if (!wr.status.isSuccess)
                {
                    if (wr.status.statusDetail[0].code.ToString() == "USER_ERROR")
                    {
                        error_flag = false;
                        logger.info(2, string.Format("Error code: {0}", wr.status.statusDetail[0].code.ToString()));
                        var error1 = string.Format("Item Fulfillment is failed.\nDetail :{0}", wr.status.statusDetail[0].message);
                        logger.error(2, error1);
                        lot_flag = true;
                    }
                    else
                    {
                        logger.info(2, string.Format("Error at migrateSO()\nError code: {0}", wr.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at migrateSO()\nSales order transfer is failed.\nDetail :{0}", wr.status.statusDetail[0].message);
                        logger.error(2, error);
                        error_flag = true;
                    }
                }
                else
                {
                    transferId = ((RecordRef)(wr.baseRef)).internalId;
                    var msg = string.Format("SO is transferred with internal id {0}.", transferId);

                    SNA_SO.internalId = soid;
                    UpdateMigrationStatus(SNA_SO);
                }
                logger.info(string.Format("Reading Sales order record complete."));

                return soid;
            }
            catch (Exception ex)
            {
                var orderNo = sonum;
                var error = string.Format("Error at migrateSO()\nProcessPendingOrders Exception while reading Order No: {0}\nDetail: {1}\n{2}",
                                          orderNo, ex.Message, ex.StackTrace);
                logger.error(error);
                error_flag = true;
                return null;
            }
        }

        #endregion

        #region Migration of Item Fulfillment

        private static void migrateItemFulfillment(string sointernalID)
        {
            logger.info("Searching For Item Fulfillment Records for SO No." + sointernalID);
            try
            {
                #region Variable Declaration

                ItemFulfillment SNA_IF, DNA_IF, SNA_IF_NEW = new ItemFulfillment();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
                WriteResponse wr = new WriteResponse();
                string transferId = "", SNA_InternalID = "";
                SearchResult result = new SearchResult();
                int resultCount;

                #endregion

                result = getAllItemFulfillmentRecords(sointernalID);     //retrieve Item fulfillment data using Sales Order's Internal ID.
                int result_Count = 0;
                if (result != null)
                {
                    result_Count = result.totalRecords;
                }
                else
                {
                    return;
                }

                for (int i = 0; i < result_Count; i++)
                {
                    if (result.recordList[i].GetType() == typeof(ItemFulfillment))
                    {
                        SNA_IF = (ItemFulfillment)(result.recordList[i]);

                        SNA_IF_NEW = SNA_IF;
                        SNA_InternalID = SNA_IF.internalId;

                        if (SNA_IF == null)
                        {
                            return;
                        }
                        else
                        {
                            DNA_IF = SNA_IF;
                        }

                        //      Boolean ismigratedbutnotcheck = GetItemfulfillmentfromDNa(SNA_IF.tranId.ToString());

                        //      if (ismigratedbutnotcheck == true)
                        //      {
                        //          Boolean check = DeleteIF(SNA_IF.internalId);
                        //          if (check != true)
                        //          {

                        //          }
                        //          //UpdateMigrationStatus(SNA_IF);

                        //      }

                        bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_IF.customFieldList
                                                 where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                                 select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                        if (IsMigrated == true)
                        {
                            logger.info("Item Fulfillment Already Migrated in DNA." + SNA_IF.tranId);
                            continue;
                        }

                        DateTime tranDate = DNA_IF.tranDate;

                        List<ItemFulfillmentItem> IFItem = new List<ItemFulfillmentItem>();

                        foreach (var itemn in DNA_IF.itemList.item)
                        {
                            string SNA_itemID = itemn.item.internalId;
                            string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                            itemn.item.internalId = DNA_itemID;  // assign item internal id.
                            //itemn.quantityRemaining = 0;
                            itemn.quantityRemainingSpecified = false;
                            itemn.quantitySpecified = true;
                            IFItem.Add(itemn);
                        }

                        DNA_IF.itemList = new ItemFulfillmentItemList();
                        DNA_IF.itemList.item = IFItem.ToArray();
                        DNA_IF.itemList.replaceAll = false;

                        ItemFulfillmentShipStatus ship_status = DNA_IF.shipStatus;

                        logger.info("Adding to DNA Item Fulfillment record...");

                        string itemFulfilmentInternalID = SNA_IF.internalId;        //store IF internal ID to variable
                        DNA_IF.externalId = itemFulfilmentInternalID;               //set External ID of DNA = Internal ID of SNA.
                        DNA_IF.internalId = null;                                   //set internal ID to null for inertion.
                        DNA_IF.postingPeriod = null;                                // set Posing period will be null
                        DNA_IF.createdFrom = new RecordRef() { externalId = sointernalID, type = RecordType.salesOrder, typeSpecified = true };    //setting DNA Sales order's internal id = created from of DNA.

                        List<CustomFieldRef> customref = DNA_IF.customFieldList.ToList();
                        StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = SNA_InternalID };
                        customref.Add(customField);
                        DNA_IF.customFieldList = customref.ToArray();

                        ReadResponse read1 = null;
                        ItemFulfillment New_IF = new ItemFulfillment();
                        InitializeRef iref = new InitializeRef() { type = InitializeRefType.salesOrder, typeSpecified = true, externalId = sointernalID };
                        InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.itemFulfillment };
                        _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                        if (!read1.status.isSuccess)
                        {
                            logger.info("Error at migrateIF initialize : " + read1.status.statusDetail[0].message);
                            logger.info("IF ID: " + SNA_IF.tranId);
                            return;
                        }
                        else
                        {
                            DNA_IF = read1.record as ItemFulfillment;
                        }

                        List<ItemFulfillmentItem> IFItemfrdna = new List<ItemFulfillmentItem>();
                        int sna_Count = 0;

                        foreach (var dna_item in DNA_IF.itemList.item)
                        {
                            bool flag = false;
                            foreach (var sna_item in SNA_IF.itemList.item)
                            {
                                if (sna_Count != SNA_IF.itemList.item.Count())
                                {

                                    if (sna_item.item.name == dna_item.item.name)
                                    {
                                        flag = true;
                                        sna_Count++;
                                        dna_item.quantity = sna_item.quantity;
                                        dna_item.serialNumbers = sna_item.serialNumbers;
                                        dna_item.location = sna_item.location;

                                        if (dna_item.location != null)
                                        {
                                            dna_item.location = new RecordRef() { internalId = get_LocationInternalID_DNA(dna_item.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };
                                        }

                                        dna_item.itemReceive = true;
                                        dna_item.itemReceiveSpecified = true;
                                        dna_item.quantityRemainingSpecified = true;
                                        dna_item.quantitySpecified = true;
                                        dna_item.itemIsFulfilled = false;
                                        dna_item.itemIsFulfilledSpecified = true;
                                        dna_item.shipGroupSpecified = true;

                                        // dna_item.orderLineSpecified = false;

                                        IFItemfrdna.Add(dna_item);
                                        break;
                                    }
                                }

                            }
                            if (flag == false)
                            {
                                dna_item.itemReceive = false;
                                dna_item.itemReceiveSpecified = true;
                                dna_item.quantityRemainingSpecified = false;
                                dna_item.quantitySpecified = false;
                                dna_item.itemIsFulfilled = true;
                                dna_item.itemIsFulfilledSpecified = true;
                                dna_item.shipGroupSpecified = false;
                                IFItemfrdna.Add(dna_item);
                            }
                            else if (flag == true)
                            {
                                /*dna_item.itemReceive = true;
                                dna_item.itemReceiveSpecified = true;
                                dna_item.quantityRemainingSpecified = true;
                                dna_item.quantitySpecified = true;
                                dna_item.itemIsFulfilled = false;
                                dna_item.itemIsFulfilledSpecified = true;
                                dna_item.shipGroupSpecified = true;
                                IFItemfrdna.Add(dna_item);*/
                            }
                        }

                        DNA_IF.itemList = new ItemFulfillmentItemList();
                        DNA_IF.itemList.item = IFItemfrdna.ToArray();
                        DNA_IF.itemList.replaceAll = false;

                        DNA_IF.tranDate = tranDate;                 // set trandate
                        DNA_IF.shipStatus = ship_status;            // set ship status
                        DNA_IF.tranId = SNA_IF.tranId;              // set tranid
                        DNA_IF.postingPeriod = null;                // set posting period

                        _client.add(US.GetDestinationePassport(), null, null, pref, DNA_IF, out wr);

                        if (!wr.status.isSuccess)
                        {
                            if (wr.status.statusDetail[0].code.ToString() == "USER_ERROR")
                            {
                                error_flag = false;
                                logger.info(2, string.Format("Error code: {0}", wr.status.statusDetail[0].code.ToString()));
                                var error1 = string.Format("Item Fulfillment is failed.\nDetail :{0}", wr.status.statusDetail[0].message);

                                logger.error(2, error1);
                                logger.info("IF tran ID: " + DNA_IF.tranId);
                                lot_flag = true;
                            }
                            else
                            {
                                logger.info(2, string.Format("Error at migrateItemFulfillment()\n Error code: {0}",
                                    wr.status.statusDetail[0].code.ToString()));
                                var error = string.Format("Error at migrateItemFulfillment()\nItem Fulfillment transfer is failed for ID : {1}.\nDetail :{0}",
                                    wr.status.statusDetail[0].message, itemFulfilmentInternalID);
                                logger.error(2, error);

                                logger.info(string.Format("Error at migrateItemFulfillment()\nReading Item Fulfillment Incomplete.\n\n"));
                                error_flag = true;
                                return;
                            }
                        }
                        else
                        {
                            transferId = ((RecordRef)(wr.baseRef)).internalId;
                            var msg = string.Format("Item Fulfillment is transferred with internal id {0}.", transferId);
                            logger.info(string.Format("Reading Item Fulfillment complete.\n\n"));

                            SNA_IF.internalId = itemFulfilmentInternalID;
                            UpdateMigrationStatus(SNA_IF);                     // update migration status of Item fulfillment.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.info(string.Format("Error at migrateItemFulfillment()\nError : {0}\n\n", ex.Message));
                error_flag = true;
                return;
            }
        }

        private static void migrate_Single_ItemFulfillment(string IF_internalID, string SO_internalID)
        {
            logger.info("Searching For Item Fulfillment Records for ID: " + IF_internalID);

            try
            {
                #region Variable Declaration

                ItemFulfillment SNA_IF, DNA_IF, SNA_IF_BackUp = new ItemFulfillment();
                Preferences pref = new Preferences()
                {
                    ignoreReadOnlyFields = true,
                    ignoreReadOnlyFieldsSpecified = true,
                    warningAsError = false,
                    warningAsErrorSpecified = true
                };
                WriteResponse wr = new WriteResponse();
                string transferId = "", SNA_InternalID = "";

                #endregion

                SNA_IF = get_SNA_IF(IF_internalID);     //retrieve Item fulfillment data using Sales Order's Internal ID.

                if (SNA_IF == null)
                {
                    return;
                }

                //for (int i = 0; i < result_Count; i++)
                //{
                //    if (result.recordList[i].GetType() == typeof(ItemFulfillment))
                //    {

                //    }
                //}

                logger.info("IF ID: " + SNA_IF.tranId);

                bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_IF.customFieldList
                                         where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                         select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                if (IsMigrated == true)
                {
                    logger.info("Item Fulfillment Already Migrated in DNA." + SNA_IF.tranId);
                    return;
                }

                DNA_IF = SNA_IF;
                SNA_IF_BackUp = SNA_IF;
                SNA_InternalID = SNA_IF.internalId;
                DateTime tranDate = DNA_IF.tranDate;
                ItemFulfillmentShipStatus ship_status = DNA_IF.shipStatus;
                string itemFulfilmentInternalID = SNA_IF.internalId;        //store IF internal ID to variable

                List<ItemFulfillmentItem> IFItem = new List<ItemFulfillmentItem>();

                foreach (var itemn in DNA_IF.itemList.item)
                {
                    string SNA_itemID = itemn.item.internalId;
                    itemn.item.internalId = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                    itemn.quantityRemainingSpecified = false;
                    itemn.quantitySpecified = true;
                    IFItem.Add(itemn);
                }

                DNA_IF.itemList = new ItemFulfillmentItemList();
                DNA_IF.itemList.item = IFItem.ToArray();
                DNA_IF.itemList.replaceAll = false;

                DNA_IF.externalId = itemFulfilmentInternalID;               //set External ID of DNA = Internal ID of SNA.
                DNA_IF.internalId = null;                                   //set internal ID to null for inertion.
                DNA_IF.postingPeriod = null;                                // set Posing period will be null
                DNA_IF.createdFrom = new RecordRef()
                {
                    externalId = IF_internalID,
                    type = RecordType.salesOrder,
                    typeSpecified = true
                };    //setting DNA Sales order's internal id = created from of DNA.

                List<CustomFieldRef> customref = DNA_IF.customFieldList.ToList();
                StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = SNA_InternalID };
                customref.Add(customField);
                DNA_IF.customFieldList = customref.ToArray();

                ReadResponse read1 = null;

                InitializeRef iref = new InitializeRef()
                {
                    type = InitializeRefType.salesOrder,
                    typeSpecified = true,
                    externalId = SO_internalID
                };

                InitializeRecord inrec = new InitializeRecord()
                {
                    reference = iref,
                    type = InitializeType.itemFulfillment
                };

                logger.info("Initializing to DNA Item Fulfillment record...");

                _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                if (!read1.status.isSuccess)
                {
                    logger.error("Error at migrateIF initialize : " + read1.status.statusDetail[0].message);
                    logger.error("IF ID: " + SNA_IF.tranId);
                    return;
                }
                else
                {
                    DNA_IF = read1.record as ItemFulfillment;
                }

                List<ItemFulfillmentItem> IFItemfrdna = new List<ItemFulfillmentItem>();
                int sna_Count = 0;

                foreach (var dna_item in DNA_IF.itemList.item)
                {
                    bool flag = false;
                    foreach (var sna_item in SNA_IF.itemList.item)
                    {
                        if (sna_Count != SNA_IF.itemList.item.Count())
                        {

                            if (sna_item.item.name == dna_item.item.name)
                            {
                                flag = true;
                                sna_Count++;
                                dna_item.quantity = sna_item.quantity;
                                dna_item.serialNumbers = sna_item.serialNumbers;
                                dna_item.location = sna_item.location;

                                if (dna_item.location != null)
                                {
                                    dna_item.location = new RecordRef()
                                    {
                                        internalId = get_LocationInternalID_DNA(dna_item.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]),
                                        type = RecordType.location,
                                        typeSpecified = true
                                    };
                                }

                                dna_item.itemReceive = true;
                                dna_item.itemReceiveSpecified = true;
                                dna_item.quantityRemainingSpecified = true;
                                dna_item.quantitySpecified = true;
                                dna_item.itemIsFulfilled = false;
                                dna_item.itemIsFulfilledSpecified = true;
                                dna_item.shipGroupSpecified = true;

                                IFItemfrdna.Add(dna_item);
                                break;
                            }
                        }

                    }
                    if (flag == false)
                    {
                        dna_item.itemReceive = false;
                        dna_item.itemReceiveSpecified = true;
                        dna_item.quantityRemainingSpecified = false;
                        dna_item.quantitySpecified = false;
                        dna_item.itemIsFulfilled = true;
                        dna_item.itemIsFulfilledSpecified = true;
                        dna_item.shipGroupSpecified = false;
                        IFItemfrdna.Add(dna_item);
                    }
                    else if (flag == true)
                    {
                        /*dna_item.itemReceive = true;
                        dna_item.itemReceiveSpecified = true;
                        dna_item.quantityRemainingSpecified = true;
                        dna_item.quantitySpecified = true;
                        dna_item.itemIsFulfilled = false;
                        dna_item.itemIsFulfilledSpecified = true;
                        dna_item.shipGroupSpecified = true;
                        IFItemfrdna.Add(dna_item);*/
                    }
                }

                DNA_IF.itemList = new ItemFulfillmentItemList();
                DNA_IF.itemList.item = IFItemfrdna.ToArray();
                DNA_IF.itemList.replaceAll = false;

                DNA_IF.tranDate = tranDate;                 // set trandate
                DNA_IF.shipStatus = ship_status;            // set ship status
                DNA_IF.tranId = SNA_IF.tranId;              // set tranid
                DNA_IF.postingPeriod = null;                // set posting period

                _client.add(US.GetDestinationePassport(), null, null, pref, DNA_IF, out wr);

                if (!wr.status.isSuccess)
                {
                    if (wr.status.statusDetail[0].code.ToString() == "USER_ERROR")
                    {
                        error_flag = false;
                        logger.info(2, string.Format("Error code: {0}", wr.status.statusDetail[0].code.ToString()));
                        var error1 = string.Format("Item Fulfillment is failed.\nDetail :{0}", wr.status.statusDetail[0].message);

                        logger.error(2, error1);
                        logger.info("IF tran ID: " + DNA_IF.tranId);
                        lot_flag = true;
                    }
                    else
                    {
                        logger.info(2, string.Format("Error at migrateItemFulfillment()\n Error code: {0}",
                            wr.status.statusDetail[0].code.ToString()));
                        var error = string.Format("Error at migrateItemFulfillment()\nItem Fulfillment transfer is failed for ID : {1}.\nDetail :{0}",
                            wr.status.statusDetail[0].message, itemFulfilmentInternalID);
                        logger.error(2, error);

                        logger.info(string.Format("Error at migrateItemFulfillment()\nReading Item Fulfillment Incomplete.\n\n"));
                        error_flag = true;
                        return;
                    }
                }
                else
                {
                    transferId = ((RecordRef)(wr.baseRef)).internalId;
                    var msg = string.Format("Item Fulfillment is transferred with internal id {0}.", transferId);
                    logger.info(string.Format("Reading Item Fulfillment complete.\n\n"));

                    SNA_IF.internalId = itemFulfilmentInternalID;
                    UpdateMigrationStatus(SNA_IF);                     // update migration status of Item fulfillment.
                }


            }
            catch (Exception ex)
            {
                logger.info(string.Format("Error at migrateItemFulfillment()\nError : {0}\n\n", ex.Message));
                error_flag = true;
                return;
            }
        }


        /* private static void migrateItemFulfillment(string sointernalID)
         {
             logger.info("Searching For Item Fulfillment Records for SO No." + sointernalID);
             try
             {
                 #region Variable Declaration

                 ItemFulfillment SNA_IF, DNA_IF = new ItemFulfillment();
                 Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true };
                 WriteResponse wr = new WriteResponse();
                 string transferId = "", SNA_InternalID = "";
                 SearchResult result = new SearchResult();
                 int resultCount;

                 #endregion

                 result = getAllItemFulfillmentRecords(sointernalID);     //retrieve Item fulfillment data using Sales Order's Internal ID.
                 int result_Count = 0;
                 if (result != null)
                 {
                     result_Count = result.totalRecords;
                 }
                 else
                 {
                     return;
                 }

                 for (int i = 0; i < result_Count; i++)
                 {
                     if (result.recordList[i].GetType() == typeof(ItemFulfillment))
                     {
                         SNA_IF = (ItemFulfillment)(result.recordList[i]);

                         SNA_InternalID = SNA_IF.internalId;

                         if (SNA_IF == null)
                         {
                             return;
                         }
                         else
                         {
                             DNA_IF = SNA_IF;
                         }

                         bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_IF.customFieldList
                                                  where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                                  select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                         if (IsMigrated == true)
                         {
                             logger.info("Sales Order Already Migrated in DNA.");
                             continue;
                         }

                         DateTime tranDate = DNA_IF.tranDate;
                         ItemFulfillmentShipStatus ship_status = DNA_IF.shipStatus;

                         logger.info("Adding to DNA Item Fulfillment record...");

                         string itemFulfilmentInternalID = SNA_IF.internalId;        //store IF internal ID to variable
                         DNA_IF.externalId = itemFulfilmentInternalID;               //set External ID of DNA = Internal ID of SNA.
                         DNA_IF.internalId = null;                                   //set internal ID to null for inertion.
                         DNA_IF.postingPeriod = null;                                // set Posing period will be null
                         DNA_IF.createdFrom = new RecordRef() { externalId = sointernalID, type = RecordType.salesOrder, typeSpecified = true };    //setting DNA Sales order's internal id = created from of DNA.

                        

                         List<CustomFieldRef> customref = DNA_IF.customFieldList.ToList();
                         StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = SNA_InternalID };
                         customref.Add(customField);
                         DNA_IF.customFieldList = customref.ToArray();

                         ReadResponse read1 = null;
                         ItemFulfillment New_IF = new ItemFulfillment();
                         InitializeRef iref = new InitializeRef() { type = InitializeRefType.salesOrder, typeSpecified = true, externalId = sointernalID };
                         InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.itemFulfillment };
                         _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                         if (!read1.status.isSuccess)
                         {
                             logger.info("" + read1.status.statusDetail[0].message);
                         }
                         else
                         {
                             DNA_IF = read1.record as ItemFulfillment;
                         }

                         DNA_IF.itemList = SNA_IF.itemList;

                         List<ItemFulfillmentItem> IFItem = new List<ItemFulfillmentItem>();

                         foreach (var itemn in DNA_IF.itemList.item)
                         {
                             string SNA_itemID = itemn.item.internalId;
                             string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                             itemn.item.internalId = DNA_itemID;  // assign item internal id.
                             //itemn.quantityRemaining = 0;
                             itemn.quantityRemainingSpecified = false;
                             itemn.quantitySpecified = true;
                             IFItem.Add(itemn);

                             if (itemn.customFieldList.Count() > 0)
                             {
                                 for (int c = 0; c < itemn.customFieldList.Count(); c++)
                                 {
                                     if (itemn.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                     {
                                         SelectCustomFieldRef customRec = (SelectCustomFieldRef)itemn.customFieldList[c];
                                         if (customRec.internalId == "custcol_salesrep")
                                         {
                                             customRec.value.internalId = get_SalesrepInternalID_DNA(customRec.value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                                             itemn.customFieldList[c] = customRec;
                                             continue;
                                             //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                                         }
                                     }
                                 }
                             }
                         }

                         DNA_IF.itemList = new ItemFulfillmentItemList();
                         DNA_IF.itemList.item = IFItem.ToArray();
                         DNA_IF.itemList.replaceAll = false;

                         foreach (var dna_item in DNA_IF.itemList.item)
                         {
                             bool flag = false;
                             foreach (var sna_item in SNA_IF.itemList.item)
                             {
                                 if (sna_item.item.internalId == dna_item.item.internalId)
                                 {
                                     flag = true;
                                     dna_item.quantity = sna_item.quantity;
                                     dna_item.serialNumbers = sna_item.serialNumbers;

                                     if (dna_item.location != null)
                                         dna_item.location = new RecordRef() { internalId = get_LocationInternalID_DNA(dna_item.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                                     break;
                                 }
                             }
                             if (flag == false)
                             {
                                 dna_item.itemReceive = false;
                                 dna_item.itemReceiveSpecified = true;
                                 dna_item.quantityRemainingSpecified = false;
                                 dna_item.quantitySpecified = false;
                                 dna_item.itemIsFulfilled = false;
                                 dna_item.shipGroupSpecified = false;
                             }
                             else
                             {
                                 dna_item.itemReceive = true;
                                 dna_item.itemReceiveSpecified = true;
                                 dna_item.quantityRemainingSpecified = true;
                                 dna_item.quantitySpecified = true;
                                 dna_item.itemIsFulfilled = true;
                                 dna_item.shipGroupSpecified = true;
                             }
                         }

                        

                         DNA_IF.itemList.replaceAll = false;
                         DNA_IF.tranDate = tranDate;                 // set trandate
                         DNA_IF.shipStatus = ship_status;            // set ship status
                         DNA_IF.tranId = SNA_IF.tranId;              // set tranid
                         DNA_IF.postingPeriod = null;                // set posting period

                         _client.add(US.GetDestinationePassport(), null, null, pref, DNA_IF, out wr);

                         if (!wr.status.isSuccess)
                         {

                             if (wr.status.statusDetail[0].code.ToString() == "USER_ERROR")
                             {
                                 error_flag = false;
                                 logger.info(2, string.Format("Error code: {0}", wr.status.statusDetail[0].code.ToString()));
                                 var error1 = string.Format("Item Fulfillment is failed.\nDetail :{0}", wr.status.statusDetail[0].message);
                                 logger.error(2, error1);
                                 lot_flag = true;

                             }
                             else
                             {
                                 logger.info(2, string.Format("Error at migrateItemFulfillment()\n Error code: {0}",
                                     wr.status.statusDetail[0].code.ToString()));
                                 var error = string.Format("Error at migrateItemFulfillment()\nItem Fulfillment transfer is failed for ID : {1}.\nDetail :{0}",
                                     wr.status.statusDetail[0].message, itemFulfilmentInternalID);
                                 logger.error(2, error);

                                 logger.info(string.Format("Error at migrateItemFulfillment()\nReading Item Fulfillment Incomplete.\n\n"));
                                 error_flag = true;
                                 return;
                             }
                         }
                         else
                         {
                             transferId = ((RecordRef)(wr.baseRef)).internalId;
                             var msg = string.Format("Item Fulfillment is transferred with internal id {0}.", transferId);
                             logger.info(string.Format("Reading Item Fulfillment complete.\n\n"));

                             SNA_IF.internalId = itemFulfilmentInternalID;
                             UpdateMigrationStatus(SNA_IF);                     // update migration status of Item fulfillment.
                         }
                     }
                 }
             }
             catch (Exception ex)
             {
                 logger.info(string.Format("Error at migrateItemFulfillment()\nError : {0}\n\n", ex.Message));
                 error_flag = true;
                 return;
             }
         }*/

        #endregion

        #region Migration of Invoice

        private static void migrateInvoice(string sointernalID)
        {
            try
            {
                Invoice DNA_Invoice, SNA_Invoice = new Invoice();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
                WriteResponse wr = new WriteResponse();
                string transferId = "", irtransferid = "";
                int result_Count = 0;
                SearchResult Invoices = new SearchResult();

                //SNA_Invoice = getAllInvoicetRecords(sointernalID);

                Invoices = getAllInvoicetRecords(sointernalID);

                if (Invoices != null)
                {
                    result_Count = Invoices.totalRecords;
                }
                else
                {
                    return;
                }

                for (int i = 0; i < result_Count; i++)
                {
                    if (Invoices.recordList[i].GetType() == typeof(Invoice))
                    {
                        SNA_Invoice = (Invoice)(Invoices.recordList[i]);

                        if (SNA_Invoice == null)
                        {
                            return;
                        }
                        else
                        {
                            DNA_Invoice = SNA_Invoice;
                        }

                        try
                        {

                            Boolean ismigratedbutnotcheck = GetInvoicefromDNa(DNA_Invoice.tranId.ToString());

                            if (ismigratedbutnotcheck == true)
                            {
                                UpdateMigrationStatus(SNA_Invoice);
                                continue;
                            }


                            bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_Invoice.customFieldList
                                                     where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                                     select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();


                            if (IsMigrated == true)
                            {
                                logger.info("Invoice Already Migrated in DNA.");
                                continue;
                            }

                            logger.info("Reading Invoice record...");
                            logger.info("Inv Tran ID: " + DNA_Invoice.tranId);
                            double total = DNA_Invoice.total;
                            double subtotal = DNA_Invoice.subTotal;
                            double amountDue = DNA_Invoice.amountRemaining;
                            string DNA_InvoiceInternalID = DNA_Invoice.internalId;
                            DateTime tranDate = DNA_Invoice.tranDate;
                            string tranID = DNA_Invoice.tranId;
                            string ItemFulfillmentID = "";
                            InvoiceItemList itemList = SNA_Invoice.itemList;

                            ReadResponse read1 = null;
                            Invoice New_IF = new Invoice();
                            InitializeRef iref = new InitializeRef() { type = InitializeRefType.salesOrder, typeSpecified = true, externalId = sointernalID };
                            InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.invoice };
                            _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                            if (!read1.status.isSuccess)
                            {
                                logger.info("Error at migrateInvoice()\nError While initializing Invoice : " + read1.status.statusDetail[0].message);
                                //if (read1.status.statusDetail[0].message.Contains("You can not initialize invoice: invalid reference") == true)
                                //{
                                //    SNA_Invoice = getSingleInvoicetRecords(SNA_Invoice.internalId);
                                //    UpdateMigrationStatus(SNA_Invoice);

                                //}

                                logger.info("Inv Tran ID: " + DNA_Invoice.tranId);
                                continue;
                            }
                            else
                            {
                                DNA_Invoice = read1.record as Invoice;
                            }

                            #region Set Custom Fields

                            List<CustomFieldRef> customList = DNA_Invoice.customFieldList.ToList();

                            if (DNA_Invoice.customFieldList.Count() > 0)
                            {
                                for (int c = 0; c < DNA_Invoice.customFieldList.Count(); c++)
                                {
                                    if (DNA_Invoice.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                    {
                                        SelectCustomFieldRef customRec = (SelectCustomFieldRef)DNA_Invoice.customFieldList[c];
                                        if (customRec.internalId == "custbody_initiator")
                                        {
                                            customRec.value.internalId = get_SalesrepInternalID_DNA(customRec.value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                                            DNA_Invoice.customFieldList[c] = customRec;
                                            continue;
                                        }
                                    }

                                    if (DNA_Invoice.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                    {
                                        SelectCustomFieldRef customRec = (SelectCustomFieldRef)DNA_Invoice.customFieldList[c];
                                        if (customRec.internalId == "custbody_last_itemfulfillment")
                                        {

                                            DNA_Invoice.customFieldList[c] = customRec;
                                            continue;
                                        }
                                    }

                                    if (DNA_Invoice.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                    {
                                        SelectCustomFieldRef customRec = (SelectCustomFieldRef)DNA_Invoice.customFieldList[c];
                                        if (customRec.internalId == "custbody_last_invoice")
                                        {
                                            ItemFulfillmentID = customRec.value.internalId;
                                            continue;
                                        }
                                    }
                                }
                            }

                            // set initiator field

                            #endregion

                            DNA_Invoice.itemList = itemList;

                            logger.info("Reading Invoice Item record...");

                            int sna_Count = 0;
                            foreach (var dna_item in DNA_Invoice.itemList.item)
                            {
                                bool flag = false;
                                foreach (var sna_item in SNA_Invoice.itemList.item)
                                {
                                    if (sna_Count != SNA_Invoice.itemList.item.Count())
                                    {
                                        if (sna_item.item.name == dna_item.item.name)
                                        {
                                            if (sna_item.quantity == dna_item.quantity)
                                            {
                                                sna_Count++;
                                                flag = true;
                                                dna_item.quantity = sna_item.quantity;
                                                dna_item.quantitySpecified = true;
                                                dna_item.serialNumbers = sna_item.serialNumbers;
                                                dna_item.orderLineSpecified = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (flag == false)
                                {
                                    dna_item.quantityRemainingSpecified = false;
                                    dna_item.quantitySpecified = false;
                                    dna_item.itemIsFulfilled = false;
                                    dna_item.shipGroupSpecified = false;
                                }
                                else
                                {

                                    dna_item.quantityRemainingSpecified = true;
                                    dna_item.quantitySpecified = true;
                                    dna_item.itemIsFulfilled = true;
                                    dna_item.shipGroupSpecified = true;
                                }
                            }

                            List<SalesTaxItem> asd = GetAllCurrencies();
                            List<InvoiceItem> invoiceItem = new List<InvoiceItem>();
                            foreach (var itemn in DNA_Invoice.itemList.item)
                            {
                                string SNA_itemID = itemn.item.internalId;
                                double qty = itemn.quantity;
                                string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                                itemn.item.internalId = DNA_itemID;  // assign item internal id.

                                if (itemn.@class != null)
                                    itemn.@class = new RecordRef() { internalId = get_classInternalID_DNA(itemn.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                                if (itemn.taxCode != null)
                                {
                                    foreach (var taxname in asd)
                                    {
                                        if (taxname.itemId == itemn.taxCode.name.Substring(7))
                                        {
                                            itemn.taxCode.internalId = taxname.internalId;
                                        }
                                    }
                                }

                                if (itemn.units != null)
                                {
                                    //itemn.units = new RecordRef() { internalId = GetUnitSearch(itemn.units.name), type = RecordType.unitsType, typeSpecified = true };
                                    //itemn.units = get_ItemUnitID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);

                                    BaseRef[] bas = getSelectoptions_exp(itemn.item.internalId, DNA_Invoice.entity.internalId);

                                    foreach (var b in bas)
                                    {
                                        if (b.name == itemn.units.name)
                                        {
                                            RecordRef new_rec = (RecordRef)b;

                                            itemn.units = new_rec;
                                        }
                                    }
                                }

                                if (itemn.customFieldList.Count() > 0)
                                {
                                    for (int c = 0; c < itemn.customFieldList.Count(); c++)
                                    {
                                        if (itemn.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                        {
                                            SelectCustomFieldRef customRec = (SelectCustomFieldRef)itemn.customFieldList[c];
                                            if (customRec.internalId == "custcol_salesrep")
                                            {
                                                customRec.value.internalId = get_SalesrepInternalID_DNA(customRec.value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                                                DNA_Invoice.customFieldList[c] = customRec;
                                                continue;
                                                //SelectCustomFieldRef customField_initiator = new SelectCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomInitiatorField"], value = new ListOrRecordRef() { internalId = initiator } };
                                            }
                                        }
                                    }
                                }

                                //itemn.quantity = qty;
                                double rate = Convert.ToDouble(itemn.rate);

                                itemn.amount = qty * rate;

                                invoiceItem.Add(itemn);
                            }

                            DNA_Invoice.itemList = new InvoiceItemList();
                            DNA_Invoice.itemList.item = invoiceItem.ToArray();

                            DNA_Invoice.total = total;
                            DNA_Invoice.subTotal = subtotal;
                            DNA_Invoice.amountRemaining = amountDue;
                            DNA_Invoice.tranDate = tranDate;
                            DNA_Invoice.tranId = tranID;

                            //adding the PO to destination account
                            logger.info("Adding to DNA Invoice record...");

                            //if (DNA_Invoice.entity != null)
                            //    DNA_Invoice.entity = new RecordRef() { internalId = get_EntityInternalID_DNA(DNA_Invoice.entity.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                            if (DNA_Invoice.job != null)
                                DNA_Invoice.job = new RecordRef() { internalId = get_JobInternalID_DNA(DNA_Invoice.job.internalId, ConfigurationManager.AppSettings["CustomProjectField"]), type = RecordType.job, typeSpecified = true };

                            if (DNA_Invoice.salesRep != null)
                                DNA_Invoice.salesRep = new RecordRef() { internalId = get_EntityInternalID_DNA(DNA_Invoice.salesRep.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]), type = RecordType.salesRole, typeSpecified = true };

                            if (DNA_Invoice.salesRep == null)
                            {
                                DNA_Invoice.salesRep = null;
                            }


                            if (DNA_Invoice.partner != null)
                                DNA_Invoice.partner = new RecordRef() { internalId = get_PartnerInternalID_DNA(DNA_Invoice.partner.internalId, ConfigurationManager.AppSettings["CustomPartnerField"]), type = RecordType.partner, typeSpecified = true };

                            if (DNA_Invoice.location != null)
                                DNA_Invoice.location = new RecordRef() { internalId = get_LocationInternalID_DNA(DNA_Invoice.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                            if (DNA_Invoice.department != null)
                                DNA_Invoice.department = new RecordRef() { internalId = get_DepartmentInternalID_DNA(DNA_Invoice.department.internalId, ConfigurationManager.AppSettings["CustomDepartmentField"]), type = RecordType.department, typeSpecified = true };

                            //if (DNA_Invoice.subsidiary != null)
                            //    DNA_Invoice.subsidiary = new RecordRef() { internalId = get_subsidiaryInternalID_DNA(DNA_Invoice.subsidiary.internalId, ConfigurationManager.AppSettings["CustomsubsidiaryField"]), type = RecordType.subsidiary, typeSpecified = true };

                            DNA_Invoice.postingPeriod = null;

                            string InvoiceID = SNA_Invoice.internalId;
                            DNA_Invoice.externalId = InvoiceID;
                            DNA_Invoice.createdFrom = new RecordRef() { externalId = sointernalID, type = RecordType.salesOrder, typeSpecified = true };
                            DNA_Invoice.internalId = null;
                            DNA_Invoice.postingPeriod = null;
                            DNA_Invoice.totalSpecified = false;
                            DNA_Invoice.dueDate = SNA_Invoice.dueDate;
                            DNA_Invoice.dueDateSpecified = true;
                            DNA_Invoice.shippingTaxCode = null;

                            List<CustomFieldRef> customref = DNA_Invoice.customFieldList.ToList();
                            StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_InvoiceInternalID };
                            customref.Add(customField);
                            DNA_Invoice.customFieldList = customref.ToArray();

                            if (DNA_Invoice.subsidiary.internalId == "3")
                            {
                                DNA_Invoice.location = new RecordRef() { internalId = "6", type = RecordType.location, typeSpecified = true };
                            }
                            else if (DNA_Invoice.subsidiary.internalId == "2")
                            {
                                DNA_Invoice.location = new RecordRef() { internalId = "3", type = RecordType.location, typeSpecified = true };
                            }

                            _client.add(US.GetDestinationePassport(), null, null, pref, DNA_Invoice, out wr);

                            if (!wr.status.isSuccess)
                            {
                                if (wr.status.statusDetail[0].code.ToString() == "USER_ERROR")
                                {
                                    error_flag = false;
                                    logger.info(2, string.Format("Error code: {0}", wr.status.statusDetail[0].code.ToString()));
                                    var error1 = string.Format("Item Fulfillment is failed.\nDetail :{0}", wr.status.statusDetail[0].message);
                                    logger.error(2, error1);
                                    lot_flag = true;
                                }
                                else
                                {
                                    logger.info(2, string.Format("Error at migrateInvoice()\nError code: {0}", wr.status.statusDetail[0].code.ToString()));

                                    var error = string.Format("Error at migrateInvoice()\nInvoice transfer is failed with internal ID : {1}.\nDetail :{0}\n\n",
                                        wr.status.statusDetail[0].message, InvoiceID);
                                    logger.error(2, error);
                                    error_flag = true;
                                    return;
                                }
                            }
                            else
                            {
                                var msg = string.Format("Invoice is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                                // Update SNA Migration Status
                                SNA_Invoice = getSingleInvoicetRecords(InvoiceID);
                                UpdateMigrationStatus(SNA_Invoice);
                            }

                        }
                        catch (Exception ex)
                        {
                            logger.info("Error at migrateInvoice()\nException while reading Invoice Order : \n" + ex.Message);
                            error_flag = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at migrateInvoice()\nError : \n\n" + ex.Message);
                error_flag = true;
                return;
            }
        }

        private static string migrate_NON_SO_Invoice(string invoiceID)
        {
            try
            {
                Invoice DNA_Invoice, SNA_Invoice = new Invoice();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
                WriteResponse wr = new WriteResponse();
                string transferId = "", irtransferid = "";

                SNA_Invoice = getInvoiceRecords(invoiceID);

                if (SNA_Invoice == null)
                {
                    return null;
                }
                else
                {
                    DNA_Invoice = SNA_Invoice;
                }

                try
                {
                    bool IsMigrated = (bool)(from CustomFieldRef cref in SNA_Invoice.customFieldList
                                             where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                             select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                    if (IsMigrated == true)
                    {
                        logger.info("Invoice Already Migrated in DNA.");
                        return SNA_Invoice.internalId;
                    }

                    logger.info("Reading Invoice record...");

                    #region Commented Code
                    //invoiceID = SNA_Invoice.internalId;
                    //DNA_Invoice.customForm = new RecordRef() { internalId = "78", type = RecordType.itemFulfillment, typeSpecified = true };
                    //DNA_Invoice.billAddress = SNA_Invoice.billAddress;
                    //DNA_Invoice.createdDate = SNA_Invoice.createdDate;
                    //DNA_Invoice.createdDateSpecified = true;
                    //DNA_Invoice.email = SNA_Invoice.email;
                    //DNA_Invoice.entity = SNA_Invoice.entity;
                    //DNA_Invoice.exchangeRate = SNA_Invoice.exchangeRate;
                    //DNA_Invoice.exchangeRateSpecified = true;
                    //DNA_Invoice.fax = SNA_Invoice.fax;
                    //DNA_Invoice.location = SNA_Invoice.location;
                    //DNA_Invoice.total = SNA_Invoice.total;
                    //DNA_Invoice.totalSpecified = true;
                    //DNA_Invoice.tranDate = SNA_Invoice.tranDate;
                    //DNA_Invoice.tranDateSpecified = true;
                    //DNA_Invoice.tranId = SNA_Invoice.tranId;
                    //DNA_Invoice.transactionShipAddress = SNA_Invoice.transactionShipAddress;
                    //DNA_Invoice.itemList = SNA_Invoice.itemList;

                    //DNA_Invoice.createdFrom = new RecordRef() { internalId = invoiceID, type = RecordType.invoice, typeSpecified = true }; // Assigning SO internal Id as created from for IF...

                    //logger.info("Reading Invoice Item record...");
                    //List<InvoiceItem> DNAItem = new List<InvoiceItem>();
                    //foreach (var itemn in SNA_Invoice.itemList.item)
                    //{
                    //    var DNAitem = new InvoiceItem();
                    //    DNAitem.description = itemn.description;
                    //    DNAitem.item = itemn.item;
                    //    DNAitem.quantity = itemn.quantity;
                    //    DNAItem.Add(DNAitem);
                    //}

                    //DNA_Invoice.itemList = new InvoiceItemList();
                    //DNA_Invoice.itemList.item = DNAItem.ToArray();
                    //DNA_Invoice.entity = new RecordRef() { internalId = "214", type = RecordType.account, typeSpecified = true }; 
                    #endregion

                    string DNA_InvoiceInternalID = DNA_Invoice.internalId;
                    double total = DNA_Invoice.total;
                    double subtotal = DNA_Invoice.subTotal;
                    double amountDue = DNA_Invoice.amountRemaining;
                    DateTime tranDate = DNA_Invoice.tranDate;
                    string tranID = DNA_Invoice.tranId;

                    List<CustomFieldRef> customref = new List<CustomFieldRef>();
                    StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_InvoiceInternalID };
                    customref.Add(customField);
                    DNA_Invoice.customFieldList = customref.ToArray();

                    logger.info("Reading Sales order Item record...");
                    List<SalesTaxItem> asd = GetAllCurrencies();
                    List<InvoiceItem> invoiceItem = new List<InvoiceItem>();
                    foreach (var itemn in DNA_Invoice.itemList.item)
                    {
                        #region Trial Code
                        //var sitem = new SalesOrderItem();
                        //sitem.amount = itemn.amount;
                        //sitem.amountSpecified = true;
                        //sitem.description = itemn.description;
                        //sitem.grossAmt = itemn.grossAmt;
                        //sitem.grossAmtSpecified = true;
                        //itemn.item = itemn.item;
                        //sitem.quantity = itemn.quantity;
                        //sitem.quantityReceived = itemn.quantityReceived;
                        //sitem.quantityReceivedSpecified = true;
                        //sitem.rate = itemn.rate;
                        //sitem.poVendor = itemn.poVendor; 
                        #endregion

                        if (itemn.taxCode != null)
                        {
                            foreach (var taxname in asd)
                            {
                                if (taxname.itemId == itemn.taxCode.name.Substring(7))
                                {
                                    itemn.taxCode.internalId = taxname.internalId;
                                }
                            }
                        }

                        if (itemn.department != null)
                            itemn.department = new RecordRef() { internalId = get_DepartmentInternalID_DNA(itemn.department.internalId, ConfigurationManager.AppSettings["CustomDepartmentField"]), type = RecordType.department, typeSpecified = true };

                        if (itemn.location != null)
                            itemn.location = new RecordRef() { internalId = get_LocationInternalID_DNA(itemn.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                        if (itemn.@class != null)
                            itemn.@class = new RecordRef() { internalId = get_classInternalID_DNA(itemn.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                        string SNA_itemID = itemn.item.internalId;
                        if (itemn.price != null)
                        {
                            string priceid = itemn.price.internalId;
                            //itemn.price = getPriceObject(priceid);
                            itemn.price = new RecordRef() { name = itemn.price.name, type = RecordType.priceLevel, typeSpecified = true }; ;
                        }
                        string DNA_itemID = get_ItemInternalID_DNA(SNA_itemID, ConfigurationManager.AppSettings["CustomItemField"]);    // retrive DNA item's internal id.
                        itemn.item.internalId = DNA_itemID;  // assign item internal id.
                        invoiceItem.Add(itemn);

                        if (itemn.customFieldList.Count() > 0)
                        {
                            for (int c = 0; c < itemn.customFieldList.Count(); c++)
                            {
                                if (itemn.customFieldList[c].GetType() == typeof(SelectCustomFieldRef))
                                {
                                    SelectCustomFieldRef customRec = (SelectCustomFieldRef)itemn.customFieldList[c];
                                    if (customRec.internalId == "custcol_salesrep")
                                    {
                                        customRec.value.internalId = get_SalesrepInternalID_DNA(customRec.value.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]);

                                        itemn.customFieldList[c] = customRec;
                                        continue;
                                    }
                                }
                            }
                        }
                    }

                    DNA_Invoice.itemList = new InvoiceItemList();
                    DNA_Invoice.itemList.item = invoiceItem.ToArray();

                    //adding the PO to destination account
                    //po.internalId = "";
                    logger.info("Adding to DNA Invoice record...");

                    string InvoiceID = SNA_Invoice.internalId;
                    DNA_Invoice.externalId = InvoiceID;
                    //DNA_Invoice.createdFrom = new RecordRef() { externalId = invoiceID, type = RecordType.salesOrder, typeSpecified = true };
                    DNA_Invoice.internalId = null;
                    DNA_Invoice.postingPeriod = null;

                    if (DNA_Invoice.@class != null)
                        DNA_Invoice.@class = new RecordRef() { internalId = get_classInternalID_DNA(DNA_Invoice.@class.internalId, ConfigurationManager.AppSettings["CustomclassField"]), type = RecordType.classification, typeSpecified = true };

                    if (DNA_Invoice.entity != null)
                        DNA_Invoice.entity = new RecordRef() { internalId = get_EntityInternalID_DNA(DNA_Invoice.entity.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                    if (DNA_Invoice.job != null)
                        DNA_Invoice.job = new RecordRef() { internalId = get_JobInternalID_DNA(DNA_Invoice.job.internalId, ConfigurationManager.AppSettings["CustomProjectField"]), type = RecordType.job, typeSpecified = true };

                    if (DNA_Invoice.salesRep.internalId != "185")
                    {

                        if (DNA_Invoice.salesRep != null)
                            DNA_Invoice.salesRep = new RecordRef() { internalId = get_SalesrepInternalID_DNA(DNA_Invoice.salesRep.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]), type = RecordType.salesRole, typeSpecified = true };
                    }
                    else
                    {
                        DNA_Invoice.salesRep = null;
                    }
                    if (DNA_Invoice.account != null)
                    {
                        DNA_Invoice.account = new RecordRef() { internalId = getAccountInternalID(DNA_Invoice.account.internalId, ConfigurationManager.AppSettings["CustomAccountField"]), type = RecordType.account, typeSpecified = true };
                    }

                    if (DNA_Invoice.partner != null)
                        DNA_Invoice.partner = new RecordRef() { internalId = get_PartnerInternalID_DNA(DNA_Invoice.partner.internalId, ConfigurationManager.AppSettings["CustomPartnerField"]), type = RecordType.partner, typeSpecified = true };

                    if (DNA_Invoice.location != null)
                        DNA_Invoice.location = new RecordRef() { internalId = get_LocationInternalID_DNA(DNA_Invoice.location.internalId, ConfigurationManager.AppSettings["CustomLocationField"]), type = RecordType.location, typeSpecified = true };

                    if (DNA_Invoice.department != null)
                        DNA_Invoice.department = new RecordRef() { internalId = get_DepartmentInternalID_DNA(DNA_Invoice.department.internalId, ConfigurationManager.AppSettings["CustomDepartmentField"]), type = RecordType.department, typeSpecified = true };

                    if (DNA_Invoice.subsidiary != null)
                        DNA_Invoice.subsidiary = new RecordRef() { internalId = get_subsidiaryInternalID_DNA(DNA_Invoice.subsidiary.internalId, ConfigurationManager.AppSettings["CustomsubsidiaryField"]), type = RecordType.subsidiary, typeSpecified = true };


                    for (int s = 0; s < DNA_Invoice.salesTeamList.salesTeam.Count(); s++)
                    {
                        if (DNA_Invoice.salesTeamList != null && DNA_Invoice.salesTeamList.salesTeam[s].employee != null)
                        {
                            DNA_Invoice.salesTeamList.salesTeam[s].employee = new RecordRef()
                             {
                                 internalId = get_SalesrepInternalID_DNA(DNA_Invoice.salesTeamList.salesTeam[s].employee.internalId, ConfigurationManager.AppSettings["CustomSalesRepField"]),
                                 type = RecordType.employee,
                                 typeSpecified = true
                             };
                        }
                    }


                    DNA_Invoice.postingPeriod = null;
                    DNA_Invoice.total = total;
                    DNA_Invoice.subTotal = subtotal;
                    DNA_Invoice.amountRemaining = amountDue;
                    DNA_Invoice.tranDate = tranDate;
                    DNA_Invoice.tranId = tranID;

                    _client.add(US.GetDestinationePassport(), null, null, pref, DNA_Invoice, out wr);

                    if (!wr.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error at migrate_NON_SO_Invoice()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at migrate_NON_SO_Invoice()\n Invoice transfer is failed.\nDetail :{0}\n\n", wr.status.statusDetail[0].message);
                        logger.error(2, error);
                        error_flag = true;
                        return InvoiceID;
                    }
                    else
                    {
                        var msg = string.Format("Invoice is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                        SNA_Invoice = getInvoiceRecords(invoiceID);
                        //SNA_Invoice.internalId = InvoiceID;
                        UpdateMigrationStatus(SNA_Invoice);

                        return InvoiceID;
                    }

                }
                catch (Exception ex)
                {
                    logger.info("Error at migrate_NON_SO_Invoice()\n Exception while reading Invoice Order : \n\n" + ex.Message);
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at migrate_NON_SO_Invoice()\n Error : \n\n" + ex.Message);
                error_flag = true;
                return null;
            }
        }

        #endregion

        #region Migration of Customer Payment

        private static void migrate_All_CustomerPayment(string CustomerPaymentID)
        {
            try
            {
                #region Veriable declaration

                CustomerPayment DNA_CustomerPayment = new CustomerPayment();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true };
                WriteResponse wr = new WriteResponse();
                string cust_pay_iD = "", irtransferid = "";

                #endregion

                CustomerPayment SNA_CustomerPayment = getCustomerPaymentRecords(CustomerPaymentID);

                if (SNA_CustomerPayment == null)
                {
                    return;
                }
                else
                {
                    DNA_CustomerPayment = SNA_CustomerPayment;
                }

                bool IsMigrated = (bool)(from CustomFieldRef cref in DNA_CustomerPayment.customFieldList
                                         where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                         select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                if (IsMigrated == true)
                {
                    logger.info("Customer Payment Already Migrated in DNA.");
                    return;
                }

                DateTime tranDate = DNA_CustomerPayment.tranDate;
                //string tranID = DNA_CustomerPayment.

                logger.info("Adding to DNA_CustomerPayment record...");

                if (DNA_CustomerPayment.customer != null)
                    DNA_CustomerPayment.customer = new RecordRef() { internalId = get_EntityInternalID_DNA(DNA_CustomerPayment.customer.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                string DNA_CustomerPaymentID = DNA_CustomerPayment.internalId;
                CustomerPaymentApplyList applyList = new CustomerPaymentApplyList();
                ReadResponse read1 = null;

                applyList = DNA_CustomerPayment.applyList;
                Invoice inv = new Invoice();
                CustomerPaymentApplyList cust_apply = new CustomerPaymentApplyList();
                CustomerPaymentApply applyItem = new CustomerPaymentApply();
                List<CustomerPaymentApply> apply_List = new List<CustomerPaymentApply>();
                foreach (var item in applyList.apply)
                {
                    string invoice_ref = item.refNum;
                    inv = get_SNA_InvoiceID(invoice_ref);

                    if (inv != null)
                    {
                        InitializeRef iref = new InitializeRef() { type = InitializeRefType.invoice, typeSpecified = true, externalId = inv.internalId };
                        InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.customerPayment };
                        _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                        if (!read1.status.isSuccess)
                        {
                            logger.info("Error at migrate_All_CustomerPayment()\n Error in function migrate_All_CustomerPayment while initialise : " + read1.status.statusDetail[0].message);
                            error_flag = true;
                            return;
                        }
                        else
                        {
                            DNA_CustomerPayment = read1.record as CustomerPayment;
                            apply_List.Add(DNA_CustomerPayment.applyList.apply[0]);
                        }
                    }
                }
                DNA_CustomerPayment.applyList.apply = apply_List.ToArray();

                for (int i = 0; i < SNA_CustomerPayment.applyList.apply.Count(); i++)           // set apply amount
                {
                    DNA_CustomerPayment.applyList.apply[i].amount = SNA_CustomerPayment.applyList.apply[i].amount;
                }

                DNA_CustomerPayment.appliedSpecified = false;                       // apply amount not specified
                DNA_CustomerPayment.postingPeriod = null;                           // set posting period to null;
                DNA_CustomerPayment.tranDate = SNA_CustomerPayment.tranDate;        // set trandate 
                DNA_CustomerPayment.tranDate = tranDate;

                // set SNA internal ID to custom field
                List<CustomFieldRef> customref = new List<CustomFieldRef>();
                StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_CustomerPaymentID };
                customref.Add(customField);
                DNA_CustomerPayment.customFieldList = customref.ToArray();

                cust_pay_iD = SNA_CustomerPayment.internalId;
                DNA_CustomerPayment.externalId = cust_pay_iD;
                DNA_CustomerPayment.internalId = null;

                _client.add(US.GetDestinationePassport(), null, null, pref, DNA_CustomerPayment, out wr);

                if (!wr.status.isSuccess)
                {
                    logger.info(2, string.Format("Error at migrate_All_CustomerPayment()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                    var error = string.Format("Error at migrate_All_CustomerPayment()\n DNA_CustomerPayment transfer is failed.\nDetail :{0}\n\n", wr.status.statusDetail[0].message);
                    logger.error(2, error);
                    error_flag = true;
                    return;
                }
                else
                {
                    var msg = string.Format("DNA_CustomerPayment is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                    SNA_CustomerPayment.internalId = cust_pay_iD;
                    UpdateMigrationStatus(SNA_CustomerPayment);
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at migrate_All_CustomerPayment()\n Exception while reading Order : \n\n" + ex.Message);
                error_flag = true;
                return;
            }
        }

        /*
        private static string migrate_ALL_CustomerPayment(string CustomerPaymentID)
        {
            try
            {
                #region Veriable declaration

                CustomerPayment DNA_CustomerPayment = new CustomerPayment();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true };
                WriteResponse wr = new WriteResponse();
                string cust_pay_iD = "", irtransferid = "";

                #endregion

                CustomerPayment SNA_CustomerPayment = getCustomerPaymentRecords(CustomerPaymentID);
                DNA_CustomerPayment = SNA_CustomerPayment;

                if (SNA_CustomerPayment == null)
                { return null; }
                else
                {
                    DNA_CustomerPayment = SNA_CustomerPayment;
                }



                try
                {
                    bool IsMigrated = (bool)(from CustomFieldRef cref in DNA_CustomerPayment.customFieldList
                                             where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                                             select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();

                    if (IsMigrated == true)
                    {
                        logger.info("Customer Payment Already Migrated in DNA.");
                        return DNA_CustomerPayment.internalId;
                    }

                    #region Commented Code

                    //soid = SNA_CustomerPayment.internalId;

                    //DNA_CustomerPayment.customForm = new RecordRef() { internalId = "66", type = RecordType.customerPayment, typeSpecified = true };
                    //DNA_CustomerPayment.customer = SNA_CustomerPayment.customer;
                    //DNA_CustomerPayment.createdDate = SNA_CustomerPayment.createdDate;
                    //DNA_CustomerPayment.createdDateSpecified = true;
                    //DNA_CustomerPayment.exchangeRate = SNA_CustomerPayment.exchangeRate;
                    //DNA_CustomerPayment.exchangeRateSpecified = true;
                    //DNA_CustomerPayment.location = SNA_CustomerPayment.location;
                    //DNA_CustomerPayment.total = SNA_CustomerPayment.total;
                    //DNA_CustomerPayment.totalSpecified = true;
                    //DNA_CustomerPayment.tranDate = SNA_CustomerPayment.tranDate;
                    //DNA_CustomerPayment.tranDateSpecified = true;

                    //logger.info("Reading Sales order Item record...");
                    //List<ItemFulfillmentItem> soitem = new List<CustomerPayment>();
                    //foreach (var itemn in SO.itemList.item)
                    //{
                    //    var sitem = new ItemFulfillmentItem();
                    //    sitem.amount = itemn.amount;
                    //    sitem.amountSpecified = true;
                    //    sitem.description = itemn.description;
                    //    sitem.grossAmt = itemn.grossAmt;
                    //    sitem.grossAmtSpecified = true;
                    //    sitem.item = itemn.item;
                    //    sitem.quantity = itemn.quantity;
                    //    //sitem.quantityReceived = itemn.quantityReceived;
                    //    //sitem.quantityReceivedSpecified = true;
                    //    sitem.rate = itemn.rate;
                    //    sitem.poVendor = itemn.poVendor;
                    //    soitem.Add(sitem);
                    //}

                    //DNA_CustomerPayment.itemList = new ItemFulfillmentItemList();
                    //DNA_CustomerPayment.itemList.item = soitem.ToArray();
                    //DNA_CustomerPayment.entity = new RecordRef() { internalId = "214", type = RecordType.account, typeSpecified = true };
                    //adding the PO to destination account
                    //po.internalId = ""; 

                    #endregion

                    logger.info("Adding to DNA_CustomerPayment record...");

                    string DNA_CustomerPaymentID = DNA_CustomerPayment.internalId;

                    List<CustomFieldRef> customref = new List<CustomFieldRef>();
                    StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_CustomerPaymentID };
                    customref.Add(customField);
                    DNA_CustomerPayment.customFieldList = customref.ToArray();

                    cust_pay_iD = SNA_CustomerPayment.internalId;
                    DNA_CustomerPayment.externalId = cust_pay_iD;
                    DNA_CustomerPayment.internalId = null;

                    CustomerPaymentApplyList applyList = new CustomerPaymentApplyList();

                    applyList = DNA_CustomerPayment.applyList;
                    Invoice inv = new Invoice();
                    foreach (var item in applyList.apply)
                    {
                        string invoice_ref = item.refNum;
                        inv = get_SNA_InvoiceID(invoice_ref);

                        //item.refNum = inv.tranId;
                    }

                    InitializeRef iref = new InitializeRef() { type = InitializeRefType.invoice, typeSpecified = true, externalId = inv.internalId };
                    InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.customerPayment };
                    ReadResponse read1 = null;

                    _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                    if (!read1.status.isSuccess)
                    {
                        logger.info("" + read1.status.statusDetail[0].message);
                    }

                    DNA_CustomerPayment = read1.record as CustomerPayment;

                    DNA_CustomerPayment.postingPeriod = null;

                    _client.add(US.GetDestinationePassport(), null, null, pref, DNA_CustomerPayment, out wr);

                    if (!wr.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error at migrate_ALL_CustomerPayment()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at migrate_ALL_CustomerPayment()\n DNA_CustomerPayment transfer is failed.\nDetail :{0}\n\n", wr.status.statusDetail[0].message);
                        logger.error(2, error);
                        error_flag = true;
                        return null;
                    }
                    else
                    {
                        var msg = string.Format("DNA_CustomerPayment is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                        // Update migrate status.
                        SNA_CustomerPayment.internalId = cust_pay_iD;
                        UpdateMigrationStatus(SNA_CustomerPayment);

                        return ((RecordRef)(wr.baseRef)).internalId;
                    }
                }
                catch (Exception ex)
                {
                    logger.info("Error at migrate_ALL_CustomerPayment()\n Exception while reading Order : \n\n" + ex.Message);
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at migrate_ALL_CustomerPayment()\n Error : \n\n" + ex.Message);
                error_flag = true;
                return null;
            }
        }

        private static string migrateCustomerPayment(string invoiceinternalID)
        {
            try
            {
                #region Veriable declaration

                CustomerPayment DNA_CustomerPayment = new CustomerPayment();
                Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true };
                WriteResponse wr = new WriteResponse();
                string cust_pay_iD = "", irtransferid = "";

                #endregion

                CustomerPayment SNA_CustomerPayment = getAllCustomerPaymentRecords(invoiceinternalID);
                DNA_CustomerPayment = SNA_CustomerPayment;

                if (SNA_CustomerPayment == null)
                {
                    return null;
                }
                else
                {
                    DNA_CustomerPayment = SNA_CustomerPayment;
                }

                CustomerPayment DNA_CP = checkFor_DNA_CP(SNA_CustomerPayment.internalId);

                if (DNA_CP == null)
                {
                    try
                    {
                        #region Commented Code

                        //soid = SNA_CustomerPayment.internalId;

                        //DNA_CustomerPayment.customForm = new RecordRef() { internalId = "66", type = RecordType.customerPayment, typeSpecified = true };
                        //DNA_CustomerPayment.customer = SNA_CustomerPayment.customer;
                        //DNA_CustomerPayment.createdDate = SNA_CustomerPayment.createdDate;
                        //DNA_CustomerPayment.createdDateSpecified = true;
                        //DNA_CustomerPayment.exchangeRate = SNA_CustomerPayment.exchangeRate;
                        //DNA_CustomerPayment.exchangeRateSpecified = true;
                        //DNA_CustomerPayment.location = SNA_CustomerPayment.location;
                        //DNA_CustomerPayment.total = SNA_CustomerPayment.total;
                        //DNA_CustomerPayment.totalSpecified = true;
                        //DNA_CustomerPayment.tranDate = SNA_CustomerPayment.tranDate;
                        //DNA_CustomerPayment.tranDateSpecified = true;

                        //logger.info("Reading Sales order Item record...");
                        //List<ItemFulfillmentItem> soitem = new List<CustomerPayment>();
                        //foreach (var itemn in SO.itemList.item)
                        //{
                        //    var sitem = new ItemFulfillmentItem();
                        //    sitem.amount = itemn.amount;
                        //    sitem.amountSpecified = true;
                        //    sitem.description = itemn.description;
                        //    sitem.grossAmt = itemn.grossAmt;
                        //    sitem.grossAmtSpecified = true;
                        //    sitem.item = itemn.item;
                        //    sitem.quantity = itemn.quantity;
                        //    //sitem.quantityReceived = itemn.quantityReceived;
                        //    //sitem.quantityReceivedSpecified = true;
                        //    sitem.rate = itemn.rate;
                        //    sitem.poVendor = itemn.poVendor;
                        //    soitem.Add(sitem);
                        //}

                        //DNA_CustomerPayment.itemList = new ItemFulfillmentItemList();
                        //DNA_CustomerPayment.itemList.item = soitem.ToArray();
                        //DNA_CustomerPayment.entity = new RecordRef() { internalId = "214", type = RecordType.account, typeSpecified = true };
                        //adding the PO to destination account
                        //po.internalId = ""; 

                        #endregion

                        logger.info("Adding to DNA_CustomerPayment record...");

                        if (DNA_CustomerPayment.customer != null)
                            DNA_CustomerPayment.customer = new RecordRef() { internalId = get_EntityInternalID_DNA(DNA_CustomerPayment.customer.internalId, ConfigurationManager.AppSettings["CustomEntityField"]), type = RecordType.entityGroup, typeSpecified = true };

                        string DNA_CustomerPaymentID = DNA_CustomerPayment.internalId;
                        CustomerPaymentApplyList applyList = new CustomerPaymentApplyList();
                        ReadResponse read1 = null;

                        applyList = DNA_CustomerPayment.applyList;
                        Invoice inv = new Invoice();
                        foreach (var item in applyList.apply)
                        {
                            string invoice_ref = item.refNum;
                            inv = get_SNA_InvoiceID(invoice_ref);

                            if (inv != null)
                            {
                                InitializeRef iref = new InitializeRef() { type = InitializeRefType.invoice, typeSpecified = true, externalId = inv.internalId };
                                InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.customerPayment };
                                _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);
                            }
                        }

                        if (!read1.status.isSuccess)
                        {
                            logger.info("" + read1.status.statusDetail[0].message);
                        }
                        else
                        {
                            DNA_CustomerPayment = read1.record as CustomerPayment;
                        }

                        DNA_CustomerPayment.postingPeriod = null;

                        // set SNA internal ID to custom field
                        List<CustomFieldRef> customref = new List<CustomFieldRef>();
                        StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_CustomerPaymentID };
                        customref.Add(customField);
                        DNA_CustomerPayment.customFieldList = customref.ToArray();

                        cust_pay_iD = SNA_CustomerPayment.internalId;
                        DNA_CustomerPayment.externalId = cust_pay_iD;
                        DNA_CustomerPayment.internalId = null;

                        _client.add(US.GetDestinationePassport(), null, null, pref, DNA_CustomerPayment, out wr);

                        if (!wr.status.isSuccess)
                        {
                            logger.info(2, string.Format("Error at migrateCustomerPayment()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                            var error = string.Format("Error at migrateCustomerPayment()\n DNA_CustomerPayment transfer is failed.\nDetail :{0}\n\n", wr.status.statusDetail[0].message);
                            logger.error(2, error);
                            error_flag = true;
                            return null;
                        }
                        else
                        {
                            var msg = string.Format("DNA_CustomerPayment is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                            SNA_CustomerPayment.internalId = cust_pay_iD;
                            UpdateMigrationStatus(SNA_CustomerPayment);

                            return ((RecordRef)(wr.baseRef)).internalId;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.info("Error at migrateCustomerPayment()\n Exception while reading Order : \n\n" + ex.Message);
                        error_flag = true;
                        return null;
                    }
                }
                else
                {
                    DNA_CustomerPayment = DNA_CP;

                    try
                    {
                        logger.info("Adding to DNA_CustomerPayment record...");

                        string DNA_CustomerPaymentID = DNA_CustomerPayment.internalId;

                        List<CustomFieldRef> customref = new List<CustomFieldRef>();
                        StringCustomFieldRef customField = new StringCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomTransactionField"], value = DNA_CustomerPaymentID };
                        customref.Add(customField);
                        DNA_CustomerPayment.customFieldList = customref.ToArray();

                        //cust_pay_iD = SNA_CustomerPayment.internalId;
                        //DNA_CustomerPayment.externalId = cust_pay_iD;
                        //DNA_CustomerPayment.internalId = null;

                        CustomerPaymentApplyList applyList = new CustomerPaymentApplyList();

                        ReadResponse read1 = null;

                        applyList = DNA_CustomerPayment.applyList;
                        Invoice inv = new Invoice();
                        foreach (var item in applyList.apply)
                        {
                            string invoice_ref = item.refNum;
                            inv = get_SNA_InvoiceID(invoice_ref);

                            InitializeRef iref = new InitializeRef() { type = InitializeRefType.invoice, typeSpecified = true, externalId = inv.internalId };
                            InitializeRecord inrec = new InitializeRecord() { reference = iref, type = InitializeType.customerPayment };
                            _client.initialize(US.GetDestinationePassport(), null, null, null, inrec, out read1);

                        }

                        if (!read1.status.isSuccess)
                        {
                            logger.info("Error at migrateCustomerPayment()\n Error in IntializeRef in CP : " + read1.status.statusDetail[0].message);
                        }

                        DNA_CustomerPayment = read1.record as CustomerPayment;

                        DNA_CustomerPayment.postingPeriod = null;

                        _client.update(US.GetDestinationePassport(), null, null, pref, DNA_CustomerPayment, out wr);

                        if (!wr.status.isSuccess)
                        {
                            logger.info(2, string.Format("Error at migrateCustomerPayment()\n Error code: {0}", wr.status.statusDetail[0].code.ToString()));

                            var error = string.Format("Error at migrateCustomerPayment()\n DNA_CustomerPayment transfer is failed.\nDetail :{0}\n\n", wr.status.statusDetail[0].message);
                            logger.error(2, error);
                            error_flag = true;
                            return null;
                        }
                        else
                        {
                            var msg = string.Format("DNA_CustomerPayment is transferred with internal id {0}.\n\n", ((RecordRef)(wr.baseRef)).internalId);

                            SNA_CustomerPayment.internalId = cust_pay_iD;
                            UpdateMigrationStatus(SNA_CustomerPayment);

                            return ((RecordRef)(wr.baseRef)).internalId;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.info("Error at migrateCustomerPayment()\n Exception while reading Customer Payment : \n\n" + ex.Message);
                        error_flag = true;
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at migrateCustomerPayment()\n Error : \n\n" + ex.Message);
                error_flag = true;
                return null;
            }
        }

        */
        #endregion



        #region Retrieve Internal IDes from DNA

        #region Unused Functions

        public static BaseRef[] GetPostingPeriod()
        {
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
            GetSelectValueFieldDescription selectValueField = new GetSelectValueFieldDescription();
            selectValueField.recordType = RecordType.salesTaxItem;
            selectValueField.recordTypeSpecified = true;
            selectValueField.field = "parent";
            GetSelectValueFilter getSelectValueFilter = new GetSelectValueFilter();
            getSelectValueFilter.@operator = GetSelectValueFilterOperator.@is;
            getSelectValueFilter.filterValue = "6";
            selectValueField.filter = getSelectValueFilter;
            GetSelectValueResult selectValueResult = new GetSelectValueResult();
            _client.getSelectValue(US.GetDestinationePassport(), null, null, pref, selectValueField, 1, out selectValueResult);
            return selectValueResult.baseRefList;
        }

        public void getSelectoptions(string itemid, string taxcodeid)
        {
            // Instantiate the NetSuite web services
            //NetSuiteService _service = new NetSuiteService();
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
            /////bool value=_service.UnsafeAuthenticatedConnectionSharing;
            ////_service.CookieContainer = new CookieContainer();
            // Status status;
            ///NetSuiteService NS = new NetSuiteService();
            // Passport passportobj = new Passport();
            //
            //passportobj.email = "user3@navigantsolutions.com";
            //passportobj.account = "3455378";
            //passportobj.password = "netsuite1";
            //RecordRef role = new RecordRef();
            //passportobj.role = role;
            //role.internalId = "18";

            ////_service.Timeout = 1000 * 60 * 60 * 2;

            // _service.Timeout = 1000 * 60 * 60 * 2;
            //  status = _service.login(US.GetDestinationePassport()).status;
            // ////Console.Write("Msg"+Rsp.status.statusDetail[0].message);


            //_service.passport = US.GetDestinationePassport();

            GetSelectFilterByFieldValue[] getSelectValueFilterbys = new GetSelectFilterByFieldValue[2];
            getSelectValueFilterbys[0] = new GetSelectFilterByFieldValue { field = "item", internalId = itemid, sublist = "itemList" };
            getSelectValueFilterbys[1] = new GetSelectFilterByFieldValue { field = "entity", internalId = taxcodeid, sublist = null };
            getSelectValueFilterbys[1] = new GetSelectFilterByFieldValue { field = "taxCode", internalId = taxcodeid, sublist = null };



            GetSelectValueFieldDescription selectValueField = new GetSelectValueFieldDescription { recordType = RecordType.salesOrder, recordTypeSpecified = true, sublist = "itemList", field = "taxCode", filterByValueList = getSelectValueFilterbys };
            //   GetSelectValueFieldDescription myGSVField = new GetSelectValueFieldDescription(RecordType.salesOrder,
            //null, "itemList", "item", null, null, myFilterByList);
            //selectValueField.recordType = RecordType.salesTaxItem;
            //selectValueField.recordTypeSpecified = true;
            //selectValueField.field = "parent";


            // GetSelectValueResult selectValueResult = _service.getSelectValue(selectValueField, 1);
            GetSelectValueResult selectvalueResult = new GetSelectValueResult();
            _client.getSelectValue(US.GetDestinationePassport(), null, null, pref, selectValueField, 1, out selectvalueResult);

        }

        public static void getSelectoptionsForUnit(string itemid, string taxcodeid)
        {
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };

            GetSelectFilterByFieldValue[] getSelectValueFilterbys = new GetSelectFilterByFieldValue[3];
            getSelectValueFilterbys[0] = new GetSelectFilterByFieldValue { field = "item", internalId = itemid, sublist = "itemList" };
            getSelectValueFilterbys[1] = new GetSelectFilterByFieldValue { field = "entity", internalId = taxcodeid, sublist = null };
            getSelectValueFilterbys[2] = new GetSelectFilterByFieldValue { field = "unit", internalId = taxcodeid, sublist = null };

            GetSelectValueFieldDescription selectValueField = new GetSelectValueFieldDescription { recordType = RecordType.salesOrder, recordTypeSpecified = true, sublist = "itemList", field = "taxCode", filterByValueList = getSelectValueFilterbys };
            GetSelectValueResult selectvalueResult = new GetSelectValueResult();

            _client.getSelectValue(US.GetDestinationePassport(), null, null, pref, selectValueField, 1, out selectvalueResult);
        }

        public static string get_taxCodeInternalID_DNA(string taxInternalID)
        {
            //Search

            var list = new CustomList();

            ReadResponse rS = null;
            var recordRef = new RecordRef { type = RecordType.salesTaxItem, typeSpecified = true, internalId = taxInternalID };
            SearchPreferences pref = new SearchPreferences() { bodyFieldsOnly = false };
            try
            {
                _client.get(US.GetDestinationePassport(), null, null, null, recordRef, out rS);
                if (rS.status.isSuccess)
                {
                    list = rS.record as CustomList;
                    if (list != null)
                    {
                        return list.internalId;
                    }
                }
            }
            catch (Exception ex)
            {
                //logger.error(string.Format("Error - {0}", ));
            }
            return "";
        }

        public static string get_internalIDOpprtunity(string OpportunityName)
        {
            var searchResult = new SearchResult();
            string unit = "";
            try
            {
                SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
                var unitSearch = new OpportunitySearch();
                var unitBasic = new OpportunitySearchBasic();
                var unitAdvsearch = new OpportunitySearchAdvanced();
                var searchfield = new SearchStringField();

                searchfield.@operator = SearchStringFieldOperator.@is;
                searchfield.operatorSpecified = true;
                searchfield.searchValue = OpportunityName;
                unitBasic.title = searchfield;
                unitSearch.basic = unitBasic;
                unitAdvsearch.criteria = unitSearch;

                _client.search(US.GetDestinationePassport(), null, null, null, unitSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.info(2, string.Format("Error code: {0}", searchResult.status.statusDetail[0].code.ToString()));

                    var error = string.Format("Unit Search is failed.\nDetail :{0}", searchResult.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }
            }
            catch (Exception ex)
            {
                error_flag = true;
                var error =
                    string.Format("Exception occurred during webservice call while retrieving UNit search {0}\nDetails:{1}\n{2}",
                         ex.Message, ex.StackTrace);
                logger.error(error);
            }


            if (searchResult.recordList != null)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    foreach (var item in searchResult.recordList)
                    {
                        if (item.GetType() == typeof(UnitsType))
                            unit = ((UnitsType)item).internalId;

                    }
                }
            }
            else
            {
                logger.info(string.Format("Webservice returned null : {0}",
                                           unit));
                var msg = string.Format("Webservice did not find Unit.: {0}", unit);
                unit = "";
            }
            return unit;
        }


        #endregion

        public static string get_ItemInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new ItemSearch();
                var itemBasic = new ItemSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                //itemBasic.itemId = ssf;
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_ItemInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }


                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID

                        string type = searchResult.recordList[0].GetType().Name;

                        if (typeof(NonInventorySaleItem) == searchResult.recordList[0].GetType())
                        {
                            NonInventorySaleItem internalID = (NonInventorySaleItem)(from s in searchResult.recordList
                                                                                     where s.GetType() == typeof(NonInventorySaleItem)
                                                                                     select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(InventoryItem) == searchResult.recordList[0].GetType())
                        {
                            InventoryItem internalID = (InventoryItem)(from s in searchResult.recordList
                                                                       where s.GetType() == typeof(InventoryItem)
                                                                       select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(ServiceSaleItem) == searchResult.recordList[0].GetType())
                        {
                            ServiceSaleItem internalID = (ServiceSaleItem)(from s in searchResult.recordList
                                                                           where s.GetType() == typeof(ServiceSaleItem)
                                                                           select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(ServiceResaleItem) == searchResult.recordList[0].GetType())
                        {
                            ServiceResaleItem internalID = (ServiceResaleItem)(from s in searchResult.recordList
                                                                               where s.GetType() == typeof(ServiceResaleItem)
                                                                               select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(LotNumberedInventoryItem) == searchResult.recordList[0].GetType())
                        {
                            LotNumberedInventoryItem internalID = (LotNumberedInventoryItem)(from s in searchResult.recordList
                                                                                             where s.GetType() == typeof(LotNumberedInventoryItem)
                                                                                             select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(LotNumberedAssemblyItem) == searchResult.recordList[0].GetType())
                        {
                            LotNumberedAssemblyItem internalID = (LotNumberedAssemblyItem)(from s in searchResult.recordList
                                                                                           where s.GetType() == typeof(LotNumberedAssemblyItem)
                                                                                           select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(SerializedInventoryItem) == searchResult.recordList[0].GetType())
                        {
                            SerializedInventoryItem internalID = (SerializedInventoryItem)(from s in searchResult.recordList
                                                                                           where s.GetType() == typeof(SerializedInventoryItem)
                                                                                           select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(NonInventoryPurchaseItem) == searchResult.recordList[0].GetType())
                        {
                            NonInventoryPurchaseItem internalID = (NonInventoryPurchaseItem)(from s in searchResult.recordList
                                                                                             where s.GetType() == typeof(NonInventoryPurchaseItem)
                                                                                             select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        else if (typeof(NonInventoryResaleItem) == searchResult.recordList[0].GetType())
                        {
                            NonInventoryResaleItem internalID = (NonInventoryResaleItem)(from s in searchResult.recordList
                                                                                         where s.GetType() == typeof(NonInventoryResaleItem)
                                                                                         select s).FirstOrDefault();

                            return internalID.internalId;
                        }

                        return null;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_ItemInternalID_DNA()\n Error item internal id : " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        public static string get_EntityInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new CustomerSearch();
                var itemBasic = new CustomerSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;
                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);
                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_EntityInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }

                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID
                        searchResult.GetType();

                        //if (typeof(EntityGroup) == searchResult.GetType())
                        {
                            Customer internalID = (Customer)(from s in searchResult.recordList
                                                             where s.GetType() == typeof(Customer)
                                                             select s).FirstOrDefault();
                            return internalID.internalId;
                        }
                        //return null;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_EntityInternalID_DNA()\n Error in entity internal id: " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        public static string get_SalesrepInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new EmployeeSearch();
                var itemBasic = new EmployeeSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;
                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);
                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_EntityInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }

                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID
                        searchResult.GetType();

                        //if (typeof(EntityGroup) == searchResult.GetType())
                        {
                            Employee internalID = (Employee)(from s in searchResult.recordList
                                                             where s.GetType() == typeof(Employee)
                                                             select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        //return null;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_EntityInternalID_DNA()\n Error in entity internal id: " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        public static string get_JobInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new JobSearch();
                var itemBasic = new JobSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_JobInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }


                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID

                        //if (typeof(Job) == searchResult.GetType())
                        {
                            Job internalID = (Job)(from s in searchResult.recordList
                                                   where s.GetType() == typeof(Job)
                                                   select s).FirstOrDefault();

                            return internalID.internalId;
                        }
                        //return null;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_JobInternalID_DNA()\n Error in Job internal id : " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        public static string get_PartnerInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new PartnerSearch();
                var itemBasic = new PartnerSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_PartnerInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }


                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID

                        Partner internalID = (Partner)(from s in searchResult.recordList
                                                       where s.GetType() == typeof(Partner)
                                                       select s).FirstOrDefault();

                        return internalID.internalId;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_PartnerInternalID_DNA()\n Error in partner internal id : " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        public static string getAccountInternalID(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new AccountSearch();
                var itemBasic = new AccountSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at getAccountInternalID()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at getAccountInternalID()\n error while getting internalID.: " + ex.Message);
                error_flag = true;
                return null;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get InternalID
                    Account internalID = (Account)(from s in searchResult.recordList
                                                   where s.GetType() == typeof(Account)
                                                   select s).FirstOrDefault();

                    return internalID.internalId;
                }
                else
                {
                    //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                //                           SONumber));
                return null;
            }
        }

        public static BaseRef[] getSelectoptions_exp(string itemInternalID, string entityInternalID)
        {
            try
            {
                Passport passportobj = new Passport();
                GetSelectFilterByFieldValue[] getSelectValueFilterbys = new GetSelectFilterByFieldValue[2];
                getSelectValueFilterbys[0] = new GetSelectFilterByFieldValue { field = "item", internalId = itemInternalID, sublist = "itemList" };
                getSelectValueFilterbys[1] = new GetSelectFilterByFieldValue { field = "entity", internalId = entityInternalID, sublist = null };
                GetSelectValueFieldDescription selectValueField = new GetSelectValueFieldDescription
                {
                    recordType = RecordType.salesOrder,
                    recordTypeSpecified = true,
                    sublist = "itemList",
                    field = "units",
                    filterByValueList = getSelectValueFilterbys
                };
                GetSelectValueResult result;

                _client.getSelectValue(US.GetDestinationePassport(), null, null, null, selectValueField, 1, out result);

                if (!result.status.isSuccess)
                {
                    logger.info("not working");
                    return null;
                }
                else
                {
                    return result.baseRefList;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error : " + ex.Message);
                return null;
            }

        }

        public static string get_LocationInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new LocationSearch();
                var itemBasic = new LocationSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_LocationInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_LocationInternalID_DNA()\n Error in location internal id : " + ex.Message);
                error_flag = true;
                return null;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get InternalID
                    Location internalID = (Location)(from s in searchResult.recordList
                                                     where s.GetType() == typeof(Location)
                                                     select s).FirstOrDefault();

                    return internalID.internalId;
                }
                else
                {
                    //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                //                           SONumber));
                return null;
            }
        }

        public static string get_DepartmentInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new DepartmentSearch();
                var itemBasic = new DepartmentSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_DepartmentInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_DepartmentInternalID_DNA()\n error while getting internalID. of Department : " + ex.Message);
                error_flag = true;
                return null;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get InternalID
                    Department internalID = (Department)(from s in searchResult.recordList
                                                         where s.GetType() == typeof(Department)
                                                         select s).FirstOrDefault();

                    return internalID.internalId;
                }
                else
                {
                    //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                //                           SONumber));
                return null;
            }
        }

        public static string get_subsidiaryInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new SubsidiarySearch();
                var itemBasic = new SubsidiarySearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_subsidiaryInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_subsidiaryInternalID_DNA()\n error while getting internalID. : " + ex.Message);
                error_flag = true;
                return null;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get InternalID
                    Subsidiary internalID = (Subsidiary)(from s in searchResult.recordList
                                                         where s.GetType() == typeof(Subsidiary)
                                                         select s).FirstOrDefault();

                    return internalID.internalId;
                }
                else
                {
                    //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                //                           SONumber));
                return null;
            }
        }

        public static string get_classInternalID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new ClassificationSearch();
                var itemBasic = new ClassificationSearchBasic();
                var ssf = new SearchStringCustomField();

                ssf.internalId = customFieldName;
                ssf.@operator = SearchStringFieldOperator.@is;
                ssf.operatorSpecified = true;
                ssf.searchValue = InternalID;
                itemBasic.customFieldList = new SearchCustomField[] { ssf };
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_subsidiaryInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_subsidiaryInternalID_DNA()\n error while getting internalID. of Department : " + ex.Message);
                error_flag = true;
                return null;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get InternalID
                    Classification internalID = (Classification)(from s in searchResult.recordList
                                                                 where s.GetType() == typeof(Classification)
                                                                 select s).FirstOrDefault();

                    return internalID.internalId;
                }
                else
                {
                    //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                //                           SONumber));
                return null;
            }
        }

        public static string GetUnitSearch(string Unitname)
        {
            var searchResult = new SearchResult();
            string unit = "";
            try
            {

                var unitSearch = new UnitsTypeSearch();
                var unitBasic = new UnitsTypeSearchBasic();
                var unitAdvsearch = new UnitsTypeSearchAdvanced();
                var searchfield = new SearchStringField();

                searchfield.@operator = SearchStringFieldOperator.@is;
                searchfield.operatorSpecified = true;
                searchfield.searchValue = Unitname;
                unitBasic.abbreviation = searchfield;
                unitSearch.basic = unitBasic;
                unitAdvsearch.criteria = unitSearch;
                SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

                _client.search(US.GetDestinationePassport(), null, null, null, unitSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.info(2, string.Format("Error code: {0}", searchResult.status.statusDetail[0].code.ToString()));

                    var error = string.Format("Unit Search is failed.\nDetail :{0}", searchResult.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }
            }
            catch (Exception ex)
            {
                error_flag = true;
                var error =
                    string.Format("Exception occurred during webservice call while retrieving UNit search {0}\nDetails:{1}\n{2}",
                         ex.Message, ex.StackTrace);
                logger.error(error);
            }


            if (searchResult.recordList != null)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    foreach (var item in searchResult.recordList)
                    {
                        if (item.GetType() == typeof(UnitsType))
                            unit = ((UnitsType)item).internalId;

                    }
                }
            }
            else
            {
                logger.info(string.Format("Webservice returned null : {0}",
                                           unit));
                var msg = string.Format("Webservice did not find Unit.: {0}", unit);
                unit = "";
            }
            return unit;
        }

        public static RecordRef get_ItemUnitID_DNA(string InternalID, string customFieldName)
        {
            var searchResult = new SearchResult();
            try
            {
                var itemSearch = new ItemSearch();
                var itemBasic = new ItemSearchBasic();
                var ssf = new SearchMultiSelectField();

                //ssf.internalId = customFieldName;
                ssf.@operator = SearchMultiSelectFieldOperator.anyOf;
                ssf.operatorSpecified = true;
                ssf.searchValue = new RecordRef[] { new RecordRef() { internalId = InternalID } };
                //itemBasic.customFieldList = new SearchCustomField[] { ssf };
                //itemBasic.itemId = ssf;
                itemBasic.internalId = ssf;
                itemSearch.basic = itemBasic;

                _client.search(US.GetDestinationePassport(), null, null, null, itemSearch, out searchResult);

                if (!searchResult.status.isSuccess)
                {
                    logger.error(string.Format("Error at get_ItemInternalID_DNA()\n error while getting internalID. : {0}", searchResult.status.statusDetail[0].message));
                    error_flag = true;
                    return null;
                }


                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get InternalID
                        UnitsType h = new UnitsType();
                        string type = searchResult.recordList[0].GetType().Name;

                        if (typeof(NonInventorySaleItem) == searchResult.recordList[0].GetType())
                        {
                            NonInventorySaleItem internalID = (NonInventorySaleItem)(from s in searchResult.recordList
                                                                                     where s.GetType() == typeof(NonInventorySaleItem)
                                                                                     select s).FirstOrDefault();

                            return internalID.unitsType;

                        }
                        else if (typeof(InventoryItem) == searchResult.recordList[0].GetType())
                        {
                            InventoryItem internalID = (InventoryItem)(from s in searchResult.recordList
                                                                       where s.GetType() == typeof(InventoryItem)
                                                                       select s).FirstOrDefault();

                            return internalID.unitsType;
                        }
                        else if (typeof(ServiceSaleItem) == searchResult.recordList[0].GetType())
                        {
                            ServiceSaleItem internalID = (ServiceSaleItem)(from s in searchResult.recordList
                                                                           where s.GetType() == typeof(ServiceSaleItem)
                                                                           select s).FirstOrDefault();

                            return internalID.unitsType;
                        }
                        else if (typeof(ServiceResaleItem) == searchResult.recordList[0].GetType())
                        {
                            ServiceResaleItem internalID = (ServiceResaleItem)(from s in searchResult.recordList
                                                                               where s.GetType() == typeof(ServiceResaleItem)
                                                                               select s).FirstOrDefault();

                            return internalID.unitsType;
                        }
                        else if (typeof(LotNumberedInventoryItem) == searchResult.recordList[0].GetType())
                        {
                            LotNumberedInventoryItem internalID = (LotNumberedInventoryItem)(from s in searchResult.recordList
                                                                                             where s.GetType() == typeof(LotNumberedInventoryItem)
                                                                                             select s).FirstOrDefault();

                            return internalID.unitsType;
                        }
                        else if (typeof(LotNumberedAssemblyItem) == searchResult.recordList[0].GetType())
                        {
                            LotNumberedAssemblyItem internalID = (LotNumberedAssemblyItem)(from s in searchResult.recordList
                                                                                           where s.GetType() == typeof(LotNumberedAssemblyItem)
                                                                                           select s).FirstOrDefault();

                            return internalID.unitsType;
                        }

                        return null;
                    }
                    else
                    {
                        //logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    //logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                    //                           SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.info("Error at get_ItemInternalID_DNA()\n Error item internal id : " + ex.Message);
                error_flag = true;
                return null;
            }
        }

        #endregion



        #region Retreive SO

        private static SalesOrder GetSOFromWebService(string SONumber)
        {
            logger.info(string.Format("Calling webservice..."));

            try
            {
                var so = new SalesOrder();
                var tranSearchAdv = new TransactionSearchAdvanced();

                //RecordRef recRef = new RecordRef(){ externalId = SONumber,typeSpecified=true, type= RecordType.purchaseOrder};
                var tranSearch = new TransactionSearch();
                var basic = new TransactionSearchBasic();
                var searchfield = new SearchMultiSelectField();
                searchfield = new SearchMultiSelectField();
                searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
                searchfield.operatorSpecified = true;
                searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = SONumber, type = RecordType.salesOrder, typeSpecified = true } };

                basic.internalId = searchfield;
                tranSearch.basic = basic;

                tranSearchAdv.criteria = tranSearch;
                SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
                var searchResult = new SearchResult();

                _client.search(US.GetSourcePassport(), null, null, sPref, tranSearch, out searchResult);
                logger.info(string.Format("Webservice call complete"));

                if (!searchResult.status.isSuccess)
                {
                    var error =
                        string.Format("Error at GetSOFromWebService() Webservice call failed while retrieving Sales order No: {0}\nDetail:{1}",
                                      SONumber, searchResult.status.statusDetail[0].message);

                    logger.error(error);
                    return null;
                }

                if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
                {
                    if (searchResult.recordList.Count() > 0)
                    {
                        //get sales order
                        var Salesorder = (SalesOrder)(from s in searchResult.recordList
                                                      where s.GetType() == typeof(SalesOrder)
                                                      select s).FirstOrDefault();

                        return Salesorder;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               SONumber));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Error at GetSOFromWebService() \nException occurred during webservice call while retrieving Sales order No: {0}\nDetails:{1}\n{2}",
                        SONumber, ex.Message, ex.StackTrace);
                logger.error(error);
                throw ex;
            }
        }

        private static SalesOrder GetSOFromWebServiceForIF(string SONumber)
        {
            logger.info(string.Format("Calling webservice..."));

            var so = new SalesOrder();
            var tranSearchAdv = new TransactionSearchAdvanced();
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = SONumber, type = RecordType.salesOrder, typeSpecified = true } };

            basic.internalId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, tranSearch, out searchResult);
                logger.info(string.Format("Webservice call complete"));

                if (!searchResult.status.isSuccess)
                {
                    var error =
                        string.Format("Error at GetSOFromWebServiceForIF()\n Webservice call failed while retrieving Sales order No: {0}\nDetail:{1}",
                                      SONumber, searchResult.status.statusDetail[0].message);

                    logger.error(error);
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Error at GetSOFromWebServiceForIF()\n Exception occurred during webservice call while retrieving Sales order No: {0}\nDetails:{1}\n{2}",
                        SONumber, ex.Message, ex.StackTrace);
                logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var Salesorder = (SalesOrder)(from s in searchResult.recordList
                                                  where s.GetType() == typeof(SalesOrder)
                                                  select s).FirstOrDefault();

                    return Salesorder;
                }
                else
                {
                    logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                           SONumber));
                return null;
            }
            //
        }

        private static SalesOrder getSOFromDNA(string SONumber)
        {
            logger.info(string.Format("Calling webservice..."));

            var so = new SalesOrder();
            var tranSearchAdv = new TransactionSearchAdvanced();
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { externalId = SONumber, type = RecordType.salesOrder, typeSpecified = true } };

            basic.externalId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, tranSearch, out searchResult);
                logger.info(string.Format("Webservice call complete"));

                if (!searchResult.status.isSuccess)
                {
                    var error =
                        string.Format("Error at getSOFromDNA()\n Webservice call failed while retrieving Sales order No: {0}\nDetail:{1}",
                                      SONumber, searchResult.status.statusDetail[0].message);

                    logger.error(error);
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Error at getSOFromDNA()\n Exception occurred during webservice call while retrieving Sales order No: {0}\nDetails:{1}\n{2}",
                        SONumber, ex.Message, ex.StackTrace);
                logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var Salesorder = (SalesOrder)(from s in searchResult.recordList
                                                  where s.GetType() == typeof(SalesOrder)
                                                  select s).FirstOrDefault();

                    return Salesorder;
                }
                else
                {
                    logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                           SONumber));
                return null;
            }
            //
        }

        #endregion

        #region Retreive IF

        private static SearchResult getAllItemFulfillmentRecords(string soid)
        {

            //soid = "1550";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = soid, type = RecordType.salesOrder, typeSpecified = true } };

            basic.createdFrom = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

            //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
            var result = new SearchResult();

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getAllItemFulfillmentRecords()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get Item Fulfillment
                        //var ItemFulfillment = (ItemFulfillment)(from s in result.recordList
                        //                                        where s.GetType() == typeof(ItemFulfillment)
                        //                                        select s).FirstOrDefault();

                        return result;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", soid));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               soid));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getAllItemFulfillmentRecords()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }

        }

        private static ItemFulfillment get_SNA_IF(string IF_ID)
        {
            //IF_ID = "1550";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = IF_ID, type = RecordType.itemFulfillment, typeSpecified = true } };

            basic.internalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at get_SNA_IF()\nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var ItemFulfillment = (ItemFulfillment)(from s in result.recordList
                                                                where s.GetType() == typeof(ItemFulfillment)
                                                                select s).FirstOrDefault();

                        return ItemFulfillment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Item Fulfillment for ID : {0}", IF_ID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Item Fulfillment for ID : {0}",
                                               IF_ID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at get_SNA_IF()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static ItemFulfillment getDNA_IF(string soid)
        {
            //soid = "1550";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { externalId = soid, type = RecordType.salesOrder, typeSpecified = true } };

            basic.externalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getDNA_IF()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var ItemFulfillment = (ItemFulfillment)(from s in result.recordList
                                                                where s.GetType() == typeof(ItemFulfillment)
                                                                select s).FirstOrDefault();

                        return ItemFulfillment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", soid));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               soid));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getDNA_IF()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static ItemFulfillment GetIFFromWebService(string SONumber)
        {
            logger.info(string.Format("Calling webservice..."));

            var so = new SalesOrder();
            var tranSearchAdv = new TransactionSearchAdvanced();

            //RecordRef recRef = new RecordRef(){ externalId = SONumber,typeSpecified=true, type= RecordType.purchaseOrder};
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchStringField();
            searchfield = new SearchStringField();
            searchfield.@operator = SearchStringFieldOperator.@is;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = SONumber;

            basic.tranId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, tranSearch, out searchResult);
                logger.info(string.Format("Webservice call complete"));

                if (!searchResult.status.isSuccess)
                {
                    //if (searchResult.status.statusDetail[0].code == LibApi.NetSuitePortType.StatusDetailCodeType.SESSION_TIMED_OUT)
                    //{
                    //    var error = string.Format("Webservice timed out.");
                    //    logger.error(error);
                    //}
                    //else
                    //{
                    //    var error =
                    //        string.Format("Webservice call failed while retrieving Sales order No: {0}\nDetail:{1}",
                    //                      SONumber, searchResult.status.statusDetail[0].message);

                    //    logger.error(error);
                    //    return null;
                    //}
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Error at GetIFFromWebService()\n Exception occurred during webservice call while retrieving Sales order No: {0}\nDetails:{1}\n{2}",
                        SONumber, ex.Message, ex.StackTrace);
                logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var ItemFulfillment = (ItemFulfillment)(from s in searchResult.recordList
                                                            where s.GetType() == typeof(ItemFulfillment)
                                                            select s).FirstOrDefault();

                    return ItemFulfillment;
                }
                else
                {
                    logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", SONumber));
                    return null;
                }
            }
            else
            {
                logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                           SONumber));
                return null;
            }
            //
        }

        private static ItemFulfillment getItemFulfillmentForInvoice(string ItemFulfillmentID)
        {
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = ItemFulfillmentID, type = RecordType.salesOrder, typeSpecified = true } };

            basic.internalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getItemFulfillmentForInvoice()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get Item Fulfillment
                        var ItemFulfillment = (ItemFulfillment)(from s in result.recordList
                                                                where s.GetType() == typeof(ItemFulfillment)
                                                                select s).FirstOrDefault();

                        return ItemFulfillment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", ItemFulfillmentID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               ItemFulfillmentID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getItemFulfillmentForInvoice()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        #endregion

        #region Retreive Invoice

        private static Invoice get_SNA_InvoiceID(string invoice_ref)
        {
            logger.info(string.Format("Calling webservice..."));

            var so = new Invoice();
            var tranSearchAdv = new TransactionSearchAdvanced();

            //RecordRef recRef = new RecordRef(){ externalId = SONumber,typeSpecified=true, type= RecordType.purchaseOrder};
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchStringField();
            searchfield = new SearchStringField();
            searchfield.@operator = SearchStringFieldOperator.@is;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = invoice_ref;

            basic.tranId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, tranSearch, out searchResult);
                logger.info(string.Format("Webservice call complete"));

                if (!searchResult.status.isSuccess)
                {
                    var error =
                        string.Format("Error at get_SNA_InvoiceID()\n Webservice call failed while retrieving Sales order No: {0}\nDetail:{1}",
                                      invoice_ref, searchResult.status.statusDetail[0].message);

                    logger.error(error);
                    error_flag = true;
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Error at get_SNA_InvoiceID()\n Exception occurred during webservice call while retrieving Sales order No: {0}\nDetails:{1}\n{2}",
                        invoice_ref, ex.Message, ex.StackTrace);
                logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get Invoice
                    var Invoice = (Invoice)(from s in searchResult.recordList
                                            where s.GetType() == typeof(Invoice)
                                            select s).FirstOrDefault();

                    return Invoice;
                }
                else
                {
                    logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", invoice_ref));
                    return null;
                }
            }
            else
            {
                logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                           invoice_ref));
                return null;
            }
        }

        private static Invoice getSingleInvoicetRecords(string InvoiceID)
        {
            //   InvoiceID = "1550";
            #region Veriable Declaration

            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            #endregion

            #region Assignment on Variable

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = InvoiceID, type = RecordType.invoice, typeSpecified = true } };

            basic.internalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            #endregion

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getSingleInvoicetRecords()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get Invoice
                        //Invoice Invoice = (Invoice)(from s in result.recordList
                        //                            where s.GetType() == typeof(Invoice)
                        //                            select s).FirstOrDefault();

                        foreach (var record in result.recordList)
                        {
                            if (record.GetType() == typeof(Invoice))
                            {
                                return (Invoice)record;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        return null;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", InvoiceID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               InvoiceID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getSingleInvoicetRecords()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static Invoice getInvoiceRecords(string InvoiceID)
        {

            //   soid = "1550";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = InvoiceID, type = RecordType.salesOrder, typeSpecified = true } };

            basic.internalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getInvoiceRecords()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var Invoice = (Invoice)(from s in result.recordList
                                                where s.GetType() == typeof(Invoice)
                                                select s).FirstOrDefault();

                        return Invoice;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", InvoiceID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               InvoiceID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getInvoiceRecords()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static SearchResult getAllInvoicetRecords(string soid)
        {
            //   soid = "1550";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = soid, type = RecordType.invoice, typeSpecified = true } };

            basic.createdFrom = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getAllInvoicetRecords()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get Invoice

                        //foreach (var i in result.recordList)
                        //{
                        //    if (i.GetType() == typeof(Invoice))
                        //    {
                        //        /*bool IsMigrated = (bool)(from CustomFieldRef cref in i.customFieldList
                        //                                 where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("CustomMigrateStatus")
                        //                                 select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();*/
                        //    }

                        //}

                        //Invoice Invoice = (Invoice)(from s in result.recordList
                        //                        where s.GetType() == typeof(Invoice)
                        //                        select s);


                        //if (IsMigrated == false)
                        {
                            return result;
                        }
                        //return null;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", soid));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               soid));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getAllInvoicetRecords()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        public static List<SalesTaxItem> GetAllCurrencies()
        {
            try
            {
                var currs = new List<SalesTaxItem>();
                var gaCurr = new GetAllRecord();
                GetAllResult result = null;
                gaCurr.recordType = GetAllRecordType.salesTaxItem;
                gaCurr.recordTypeSpecified = true;
                // Get all the currencies. No particular order
                try
                {
                    logger.info("Getting All Tax Code...");
                    _client.getAll(US.GetDestinationePassport(), null, null, null, gaCurr, out result);
                }
                catch (Exception ex)
                {
                    string message = string.Format("Exception occured while getting Currency data from Netsuite webservice, Error : {0}", ex.Message);
                    logger.error(message);
                }

                if (result != null)
                {
                    var rrecords = result.recordList;
                    if (rrecords != null)
                    {
                        foreach (var item in rrecords)
                        {
                            var c = item as SalesTaxItem;
                            if (c != null)
                            {
                                currs.Add(c);
                            }
                        }
                    }
                    return currs;
                }
                return currs;
            }
            catch (Exception ex)
            {

                logger.info("Error : " + ex.Message);
                return null;
            }
        }

        /* private static int getCountOfSNA_Invoice(string sointernalID)
         {

             //soid = "1550";
             var transearch_adv = new TransactionSearchAdvanced();
             var transearch = new TransactionSearch();
             var basic = new TransactionSearchBasic();
             var searchfield = new SearchMultiSelectField();

             searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
             searchfield.operatorSpecified = true;
             searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = sointernalID, type = RecordType.salesOrder, typeSpecified = true } };

             basic.createdFrom = searchfield;
             transearch.basic = basic;
             transearch_adv.criteria = transearch;

             SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

             //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
             var result = new SearchResult();

             try
             {
                 _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                 if (!result.status.isSuccess)
                 {
                     var error = string.Format("Error while getting pending orders \nDetail: {0}",
                                               result.status.statusDetail[0].message);
                     logger.error(error);
                     return 0;
                 }

                 int count = 0;
                 if (result.recordList != null && result.recordList.Count() > 0)
                 {
                     if (result.recordList.Count() > 0)
                     {
                         //get sales order
                         Int32 Invoice = (from s in result.recordList
                                          where s.GetType() == typeof(Invoice)
                                          select s).Count();

                         return Invoice;
                     }
                     else
                     {
                         logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", sointernalID));
                         return 0;
                     }
                 }
                 else
                 {
                     logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                                sointernalID));
                     return 0;
                 }
             }
             catch (Exception ex)
             {
                 var error = string.Format("Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                           ex.StackTrace);
                 //email
                 logger.error(error);
                 return 0;
             }
             return 0;
         }*/

        /* private static Invoice getDNAInvoice(string soid)
         {
             //soid = "1550";
             var transearch_adv = new TransactionSearchAdvanced();
             var transearch = new TransactionSearch();
             var basic = new TransactionSearchBasic();
             var searchfield = new SearchMultiSelectField();
             SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
             var result = new SearchResult();

             searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
             searchfield.operatorSpecified = true;
             searchfield.searchValue = new RecordRef[] { new RecordRef() { externalId = soid, type = RecordType.salesOrder, typeSpecified = true } };

             basic.externalId = searchfield;
             transearch.basic = basic;
             transearch_adv.criteria = transearch;

             try
             {
                 _client.search(US.GetDestinationePassport(), null, null, sPref, transearch_adv, out result);

                 if (!result.status.isSuccess)
                 {
                     var error = string.Format("Error while getting pending orders \nDetail: {0}",
                                               result.status.statusDetail[0].message);
                     logger.error(error);
                     return null;
                 }

                 if (result.recordList != null && result.recordList.Count() > 0)
                 {
                     if (result.recordList.Count() > 0)
                     {
                         //get sales order
                         var Invoice = (Invoice)(from s in result.recordList
                                                 where s.GetType() == typeof(Invoice)
                                                 select s).FirstOrDefault();

                         return Invoice;
                     }
                     else
                     {
                         logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", soid));
                         return null;
                     }
                 }
                 else
                 {
                     logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                                soid));
                     return null;
                 }
             }
             catch (Exception ex)
             {
                 var error = string.Format("Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                           ex.StackTrace);
                 //email
                 logger.error(error);
                 return null;
             }
         } 
         */

        #endregion

        #region Retreive CP

        private static CustomerPayment checkFor_DNA_CP(string CP_ID)
        {
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { externalId = CP_ID, type = RecordType.customerPayment, typeSpecified = true } };

            basic.externalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

            //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
            var result = new SearchResult();

            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at checkFor_DNA_CP ()\n Error in checkFor_DNA_CP while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    error_flag = true;
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var CustomerPayment = (CustomerPayment)(from s in result.recordList
                                                                where s.GetType() == typeof(CustomerPayment)
                                                                select s).FirstOrDefault();

                        return CustomerPayment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find CP for order no.: {0}", CP_ID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for CP for order no.: {0}",
                                               CP_ID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at checkFor_DNA_CP ()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                error_flag = true;
                return null;
            }
        }

        private static CustomerPayment getAllCustomerPaymentRecords(string customerPayment)
        {
            //invoiceID = "1551";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };
            var result = new SearchResult();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = customerPayment, type = RecordType.customerPayment, typeSpecified = true } };

            basic.createdFrom = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getAllCustomerPaymentRecords ()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var CustomerPayment = (CustomerPayment)(from s in result.recordList
                                                                where s.GetType() == typeof(CustomerPayment)
                                                                select s).FirstOrDefault();

                        return CustomerPayment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", customerPayment));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               customerPayment));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getAllCustomerPaymentRecords ()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static CustomerPayment getCustomerPaymentRecords(string CustomerPaymentID)
        {

            //invoiceID = "1551";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = CustomerPaymentID, type = RecordType.customerPayment, typeSpecified = true } };

            basic.internalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

            //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
            var result = new SearchResult();

            try
            {
                _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getCustomerPaymentRecords ()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var CustomerPayment = (CustomerPayment)(from s in result.recordList
                                                                where s.GetType() == typeof(CustomerPayment)
                                                                select s).FirstOrDefault();

                        return CustomerPayment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", CustomerPaymentID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               CustomerPaymentID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getCustomerPaymentRecords ()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        private static CustomerPayment get_DNA_CustomerPayment(string CustomerPaymentID)
        {

            //invoiceID = "1551";
            var transearch_adv = new TransactionSearchAdvanced();
            var transearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchMultiSelectField();

            searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = new RecordRef[] { new RecordRef() { externalId = CustomerPaymentID, type = RecordType.customerPayment, typeSpecified = true } };

            basic.externalId = searchfield;
            transearch.basic = basic;
            transearch_adv.criteria = transearch;

            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

            //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
            var result = new SearchResult();

            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, transearch_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at get_DNA_CustomerPayment ()\n Error while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    return null;
                }

                if (result.recordList != null && result.recordList.Count() > 0)
                {
                    if (result.recordList.Count() > 0)
                    {
                        //get sales order
                        var CustomerPayment = (CustomerPayment)(from s in result.recordList
                                                                where s.GetType() == typeof(CustomerPayment)
                                                                select s).FirstOrDefault();

                        return CustomerPayment;
                    }
                    else
                    {
                        logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", CustomerPaymentID));
                        return null;
                    }
                }
                else
                {
                    logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                               CustomerPaymentID));
                    return null;
                }
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at get_DNA_CustomerPayment ()\n Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }
        }

        /* private static CustomerPayment getAllCustPaymentRecords(string soid)
         {
             soid = "1551";
             var transearch_adv = new TransactionSearchAdvanced();
             var transearch = new TransactionSearch();
             var basic = new TransactionSearchBasic();
             var searchfield = new SearchMultiSelectField();

             searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
             searchfield.operatorSpecified = true;
             searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = soid, type = RecordType.customerPayment, typeSpecified = true } };

             basic.createdFrom = searchfield;
             transearch.basic = basic;
             transearch_adv.criteria = transearch;

             SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

             //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
             var result = new SearchResult();

             try
             {
                 _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                 if (!result.status.isSuccess)
                 {
                     var error = string.Format("Error while getting pending orders \nDetail: {0}",
                                               result.status.statusDetail[0].message);
                     logger.error(error);
                     return null;
                 }

                 if (result.recordList != null && result.recordList.Count() > 0)
                 {
                     if (result.recordList.Count() > 0)
                     {
                         //get sales order
                         var CustomerPayment = (CustomerPayment)(from s in result.recordList
                                                                 where s.GetType() == typeof(CustomerPayment)
                                                                 select s).FirstOrDefault();

                         return CustomerPayment;
                     }
                     else
                     {
                         logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", soid));
                         return null;
                     }
                 }
                 else
                 {
                     logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                                soid));
                     return null;
                 }
             }
             catch (Exception ex)
             {
                 var error = string.Format("Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                           ex.StackTrace);
                 //email
                 logger.error(error);
                 return null;
             }
         }*/

        #endregion



        #region Update Migration Status

        public static void UpdateMigrationStatus(object tempobject)
        {

            var temp = tempobject.GetType().Name;
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };
            #region SalesOrder

            if (temp == "SalesOrder")
            {
                try
                {
                    SalesOrder cashSaleForRA = (SalesOrder)tempobject;
                    SalesOrder newSOrder = new SalesOrder();
                    List<CustomFieldRef> customList = new List<CustomFieldRef>();
                    List<CustomFieldRef> customref = new List<CustomFieldRef>();
                    BooleanCustomFieldRef customField = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomMigrateStatus"], value = true };
                    WriteResponse response = null;

                    customref.Add(customField);
                    newSOrder.customFieldList = customref.ToArray();
                    newSOrder.internalId = cashSaleForRA.internalId;
                    newSOrder.customFieldList = customref.ToArray();

                    _client.update(US.GetSourcePassport(), null, null, pref, newSOrder, out response);

                    if (!response.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at UpdateMigrationStatus()\nFailed unable to update Migration status for SO number : {1} \nDetail :{0}", response.status.statusDetail[0].message, newSOrder.internalId);
                        logger.error(2, error);
                        error_flag = true;
                    }
                    else
                    {
                        logger.info("Migration status updated successfully for Sales Order : " + cashSaleForRA.internalId);
                    }
                }
                catch (Exception ex)
                {
                    logger.info("Error at UpdateMigrationStatus()\nError : " + ex.Message);
                    error_flag = true;
                }
            }
            #endregion

            #region Itemfulfillment

            else if (temp == "ItemFulfillment")
            {
                try
                {
                    ItemFulfillment cashSaleForRA = (ItemFulfillment)tempobject;
                    List<CustomFieldRef> customList = new List<CustomFieldRef>();

                    BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("CustomMigrateStatus"), value = true };
                    customList.Add(prepaidcostCheckRef);

                    ItemFulfillment csToUpdate = new ItemFulfillment()
                    {
                        internalId = cashSaleForRA.internalId,
                        customFieldList = customList.ToArray()
                    };

                    WriteResponse response = null;

                    _client.update(US.GetSourcePassport(), null, null, pref, csToUpdate, out response);

                    if (!response.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
                        logger.error(2, error);
                    }
                    else
                    {
                        logger.info("Migration status updated successfully for Item Fulfillment : " + cashSaleForRA.internalId);
                    }
                }
                catch (Exception ex)
                {
                    error_flag = true;
                    logger.info(string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF.\nDetail :{0}", ex.Message));
                }
            }
            #endregion

            #region Invoice
            else if (temp == "Invoice")
            {
                try
                {
                    Invoice cashSaleForRA = (Invoice)tempobject;
                    List<CustomFieldRef> customref = new List<CustomFieldRef>();
                    BooleanCustomFieldRef customField = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings["CustomMigrateStatus"], value = true };
                    customref.Add(customField);
                    cashSaleForRA.customFieldList = customref.ToArray();
                    cashSaleForRA.subTotalSpecified = false;
                    cashSaleForRA.recognizedRevenueSpecified = false;
                    cashSaleForRA.deferredRevenueSpecified = false;
                    cashSaleForRA.totalSpecified = false;
                    cashSaleForRA.totalCostEstimateSpecified = false;
                    cashSaleForRA.estGrossProfitPercentSpecified = false;
                    cashSaleForRA.estGrossProfitSpecified = false;
                    cashSaleForRA.discountTotalSpecified = false;
                    cashSaleForRA.shippingTax1RateSpecified = false;

                    for (int i = 0; i < cashSaleForRA.itemList.item.Count(); i++)
                    {
                        cashSaleForRA.itemList.item[i].quantityRemainingSpecified = false;
                        cashSaleForRA.itemList.item[i].costEstimateSpecified = false;
                    }

                    WriteResponse response = null;
                    _client.update(US.GetSourcePassport(), null, null, pref, cashSaleForRA, out response);
                    if (!response.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at UpdateMigrationStatus()\nunable to update migration status for Invoice number : {1}.\nDetail :{0}",
                            response.status.statusDetail[0].message, cashSaleForRA.internalId);
                        logger.error(2, error);
                    }
                    else
                    {
                        logger.info("Migration status updated successfully for Invoice : " + cashSaleForRA.internalId);
                    }
                }
                catch (Exception ex)
                {
                    logger.info(string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF.\nDetail :{0}", ex.Message));
                    error_flag = true;
                }
            }
            #endregion

            #region CustomerPayment
            else if (temp == "CustomerPayment")
            {
                try
                {
                    CustomerPayment cashSaleForRA = (CustomerPayment)tempobject;
                    List<CustomFieldRef> customList = new List<CustomFieldRef>();
                    WriteResponse response = null;
                    BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("CustomMigrateStatus"), value = true };
                    customList.Add(prepaidcostCheckRef);

                    CustomerPayment csToUpdate = new CustomerPayment()
                    {
                        internalId = cashSaleForRA.internalId,
                        customFieldList = customList.ToArray()
                    };
                    _client.update(US.GetSourcePassport(), null, null, pref, csToUpdate, out response);
                    if (!response.status.isSuccess)
                    {
                        logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                        var error = string.Format("Error at UpdateMigrationStatus()\nunable to update migration status for Invoice number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
                        logger.error(2, error);
                    }
                    else
                    {
                        logger.info("Migration status updated successfully for Customer Payment : " + cashSaleForRA.internalId);
                    }
                }
                catch (Exception ex)
                {
                    logger.info(string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF.\nDetail :{0}", ex.Message));
                    error_flag = true;
                }
            }
            #endregion

        }

        #endregion



        #region Get data from saved search

        public static SearchResult getAllSourceSO(NetSuitePortTypeClient _client, Logger logger, Utility US)
        {
            TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
            var result = new SearchResult();

            try
            {
                _client.search(US.GetSourcePassport(), null, null, null, tra_search_adv, out result);

                if (!result.status.isSuccess)
                {
                    var error = string.Format("Error at getAllSourceSO()\nError while getting pending orders \nDetail: {0}",
                                              result.status.statusDetail[0].message);
                    logger.error(error);
                    error_flag = true;
                    return null;
                }

                String sSearchId = result.searchId;
                logger.info(string.Format("Search Id = {0}", sSearchId));
                int iNumPages = result.totalPages;
                logger.info(string.Format("Total Pages = {0}", iNumPages));

                for (int i = 2; i <= iNumPages; i++)
                {
                    SearchResult searchResult = null;

                    _client.searchMoreWithId(US.GetSourcePassport(), null, null, null, sSearchId, i, out searchResult);

                    if (!result.status.isSuccess)
                    {
                        var error = string.Format("Error at getAllSourceSO()\nError while getting pending orders \nDetail: {0}",
                                                  result.status.statusDetail[0].message);
                        logger.error(error);
                        return null;
                    }

                    logger.info(string.Format("searchResult.searchRowList.Length = {0}",
                                               searchResult.searchRowList.Length));
                    if (searchResult.searchRowList.Length > 0)
                    {
                        result.searchRowList =
                            result.searchRowList.Concat(searchResult.searchRowList.AsEnumerable()).ToArray();
                        logger.info(string.Format("Result.searchRowList.Length = {0}",
                                                   result.searchRowList.Length));
                    }
                }
                logger.info(string.Format("Total = result.searchRowList.Length = {0}",
                                           result.searchRowList.Length));
            }
            catch (Exception ex)
            {
                var error = string.Format("Error at getAllSourceSO()\nException while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                          ex.StackTrace);
                //email
                logger.error(error);
                return null;
            }

            return result;
        }

        #endregion



        #region Commented Code

        /*public static void UpdateSaleOrder_MigrationStatus(SalesOrder salesorderForRA)
        {
            try
            {
                List<CustomFieldRef> customList = new List<CustomFieldRef>();
                WriteResponse response = null;
                BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("custbody_migratestatus"), value = true };
                customList.Add(prepaidcostCheckRef);

                SalesOrder soToUpdate = new SalesOrder()
                {
                    internalId = salesorderForRA.internalId,
                    customFieldList = customList.ToArray()
                };

                _client.update(US.GetSourcePassport(), null, null, null, soToUpdate, out response);

                if (!response.status.isSuccess)
                {
                    logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                    var error = string.Format("Sales order transfer is failed.\nDetail :{0}", response.status.statusDetail[0].message);
                    logger.error(2, error);
                    error_flag = true;
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("Failed to update Shipping Charges Refunded check box for Cash Sale with internalId {0}.\nDetails {1}", salesorderForRA.internalId, ex.Message);
                logger.error(message);
                error_flag = true;
            }
        }
        */

        /* private static int getCountOfSNA_IF(string sointernalID)
         {

             //soid = "1550";
             var transearch_adv = new TransactionSearchAdvanced();
             var transearch = new TransactionSearch();
             var basic = new TransactionSearchBasic();
             var searchfield = new SearchMultiSelectField();

             searchfield.@operator = SearchMultiSelectFieldOperator.anyOf;
             searchfield.operatorSpecified = true;
             searchfield.searchValue = new RecordRef[] { new RecordRef() { internalId = sointernalID, type = RecordType.salesOrder, typeSpecified = true } };

             basic.createdFrom = searchfield;
             transearch.basic = basic;
             transearch_adv.criteria = transearch;

             SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = false };

             //TransactionSearchAdvanced tra_search_adv = new TransactionSearchAdvanced() { savedSearchId = ConfigurationManager.AppSettings["SavedSearchID"] };
             var result = new SearchResult();

             try
             {
                 _client.search(US.GetSourcePassport(), null, null, sPref, transearch_adv, out result);

                 if (!result.status.isSuccess)
                 {
                     var error = string.Format("Error while getting pending orders \nDetail: {0}",
                                               result.status.statusDetail[0].message);
                     logger.error(error);
                     return 0;
                 }

                 int count = 0;
                 if (result.recordList != null && result.recordList.Count() > 0)
                 {
                     if (result.recordList.Count() > 0)
                     {
                         //get sales order
                         Int32 ifcnt = (from s in result.recordList
                                        where s.GetType() == typeof(ItemFulfillment)
                                        select s).Count();

                         return ifcnt;
                     }
                     else
                     {
                         logger.info(1, string.Format("Webservice did not find Sales order for order no.: {0}", sointernalID));
                         return 0;
                     }
                 }
                 else
                 {
                     logger.info(1, string.Format("Webservice returned null object for Sales order for order no.: {0}",
                                                sointernalID));
                     return 0;
                 }
             }
             catch (Exception ex)
             {
                 var error = string.Format("Exception while getting pending orders..\nDetail: {0}, \n {1} .", ex.Message,
                                           ex.StackTrace);
                 //email
                 logger.error(error);
                 return 0;
             }
             return 0;
         } */

        //bool IsShippingChargesRefunded = (bool)(from CustomFieldRef cref in cashSaleForRA.customFieldList
        //                                        where cref is BooleanCustomFieldRef && ((BooleanCustomFieldRef)cref).internalId == ConfigurationManager.AppSettings.Get("migratestatus")
        //                                        select ((BooleanCustomFieldRef)cref).value).FirstOrDefault();        

        #endregion



        public static Boolean GetInvoicefromDNa(string toNumber)
        {
            ///// logger.info(string.Format("Calling webservice..."));

            var to = new Invoice();
            var tranSearchAdv = new TransactionSearchAdvanced();

            //RecordRef recRef = new RecordRef(){ externalId = poNumber,typeSpecified=true, type= RecordType.purchaseOrder};
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchStringField();
            searchfield = new SearchStringField();
            searchfield.@operator = SearchStringFieldOperator.@is;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = toNumber;

            basic.tranId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = true, returnSearchColumns = true };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, tranSearch, out searchResult);
                //////logger.info(string.Format("Webservice call complete"));

                //if (!searchResult.status.isSuccess)
                //{
                //    if (searchResult.status.statusDetail[0].code == LibApi.NetSuitePortType.StatusDetailCodeType.SESSION_TIMED_OUT)
                //    {
                //        var error = string.Format("Webservice timed out.");
                //        logger.error(error);
                //    }
                //    else
                //    {
                //        var error =
                //            string.Format("Webservice call failed while retrieving purchase order No: {0}\nDetail:{1}",
                //                          toNumber, searchResult.status.statusDetail[0].message);

                //        logger.error(error);
                //        return null;
                //    }
                //}
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Exception occurred during webservice call while retrieving purchase order No: {0}\nDetails:{1}\n{2}",
                        toNumber, ex.Message, ex.StackTrace);
                /////logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var Tranorder = (Invoice)(from t in searchResult.recordList
                                              where t.GetType() == typeof(Invoice)
                                              select t).FirstOrDefault();

                    return true;
                }
                else
                {
                    //// logger.info(1, string.Format("Webservice did not find purchase order for order no.: {0}", toNumber));
                    return false;
                }
            }
            else
            {
                //// logger.info(1, string.Format("Webservice returned null object for purchase order for order no.: {0}",
                //// toNumber));
                return false;
            }
            //
        }

        public static string GetItemfulfillmentfromDNa(string toNumber)
        {
            ///// logger.info(string.Format("Calling webservice..."));

            var to = new ItemFulfillment();
            var tranSearchAdv = new TransactionSearchAdvanced();

            //RecordRef recRef = new RecordRef(){ externalId = poNumber,typeSpecified=true, type= RecordType.purchaseOrder};
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchStringField();
            searchfield = new SearchStringField();
            searchfield.@operator = SearchStringFieldOperator.@is;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = toNumber;

            basic.tranId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = true, returnSearchColumns = true };
            var searchResult = new SearchResult();
            try
            {
                _client.search(US.GetDestinationePassport(), null, null, sPref, tranSearch, out searchResult);
                //////logger.info(string.Format("Webservice call complete"));

                //if (!searchResult.status.isSuccess)
                //{
                //    if (searchResult.status.statusDetail[0].code == LibApi.NetSuitePortType.StatusDetailCodeType.SESSION_TIMED_OUT)
                //    {
                //        var error = string.Format("Webservice timed out.");
                //        logger.error(error);
                //    }
                //    else
                //    {
                //        var error =
                //            string.Format("Webservice call failed while retrieving purchase order No: {0}\nDetail:{1}",
                //                          toNumber, searchResult.status.statusDetail[0].message);

                //        logger.error(error);
                //        return null;
                //    }
                //}
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Exception occurred during webservice call while retrieving purchase order No: {0}\nDetails:{1}\n{2}",
                        toNumber, ex.Message, ex.StackTrace);
                /////logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var Tranorder = (ItemFulfillment)(from t in searchResult.recordList
                                                      where t.GetType() == typeof(ItemFulfillment)
                                                      select t).FirstOrDefault();


                    return Tranorder.internalId;
                }
                else
                {
                    //// logger.info(1, string.Format("Webservice did not find purchase order for order no.: {0}", toNumber));
                    return null;
                }
            }
            else
            {
                //// logger.info(1, string.Format("Webservice returned null object for purchase order for order no.: {0}",
                //// toNumber));
                return null;
            }
            //
        }

        public static Boolean DeleteIF(string x)
        {
            error_flag = false;
            try
            {
                // string[] irs = x.Split(',');
                // foreach (string y in irs)
                // {
                WriteResponse response = null;
                // if (y != "")
                // {
                RecordRef r = new RecordRef() { internalId = x, type = RecordType.itemFulfillment, typeSpecified = true };
                _client.delete(US.GetDestinationePassport(), null, null, null, r, out response);

                if (!response.status.isSuccess)
                {
                    logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));
                    logger.info(string.Format("Item Fulfillment is failed, unable to delete ."));
                    //var error = string.Format("Item Receipt is failed, unable to update Migration status for IF number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
                    logger.error(response.status.statusDetail[0].message);
                    error_flag = true;
                }
                else
                {
                    logger.info("Deleted Item receipt : " + x);
                    //UpdateMigrationStatus(r);

                    // for updation
                    //List<CustomFieldRef> customList = new List<CustomFieldRef>();

                    //BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("CustomMigrateStatus"), value = false };
                    //customList.Add(prepaidcostCheckRef);

                    //ItemFulfillment csToUpdate = new ItemFulfillment()
                    //{
                    //    internalId = x,
                    //    customFieldList = customList.ToArray()
                    //};
                    //_client.update(US.GetSourcePassport(), null, null, null, csToUpdate, out response);
                    //if (!response.status.isSuccess)
                    //{
                    //    logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                    //    //var error = string.Format("Item Receipt is failed, unable to update Migration status for IF number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
                    //    //logger.error(2, error);
                    //    logger.error(response.status.statusDetail[0].message);
                    //    error_flag = true;
                    //}
                    //else
                    //{
                    //    logger.info("Updated Item Fulfillment : " + x);
                    //    //UpdateMigrationStatusforIR(r);
                    //}
                    ////  }
                    // }
                }
            }
            catch (Exception ex)
            {
                error_flag = true;
                logger.info(string.Format("Item Fulfillment is failed, unable to delete .\nDetail :{0}", ex.Message));
            }
            return error_flag;
        }

        public static string GetItemfulfillmentfromSNA(string toNumber)
        {
            ///// logger.info(string.Format("Calling webservice..."));



            var to = new ItemFulfillment();
            var tranSearchAdv = new TransactionSearchAdvanced();

            //RecordRef recRef = new RecordRef(){ externalId = poNumber,typeSpecified=true, type= RecordType.purchaseOrder};
            var tranSearch = new TransactionSearch();
            var basic = new TransactionSearchBasic();
            var searchfield = new SearchStringField();
            searchfield = new SearchStringField();
            searchfield.@operator = SearchStringFieldOperator.@is;
            searchfield.operatorSpecified = true;
            searchfield.searchValue = toNumber;

            basic.tranId = searchfield;
            tranSearch.basic = basic;

            tranSearchAdv.criteria = tranSearch;
            SearchPreferences sPref = new SearchPreferences() { bodyFieldsOnly = true, returnSearchColumns = true };
            var searchResult = new SearchResult();
            try
            {

                _client.search(US.GetSourcePassport(), null, null, sPref, tranSearch, out searchResult);


                //////logger.info(string.Format("Webservice call complete"));

                //if (!searchResult.status.isSuccess)
                //{
                //    if (searchResult.status.statusDetail[0].code == LibApi.NetSuitePortType.StatusDetailCodeType.SESSION_TIMED_OUT)
                //    {
                //        var error = string.Format("Webservice timed out.");
                //        logger.error(error);
                //    }
                //    else
                //    {
                //        var error =
                //            string.Format("Webservice call failed while retrieving purchase order No: {0}\nDetail:{1}",
                //                          toNumber, searchResult.status.statusDetail[0].message);

                //        logger.error(error);
                //        return null;
                //    }
                //}
            }
            catch (Exception ex)
            {
                var error =
                    string.Format(
                        "Exception occurred during webservice call while retrieving purchase order No: {0}\nDetails:{1}\n{2}",
                        toNumber, ex.Message, ex.StackTrace);
                /////logger.error(error);
                throw ex;
            }

            if (searchResult.recordList != null && searchResult.recordList.Count() > 0)
            {
                if (searchResult.recordList.Count() > 0)
                {
                    //get sales order
                    var Tranorder = (ItemFulfillment)(from t in searchResult.recordList
                                                      where t.GetType() == typeof(ItemFulfillment)
                                                      select t).FirstOrDefault();

                    return Tranorder.internalId;
                }
                else
                {
                    //// logger.info(1, string.Format("Webservice did not find purchase order for order no.: {0}", toNumber));
                    return null;
                }
            }
            else
            {
                //// logger.info(1, string.Format("Webservice returned null object for purchase order for order no.: {0}",
                //// toNumber));
                return null;
            }
            //
        }

        public static void UpdateMigrationStatusUncheck(string internalidsna)
        {

            //var temp = tempobject.GetType().Name;
            Preferences pref = new Preferences() { ignoreReadOnlyFields = true, ignoreReadOnlyFieldsSpecified = true, warningAsError = false, warningAsErrorSpecified = true };


            #region Itemfulfillment


            //    try
            //    {
            //        ItemFulfillment cashSaleForRA = new ItemFulfillment();
            //        List<CustomFieldRef> customList = new List<CustomFieldRef>();

            //        BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("CustomMigrateStatus"), value = false };
            //        customList.Add(prepaidcostCheckRef);

            //        ItemFulfillment csToUpdate = new ItemFulfillment()
            //        {
            //            tranId = tranidtemp,
            //            customFieldList = customList.ToArray()
            //        };

            //        WriteResponse response = null;

            //        _client.update(US.GetSourcePassport(), null, null, pref, csToUpdate, out response);

            //        if (!response.status.isSuccess)
            //        {
            //            logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

            //            var error = string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
            //            logger.error(2, error);
            //        }
            //        else
            //        {
            //            logger.info("Migration status updated successfully for Item Fulfillment : " + cashSaleForRA.internalId);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        error_flag = true;
            //        logger.info(string.Format("Error at UpdateMigrationStatus()\nunable to update Migration status for IF.\nDetail :{0}", ex.Message));
            //    }

            #endregion



            List<CustomFieldRef> customList = new List<CustomFieldRef>();

            BooleanCustomFieldRef prepaidcostCheckRef = new BooleanCustomFieldRef() { internalId = ConfigurationManager.AppSettings.Get("CustomMigrateStatus"), value = false };
            customList.Add(prepaidcostCheckRef);

            ItemFulfillment csToUpdate = new ItemFulfillment()
            {
                internalId = internalidsna,
                customFieldList = customList.ToArray()
            };
            WriteResponse response = null;
            _client.update(US.GetSourcePassport(), null, null, null, csToUpdate, out response);
            if (!response.status.isSuccess)
            {
                logger.info(2, string.Format("Error code: {0}", response.status.statusDetail[0].code.ToString()));

                //var error = string.Format("Item Receipt is failed, unable to update Migration status for IF number : {1}.\nDetail :{0}", response.status.statusDetail[0].message, cashSaleForRA.internalId);
                //logger.error(2, error);
                logger.error(response.status.statusDetail[0].message);
                error_flag = true;
            }
            else
            {
                logger.info("Updated Item Fulfillment : " + internalidsna);
                //UpdateMigrationStatusforIR(r);
            }





        }

    }
}