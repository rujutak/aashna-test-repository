﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;

namespace LibApi
{
    public class XMLParser
    {
        public static XmlNameTable nameTable { get; set; }
        public static XElement GetXMLFileAsXElementBasedOnType(string filePathWithFileName, TypeOfXML typeOfXML)
        {
            if (string.IsNullOrEmpty(filePathWithFileName))
                throw new ArgumentNullException("filePath");
            if (!File.Exists(filePathWithFileName))
                throw new FileNotFoundException(string.Format("File not present at {0}", filePathWithFileName));

            if (typeOfXML == TypeOfXML.FullPriCat)
            {
                StreamReader reader = new StreamReader(filePathWithFileName, Encoding.Default);
                try
                {
                   

                    var s = reader.ReadToEnd();
                    if (s.StartsWith("?"))
                    {
                        var d = s.Remove(0, 1);
                        var xFile = XElement.Parse(d);
                        reader.Close();
                        return xFile;
                    }

                    var xFileElement = XElement.Parse(s);
                    reader.Close();
                    return xFileElement;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    reader.Close();
                }
                
                
            }

            var xmlReader = XmlTextReader.Create(filePathWithFileName);
            try
            {
                var xFileElement = XElement.Load(xmlReader);//.Parse(xmlReader.ReadToEnd());

                nameTable = xmlReader.NameTable;

                switch (typeOfXML)
                {
                    case TypeOfXML.PriCat:
                        if (xFileElement.Name.LocalName != PriCatXPath.ROOT)
                        {
                            xmlReader.Close();
                            throw new FileLoadException("File can not be loaded of type Pricat");
                        }

                        break;
                    case TypeOfXML.StylePre:
                        //if (xFileElement.Document.Root.Name != StylePreXPath.ROOT)
                        //    throw new FileLoadException("File can not be loaded of type Pricat");
                        break;
                    case TypeOfXML.DesAdvise:
                        //if (xFileElement.Document.Root.Name != DesAdviseXPath.ROOT)
                        //    throw new FileLoadException("File can not be loaded of type Pricat");
                        break;
                    default:
                        break;
                }

                return xFileElement;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                xmlReader.Close();
            }

        
        }

        /// <summary>
        ///     utility method to read xmlnode value from xDoc directly
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="xPathExpression"></param>
        /// <returns></returns>
        public static string GetString(XElement xElement, string xPathExpression)
        {
            var xSingleElement = xElement.XPathSelectElement(xPathExpression);
            return xSingleElement == null ? string.Empty : xSingleElement.Value;
        }

        public static List<XElement> GetPropertyNamesFromEmbeddedResource(string p, string embeddedResourceFileName)
        {
            var xElement = GetXElementForPropertyNames(p, embeddedResourceFileName);
            return xElement;
        }

        public static List<XElement> GetXElementForPropertyNames(string p, string embeddedResourceFileName)
        {
            //Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(embeddedResourceFileName);
            var path = Assembly.GetExecutingAssembly().Location;
            var directory = Path.GetDirectoryName(path);
            if (!directory.EndsWith(@"\"))
                directory += "\\";
            var fileName = directory + embeddedResourceFileName;
            var xmlReader = new StreamReader(File.OpenRead(fileName));
            
            var xFileElement = XElement.Parse(xmlReader.ReadToEnd());
            xmlReader.Close();

            
            return xFileElement.XPathSelectElements(p + "/Property").ToList();
        }
    }
}