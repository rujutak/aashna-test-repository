using System;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Diagnostics;
using System.Text;

namespace LibApi
{
    public class LogEntry
    {
        private string caseNumber;

        public string CaseNumber
        {
            get { return caseNumber; }
        }
        private string bookingNumber;

        public string BookingNumber
        {
            get { return bookingNumber; }
        }
        private string caller;

        public LogEntry(string caller, string caseNumber, string bookingNumber)
        {
            this.caller = caller;
            this.caseNumber = caseNumber;
            this.bookingNumber = bookingNumber;
        }

        public override string ToString()
        {
            return "[" + caller + "][CN:" + caseNumber + "][BN:" + bookingNumber + "]";
        }
    }

    public class Logger
    {
        static private String LEVEL_INFO = "info";
        static private String LEVEL_DEBUG = "debug";
        private String folderPath = "Netsuite_Logs";

        public enum LogMode { TEXT, XML, ALL };
        private String _level;
        private String _class;
        private LogMode _mode;

        //DateTimeFormatInfo format = new DateTimeFormatInfo();
        //DateTimeFormatInfo fileFormat = new DateTimeFormatInfo();

        /// <summary>
        /// Gets or sets LogFile
        /// </summary>
        public string LogFile { get; private set; }
        public Logger(String className, String level)
        {
            Init(className, level, LogMode.TEXT);
        }
        public Logger(String className, LogMode logMode)
        {
            Init(className, string.Empty, logMode);
        }
        private void Init(string className, string level, LogMode logMode)
        {
            switch (logMode)
            {
                case LogMode.TEXT:
                    _level = level;
                    _class = className;
                    _mode = LogMode.TEXT;
                    break;
                case LogMode.XML:
                    _class = className;
                    _mode = LogMode.XML;
                    break;
                case LogMode.ALL:
                    _class = className;
                    _mode = LogMode.ALL;
                    _level = level;
                    break;
                default:
                    break;
            }
            if (ConfigurationManager.AppSettings.Get("LogFileFolder") != null)
            {
                folderPath = ConfigurationManager.AppSettings.Get("LogFileFolder");
                if (!System.IO.Directory.Exists(folderPath)) System.IO.Directory.CreateDirectory(folderPath);
            }
        }

        private string GetLogFileName(LogMode mode)
        {
            switch (mode)
            {
                case LogMode.TEXT:
                    return folderPath + "\\LogFile-" + _class + "-" + DateTime.Now.ToString("ddMMyy") + ".csv";

                case LogMode.XML:
                    return folderPath + "\\LogFile-" + _class + "-" + DateTime.Now.ToString("ddMMyy") + "-xml.log";

                default:
                    throw new InvalidOperationException();
            }

        }
        private string GetLogFileName()
        {
            string file = folderPath + "\\LogFile-" + _class + "-" + DateTime.Now.ToString("ddMMyy") + ".log";
            LogFile = file;
            return file;
        }

        private string GetDateTimeStamp()
        {
            return DateTime.Now.ToString("dd-MM-yy HH:mm:ss");
        }

        private void log(String level, String msg)
        {
            try
            {
                System.Console.Out.WriteLine("[" + _class + "]" + msg);
                FileStream fstr = new FileStream(GetLogFileName(LogMode.TEXT), FileMode.Append | FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(fstr);
                string dttime = GetDateTimeStamp();
                sw.WriteLine("[" + dttime + "][" + level + "] " + msg);
                sw.Close();
                fstr.Close();
                Debug.WriteLine(msg);
            }
            catch (Exception ex)
            {
                try
                {
                    EventLog.WriteEntry("Application", "Logger - log() - " + ex.Message + ex.StackTrace);
                }
                catch (Exception)
                {
                }
            }
        }
        private void log(String msg, LogMode mode)
        {
            try
            {
                System.Console.Out.WriteLine("[" + _class + "]" + msg);
                FileStream fstr = new FileStream(GetLogFileName(mode), FileMode.Append | FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(fstr);
                string dttime = GetDateTimeStamp();
                sw.WriteLine(msg);
                sw.Close();
                fstr.Close();
                Debug.WriteLine(msg);
            }
            catch (Exception ex)
            {
                try
                {
                    EventLog.WriteEntry("Application", "Logger - log() - " + ex.Message + ex.StackTrace);
                }
                catch (Exception)
                {
                }
            }
        }

        private void log(LogEntry entry, String level, String msg)
        {
            log(level, entry.ToString() + "-" + msg);
        }

        /// <summary>
        /// Logs if level is LEVEL_INFO
        /// </summary>
        public void info(String msg)
        {
            if (_level == LEVEL_INFO || _level == LEVEL_DEBUG)
            {
                log("INFO", msg);
            }
        }

        public void info(LogEntry entry, String msg)
        {
            log(entry, "INFO", msg);
        }

        /// <summary>
        /// log message with indentation
        /// </summary>
        /// <param name="indent">1 number of tabs before message</param>
        /// <param name="msg"></param>
        public void info(int indent, String msg)
        {
            for (int i = 0; i < indent; i++)
            {
                msg = "\t" + msg;
            }
            log("INFO", msg);
        }

        /// <summary>
        /// Logs if level is LEVEL_DEBUG
        /// </summary>
        public void debug(String msg)
        {
            log("DEBUG", msg);
        }

        public void debug(LogEntry entry, String msg)
        {
            log(entry, "DEBUG", msg);
        }

        /// <summary>
        /// warning message with indentation
        /// </summary>
        /// <param name="indent">1 number of tabs before message</param>
        /// <param name="msg"></param>
        public void warning(int indent, String msg)
        {
            for (int i = 0; i < indent; i++)
            {
                msg = "\t" + msg;
            }
            log("WARNING", msg);
        }

        /// <summary>
        /// Logs warnings
        /// </summary>
        public void warning(String msg)
        {
            log("WARNING", msg);
        }

        public void warning(LogEntry entry, String msg)
        {
            log(entry, "WARNING", msg);
        }
        /// <summary>
        /// Logs an error message
        /// </summary>
        public void error(String msg)
        {
            log("ERROR", msg);
        }
        /// <summary>
        /// log error with indentation
        /// </summary>
        /// <param name="indent">1 number of tabs before message</param>
        /// <param name="msg"></param>
        public void error(int indent, String msg)
        {
            for (int i = 0; i < indent; i++)
            {
                msg = "\t" + msg;
            }
            log("ERROR", msg);
        }

        public void error(LogEntry entry, String msg)
        {
            log(entry, "ERROR", msg);
        }

        /// <summary>
        /// Logs an error message with a new line
        /// </summary>
        public void error(String msg, bool isNewLine)
        {
            if (isNewLine)
                log("ERROR", msg);
            else
                log("ERROR", msg);
        }

        public void SeparatorNormal()
        {
            log("", "====================================================================================================");
        }

        public void SeparatorMinor()
        {
            log("", "----------------------------------------------------------------------------------------------------");
        }

        public void Separator(char @char, int length)
        {
            string separator = "";
            for (int i = 0; i < length; i++)
            {
                separator += @char;
            }
            log("", separator);
        }
        //public void error(LogEntry entry, String msg)
        //{
        //    log(entry, "ERROR", msg);
        //}

        //public void error( String msg, bool isNewLine )
        //{
        //    if (isNewLine)
        //        log("ERROR",msg);
        //    else
        //        log("ERROR",msg);
        //}
        public void LogXML(XMLLogEntry xmlLogEntry)
        {

            log(xmlLogEntry.ToXMLString(), LogMode.XML);

        }

        public void FinalizeXMLLogFile()
        {
            if (_mode == LogMode.ALL || _mode == LogMode.XML)
            {
                try
                {
                    var logFileName = GetLogFileName(LogMode.XML);
                    var s = File.ReadAllText(logFileName, Encoding.UTF8);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(@"<?xml version=""1.0"" ?>");
                    sb.AppendLine("<Log>");
                    sb.AppendLine(s);
                    sb.AppendLine("</Log>");



                    File.WriteAllText(Path.GetFullPath(logFileName) + ".xml", sb.ToString(), Encoding.UTF8);

                    //if (File.Exists(logFileName))
                    //    File.Delete(logFileName);
                }
                catch (Exception ex)
                {
                    try
                    {
                        EventLog.WriteEntry("Application", "Logger - FinalizeXMLLogFile() - " + ex.Message + ex.StackTrace);
                    }
                    catch (Exception)
                    {


                    }

                }

            }
        }
    }
}
