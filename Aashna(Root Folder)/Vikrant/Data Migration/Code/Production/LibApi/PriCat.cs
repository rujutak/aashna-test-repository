﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LibApi
{
    public enum PriCatItemType {Parent,Child};
    public interface IXPathExpression
    {
         string  GetXPathExpression();
        
    }
    /// <summary>
    /// 
    /// </summary>
    public class PriCat:IXPathExpression
    {
        public string ID { get; set; }

        public string IssueDate { get; set; }

        public string FunctionCode { get; set; }

        public string RevisionDate { get; set; }

        public string DocumentCurrencyCode { get; set; }

        public List<Party> Parties { get; set; }

        public TaxCategory TaxCategory { get; set; }

        public PriCatItem Item { get; set; }

        public List<CustomImportProperty> CustomImportProperties
        {
            get;
            set;
        }

        #region IXPathExpression Members

        public  string GetXPathExpression()
        {
            return "PriceCatalog";
        }

        #endregion
    }
    public class Party : IXPathExpression
    {
        public const string PARTY_TYPE = "PartyType";
        public const string PARTY_IDENTIFICATION_ID = "PartyIdentification/ID";
        public const string PARTY_NAME = "PartyName/Name";

        public string PartyType { get; set; }

        public string PartyIdentification { get; set; }

        public string PartyName { get; set; }

        public Address Address { get; set; }

        public PartyTaxScheme PartyTaxScheme { get; set; }



        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "Party";
        }

        #endregion
    }

    public class Address : IXPathExpression
    {
        public const string STREET_NAME = "StreetName";
        public const string CITY_NAME = "CityName";
        public const string POSTAL_ZONE = "PostalZone";

        public string StreetName { get; set; }

        public string CityName { get; set; }

        public string PostalZone { get; set; }

        public Country Country { get; set; }
        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "Address";
        }

        #endregion
    }

    public class Country :IXPathExpression
    {
        public string IdentificationCode { get; set; }

        public string Name { get; set; }

        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "Country";
        }

        #endregion
    }

    public class PartyTaxScheme :IXPathExpression
    {
        public string RegistrationName { get; set; }
        public string CompanyId { get; set; }

        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "PartyTaxScheme";
        }

        #endregion
    }

    public class PriCatItem : IXPathExpression
    {
        public string  ActionCode { get; set; }
        public TaxCategory  TaxCat{get;set;}
        public string BrandName { get; set; }
        public string SuppliersItemId {get;set;}
        public string Description { get; set; }
        public PriCatItemType ItemType {get;set;}
        public string EAN { get; set; }

        public ItemValidityPeriod ValidityPeriod {get;set;}

        public List<ItemPrice> ItemPrice {get;set;}

        public string OriginCountry {get;set;}

        public List<AdditionalItemProperty> AdditionalProperties
        {
            get;
            set;
        }

        public List<PhysicalFieldProperty> PhysicalFieldProperties
        {
            get;
            set;
        }
        public List<MeasurementProperty> MeasurementProperties
        {
            get;
            set;
        }
        
        public List<Variant> Variants { get; set; }
        

        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "PriceCatalogLine/Item";
        }

        #endregion
    }
    public class ItemValidityPeriod :IXPathExpression
    {
    
         public string StartDate { get; set; }
        public string EndDate { get; set; }



        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "ValidityPeriod";
        }

        #endregion
    }

    public class TaxCategory : IXPathExpression
    {
        public string Name { get; set; }
        public string Percent {get;set;}


        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "TaxCategory";
        }

        #endregion
    }
    public class ItemSuppliersItemIdentification : IXPathExpression
    {

        public string ID { get; set; }


        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "SuppliersItemIdentification";
        }

        #endregion
    }

    public class ItemPrice : IXPathExpression
    {

        public string PriceType { get; set; }

        public string ValidityStartDate { get; set; }

        public PriceAmount PriceAmount;





        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "ItemPrice";
        }

        #endregion
    }

    public class PriceAmount :IXPathExpression
    {
    
        public string CurrencyIDAttribute {get;set;}

        public string Value {get;set;}

        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "PriceAmount";
        }

        #endregion
    }

    public class OriginCountry : IXPathExpression
    {
   
        public string Name {get;set;}

        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "OriginCountry";
        }

        #endregion
    }

    public class AdditionalItemProperty : ItemProperty
    {
    
       
    }

    public class PhysicalFieldProperty : ItemProperty
    {
    
     
    }
    public class MeasurementProperty : ItemProperty
    {

    }
    public class CustomImportProperty : ItemProperty
    {
        public string TypeOfField { get; set; }
    }

    public class ItemProperty
    {

        public string Name { get; set; }


        public string Value { get; set; }


        public string CustomFieldId { get; set; }
    }
    public class VariantListEntry
    {

        public string InternalId { get; set; }


        public string TypeId { get; set; }


        public string Name { get; set; }
    }

    public class Variant
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public List<PhysicalFieldProperty> PhysicalFieldProperties
        {
            get;
            set;
        }
    }
}


