﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AlexPilotti.FTPS.Common;
using LibApi;

namespace ExecutedOrderProcessorConsole
{
    public abstract class FileBase
    {
        protected string fileName;
        protected char delimiter;
        protected bool hasHeader = true;
        protected bool hasFooter = true;


        public FileBase()
        {
        }

        public FileBase(string fileName, char delimiter)
        {
            this.fileName = fileName;
            this.delimiter = delimiter;
        }
        public FileBase(string fileName, char delimiter, bool hasHeader, bool hasFooter)
        {
            this.fileName = fileName;
            this.delimiter = delimiter;
            this.HasFooter = hasFooter;
            this.HasHeader = hasHeader;
        }


        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        public char Delimiter
        {
            get { return this.delimiter; }
            set { this.delimiter = value; }
        }

        public bool HasHeader
        {
            get { return this.hasHeader; }
            set { this.hasHeader = value; }
        }

        public bool HasFooter
        {
            get { return this.hasFooter; }
            set { this.hasFooter = value; }
        }
    }

    public abstract class FileReader : FileBase
    {
        private Logger logger = new Logger("FileReader", "info");
        protected StreamReader reader = null;
        private bool hasMoreRecords = false;

        public FileReader() : base() { }
        public FileReader(string fileName, char delimiter)
            : base(fileName, delimiter)
        {
            try
            {
                //reader = File.OpenText(fileName);
                reader = new StreamReader(fileName, Encoding.Default);
                if (hasHeader)
                {
                    string line = reader.ReadLine();
                    ParseHeader(line.Split(new Char[] { delimiter }));
                }
            }
            catch (Exception ex)
            {
                logger.error("File not found " + ex.Message);
                logger.error("Error trace " + ex.StackTrace);
                throw ex;
            }
        }

        public FileReader(string fileName, char delimiter, bool hasHeader, bool hasFooter)
            : base(fileName, delimiter, hasHeader, hasFooter)
        {
            try
            {
                // reader = File.OpenText(fileName);
                reader = new StreamReader(fileName, Encoding.Default);
                if (hasHeader)
                {
                    //string line = reader.ReadLine();
                    //ParseHeader(line.Split(new Char[] { delimiter }));
                }
            }
            catch (Exception ex)
            {
                logger.error("File not found " + ex.Message);
                logger.error("Error trace " + ex.StackTrace);
                throw ex;
            }
        }

        public bool HasMoreRecords
        {
            get { return hasMoreRecords; }
            set { this.hasMoreRecords = value; }
        }

        public List<string[]> ParseRecords()
        {
            List<string[]> fieldsPerRow = new List<string[]>();
            try
            {
                fieldsPerRow = GetRows(reader, delimiter);
                return fieldsPerRow;

            }
            catch (System.Exception e)
            {
                logger.error("Failed to read file " + fileName);
                logger.error("stack is " + e.StackTrace);
            }
            return fieldsPerRow;
        }

        public List<string[]> ParseMoreRecords()
        {
            List<string[]> fieldsPerRow = GetRows(reader, delimiter);
            return fieldsPerRow;
        }

        private List<string[]> GetRows(StreamReader reader, char delimiter)
        {
            List<string[]> fieldsPerRow = new List<string[]>();
            try
            {
                StringBuilder footer = new StringBuilder();
                hasMoreRecords = false;
                //aug 30 int count = 1;
                string line = reader.ReadLine();
                while (line != null)
                {
                    if (hasFooter) footer.Remove(0, footer.Length).Append(line);
                    fieldsPerRow.Add(line.Split(new char[] { delimiter }));
                    //if (count++ == 200)
                    //{
                    //    hasMoreRecords = true;
                    //    break;
                    //}
                    line = reader.ReadLine();
                }
                if (footer.Length > 0)
                {
                    ParseFooter(footer.ToString().Split(Delimiter));
                }
            }
            catch (System.Exception e)
            {
                logger.error("Message " + e.StackTrace);
            }
            return fieldsPerRow;
        }

        public void close()
        {
            if (reader != null) reader.Close();
        }

        public abstract void ParseHeader(string[] headerFields);
        public abstract void ParseFooter(string[] footerFields);
        public abstract void ProcessRecord(string[] recordFields);
    }

    public class GenericFileReader : FileReader
    {
        //private Logger logger = new Logger("GenericFileReader", "info");
        public GenericFileReader()
            : base(null, ',')
        {
        }

        public GenericFileReader(string fileName)
            : base(fileName, ',')
        {
        }

        public GenericFileReader(string fileName, char delimiter)
            : base(fileName, delimiter)
        {
        }
        public GenericFileReader(string fileName, char delimiter, bool hasHeader, bool hasFooter)
            : base(fileName, delimiter, hasHeader, hasHeader)
        {
        }
        public override void ParseHeader(string[] headerFields)
        {
            //logger.info("Header is " + headerFields.Length);
        }

        public override void ParseFooter(string[] footerFields)
        {
            //logger.info("Footer is " + footerFields.Length);
        }
        public override void ProcessRecord(string[] recordFields)
        {
            for (int i = 0; i < recordFields.Length; i++)
            {
                //logger.info(recordFields[i] + '\t');
            }
        }
    }
}
