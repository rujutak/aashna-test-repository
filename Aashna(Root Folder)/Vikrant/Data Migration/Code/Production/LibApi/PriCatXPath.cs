﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Configuration;


namespace LibApi
{
    public class PriCatXPath
    {
        public const string ROOT = "PriceCatalog";
        public const string ID = "ID";
        public const string ISSUE_DATE = "IssueDate";
        public const string FUNCTION_CODE = "FunctionCode";
        public const string REVISION_DATE = "RevisionDate";
        public const string DOCUMENT_CURRENCY_CODE = "DocumentCurrencyCode";


        public static PriCat GetPriCatInput(XElement xElement)
        {
            if (xElement == null)
                throw new ArgumentNullException("xElement");
            var preCat = new PriCat
                             {
                                 ID = XMLParser.GetString(xElement, PriCatXPath.ID),
                                 IssueDate = XMLParser.GetString(xElement, PriCatXPath.ISSUE_DATE),
                                 FunctionCode = XMLParser.GetString(xElement, PriCatXPath.FUNCTION_CODE),
                                 RevisionDate = XMLParser.GetString(xElement, PriCatXPath.REVISION_DATE),
                                 DocumentCurrencyCode =
                                     XMLParser.GetString(xElement, PriCatXPath.DOCUMENT_CURRENCY_CODE),
                                 Parties = GetParties(xElement),
                                 Item = GetPriCatItem(xElement),
                                 CustomImportProperties = new List<CustomImportProperty>()
                             };

            SetCustomPropertis(preCat);
            return preCat;
        }

        private static void SetCustomPropertis(PriCat preCat)
        {
            GetCustomImportProperties(preCat);
        }

        private static PriCatItem GetPriCatItem(XElement xElement)
        {
            var pItem = new PriCatItem();
            var pItems = new List<PriCatItem>();
            var pItemNodes = xElement.XPathSelectElements(pItem.GetXPathExpression());
            foreach (var item in pItemNodes)
            {
                pItem = new PriCatItem
                            {
                                BrandName = XMLParser.GetString(item, "BrandName"),
                                SuppliersItemId = XMLParser.GetString(item, "SuppliersItemIdentification/ID"),
                                OriginCountry = XMLParser.GetString(item, "OriginCountry"),
                                ItemPrice = GetItemPrices(item),
                                //ValidityPeriod = GetValidityPeriod(item),
                    
                                TaxCat = GetTaxCategory(item)
                            };

                GetParentItemDescriptionAndEAN(item, pItem);

                pItem.AdditionalProperties = GetAdditionalProperties(item);
                pItem.MeasurementProperties = GetMeasurementProperties(item);
                pItem.PhysicalFieldProperties = GetParentPhysicalFieldProperties(item);
                pItem.Variants = GetVariants(item);


                pItems.Add(pItem);
            }
            if (pItems.Count > 0)
                return pItems[0];
            return pItem;
        }

        private static void GetCustomImportProperties(PriCat preCat)
        {
            //AddCustomItemProperty("DocumentCurrencyCode", preCat.DocumentCurrencyCode, preCat);
            AddCustomItemProperty("EAN", preCat.Item.EAN, preCat);
            AddCustomItemProperty("ItemTaxPercent", preCat.Item.TaxCat.Percent, preCat);
            AddCustomItemProperty("OrderNumber", preCat.ID, preCat);

            AddCustomItemProperty("DateOfFile", preCat.IssueDate, preCat);
            AddCustomItemProperty("Brand", preCat.Item.BrandName, preCat);
            AddCustomItemProperty("Gender", ConfigurationManager.AppSettings["Gender"], preCat);
            AddCustomItemProperty("BrandName", preCat.Item.Description, preCat);
            //var aProp = new CustomImportProperty();
            //        aProp.Name = "DocumentCurrencyCode";
            //        aProp.Value = XMLParser.GetString(p, "Value");
            //        aProp.CustomFieldId = XMLParser.GetString(item, "CustomFieldId");
            //        aProps.Add(aProp);
            //foreach (var item in customNames)
            //{
            //    var cust = new CustomImportProperty{  CustomFieldId = XMLParser.GetString(item,"CustomFieldId"), n}
            //}
        }

        private static void AddCustomItemProperty(string p, string value, PriCat preCat)
        {
            var aPropNames = XMLParser.GetPropertyNamesFromEmbeddedResource("CustomPropertyNames", "PriCatElements.xml");
            var v = aPropNames.Where(c => XMLParser.GetString(c, "Name").Trim() == p).FirstOrDefault();
            if (v != null)
            {
                var aProp = new CustomImportProperty
                                {
                                    Name = p,
                                    Value = value,
                                    CustomFieldId = XMLParser.GetString(v, "CustomFieldId"),
                                    TypeOfField = XMLParser.GetString(v, "Type")
                                };
                preCat.CustomImportProperties.Add(aProp);
            }
        }

        private static TaxCategory GetTaxCategory(XElement xElement)
        {
            var taxCat = new TaxCategory();
            var iP = xElement.Parent.XPathSelectElement(taxCat.GetXPathExpression());
            taxCat.Name = XMLParser.GetString(iP, "Name");
            taxCat.Percent = XMLParser.GetString(iP, "Percent");
            return taxCat;
        }

        private static List<PhysicalFieldProperty> GetParentPhysicalFieldProperties(XElement xElement)
        {
            var aPropNames = XMLParser.GetPropertyNamesFromEmbeddedResource("PhysicalPropertyNames",
                                                                            "PriCatElements.xml");
            var itemAddProps = xElement.XPathSelectElements("StandardItemIdentification/MeasurementDimension");
            List<XElement> parentProps = new List<XElement>();
            if (itemAddProps.ToList().Count > 0)
            {
                if (itemAddProps.ToList()[0].Parent != null)
                    parentProps = itemAddProps.ToList()[0].Parent.XPathSelectElements("PhysicalAttribute").ToList();
            }
            var aProps = new List<PhysicalFieldProperty>();
            foreach (var item in aPropNames)
            {
                var p =
                    parentProps.Where(a => XMLParser.GetString(a, "Name").Trim() == XMLParser.GetString(item, "Name")).
                        FirstOrDefault();
                if (p != null)
                {
                    var aProp = new PhysicalFieldProperty
                                    {
                                        Name = XMLParser.GetString(item, "Name"),
                                        Value = XMLParser.GetString(p, "Value"),
                                        CustomFieldId = XMLParser.GetString(item, "CustomFieldId")
                                    };
                    aProps.Add(aProp);
                }
            }
            return aProps;
        }

        private static void GetParentItemDescriptionAndEAN(XElement xElement, PriCatItem preCat)
        {
            var variants = xElement.XPathSelectElements("StandardItemIdentification");
            var vars = new List<Variant>();
            if (variants != null && variants.Count() > 0)
            {
                var parentItem = variants.First();

                preCat.Description = parentItem.XPathSelectElement("Description").Value;
                preCat.EAN = parentItem.XPathSelectElement("ID").Value;
            }
            //foreach (var item in variants)
            //{
            //    if (item.XPathSelectElements("MeasurementDimension").Count() > 0)
            //    {

            //        preCat.Description = item.XPathSelectElement("Description").Value;
            //        preCat.EAN = item.XPathSelectElement("ID").Value;
            //    }
            //}
        }

        private static List<Variant> GetVariants(XElement xElement)
        {
            var variants = xElement.XPathSelectElements("StandardItemIdentification").ToList();
            var vars = new List<Variant>();
            for (int i = 0; i < variants.Count; i++)
            {
                if (i == 0)
                    continue;
                var item = variants[i];
                var vary = new Variant
                               {
                                   Id = XMLParser.GetString(item, "ID"),
                                   Description = XMLParser.GetString(item, "Description"),
                                   PhysicalFieldProperties = GetPhysicalFieldProperties(item)
                               };
                vars.Add(vary);
            }

            return vars;
        }

        private static List<PhysicalFieldProperty> GetPhysicalFieldProperties(XElement xElement)
        {
            var physicalNames = XMLParser.GetPropertyNamesFromEmbeddedResource("PhysicalPropertyNames",
                                                                               "PriCatElements.xml");

            var itemAddProps = xElement.XPathSelectElements("PhysicalAttribute");
            var aProps = new List<PhysicalFieldProperty>();
            foreach (var item in physicalNames)
            {
                var p =
                    itemAddProps.Where(a => XMLParser.GetString(a, "Name").Trim() == XMLParser.GetString(item, "Name")).
                        FirstOrDefault();
                if (p != null)
                {
                    var aProp = new PhysicalFieldProperty
                                    {
                                        Name = XMLParser.GetString(item, "Name"),
                                        Value = XMLParser.GetString(p, "Value"),
                                        CustomFieldId = XMLParser.GetString(item, "CustomFieldId")
                                    };
                    aProps.Add(aProp);
                }
            }
            return aProps;
        }


        private static List<MeasurementProperty> GetMeasurementProperties(XElement xElement)
        {
            var aPropNames = XMLParser.GetPropertyNamesFromEmbeddedResource("MeasurementPropertyNames",
                                                                            "PriCatElements.xml");
            var itemAddProps = xElement.XPathSelectElements("StandardItemIdentification/MeasurementDimension");
            var aProps = new List<MeasurementProperty>();
            foreach (var item in aPropNames)
            {
                var p =
                    itemAddProps.Where(a => XMLParser.GetString(a, "AttributeID") == XMLParser.GetString(item, "Name")).
                        FirstOrDefault();
                if (p != null)
                {
                    var aProp = new MeasurementProperty
                                    {
                                        Name = XMLParser.GetString(item, "Name"),
                                        Value = XMLParser.GetString(p, "Measure"),
                                        CustomFieldId = XMLParser.GetString(item, "CustomFieldId")
                                    };
                    aProps.Add(aProp);
                }
            }
            return aProps;
        }

        private static List<AdditionalItemProperty> GetAdditionalProperties(XElement xElement)
        {
            var aPropNames = XMLParser.GetPropertyNamesFromEmbeddedResource("AdditionalPropertyNames",
                                                                            "PriCatElements.xml");
            var itemAddProps = xElement.XPathSelectElements("AdditionalItemProperty");
            var aProps = new List<AdditionalItemProperty>();
            foreach (var item in aPropNames)
            {
                var p =
                    itemAddProps.Where(a => XMLParser.GetString(a, "Name").Trim() == XMLParser.GetString(item, "Name")).
                        FirstOrDefault();
                if (p != null)
                {
                    var aProp = new AdditionalItemProperty();
                    aProp.Name = XMLParser.GetString(item, "Name");
                    aProp.Value = XMLParser.GetString(p, "Value");
                    aProp.CustomFieldId = XMLParser.GetString(item, "CustomFieldId");
                    aProps.Add(aProp);
                }
            }
            return aProps;
        }


        private static List<ItemPrice> GetItemPrices(XElement xElement)
        {
            var iPrice = new ItemPrice();
            var iP = xElement.XPathSelectElements(iPrice.GetXPathExpression());
            var iPrices = new List<ItemPrice>();
            foreach (var item in iP)
            {
                iPrice = new ItemPrice
                             {
                                 PriceType = XMLParser.GetString(item, "PriceType"),
                                 ValidityStartDate = XMLParser.GetString(item, "ValidityStartDate"),
                                 PriceAmount = GetPriceAmount(item)
                             };
                iPrices.Add(iPrice);
            }
            return iPrices;
        }

        private static PriceAmount GetPriceAmount(XElement item)
        {
            var pAmount = new PriceAmount();
            var pA = item.XPathSelectElement(pAmount.GetXPathExpression());
            if (pA == null)
                return null;
            pAmount.CurrencyIDAttribute =
                pA.Attributes().Where(a => a.Name == "currencyID").Select(a => a.Value).FirstOrDefault();
            pAmount.Value = XMLParser.GetString(item, "PriceAmount");
            return pAmount;
        }

        private static List<Party> GetParties(XElement xElement)
        {
            var party = new Party();
            var parties = new List<Party>();
            var partyNodes = xElement.XPathSelectElements(party.GetXPathExpression());
            foreach (var item in partyNodes)
            {
                var check =
                    parties.Where(p => p.PartyIdentification == XMLParser.GetString(item, Party.PARTY_IDENTIFICATION_ID))
                        .FirstOrDefault();
                if (check != null) continue;
                party = new Party
                            {
                                PartyType = XMLParser.GetString(item, Party.PARTY_TYPE),
                                PartyIdentification = XMLParser.GetString(item, Party.PARTY_IDENTIFICATION_ID),
                                PartyName = XMLParser.GetString(item, Party.PARTY_NAME),
                                Address = GetAddress(item),
                                PartyTaxScheme = GetPartyTaxScheme(item)
                            };
                parties.Add(party);
            }
            return parties;
        }

        private static PartyTaxScheme GetPartyTaxScheme(XElement item)
        {
            var pTaxEle = new PartyTaxScheme();
            var pTax = item.XPathSelectElement(pTaxEle.GetXPathExpression());
            if (pTax == null)
                return null;
            return pTaxEle = new PartyTaxScheme
                                 {
                                     CompanyId = XMLParser.GetString(pTax, "CompanyID"),
                                     RegistrationName = XMLParser.GetString(pTax, "RegistrationName")
                                 };
        }

        private static Address GetAddress(XElement item)
        {
            var address = new Address();
            var add = item.XPathSelectElement(address.GetXPathExpression());
            if (add == null)
                return null;
            address.StreetName = XMLParser.GetString(add, Address.STREET_NAME);
            address.CityName = XMLParser.GetString(add, Address.CITY_NAME);
            address.PostalZone = XMLParser.GetString(add, Address.POSTAL_ZONE);
            var country = add.XPathSelectElement(new Country().GetXPathExpression());
            if (country != null)
            {
                address.Country = new Country
                                      {
                                          IdentificationCode = XMLParser.GetString(country, "IdentificationCode"),
                                          Name = XMLParser.GetString(country, "Name")
                                      };
            }
            return address;
        }
    }
}