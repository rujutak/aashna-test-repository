﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Reflection;
using System.Xml;
using System.Diagnostics;
using System.IO;

namespace LibApi
{
    /// <summary>
    /// Provides a description for an enumerated type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field,
     AllowMultiple = false)]
    public sealed class EnumDescriptionAttribute : Attribute
    {
        private string description;

        /// <summary>
        /// Gets the description stored in this attribute.
        /// </summary>
        /// <value>The description stored in the attribute.</value>
        public string Description
        {
            get
            {
                return this.description;
            }
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EnumDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="description">The description to store in this attribute.
        /// </param>
        public EnumDescriptionAttribute(string description)
            : base()
        {
            this.description = description;
        }
    }
    public enum LogCategory {ANY, INFO, FAILURE, SUCCESS, RETRY, SUMMARY, NETSUITE, FTP,SERIALIZATION, VALIDATION, FILESUCCESS, FILEFAILURE, FILERETRY  };
    public enum LogLevel { ANY, INFO, DEBUG, ERROR, WARNING, VERBOSE };
    public enum   LoadModuleName { NotSet = 0, FULLPRICAT = 1, PRICAT = 2};
    public enum Operator { 
        [EnumDescription("Equals")]
        EQUALS, 
        [EnumDescription("Contains")]
        CONTAINS, 
        [EnumDescription("Not Contains")]
        NOTCONTAINS}
    public  class XMLLogEntry
    {
       // public string Number { get { return System.Guid.NewGuid().ToString(); } }

        public string TimeStamp { get { return DateTime.Now.ToString("dd-MM-yy HH:mm:ss"); }  }

        //public string MachineName { get { return Dns.GetHostName(); }  }

        //public string ProcessName { get { return Process.GetCurrentProcess().MainModule.FileName; } }

        public LogCategory Category { get; set; }

        public LogLevel LogLevel { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string DetailMessage { get; set; }

        public LoadModuleName ModuleName { get;  protected set; }

        public string SettableTimeStamp { get; set; }

        public string SettableMachineName { get; set; }

        public string LogFileName { get; set; }

        public string ToXMLString()
        {
            var sBuilder = new StringBuilder { };

            using (XmlWriter xmlWriter = XmlWriter.Create(sBuilder, new XmlWriterSettings { Indent = false, ConformanceLevel = ConformanceLevel.Fragment, Encoding = Encoding.UTF8 }))
            {
                xmlWriter.WriteStartElement("LogEntry");
                foreach (var propName in this.GetType().GetProperties())
                {
                    if (propName.GetValue(this, null) != null)
                    {
                        xmlWriter.WriteElementString(propName.Name, propName.GetValue(this, null).ToString());
                        
                    }

                }
                xmlWriter.WriteEndElement();
                xmlWriter.Flush();
                
            }
            return sBuilder.ToString();

        }
        public string ToCSVString()
        {
            var sBuilder = new StringBuilder { };
            


                foreach (var propName in this.GetType().GetProperties().OrderBy(c => c.Name))
                {
                    if (!GetExcludedProperties().Contains(propName.Name))
                    {
                        var value = propName.GetValue(this, null);

                        if (value != null)
                        {
                            var strValue = value.ToString();
                            if (strValue.Contains(","))
                                strValue = strValue.Replace(",", "$$$$");
                            sBuilder.Append(strValue);
                        }
                        else
                        {
                            sBuilder.Append(string.Empty);
                        }
                        sBuilder.Append(",");
                    }

                }


                var finalResult = sBuilder.ToString();
                return finalResult.Substring(0, finalResult.Length - 1);
            
           
        }
        public string GetCSVHeader()
        {
            var sBuilder = new StringBuilder { };



            foreach (var propName in this.GetType().GetProperties().OrderBy(c => c.Name))
            {
                if (!GetExcludedProperties().Contains(propName.Name))
                {
                    var value = propName.GetValue(this, null);

                    
                        sBuilder.Append(propName.Name);
                   
                    sBuilder.Append(",");
                }

            }


            var finalResult = sBuilder.ToString();
            return finalResult.Substring(0, finalResult.Length - 1);
        }

        public List<string> GetExcludedProperties()
        {
            return new List<string> {"TimeStamp", "MachineName", "ProcessName","LogFileName" };
        }
    }
}
