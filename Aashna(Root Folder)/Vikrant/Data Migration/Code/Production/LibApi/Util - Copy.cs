#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using AlexPilotti.FTPS.Client;
using AlexPilotti.FTPS.Common;
using System.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Linq;
using LibApi.NetSuitePortType;
using System.Diagnostics;
using System.Net.Mail;

#endregion

namespace LibApi
{
    public enum TypeOfXML
    {
        PriCat,
        StylePre,
        DesAdvise,
        FullPriCat
    }

    public enum TypeOfFileToCheck
    {
        PriCat,
        DesAdvise,
        StylePre,
        POHE,
        SOHE,
        POLE,
        ADDVALUETOEnum
    }
    public class FaultFormatingBehavior : IEndpointBehavior
    {

        public FaultFormatingBehavior(Logger logger)
        {
            loggerProp = logger;
        }
        public void Validate(ServiceEndpoint endpoint) { }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new FaultMessageInspector(loggerProp));
        }
        Logger loggerProp { get; set; }
    }

    public class FaultMessageInspector : IClientMessageInspector
    {

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            return null;
        }

        public FaultMessageInspector(Logger logger)
        {
            loggerProp = logger;
        }
        Logger loggerProp { get; set; }
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            if (reply.IsFault)
            {
                using (XmlDictionaryReader reader = reply.GetReaderAtBodyContents())
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(reader);
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);
                    //string xml = reader.ReadInnerXml();
                    XmlNode faultCodeNode = document.SelectSingleNode("//faultcode", nsmgr);
                    XmlNode faultNode = document.SelectSingleNode("//faultstring", nsmgr);
                    XmlNode detailNode = document.SelectSingleNode("//detail", nsmgr);
                    

                    //Provide your handling of additional XML
                    var errorCodeNode = document.SelectSingleNode("//faultcode", nsmgr); 
                    string errorCode = string.Empty;
                    string errorMessage = string.Empty;
                    if (errorCodeNode != null)
                    {
                        errorCode = errorCodeNode.InnerText ?? string.Empty;
                    }
                    
                    if (faultNode!= null)
                    {
                        errorMessage = faultNode.InnerText ?? string.Empty;
                    }
                    loggerProp.error(string.Format("From FaultMessageInspector - ErrorCode - {0}, Message - {1}, DetailNode {2}", errorCode, errorMessage, detailNode.Value));
                    if (!String.IsNullOrEmpty(errorCode))
                    {
                        FaultCode fc = new FaultCode(errorCode);

                        System.ServiceModel.Channels.Message newFaultMessage = System.ServiceModel.Channels.Message.CreateMessage(reply.Version, fc, errorMessage, "");
                        reply = newFaultMessage;
                    }
                }

            }
        }
    }

    public static class LibApiUtil
    {
        /// <summary>
        /// Downloads the files from FTP.
        /// </summary>
        /// <param name="typeOfFiles">The type of files. Pass null to download all files from directory</param>
        /// <param name="ftpDirectory">The FTP directory.</param>
        /// <param name="localDirectory">The local directory.</param>
        /// <param name="ftpServerName">Name of the FTP server.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="passWord">The pass word.</param>
        public static void DownloadFilesFromFTP(string typeOfFiles, string ftpDirectory, string localDirectory,
                                                string ftpServerName, string userName, string passWord, Logger logger, ref List<EmailMessage> emailMessages)
        {
            var client = GetFTPClientWithCurrentDirectorySet(ftpServerName, userName, passWord, ftpDirectory, logger);
            if (String.IsNullOrEmpty(typeOfFiles))
            {
                try
                {
                    client.GetFiles(localDirectory,false);
                    logger.info("Done downloading files");
                }
                catch (Exception ex)
                {
                    logger.error(string.Format("[DownloadFilesFromFTP]Error downloading files {0}", ex.Message + ex.StackTrace));
                    emailMessages.Add(new EmailMessage("Error downloading file from FTP", ex.Message + ex.StackTrace));
                }

            }
            else
            {
                try
                {
                    client.GetFiles(localDirectory, typeOfFiles, EPatternStyle.Wildcard, false);
                }
                catch (Exception ex)
                {
                    emailMessages.Add(new EmailMessage("Error downloading file from FTP", ex.Message + ex.StackTrace));

                }

            }
        }

        /// <summary>
        /// Uploads the files to FTP.
        /// </summary>
        /// <param name="typeOfFiles">The type of files. Pass null to upload all files from directory</param>
        /// <param name="ftpDestDirectory">The FTP dest directory.</param>
        /// <param name="sourceLocalDirectory">The source local directory.</param>
        /// <param name="ftpServerName">Name of the FTP server.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="passWord">The pass word.</param>
        public static void UploadFilesToFTP(List<string> listOfFiles, string typeOfFiles, string ftpDestDirectory,
                                            string sourceLocalDirectory,
                                            string ftpServerName, string userName, string passWord, Logger logger, ref List<EmailMessage> emailMessages)
        {
            var client = GetFTPClientWithCurrentDirectorySet(ftpServerName, userName, passWord, ftpDestDirectory, logger);
            var message = new List<String>();
            foreach (var item in listOfFiles)
            {
                try
                {
                    var fileNameSource = sourceLocalDirectory + item.Substring(item.LastIndexOf("\\") + 1);
                    var destFileName = item.Substring(item.LastIndexOf("\\") + 1);
                    client.PutFile(fileNameSource, destFileName);
                }
                catch (Exception ex)
                {
                    logger.error("[UploadFilesToFTP]Error with uploading the file " + item + " in the directory " + ftpDestDirectory + " reason - " + ex.Message);
                    emailMessages.Add(new EmailMessage("Error with uploading the file", "[UploadFilesToFTP]Error with uploading the file " + item + " in the directory " + ftpDestDirectory + " reason - " + ex.Message));

                }
            }
            
        }

        /// <summary>
        /// Deletes the files from FTP.
        /// </summary>
        /// <param name="listOfFiles">The list of files.Just pass the file name not full qualified name</param>
        /// <param name="ftpSourceDirectory">The FTP source directory.</param>
        /// <param name="ftpServerName">Name of the FTP server.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="passWord">The pass word.</param>
        public static void DeleteFilesFromFTP(List<string> listOfFiles, string ftpSourceDirectory, string ftpServerName,
                                              string userName, string passWord, Logger logger, ref List<EmailMessage> emailMessages)
        {
            var client = GetFTPClientWithCurrentDirectorySet(ftpServerName, userName, passWord, ftpSourceDirectory, logger);
            var message = new List<String>();
            foreach (var item in listOfFiles)
            {
                try
                {
                    client.DeleteFile(item.Substring(item.LastIndexOf("\\") + 1));
                }
                catch (Exception ex)
                {
                    logger.error("[DeleteFilesFromFTP]Error with deleting file from FTP  " + item + ex.Message);
                    emailMessages.Add(new EmailMessage("Error with deleting file from FTP", "[DeleteFilesFromFTP]Error with deleting file from FTP  " + item + ex.Message));

                }
            }
            
        }

        public static FTPSClient GetFTPClientWithCurrentDirectorySet(string ftpServerName, string userName,
                                                                      string ftpPassword, string destDirectory, Logger logger)
        {
            var ftpClient = new FTPSClient {   };
            //using timeout as 30 minutes. 30 * 60 * 100000
            int timeoutForFtp = 0;
            var timeoutfromConfig = ConfigurationSettings.AppSettings["FTPTimeout"];
            

            if (!String.IsNullOrEmpty(timeoutfromConfig))
            {
                logger.info("FTPTimeout value in config file is " + timeoutfromConfig);
                if (!Int32.TryParse(timeoutfromConfig, out timeoutForFtp))
                {
                    logger.info(string.Format("Setting FTPTimeout to {0}", timeoutForFtp));
                    timeoutForFtp = 60 * 60 * 100000;
                }
            }
            else
            {
                logger.info("FTPTimeout value in config file is not present");
                timeoutForFtp = 60 * 60 * 100000;
            }
            ftpClient.Connect(ftpServerName, 21, 
                new NetworkCredential(userName, ftpPassword), 
                ESSLSupportMode.ClearText, null, null, 0, 0, 0, 
                timeoutForFtp);
            ftpClient.SetCurrentDirectory(destDirectory);
            logger.info(string.Format("Connected with {0},{1},{2},{3}", ftpServerName, userName, ftpPassword, timeoutForFtp));
            return ftpClient;
        }

        public static void MoveThisFileToDestinationDirectory(string fileName, string destinationDir, Logger logger)
        {
            if (System.IO.File.Exists(fileName) && Directory.Exists(destinationDir))
            {
                try
                {
                    if (System.IO.File.Exists(destinationDir + Path.GetFileName(fileName)))
                    {
                        //System.IO.File.Delete(destinationDir + Path.GetFileName(fileName));
                        var msg = string.Format("File {0} already present in Directory {1} hence ignored...", fileName ,
                                                destinationDir + fileName.Substring(fileName.LastIndexOf("\\") + 1));
                        logger.info(msg);
                        return;
                    }
                    System.IO.File.Move(fileName, destinationDir + fileName.Substring(fileName.LastIndexOf("\\") + 1));
                    var message = string.Format("[MoveThisFileToDestinationDirectory]Moved {0} to Directory {1}", fileName,
                                                destinationDir + fileName.Substring(fileName.LastIndexOf("\\") + 1));
                    logger.info(message);
                }
                catch (Exception ex)
                {
                    var message = string.Format("[MoveThisFileToDestinationDirectory]Error while moving {0} to Directory {1}. Error - {2} ", fileName,
                                                destinationDir + fileName.Substring(fileName.LastIndexOf("\\") + 1), ex.Message + ex.StackTrace);
                    //throw  new Exception("Error occured while moving " + fileName + "to the directory " + destinationDir);
                    logger.info(message);
                }
            }
        }


       


       
    }

    /// <summary>
    /// Stores the list of Currency and shop code data
    /// </summary>
    public class CurrencyShopCodeMap
    {
        /// <summary>
        /// Gets or sets Currrency
        /// </summary>
        public string Currency { get; set; }


        /// <summary>
        /// Gets or sets CurrencyCode
        /// </summary>
        public string CurrencyCode { get; set; }


        /// <summary>
        /// Gets or sets ShopCode
        /// </summary>
        public string ShopCode { get; set; }
    }


    public static class Utils
    {

        static Logger _logger = new Logger("Utils", "Info");


        static Dictionary<string, string> _countries = new Dictionary<string, string>();

        static List<CurrencyShopCodeMap> _currencyShopCodeMapping = new List<CurrencyShopCodeMap>();

        /// <summary>
        /// Gets the list Currency and shop code mapping objects
        /// </summary>
        public static List<CurrencyShopCodeMap> CurrencyShopCodeMappings
        {
            get
            {
                if (_currencyShopCodeMapping.Count == 0)
                {
                    //_currencyShopCodeMapping = GetCurrencyShopCodeMappings();
                    //_currencyShopCodeMapping = GetCurrencyShopCodeMappings();

                }

                return _currencyShopCodeMapping;
            }

        }


        /// <summary>
        /// Reads the CurrencyShopCodeMapping xml file and create the list of mappings
        /// </summary>
        /// <returns></returns>
        //This method is not in use as of now
        private static List<CurrencyShopCodeMap> GetCurrencyShopCodeMappingsOld()
        {
            List<CurrencyShopCodeMap> currencyShopCodeMap = new List<CurrencyShopCodeMap>();
            var currecyShopCodeMapFile = "CurrencyShopCodeMapping.xml";
            //check if file exist
            System.Xml.XmlReader xmlReader = null;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<CurrencyShopCodeMap>));
                xmlReader = new System.Xml.XmlTextReader(currecyShopCodeMapFile);
                currencyShopCodeMap = (List<CurrencyShopCodeMap>)xmlSerializer.Deserialize(xmlReader);

                xmlReader.Close();
                return currencyShopCodeMap;

            }
            catch (FileNotFoundException)
            {
                //throw new FileNotFoundException(@"File 'CurrencyShopCodeMapping' not found." );
                _logger.info(string.Format("File 'CurrencyShopCodeMapping' not found."));

            }
            catch (Exception ex)
            {
                _logger.info(string.Format("Error while reading CurrencyShopCodeMapping."));
                _logger.info(string.Format("Detail: {0} \n{1}", ex.Message, ex.StackTrace));
            }
            finally
            {
                xmlReader.Close();
            }
            return currencyShopCodeMap;
        }

        /// <summary>
        /// Gets or sets Countries
        /// </summary>
        public static Dictionary<string, string> Countries
        {
            get
            {
                if (_countries.Count == 0)
                {
                    PopulateContriesDictionary();
                }
                return _countries;
            }
        }

        public static string GetCountryNameFromEnum(string enumvalue)
        {
            var country = string.Empty;

            var charArray = enumvalue.ToCharArray();

            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] == '_')
                    continue;
                if (string.Equals(charArray[i].ToString(), charArray[i].ToString().ToUpper(), StringComparison.Ordinal))
                {
                    _countries.Add("Sweden", "9402");
                    _countries.Add("Norway", "9404");

                }
            }

            return country;
        }

        public static string[] GetCountryNameAndCode(string enumString)
        {
            string[] code = new string[2] { string.Empty, string.Empty };

            //

            var countryName = (from c in LibApi.Utils.Countries
                               where c.Key == enumString
                               select c.Value).FirstOrDefault();

            if (countryName != null)
            {
                code = countryName.Split('|');
            }

            return code;
        }

        private static void PopulateContriesDictionary()
        {
            _countries.Add("_albania", "Albania|AL");
            _countries.Add("_algeria", "Algeria|DZ");
            _countries.Add("_americanSamoa", "American Samoa|AS");
            _countries.Add("_andorra", "Andorra|AD");
            _countries.Add("_angola", "Angola|AO");
            _countries.Add("_anguilla", "Anguilla|AI");
            _countries.Add("_antarctica", "Antarctica|AQ");
            _countries.Add("_antiguaAndBarbuda", "Antigua and Barbuda|AG");
            _countries.Add("_argentina", "Argentina|AR");
            _countries.Add("_armenia", "Armenia|AM");
            _countries.Add("_aruba", "Aruba|AW");
            _countries.Add("_australia", "Australia|");
            _countries.Add("_austria", "Austria|AT");
            _countries.Add("_azerbaijan", "Azerbaijan|AZ");
            _countries.Add("_bahamas", "Bahamas|BS");
            _countries.Add("_bahrain", "Bahrain|BH");
            _countries.Add("_bangladesh", "Bangladesh|BD");
            _countries.Add("_barbados", "Barbados|BB");
            _countries.Add("_belarus", "Belarus|BY");
            _countries.Add("_belgium", "Belgium|BE");
            _countries.Add("_belize", "Belize|BZ");
            _countries.Add("_benin", "Benin|BJ");
            _countries.Add("_bermuda", "Bermuda|BM");
            _countries.Add("_bhutan", "Bhutan|BT");
            _countries.Add("_bolivia", "Bolivia|BO");
            _countries.Add("_bosniaAndHerzegovina", "Bosnia|BA");
            _countries.Add("_botswana", "Botswana and Herzegobina|BW");
            _countries.Add("_bouvetIsland", "Bouvet Island|BV");
            _countries.Add("_brazil", "Brazil|BR");
            _countries.Add("_britishIndianOceanTerritory", "British Indian Ocean Territory|IO");
            _countries.Add("_bruneiDarussalam", "Brunei Darussalam|BN");
            _countries.Add("_bulgaria", "Bulgaria|BG");
            _countries.Add("_burkinaFaso", "Burkina Faso|BF");
            _countries.Add("_burundi", "Burundi|BI");
            _countries.Add("_cambodia", "Cambodia|KH");
            _countries.Add("_cameroon", "Cameroon|CM");
            _countries.Add("_canada", "Canada|CA");
            _countries.Add("_capVerde", "Cap Verde|CV");
            _countries.Add("_caymanIslands", "Cayman Islands|KY");
            _countries.Add("_centralAfricanRepublic", "Central African Republic|CF");
            _countries.Add("_chad", "Chad|TD");
            _countries.Add("_chile", "Chile|CL");
            _countries.Add("_china", "China|CN");
            _countries.Add("_christmasIsland", "Christmas Island|CX");
            _countries.Add("_cocosKeelingIslands", "Cocos (Keeling) Islands|CC");
            _countries.Add("_colombia", "Colombia|CO");
            _countries.Add("_comoros", "Comoros|KM");
            _countries.Add("_congoDemocraticPeoplesRepublic", "Congo, Democratic People’s Republic|CD");
            _countries.Add("_congoRepublicOf", "Congo, Republic of|CG");
            _countries.Add("_cookIslands", "Cook Islands|CK");
            _countries.Add("_costaRica", "Costa Rica|CR");
            _countries.Add("_coteDIvoire", "Cote d’Ivoire|CI");
            _countries.Add("_croatiaHrvatska", "Croatia/Hrvatska|HR");
            _countries.Add("_cuba", "Cuba|CU");
            _countries.Add("_cyprus", "Cyprus|CY");
            _countries.Add("_czechRepublic", "Czech Republic|CZ");
            _countries.Add("_denmark", "Denmark|DK");
            _countries.Add("_djibouti", "Djibouti|DJ");
            _countries.Add("_dominica", "Domenica|DM");
            _countries.Add("_dominicanRepublic", "Dominican Republic|DO");
            _countries.Add("_eastTimor", "East Timor|TP");
            _countries.Add("_ecuador", "Ecuador|EC");
            _countries.Add("_egypt", "Egypt|EG");
            _countries.Add("_elSalvador", "El Salvador|SV");
            _countries.Add("_equatorialGuinea", "Equatorial Guinea|GQ");
            _countries.Add("_eritrea", "Eritrea|ER");
            _countries.Add("_estonia", "Estonia|EE");
            _countries.Add("_ethiopia", "Ethiopia|ET");
            _countries.Add("_falklandIslandsMalvina", "Falkland Islands (Malvina)|FK");
            _countries.Add("_faroeIslands", "Faroe Islands|FO");
            _countries.Add("_fiji", "Fiji|FJ");
            _countries.Add("_finland", "Finland|FI");
            _countries.Add("_france", "France|FR");
            _countries.Add("_frenchGuiana", "French Guiana|GF");
            _countries.Add("_frenchPolynesia", "French Polynesia|PF");
            _countries.Add("_frenchSouthernTerritories", "French Southern Territories|TF");
            _countries.Add("_gabon", "Gabon|GA");
            _countries.Add("_gambia", "Gambia|GM");
            _countries.Add("_georgia", "Georgia|GE");
            _countries.Add("_germany", "Germany|DE");
            _countries.Add("_ghana", "Ghana|GH");
            _countries.Add("_gibraltar", "Gibraltar|GI");
            _countries.Add("_greece", "Greece|GR");
            _countries.Add("_greenland", "Greenland|GL");
            _countries.Add("_grenada", "Grenada|GD");
            _countries.Add("_guadeloupe", "Guadeloupe|GP");
            _countries.Add("_guam", "Guam|GU");
            _countries.Add("_guatemala", "Guatemala|GT");
            _countries.Add("_guernsey", "Guernsey|GG");
            _countries.Add("_guinea", "Guinea|GN");
            _countries.Add("_guineaBissau", "Guinea-Bissau|GW");
            _countries.Add("_guyana", "Guyana|GY");
            _countries.Add("_haiti", "Haiti|HT");
            _countries.Add("_heardAndMcDonaldIslands", "Heard and McDonald Is.|HM");
            _countries.Add("_holySeeCityVaticanState", "Holy See (City Vatican State)|VA");
            _countries.Add("_honduras", "Honduras|HN");
            _countries.Add("_hongKong", "Hong Kong|HK");
            _countries.Add("_hungary", "Hungary|HU");
            _countries.Add("_iceland", "Iceland|IS");
            _countries.Add("_india", "India|IN");
            _countries.Add("_indonesia", "Indonesia|ID");
            _countries.Add("_iranIslamicRepublicOf", "Iran Islamic Republic|IR");
            _countries.Add("_iraq", "Iraq|IQ");
            _countries.Add("_ireland", "Ireland|IE");
            _countries.Add("_isleOfMan", "Isle of Man|IM");
            _countries.Add("_israel", "Israel|IL");
            _countries.Add("_italy", "Italy|IT");
            _countries.Add("_jamaica", "Jamaica|JM");
            _countries.Add("_japan", "Japan|JP");
            _countries.Add("_jersey", "Jersey|JE");
            _countries.Add("_jordan", "Jordan|JO");
            _countries.Add("_kazakhstan", "Kazakhstan|KZ");
            _countries.Add("_kenya", "Kenya|KE");
            _countries.Add("_kiribati", "Kiribati|KI");
            _countries.Add("_koreaDemocraticPeoplesRepublic", "Korea, Democratic People’s Republic|KP");
            _countries.Add("_koreaRepublicOf", "Korea, Republic of|KR");
            _countries.Add("kuwait", "Kuwait|KW");
            _countries.Add("_kyrgyzstan", "Krgyzstan|KG");
            _countries.Add("_laoPeoplesDemocraticRepublic", "Lao, People’s Democratic Republic|LA");
            _countries.Add("_latvia", "Latvia|LV");
            _countries.Add("_lebanon", "Lebanon|LB");
            _countries.Add("_lesotho", "Lesotho|LS");
            _countries.Add("_liberia", "Liberia|LR");
            _countries.Add("_libyanArabJamahiriya", "Libyan Arab Jamahiriya|LY");
            _countries.Add("_liechtenstein", "Liechtenstein|LI");
            _countries.Add("_lithuania", "Lithuania|LT");
            _countries.Add("_luxembourg", "Luxembourg|LU");
            _countries.Add("_macau", "Macau|MO");
            _countries.Add("_macedonia", "Macedonia|MK");
            _countries.Add("_madagascar", "Madagascar|MG");
            _countries.Add("_malawi", "Malawi|MW");
            _countries.Add("_malaysia", "Malaysia|MY");
            _countries.Add("_maldives", "Maldives|MV");
            _countries.Add("_mali", "Mali|ML");
            _countries.Add("_malta", "Malta|MT");
            _countries.Add("_marshallIslands", "Marshall Islands|MH");
            _countries.Add("_martinique", "Martinique|MQ");
            _countries.Add("_mauritania", "Mauritania|MR");
            _countries.Add("_mauritius", "Mauritius|MU");
            _countries.Add("_mayotte", "Mayotte|YT");
            _countries.Add("_mexico", "Mexico|MX");
            _countries.Add("_micronesiaFederalStateOf", "Micronesia, Federal State of|FM");
            _countries.Add("_moldovaRepublicOf", "Moldova, Republic of|MD");
            _countries.Add("_monaco", "Monaco|MC");
            _countries.Add("_mongolia", "Mongolia|MN");
            _countries.Add("_montenegro", "Montenegro|ME");
            _countries.Add("_montserrat", "Montserrat|MS");
            _countries.Add("_morocco", "Morocco|MA");
            _countries.Add("_mozambique", "Mozambique|MZ");
            _countries.Add("_myanmar", "Myanmar|MM");
            _countries.Add("_namibia", "Namibia|NA");
            _countries.Add("_nauru", "Nauru|NR");
            _countries.Add("_nepal", "Nepal|NP");
            _countries.Add("_netherlands", "Netherlands|NL");
            _countries.Add("_netherlandsAntilles", "Netherlands Antilles|AN");
            _countries.Add("_newCaledonia", "New Caledonia|NC");
            _countries.Add("_newZealand", "New Zealand|NZ");
            _countries.Add("_nicaragua", "Nicaragua|NI");
            _countries.Add("_niger", "Niger|NE");
            _countries.Add("_nigeria", "Nigeria|NG");
            _countries.Add("_niue", "Niue|NU");
            _countries.Add("_norfolkIsland", "Norfolk Islands|NF");
            _countries.Add("_northernMarianaIslands", "Northern Mariana Islands|MP");
            _countries.Add("_norway", "Norway|NO");
            _countries.Add("_oman", "Oman|OM");
            _countries.Add("_pakistan", "Pakistan|PK");
            _countries.Add("_palau", "Palau|PW");
            _countries.Add("_palestinianTerritories", "Palestinian Territories|PS");
            _countries.Add("_panama", "Panama|PA");
            _countries.Add("_papuaNewGuinea", "Papua New Guinea|PG");
            _countries.Add("_paraguay", "Paraguay|PY");
            _countries.Add("_peru", "Peru|PE");
            _countries.Add("_philippines", "Philppines|PH");
            _countries.Add("_pitcairnIsland", "Pitcairn Island|PN");
            _countries.Add("_poland", "Poland|PL");
            _countries.Add("_portugal", "Portugal|PT");
            _countries.Add("_puertoRico", "Puerto Rico|PR");
            _countries.Add("_qatar", "Qatar|QA");
            _countries.Add("_reunionIsland", "Reunion Island|RE");
            _countries.Add("_romania", "Romania|RO");
            _countries.Add("_russianFederation", "Russian Federation|RU");
            _countries.Add("_rwanda", "Rwanda|RW");
            _countries.Add("_saintKittsAndNevis", "Saint Kitts and Nevis|KN");
            _countries.Add("_saintLucia", "Saint Lucia|LC");
            _countries.Add("_saintVincentAndTheGrenadines", "Saint Vincent and the Grenadines|VC");
            _countries.Add("_sanMarino", "San Marino|SM");
            _countries.Add("_saoTomeAndPrincipe", "Sao Tome and Principe|ST");
            _countries.Add("_saudiArabia", "Saudi Arabia|SA");
            _countries.Add("_senegal", "Senegal|SN");
            _countries.Add("_serbia", "Serbia|CS");
            _countries.Add("_seychelles", "Seychelles|SC");
            _countries.Add("_sierraLeone", "Sierra Leone|SL");
            _countries.Add("_singapore", "Singapore|SG");
            _countries.Add("_slovakRepublic", "Slovak Republic|SK");
            _countries.Add("_slovenia", "Slovenia|SI");
            _countries.Add("_solomonIslands", "Solomon Islands|SB");
            _countries.Add("_somalia", "Somalia|SO");
            _countries.Add("_southAfrica", "South Africa|ZA");
            _countries.Add("_southGeorgia", "South Georgia|GS");
            _countries.Add("_spain", "Spain|ES");
            _countries.Add("_sriLanka", "Sri Lanka|LK");
            _countries.Add("_stHelena", "St. Helena|SH");
            _countries.Add("_stPierreAndMiquelon", "St. Pierre and Miquelon|PM");
            _countries.Add("_sudan", "Sudan|SD");
            _countries.Add("_suriname", "Suriname|SR");
            _countries.Add("_svalbardAndJanMayenIslands", "Svalbard and Jan Mayen Islands|SJ");
            _countries.Add("_swaziland", "Swaziland|SZ");
            _countries.Add("_sweden", "Sweden|SE");
            _countries.Add("_switzerland", "Switzerland|CH");
            _countries.Add("_syrianArabRepublic", "Syrian Arab Republic|SY");
            _countries.Add("_taiwan", "Taiwan|TW");
            _countries.Add("_tajikistan", "Tajikistan|TJ");
            _countries.Add("_tanzania", "Tanzania|TZ");
            _countries.Add("_thailand", "Thailand|TH");
            _countries.Add("_togo", "Togo|TG");
            _countries.Add("_tokelau", "Tokelau|TK");
            _countries.Add("_tonga", "Tonga|TO");
            _countries.Add("_trinidadAndTobago", "Trinidad and Tobago|TT");
            _countries.Add("_tunisia", "Tunisia|TN");
            _countries.Add("_turkey", "Turkey|TR");
            _countries.Add("_turkmenistan", "Turkmenistan|TM");
            _countries.Add("_turksAndCaicosIslands", "Turks and Caicos Islands|TC");
            _countries.Add("_tuvalu", "Tuvalu|TV");
            _countries.Add("_uganda", "Uganda|UG");
            _countries.Add("_ukraine", "Ukraine|UA");
            _countries.Add("_unitedArabEmirates", "United Arab Emirates|AE");
            _countries.Add("_unitedKingdomGB", "United Kingdom (GB)|GB");
            _countries.Add("_unitedStates", "United States|US");
            _countries.Add("_uruguay", "Uruguay|UY");
            _countries.Add("_uSMinorOutlyingIslands", "US Minor Outlying Islands|UM");
            _countries.Add("_uzbekistan", "Uzbekistan|UZ");
            _countries.Add("_vanuatu", "Vanuatu|VU");
            _countries.Add("_venezuela", "Venezuela|VE");
            _countries.Add("_vietnam", "Vietnam|VN");
            _countries.Add("_virginIslandsBritish", "Virgin Islands British|VG");
            _countries.Add("_virginIslandsUSA", "Virgin Islands USA|VI");
            _countries.Add("_wallisAndFutunaIslands", "Wallis and Futana Islands|WF");
            _countries.Add("_westernSahara", "Western Sahara|EH");
            _countries.Add("_westernSamoa", "Western Samoa|WS");
            _countries.Add("_yemen", "Yemen|YE");
            _countries.Add("_yugoslavia", "Yugoslavia|YU");
            _countries.Add("_zambia", "Zambia|ZM");
            _countries.Add("_zimbabwe", "Zimbabwe|ZWe");
        }

        /// <summary>
        /// Get the shop code for the currency
        /// </summary>
        /// <param name="currency">Currency</param>
        /// <returns>Shop code</returns>
        public static string GetShopCodeFromCurrency(string currency)
        {
            var retVal = "";

            retVal = (from c in CurrencyShopCodeMappings
                      where c.Currency == currency
                      select c.ShopCode).FirstOrDefault();

            return retVal ?? string.Empty;
        }

        /// <summary>
        /// Convert to string of comma separated values
        /// </summary>
        /// <param name="list"></param>
        /// <param name="enclosedWith">char in which value to be enclosed</param>
        /// <returns></returns>
        public static string ToCSV<T>(this IEnumerable<T> list, string enclosedWith)
        {
            return list.ToCSV(",", enclosedWith);

        }
        /// <summary>
        /// Convert to string of separated values by provided separater
        /// </summary>
        /// <param name="list"></param>
        /// <param name="separator">Separater to be used between two values</param>
        /// <param name="enclosedWith">char in which value to be enclosed</param>
        /// <returns></returns>
        public static string ToCSV<T>(this IEnumerable<T> list, string separator, string enclosedWith)
        {
            var retVal = new StringBuilder();
            //If the separat
            if (separator == null) separator = " ";
            foreach (var item in list)
            {
                //if itemis blank, we do not want it.
                if (item.ToString().Length <= 0)
                {
                    continue;
                }

                if (retVal.Length != 0)
                {
                    retVal.Append(separator);
                }

                retVal.Append(string.Format("{0}{1}{2}", enclosedWith, item, enclosedWith));
            }

            return retVal.ToString();
        }
        /// <summary>
        /// Convert to string of comma separated values
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string ToCSV<T>(this IEnumerable<T> list)
        {
            return list.ToCSV("");
        }


        public static void SendEmail(List<EmailMessage> messages, string subject, Logger logger)
        {
            var sbr = new StringBuilder();
            var serialNo = 0;
            foreach (var message in messages)
            {
                sbr.AppendLine(string.Format("[Message : {0}]", ++serialNo));
                sbr.AppendLine(message.ToString());
            }

            var messageBody = sbr.ToString();
            Debug.WriteLine(messageBody);
            SendEmail(messageBody, subject,logger);
            
        }

        public static void SendEmail(List<EmailMessage> messages, string subject)
        {
            SendEmail(messages, subject, null);
        }

        public static void SendEmail(string message, Logger logger)
        {
            if (ConfigurationSettings.AppSettings["Email.Enabled"] == "TRUE")
            {
                int port = 0;
                if (Int32.TryParse(ConfigurationSettings.AppSettings["Email.SmtpPort"], out port))
                {
                }
                else
                {
                    port = 25;
                }
                EmailHandler.SendMailMessage(ConfigurationSettings.AppSettings["Email.Smtp"],
                                             port,
                                             ConfigurationSettings.AppSettings["Email.Sender"],
                                             ConfigurationSettings.AppSettings["Email.Recipient"],
                                             ConfigurationSettings.AppSettings["Email.Subject"],
                                             message, MailPriority.High, logger,
                                             ConfigurationSettings.AppSettings["Email.UserName"],
                                             ConfigurationSettings.AppSettings["Email.PassWord"]);
            }
        }

        public static void SendEmail(string message, string subject, Logger logger)
        {
            if (ConfigurationSettings.AppSettings["Email.Enabled"] == "TRUE")
            {
                int port = 0;
                if (Int32.TryParse(ConfigurationSettings.AppSettings["Email.SmtpPort"], out port))
                {
                }
                else
                {
                    port = 25;
                }
                
                EmailHandler.SendMailMessage(ConfigurationSettings.AppSettings["Email.Smtp"],
                                             port,
                                             ConfigurationSettings.AppSettings["Email.Sender"],
                                             ConfigurationSettings.AppSettings["Email.Recipient"],
                                              subject,
                                             message, MailPriority.High,logger ,
                                             ConfigurationSettings.AppSettings["Email.UserName"],
                                             ConfigurationSettings.AppSettings["Email.PassWord"],true);
            }
        }

        public static void SendEmail(string message, string subject)
        {
            SendEmail(message, subject, null);
        }
    }

    public class LibUtil
    {
        public static CustomFieldRef GetStringCustomField(String id, String value)
        {
            var field = new StringCustomFieldRef();
            field.internalId = id;
            field.value = value;
            
            return field;
        }

        public static RecordRef GetRecordRef(string internalId, string externalId)
        {
            var recordToReturn = new RecordRef { internalId = internalId, externalId = externalId };
            return recordToReturn;
        }

        public static SelectCustomFieldRef GetSelectCustomField(string typeId, string internalId, string name,
                                                                string fieldId)
        {
            var fdref = new ListOrRecordRef();

            if (!String.IsNullOrEmpty(internalId))
            {
                fdref.internalId = internalId;
            }
            fdref.typeId = typeId;
            fdref.name = name;
            fdref.externalId = name;

            var field = new SelectCustomFieldRef();
            field.internalId = fieldId;
            field.value = fdref;

            return field;
        }

        public static ListOrRecordRef GetListOrRecordRef(string typeId, string internalId, string name)
        {
            return new ListOrRecordRef { internalId = internalId, typeId = typeId, name = name };
        }

        public static CustomList GetCustomListAvailableValues(string listInternalId, string listName,
                                                       NetSuitePortTypeClient client, Logger logger, Passport passport)
        {
            var list = new CustomList();

            ReadResponse rS = null;
            var recordRef = new RecordRef { type = RecordType.customList, typeSpecified = true, internalId = listInternalId };

            try
            {
                logger.info(string.Format("Trying to get a custom list with name {0} and id {1}", listInternalId,
                                          listName));

                client.get(passport, null, null, null, recordRef, out rS);
                if (rS.status.isSuccess)
                {
                    logger.info(string.Format("got the custom list {0} - {1}", listInternalId, listName));

                    list = rS.record as CustomList;
                }
            }
            catch (Exception ex)
            {
                logger.error(string.Format("Exception occured while getting custom list {0} id {1}", listName,
                                           listInternalId));
                logger.error(string.Format("vendor add exception - {0}", ex.Message + ex.StackTrace));
            }
            return list;
        }

    }

}