﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibApi
{
    public class EmailMessage
    {

        /// <summary>
        /// Gets or sets HeaderLine
        /// </summary>
        public string HeaderLine { get; set; }


        /// <summary>
        /// Gets or sets DetailedMessage
        /// </summary>
        public string DetailedMessage { get; set; }


        public EmailMessage(string headerLine, string detailedMessage)
        {
            HeaderLine = headerLine;
            DetailedMessage = detailedMessage;
        }

        public override string ToString()
        {
            var retString = string.Empty;
            StringBuilder sbr = new StringBuilder();

            //Header line
            //-------------------------------------------------------------------------------------------------------------
            //DetailedMessage
            //=============================================================================================================
            
            if(!String.IsNullOrEmpty(HeaderLine))
            {
                sbr.AppendLine(HeaderLine);
            }

            sbr.AppendLine("---------------------------------------------------------------------------------");
            if (!String.IsNullOrEmpty(DetailedMessage))
            {
                sbr.AppendLine(DetailedMessage);
            }
            sbr.AppendLine("================================================================================");
            retString = sbr.ToString();
            return retString;
        }
    }
}
