﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace LibApi
{
    public static class EmailHandler
    {
        public static void SendMailMessage(string smtpHost,int smtpPort, string from, string to, string subject, string message, MailPriority priority, Logger logger, string credentialsUserName, string credentialsPassword)
        {
            SendMailMessage(smtpHost,smtpPort, from, to, subject, message, priority, logger, credentialsUserName, credentialsPassword, false);
        }

        public static void SendMailMessage(string smtpHost, int smtpPort, string from, string to, string subject, string message, MailPriority priority, Logger logger, string credentialsUserName, string credentialsPassword, bool attachLogFile)
        {
            try
            {
                Console.WriteLine(string.Format("Sending an email"));

                var result = to.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int count = 0; count < result.Length; count++)
                {

                    using (var mailMsg = new MailMessage
                    {
                        Subject = subject,
                        Body = message,
                        Priority = priority,
                        BodyEncoding = Encoding.ASCII,

                    })
                    {
                        mailMsg.From = new MailAddress(from);
                        mailMsg.To.Add(new MailAddress(result[count]));
                        Attachment attachemnt = null;
                        //GEt the log file
                        if (logger != null && !string.IsNullOrEmpty(logger.LogFile))
                        {
                            if (System.IO.File.Exists(logger.LogFile))
                            {
                                attachemnt = new Attachment(logger.LogFile);
                            }
                        }

                        mailMsg.Attachments.Add(attachemnt);

                        var smtpClient = new SmtpClient(smtpHost, smtpPort);
                        smtpClient.Credentials = new NetworkCredential { UserName = credentialsUserName, Password = credentialsPassword };
                        smtpClient.Send(mailMsg);

                        attachemnt = null;

                    }

                }

            }
            catch (Exception exc)
            {
                if (logger == null)
                {
                    Console.WriteLine(string.Format("Error occured while sending an email to {0} - Message - {1} Error trace -{2}", to, exc.Message, exc.StackTrace));
                }
                else
                {
                    logger.error(string.Format("Error occured while sending an email to {0} - Message - {1} Error trace -{2}", to, exc.Message, exc.StackTrace));
                }
            }
        }

    }
}
