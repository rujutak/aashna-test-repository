﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;


namespace LibApi
{
    public class DesAdviseXPath
    {

        public const string ROOT = "ns0:DespatchAdvice";
        public const string ID = "cac:DocumentReference/cbc:ID";
        public const string ISSUE_DATE = "cbc:IssueDate";
        public const string NO_OF_PACKAGES = "cac:DespatchLine/cbc:NumberOfPackages";


        public static XmlNamespaceManager namespaceManager { get; set; }

        public static DesAdv GetDesAdvInput(XElement xElement)
        {
            if (xElement == null)
                throw new ArgumentNullException("xElement");
            var desAdv = new DesAdv();

            namespaceManager = new XmlNamespaceManager(XMLParser.nameTable);
            namespaceManager.AddNamespace("cbc" , "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2:Dan-1");
            namespaceManager.AddNamespace("ns0" , "urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2:Dan-1");
            namespaceManager.AddNamespace("cac" , "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2:Dan-1");


            desAdv.Id = GetString(xElement, ID);
            desAdv.PODate = GetString(xElement, ISSUE_DATE);
            desAdv.SellerReferenceNumber = GetSellerReferenceNumber(xElement);
            desAdv.POItems = GetPOItems(xElement);
            desAdv.NoOfPackages = Convert.ToDouble(GetString(xElement, NO_OF_PACKAGES));
            //Change Request to set NoOfPackages to 1 if NoOfPackages is ZERO
            if (desAdv.NoOfPackages == 0)
            {
                desAdv.NoOfPackages = 1;
            }
            return desAdv;
        }

        private static List<POItem> GetPOItems(XElement xElement)
        {
            var poItem = new POItem();
            var POItems = new List<POItem>();
            var poItemNodes = xElement.XPathSelectElements(poItem.GetXPathExpression(), namespaceManager);
            foreach (var item in poItemNodes)
            {
                poItem = new POItem
                {
                    PoItem_Id = GetString(item, POItem.POITEM_ID),
                    PoItem_Description = GetPO_Description(item),
                    PoItem_Quantity = GetPO_Qty(item),
                    
                };
                POItems.Add(poItem);
            }
            return POItems;
        }

        private static string GetSellerReferenceNumber(XElement xElement)
        {
            var SellerPartyElements = xElement.XPathSelectElements(POItem.SELLER_PARTY_NODE, namespaceManager);
            foreach (var item in SellerPartyElements)
            {
                var xSingleElement = item.XPathSelectElement(POItem.SELLER_PARTY_ID, namespaceManager);
                if (xSingleElement != null)
                {
                    return xSingleElement.Value;
                }
            }
            return string.Empty;
        }

        private static string GetPO_Description(XElement xElement)
        {
            var poItems_PhyAttrs = xElement.XPathSelectElements(POItem.POITEM_PHY_ATTR, namespaceManager);

            foreach (var item in poItems_PhyAttrs)
            {
                if (GetString(item, POItem.POITEM_ATTR_ID) == "Description")
                {
                    return GetString(item, POItem.POITEM_ATTR_DESC);
                }
            }

            return string.Empty;

        }

        private static double GetPO_Qty(XElement xElement)
        {
            double qty = 0;
            var poItems_PhyAttrs = xElement.XPathSelectElements(POItem.POITEM_PHY_ATTR, namespaceManager);

            foreach (var item in poItems_PhyAttrs)
            {
                if (GetString(item, POItem.POITEM_ATTR_ID) == "QuantityPerPack")
                {
                    qty = Convert.ToDouble( GetString(item, POItem.POITEM_ATTR_DESC) );
                }
            }

            return qty;
        }

        /// <summary>
        ///     utility method to read xmlnode value from xDoc directly
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="xPathExpression"></param>
        /// <returns></returns>
        public static string GetString(XElement xElement, string xPathExpression)
        {
            var xSingleElement = xElement.XPathSelectElement(xPathExpression, namespaceManager);
            return xSingleElement == null ? string.Empty : xSingleElement.Value;
        }
    }
}
