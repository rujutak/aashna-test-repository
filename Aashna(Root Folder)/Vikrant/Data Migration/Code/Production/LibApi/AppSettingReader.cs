﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace LibApi
{
    public class AppSettingReader
    {
        private Settings appSettings = null;

        public AppSettingReader(string settingsFilePath)
        {
            appSettings = SerializeSettingXml(settingsFilePath);
        }

        private Settings SerializeSettingXml(string file)
        {
            Settings settings = null;

            using (XmlReader reader = new XmlTextReader(file))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                settings = (Settings)serializer.Deserialize(reader);   
            }

            return settings;
        }

        /// <summary>
        /// Get the setting value by name
        /// </summary>
        /// <param name="name">Name of the setting which value is to be retrived</param>
        /// <returns>Value</returns>
        /// <exception cref="NullReferenceException">Throw null ref exception when mode or setting name is not available.</exception>
        public string GetSetting(string name)
        {
            string settingVal = null;

            var currentMode = appSettings.RunningMode;
            if (string.IsNullOrEmpty(currentMode))
	        {
		        throw new NullReferenceException("RunningMode is not set in the xml."); 
	        }

            var moadSettings = GetModeSettings(currentMode);
            try
            {

                settingVal = (from s in moadSettings
                              where s.Name.ToUpper().Equals(name.ToUpper())
                              select s.Value).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw new NullReferenceException(string.Format("Setting by name '{0}' is not available for '{1}' mode.",name,currentMode),ex);    
            }
            

            return settingVal;
        }

        private List<Setting> GetModeSettings(string currentMode)
        {
            var mode = (from m in appSettings.Modes
                        where m.Name.ToUpper().Equals(currentMode.ToUpper())
                        select m).FirstOrDefault();
            if (mode == null)
            {
                throw new NullReferenceException(string.Format("No mode in settings by name '{0}'"));
            }

            return mode.Settings;
        }

    }

    public class Settings
    {
        /// <summary>
        /// Gets or sets Name
        /// </summary>
        [XmlElement]
        public string RunningMode { get; set; }
        
        /// <summary>
        /// Gets or sets Setting
        /// </summary>
        [XmlElement("Mode")]
        public List<Mode> Modes{ get; set; }
    }

    public class Mode
    {
        /// <summary>
        /// Gets or sets Name
        /// </summary>
        [XmlAttribute("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Setting
        /// </summary>
        [XmlElement("Setting")]
        public List<Setting> Settings { get; set; }
    }

    public class Setting
    {
        /// <summary>
        /// Gets or sets Name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets Desc
        /// </summary>
        public string Desc { get; set; }
    }
}
