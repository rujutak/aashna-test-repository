﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibApi;

namespace FullPriCat
{
    public class FPcatXmlLogEntry : XMLLogEntry
    {
        public  FPcatXmlLogEntry()
        {
            ModuleName = LoadModuleName.FULLPRICAT;
        }
        
        public string StyleNo { get; set; }

        public string FileName { get; set; }

        public string EAN { get; set; }

        public string Brand { get; set; }

        public string ItemName { get; set; }

        

    }
}
