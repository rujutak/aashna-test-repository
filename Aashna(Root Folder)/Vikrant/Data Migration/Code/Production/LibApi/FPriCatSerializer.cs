﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace LibApi
{
    public class FPriCatSerializer
    {
        public static SalesPriceCatalogue_Xml_Full GetFPriCatDeserialized(string FileNameTobeImported)
        {
            var s = File.ReadAllText(FileNameTobeImported);
            if (s.StartsWith("?"))
            {
                var d = s.Remove(0, 1);
                if (File.Exists(FileNameTobeImported))
                    File.Delete(FileNameTobeImported);
                File.WriteAllText(FileNameTobeImported, d, Encoding.Default);
            }
           
            XmlSerializer deserializer = new XmlSerializer(typeof(SalesPriceCatalogue_Xml_Full));
            
            TextReader textReader = new StreamReader(FileNameTobeImported,Encoding.Default);
            
            
            
            var fullPriCat = deserializer.Deserialize(textReader) as SalesPriceCatalogue_Xml_Full;
            
            textReader.Close();

            if (fullPriCat == null || fullPriCat.Items.Count() < 2)
            {
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]Can not deserialize the pri cat file ");
            }

            var group = fullPriCat.Items[1] as SalesPriceCatalogue_Xml_FullGroups;
            var header = fullPriCat.Items[0] as SalesPriceCatalogue_Xml_FullHeader;

            if (group == null)
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]Can not deserialize the pri cat file. the second elemet is not Group");
            if(header == null)
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]Can not deserialize the pri cat file. the first elemet is not Header");

            if (group.Group == null || group.Group.Count() == 0)
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]the group is incorrect in the pri cat file");

            var realGroup = group.Group[0] as SalesPriceCatalogue_Xml_FullGroupsGroup;
            if (realGroup == null)
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]the groups group value is null");

            if(realGroup.Variants.Length == 0)
                throw new System.InvalidOperationException("[GetFPriCatDeserialized]the variants are not present.");

            
            return fullPriCat;
        }
    }
}
