﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LibApi
{
    
    public class DesAdv:IXPathExpression
    {
        public string Id { get; set; }

        public string PODate { get; set; }

        public List<POItem> POItems { get; set; }

        public string SellerReferenceNumber { get; set; }

        public double NoOfPackages { get; set; }

        #region IXPathExpression Members

        public  string GetXPathExpression()
        {
            return "ns0:DespatchAdvice";
        }

        #endregion
    }
    public class POItem : IXPathExpression
    {
        public const string POITEM_ID = "cbc:ID";
        public const string POITEM_PHY_ATTR = "cac:PhysicalAttribute";
        public const string POITEM_ATTR_ID = "cbc:AttributeID";
        public const string POITEM_ATTR_DESC = "cbc:Description";
        public const string SELLER_PARTY_NODE = "cac:SellerSupplierParty/cac:Party";
        public const string SELLER_PARTY_ID = "cac:PartyIdentification/cbc:ID";
        
        public string PoItem_Id { get; set; }

        public string PoItem_Description { get; set; }

        public double PoItem_Quantity { get; set; }


        #region IXPathExpression Members

        public string GetXPathExpression()
        {
            return "cac:DespatchLine/cac:Item/cac:AdditionalItemIdentification";
        }

        #endregion
    }

}


