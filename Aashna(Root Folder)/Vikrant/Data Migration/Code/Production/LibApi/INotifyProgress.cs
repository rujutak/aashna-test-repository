﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibApi
{
    public class Class1
    {
    }
    public class NotifyProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets Message
        /// </summary>
        public string Message { get; set; }

        public NotifyProgressEventArgs()
        {
            Message = string.Empty;
        }
    }

    public interface INotifyProgress
    {
        event EventHandler<NotifyProgressEventArgs> OnProgress;
    }

    
}
