// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_customization_ince_item.js
     Author: Mayur
     Company:AASHNA Cloudtech Pvt. Ltd
     Date:23/09/2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     
     
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function inve_item_scheduler_load(type){
    /*  On scheduled function:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     
     */
    //  LOCAL VARIABLES
    
    var searchresults;
    var total_quantity = 0;
    var temp = '';
    var flag = 0;
    var consumption = 0;
    //  SCHEDULED FUNCTION CODE BODY
    
    // Define the search filters
    
    var today = new Date();
    var fifteenDaysAgo = nlapiAddDays(new Date(), -15);
    
    var filters = new Array();
    var columns = new Array();
   
    filters[0] = new nlobjSearchFilter('trandate', null, 'onorafter', fifteenDaysAgo);
    filters[1] = new nlobjSearchFilter('trandate', null, 'onorbefore', today);
    
    
    // specify the record type and the saved search ID
    
    var searchresults = nlapiSearchRecord('invoice', 'customsearch_item_invoice', filters, columns)
    nlapiLogExecution('DEBUG', 'inventoryitem', 'searchresults =' + searchresults)
    if (searchresults != null)
	 {
        nlapiLogExecution('DEBUG', 'inventoryitem', 'searchresults Length_1=' + searchresults.length)
        
        for (var i = 0; i < searchresults.length; i++) {
            var result = searchresults[i];
            nlapiLogExecution('DEBUG', 'inventoryitem', 'searchresult Length_3=' + result)
            var columns = result.getAllColumns();
            nlapiLogExecution('DEBUG', 'inventoryitem', 'columns =' + columns)
            var columnLen = columns.length;
            nlapiLogExecution('DEBUG', 'inventoryitem', 'columnLen=' + columnLen)
            
            var column9 = columns[9];
            var column10 = columns[10];
            var column11 = columns[11];
            
            var value9 = result.getValue(column9);
            // nlapiLogExecution('DEBUG', 'inventoryitem', 'value9 =' + value9)
            var value10 = result.getValue(column10);
            //  nlapiLogExecution('DEBUG', 'inventoryitem', 'value10 =' + value10)
            var value11 = result.getValue(column11);
            //  nlapiLogExecution('DEBUG', 'inventoryitem', 'value11 =' + value11)
            
            for (var j = 0; j < searchresults.length; j++) {
                var result1 = searchresults[j];
                if (value11 == result1.getValue(column11)) {
                    var qty = result1.getValue(column10);
                    nlapiLogExecution('DEBUG', 'inventoryitem', 'qty =' + qty)
                    total_quantity = parseInt(total_quantity) + parseInt(qty);
                    nlapiLogExecution('DEBUG', 'inventoryitem', 'total_quantity=' + total_quantity)
                    flag = 1;
                    
                }
            }
            if (flag == 1) {
                nlapiLogExecution('DEBUG', 'inventoryitem', 'flag 1 taken--->>> ' + flag)
                var obj_item = nlapiLoadRecord('inventoryitem', value11);
                nlapiLogExecution('DEBUG', 'inventoryitem', 'value11 =' + value11)
                obj_item.setFieldValue('custitem_perdaycosumption', total_quantity);
                consumption = (parseFloat(consumption) + parseFloat(total_quantity)) / 15;
                obj_item.setFieldValue('custitem_last15dayssale', consumption.toFixed(2));
                nlapiLogExecution('DEBUG', 'inventoryitem', 'consumption=' + consumption)
                nlapiSubmitRecord(obj_item, true, true);
                total_quantity = 0;
            }
            
        }
    }
    
    
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
