// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_apm__validation_emp_data_change.js
	Author: Mayur Rajput
	Company: Aashna cloudtech 
	Date:04/09/2013
    Description: This script work on validation on employee data change.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function emp_date_validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY
if(name == 'custrecord_apm_edc_increment_applied_dt')
   {
   	
	//*************Date entered on increment applied date**********
	
   	var d_date = nlapiGetFieldValue('custrecord_apm_edc_increment_applied_dt')
	//alert('d_date-->' + d_date);
	var split_date = new Array();
	split_date = d_date.split('/')
	var i_month_val = split_date[0]
	//alert('i_month_val-->' + i_month_val);
	var i_date_val =split_date[1]
	//alert('d_date_val-->' + i_date_val);
	var i_year_val =split_date[2] 
	//alert('d_year_val------>' + i_year_val);
	
	if(i_date_val == 1)
	{
		return true;
	}
	
	else
	{
	   alert('Increment Applied Date Should be Start Date of the Month')
	   return false;	
	}
	
	
   } 

return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function emp_identification_fieldChanged(type, name)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */

     
    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY
	   if (name== 'custrecord_apm_edc_emp_id')
	   {
	   	   //  alert('type-->' + type);
           var emp_id = nlapiGetFieldValue('custrecord_apm_edc_emp_id');
		   
           var o_emp_rec = nlapiLoadRecord('employee',emp_id)
           var maritial_status = o_emp_rec.getFieldValue('maritalstatus')
		  // alert('maritial_status-->' + maritial_status);
		   var gender = o_emp_rec.getFieldValue('gender')
		  // alert('gender-->' + gender);
		   
           nlapiSetFieldValue('custrecord_apm_edc_martial_status',maritial_status,true, true)
		 
		   if(gender=='m'|| gender=='Male')
		   {
		   	nlapiSetFieldValue('custrecord_apm_edc_gender','1', true, true)
		   }
		   if (gender=='f'|| gender=='Female')
           {
		   	  nlapiSetFieldValue('custrecord_apm_edc_gender','2',true, true)
		   }
		   		   
	   }
	
       if (name == 'custrecord_apm_edc_identification') 
	   {
	   	var s_emp_identification = nlapiGetFieldValue('custrecord_apm_edc_identification');
	   	//alert('s_emp_identification-->' + s_emp_identification);
		
		var b_check = isValid(s_emp_identification);
		//alert('b_check-->' + b_check);
		
	   }
	   
	   
	   

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
function isValid(str) 
{
	//alert('IN FUNCTION-->');
    var iChars = "~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";

    for (var i = 0; i < str.length; i++)
	   {
       if (iChars.indexOf(str.charAt(i)) != -1)
	   {
           alert ("Employee Identification has special characters ~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n");
		   nlapiSetFieldValue('custrecord_apm_edc_identification','');
		   return false;
       }
	  
    }
    return true;
}