// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_apm_setup_master_valid.js
	Author: Kapil
	Company: Aashna cloudtech 
	Date:10/09/2013
    Description: This script work on validation on multiple records.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

var operation_type;
	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES

    //alert('type-->>'+type);
	operation_type=type;
	var s_rounded_type = nlapiGetFieldValue('custrecord_apm_pc_rounded_off_type')
	if(s_rounded_type == 3 && operation_type == 'edit') 
	{
		nlapiDisableField('custrecord_apm_pc_rounded_off_value',true)
	}
    //  PAGE INIT CODE BODY
	

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function pay_component_valid_saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	
//***********validation for pay component master

var s_record_type = nlapiGetRecordType()
if(s_record_type == 'customrecord_apm_payroll_component')
{
	var s_rounded_type = nlapiGetFieldValue('custrecord_apm_pc_rounded_off_type')
	var s_rounded_value = nlapiGetFieldValue('custrecord_apm_pc_rounded_off_value')
	var i_calculation_type = nlapiGetFieldValue('custrecord_apm_pc_calculation_type')
	
    if((s_rounded_type == '') && (i_calculation_type ==3 || i_calculation_type ==2))
	{
		alert('You are not allowed to save the record as rounded off type can not be left blank');
		return false;
	}

    if((s_rounded_type == 3) && (i_calculation_type ==3 || i_calculation_type ==2) && s_rounded_value == '')
	{
		//alert('You are not allowed to save the record as rounded off type can not be left blank');
		return true;
	}
	if((s_rounded_value == '' ) && (i_calculation_type ==3 || i_calculation_type ==2))
	{
		alert('You are not allowed to save the record as rounded off value can not be left blank');
		return false;
	}
	
}
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function validation_code_desc_fieldChanged(type, name, linenum)
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	//***********Common validation for multiple records
	var s_rec_type = nlapiGetRecordType();
	
	//if ((name == 'name') && (s_rec_type !='customrecord_apm_cost_center' || s_rec_type !='customrecord_apm_grade')) 
	
	
	//********validation for cost center master description field
	
	if ((name == 'name') && (s_rec_type == 'customrecord_apm_cost_center')) 
	{
		var s_desc = nlapiGetFieldValue('name');
		//alert('s_desig_code-->' + s_desig_code);
		
		var b_check = isValid_field_entry(s_desc, 'name');
		//alert('b_check-->' + b_check);
	}
	
	//********validation for grade master description field
	
	else 
		if ((name == 'name') && (s_rec_type == 'customrecord_apm_grade')) 
		{
			var s_desc = nlapiGetFieldValue('name');
			//alert('s_desig_code-->' + s_desig_code);
			
			var b_check = isValid_field_entry(s_desc, 'name');
		//alert('b_check-->' + b_check);
		}
		//********validation for all other master description field
		else {
			var s_desig_desc = nlapiGetFieldValue('name');
			//alert('s_desig_code-->' + s_desig_code);
			
			var b_check = isValid_desc(s_desig_desc);
		//alert('b_check-->' + b_check);
		}
	
	
	//***********validation for designation master
	
	if (name == 'custrecord_apm_desig_code') 
	{
		    var s_desig_code = nlapiGetFieldValue('custrecord_apm_desig_code');
		
		    var b_check_code = isValid_code(s_desig_code, 'custrecord_apm_desig_code');
	}
	//***********validation for cost center master
	
	else if (name == 'custrecord_apm_cost_center_code') 
	{
			var s_cost_center_code = nlapiGetFieldValue('custrecord_apm_cost_center_code');
			
			var b_check_code = isValid_field_entry(s_cost_center_code, 'custrecord_apm_cost_center_code');
	}
		
	//***********validation for payroll component master
		
	else if (name == 'custrecord_apm_pc_name') 
	{
			var s_pay_component_code = nlapiGetFieldValue('custrecord_apm_pc_name');
				
			var b_check_code = isValid_code(s_pay_component_code, 'custrecord_apm_pc_name');
	}
			
	//***********validation for process group master
			
	else if (name == 'custrecord_apm_pro_grp_short_name') 
	{
			var s_process_grp_code = nlapiGetFieldValue('custrecord_apm_pro_grp_short_name');
					
			var b_check_code = isValid_code(s_process_grp_code, 'custrecord_apm_pro_grp_short_name');
	}
				
	//***********validation for grade master
				
	else if (name == 'custrecord_apm_grade_code') 
	{
			var s_grade_code = nlapiGetFieldValue('custrecord_apm_grade_code');
						
			var b_check_code = isValid_field_entry(s_grade_code, 'custrecord_apm_grade_code');
	}
					
	//***********validation for gender master
					
	else if (name == 'custrecord_apm_gender_code') 
	{
			var s_gender_code = nlapiGetFieldValue('custrecord_apm_gender_code');
							
			var b_check_code = isValid_code(s_gender_code, 'custrecord_apm_gender_code');
	}
						
	//***********validation for leave payroll master
						
	else if (name == 'custrecord_apm_lev_payroll_short_name') 
	{
			var s_leave_code = nlapiGetFieldValue('custrecord_apm_lev_payroll_short_name');
								
			var b_check_code = isValid_code(s_leave_code, 'custrecord_apm_lev_payroll_short_name');
	}
							
	//***********validation for global parameter master
							
	else if (name == 'custrecord_apm_glo_para_value') 
	{
			var s_global_para_value = nlapiGetFieldValue('custrecord_apm_glo_para_value');
									
			var b_check_code = isValid_field_entry(s_global_para_value, 'custrecord_apm_glo_para_value');
	}
								
	else if (name == 'custrecord_apm_glo_para_desc') 
	{
			var s_global_para = nlapiGetFieldValue('custrecord_apm_glo_para_desc');
						
			var b_check_code = isValid_code(s_global_para, 'custrecord_apm_glo_para_desc');
	}
	
	
	//***********validation for bank master
	
	if (s_rec_type == 'customrecord_apm_bank') 
	{
		if (name == 'custrecord_apm_bank_code') 
		{
			var s_bank_code = nlapiGetFieldValue('custrecord_apm_bank_code');
			
			var b_check_code = isValid_code(s_bank_code, 'custrecord_apm_bank_code');
		}
		
		if (name == 'custrecord_amp_bank_br') 
		{
			var s_bank_branch = nlapiGetFieldValue('custrecord_amp_bank_br');
			
			var b_check_code = isValid_code(s_bank_branch, 'custrecord_amp_bank_br');
		}
		
		if (name == 'custrecord_apm_bank_acc_no') 
		{
			var s_bank_acc_no = nlapiGetFieldValue('custrecord_apm_bank_acc_no');
			
			var b_check_code = isValid_field_entry(s_bank_acc_no, 'custrecord_apm_bank_acc_no');
		}
		if (name == 'custrecord_apm_bank_ifsc_code') 
		{
			var s_bank_ifsc_code = nlapiGetFieldValue('custrecord_apm_bank_ifsc_code');
			
			var b_check_code = isValid_field_entry(s_bank_ifsc_code, 'custrecord_apm_bank_ifsc_code');
		}
		
	}
	
	//***********validation for payroll component master
	
	if (s_rec_type == 'customrecord_apm_payroll_component') 
	{
		if (name == 'custrecord_apm_pc_calculation_type') 
		{
			var s_calculat_type = nlapiGetFieldValue('custrecord_apm_pc_calculation_type')
			if (s_calculat_type == 1) 
			{
				nlapiDisableField('custrecord_apm_pc_rounded_off_type', true)
				nlapiDisableField('custrecord_apm_pc_rounded_off_value', true)
			}
			else 
			{
				nlapiDisableField('custrecord_apm_pc_rounded_off_type', false)
				nlapiDisableField('custrecord_apm_pc_rounded_off_value', false)
			}
		}
		if (name == 'custrecord_apm_pc_rounded_off_type') 
		{
			var s_rounded_type = nlapiGetFieldValue('custrecord_apm_pc_rounded_off_type')
			if (s_rounded_type == 3) 
			{
				nlapiDisableField('custrecord_apm_pc_rounded_off_value', true)
			}
			else 
			{
				nlapiDisableField('custrecord_apm_pc_rounded_off_value', false)
			}
		}
	}
	
	//***********validation for process group  master wage period from date and to date
	
	
	if (s_rec_type == 'customrecord_apm_process_group') 
	{
		if (name == 'custrecord_apm_pro_grp_wage_frm_dt' && name == 'custrecord_apm_pro_grp_wage_to_dt' && operation_type == 'edit') 
		{
			nlapiDisableField(' custrecord_apm_pro_grp_atten_frm_dt', true);
			nlapiDisableField('custrecord_apm_pro_grp_atten_to_dt', true);
		}
		if (name == 'custrecord_apm_pro_grp_wage_frm_dt') 
		{
			//*************Date entered on wage period from date**********
			
			var d_date = nlapiGetFieldValue('custrecord_apm_pro_grp_wage_frm_dt')
			
			var splitdt = d_date.split('/')
			
			var curmonth = splitdt[0];
			var curdate=  splitdt[1];
			var curyer = splitdt[2];
					
	     	var date = parseInt(curdate);
		
            
			if (date == 1) 
			{
		       //nlapiDisableField('custrecord_apm_pro_grp_wage_frm_dt', true);
				return true;
			}
			else 
			{
				alert('You are not allowed to enter wage from date other then start date of month')
				nlapiSetFieldValue('custrecord_apm_pro_grp_wage_frm_dt','',false,false);
				//nlapiDisableField('custrecord_apm_pro_grp_wage_frm_dt', true);
				return false;
			}
			
		}
		else 
			if (name == 'custrecord_apm_pro_grp_wage_to_dt') 
			{
				//*************Date entered on wage period TO date**********
				
				var dd_date = nlapiGetFieldValue('custrecord_apm_pro_grp_wage_to_dt')
				
		
				var splitdate = dd_date.split('/')
				var curmonth_to = splitdate[0];
		     	var curdate_to=  splitdate[1];
			    var curyer_to = splitdate[2];
			
			    var frm_wg_dt = nlapiGetFieldValue('custrecord_apm_pro_grp_wage_frm_dt')
				var splitwgdt = frm_wg_dt.split('/')
				var curmonth_wg = splitwgdt[0];
				var curdate_wg=  splitwgdt[1];
				var curyer_wg = splitwgdt[2];
		
				var month = parseInt(curmonth_wg);		
				var sss= daysInMonth(curmonth_wg,curyer_wg)
				var enddt=parseInt(sss);
		
				var intcurdt = parseInt(curdate_to);
				var intcurmth= parseInt(curmonth_to);
		
				if (intcurdt==enddt && intcurmth==month )
				{
				  //nlapiDisableField('custrecord_apm_pro_grp_wage_to_dt', true);
					return true;
				}
				else 
				{
					alert('You are not allowed to enter wage to date other then End date of month')
					nlapiSetFieldValue('custrecord_apm_pro_grp_wage_to_dt','',false,false);
				//	nlapiDisableField('custrecord_apm_pro_grp_wage_to_dt', true);
					return false
				}
				
			}
		
	}
	
	//***********validation for process group  master atttendance  period from date and to date
	
	if (s_rec_type == 'customrecord_apm_process_group') 
	{
		if (name == 'custrecord_apm_pro_grp_atten_frm_dt' && name == 'custrecord_apm_pro_grp_atten_to_dt' && operation_type == 'edit') 
		{
			nlapiDisableField(' custrecord_apm_pro_grp_atten_frm_dt', true);
			nlapiDisableField('custrecord_apm_pro_grp_atten_to_dt', true);
		}
		
		if (name == 'custrecord_apm_pro_grp_atten_frm_dt') 
		{
			var att_peri_frm = nlapiGetFieldValue('custrecord_apm_pro_grp_atten_frm_dt')
			var weg_peri_frm = nlapiGetFieldValue('custrecord_apm_pro_grp_wage_frm_dt')
			
		//	alert('att_peri_frm--->>>' + att_peri_frm);
		//	alert('weg_peri_frm--->>>' + weg_peri_frm);
			
			att_peri_frm=Date.parse(att_peri_frm);
			weg_peri_frm=Date.parse(weg_peri_frm);
				
			if (att_peri_frm < weg_peri_frm || att_peri_frm == weg_peri_frm)
			{
				//nlapiSetFieldValue('custrecord_apm_pro_grp_atten_frm_dt','')
				return true;
			}
			else 
			{
				alert('You are not allowed to enter attendance from date greater than or not equal to Wage period from date')
				nlapiSetFieldValue('custrecord_apm_pro_grp_atten_frm_dt','',false,false);
				return false;
			}
			
		}
		else 
		{
			if (name == 'custrecord_apm_pro_grp_atten_to_dt') 
			{
				var att_peri_to = nlapiGetFieldValue('custrecord_apm_pro_grp_atten_to_dt')
				var weg_peri_frm_dt =nlapiGetFieldValue('custrecord_apm_pro_grp_wage_frm_dt')
				
				var att_peri_frm_dt = nlapiGetFieldValue('custrecord_apm_pro_grp_atten_frm_dt')
			//	alert('att_peri_to--->>>' + att_peri_to);
			// alert('weg_peri_to--->>>' + weg_peri_to);
				
				att_peri_to=Date.parse(att_peri_to);
			    weg_peri_frm_dt=Date.parse(weg_peri_frm_dt);
				
				//att_peri_frm_dt=Date.parse(att_peri_frm_dt);
				
				if (att_peri_to > weg_peri_frm_dt )   //|| att_peri_to < Date.parse(att_peri_frm_dt)) 
				{
		
						return false;
				}
				else 
				{
					alert('You are not allowed to enter attendance to date less then or not equal to Wage period from date');
					nlapiSetFieldValue('custrecord_apm_pro_grp_atten_to_dt','',false,false)
					return true;
				}
				
			}
		}
	}

}
// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================

// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================

// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================

// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================

// BEGIN FUNCTION ===================================================

// END FUNCTION =====================================================
//*******function for desscription/name field
function isValid_desc(str) 
{
	//alert('IN FUNCTION-->');
    var iChars = "@~`!#$%^&*+=-[]()\\\';,/{}|\":<>?.0123456789";

    for (var i = 0; i < str.length; i++)
	   {
       if (iChars.indexOf(str.charAt(i)) != -1)
	   {
           //alert ("Employee Identification has special characters ~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n");
		   alert('You are not allowed to enter numbers or special characters on a text field')
		   nlapiSetFieldValue('name','')
		   return false;
       }
	  
    }
    return true;
}

//*******function for short name or code field

function isValid_code(str,fieldId) 
{
	//alert('IN FUNCTION-->');
    var iChars = "@~`!#$%^&*+=-[]()\\\';,/{}|\":<>?.0123456789";

    for (var i = 0; i < str.length; i++)
	   {
       if (iChars.indexOf(str.charAt(i)) != -1)
	   {
           //alert ("Employee Identification has special characters ~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n");
		   alert('You are not allowed to enter numbers or special characters on a text field')
		   nlapiSetFieldValue(fieldId,'')
		   return false;
       }
	  
    }
    return true;
}

//*********function for grade,global parameter & cost center master
function isValid_field_entry(str,fieldId) 
{
	//alert('IN FUNCTION-->');
    var iChars = "@~`!#$%^&*+=-[]()\\\';,/{}|\":<>?.";

    for (var i = 0; i < str.length; i++)
	   {
       if (iChars.indexOf(str.charAt(i)) != -1)
	   {
           //alert ("Employee Identification has special characters ~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n");
		   alert('You are not allowed to enter special characters on a text field')
		   nlapiSetFieldValue(fieldId,'')
		   return false;
       }
	  
    }
    return true;
}

function daysInMonth(month,year)
 {
    return new Date(year, month, 0).getDate();
}

	
	

