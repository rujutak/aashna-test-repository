//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:CLI_validation_comp_details.js
	 Author:Kapil
	 Company:Aashna
	 Date:30-08-13
	 Version:0.1
	 Description:This script will work on validation1 on compensation details
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 PAGE INIT
	 pageInit(type)
	 
	 SAVE RECORD
	 SaveRecord()
	 
	 FIELD CHANGED
	 fieldchanged(type,name,linenum)
	 
	 VALIDATE FIELD
	 validatefield(type,name,linenum)
	 
	 POSTSOURCING
	 postsourcing(type,name)
	 
	 LINE INIT
	 lineinit(type)
	 
	 VALIDATE LINE
	 validateline(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
		
		
	 //BEGIN PAGE INIT
		
function pageInit(type)
{
	/* On Page Init:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN PAGE INIT CODE BODY
		
		
}

     //END PAGE INIT
		
	 //BEGIN SAVE RECORD
		
function SaveRecord()
{
	/* On Save Record:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN SAVE RECORD CODE BODY
	
	
}

     //END SAVE RECORD
	 
	 
	 
	 //BEGIN FIELD CHANGED	
function fieldChanged_validation_comp_details(type, name, linenum)
{
	/* On FIELD CHANGED:
	 
	 PURPOSE:
	 
	 FIELDS USED:
	 --FIELD NAME--           --ID--               --LINEITEM--
	 */
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN FIELD CHANGED CODE BODY
	if (name == 'custrecord_apm_comp_rec_details_monthly') 
	{
	   var s_payroll_component = nlapiGetFieldValue('custrecord_apm_comp_rec_details_pay_comp');
	   alert('s_payroll_component-->'+s_payroll_component);
	   if(s_payroll_component != null && s_payroll_component!='' && s_payroll_component!= 'undefined')
	   {
	   	var o_pay_comp = nlapiLoadRecord('customrecord_apm_payroll_component',s_payroll_component);
		alert('o_pay_comp-->'+o_pay_comp);
		
		var ch_CTC = o_pay_comp.getFieldValue('custrecord_apm_pc_ctc')
		alert('ch_CTC-->'+ch_CTC);
		
		var ch_benefits = o_pay_comp.getFieldValue('custrecord_apm_pc_benefits')
		alert('ch_benefits-->'+ch_benefits);
		
		var s_pay_freq = o_pay_comp.getFieldValue('custrecord_apm_pc_pay_frequency')
		alert('s_pay_freq-->'+s_pay_freq);
		
		var s_calc_type = o_pay_comp.getFieldValue('custrecord_apm_pc_calculation_type')
		alert('s_calc_type-->'+s_calc_type);
		
		//************Validation for Section A1*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 1 && s_calc_type == 1)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			//alert('s_month-->'+i_month);
			
			var i_annual = i_month*12;
			nlapiSetFieldValue('custrecord_apm_comp_rec_details_annually',i_annual);
			
			nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
		}
		
		//************Validation for Section A2*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 1 && s_calc_type == 3)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			//alert('s_month-->'+i_month);
			nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
			
			var i_annual = i_month*12;
			nlapiSetFieldValue('custrecord_apm_comp_rec_details_annually',i_annual);
			
			nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
		}
		
		//************Validation for Section B1*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'T' && s_pay_freq == 1 && s_calc_type == 1)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			alert('s_month-->'+i_month);
			
			var i_annual = i_month*12;
			nlapiSetFieldValue('custrecord_apm_comp_rec_details_annually',i_annual);
			
			nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
		}
		
		//************Validation for Section B2*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'T' && s_pay_freq == 1 && s_calc_type == 3)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			//alert('s_month-->'+i_month);
			nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
			
			var i_annual = i_month*12;
			nlapiSetFieldValue('custrecord_apm_comp_rec_details_annually',i_annual);
			
			nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
		}
		
		
		
		/*
//************Validation for Section C1*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 2 && s_calc_type == 3)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			//alert('s_month-->'+i_month);
			nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
			
			nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
		}
		
		//************Validation for Section C2*****************
		
		if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 2 && s_calc_type == 1)
		{
			var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
			//alert('s_month-->'+i_month);
			nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
			
		}
*/
	   }
	}
	
	if (name == 'custrecord_apm_comp_rec_details_pay_comp' ) 
	//alert('name-->' + name);
	{
		var s_payroll_component = nlapiGetFieldValue('custrecord_apm_comp_rec_details_pay_comp');
	   //alert('s_payroll_component-->'+s_payroll_component);
	   if (s_payroll_component != null && s_payroll_component != '' && s_payroll_component != 'undefined') 
	   {
	   	var o_pay_comp = nlapiLoadRecord('customrecord_apm_payroll_component', s_payroll_component);
	   	//alert('o_pay_comp-->' + o_pay_comp);
	   	
	   	var ch_CTC = o_pay_comp.getFieldValue('custrecord_apm_pc_ctc')
	   	//alert('ch_CTC-->' + ch_CTC);
	   	
	   	var ch_benefits = o_pay_comp.getFieldValue('custrecord_apm_pc_benefits')
	   	//alert('ch_benefits-->' + ch_benefits);
	   	
	   	var s_pay_freq = o_pay_comp.getFieldValue('custrecord_apm_pc_pay_frequency')
	   	//alert('s_pay_freq-->' + s_pay_freq);
	   	
	   	var s_calc_type = o_pay_comp.getFieldValue('custrecord_apm_pc_calculation_type')
	   	//alert('s_calc_type-->' + s_calc_type);
	   
	   		//************Validation for Section B3*****************
						
			if (ch_CTC == 'T' && ch_benefits == 'T' && s_pay_freq == 2 && s_calc_type == 1) 
			{
				//alert('name in if-->' + name);
				var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
				//alert('s_month-->'+i_month);
				nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
				
				nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
			}
			
			//************Validation for Section B4*****************
		
			if(ch_CTC == 'T' && ch_benefits == 'T' && s_pay_freq == 2 && s_calc_type == 3)
			{
				var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
				//alert('s_month-->'+i_month);
				nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
				
				nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
			}
			
			//************Validation for Section C1*****************
		
			if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 2 && s_calc_type == 3)
			{
				var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
				//alert('s_month-->'+i_month);
				nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
				
				nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
			}	
			
			//************Validation for Section C2*****************
		
			if(ch_CTC == 'T' && ch_benefits == 'F' && s_pay_freq == 2 && s_calc_type == 1)
			{
				var i_month = nlapiGetFieldValue('custrecord_apm_comp_rec_details_monthly')
				//alert('s_month-->'+i_month);
				nlapiDisableField('custrecord_apm_comp_rec_details_monthly', true);
				
				//nlapiDisableField('custrecord_apm_comp_rec_details_annually', true);
			}		
		}
	}
	
	
}	
	//END FIELD CHANGED
	
	
	
	//BEGIN VALIDATE FIELD

	/* On Validate Field:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN VALIDATE FIELD CODE BODY
	
	//END VALIDATE FIELD
	
	
	//BEGIN POST SOURCING

	/* On Post Sourcing:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN POST SOURCING CODE BODY
	
	//END POST SOURCING
	
		
	 //BEGIN LINE INIT 
		
function lineinit(type)
{
	/* On Line Init:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN LINE INIT CODE BODY
	
	
}

     //END LINE INIT
	   
	   
	   
	   //BEGIN VALIDATE LINE

	/* On Validate Line:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN VALIDATE LINE CODE BODY
	
	//END VALIDATE LINE
	   
