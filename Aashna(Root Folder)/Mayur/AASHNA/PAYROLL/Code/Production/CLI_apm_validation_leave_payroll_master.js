//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
	/*
	 Script Name:CLI_apm_validation_leave_payroll_master.js
	 Author:Mayur
	 Company:Aashna
	 Date:10-09-13
	 Version:0.1
	 Description:This script will work on validation1 on leave payroll master
	 
	 Script Modification Log:
	 -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
	 
	 
	 Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
	 inthe appropriate functions headers and code blocks.
	 
	 //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
	 
	 PAGE INIT
	 pageInit(type)
	 
	 SAVE RECORD
	 SaveRecord()
	 
	 FIELD CHANGED
	 fieldchanged(type,name,linenum)
	 
	 VALIDATE FIELD
	 validatefield(type,name,linenum)
	 
	 POSTSOURCING
	 postsourcing(type,name)
	 
	 LINE INIT
	 lineinit(type)
	 
	 VALIDATE LINE
	 validateline(type)
	 
	 //END MAIN FUNCTIONS BLOCK--------------------------------------
	 
	 
	 
	 //BEGIN SUB FUNCTIONS BLOCK--------------------------------
	 
	 //The following sub-functions are called by the above main functions.
	 
	 //END SUB FUNCTIONS BLOCK----------------------------------
	 
	 //END SCRIPT DESCRIPTION BLOCK
	 */
}		
     //BEGIN SCRIPT UPDATION BLOCK------------------------------
	 
	 //END SCRIPT UPDATION SCRIPT-------------------------------
	 
	 
	 //BEGIN GLOBAL VARIABLE BLOCK------------------------------
		
     //Initialize any global variables,in particular debugging variables.....
		
     //END GLOBAL VARIABLE BLOCK--------------------------------
		
		
	 //BEGIN PAGE INIT
		
function lp_pageInit(type)
{
	/* On Page Init:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN PAGE INIT CODE BODY
		
		/*var s_pay_encash = nlapiGetFieldValue('custrecord_apm_lev_payroll_encashable');
		if (s_pay_encash == 'F') 
		{
			nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
			nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
		}*/
		
}

     //END PAGE INIT
		
	 //BEGIN SAVE RECORD
		
function lp_SaveRecord()
{
	/* On Save Record:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	var p_pay_encash;
	var p_fullpaid ;
	var p_unpaid;
	
	//END LOCAL VARIABLES
	
	//BEGIN SAVE RECORD CODE BODY
	
	     p_pay_encash = nlapiGetFieldValue('custrecord_apm_lev_payroll_encashable');
		 p_fullpaid=nlapiGetFieldValue('custrecord_apm_lev_payroll_fullpaid');
		 p_unpaid = nlapiGetFieldValue('custrecord_apm_lev_payroll_unpaid');
		// alert('p_fullpaid-->>'+p_fullpaid)
		// alert('p_unpaid-->>'+p_unpaid)
		
		if (p_pay_encash == 'F') 
		{
			alert('Please select the Encashable Value')
			return false;
		}
		
	    		
		if (p_fullpaid == 'F' && p_unpaid == 'F') 
		{
			alert('Please select the Full paid Or Unpaid Value');
			nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', false);
		    nlapiDisableField('custrecord_apm_lev_payroll_unpaid', false);
			return false;
		}
		if (p_fullpaid == 'T' && p_unpaid == 'F')
			 {
			   //	alert('Please selected the full paid ')
				//nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
				nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
				return true;
			}
		if (p_fullpaid == 'F' && p_unpaid == 'T') 
			{
				//alert('Please selected the Unpaid Value')
				nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
			    //nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
				return true;
			}
		
		
		
}
	
	//END SAVE RECORD
	 
	 //BEGIN FIELD CHANGED	
function lp_fieldChanged_validation(type, name)
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	var s_pay_encash;
	var fullpaid ;
	var unpaid;
	
	//  FIELD CHANGED CODE BODY
	
	if (name == 'custrecord_apm_lev_payroll_encashable'||name=='name'||name=='custrecord_apm_lev_payroll_short_name') 
	{
	    s_pay_encash = nlapiGetFieldValue('custrecord_apm_lev_payroll_encashable');
		//alert('s_pay_encash-->' + s_pay_encash);
		
		nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', false);
		nlapiDisableField('custrecord_apm_lev_payroll_unpaid', false);
		
		if (s_pay_encash == 'F')
		 {
			nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
			nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
		}
	}
	else
	 {
	    fullpaid = nlapiGetFieldValue('custrecord_apm_lev_payroll_fullpaid')
		//alert('fullpaid-->' + fullpaid);
		
		unpaid = nlapiGetFieldValue('custrecord_apm_lev_payroll_unpaid')
		//alert('unpaid-->' + unpaid);
		
		if (fullpaid == 'F' && unpaid == 'F') 
		{
			alert('Please select the full paid Or Unpaid Value')
			
			nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', false);
		    nlapiDisableField('custrecord_apm_lev_payroll_unpaid', false);
			
			return false;
		}
		
	}
		
		if (name == 'custrecord_apm_lev_payroll_fullpaid') 
		{
			if (fullpaid == 'T' && unpaid == 'F')
			 {
			   //	alert('Please selected the full paid ')
				//nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
				nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
				return true;
			}
		}
		if (name == 'custrecord_apm_lev_payroll_unpaid') 
		{
			if (fullpaid == 'F' && unpaid == 'T') 
			{
				//alert('Please selected the Unpaid Value')
				nlapiDisableField('custrecord_apm_lev_payroll_fullpaid', true);
			    //nlapiDisableField('custrecord_apm_lev_payroll_unpaid', true);
				return true;
			}
		}
		
		
	
	
}
// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

		
function lineinit(type)
{
	/* On Line Init:
	      
	      PURPOSE:
	             
	             FIELDS USED:
	             --FIELD NAME--            --ID--                --LINEITEM--
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN LINE INIT CODE BODY
	
	
}

     //END LINE INIT
	   
	   
	   
	   //BEGIN VALIDATE LINE

	/* On Validate Line:
	   
	      PURPOSE:
	         
	             FIELDS USED:
	             --FIELD NAME--           --ID--               --LINEITEM--	             
	 */
	
	//BEGIN LOCAL VARIABLES
	
	//END LOCAL VARIABLES
	
	//BEGIN VALIDATE LINE CODE BODY
	
	//END VALIDATE LINE
	   
