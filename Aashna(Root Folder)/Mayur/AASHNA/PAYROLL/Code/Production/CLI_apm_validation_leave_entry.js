// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: CLI_apm__validation_leave_entry.js
     Author: Kapil
     Company: Aashna cloudtech
     Date:05/09/2013
     Description: This script work on validation on Leave Entry Pay date.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     30-09-13				Kapil							Dipika						"Wage period end date" is set to "wage to" date on Pay process group master.
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function emp_leave_entry_pageInit(type){
    /*  On page init:
     - PURPOSE
     FIELDS USED:
     --Field Name--				--ID--			--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    //  PAGE INIT CODE BODY
    
    //  alert('type-->'+type)
    if (type == 'create') {
    
        //	alert('type11111-->'+type)
        var i_emp_id = nlapiGetFieldValue('custrecord_apm_le_emp_name')
        //	alert('i_emp_id-->' + i_emp_id)
        if (i_emp_id != null && i_emp_id != '' && i_emp_id != 'undefined') {
            var search_pay_grp = get_paygroup(i_emp_id);
            //	alert('search_pay_grp-->' + search_pay_grp)
            
            var search_wageend_date = get_wagedetails(search_pay_grp)
            //	alert('search_wageend_date-->' + search_wageend_date)
            var w_Date = search_wageend_date.toString().split('#');
            //	alert('w_Date-->' + w_Date)
            var end_date = w_Date[0];
            //	alert('end_date-->' + end_date)
            var start_date = w_Date[1];
            //	alert('start_date-->' + start_date)
            if (search_wageend_date != null && search_wageend_date != 'undefined' && search_wageend_date != '') {
                nlapiSetFieldValue('custrecord_apm_le_pay_dt', search_wageend_date, false, false);
                nlapiDisableField('custrecord_apm_le_pay_dt', true);
                
            }
            
        }
    }
    
    
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord(){
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SAVE RECORD CODE BODY
    
    
    return true;
    
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum){

    /*  On validate field:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  VALIDATE FIELD CODE BODY
    
    
    return true;
    
}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function emp_leave_entry_valid_fieldChanged(type, name, linenum){
    /*  On field changed:
     - PURPOSE
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  FIELD CHANGED CODE BODY
    //
    try {
        //if(name == 'custrecord_apm_le_pay_dt')
        if (name == 'custrecord_apm_le_emp_name') {
            //alert('name-->'+name)
            var i_emp_id = nlapiGetFieldValue('custrecord_apm_le_emp_name')
            //alert('i_emp_id-->' + i_emp_id)
            if (i_emp_id != null && i_emp_id != '' && i_emp_id != 'undefined') {
                var search_pay_grp = get_paygroup(i_emp_id);
                //alert('search_pay_grp-->' + search_pay_grp)
                
                var search_wageend_date = get_wagedetails(search_pay_grp)
                //alert('search_wageend_date-->' + search_wageend_date)
                var w_Date = search_wageend_date.toString().split('#');
                var end_date = w_Date[0];
                var start_date = w_Date[1];
                if (search_wageend_date != null && search_wageend_date != 'undefined' && search_wageend_date != '') {
                    nlapiSetFieldValue('custrecord_apm_le_pay_dt', search_wageend_date, false, false);
                    nlapiDisableField('custrecord_apm_le_pay_dt', true);
                    
                    var o_pay_grp = nlapiLoadRecord('customrecord_apm_process_group', search_pay_grp);
                    //alert('o_pay_grp-->'+o_pay_grp)
                    
                    o_pay_grp.setFieldValue('custrecord_apm_pro_grp_wage_to_dt', end_date, false, false);
                    o_pay_grp.setFieldValue('custrecord_apm_pro_grp_wage_frm_dt', start_date, false, false);
                    var o_submit_pay_grp = nlapiSubmitRecord(o_pay_grp, false, false)
                    //alert('o_submit_pay_grp-->'+o_submit_pay_grp)
                
                }
                
            }
        }
    } 
    catch (e) {
        nlapiLogExecution('DEBUG', 'Error details', 'Details-->' + e.getDetails())
    }
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name){

    /*  On post sourcing:
    
     - PURPOSE
    
     FIELDS USED:
    
     --Field Name--			--ID--		--Line Item Name--
    
     */
    
    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type){

    /*  On Line Init:
    
     - PURPOSE
    
     FIELDS USED:
    
     --Field Name--			--ID--		--Line Item Name--
    
     */
    
    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  VALIDATE LINE CODE BODY
    
    
    return true;
    
}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type){

    /*  On recalc:
    
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
    
     FIELDS USED:
    
     --Field Name--				--ID--
    
     */
    
    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================

//************Function used to get employee's pay group***************

function get_paygroup(employee_id){
    //alert('IN Fun1')
    var filters = new Array();
    var columns = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_apm_edc_emp_id', null, 'is', employee_id)
    columns[0] = new nlobjSearchColumn('custrecord_apm_edc_emp_pay_process_group')
    
    var result = nlapiSearchRecord('customrecord_apm_emp_data_change', null, filters, columns)
    //alert('result--'+result)
    if (result != null) {
        var paygroup = result[0].getValue('custrecord_apm_edc_emp_pay_process_group')
        //alert('paygroup--'+paygroup)
    }
    return paygroup;
}

//************Function used to get employee's wage period end date***************

function get_wagedetails(search_pay_grp){
    //alert('IN Fun2')
    var filters = new Array();
    var columns = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_apm_wage_per_detail_pay_grp', null, 'is', search_pay_grp)
    columns[0] = new nlobjSearchColumn('custrecord_apm_wage_per_detail_end_dt')
    columns[1] = new nlobjSearchColumn('custrecord_apm_wage_per_detail_start_dt')
    columns[2] = new nlobjSearchColumn('internalid')
    //columns[1].setSort(true);
    
    var result = nlapiSearchRecord('customrecord_apm_wage_period_detail', null, filters, columns)
    //alert('result2--'+result)
    if (result != null) {
        var end_date = result[0].getValue('custrecord_apm_wage_per_detail_end_dt')
        //alert('end_date--'+end_date);
        var start_date = result[0].getValue('custrecord_apm_wage_per_detail_start_dt')
        //alert('start_date--'+start_date);
    }
    return end_date + "#" + start_date;
}
