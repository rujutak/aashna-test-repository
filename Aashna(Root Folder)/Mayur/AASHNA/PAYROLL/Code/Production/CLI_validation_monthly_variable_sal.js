//BEGIN SCRIPT DESCRIPTION BLOCK---------------------------------
{
    /*
     Script Name:CLI_validation_monthly_variable_sal.js
     Author:Mayur
     Company:Aashna
     Date:02-09-13
     Version:0.1
     Description:This script will work on validation2 on Monthly variable Salary.
     
     Script Modification Log:
     -- Date --                 -- Modified By --                 -- Requested By --                    -- Description --
     26-09-13						Kapil							Team payroll						Paygroup will be sourced on the basis of employee selection
     
     Below is a summary of the process controls enforced by this script file.The control logic is described more fully below,
     inthe appropriate functions headers and code blocks.
     
     //BEGIN MAIN FUNCTIONS BLOCK-----------------------------
     
     PAGE INIT
     pageInit(type)
     
     SAVE RECORD
     SaveRecord()
     
     FIELD CHANGED
     fieldchanged(type,name,linenum)
     
     VALIDATE FIELD
     validatefield(type,name,linenum)
     
     POSTSOURCING
     postsourcing(type,name)
     
     LINE INIT
     lineinit(type)
     
     VALIDATE LINE
     validateline(type)
     
     //END MAIN FUNCTIONS BLOCK--------------------------------------
     
     
     
     //BEGIN SUB FUNCTIONS BLOCK--------------------------------
     
     //The following sub-functions are called by the above main functions.
     
     //END SUB FUNCTIONS BLOCK----------------------------------
     
     //END SCRIPT DESCRIPTION BLOCK
     */
}
//BEGIN SCRIPT UPDATION BLOCK------------------------------

//END SCRIPT UPDATION SCRIPT-------------------------------


//BEGIN GLOBAL VARIABLE BLOCK------------------------------

//Initialize any global variables,in particular debugging variables.....

//END GLOBAL VARIABLE BLOCK--------------------------------


//BEGIN PAGE INIT

function pageInit_monthly_variable(type)
{
    /* On Page Init:
    
     
    
     PURPOSE:
    
     
    
     FIELDS USED:
    
     --FIELD NAME--           --ID--               --LINEITEM--
    
     */
    
    //BEGIN LOCAL VARIABLES

    //END LOCAL VARIABLES

    //BEGIN PAGE INIT CODE BODY
	//alert('type--->>>>'+type)

	if (type == 'create') 
	{
	//	alert('type1--->>>>'+type)
		
		var i_emp_name = nlapiGetFieldValue('custrecord_apm_mth_var_sal_emp_name');
     //   alert('i_emp_name-->'+i_emp_name);
        if (i_emp_name != null && i_emp_name != '' && i_emp_name != 'undefined') 
		{
            var s_paygrp = search_paygroup(i_emp_name);
         //   alert('s_paygrp-->'+s_paygrp);
			var get_wage_date = search_wageperiod(s_paygrp)
		//	alert('get_wage_date-->'+get_wage_date);
			var w_Date = get_wage_date.toString().split('#');
		//	alert('w_Date-->'+w_Date);
			var end_date = w_Date[0];
		//	alert('end_date-->'+end_date);
			var start_date = w_Date[1];
		//	alert('start_date-->'+start_date);
            if (s_paygrp != null && s_paygrp != 'undefined' && s_paygrp != '')
			{
                nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_grp', s_paygrp)
            }
			if (get_wage_date != null && get_wage_date != 'undefined' && get_wage_date != '')
			{
				nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_grp', s_paygrp);	
                nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_dt', get_wage_date);
				nlapiDisableField('custrecord_apm_mth_var_sal_pay_dt', true);
            }
		
        }
        
    }
    
	
	
	


}

//END PAGE INIT

//BEGIN SAVE RECORD

function SaveRecord()
{
    /* On Save Record:
    
     
    
     PURPOSE:
    
     
    
     FIELDS USED:
    
     --FIELD NAME--            --ID--                --LINEITEM--
    
     */
    
    //BEGIN LOCAL VARIABLES

    //END LOCAL VARIABLES

    //BEGIN SAVE RECORD CODE BODY


}

//END SAVE RECORD



//BEGIN FIELD CHANGED	
function fieldChanged_validation_monthly_variable(type, name, linenum)
{
    /* On FIELD CHANGED:
     
     PURPOSE:
     
     FIELDS USED:
     --FIELD NAME--           --ID--               --LINEITEM--
     */
    //BEGIN LOCAL VARIABLES
    
    //END LOCAL VARIABLES
    
    //BEGIN FIELD CHANGED CODE BODY
    if (name == 'custrecord_apm_mth_var_sal_emp_name') 
	{
        var i_emp_name = nlapiGetFieldValue('custrecord_apm_mth_var_sal_emp_name');
        //alert('i_emp_name-->'+i_emp_name);
        if (i_emp_name != null && i_emp_name != '' && i_emp_name != 'undefined') 
		{
            var s_paygrp = search_paygroup(i_emp_name);
            //alert('s_paygrp-->'+s_paygrp);
			var get_wage_date = search_wageperiod(s_paygrp)
			//alert('get_wage_date-->'+get_wage_date);
			var w_Date = get_wage_date.toString().split('#');
			var end_date = w_Date[0];
			var start_date = w_Date[1];
            if (s_paygrp != null && s_paygrp != 'undefined' && s_paygrp != '')
			{
                nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_grp', s_paygrp)
            }
			if (get_wage_date != null && get_wage_date != 'undefined' && get_wage_date != '')
			{
                nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_dt', get_wage_date);
				nlapiSetFieldValue('custrecord_apm_mth_var_sal_pay_dt', get_wage_date);
				nlapiDisableField('custrecord_apm_mth_var_sal_pay_dt', true);
            }
			
			var o_pay_grp = nlapiLoadRecord('customrecord_apm_process_group',s_paygrp);
					//alert('o_pay_grp-->'+o_pay_grp)
					
					o_pay_grp.setFieldValue('custrecord_apm_pro_grp_wage_to_dt',end_date,false,false);
					o_pay_grp.setFieldValue('custrecord_apm_pro_grp_wage_frm_dt',start_date,false,false);
					var o_submit_pay_grp = nlapiSubmitRecord(o_pay_grp,false,false)
					//alert('o_submit_pay_grp-->'+o_submit_pay_grp)
            
        }
        
    }
    
    /*//var s_component_name = nlapiGetFieldValue('custrecord_apm_mth_var_sal_comp_name');
     var s_component_name = nlapiGetFieldText('custrecord_apm_mth_var_sal_comp_name');
     //alert('s_component_name if-->'+s_component_name);
     
     if (name == 'custrecord_apm_mth_var_sal_comp_name')
     {
     //   alert('name-->'+name);
     var s_component_name = nlapiGetFieldValue('custrecord_apm_mth_var_sal_comp_name');
     // alert('s_payroll_component-->'+s_component_name);
     if (s_component_name != null && s_component_name != '' && s_component_name != 'undefined')
     {
     var o_pay_comp = nlapiLoadRecord('customrecord_apm_payroll_component', s_component_name);
     //alert('o_pay_comp-->' + o_pay_comp);
     
     var ch_CTC = o_pay_comp.getFieldValue('custrecord_apm_pc_ctc')
     //alert('ch_CTC-->' + ch_CTC);
     
     var ch_benefits = o_pay_comp.getFieldValue('custrecord_apm_pc_benefits')
     //alert('ch_benefits-->' + ch_benefits);
     
     var s_pay_freq = o_pay_comp.getFieldValue('custrecord_apm_pc_pay_frequency')
     //alert('s_pay_freq-->' + s_pay_freq);
     
     var s_calc_type = o_pay_comp.getFieldValue('custrecord_apm_pc_calculation_type')
     //alert('s_calc_type-->' + s_calc_type);
     
     //************Validation for Monthly variable salary *****************
     
     if (ch_CTC == 'F' && ch_benefits == 'F' && s_pay_freq == 1 && s_calc_type == 2)
     {
     // alert('s_payroll_component-->'+s_payroll_component);
     nlapiDisableField('custrecord_apm_mth_var_sal_amt', false);
     }
     else
     {
     // alert('s_payroll_component-->'+s_payroll_component);
     nlapiDisableField('custrecord_apm_mth_var_sal_amt', true);
     }
     
     }
     }
     
     */
}

//END FIELD CHANGED



//BEGIN VALIDATE FIELD

/* On Validate Field:
 
 PURPOSE:
 
 FIELDS USED:
 --FIELD NAME--           --ID--               --LINEITEM--
 */
//BEGIN LOCAL VARIABLES

//END LOCAL VARIABLES

//BEGIN VALIDATE FIELD CODE BODY

//END VALIDATE FIELD


//BEGIN POST SOURCING

/* On Post Sourcing:
 
 PURPOSE:
 
 FIELDS USED:
 --FIELD NAME--           --ID--               --LINEITEM--
 */
//BEGIN LOCAL VARIABLES

//END LOCAL VARIABLES

//BEGIN POST SOURCING CODE BODY

//END POST SOURCING


//BEGIN LINE INIT 

function lineinit(type){
    /* On Line Init:
    
     
    
     PURPOSE:
    
     
    
     FIELDS USED:
    
     --FIELD NAME--            --ID--                --LINEITEM--
    
     */
    
    //BEGIN LOCAL VARIABLES

    //END LOCAL VARIABLES

    //BEGIN LINE INIT CODE BODY


}

//END LINE INIT



//BEGIN VALIDATE LINE

/* On Validate Line:
 
 PURPOSE:
 
 FIELDS USED:
 --FIELD NAME--           --ID--               --LINEITEM--
 */
//BEGIN LOCAL VARIABLES

//END LOCAL VARIABLES

//BEGIN VALIDATE LINE CODE BODY

//END VALIDATE LINE

function search_paygroup(employee_name)
{
    var filters = new Array();
    var columns = new Array();
    
    filters[0] = new nlobjSearchFilter('custrecord_apm_edc_emp_id', null, 'is', employee_name)
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('custrecord_apm_edc_emp_pay_process_group')
	//columns[2] = new nlobjSearchColumn('custrecord_apm_pro_grp_wage_to_dt')
    
    columns[0].setSort(true);
    
    var s_result = nlapiSearchRecord('customrecord_apm_emp_data_change', null, filters, columns)
    
    if (s_result != null) 
	{
        var s_paygroup = s_result[0].getValue('custrecord_apm_edc_emp_pay_process_group')      
    }
    return s_paygroup 
}

function search_wageperiod(pay_group)
{
	//alert('pay_group' +pay_group);
	var Filters = new Array();
	Filters[0] = new nlobjSearchFilter('custrecord_apm_wage_per_detail_pay_grp', null,'is',pay_group);
	
	var Column= new Array();
	Column[0]= new nlobjSearchColumn('internalid');
	Column[1]= new nlobjSearchColumn('custrecord_apm_wage_per_detail_end_dt');
	Column[2] = new nlobjSearchColumn('custrecord_apm_wage_per_detail_start_dt')
	//Columns[1].setSort(true);
	
	var searchResult =nlapiSearchRecord('customrecord_apm_wage_period_detail',null,Filters,Column);
	//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult.length);
	
	if(searchResult !=null)
	{
		var WagePeriod_EndDate = searchResult[0].getValue('custrecord_apm_wage_per_detail_end_dt')
		//alert('WagePeriod_EndDate' +WagePeriod_EndDate);
		var start_date = searchResult[0].getValue('custrecord_apm_wage_per_detail_start_dt')
		//alert('start_date--'+start_date);
	}
	return WagePeriod_EndDate+"#"+ start_date;
}