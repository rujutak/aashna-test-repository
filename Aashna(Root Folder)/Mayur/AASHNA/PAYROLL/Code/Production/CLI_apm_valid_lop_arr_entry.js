// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: CLI_apm__valid_lop_arr_entry.js
     Author: Sulakshana
     Company: Aashna cloudtech
     Date:05/09/2013
     Description: This script work on validation on LOP Arrear Entry.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     30-09-13				Kapil							Dipika							"Wage period end date" is set to "wage to" date on Pay process group master.
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_lop_arr(type){
    //alert('I am in page in it')
    /*  On page init:
     - PURPOSE
     FIELDS USED:
     --Field Name--				--ID--			--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    //  PAGE INIT CODE BODY
    //alert('type-->' + type);
    if (type == 'create') {
        //alert('type--11111>' + type);
        var Emp_Id = nlapiGetFieldValue('custrecord_apm_lop_arr_entry_emp_name');
        //alert('Emp_Id-->' + Emp_Id);
        
        var wage_Period = nlapiGetFieldValue('custrecord_apm_lop_arr_entry_wage_end_dt');
        
        var pay_group = Paygroup(Emp_Id);
        //alert ('pay_group=='+pay_group);
        
        var wage_period_EndDate = wage_period(pay_group);
        //alert ('wage_period_EndDate=='+wage_period_EndDate);
        
        nlapiSetFieldValue('custrecord_apm_lop_arr_entry_wage_end_dt', wage_period_EndDate);
        nlapiDisableField('custrecord_apm_lop_arr_entry_wage_end_dt', true);
        
        
    }
    
    
    
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord(){
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SAVE RECORD CODE BODY
    
    
    return true;
    
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum){

    /*  On validate field:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  VALIDATE FIELD CODE BODY
    
    return true;
    
}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function lop_arr_days_fieldChanged(type, name, linenum){
    //alert('I am in field change')
    /*  On field changed:
     - PURPOSE
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  FIELD CHANGED CODE BODY
    try {
        if (name == 'custrecord_apm_lop_arr_entry_emp_name') {
            var Emp_Id = nlapiGetFieldValue('custrecord_apm_lop_arr_entry_emp_name');
            //	alert('Emp_Id-->' + Emp_Id);
            
            
            var wage_Period = nlapiGetFieldValue('custrecord_apm_lop_arr_entry_wage_end_dt');
            
            var pay_group = Paygroup(Emp_Id);
            //alert ('pay_group=='+pay_group);
            
            var wage_period_EndDate = wage_period(pay_group);
            //alert ('wage_period_EndDate=='+wage_period_EndDate);
            //if(wage_period_EndDate != '' && wage_period_EndDate != null && wage_period_EndDate != 'undefined' && !isNaN(wage_period_EndDate))
            {
                //alert('In if')
                nlapiSetFieldValue('custrecord_apm_lop_arr_entry_wage_end_dt', wage_period_EndDate);
                //nlapiDisableField('custrecord_apm_lop_arr_entry_wage_end_dt', true);
                
                var o_pay_grp = nlapiLoadRecord('customrecord_apm_process_group', pay_group);
                //alert('o_pay_grp-->'+o_pay_grp)
                
                o_pay_grp.setFieldValue('custrecord_apm_pro_grp_wage_to_dt', wage_period_EndDate, false, false);
                
                var o_submit_pay_grp = nlapiSubmitRecord(o_pay_grp, false, false)
                //alert('o_submit_pay_grp-->'+o_submit_pay_grp)
            }
            
            
            
            
            
        }
    } 
    
    catch (e) {
        alert('Wage period is not defined for this employee');
        nlapiSetFieldValue('custrecord_apm_lop_arr_entry_wage_end_dt', '');
    }
    
    
    
    
    
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name){

    /*  On post sourcing:
    
     - PURPOSE
    
     FIELDS USED:
    
     --Field Name--			--ID--		--Line Item Name--
    
     */
    
    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type){

    /*  On Line Init:
    
     - PURPOSE
    
     FIELDS USED:
    
     --Field Name--			--ID--		--Line Item Name--
    
     */
    
    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  VALIDATE LINE CODE BODY
    
    
    return true;
    
}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type){

    /*  On recalc:
    
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
    
     FIELDS USED:
    
     --Field Name--				--ID--
    
     */
    
    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
function Paygroup(Emp_Id){
    //alert('Emp_Id' +Emp_Id);
    var Filters = new Array();
    Filters[0] = new nlobjSearchFilter('custrecord_apm_edc_emp_id', null, 'is', Emp_Id);
    
    var Column = new Array();
    Column[0] = new nlobjSearchColumn('custrecord_apm_edc_emp_pay_process_group');
    
    var searchResult = nlapiSearchRecord('customrecord_apm_emp_data_change', null, Filters, Column);
    //nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult.length);
    
    if (searchResult != null) {
        var paygrp = searchResult[0].getValue('custrecord_apm_edc_emp_pay_process_group')
        //alert('paygrp' +paygrp);
        return paygrp;
    }
    
}

function wage_period(pay_group){
    //alert('pay_group' +pay_group);
    var Filters = new Array();
    Filters[0] = new nlobjSearchFilter('custrecord_apm_wage_per_detail_pay_grp', null, 'is', pay_group);
    
    var Column = new Array();
    Column[0] = new nlobjSearchColumn('custrecord_apm_wage_per_detail_end_dt');
    Column[1] = new nlobjSearchColumn('internalid');
    //Columns[0].setSort(true);
    
    var searchResult = nlapiSearchRecord('customrecord_apm_wage_period_detail', null, Filters, Column);
    //nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult.length);
    
    if (searchResult != null) {
        var WagePeriod_EndDate = searchResult[0].getValue('custrecord_apm_wage_per_detail_end_dt')
        //alert('WagePeriod_EndDate' +WagePeriod_EndDate);
    
    }
    return WagePeriod_EndDate;
}





// END FUNCTION =====================================================


