// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:USE_UES_EmailNotificationOnSalesOrder.js
      Date:10-Sep-2013
    
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ===============================================

function afterSubmitRecord_Salesorder(type){
    
        try {
			
			var recordId = nlapiGetRecordId()
			//nlapiLogExecution('DEBUG', 'after Submit', 'recordId=' + recordId);
			var rectype = nlapiGetRecordType();
			//nlapiLogExecution('DEBUG', 'after Submit', 'rectype=' + rectype);
			var salesOrderObj = nlapiLoadRecord(rectype, recordId);
			//nlapiLogExecution('DEBUG', 'after Submit', 'IFObj=' + IFObj);
			
			//Load Sales Order
			//var salesOrderObj = nlapiLoadRecord('salesorder', internalid);
			var customerObj = salesOrderObj.getFieldValue('entity');   	              // Customer obj
			
			//Load Customer record
			var custObj = nlapiLoadRecord('customer', customerObj); 		
			
			var customerName = custObj.getFieldValue('entityid');   	   			 // Dear --- Customer Name (from Customer)
			var emailId = custObj.getFieldValue('email');   	 		   			 // E-mail Address   (from Customer)
			
			var billingAddress = salesOrderObj.getFieldValue('billaddress');         //Billing  Address  (from Billing Tab of Sales Order)
			var shippingAddress = salesOrderObj.getFieldValue('shipaddress');        //Shipping Address	(from Shipping Tab)
			var orderGrandTotal = salesOrderObj.getFieldValue('total');	  		     //Order Grand Total
			var OrderNumber = salesOrderObj.getFieldValue('tranid');				 //Order number
			var shippingMethod = salesOrderObj.getFieldValue('shipmethod');			 //Shipping Method
			var subtotalOfItems = salesOrderObj.getFieldValue('subtotal');           //Subtotal of Items
			var shippingAndHandling = salesOrderObj.getFieldValue('subtotal');       					    //Shipping & Handling
			//Promotion Applied																								
			var totalForThisOrder = orderGrandTotal ;	        					 //Total for this order
			//Delivery Estimate
			
			//Item
			var itemCount = salesOrderObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'line item count', 'itemCount= ' + itemCount);
			
				for (var i = 1; i <= itemCount; i++) {
					itemName = salesOrderObj.getLineItemValue('item', 'description', i);     	 // Item name
					quantity = salesOrderObj.getLineItemValue('item', 'quantity', i);     		 // Quantity
					price = salesOrderObj.getLineItemValue('item', 'rate', i);         		 // price
					//total = GetCurrentLineItemValue('item', 'total', i);           	 // total
					
					nlapiLogExecution('DEBUG', 'line item', 'itemName= ' + itemName);
					nlapiLogExecution('DEBUG', 'line item', 'quantity= ' + quantity);
					nlapiLogExecution('DEBUG', 'line item', 'price= ' + price);
				}
				
					nlapiLogExecution('DEBUG', 'values', 'salesOrderObj= ' + salesOrderObj);
					nlapiLogExecution('DEBUG', 'values', 'customerObj= ' + customerObj);
					nlapiLogExecution('DEBUG', 'values', 'custObj= ' + custObj);
					nlapiLogExecution('DEBUG', 'values', 'customerName= ' + customerName);
					nlapiLogExecution('DEBUG', 'values', 'emailId= ' + emailId);
					nlapiLogExecution('DEBUG', 'values', 'billingAddress= ' + billingAddress);
					nlapiLogExecution('DEBUG', 'values', 'shippingAddress= ' + shippingAddress);
					nlapiLogExecution('DEBUG', 'values', 'orderGrandTotal= ' + orderGrandTotal);
					nlapiLogExecution('DEBUG', 'values', 'OrderNumber= ' + OrderNumber);
					nlapiLogExecution('DEBUG', 'values', 'shippingMethod= ' + shippingMethod);
					nlapiLogExecution('DEBUG', 'values', 'subtotalOfItems= ' + subtotalOfItems);
					nlapiLogExecution('DEBUG', 'values', 'shippingAndHandling= ' + shippingAndHandling);
					nlapiLogExecution('DEBUG', 'values', 'shippingAndHandling= ' + totalForThisOrder);
					
				
				//STC shop launch special discount
				
			//salesOrderObj.setFieldValue('custrecord_projectcode_profitability', projectcode);
			
			var id = nlapiSubmitRecord(salesOrderObj, false, false)
			
		} 
		catch (e) {
			nlapiLogExecution('DEBUG', 'catch ', 'Exception =' + e);
			
		}
        
        //return true;
    }
    



