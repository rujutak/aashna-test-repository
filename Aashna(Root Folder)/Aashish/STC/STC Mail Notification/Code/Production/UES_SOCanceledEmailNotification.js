// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:USE_UES_SOCanceledEmailNotification.js
      Date:10-Sep-2013
    
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ===============================================

function afterSubmitRecord_SalesorderCancel(type)
{
    
        try {
		//if (type == 'create') 
		{
			var recordId = nlapiGetRecordId()
			//nlapiLogExecution('DEBUG', 'after Submit', 'recordId=' + recordId);
			var rectype = nlapiGetRecordType();
			//nlapiLogExecution('DEBUG', 'after Submit', 'rectype=' + rectype);
			var salesOrderObj = nlapiLoadRecord(rectype, recordId);
			//nlapiLogExecution('DEBUG', 'after Submit', 'IFObj=' + IFObj);
			
			//Load Sales Order
			//var salesOrderObj = nlapiLoadRecord('salesorder', internalid);
			var customerObj = salesOrderObj.getFieldValue('entity'); 						   // Customer obj
			//Load Customer record
			var custObj = nlapiLoadRecord('customer', customerObj);
			
			var customerName = custObj.getFieldValue('entityid'); 							  // Dear --- Customer Name (from Customer)
			var emailId = custObj.getFieldValue('email');									 // E-mail Address   (from Customer)
			//var toEmailAdd = salesOrderObj.getFieldValue('tobeemailed');
						
			var orderGrandTotal = salesOrderObj.getFieldValue('total'); 				     //Order Grand Total
			var OrderNumber = salesOrderObj.getFieldValue('tranid');                         //Order number
			var shippingMethod = salesOrderObj.getFieldText('shipmethod');                   //Shipping Method
			//var shippingMethod= formatAndReplaceMessageForAnd(shipMethod);
			
			var subtotalOfItems = salesOrderObj.getFieldValue('subtotal');                  //Subtotal of Items
			var shippingAndHandling = salesOrderObj.getFieldValue('shippingcost');          //Shipping & Handling
																										
			var totalForThisOrder = orderGrandTotal; 									    //Total for this order
			//Delivery Estimate

			var status = salesOrderObj.getFieldValue('status');
			nlapiLogExecution('DEBUG', 'status Submit', 'status=' + status); 					//Cancelled
			
			//To process the mail
			if(status=='Cancelled' || status=='Closed'){
			sendMalOnSOCancel(customerName, emailId, orderGrandTotal, OrderNumber, shippingMethod, subtotalOfItems, shippingAndHandling, totalForThisOrder, salesOrderObj);
			}
			
			
			var id = nlapiSubmitRecord(salesOrderObj, false, false)
		}
		} 
		catch (e) {
			nlapiLogExecution('DEBUG', 'catch ', 'Exception =' + e);
			
		}
        
        //return true;
    }
    
function sendMalOnSOCancel(customerName,emailId,orderGrandTotal,OrderNumber,shippingMethod,subtotalOfItems,shippingAndHandling,totalForThisOrder,salesOrderObj)
{
	var subject = "Email notification after SO Cancellation";
	nlapiLogExecution('DEBUG', 'to be sent to ', 'emailId =' + emailId);
	//var toList= emailId ;
	var toList= "aashishrk88@gmail.com" ;   
	
	
	var strVar="";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>";
	strVar += "		<img border=\"0\" src=\"http://shopping.na1.netsuite.com/core/media/media.nl?id=11&c=3563610&h=d6047abd12079c763ad1\" width=\"182\" height=\"86\"><\/td>";
	strVar += "		<td width=\"337\"><b>VIEW CART<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ";
	strVar += "		<b>MY ACCOUNT<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <b>HELP<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\" height=\"52\">";
	strVar += "	<tr>";
	strVar += "		<td>Dear " + customerName + ";<p>We're writing to inform you that your order ";
	strVar += "		" + OrderNumber + " has been canceled. Your credit card was<br>";
	strVar += "		not charged for this order.<p>If you're still interested in this item, ";
	strVar += "		please search for it again on shop.stc.com<p>We've included your order ";
	strVar += "		details below for reference.<p>We value you as a customer and hope you ";
	strVar += "		will continue shopping with us!<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";

	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Your Order Details:<\/b></font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Order number:<\/b><\/td>";
	if (OrderNumber != null && OrderNumber != '' && OrderNumber != 'undefined') {
		strVar += "		<td>" + OrderNumber + "<\/td>";
	}
	else{
		strVar += "		<td>   <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping Method:<\/b><\/td>";
	if (shippingMethod != null && shippingMethod != '' && shippingMethod != 'undefined') {
		strVar += "		<td>" + shippingMethod + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined') {
		strVar += "		<td>" + subtotalOfItems + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping & Handling:<\/b><\/td>";
	if (shippingAndHandling != null && shippingAndHandling != '' && shippingAndHandling != 'undefined') {
		strVar += "		<td>" + shippingAndHandling + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Promotion Applied:<\/b><\/td>";
	strVar += "		<td>NA<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Total for this order:<\/b><\/td>";
	if (totalForThisOrder != null && totalForThisOrder != '' && totalForThisOrder != 'undefined') {
		strVar += "		<td>" + totalForThisOrder + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Delivery Estimate:<\/b><\/td>";
	strVar += "		<td>  <\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";


	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr bgcolor=\"#942192\">";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Line<\/b><\/font><\/td>";
	strVar += "		<td width=\"40%\" align=center><font color=\"#FFFFFF\"><b>Item<\/b><\/font><\/td>";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Quantity<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=center width=\"266\" ><font color=\"#FFFFFF\"><b>Price<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=right><font color=\"#FFFFFF\"><b>Total<\/b><\/font><\/td>";
	strVar += "	<\/tr>";
	
	//Line Item Values rows
	var itemCount = salesOrderObj.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'line item count', 'itemCount= ' + itemCount);
			
	for (var i = 1; i <= itemCount; i++) {
			var itemName = salesOrderObj.getLineItemText('item', 'item', i);     	 // Item name
			nlapiLogExecution('DEBUG', 'line item count', 'item Name= ' + itemName);
			//var itemConvName = formatAndReplaceMessageForAnd(itemName);
			//nlapiLogExecution('DEBUG', 'line item count', 'itemConvName= ' + itemConvName);
			var quantity = salesOrderObj.getLineItemValue('item', 'quantity', i);     		 // Quantity
			var price = salesOrderObj.getLineItemValue('item', 'rate', i);         		 // price
			//total = GetCurrentLineItemValue('item', 'total', i);           	 // total
			
	strVar += "	<tr>";
	strVar += "		<td align=center width=\"10%\" border=\"0.1\" style=\"background-color: #FFFFFF\">" + i + "<\/td>";
	if (itemName != null && itemName != '' && itemName != 'undefined') {
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">" + itemName + "<\/td>";
	}
	else{
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">  <\/td>";
	}
	if (quantity != null && quantity != '' && quantity != 'undefined') {
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">" + quantity + "<\/td>";
	}
	else{
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\"align=right border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=right border=\"0.1\">   <\/td>";
	}
	strVar += "	<\/tr>";
	}
	strVar += "<\/table>";
	
			    
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr style=\"border-top-style:none;\">";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined')
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>" + subtotalOfItems + "<\/b><\/td>";
	}
	else
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\">  <\/td>";
	}
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>STC shop launch special discount:<\/b><\/td>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>0<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	

	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>Please note: This e-mail message was sent from a notification-only ";
	strVar += "		address that cannot accept incoming email.<br>";
	strVar += "		Please do not reply to this message.<br>";
	strVar += "		Thanks again for shopping with STC!<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	var authorId= 59;
	nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', " authorId=== "+authorId);
	
	nlapiSendEmail(authorId, toList, subject, strVar, null, null, null, null);
	//nlapiSendEmail(nlapiGetUser(), toList, subject, strVar, ccList, null, null, null);

}


function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);

 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&amp;");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);

 return messgaeToBeSendParaAnd;
}


function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
//&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 return messgaeToBeSendPara;
}