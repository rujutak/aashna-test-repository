// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:USE_UES_EmailNotificationOnSalesOrder.js
      Date:10-Sep-2013
    
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ===============================================

function afterSubmitRecord_Salesorder(type){
    
        try {
			var recordId = nlapiGetRecordId()
			//nlapiLogExecution('DEBUG', 'after Submit', 'recordId=' + recordId);
			var rectype = nlapiGetRecordType();
			//nlapiLogExecution('DEBUG', 'after Submit', 'rectype=' + rectype);
	if (type == 'create') {
	
		if (rectype == 'salesorder') {
			var salesOrderObj = nlapiLoadRecord(rectype, recordId);
			nlapiLogExecution('DEBUG', 'after Submit', 'salesOrderObj=' + salesOrderObj);
			
			
			//Load Sales Order
			//var salesOrderObj = nlapiLoadRecord('salesorder', internalid);
			var customerObj = salesOrderObj.getFieldValue('entity'); // Customer obj
			//Load Customer record
			var custObj = nlapiLoadRecord('customer', customerObj);
			
			var customerName = custObj.getFieldValue('entityid'); // Dear --- Customer Name (from Customer)
			var emailId = custObj.getFieldValue('email'); // E-mail Address   (from Customer)
			//var toEmailAdd = salesOrderObj.getFieldValue('tobeemailed');
			var billAddress = salesOrderObj.getFieldValue('billaddress'); //Billing  Address  (from Billing Tab of Sales Order)
			var billingAddress = formatAndReplaceMessageForAnd(billAddress);
			//var finalBilAddress = nlapiEscapeXML(billingAddress);
			//nlapiLogExecution('DEBUG', 'finalBilAddress', 'finalBilAddress= ' + finalBilAddress);
			billingAddress = formatAndReplaceSpacesofMessage(nlapiEscapeXML(billingAddress));
			
			var shipAddress = salesOrderObj.getFieldValue('shipaddress'); //Shipping Address	(from Shipping Tab)
			var shippingAddress = formatAndReplaceMessageForAnd(shipAddress);
			//var finalShipAddress = nlapiEscapeXML(shippingAddress);
			//nlapiLogExecution('DEBUG', 'finalShipAddress', 'finalShipAddress= ' + finalShipAddress);
			shippingAddress = formatAndReplaceSpacesofMessage(nlapiEscapeXML(shippingAddress));
			
			var orderGrandTotal = salesOrderObj.getFieldValue('total'); //Order Grand Total
			var OrderNumber = salesOrderObj.getFieldValue('tranid'); //Order number
			var shippingMethod = salesOrderObj.getFieldText('shipmethod'); //Shipping Method
			//var shippingMethod= formatAndReplaceMessageForAnd(shipMethod);
			
			var subtotalOfItems = salesOrderObj.getFieldValue('subtotal'); //Subtotal of Items
			var shippingAndHandling = salesOrderObj.getFieldValue('shippingcost'); //Shipping & Handling
			//Promotion Applied																								
			var totalForThisOrder = orderGrandTotal; //Total for this order
			//Delivery Estimate
			
			//Item
			var itemCount = salesOrderObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'line item count', 'itemCount= ' + itemCount);
			
			for (var i = 1; i <= itemCount; i++) {
				itemName = salesOrderObj.getLineItemValue('item', 'description', i); // Item name
				quantity = salesOrderObj.getLineItemValue('item', 'quantity', i); // Quantity
				price = salesOrderObj.getLineItemValue('item', 'rate', i); // price
			//total = GetCurrentLineItemValue('item', 'total', i);           	 // total
			
			}
			//STC shop launch special discount
			
			//salesOrderObj.setFieldValue('custrecord_projectcode_profitability', projectcode);
			
			//To process the mail
			sendMalOnSO(customerName, emailId, billingAddress, shippingAddress, orderGrandTotal, OrderNumber, shippingMethod, subtotalOfItems, shippingAndHandling, totalForThisOrder, salesOrderObj);
			
			var id = nlapiSubmitRecord(salesOrderObj, false, false)
		}
	}
		
		//Item Fulfilment
		if (rectype == 'itemfulfillment') {
			
			var itemFulfilmentObj = nlapiLoadRecord(rectype, recordId);
			nlapiLogExecution('DEBUG', 'after Submit', 'itemFulfilmentObj=' + itemFulfilmentObj);

			var customerObj = itemFulfilmentObj.getFieldValue('entity');         // 		Customer obj
			
			var trackingNumber = itemFulfilmentObj.getLineItemValue('package','packagetrackingnumber',1);     //Tracking number
			nlapiLogExecution('DEBUG', 'after Submit', 'trackingNumber=' + trackingNumber);
			//Load Customer record
			var custObj = nlapiLoadRecord('customer', customerObj);
			
			var customerName = custObj.getFieldValue('entityid');		        // Dear --- Customer Name (from Customer)
			
			var emailId = custObj.getFieldValue('email'); 				 		// E-mail Address   (from Customer)
 			var firstName = custObj.getFieldText('glommedname'); 				// first Name   (from Customer)
 			nlapiLogExecution('DEBUG', 'firstName', 'firstName= ' + firstName);
 			
 			var salesOrderObj = itemFulfilmentObj.getFieldValue('createdfrom');         // 		Sales Order Obj
			//Load Sales Order
			var SOObj = nlapiLoadRecord('salesorder', salesOrderObj);
			
 		    var shippingMethod = SOObj.getFieldText('shipmethod'); 					  //Shipping Method
			//var shippingMethod= formatAndReplaceMessageForAnd(shipMethod);
 						
			var shipAddress = SOObj.getFieldValue('shipaddress');	   				 //Shipping Address	(from Shipping Tab)
			var shippingAddress = formatAndReplaceMessageForAnd(shipAddress);
			//nlapiLogExecution('DEBUG', 'finalShipAddress', 'finalShipAddress= ' + finalShipAddress);
			shippingAddress = formatAndReplaceSpacesofMessage(nlapiEscapeXML(shippingAddress));
			
			var orderGrandTotal = SOObj.getFieldValue('total'); 				//Order Grand Total
			var OrderNumber = SOObj.getFieldValue('tranid');					//Order number
			
			var subtotalOfItems = SOObj.getFieldValue('subtotal');	      	    //Subtotal of Items
			var shippingAndHandling = SOObj.getFieldValue('shippingcost');      //Shipping & Handling
			
			var totalForThisOrder = orderGrandTotal; 						    //Total for this order
			
			//Delivery Estimate
			
			//To process the mail
			sendMalOnItemFullfilment(customerName, emailId, billingAddress, shippingAddress, orderGrandTotal, OrderNumber, shippingMethod, subtotalOfItems, shippingAndHandling, totalForThisOrder, SOObj,trackingNumber,firstName);
			
			var id = nlapiSubmitRecord(itemFulfilmentObj, false, false)
		}
		//	
		
	
		} 
		catch (e) {
			nlapiLogExecution('DEBUG', 'catch ', 'Exception =' + e);
			
		}
        
        //return true;
    }
    
	
function sendMalOnSO(customerName,emailId,billingAddress,shippingAddress,orderGrandTotal,OrderNumber,shippingMethod,subtotalOfItems,shippingAndHandling,totalForThisOrder,salesOrderObj)
{
	var subject = "Sales Order notification mail";
	nlapiLogExecution('DEBUG', 'to be sent to ', 'emailId =' + emailId);
	var toList= emailId ;   
	
	
	var strVar="";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>";
	strVar += "		<img border=\"0\" src=\"http://shopping.na1.netsuite.com/core/media/media.nl?id=11&c=3563610&h=d6047abd12079c763ad1\" width=\"182\" height=\"86\"><\/td>";
	strVar += "		<td width=\"337\"><b>VIEW CART<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ";
	strVar += "		<b>MY ACCOUNT<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <b>HELP<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\" height=\"52\">";
	strVar += "	<tr>";
	strVar += "		<td>Dear " + customerName + ";<p>Thank you for your order! If you need to check ";
	strVar += "		the status of your order or make changes, visit our<br>";
	strVar += "		home page at shop.stc.com.sa and click on My Account.<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";

	strVar += "<table border=\"1\" width=\"100%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Purchasing Information<\/b></font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	if (emailId != null && emailId != '' && emailId != 'undefined') {
		strVar += "		<td width=\"591\"><b>E-mail Address:  <\/b>" + emailId + "<\/td>";
	}
	else{
		strVar += "		<td width=\"591\"><b>E-mail Address:  <\/b>  <\/td>";
	}
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"591\"><b>Billing Address:<\/b>";
	if (billingAddress != null && billingAddress != '' && billingAddress != 'undefined') {
		strVar += "		" + billingAddress + "<\/td>"
	}
	else{
		strVar += "	  <\/td>"
	}
	strVar += "		<td><b>Shipping Address:<\/b>";
	if (shippingAddress != null && shippingAddress != '' && shippingAddress != 'undefined') {
	strVar += "		" + shippingAddress + "<\/td>"
	}
	else{
	strVar += "		<\/td>"	
	}
	strVar += "	<\/tr>";

	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	if (orderGrandTotal != null && orderGrandTotal != '' && orderGrandTotal != 'undefined') {
		strVar += "		<td width=\"591\"><b>Order Grand Total: <\/b>" + orderGrandTotal + "<\/td>";
	}
	else{
		strVar += "		<td width=\"591\"><b>Order Grand Total: <\/b>   <\/td>";
	}
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Your Order Summary:<\/b></font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Order number:<\/b><\/td>";
	if (OrderNumber != null && OrderNumber != '' && OrderNumber != 'undefined') {
		strVar += "		<td>" + OrderNumber + "<\/td>";
	}
	else{
		strVar += "		<td>   <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping Method:<\/b><\/td>";
	if (shippingMethod != null && shippingMethod != '' && shippingMethod != 'undefined') {
		strVar += "		<td>" + shippingMethod + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined') {
		strVar += "		<td>" + subtotalOfItems + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping & Handling:<\/b><\/td>";
	if (shippingAndHandling != null && shippingAndHandling != '' && shippingAndHandling != 'undefined') {
		strVar += "		<td>" + shippingAndHandling + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Promotion Applied:<\/b><\/td>";
	strVar += "		<td>NA<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Total for this order:<\/b><\/td>";
	if (totalForThisOrder != null && totalForThisOrder != '' && totalForThisOrder != 'undefined') {
		strVar += "		<td>" + totalForThisOrder + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Delivery Estimate:<\/b><\/td>";
	strVar += "		<td>  <\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";


	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr bgcolor=\"#942192\">";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Line<\/b><\/font><\/td>";
	strVar += "		<td width=\"40%\" align=center><font color=\"#FFFFFF\"><b>Item<\/b><\/font><\/td>";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Quantity<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=center width=\"266\" ><font color=\"#FFFFFF\"><b>Price<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=right><font color=\"#FFFFFF\"><b>Total<\/b><\/font><\/td>";
	strVar += "	<\/tr>";
	
	//Line Item Values rows
	var itemCount = salesOrderObj.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'line item count', 'itemCount= ' + itemCount);
			
	for (var i = 1; i <= itemCount; i++) {
			var itemName = salesOrderObj.getLineItemText('item', 'item', i);     	 // Item name
			nlapiLogExecution('DEBUG', 'line item count', 'item Name= ' + itemName);
			//var itemConvName = formatAndReplaceMessageForAnd(itemName);
			//nlapiLogExecution('DEBUG', 'line item count', 'itemConvName= ' + itemConvName);
			var quantity = salesOrderObj.getLineItemValue('item', 'quantity', i);     		 // Quantity
			var price = salesOrderObj.getLineItemValue('item', 'rate', i);         		 // price
			//total = GetCurrentLineItemValue('item', 'total', i);           	 // total
			
	strVar += "	<tr>";
	strVar += "		<td align=center width=\"10%\" border=\"0.1\" style=\"background-color: #FFFFFF\">" + i + "<\/td>";
	if (itemName != null && itemName != '' && itemName != 'undefined') {
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">" + itemName + "<\/td>";
	}
	else{
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">  <\/td>";
	}
	if (quantity != null && quantity != '' && quantity != 'undefined') {
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">" + quantity + "<\/td>";
	}
	else{
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\"align=right border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=right border=\"0.1\">   <\/td>";
	}
	strVar += "	<\/tr>";
	}
	strVar += "<\/table>";
	
			    
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr style=\"border-top-style:none;\">";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined')
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>" + subtotalOfItems + "<\/b><\/td>";
	}
	else
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\">  <\/td>";
	}
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>STC shop launch special discount:<\/b><\/td>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>0<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	

	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td><b>Where can I get help with reviewing or changing my orders?<\/b><br>";
	strVar += "		To learn more about managing your orders on shop.stc.com please visit ";
	strVar += "		our Help pages at shop.stc.com\/<br>";
	strVar += "		help\/orders\/.<br>";
	strVar += "		Please note: This e-mail message was sent from a notification-only ";
	strVar += "		address that cannot accept incoming email.<br>";
	strVar += "		Please do not reply to this message.<br>";
	strVar += "		If you ever need to return an order, visit our Online Returns Center: ";
	strVar += "		shop.stc.com\/returns<br>";
	strVar += "		Thanks again for shopping with STC!<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";

	//var authorId= nlapiGetUser();
	//var authorId= nlapiGetUser();
	var authorId= 59;
	nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', " authorId=== "+authorId);
	
	nlapiSendEmail(authorId, toList, subject, strVar, null, null, null, null);
	//nlapiSendEmail(nlapiGetUser(), toList, subject, strVar, ccList, null, null, null);

}


function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);

 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&amp;");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);

 return messgaeToBeSendParaAnd;
}


function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
//&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 return messgaeToBeSendPara;
}


function sendMalOnItemFullfilment(customerName,emailId,billingAddress,shippingAddress,orderGrandTotal,OrderNumber,shippingMethod,subtotalOfItems,shippingAndHandling,totalForThisOrder,SOObj,trackingNumber,firstName)
{
	var subject = "Email notification after SO Fulfilled";
	nlapiLogExecution('DEBUG', 'Item full ', 'emailId =' + emailId);
	//var toList= emailId ;
	var toList= "aashishrk88@gmail.com" ;  
	
	
	var strVar="";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>";
	strVar += "		<img border=\"0\" src=\"http://shopping.na1.netsuite.com/core/media/media.nl?id=11&c=3563610&h=d6047abd12079c763ad1\" width=\"182\" height=\"86\"><\/td>";
	strVar += "		<td width=\"337\"><b>VIEW CART<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ";
	strVar += "		<b>MY ACCOUNT<\/b>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <b>HELP<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\" height=\"52\">";
	strVar += "	<tr>";
	strVar += "		<td>Dear " + firstName + ";.<p>Thank you for shopping with us. We would like ";
	strVar += "		to inform you that we shipped your item(s). Your<br>";
	strVar += "		order is on its way, and can no longer be changed. If you need to return ";
	strVar += "		an item from this shipment<br>";
	strVar += "		or manage other orders, please visit My Orders on shop.stc.com.<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	////
	strVar += "<table border=\"1\" width=\"100%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Order Delivery Information<\/b><\/font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"48%\">Your estimated delivery date is<b>: <\/b><\/td>";
	strVar += "		<td rowspan=\"3\" width=\"51%\">Your order was shipped to<b>: <\/b>" ;
	if (shippingAddress  != null && shippingAddress  != '' && shippingAddress  != 'undefined')
	{
		strVar += "		<b><br>" + shippingAddress + "<\/b><\/td>";
	}
	else{
		strVar += "		<br><\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"48%\">Your package is being shipped by "
	if (shippingMethod != null && shippingMethod != '' && shippingMethod != 'undefined')
	{
	strVar += "	 <b>" + shippingMethod + "<\/b>"
	}
	else
	{
	strVar += "	 "	
	}
	strVar += "	 and<br>";
	strVar += "		the tracking number is ";
	if (trackingNumber  != null && trackingNumber  != '' && trackingNumber  != 'undefined'){
	strVar +=  "<b>" + trackingNumber + "<\/b>.<\/td>";
	}
	else{
	strVar +=  " .<\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td width=\"51%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
	strVar += "<table border=\"0\" border-radius:5px width=\"24%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Track your Package<\/b><\/font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>You can also contact " + shippingMethod + " Customer service on 92 000 9999 to ";
	strVar += "		inquire about the status of your shipment<br>";
	strVar += "		or arrange for delivery!<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";


	//strVar += "	<tr>";
	//strVar += "		<td colspan=\"2\">You can also contact " + shippingMethod + " Customer service on 92 000 ";
	//strVar += "		9999 to inquire about the status of your shipment<br>";
	//strVar += "		or arrange for delivery!<\/td>";
	//strVar += "	<\/tr>";

	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" bgcolor=\"#942192\">";
	strVar += "	<tr>";
	strVar += "		<td><font color=\"#FFFFFF\"><b>Your Order Summary:<\/b></font><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Order number:<\/b><\/td>";
	if (OrderNumber != null && OrderNumber != '' && OrderNumber != 'undefined') {
		strVar += "		<td>" + OrderNumber + "<\/td>";
	}
	else{
		strVar += "		<td>   <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping Method:<\/b><\/td>";
	if (shippingMethod != null && shippingMethod != '' && shippingMethod != 'undefined') {
		strVar += "		<td>" + shippingMethod + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined') {
		strVar += "		<td>" + subtotalOfItems + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Shipping & Handling:<\/b><\/td>";
	if (shippingAndHandling != null && shippingAndHandling != '' && shippingAndHandling != 'undefined') {
		strVar += "		<td>" + shippingAndHandling + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Promotion Applied:<\/b><\/td>";
	strVar += "		<td>NA<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Total for this order:<\/b><\/td>";
	if (totalForThisOrder != null && totalForThisOrder != '' && totalForThisOrder != 'undefined') {
		strVar += "		<td>" + totalForThisOrder + "<\/td>";
	}
	else{
		strVar += "		<td>  <\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"205\"><b>Delivery Estimate:<\/b><\/td>";
	strVar += "		<td>  <\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";


	//strVar += "<p>&nbsp;<\/p>";
	strVar += "<br>";
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr bgcolor=\"#942192\">";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Line<\/b><\/font><\/td>";
	strVar += "		<td width=\"40%\" align=center><font color=\"#FFFFFF\"><b>Item<\/b><\/font><\/td>";
	strVar += "		<td width=\"10%\" align=center><font color=\"#FFFFFF\"><b>Quantity<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=center width=\"266\" ><font color=\"#FFFFFF\"><b>Price<\/b><\/font><\/td>";
	strVar += "		<td width=\"20%\" align=right><font color=\"#FFFFFF\"><b>Total<\/b><\/font><\/td>";
	strVar += "	<\/tr>";
	
	//Line Item Values rows
	var itemCount = SOObj.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'line item count', 'itemCount= ' + itemCount);
			
	for (var i = 1; i <= itemCount; i++) {
			var itemName = SOObj.getLineItemText('item', 'item', i);     	 // Item name
			nlapiLogExecution('DEBUG', 'line item count', 'item Name= ' + itemName);
			//var itemConvName = formatAndReplaceMessageForAnd(itemName);
			//nlapiLogExecution('DEBUG', 'line item count', 'itemConvName= ' + itemConvName);
			var quantity = SOObj.getLineItemValue('item', 'quantity', i);     		 // Quantity
			var price = SOObj.getLineItemValue('item', 'rate', i);         		 // price
			//total = GetCurrentLineItemValue('item', 'total', i);           	 // total
			
	strVar += "	<tr>";
	strVar += "		<td align=center width=\"10%\" border=\"0.1\" style=\"background-color: #FFFFFF\">" + i + "<\/td>";
	if (itemName != null && itemName != '' && itemName != 'undefined') {
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">" + itemName + "<\/td>";
	}
	else{
		strVar += "		<td width=\"40%\" align=left border=\"0.1\">  <\/td>";
	}
	if (quantity != null && quantity != '' && quantity != 'undefined') {
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">" + quantity + "<\/td>";
	}
	else{
		strVar += "		<td width=\"10%\" align=center border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=center width=\"266\" border=\"0.1\">   <\/td>";
	}
	if (price != null && price != '' && price != 'undefined') {
		strVar += "		<td width=\"20%\"align=right border=\"0.1\">" + price + "<\/td>";
	}
	else{
		strVar += "		<td width=\"20%\" align=right border=\"0.1\">   <\/td>";
	}
	strVar += "	<\/tr>";
	}
	strVar += "<\/table>";
	
			    
	strVar += "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
	strVar += "	<tr style=\"border-top-style:none;\">";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>Subtotal of Items:<\/b><\/td>";
	if (subtotalOfItems != null && subtotalOfItems != '' && subtotalOfItems != 'undefined')
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\"><b>" + subtotalOfItems + "<\/b><\/td>";
	}
	else
	{
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"80%\" align=\"right\">  <\/td>";
	}
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>STC shop launch special discount:<\/b><\/td>";
	strVar += "		<td border=\"0.1\" style=\"border-top-style:none;\" width=\"20%\" align=\"right\"><b>0<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	

	strVar += "<p>&nbsp;<\/p>";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td><b>Where can I get help with reviewing or changing my orders?<\/b><br>";
	strVar += "		To learn more about managing your orders on shop.stc.com please visit ";
	strVar += "		our Help pages at shop.stc.com\/<br>";
	strVar += "		help\/orders\/.<br>";
	strVar += "		Please note: This e-mail message was sent from a notification-only ";
	strVar += "		address that cannot accept incoming email.<br>";
	strVar += "		Please do not reply to this message.<br>";
	strVar += "		If you ever need to return an order, visit our Online Returns Center: ";
	strVar += "		shop.stc.com\/returns<br>";
	strVar += "		Thanks again for shopping with STC!<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";

	//var authorId= nlapiGetUser();
	//var authorId= nlapiGetUser();
	var authorId= 59;
	nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', " authorId=== "+authorId);
	
	nlapiSendEmail(authorId, toList, subject, strVar, null, null, null, null);
	//nlapiSendEmail(nlapiGetUser(), toList, subject, strVar, ccList, null, null, null);

}