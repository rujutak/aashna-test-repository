/**
 * @author Shweta
 */
/**
 * @author Modified By Supriya  done changes
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_Approval_CustomerProject_Validation.js
	Date       : 23 May 2013
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
    if(type=='edit')
    {
		var approvalStatus=nlapiGetFieldValue('custbody_approval_status')
		if(approvalStatus==2)
		{
			//nlapiSetFieldValue('custbody_approval_status','',true)
			//nlapiSetFieldValue('custbody_next_approver','',true)
		}
    }
	
	var CurrentUserRole = nlapiGetRole();
	//alert('CurrentUserRole ==>'+CurrentUserRole)
	
	if(CurrentUserRole != 3)
	{
		//****************************Changed By Supriya on 24 july********************************************************************************
		if (CurrentUserRole != 1007 && CurrentUserRole != 1009 && CurrentUserRole != 3)
		{
			nlapiDisableField('custbody_next_approver',true)
		}
		
		nlapiDisableField('custbody_approval_status',true)
		nlapiDisableField('custbody_approved_by',true)
		nlapiDisableField('custbody_sendemail',true)
		nlapiDisableField('custbody_rejection_code',true)
               nlapiDisableField('custbody_approvalrejectionnote',true)
		
		
	}

	

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	
	
	
	
	
	
	
	
	
	var ProjectId = nlapiGetFieldValue('custbody_customer_project_name')
    //alert('ProjectId==>'+ProjectId)
	
	var ProjectManager = nlapiGetFieldValue('custbodyproject_manager')
	//alert('ProjectManager==>'+ProjectManager)
	
	
	if(ProjectManager != null && ProjectManager != undefined && ProjectManager !='')
	{
	return true;	
	}
	else
	{
	alert('There is no manager assigend for selected project.')	
	return false;
	}

	

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================


var flag=0;


// BEGIN FIELD CHANGED ==============================================

function fieldChanged_customer(type, name, linenum)
{
    /*  On field changed :

          - PURPOSE - To set the Customer with Customer Project
                      Do not allow user to enter in Customer first.


          FIELDS USED:

			Field Name --> Cutomer



    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));



//================== Customer Field Change =================================
    
    /*
if(name=='custbody_customer_project_name')
	{
	var ProjectIdValue = nlapiGetFieldValue('custbody_customer_project_name')	
    //alert('ProjectIdValue=='+ProjectIdValue)
	
		if(ProjectIdValue != null && ProjectIdValue != undefined && ProjectIdValue !='' )
		{
			//var ProjectRec = nlapiLoadRecord('job',ProjectIdValue)
			//alert('ProjectRec=='+ProjectRec)
			
			//var Isinternal = ProjectRec.getFieldValue('custentity_internal_project')
			//alert('Isinternal=='+Isinternal)
			//var Isinternal
			//alert('Isinternal=='+Isinternal)
			
			var Isinternal = nlapiLookupField('job', ProjectIdValue, 'custentity_internal_project');
			alert('Isinternal=='+Isinternal)
			
			if(Isinternal == 'T')
			{
				nlapiSetFieldValue('custbody_is_internal_project','T')
			}
			
			
			
			
		}
		
		
	}
*/





	 if(name=='customer')
	 {
	 	var b_is_internal_project=nlapiGetFieldValue('custbody_is_internal_project');
	//	alert(' b_is_internal_project -->'+b_is_internal_project);

		if(b_is_internal_project == 'F')
		{

	 	var blank='';

	 	var i_customer_project=nlapiGetFieldValue('custbody_customer_project_name');
		//alert(' Customer Project -->'+i_customer_project);

		var i_customer=nlapiGetCurrentLineItemValue('expense','customer');
		//alert(' Customer -->'+i_customer);

		var i_project=nlapiGetCurrentLineItemValue('expense','custcol_project_line');
		//alert(' Project Line -->'+i_project);

		// ************* If Customer Project does not contain any value  **************************

			if(i_customer_project == '' || i_customer_project == null)
			{
				if(i_customer != '' && i_customer != null && i_customer != undefined)
				{
					alert(' Enter Customer Project.');
					nlapiSetCurrentLineItemValue('expense','customer',blank,true,true);

				}// Customer Check

			}// Customer Project

			    if(i_customer_project != '' && i_customer_project != null &&  i_customer_project != undefined)
				{
					var o_projectOBJ=nlapiLoadRecord('job',i_customer_project);


					 	if(o_projectOBJ!=null && o_projectOBJ!='')
						{
							var i_customer_for_project = o_projectOBJ.getFieldValue('parent');

							if(i_customer != i_customer_for_project)
							{
								if(i_customer != '' && i_customer != null &&  i_customer != undefined)
								{
									alert(' Customer does not match with Customer Project .')
									nlapiSetCurrentLineItemValue('expense','customer',i_customer_for_project);
									nlapiSetCurrentLineItemValue('expense','custcol_project_line',i_customer_project);

								}// Customer

							}//Customer - Customer Project Check

						}//Customer OBJ


				}// Customer Project




		}


	 }// Customer Field Change

       if(name=='custcol_project_line')
	   {
	   	var blank='';

	   	var b_is_internal_project=nlapiGetFieldValue('custbody_is_internal_project');
		//alert(' b_is_internal_project -->'+b_is_internal_project);

		if(b_is_internal_project == 'F')
		{
				var i_customer_project=nlapiGetFieldValue('custbody_customer_project_name');
			//	alert(' Customer Project -->'+i_customer_project);

			    var i_project=nlapiGetCurrentLineItemValue('expense','custcol_project_line');
		   	   // alert(' Project -->'+i_project);

			    if(i_customer_project == '' || i_customer_project == null)
				{
					if(i_project != '' && i_project != null && i_project != undefined)
					{
						alert(' Enter Customer Project.');
						nlapiSetCurrentLineItemValue('expense','custcol_project_line',blank,true,true);

					}// Project Check

				}// Customer Project

				if(i_customer_project != '' && i_customer_project != null &&  i_customer_project != undefined)
				{
					if(i_customer_project!=i_project)
					{
						alert(' Project should match with Customer Project.')
						nlapiSetCurrentLineItemValue('expense','custcol_project_line',i_customer_project,true,true);
					}

				}

		}
		else if(b_is_internal_project == 'T')
		{
			 var i_project=nlapiGetCurrentLineItemValue('expense','custcol_project_line');
		  // 	 alert(' Project -->'+i_project);

			 if(i_project!=null && i_project!='' && i_project!= undefined)
			 {
			 	var o_projectOBJ=nlapiLoadRecord('job',i_project);
			//	 alert(' Project Object  -->'+o_projectOBJ);

			 	 if(o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined)
				 {

				    var b_internal_project = o_projectOBJ.getFieldValue('custentity_internal_project');
				//	alert(' Internal Project -->'+b_internal_project)

				    if(b_internal_project == 'F')
					{
						alert(' You have entered a non - internal Project \n  Only Internal Projects are allowed .')
						nlapiSetCurrentLineItemValue('expense','custcol_project_line',blank,true,true);

						return false;

					}





				 }//Project OBJ

			 }//Project Check

		}//I sInternal Project Check


	   }

return true;

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
