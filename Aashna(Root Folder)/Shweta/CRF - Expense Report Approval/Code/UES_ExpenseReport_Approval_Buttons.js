/**
 * @author Shweta
 */
/**
 * @Updated By Supriya
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_ExpenseReport_Approval_Buttons.js
	Date        : 23 May 2013
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   6  Jun 2013             Supriya                                                      Changed condition of displaying buttons.
   13 jun 2013             Supriya                                                      Hide Standard approve & reject buttons.
   13 jun 2013             Supriya                                                      To show submit for approval button after rejection
   8 july 2013
                                                                                                                                            
  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_button(type,form)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY

      var blank='';
	  
	    var i_approval_status = nlapiGetFieldValue('custbody_approval_status');
			
		var CurrentUserRole = nlapiGetRole();
				
		if(i_approval_status == 2)
		{
			nlapiSetFieldValue('complete', 'F');
			
		}
	  
	  
	  
	  
       var buttonOBJ1=form.getButton('approve');
	 	  
	     if(buttonOBJ1!=null)
		  {
			 buttonOBJ1.setVisible(false);
		  }
	  
	   var buttonOBJ2=form.getButton('reject');
	   	  
	      if(buttonOBJ2!=null)
		  {
	
			buttonOBJ2.setVisible(false);
		  }


      if(type== 'create')
	  {
	  	 nlapiSetFieldValue('custbody_approval_status',blank)
	  }
	 
	 if(type=='view')
	 {
	 	
	  var b_complete = nlapiGetFieldValue('complete');
	
	 
	  var approval_status_current = nlapiGetFieldValue('custbody_approval_status');
	 
	  
	  //****************************Changed By Supriya on 24 july********************************************************************************
	 
     if((b_complete == 'T')  && (approval_status_current != 2 ) && (CurrentUserRole != 1007 && CurrentUserRole != 1009 && CurrentUserRole != 3))
	 {
		 var buttonOBJ=form.getButton('edit');
				 
		  if(buttonOBJ!=null)
		  {
		    buttonOBJ.setDisabled(true);
		  }
		
	  }	

	var CurrentUserRole = nlapiGetRole();
	
	if(CurrentUserRole == 3)
	{
		 if((approval_status_current == 2))
		 {
		 	 var buttonOBJ=form.getButton('edit');
						 
			  if(buttonOBJ!=null)
			  {
			    buttonOBJ.setDisabled(false);
			  }
					
		 }
				
	}
	 	 
	  var i_recordID = nlapiGetRecordId();
	  var s_record_type=nlapiGetRecordType();
	 
	 if(i_recordID != null && i_recordID !=undefined && i_recordID!='')
	 {
		
		 var o_recordOBJ= nlapiLoadRecord(s_record_type, i_recordID);
	    
		 var i_internal_project = o_recordOBJ.getFieldValue('custbody_is_internal_project');
	     
		 var i_approval_status=o_recordOBJ.getFieldValue('custbody_approval_status');
	    
	     var next_approver = o_recordOBJ.getFieldValue('custbody_next_approver');
		 
		 var approval_status_current = o_recordOBJ.getFieldValue('custbody_approval_status');
		
	    var i_user_id=nlapiGetUser();
	  	
	     var i_user_role=nlapiGetRole();
		
		var i_employee = o_recordOBJ.getFieldValue('entity')
		
	     if(i_user_role == 15 || i_user_role == 3 || i_user_role == 18 )
		 {		 	
			  if(i_approval_status == '' || i_approval_status == null || i_approval_status == 2)
			  {		  			
				
				if(i_internal_project == 'T')
				{
				
				}
				else if(i_internal_project == 'F')
				{
								
				}
				if (b_complete == 'F')
				{
					form.setScript('customscript_expensereport_completelater');
					form.addButton('custpage_complete_later', ' Complete Later ', 'complete_later_check(' + i_recordID + ')');
				}
				
			  }		 	
			
		 }
	  
	 
	 var approved_by = nlapiGetFieldValue('custbody_approved_by');
		  
	
	  {
	  	
	if( (i_user_id == next_approver) || ( i_user_role== 1014)|| ( i_user_role== 1024)|| ( i_user_role== 1009) || (i_user_role== 1007) || ( (i_user_role == 3 || i_user_role == 18 )&& (i_user_id != i_employee && i_user_role != 15)  ) )
	{
	   	      if(  (( (i_approval_status == 4) || (i_approval_status == 5) || (i_approval_status == 6) || (i_approval_status == 7) ) && (i_user_id == next_approver) ) || (i_approval_status == 8) || (i_approval_status == 9) || (i_approval_status == 10))
			  {
			    	if(i_internal_project == 'T')
					{
						if(approved_by != i_user_id)
						{
							 form.setScript('customscript_expense_approve_noncustomer');		
			  	             form.addButton('custpage_approve_noncust',' Approve','approve_noncustomer_report('+i_recordID+')');
						
						}
						
					}
					else if(i_internal_project == 'F')
					{
						if(approved_by != i_user_id)
						{
							 form.setScript('customscript_cli_expensereport_approve');		
			  	             form.addButton('custpage_approve',' Approve','approve_report('+i_recordID+')');
													
						}
										
					}
				 
				 	if(approved_by != i_user_id)
					{
					   form.setScript('customscript_cli_expensereport_reject');	
				       form.addButton('custpage_reject',' Reject','reject_report('+i_recordID+')');
						
					}
				 
			  }
	   	
	    }
	  	
	  }
			
	 }
			
	 }
		 
	return true;

}

// END BEFORE LOAD ====================================================


// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_complete(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	
	
	
	if (type == 'create')
	{
		var b_complete = nlapiSetFieldValue('complete', 'F');
		
	}
	
	
	
		var i_approval_status = nlapiGetFieldValue('custbody_approval_status');
		
		if(i_approval_status == 2)
		{
			nlapiSetFieldValue('complete', 'F');
			
		}
		
	
	   if(type == 'edit')
	   {
	   	var contextObj = nlapiGetContext();
    
        var newType = contextObj.getExecutionContext();
      		
		if(newType == 'userinterface')
		{
			var user_role = nlapiGetRole();
	    
	    var approval_status = nlapiGetFieldValue('custbody_approval_status');
	    
		if(user_role!= 3 && user_role!= 15 && user_role!= 1009 && user_role!= 1007 && approval_status!= 2 && approval_status!= 1)
		{
			throw 'This expense report cannot be edited while pending approval. Please contact an administrator to override.';	
	       return false;
			
		}		
			
		}
		
		   
	   	
		
		
	   }
	
	
	
	
	
	
	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_expense(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	var contextObj = nlapiGetContext();
    
   var newType = contextObj.getExecutionContext();
             
	
	try
	{
		
	if(type == 'create' || type == 'edit')
	{
		
	var i_user = nlapiGetUser();
	
	var s_user = name_employee(i_user);

	var i_user_role = nlapiGetRole();
	
	var i_record_id=nlapiGetRecordId();
		
	var s_record_type=nlapiGetRecordType();
	
	var o_recordOBJ=nlapiLoadRecord(s_record_type, i_record_id);
	
	var ProjectIdValue=o_recordOBJ.getFieldValue('custbody_customer_project_name');
	
	var approval_status=o_recordOBJ.getFieldValue('custbody_approval_status');
	
	var next_approver_new=o_recordOBJ.getFieldValue('custbody_next_approver');
	
	var s_next_approver_new = name_employee(next_approver_new);
	
	var EXpenseseReprotTranId= o_recordOBJ.getFieldValue('tranid');
	
	var ExpenseReportOldRec = nlapiGetOldRecord();
	
	var i_approval_OLdstatus;
	var i_nextapprover_Old;
	
	if(ExpenseReportOldRec != null && ExpenseReportOldRec != undefined && ExpenseReportOldRec !='')
	{
	 i_approval_OLdstatus = ExpenseReportOldRec.getFieldValue('custbody_approval_status');
	
	}
	
	
	if((type == 'edit') && ( (i_user_role==3) || (i_user_role==1009)|| (i_user_role==1007)) )
	{
		
	var contextObj = nlapiGetContext();
    var newType = contextObj.getExecutionContext();	
	
	if(newType == 'userinterface')
	{
	var OldRec = nlapiGetOldRecord();
	nlapiLogExecution('DEBUG',' Expense Report ',' OldRec  -->'+OldRec);
	
	var i_approval_OLdstatus;
	var i_nextapprover_Old;
	
	if(OldRec != null && OldRec != undefined && OldRec !='')
	{
	  i_approval_OLd = OldRec.getFieldValue('custbody_approval_status');
	 
	  i_nextapprover_Old = OldRec.getFieldValue('custbody_next_approver');
	
	}
	
		
		if ( (approval_status == i_approval_OLdstatus) && (next_approver_new != i_nextapprover_Old) )
		{
				
		if(approval_status == 5)
		{
			level = ' Program Director '
			send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,next_approver_new,s_next_approver_new,s_user);
		}
		else if(approval_status == 4)
		{
			level = ' Project Manager '
			send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,next_approver_new,s_next_approver_new,s_user);
		}
		else if(approval_status == 6)
		{
			level = ' Supervisor '
			send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,next_approver_new,s_next_approver_new,s_user);
		}
		else if(approval_status == 7)
		{
			level = ' Stream Leader '
			send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,next_approver_new,s_next_approver_new,s_user);
		}
        else if(approval_status == 8)
		{
			level = ' Executive '
			send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,next_approver_new,s_next_approver_new,s_user);
		}
			
			
		}
		
		
	}
	
		
	}
	
	
	
	
		if (ProjectIdValue != null && ProjectIdValue != undefined && ProjectIdValue != '')	
		 {
					
			var Isinternal = nlapiLookupField('job', ProjectIdValue, 'custentity_internal_project');
			
			if (Isinternal == 'T') 	
			{
				o_recordOBJ.setFieldValue('custbody_is_internal_project', 'T')
				
					o_recordOBJ.setFieldValue('complete','T');
					
					if (type == 'create') 
					{
						
						var i_amount = o_recordOBJ.getFieldValue('amount');
					
						var i_expenselineCount = o_recordOBJ.getLineItemCount('expense')
					
						var i_currancy = 0;
						
						if(i_expenselineCount != null && i_expenselineCount != undefined && i_expenselineCount != '')
						{
						i_currancy = o_recordOBJ.getLineItemValue('expense','currency',1)	
					
						}
									
						var i_employee = o_recordOBJ.getFieldValue('entity');
						
						var s_employee = name_employee(i_employee);
						
						if (i_employee != null && i_employee != '' && i_employee != undefined) 
						{
							var o_employee_OBJ = nlapiLoadRecord('employee', i_employee);
						
							if (o_employee_OBJ != null && o_employee_OBJ != '' && o_employee_OBJ != undefined) 
							{							
								var i_supervisor = o_employee_OBJ.getFieldValue('supervisor');
						     								
								var s_supervisor = name_employee(i_supervisor);
													
							if(i_supervisor!=null && i_supervisor!=undefined && i_supervisor!='')
							{
							   var is_streamleader = is_supervisor_streamleader(i_supervisor)
													
						       var is_Exceutive =   is_supervisor_streamleader_ceo_cfo(i_supervisor);
							 								
							}
													
							   var Supervisoramount = ' '
						
								if(i_currancy == 4  )
								{
								 Supervisoramount = 500 ;	
								}
								else if (i_currancy == 6 || i_currancy == 1)
								{
								 Supervisoramount = 500 ;	 
								}
							
							
							   var StreamLeaderAmount = '';
			
								if(i_currancy == 4  )
								{
								 StreamLeaderAmount = 5000 ;	
								}
								else if (i_currancy == 6 || i_currancy == 1)
								{
								 StreamLeaderAmount = 4000 ;	 
								}
							
							
							    var CEO_CFOAmount = '';
			
								if(i_currancy == 4  )
								{
								 CEO_CFOAmount = 5000 ;	
								}
								else if (i_currancy == 6 || i_currancy == 1)
								{
								 CEO_CFOAmount = 4000 ;	 
								}
							
							
							//================================ Start - Stream Leader ==  Supervisor =====================
											
							
						    var is_CEO = CEO_EmployeeCentre_Role(i_user);
							
							 var search_CFO =  CFO_Search()
							 
							 var is_CFO = CFO_EmployeeCentre_Role(i_user);
							 
                             if(is_CFO ==  true)
							 {
							 	CFO_Approval(o_recordOBJ,type , i_approval_OLdstatus , newType ,i_user,s_user,i_user_role,i_record_id,s_record_type)
							 }                            
                             else if(is_CEO ==  true)
							 {
							 	new_CEO_Approval(o_recordOBJ,type , i_approval_OLdstatus , newType ,i_user,s_user,i_user_role,i_record_id,s_record_type) ;
							 }
							 else
							 {
							 	
								try
								{
										
							 	
							    if(((i_amount>Supervisoramount) ) &&  (is_streamleader ==  true) && (is_Exceutive!=true) )
								{															 
								   o_recordOBJ.setFieldValue('custbody_approval_status',7);
			                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
								   
								   
								    // ============= Email Sent for Supervisor  =======================

								 		var author_pm=i_user;
										var recipient_cfo= i_supervisor;
								        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Stream Leader Approval .';
										var body_pm=" ";
									   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
										body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
										body_pm+= " Expense Report is Pending For your Approval <br\/> ";
										body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
										body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
										body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
										body_pm+="Regards,<br\/><br\/>";
										body_pm+=" "+s_user+"<br\/>";
										
										 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
										  {												  
										  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
											  {													
											  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
					
											  }
					
										  }  
								
									
								}// Stream Leader / Supervisor
							  																
							//================================ END - Stream Leader ==  Supervisor =====================
							 
							 else if((i_amount > CEO_CFOAmount) && (is_Exceutive == true))
							 {							 	
								
								  o_recordOBJ.setFieldValue('custbody_approval_status',8);
		                          o_recordOBJ.setFieldValue('custbody_next_approver','');
							   							   
							    nlapiLogExecution('DEBUG', ' Expense Report ', '***** i_supervisor set ****  -->' + i_supervisor);
									
									var user_id = nlapiGetUser();
										
									if(i_supervisor!=null && i_supervisor!=undefined && i_supervisor!='')
									{
										var is_CEO = isSupervior_CEO(i_supervisor) ;
									    var is_CFO  = isSupervior_CFO(i_supervisor) ;
									
									}	
																							      
									
									if(is_CFO == true)
									{												
									// =============== CFO ============================== 
									
									var columns=new Array();
									var filters=new Array();
											   
									columns[0] = new nlobjSearchColumn('internalid');
													
									filters[0] = new nlobjSearchFilter('role', null, 'is', 1014);
																					
									var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
												
									if (searchresults != null)
									{
									   	for (var k = 0; k < searchresults.length; k++) 
										{
											var i_cfo = searchresults[k].getValue('internalid');
											
											var s_cfo = name_employee(i_cfo);
																
									
									// ============= Email Sent for CFO  =======================
								
							 		var author_pm=i_user;		 			
									var recipient_cfo= i_cfo;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending CFO Approval';
									var body_pm=" ";    
								   	body_pm+="Hello "+s_cfo+", <br\/><br\/>"; 
									body_pm+= " Expense Report:"+ EXpenseseReprotTranId+"is submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  	
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										   
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
											
										  }
									  						
									  }
											
											
										}
										
										
									}
										
										
										
									}//CFO Mail Sent
																
							
							        if(is_CEO == true)
									{
											
									// =============== CEO ============================== 
																
							
									var columns=new Array();
									var filters=new Array();
											   
									columns[0] = new nlobjSearchColumn('internalid');
													
									filters[0] = new nlobjSearchFilter('role', null, 'is', 1024);
																					
									var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
												
									if (searchresults != null)
									{									  
										for (var k = 0; k < searchresults.length; k++) 
										{
											i_ceo = searchresults[k].getValue('internalid');
																					
											s_ceo = name_employee(i_ceo);
										
									// ============= Email Sent for CEO  =======================
								
							 		var author_pm=i_user;		 			
									var recipient_ceo= i_ceo;
							        var subject_ceo =' Expense Report  '+EXpenseseReprotTranId+' Pending CEO Approval';
									var body_pm=" ";    
								   	body_pm+="Hello "+s_ceo+", <br\/><br\/>"; 
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is submitted. <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  										
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  
										  	nlapiSendEmail(author_pm, recipient_ceo, subject_ceo, body_pm );
											
										  }
									  						
									  }											
											
										}
										
										
									}
										
									}//CEO Mail sent
							
									 nlapiLogExecution('DEBUG', ' Expense Report ', '***** Email Formazt  ****  -->' + i_supervisor);
	
							 }//Exceutive Amount
							 else 
							{
								
							  nlapiLogExecution('DEBUG',' Expense Report ','  Supervisor ....');
								
						   	   o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
							   
							   
							    // ============= Email Sent for Supervisor  =======================

							 		var author_pm=i_user;
									var recipient_cfo = i_supervisor;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_user+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {									  	
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {				
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
														
										  }
				
									  }  
																	
							}
						  					
									
								}
								catch(we)
								{
									 nlapiLogExecution('DEBUG',' Expense Report ',' we....'+we);
									
								}
								
							 }
							
							}
							
							
						}
						
						
						 var UpdatedExpenseReport = nlapiSubmitRecord(o_recordOBJ , true ,true)
				         nlapiLogExecution('DEBUG',' Expense Report ','IF UpdatedExpenseReport  -->'+UpdatedExpenseReport);
					}
				
				        if( (is_CFO ==  true) || (is_CEO ==  true))
						{
							var o_recordOBJ=nlapiLoadRecord(s_record_type, i_record_id);
						}
				
				
				var CheckFlag = o_recordOBJ.getFieldValue('custbody_is_rejected');
								
				var contextObj = nlapiGetContext();
    
                var newType = contextObj.getExecutionContext();
   		  
				  if(i_approval_OLdstatus == 2 &&  type == 'edit' && newType != 'userevent')
		            {
					
						var RejectionCode =  o_recordOBJ.getFieldValue('custbody_rejection_code');
												
						var RejectionNote = o_recordOBJ.getFieldValue('custbody_approvalrejectionnote');
									
						
						var i_user_role = nlapiGetRole();
						
						if (i_user_role == 1007 || i_user_role == 1009) 	
						{
							var accountingApproval = o_recordOBJ.getFieldValue('accountingapproval')
														
							if (accountingApproval == 'T') 	
							{
								
								var i_employee = o_recordOBJ.getFieldValue('entity');
																
								var s_employee = name_employee(i_employee);
								
							    var i_finance_user = nlapiGetUser();
								var i_finance_role = nlapiGetRole();
								
								if (i_finance_user != '' && i_finance_user != null && i_finance_user != undefined) 
								{								
										
										var s_finance_user = name_employee(i_finance_user)
										o_recordOBJ.setFieldValue('custbody_approval_status',1);
								        o_recordOBJ.setFieldValue('custbody_next_approver','');
								        o_recordOBJ.setFieldValue('custbody_approved_by',i_finance_user);
										o_recordOBJ.setFieldValue('supervisorapproval','T');
										o_recordOBJ.setFieldValue('accountingapproval','T');
																				
										o_recordOBJ.setFieldValue('custbody_rejection_code','');
									    o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
																			
									// ============= Email Sent for Finance  =======================
								
							 		author_pm = i_finance_user;		 			
									recipient_cfo= i_employee;
							        subject_cfo =" Expense Report: " +EXpenseseReprotTranId+" is Approved  ";
									var body_pm=" ";    
								   	body_pm+= " Expense Report submitted by  "+s_employee+" is approved <br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
																	
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									 										
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										 
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
											
											 nlapiLogExecution('DEBUG', ' Expense Report ', '*Email Approved Sent  -->' + accountingApproval);
																		   
										  }
									  						
									  }
									
								}//Finance User 
							 
							
							}//if (accountingApproval == 'T') 	
							
						}//if (i_user_role == 1007 || i_user_role == 1009) 
						else
						{
							
							
							
							 nlapiLogExecution('DEBUG', ' Expense Report ','Else Block Of Edit Customer ..')
							
				                var i_employee = o_recordOBJ.getFieldValue('entity');
								
								var s_employee = name_employee(i_employee);
								
								var user_id = nlapiGetUser();
								
								if (i_employee != null && i_employee != '' && i_employee != undefined) 
								{
												var o_employee_OBJ = nlapiLoadRecord('employee', i_employee);
												
												if (o_employee_OBJ != null && o_employee_OBJ != '' && o_employee_OBJ != undefined) 
												{
												
												var i_supervisor = o_employee_OBJ.getFieldValue('supervisor');
												
												var s_supervisor = name_employee(i_supervisor);
												
												var is_employee_CEO = CEO_EmployeeCentre_Role(i_employee)
												nlapiLogExecution('DEBUG', ' Expense Report ',' is_employee_CEO -->'+is_employee_CEO)
																									
												var is_employee_CFO = CFO_EmployeeCentre_Role(i_employee)
												nlapiLogExecution('DEBUG', ' Expense Report ',' is_employee_CFO -->'+is_employee_CFO)
																								
												var is_CEO;
												if(i_supervisor!=null && i_supervisor!='' &&i_supervisor!=undefined)
												{
													 is_CEO = CEO_EmployeeCentre_Role(i_supervisor)
													
												}
												var is_CFO = CFO_EmployeeCentre_Role(user_id)
			
			                                    nlapiLogExecution('DEBUG', ' Expense Report ','i_supervisor -->'+i_supervisor)
			                                    nlapiLogExecution('DEBUG', ' Expense Report ','is_CEO -->'+is_CEO)
												nlapiLogExecution('DEBUG', ' Expense Report ','is_CFO -->'+is_CFO)
			
			
			
														 	
												 if(i_supervisor!=null && i_supervisor!=undefined && i_supervisor!='')
												{
													  var is_streamleader = is_supervisor_streamleader(i_supervisor)
												   
											           var is_Exceutive =   is_supervisor_streamleader_ceo_cfo(i_supervisor);
												 															
												}
												
												 nlapiLogExecution('DEBUG', ' Expense Report ','is_streamleader -->'+is_streamleader)
												 nlapiLogExecution('DEBUG', ' Expense Report ','is_Exceutive -->'+is_Exceutive)
			
							
							                    var i_amount = o_recordOBJ.getFieldValue('amount');
								
									
												var i_expenselineCount = o_recordOBJ.getLineItemCount('expense')
												
												var i_currancy = 0;
												
												if(i_expenselineCount != null && i_expenselineCount != undefined && i_expenselineCount != '')
												{
												i_currancy = o_recordOBJ.getLineItemValue('expense','currency',1)	
												
												}
							
											   var Supervisoramount = ' '
										
												if(i_currancy == 4  )
												{
												 Supervisoramount = 500 ;	
												}
												else if (i_currancy == 6 || i_currancy == 1)
												{
												 Supervisoramount = 500 ;	 
												}
											
											
											   var StreamLeaderAmount = '';
							
												if(i_currancy == 4  )
												{
												 StreamLeaderAmount = 5000 ;	
												}
												else if (i_currancy == 6 || i_currancy == 1)
												{
												 StreamLeaderAmount = 4000 ;	 
												}
											
											
											    var CEO_CFOAmount = '';
							
												if(i_currancy == 4  )
												{
												 CEO_CFOAmount = 5000 ;	
												}
												else if (i_currancy == 6 || i_currancy == 1)
												{
												 CEO_CFOAmount = 4000 ;	 
												}
							
							//================================ Start - Stream Leader ==  Supervisor =====================
							
							    if(((i_amount>Supervisoramount) ) &&  (is_streamleader ==  true) && (is_Exceutive!=true)  && (is_CEO == false) && (is_CFO == false))
								{	
								
								nlapiLogExecution('DEBUG', ' Expense Report ',' First Block .....')
																		
															 
										   o_recordOBJ.setFieldValue('custbody_approval_status',7);
					                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
										   o_recordOBJ.setFieldValue('custbody_rejection_code','');
								           o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
										   
										    // ============= Email Sent for Supervisor  =======================
		
										 		var author_pm=i_employee;
												var recipient_cfo= i_supervisor;
										        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Stream Leader Approval .';
												var body_pm=" ";
											   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
												body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
												body_pm+= " Expense Report is Pending For your Approval <br\/> ";
												body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
												body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
												body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
												body_pm+="Regards,<br\/><br\/>";
												body_pm+=" "+s_user+"<br\/>";
												
												 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
												  {
												  	
												  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
													  {
							
													  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
							
														
													  }
							
												  }  
										
								}// Stream Leader / Supervisor
							  																
							//================================ END - Stream Leader ==  Supervisor =====================
							 
							 else if((i_amount > CEO_CFOAmount) && (is_Exceutive == true) && (is_CEO == false) && (is_CFO == false))
							 {	
							 nlapiLogExecution('DEBUG', ' Expense Report ',' Second Block .....')
														 	
							 
								  o_recordOBJ.setFieldValue('custbody_approval_status',8);
		                          o_recordOBJ.setFieldValue('custbody_next_approver','');
							      o_recordOBJ.setFieldValue('custbody_rejection_code','');
								  o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
							   
							    if (i_supervisor != null && i_supervisor != undefined && i_supervisor != '') 
								{
								
									var is_CEO = isSupervior_CEO(i_supervisor);
									var is_CFO = isSupervior_CFO(i_supervisor);
								}
									
									if(is_CFO == true)
									{
												
									// =============== CFO ============================== 
									
									var columns=new Array();
									var filters=new Array();
											   
									columns[0] = new nlobjSearchColumn('internalid');
													
									filters[0] = new nlobjSearchFilter('role', null, 'is', 1014);
																					
									var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
												
									if (searchresults != null)
									{
									 	for (var k = 0; k < searchresults.length; k++) 
										{
											var i_cfo = searchresults[k].getValue('internalid');
										
											var s_cfo = name_employee(i_cfo);
										
									
									
									// ============= Email Sent for CFO  =======================
								
							 		var author_pm=i_employee;		 			
									var recipient_cfo= i_cfo;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending CFO Approval';
									var body_pm=" ";    
								   	body_pm+="Hello "+s_cfo+", <br\/><br\/>"; 
									body_pm+= " Expense Report:"+ EXpenseseReprotTranId+"is submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  	 
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
											
										  }
									  						
									  }
											
											
										}
										
										
									}
										
									}//CFO Mail Sent
																
							
							        if(is_CEO == true)
									{
											
									// =============== CEO ============================== 
																		
							
									var columns=new Array();
									var filters=new Array();
											   
									columns[0] = new nlobjSearchColumn('internalid');
													
									filters[0] = new nlobjSearchFilter('role', null, 'is', 1024);
									
												
									var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
												
									if (searchresults != null)
									{
									    for (var k = 0; k < searchresults.length; k++) 
										{
											i_ceo = searchresults[k].getValue('internalid');
										
											s_ceo = name_employee(i_ceo);
										
									
									
									// ============= Email Sent for CEO  =======================
								
							 		var author_pm=i_employee;		 			
									var recipient_ceo= i_ceo;
							        var subject_ceo =' Expense Report  '+EXpenseseReprotTranId+' Pending CEO Approval';
									var body_pm=" ";    
								   	body_pm+="Hello "+s_ceo+", <br\/><br\/>"; 
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is submitted. <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									
									
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  	
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  
										  	nlapiSendEmail(author_pm, recipient_ceo, subject_ceo, body_pm );
										
											   
										  }
									  						
									  }
											
											
										}
										
										
									}
										
										
										
									}//CEO Mail sent
							
							   
							 }//Exceutive Amount
							 else if (is_employee_CEO == true)
							 {
							 	
								nlapiLogExecution('DEBUG', ' Expense Report ',' Third Block .....')
							 	var cfo_search = CFO_Search();
								nlapiLogExecution('DEBUG', ' Expense Report ',' cfo_search .....'+cfo_search)
								
								var s_cfo_search = name_employee(cfo_search);
								nlapiLogExecution('DEBUG', ' Expense Report ',' s_cfo_search .....'+s_cfo_search)
									
							   o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                       o_recordOBJ.setFieldValue('custbody_next_approver',cfo_search);
							   o_recordOBJ.setFieldValue('custbody_rejection_code','');
							   o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
							  							  
							    // ============= Email Sent for Supervisor  =======================

							 		var author_pm=i_employee;
									var recipient_cfo = cfo_search;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_cfo_search+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_user+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {						  	
				
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  	
											nlapiLogExecution('DEBUG', ' Expense Report ',' Send Email....')
												
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
				
										  }
				
									  } 
								
								
								
								
							 }
							 
							 
							  else if (is_employee_CFO == true)
							 {
							 	nlapiLogExecution('DEBUG', ' Expense Report ',' Fourth Block .....')
							 										
							   o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
							   o_recordOBJ.setFieldValue('custbody_rejection_code','');
							   o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
							  
							  
							    // ============= Email Sent for Supervisor  =======================

							 		var author_pm=i_employee;
									var recipient_cfo = i_supervisor;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_user+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {						  	
				
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
				
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
				
										  }
				
									  } 
								
								
								
								
							 }
							 						 
							 
							 else 
							{
								nlapiLogExecution('DEBUG', ' Expense Report ',' Fifth Block .....')
						
						   	   o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
							   o_recordOBJ.setFieldValue('custbody_rejection_code','');
							   o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
							  							  
							    // ============= Email Sent for Supervisor  =======================

							 		var author_pm=i_employee;
									var recipient_cfo = i_supervisor;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_user+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {						  	
				
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
				
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
				
										  }
				
									  }  
																	
							}
									
											          }
											
											
										    }
								 	
						}
						
		            }

				  // ===================  Start - Finacial Approval ===========================
										
				
					 var app_status = o_recordOBJ.getFieldValue('custbody_approval_status');
					 
					 var i_employee = o_recordOBJ.getFieldValue('entity');
										 
					 var s_employee = name_employee(i_employee);
					 
					  var account_approval_check = o_recordOBJ.getFieldValue('accountingapproval');
					 
					if((app_status == 9) && (account_approval_check == 'T'))
					{
					
				var i_finance_user = nlapiGetUser();
				var i_finance_role = nlapiGetRole();
				
				if (i_finance_user != '' && i_finance_user != null && i_finance_user != undefined) 
				{				
					{
						
						var s_finance_user = name_employee(i_finance_user)
						o_recordOBJ.setFieldValue('custbody_approval_status',1);
				        o_recordOBJ.setFieldValue('custbody_next_approver','');
				        o_recordOBJ.setFieldValue('custbody_approved_by',i_finance_user);
						o_recordOBJ.setFieldValue('supervisorapproval','T');
						o_recordOBJ.setFieldValue('accountingapproval','T');
								
					// ============= Email Sent for Finance  =======================
				
			 		author_pm = i_finance_user;		 			
					recipient_cfo= i_employee;
			        subject_cfo =" Expense Report: " +EXpenseseReprotTranId+" is Approved  ";
					var body_pm=" ";    
				   	body_pm+= " Expense Report submitted by  "+s_employee+" is approved <br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									
					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					   	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {
						 	  
						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
							   
						  }
					  						
					  }
					
						
						
					}
					
					
				}//Finance User 			
					
						
					}
					
					
					
					
				 // ===================  Finacial Approval ===========================
				
					
			
		        var UpdatedExpenseReport = nlapiSubmitRecord(o_recordOBJ , true ,true)
			
			}
			else
			{
				
				o_recordOBJ.setFieldValue('custbody_is_internal_project', 'F')
				
				
				 o_recordOBJ.setFieldValue('complete','T');
				 
				 
				 if(type =='create')
				 {
				 	
					var is_CEO = CEO_EmployeeCentre_Role(i_user);
				
					var is_CFO = CFO_EmployeeCentre_Role(i_user);
					
				 	  var i_project_manager=o_recordOBJ.getFieldValue('custbodyproject_manager');
				      var s_project_manager  = name_employee(i_project_manager) ;
				 
					  o_recordOBJ.setFieldValue('custbody_approval_status',4);
				      o_recordOBJ.setFieldValue('custbody_next_approver',i_project_manager);
					  
					 // ============= Email Sent for Project Manager  =======================

			 		var author_pm=i_user;
					var recipient_cfo= i_project_manager;
			        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Project Manager Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_project_manager+", <br\/><br\/>";
					body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
					body_pm+="Regards,<br\/><br\/>";
					body_pm+=" "+s_user+"<br\/>";
					
					 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	
					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

						  }

					  } 
						
				 }
				 
		
			var CheckFlag = o_recordOBJ.getFieldValue('custbody_is_rejected');
			
		
               if(i_approval_OLdstatus == 2 &&  type == 'edit' && newType != 'userevent'  )
				 {
				 	  
					  var RejectionCode =  o_recordOBJ.getFieldValue('custbody_rejection_code');
					 
					  var RejectionNote = o_recordOBJ.getFieldValue('custbody_approvalrejectionnote');
					  
					  var i_project_manager=o_recordOBJ.getFieldValue('custbodyproject_manager');
				      var s_project_manager  = name_employee(i_project_manager) ;
					  
					
					    var i_user_role = nlapiGetRole();
						
						if (i_user_role == 1007 || i_user_role == 1009) 	
						{
							var accountingApproval = o_recordOBJ.getFieldValue('accountingapproval')
							
							if (accountingApproval == 'T') 	
							{
							
							   var i_employee = o_recordOBJ.getFieldValue('entity');
					 
					 var s_employee = name_employee(i_employee);
					 
							 
							    var i_finance_user = nlapiGetUser();
								var i_finance_role = nlapiGetRole();
								
								if (i_finance_user != '' && i_finance_user != null && i_finance_user != undefined) 
								{
															
										var s_finance_user = name_employee(i_finance_user)
										
										o_recordOBJ.setFieldValue('custbody_approval_status',1);
								        o_recordOBJ.setFieldValue('custbody_next_approver','');
								        o_recordOBJ.setFieldValue('custbody_approved_by',i_finance_user);
										o_recordOBJ.setFieldValue('supervisorapproval','T');
										o_recordOBJ.setFieldValue('accountingapproval','T');
										o_recordOBJ.setFieldValue('custbody_rejection_code','');
									    o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
									
									
									// ============= Email Sent for Finance  =======================
								
								
							 		author_pm = i_finance_user;		 			
									recipient_cfo= i_employee;
							      
									
									subject_cfo =" Expense Report: " +EXpenseseReprotTranId+" is Approved  ";
									var body_pm=" ";    
								   	body_pm+= " Expense Report submitted by  "+s_employee+" is approved <br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
								
																	
									
									  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  	
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
										
										  }
									  						
									  }
									
									
								}//Finance User 
							
								
							}//if (accountingApproval == 'T') 
							
						}//if (i_user_role == 1007 || i_user_role == 1009) 
						else
						{
					
							  o_recordOBJ.setFieldValue('custbody_approval_status',4);
						      o_recordOBJ.setFieldValue('custbody_next_approver',i_project_manager);
							  o_recordOBJ.setFieldValue('custbody_rejection_code','');
							  o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
							  
							 // ============= Email Sent for Project Manager  =======================
		
					 		var author_pm=i_user;
							var recipient_cfo= i_project_manager;
					        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Project Manager Approval .';
							var body_pm=" ";
						   	body_pm+="Hello "+s_project_manager+", <br\/><br\/>";
							body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
							body_pm+= " Expense Report is Pending For your Approval <br\/> ";
							body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
							body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
							body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
							body_pm+="Regards,<br\/><br\/>";
							body_pm+=" "+s_user+"<br\/>";
							
							 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
							  {
							   	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
								  {
		
								  	 nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
		                             
								  }
		
							  }
					  	
					       }
						
					   
				 }
				

				  // ===================  Start - Finacial Approval ===========================
					
					
				
					 var app_status = o_recordOBJ.getFieldValue('custbody_approval_status');
										 
					 var i_employee = o_recordOBJ.getFieldValue('entity');
										 
					 var s_employee = name_employee(i_employee);
					 
					  var account_approval_check = o_recordOBJ.getFieldValue('accountingapproval');
										
					if((app_status == 9) && (account_approval_check == 'T'))
					{
					
				var i_finance_user = nlapiGetUser();
				var i_finance_role = nlapiGetRole();
				
				if (i_finance_user != '' && i_finance_user != null && i_finance_user != undefined) 
				{
				
					{
						
						var s_finance_user = name_employee(i_finance_user)
						o_recordOBJ.setFieldValue('custbody_approval_status',1);
				        o_recordOBJ.setFieldValue('custbody_next_approver','');
				        o_recordOBJ.setFieldValue('custbody_approved_by',i_finance_user);
						o_recordOBJ.setFieldValue('supervisorapproval','T');
						o_recordOBJ.setFieldValue('accountingapproval','T');
					
					// ============= Email Sent for Finance  =======================
				
			 		author_pm = i_finance_user;		 			
					recipient_cfo= i_employee;
			        subject_cfo =" Expense Report: " +EXpenseseReprotTranId+" is Approved  ";
					var body_pm=" ";    
				   	body_pm+= " Expense Report submitted by  "+s_employee+" is approved <br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
				
					
					
					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	
					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {
						  
						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
														
						  }
					  						
					  }
					
						
						
					}
					
					
				}//Finance User 			
											
					}
					
				
					
		        var UpdatedExpenseReport = nlapiSubmitRecord(o_recordOBJ , true ,true)
				
			}
		
		}
		
		
	}
		
		
	}
	catch(Ex)
	{
	nlapiLogExecution('DEBUG','AFTER SUBMIT','Ex==>'+Ex)	
	}
	
    if(type =='edit')
	{
		
	
	var recordID=nlapiGetRecordId();
		
	var s_record_type=nlapiGetRecordType();
	
	var o_recordOBJ=nlapiLoadRecord(s_record_type,recordID);

	
	    if (o_recordOBJ != null && o_recordOBJ != '' && o_recordOBJ != undefined) 
		{
		
			
			 var i_approval_status = o_recordOBJ.getFieldValue('custbody_approval_status');
			
			
			if(i_approval_status == 2)
			{
				o_recordOBJ.setFieldValue('complete', 'F');
				nlapiLogExecution('DEBUG', ' Expense Report ', ' ................. Rejected Set After Submit  ...................');
			}
			
			if(i_approval_status == 6)
			{
				o_recordOBJ.setFieldValue('supervisorapproval', 'F');
				nlapiLogExecution('DEBUG', ' Expense Report ', ' ................. supervisorapproval Set After Submit  ...................');
			}
			
			  var i_submitID=nlapiSubmitRecord(o_recordOBJ,true,false);
		     nlapiLogExecution('DEBUG', ' Employee ','i_submitID-->'+i_submitID) 
			
		}
		
	}//edit
	
	return true;

}

// END AFTER SUBMIT ===============================================


function role_search(next_approver,approval_status_current)
{
	     var i_role;
	     if(next_approver != null && next_approver !=undefined && next_approver!='')
		  {
		  	var o_approverOBJ=nlapiLoadRecord('employee',next_approver);
			//nlapiLogExecution('DEBUG',' Expense Report ',' approval_status_current OBJ-->'+o_approverOBJ);
			
			  if(o_approverOBJ != null && o_approverOBJ != undefined && o_approverOBJ!='')
			  {
			  	 var i_approver_role_count = o_approverOBJ.getLineItemCount('roles');
				 //nlapiLogExecution('DEBUG',' Expense Report ',' approval_status_current -->'+i_approver_role_count);
				
				 if(i_approver_role_count != null && i_approver_role_count !=undefined && i_approver_role_count!='')
				 {
				 	
					for(var i=1 ; i<=i_approver_role_count ; i++)
					{
						i_role = o_approverOBJ.getLineItemValue('roles','role' ,i)
						//nlapiLogExecution('DEBUG',' Expense Report ',' i_role -->'+i_role);
						
						if(approval_status_current == 4)
						{
							if(i_role == 1015)
							{
								i_role = 1015 ;
								break ;																
							}
							
							
						}//Project Manager
						else if(approval_status_current == 5)
						{							
							if(i_role == 1020)
							{
								i_role = 1020 ;
								break ;																
							}
							
						}//Program Director
						else if(approval_status_current == 6)
						{														
							if(i_role == 1028)
							{
								i_role = 1028 ;
								break ;																
							};
							
						}//Supervisor
						else if(approval_status_current == 7)
						{														
							if(i_role == 1027)
							{
								i_role = 1027 ;
								break ;																
							}
							
						}//Stream Leader 
						else if(approval_status_current == 8)
						{							
							if(i_role == 1024 )
							{
								i_role = 1024;
								break ;																
							}
							else 
							{
								if(i_role == 1014 )
								{
									i_role = 1014;
									break ;																
								}
							}
													
						}// CFO / CEO
						else if(approval_status_current == 9)
						{							
							if(i_role == 1007 )
							{
								i_role = 1007;
								break ;																
							}
							else 
							{
								if(i_role == 1009 )
								{
									i_role = 1009;
									break ;																
								}
							}
													
						}// CFO / CEO
				
						
					}
					
					
				 }
				
			  }
			
			
		  }
		
	return i_role ;
	
	
}

function name_employee(emp_id)
{
	var s_emp ='';
	
	if(emp_id!= null && emp_id!='' && emp_id!=undefined)
	{
		var columns=new Array();
	    var filters=new Array();
	
	   columns[0] = new nlobjSearchColumn('internalid');
	   columns[1] = new nlobjSearchColumn('firstname');
	   columns[2] = new nlobjSearchColumn('middlename');
	   columns[3] = new nlobjSearchColumn('lastname');
					
	   filters[0] = new nlobjSearchFilter('internalid', null, 'is', emp_id);
	   
	   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
	   
	   if (searchresults != null)
	   {
		   	for (var k = 0; k < searchresults.length; k++) 
			{
				var emp_i_id = searchresults[k].getValue('internalid');
			   // alert('  emp_i_id ==' + emp_i_id);
				
				var s_emp_fn=searchresults[k].getValue('firstname');
			    ////alert('s_emp_fn-->'+s_emp_fn)

			    var s_emp_mn=searchresults[k].getValue('middlename');
			   ////alert('s_emp_mn-->'+s_emp_mn)
 
			    var s_emp_ln=searchresults[k].getValue('lastname');
			    //////////////alert('s_emp_ln-->'+s_emp_ln)
				
				 if(s_emp_fn == null ||  s_emp_fn == '' ||  s_emp_fn == undefined)
				  {

					s_emp_fn='';

				  }
				   if(s_emp_mn == null ||  s_emp_mn == '' ||  s_emp_mn == undefined)
				  {

					s_emp_mn='';

				  }
				   if(s_emp_ln == null ||  s_emp_ln == '' ||  s_emp_ln == undefined)
				  {

					s_emp_ln='';

				  }



			   s_emp=s_emp_fn+' '+s_emp_mn+' '+s_emp_ln;
		   	   //alert('s_emp-->'+s_emp)
				
			}
		
		
		
	   }
	   
		
		
	}
	
		
	
	
	return s_emp;
}



function _logValidation(value)	
{
    if (value != null && value != '' && value != undefined) 	
	{
        return true;
    }
    else 	
	{
        return false;
    }
}






function is_supervisor_streamleader(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  
	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1027);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		  //  nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Emp ID -->'+emp_id);
			
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;
}//Check Maon Function


function is_supervisor_streamleader_ceo_cfo(supervisor_id)
{
	var emp_id;
	var emp_role;
	var result;
	var filters= new Array();
	var columns = new Array();
	var role_array = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('role');
	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
 
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		 //   nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Emp ID -->'+emp_id);
						
			emp_role = searchresults[k].getValue('role');
		//    nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Emp Role -->'+emp_role);
			
			if(emp_role == 1024 || emp_role == 1014 || emp_role== 1027)
			{
				role_array.push(emp_role); 
			}
			
					
		}
	
	//nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Role Array -->'+role_array);
	
   }//Search Results Check
	role_array=role_array.sort();
	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' role_array.sort'+role_array);
	
	
	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' role_array[0]'+role_array[0]);
	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' role_array[1]'+role_array[1]);
	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' role_array[2]'+role_array[2]);
	
	
	
	
	if ( ((role_array[1]== 1024) && (role_array[0]==1014) && (role_array[2]==1027)) || ((role_array[0]== 1024) && (role_array[1]==1027) ) || ((role_array[1]== 1027) && (role_array[0]==1014) )) 
	{
		nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' In Role CEO / CFO / Stream Leader');
		result = true ;
		
	}
	else
	{
		result = false ;
	}
	
	
	return result;
}//Check Main Function


function send_alternate_app_email(EXpenseseReprotTranId,level,i_record_id,i_user,i_supervisor,s_supervisor,s_user)
{
	    var author_pm=i_user;
		var recipient_cfo= i_supervisor;
        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending '+level+' Approval';
		var body_pm=" ";
	   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
		body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
		body_pm+= " Expense Report is Pending For your Approval <br\/> ";
		body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
		body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
		body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
		body_pm+="Regards,<br\/><br\/>";
		body_pm+=" "+s_user+"<br\/>";
		
		 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
		  {
		  
		  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
			  {
			  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

			  }

		  } 

	
	
}



function isSupervior_CFO(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  
	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1014);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		 	
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;
	
	
	
}

function isSupervior_CEO(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  
	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1024);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		  
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;
	
}



function CFO_EmployeeCentre_Role(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1014);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		  
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;
	
	
}

function CEO_EmployeeCentre_Role(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1024);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' CEO_EmployeeCentre_Role ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		    nlapiLogExecution('DEBUG',' CEO_EmployeeCentre_Role ',' Emp ID -->'+emp_id);
			
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;

}


function CFO_Approval(o_recordOBJ,type , i_approval_OLdstatus , newType ,i_user,s_user,i_user_role,i_record_id,s_record_type)
{	

try
{
	var i_supervisor;
  
  var is_CFO = CFO_EmployeeCentre_Role(i_user);
  
  
  if(i_user_role == 15)
  {
  	  if(is_CFO == true)
	  {	
		        if(o_recordOBJ!=null && o_recordOBJ!='' &&o_recordOBJ!= undefined)
				{
					
				var EXpenseseReprotTranId = o_recordOBJ.getFieldValue('tranid')
				
				 var i_employee = o_recordOBJ.getFieldValue('entity')
								
					 var s_employee = name_employee(i_employee);
					 
			       if (i_employee != null && i_employee != '' && i_employee != undefined) 
				   {
				   	var o_employee_OBJ = nlapiLoadRecord('employee', i_employee);
				   	nlapiLogExecution('DEBUG',' CFO_Approval ',' Employee OBJ  -->' + o_employee_OBJ);
								
						if (o_employee_OBJ != null && o_employee_OBJ != '' && o_employee_OBJ != undefined) 
						{									
							 i_supervisor = o_employee_OBJ.getFieldValue('supervisor');
							nlapiLogExecution('DEBUG',' CFO_Approval ','i_supervisor  -->'+i_supervisor);
						     var s_supervisor = name_employee(i_supervisor);
						
							  var is_CEO = CEO_EmployeeCentre_Role(i_supervisor);
							  nlapiLogExecution('DEBUG',' CFO_Approval ','I am CEO  -->'+is_CEO);
							  
							  if(is_CEO == true)
							  {
					  	       o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                       o_recordOBJ.setFieldValue('custbody_next_approver',i_supervisor);
							   
							    if(i_approval_OLdstatus == 2 &&  type == 'edit' && newType != 'userevent'  )
								{
									nlapiLogExecution('DEBUG',' CFO_Approval ','in loop ');
									
									o_recordOBJ.setFieldValue('custbody_rejection_code','');
									o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
									o_recordOBJ.setFieldValue('custbody_approved_by','');
																
								}
							    // ============= Email Sent for Supervisor  =======================
	
							 		var author_pm=i_employee;
									var recipient_cfo= i_supervisor;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_supervisor+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_employee+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
				
										  }
				
									  }  
							
							  }
							  
															
						}
					}
			
				}//o_recordOBJ
		             
					
	  }// Is CFO True
  	
	
  }
  

	
}
catch(er)
{
	nlapiLogExecution('DEBUG',' ERROR ','er  -->'+er);
	
}

	
	
}//Main Function

function CFO_Search()
{
	var i_ceo;
	var s_ceo;
	
		var columns=new Array();
		var filters=new Array();
				   
		columns[0] = new nlobjSearchColumn('internalid');
						
		filters[0] = new nlobjSearchFilter('role', null, 'is', 1014);
			
					
		var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
					
		if (searchresults != null) 
		{
			nlapiLogExecution('DEBUG','CFO_Search',' Number of CFO Employees  ==  ' + searchresults.length);
		
				i_ceo = searchresults[0].getValue('internalid');
							
				s_ceo = name_employee(i_ceo);
			
		}
		
		nlapiLogExecution('DEBUG','CFO_Search',' i_ cfo   ==  ' + i_ceo+' =========   s_ceo == '+s_ceo);
		
	
	return i_ceo ;
}



function new_CEO_Approval(o_recordOBJ,type , i_approval_OLdstatus , newType ,i_user,s_user,i_user_role,i_record_id,s_record_type)
{	

try
{
  var i_supervisor;
  
  var is_CEO = CEO_EmployeeCentre_Role(i_user);
  nlapiLogExecution('DEBUG',' new_CEO_Approval ','is_CEO  -->'+is_CEO);
  
  if(i_user_role == 15)
  {
  	  if(is_CEO == true)
	  {
	
		        if(o_recordOBJ!=null && o_recordOBJ!='' &&o_recordOBJ!= undefined)
				{
					
				var EXpenseseReprotTranId = o_recordOBJ.getFieldValue('tranid')
				
				 var i_employee = o_recordOBJ.getFieldValue('entity')
								
					 var s_employee = name_employee(i_employee);
					 
			       if (i_employee != null && i_employee != '' && i_employee != undefined) 
				   {
				   	var o_employee_OBJ = nlapiLoadRecord('employee', i_employee);
				   	nlapiLogExecution('DEBUG',' new_CEO_Approval ',' Employee OBJ  -->' + o_employee_OBJ);
								
						if (o_employee_OBJ != null && o_employee_OBJ != '' && o_employee_OBJ != undefined) 
						{									
							 var cfo_id = CFO_Search()
							
                            var s_cfo = name_employee(cfo_id);
							 
							 
							  if(cfo_id!=null && cfo_id!= '' && cfo_id!= undefined)
							  {
							  	
															
									 o_recordOBJ.setFieldValue('custbody_approval_status',6);
		                             o_recordOBJ.setFieldValue('custbody_next_approver',cfo_id);
									 o_recordOBJ.setFieldValue('supervisorapproval','F');
							   											  	      
							   
							    if(i_approval_OLdstatus == 2 &&  type == 'edit' && newType != 'userevent'  )
								{
									
									o_recordOBJ.setFieldValue('custbody_rejection_code','');
									o_recordOBJ.setFieldValue('custbody_approvalrejectionnote','');
									o_recordOBJ.setFieldValue('custbody_approved_by','');
																
								}					  							   
							    
								
								// ============= Email Sent for Supervisor  =======================
	
							 		var author_pm=i_employee;
									var recipient_cfo= cfo_id;
							        var subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Pending Supervisor Approval .';
									var body_pm=" ";
								   	body_pm+="Hello "+s_cfo+", <br\/><br\/>";
									body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is Submitted <br\/>";
									body_pm+= " Expense Report is Pending For your Approval <br\/> ";
									body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
									body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+i_record_id+"&whence=<br\/>";
									body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body_pm+="Regards,<br\/><br\/>";
									body_pm+=" "+s_employee+"<br\/>";
									
									 if(author_pm!=null && author_pm!=undefined && author_pm!='' )
									  {
									  					
									  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
										  {				
				
										  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );
												
				
										  }
				
									  }  
							
					  	
								
							  }
							  
															
						}
					}
					
				
			
				}//o_recordOBJ
		       
		
	  }// Is CFO True
  	
	
  }
  

	
}
catch(er)
{
	nlapiLogExecution('DEBUG',' ERROR ','er  -->'+er);
	
}

	
}
