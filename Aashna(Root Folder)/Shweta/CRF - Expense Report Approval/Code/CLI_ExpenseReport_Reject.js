/**
 * @author Shweta
 */
/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_ExpenseReport_Reject.js
	Date        : 27 May 2013
    Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//////////alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================


function reject_report(recordID)
{
	try
	{
	var author;
	var recipient ;
    var subject_employee ;
	var s_employee;
	var s_manager;
	
	////////alert('recordID')
		
	if(recordID!=null && recordID!=''  && recordID!=undefined)
	{
		var o_recordOBJ=nlapiLoadRecord('expensereport',recordID);
		
		
		if(o_recordOBJ!=null && o_recordOBJ!='' && o_recordOBJ!=undefined)
		{
			
			 var i_rejection_note=o_recordOBJ.getFieldValue('custbody_approvalrejectionnote');
			 ////////alert('reached ')	
			 if(i_rejection_note=='' || i_rejection_note==null || i_rejection_note == undefined)
			 {
			 	
				
					//var comments =  prompt("Please enter reason for rejection","");
								
							     
								 
						//if(comments != null && comments != undefined && comments !='' )
						{
						 	//o_recordOBJ.setFieldValue('custbody_approvalrejectionnote', comments);
							
							reject_report_suitelet(recordID)
							
						 	
								   
				 }	// if(comments != null && comments != undefined && comments !='' )
				
				
				 
			 }//end  if(i_rejection_note=='' || i_rejection_note==null)
			
			
		}// if(o_recordOBJ!=null && o_recordOBJ!='' && o_recordOBJ!=undefined)
	
	}// end if(recordID!=null && recordID!=''  && recordID!=undefined)
		
	}
	catch(er)
	{
		//////alert(' Error -->'+er.getDetails());
		
	}
	
	
		
}// end function reject_report(recordID)







function reject_report_suitelet(recordID)
{
	var temp = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=203&deploy=1&custscript_CurrentRecord=' + recordID ;
	var objWind = window.open(temp, "_blank", "toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=900,Height=250");

}



function addComments(data)
{
	//alert('addComments')
	
	var blank='';

	if (data != null && data != undefined && data != '')
	{
		var split_array = new Array();
		split_array = data.split('##');
		var recordID=  split_array[0];
		var Code = split_array[1];
		var Note = split_array[2];
		
		if(recordID!=null && recordID!='' && recordID!=undefined)
		{
			 var o_recordOBJ=nlapiLoadRecord('expensereport',recordID);
			 		          				   
			if (o_recordOBJ != null && o_recordOBJ != '' && o_recordOBJ != undefined) 
			{
				
				var approval_status = o_recordOBJ.getFieldValue('custbody_approval_status')
				//alert('approval_status 1-->'+approval_status)
				
				
				var set_status = 'Rejected by Accounting'
				
				var EXpenseseReprotTranId= o_recordOBJ.getFieldValue('tranid');
				
				var rejected= o_recordOBJ.getFieldValue('rejected');
							
				o_recordOBJ.setFieldValue('custbody_rejection_code',Code)
				o_recordOBJ.setFieldValue('custbody_approvalrejectionnote', Note);
				o_recordOBJ.setFieldValue('custbody_approved_by', blank);
			
				var role_current = nlapiGetRole();
				
				
			 
               if( (role_current == 1007) || (role_current ==1009))
				{					
					var status_txt='rejectedByAcct';
					o_recordOBJ.setFieldValue('custbody_next_approver', blank);	
					o_recordOBJ.setFieldValue('supervisorapproval', 'F');	
					o_recordOBJ.setFieldValue('accountingapproval', 'F');	
					o_recordOBJ.setFieldValue('rejected', 'T');
					o_recordOBJ.setFieldValue('statusRef', status_txt);		
					 o_recordOBJ.setFieldValue('complete', 'F');										
				}
				else
				{
					var status_txt='rejectedBySup';
					var text='Rejected by Supervisor';
										
					o_recordOBJ.setFieldValue('rejected', 'T');
					
					o_recordOBJ.setFieldValue('status', text);
					o_recordOBJ.setFieldValue('statusRef', status_txt);
					o_recordOBJ.setFieldValue('supervisorapproval', 'F');
				
					 o_recordOBJ.setFieldValue('complete', 'F');	
					
				}	


					
							var i_manager = nlapiGetUser();
							//alert('i_manager -->'+i_manager)
				        		
							if(i_manager!=null && i_manager!='' && i_manager!=undefined)
							{
								
								 var s_manager_fn = nlapiLookupField('employee', i_manager, 'firstname');
								 var s_manager_mn = nlapiLookupField('employee', i_manager, 'middlename');
								 var s_manager_ln = nlapiLookupField('employee', i_manager, 'lastname');
						       	
									      if(s_manager_fn == null ||  s_manager_fn == '' ||  s_manager_fn == undefined)
										  {
										  	
											s_manager_fn='';
											
										  }
										   if(s_manager_mn == null ||  s_manager_mn == '' ||  s_manager_mn == undefined)
										  {
										  	
											s_manager_mn='';
											
										  }
										   if(s_manager_ln == null ||  s_manager_ln == '' ||  s_manager_ln == undefined)
										  {
										  	
											s_manager_ln='';
											
										  }
									  		   
									   
									   s_manager=s_manager_fn+' '+s_manager_mn+' '+s_manager_ln;
								   	  	//alert('s_manager -->'+s_manager)	 		   	
								 
										
						}// end if(i_manager!=null && i_manager!='' && i_manager!=undefined)
								
							
				            var i_employee=o_recordOBJ.getFieldValue('entity');
								//alert('i_employee -->'+i_employee)	
							
							
							if(i_employee!=null && i_employee!='' && i_employee!=undefined)
							{
								 var s_employee_fn = nlapiLookupField('employee', i_employee, 'firstname');
								 var s_employee_mn = nlapiLookupField('employee', i_employee, 'middlename');
								 var s_employee_ln = nlapiLookupField('employee', i_employee, 'lastname');
								  
									    if(s_employee_fn == null ||  s_employee_fn == '' ||  s_employee_fn == undefined)
										  {
										  	
											s_employee_fn='';
											
										  }
										   if(s_employee_mn == null ||  s_employee_mn == '' || s_employee_mn == undefined)
										  {
										  	
											s_employee_mn='';
											
										  }
										   if(s_employee_ln == null ||  s_employee_ln == '' || s_employee_ln == undefined)
										  {
										  	
											s_employee_ln='';
											
										  }
									   					   
									   s_employee=s_employee_fn+' '+s_employee_mn+' '+s_employee_ln;
									   //alert('s_employee -->'+i_employee)	
								 									
							}// if(i_employee!=null && i_employee!='' && i_employee!=undefined)
								
							o_recordOBJ.setFieldValue('custbody_approval_status',2);
							
							var i_submitID=nlapiSubmitRecord(o_recordOBJ,true,false);
								
				              //alert('i_submitID-->'+i_submitID) 
				
							
						
							 if(i_submitID!='' && i_submitID!=null && i_submitID!=undefined)
							 {
							 	//alert('role_current-->'+role_current) 
																	
							 	var role_current = nlapiGetRole();
								
						 		// ============= Email Sent for Employee =======================
									
									author = i_manager;		 			
									recipient = i_employee;
									
									//alert('author-->'+author);
									//alert('recipient-->'+recipient);
									//alert('approval_status-->'+approval_status);
		
                             var subject_employee='';
							 var body=" ";    
						     body+="Dear "+s_employee+", <br\/><br\/>"; 
                      

                              if(approval_status == 4)
							  {
							  	
								subject_employee = 'Expense Report - '+EXpenseseReprotTranId+ 'is rejected by Project Manager'
								body+= " Expense Report is rejected by Project Manager, please review and re-submit for approval. <br\/> ";	
							  } 
							  else if(approval_status == 5)
							  {
							  	
								subject_employee = 'Expense Report - '+EXpenseseReprotTranId+ 'is rejected by Program Director'
								body+= " Expense Report is rejected by Program Director, please review and re-submit for approval. <br\/> ";	
							  } 
							  else if(approval_status == 6)
							  {
							  	
								var is_CFO = CFO_EmployeeCentre_Role(i_employee)
										
								var is_CEO = CEO_EmployeeCentre_Role(i_employee)
										
								if(is_CFO == true)
								{
									subject_employee =' Expense Report - '+EXpenseseReprotTranId+' is rejected by Supervisor (CEO)';
									body+= " Expense Report is rejected by Supervisor ( CEO ), please review and re-submit for approval. <br\/> ";		
								   
								}
								else  if(is_CEO == true)
								{
									subject_employee =' Expense Report - '+EXpenseseReprotTranId+' is rejected by Supervisor (CFO)';
									body+= " Expense Report is rejected by Supervisor ( CFO ), please review and re-submit for approval. <br\/> ";		
								}
								else
								{
									subject_employee = 'Expense Report - '+EXpenseseReprotTranId+ 'is rejected by Supervisor'
								    body+= " Expense Report is rejected by Supervisor, please review and re-submit for approval. <br\/> ";	
								
								}
							  	
								
							  } 
							  else if(approval_status == 7)
							  {
							  	
								subject_employee = 'Expense Report - '+EXpenseseReprotTranId+ 'is rejected by Stream Leader'
								body+= " Expense Report is rejected by Stream Leader, please review and re-submit for approval. <br\/> ";	
								
							  } 
							  else if(approval_status == 8)
							  {
							  	
								subject_employee =' Expense Report - '+EXpenseseReprotTranId+' is rejected by Executive';	
								body+= " Expense Report is rejected by Executive, please review and re-submit for approval. <br\/> ";		
								
							  } 
							  else if(approval_status == 9)
							  {
							  	
								subject_employee = 'Expense Report - '+EXpenseseReprotTranId+ 'is rejected by Accounting'
								body+= " Expense Report "+EXpenseseReprotTranId+" is Rejected . <br\/> ";
								body+="Please review and contact Finance Department for further processing.<br\/>"
								
							  } 
								
									body+=" If you have any questions about your Expense Report please contact your Support Team.<br\/><br\/>";;
									body+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
									body+=" Regards,<br\/><br\/>";
									body+=" "+s_manager+"<br\/>";
									
									
									  if(author!=null && author!=undefined && author!='' )
									  {
									  	  if(recipient!=null && recipient!=undefined && recipient!='' )
										  {
										  	  
										  	nlapiSendEmail(author, recipient, subject_employee, body );
											
										  }
									  						
									  }
												
							 }
				 
						
				//-----------------------------------------------------------------
				
				location.reload();
				
			
			}
			
		}
	
		
	}
}


function CEO_EmployeeCentre_Role(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1024);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		  //  nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Emp ID -->'+emp_id);
			
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;

}



function CFO_EmployeeCentre_Role(supervisor_id)
{
	var emp_id;
	var result;
	var filters= new Array();
	var columns = new Array();
	
   columns[0] = new nlobjSearchColumn('internalid');
  	
   filters[0] = new nlobjSearchFilter('internalid', null, 'is', supervisor_id);
   filters[1] = new nlobjSearchFilter('role', null, 'is', 1014);
	   
   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
   
   if(searchresults != null && searchresults != ''&& searchresults!= undefined)
   {
   	nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Search Results Length -->'+searchresults.length);
   		for (var k = 0; k < searchresults.length; k++) 
		{
			emp_id = searchresults[k].getValue('internalid');
		  //  nlapiLogExecution('DEBUG',' is_supervisor_streamleader ',' Emp ID -->'+emp_id);
			
			result = true; 
		}
	
	
   }//Search Results Check
	else
	{
		result = false ;
	}
	
	return result;
	
	
}

