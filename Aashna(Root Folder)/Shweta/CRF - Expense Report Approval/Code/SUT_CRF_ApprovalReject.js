
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_CRF_ApprovalReject.js
	Date        : 
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
	
	try
	{
		
		
		if (request.getMethod() == 'GET')
		{
			var CurrentRecordId = request.getParameter('custscript_CurrentRecord');
			nlapiLogExecution('DEBUG', 'POST', 'CurrentRecordId -->' + CurrentRecordId);
			
			
			var form = nlapiCreateForm('Rejection Code');
			
			var record_id = form.addField('custpage_rec_id', 'text', 'Record ID ');
			record_id.setDefaultValue(CurrentRecordId);
			record_id.setDisplayType('hidden');
			
			
			var reject_note = form.addField('custpage_reject_note', 'text', 'Rejection Note ');
			reject_note.setMandatory(true)
			
			var ExpenseReportRec = nlapiCreateRecord('expensereport')
			
			var myFld1 = ExpenseReportRec.getField('custbody_rejection_code');
        	var options1 = myFld1.getSelectOptions();
       	 	var select = form.addField('custpage_reject_code', 'select', 'Rejection Code:');
			select.setMandatory(true)
			
			select.addSelectOption('', '');
            for (var i = 0; i < options1.length; i++) 	
			{
            nlapiLogExecution('DEBUG', 'Search results ', ' options1[i].getId() =' + options1[i].getId())
            select.addSelectOption(options1[i].getId(), options1[i].getText());
            }
			
		    var checkBtn = form.addSubmitButton();
			response.writePage(form);
			
		}
		else if (request.getMethod() == 'POST')
		{
			var data;
			
			var custVal = request.getParameter('custpage_reject_code');
			nlapiLogExecution('DEBUG', 'POST', 'reject_note -->' + custVal);
			
			var Rejectnote = request.getParameter('custpage_reject_note');
			nlapiLogExecution('DEBUG', 'POST', 'reject_note -->' + Rejectnote);
			
			var rec_id = request.getParameter('custpage_rec_id');
			nlapiLogExecution('DEBUG', 'POST', 'rec_id -->' + rec_id);
			
			data = rec_id+'##'+custVal+'##'+Rejectnote;
			
			    if (custVal != null && custVal != '') 
				{
					response.write('<html><head><script>window.opener.addComments("' + data + '");self.close();</script></head><body></body></html>');
				    nlapiLogExecution('DEBUG', 'POST', ' response sent .............');
				}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	catch(er)
	{
	  nlapiLogExecution('DEBUG', 'suiteletFunction()', 'Error -->' + er);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
