/**
 * @author Shweta
 */
/**
 * @Updated By Supriya
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_ExpenseReport_Approve.js
	Date        : 27 May 2013
    Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

   3 june 2013          Supriya                         Sachin                          Added Currency code
   
   4 june 2013          Supriya                         Sachin                          Removed Code for sending mails on each level to all 
                                                                                        & entered condition for seding mails on each level as 
                                                                                        per given amount limit


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	////////alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================


function approve_report(recordID)
{
	var author_pm;
	var recipient_pm;
	var subject_program_director;
	var subject_stream_leader;
	var s_program_director;
	var s_next_approver;
	var s_stream_leader;
	var recipient_sl;

	var i_stream_leader;
	var i_department;
	var i_ceo;
	var i_cfo;
    var i_department;
	var s_cfo;
	var s_ceo;
	var recipient_cfo;
	var subject_cfo;
	var recipient_ceo;
	var subject_ceo;
	var i_supervisor;

	var finance_id;
	var s_finance;

	var s_employee;
	var i_employee;
	
	var flag_sl =0 ;
	var flag_pd =0 ;
		

	if(recordID!=null && recordID!=''  && recordID!=undefined)
	{
	var o_recordOBJ=nlapiLoadRecord('expensereport',recordID);
	////////alert(' Expense ID -->'+recordID);
	
	var EXpenseseReprotTranId = o_recordOBJ.getFieldValue('tranid');

		if(o_recordOBJ!=null && o_recordOBJ!='' && o_recordOBJ!=undefined)
		{

			var i_project_manager = o_recordOBJ.getFieldValue('custbodyproject_manager');
			////////alert(' Project Manager -->'+i_project_manager);

			var i_employee = o_recordOBJ.getFieldValue('entity');
			////////alert(' Employee -->'+i_employee);


		    s_employee = name_employee(i_employee);

			var i_program_director = o_recordOBJ.getFieldValue('custbody_program_manager');
			////////alert(' Program Director -->'+i_program_director);

		    s_program_director = name_employee(i_program_director);


			var i_next_approver = o_recordOBJ.getFieldValue('custbody_next_approver');
			//////////alert(' Next Approver -->'+i_next_approver);

		    s_next_approver = name_employee(i_next_approver);

			//////////alert(' Next s_next_approver -->'+s_next_approver);

			if (i_employee != '' && i_employee != null && i_employee != undefined)
			{
				//////////alert(' Next i_employee -->'+i_employee);
					
					i_supervisor = nlapiLookupField('employee', i_employee, 'supervisor');
					nlapiLogExecution('DEBUG', ' Employee ', ' Supervisor  -->' + i_supervisor);
                    //////////alert('i_supervisor'+i_supervisor)

					i_department = nlapiLookupField('employee', i_employee, 'department');
					nlapiLogExecution('DEBUG', ' Employee ', ' Department  -->' + i_department);
				
			}

           
			if (i_department != null && i_department != '' && i_department != undefined) 
			{
				i_stream_leader =  nlapiLookupField('department', i_department, 'custrecord_stream_leader');
				////alert(' Stream Leader  -->' + i_stream_leader);
				
		        s_stream_leader = name_employee(i_stream_leader);
			
			}
           
			
			//////////alert('s_stream_leader'+s_stream_leader)
			var i_approval_status = o_recordOBJ.getFieldValue('custbody_approval_status');
			//////////alert(' Approval Status -->'+i_approval_status);

			var i_amount = o_recordOBJ.getFieldValue('total');
			//////////alert(' Amount -->'+i_amount);
			
			var i_expenselineCount = o_recordOBJ.getLineItemCount('expense')
			//////////alert('i_expenselineCount==>'+i_expenselineCount)
			
			var i_currancy = 0;
			
			if(i_expenselineCount != null && i_expenselineCount != undefined && i_expenselineCount != '')
			{
			i_currancy = o_recordOBJ.getLineItemValue('expense','currency',1)	
			//////////alert('i_currancy==>'+i_currancy)
			}


			// ==================== Project Manager Level : Up to Ã¯Â¿Â½2,000, $2,500 Ã¯Â¿Â½2,000 =================================

			
			if( (i_approval_status == 4 ))
			{

				var i_project_manager_role=nlapiGetRole();
				var i_project_manager=nlapiGetUser();
	
					
				o_recordOBJ.setFieldValue('custbody_approval_status',5);
				o_recordOBJ.setFieldValue('custbody_next_approver',i_program_director);
				o_recordOBJ.setFieldValue('custbody_approved_by',i_project_manager);
				
			     // ============= Email Sent for Program Director   =======================

			 		author_pm=i_project_manager;
					recipient_pm = i_program_director;
			        subject_program_director =' Expense Report  '+EXpenseseReprotTranId+' Approved By Project Manager - Pending For Program Director Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_program_director+", <br\/><br\/>";
					body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is approved by Project Manager <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
					body_pm+="Regards,<br\/><br\/>";
					body_pm+=" "+s_next_approver+"<br\/>";


					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	  if(recipient_pm!=null && recipient_pm!=undefined && recipient_pm!='' )
						  {

						  	nlapiSendEmail(author_pm, recipient_pm, subject_program_director, body_pm );

						  }

					  }

			}
						
			// ============================== Program Director Level ==================================
			/*
			==================== Stream Leader Level : Up to Ã¯Â¿Â½4,000, $5,000, Ã¯Â¿Â½4,000 =================================
			*/
			
	       /*
		    var StreamLeaderAmount=' '; 
			if( i_currancy == 4)
			{
			StreamLeaderAmount = 5000;	
			}//if( i_currancy == 4)
			else if(i_currancy == 1 || i_currancy == 6)
			{
			StreamLeaderAmount = 4000;	
			}

            */
			
			
            var StreamLeaderAmount=' '; 
			if( i_currancy == 4)
			{
			StreamLeaderAmount = 2500;	
			}//if( i_currancy == 4)
			else if(i_currancy == 1 || i_currancy == 6)
			{
			StreamLeaderAmount = 2000;	
			}
			
			if(i_approval_status == 5  )
			{
				if((i_amount > StreamLeaderAmount))
				{
					
				}
				else
				{
				  flag_pd =1 ;	
				}
								
			}
			
						
			   var approved_by ;
				
				if(i_approval_status == 5)
				{
					
					approved_by = "Program Director";
					
				}
					
		
			 if( ((i_approval_status == 5 )) && (i_amount > StreamLeaderAmount))
			{		

					var i_program_director_role=nlapiGetRole();
					var i_program_director=nlapiGetUser();
									
				
							o_recordOBJ.setFieldValue('custbody_approval_status',7);
						    o_recordOBJ.setFieldValue('custbody_next_approver',i_stream_leader);
						    o_recordOBJ.setFieldValue('custbody_approved_by',i_program_director);


							// ============= Email Sent for Stream Leader  =======================

					 		author_pm = i_program_director;
							recipient_sl= i_stream_leader;
					        subject_stream_leader =' Expense Report  '+EXpenseseReprotTranId+'Approved By '+approved_by+' - Pending For Stream Leader Approval .';
							var body_pm=" ";
						   	body_pm+="Hello "+s_stream_leader+", <br\/><br\/>";
							body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is approved by "+approved_by+" <br\/>";
							body_pm+= " Expense Report is Pending For your Approval <br\/> ";
							body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
							body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
							body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
							body_pm+="Regards,<br\/><br\/>";
							body_pm+=" "+s_next_approver+"<br\/>";


							  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
							  {
							  	  if(recipient_sl!=null && recipient_sl!=undefined && recipient_sl!='' )
								  {

								  	nlapiSendEmail(author_pm, recipient_sl, subject_stream_leader, body_pm );
						
                                    
								  }

							  }

			}

		

			// ==============================  Stream Leader  Level ==================================
			
			var ExecutiveapprovalAmount=' '; 
			if( i_currancy == 4)
			{
			ExecutiveapprovalAmount = 5000;	
			}//if( i_currancy == 4)
			else if(i_currancy == 1 || i_currancy == 6)
			{
			ExecutiveapprovalAmount = 4000;	
			}
			
			if(i_approval_status == 7 )
			{
				if((i_amount > ExecutiveapprovalAmount))
				{
					
				}
				else
				{
					flag_sl = 1
				}
				
			}
			
			   
			  var approved_by ;
				
			    if(i_approval_status == 7)
				{
					approved_by = "Stream Leader";
									
				}
							
			if(((i_approval_status == 7))  && (i_amount >= ExecutiveapprovalAmount))
			{
				
				var i_stream_role=nlapiGetRole();
				var i_stream_leader=nlapiGetUser();
               
					o_recordOBJ.setFieldValue('custbody_approval_status',8);
				    o_recordOBJ.setFieldValue('custbody_next_approver','');
				    o_recordOBJ.setFieldValue('custbody_approved_by',i_stream_leader);

					// =============== CFO ==============================

					var columns=new Array();
					var filters=new Array();

					columns[0] = new nlobjSearchColumn('internalid');

					filters[0] = new nlobjSearchFilter('role', null, 'is', 1014);


					var	searchresults = nlapiSearchRecord('employee',null , filters, columns);

					if (searchresults != null)
					{
					    ////////////alert(' Number of CFO Employee  ==  ' + searchresults.length);
						for (var k = 0; k < searchresults.length; k++)
						{
							i_cfo = searchresults[k].getValue('internalid');
							////////////alert('  cfo_id ==' + cfo_id);

							s_cfo = name_employee(i_cfo);



					// ============= Email Sent for CFO  =======================

			 		author_pm=i_stream_leader;
					recipient_cfo= i_cfo;
			        subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Approved By '+approved_by+'- Pending For Executive Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_cfo+", <br\/><br\/>";
					body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is approved by "+approved_by+" <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
					body_pm+="Regards,<br\/><br\/>";
					body_pm+=" "+s_next_approver+"<br\/>";


					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	////////////alert(' author_pm set.............');

					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

							////////////alert(' recipient_cfo .............');

						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

							////////////alert(' Email Sent......... ');

						  }

					  }


					}


				}

					// =============== CEO ==============================


					var columns=new Array();
					var filters=new Array();

					columns[0] = new nlobjSearchColumn('internalid');

					filters[0] = new nlobjSearchFilter('role', null, 'is', 1024);


					var	searchresults = nlapiSearchRecord('employee',null , filters, columns);

					if (searchresults != null)
					{
					    ////////////alert(' Number of CFO Employee  ==  ' + searchresults.length);
						for (var k = 0; k < searchresults.length; k++)
						{
							i_ceo = searchresults[k].getValue('internalid');
							//////////alert('  cfo_id ==' + cfo_id);

							s_ceo = name_employee(i_ceo);



					// ============= Email Sent for CEO  =======================

			 		author_pm=i_stream_leader;
					recipient_ceo= i_ceo;
			        subject_ceo =' Expense Report  '+EXpenseseReprotTranId+'Approved By '+approved_by+' - Pending For Executive Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_ceo+", <br\/><br\/>";
					body_pm+= " Expense Report: " +EXpenseseReprotTranId+" is approved by "+approved_by+" <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
					body_pm+="Regards,<br\/><br\/>";
					body_pm+=" "+s_next_approver+"<br\/>";


					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	////////////alert(' author_pm set.............');

					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

							//////////alert(' recipient_cfo .............');

						  	nlapiSendEmail(author_pm, recipient_ceo, subject_ceo, body_pm );

							  // ////////alert(' Email Sent......... ');

						  }

					     }

						}

					}

			}
			
			
			var ExecutiveapprovalAmount=' '; 
			if( i_currancy == 4)
			{
			ExecutiveapprovalAmount = 5000;	
			}//if( i_currancy == 4)
			else if(i_currancy == 1 || i_currancy == 6)
			{
			ExecutiveapprovalAmount = 4000;	
     		}

			
			if(i_approval_status == 8  )
			{
				if((i_amount > ExecutiveapprovalAmount))
				{
					
				}
								
			}
				
			
			if(  (( (i_approval_status == 8)  && (i_amount > ExecutiveapprovalAmount)) ))
			{
				var i_user=nlapiGetUser();
				var i_cfo_role = nlapiGetRole();
				var approved_by ;
				
				if(i_approval_status == 8)
				{
					
					approved_by = "CFO";
					
				}
				
				//if(i_cfo_role == 1014 || i_cfo_role == 1024 )
				{


					o_recordOBJ.setFieldValue('custbody_approval_status',9);
				    o_recordOBJ.setFieldValue('custbody_next_approver','');
				    o_recordOBJ.setFieldValue('custbody_approved_by',i_user);
					o_recordOBJ.setFieldValue('supervisorapproval','T');

					////////////alert(' details set.............');



				// ============ Finance Group =================



				var columns=new Array();
				var filters=new Array();

				columns[0] = new nlobjSearchColumn('internalid');

				var	searchresults = nlapiSearchRecord('employee','customsearch_emp_finance_search' , null , columns);

				if (searchresults != null)
				{
				    //////////alert(' Serach Finance -->'+searchresults.length)
					for (var k = 0; k < searchresults.length; k++)
					{
					  finance_id = searchresults[k].getValue('internalid');
					  s_finance = name_employee(finance_id);


					// ============= Email Sent for Finance  =======================

			 		author_pm=i_user;
					recipient_cfo= finance_id;
			        subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Approved By '+approved_by+' - Pending For Finance Group Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_finance+", <br\/><br\/>";
					body_pm+=" Expense Report: " +EXpenseseReprotTranId+" is approved by "+approved_by+" <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";


					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	////////////alert(' author_pm set.............');

					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

							////////////alert(' recipient_cfo .............');

						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

							   ////////////alert(' Email Sent......... ');

						  }

					   }



					}
				
				
				}



			}


		}
             ////alert(' Before Finance Set...........')
           	
			if((( (i_approval_status ==7)  || (i_approval_status == 5) ) && (i_amount < ExecutiveapprovalAmount))   && ( (flag_sl == 1) || (flag_pd == 1)   ) )
			{
				var i_user=nlapiGetUser();
				var i_cfo_role = nlapiGetRole();
				var approved_by ;
				
				if(i_approval_status == 7)
				{
					approved_by = "Stream Leader";
									
				}
				else if(i_approval_status == 4)
				{
					approved_by = "Project Manager";
					
					
				}
				else if(i_approval_status == 5)
				{
					
					approved_by = "Program Director";
					
				}

				//if(i_cfo_role == 1014 || i_cfo_role == 1024 )
				{


					o_recordOBJ.setFieldValue('custbody_approval_status',9);
				    o_recordOBJ.setFieldValue('custbody_next_approver','');
				    o_recordOBJ.setFieldValue('custbody_approved_by',i_user);
					o_recordOBJ.setFieldValue('supervisorapproval','T');

					////////////alert(' details set.............');



				// ============ Finance Group =================



				var columns=new Array();
				var filters=new Array();

				columns[0] = new nlobjSearchColumn('internalid');

				var	searchresults = nlapiSearchRecord('employee','customsearch_emp_finance_search' , null , columns);

				if (searchresults != null)
				{
				    //////////alert(' Serach Finance -->'+searchresults.length)
					for (var k = 0; k < searchresults.length; k++)
					{
					  finance_id = searchresults[k].getValue('internalid');
					  s_finance = name_employee(finance_id);


					// ============= Email Sent for Finance  =======================

			 		author_pm=i_user;
					recipient_cfo= finance_id;
			        subject_cfo =' Expense Report  '+EXpenseseReprotTranId+' Approved By '+approved_by+' - Pending For Finance Group Approval .';
					var body_pm=" ";
				   	body_pm+="Hello "+s_finance+", <br\/><br\/>";
					body_pm+=" Expense Report: " +EXpenseseReprotTranId+" is approved by "+approved_by+" <br\/>";
					body_pm+= " Expense Report is Pending For your Approval <br\/> ";
					body_pm+= " Please click on below link to Approve / Reject the report : <br\/>  ";
					body_pm+= " https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id="+recordID+"&whence=<br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";


					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	////////////alert(' author_pm set.............');

					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

							////////////alert(' recipient_cfo .............');

						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

							   ////////////alert(' Email Sent......... ');

						  }

					   }



					}
				
				
				}




			}


		}
 

			if((i_approval_status == 9))
			{
				var i_finance_user = nlapiGetUser();
				var i_finance_role = nlapiGetRole();
                 
				 //////////alert('9 i_finance_user=='+i_finance_user)
				 //////////alert('9 i_finance_role=='+i_finance_role)
				 
				if (i_finance_user != '' && i_finance_user != null && i_finance_user != undefined)
				{

					  //////////alert(' i_finance_......... ');
					//if(i_finance_role ==  1007 || i_finance_role == 1009)
					{

						var s_finance_user = name_employee(i_finance_user)
						//////////alert(' s_finance_user......... '+s_finance_user);
						o_recordOBJ.setFieldValue('custbody_approval_status',1);
				        o_recordOBJ.setFieldValue('custbody_next_approver','');
				        o_recordOBJ.setFieldValue('custbody_approved_by',i_finance_user);
						o_recordOBJ.setFieldValue('supervisorapproval','T');
						o_recordOBJ.setFieldValue('accountingapproval','T');
						
						

						  ////////////alert(' i_finance_. set  ........ ');
					// ============= Email Sent for Finance  =======================

			 		author_pm = i_finance_user;
					recipient_cfo= i_employee;
			        subject_cfo = " Expense Report   " +EXpenseseReprotTranId+" is Approved  ";
					var body_pm=" ";
				   	body_pm+= " Expense Report submitted by  "+s_employee+" is approved <br\/>";
					body_pm+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";



					  if(author_pm!=null && author_pm!=undefined && author_pm!='' )
					  {
					  	//////////////alert(' author_pm set.............');

					  	  if(recipient_cfo != null && recipient_cfo != undefined && recipient_cfo != '' )
						  {

							//////////////alert(' recipient_cfo .............');

						  	nlapiSendEmail(author_pm, recipient_cfo, subject_cfo, body_pm );

							   //////////alert(' account Email Sent......... ');

						  }

					  }


				  }


				}//Finance User



			}


			 var i_submitID=nlapiSubmitRecord(o_recordOBJ,true,true);
		     //////////////alert(' Expense Submit ID -->'+i_submitID);



		}//REcord Object

	}//REcord ID

	 location.reload();
}//Approve




function name_employee(emp_id)
{
	var s_emp ='';
	
	if(emp_id!= null && emp_id!='' && emp_id!=undefined)
	{
		var columns=new Array();
	    var filters=new Array();
	
	   columns[0] = new nlobjSearchColumn('internalid');
	   columns[1] = new nlobjSearchColumn('firstname');
	   columns[2] = new nlobjSearchColumn('middlename');
	   columns[3] = new nlobjSearchColumn('lastname');
					
	   filters[0] = new nlobjSearchFilter('internalid', null, 'is', emp_id);
	   
	   var	searchresults = nlapiSearchRecord('employee',null , filters, columns);
	   
	   if (searchresults != null)
	   {
		   	for (var k = 0; k < searchresults.length; k++) 
			{
				var emp_i_id = searchresults[k].getValue('internalid');
			   // //alert('  emp_i_id ==' + emp_i_id);
				
				var s_emp_fn=searchresults[k].getValue('firstname');
			    //////alert('s_emp_fn-->'+s_emp_fn)

			    var s_emp_mn=searchresults[k].getValue('middlename');
			   //////alert('s_emp_mn-->'+s_emp_mn)
 
			    var s_emp_ln=searchresults[k].getValue('lastname');
			    ////////////////alert('s_emp_ln-->'+s_emp_ln)
				
				 if(s_emp_fn == null ||  s_emp_fn == '' ||  s_emp_fn == undefined)
				  {

					s_emp_fn='';

				  }
				   if(s_emp_mn == null ||  s_emp_mn == '' ||  s_emp_mn == undefined)
				  {

					s_emp_mn='';

				  }
				   if(s_emp_ln == null ||  s_emp_ln == '' ||  s_emp_ln == undefined)
				  {

					s_emp_ln='';

				  }



			   s_emp=s_emp_fn+' '+s_emp_mn+' '+s_emp_ln;
		   	   ////alert('s_emp-->'+s_emp)
				
			}
		
		
		
	   }
	   
		
		
	}
	
		
	
	
	return s_emp;
}



function search_ceo()
{
	var ceo_id;
	var columns=new Array();
	var filters=new Array();

	columns[0] = new nlobjSearchColumn('internalid');

	filters[0] = new nlobjSearchFilter('custentity_emp_job_role', null, 'is', 25);


	var	searchresults = nlapiSearchRecord('employee',null , filters, columns);

	if (searchresults != null)
	{
	    //////////////alert(' Number of CEO Employee  ==  ' + searchresults.length);
		for (var k = 0; k < searchresults.length; k++)
		{
			ceo_id = searchresults[k].getValue('internalid');
			//////////////alert('  ceo_id ==' + ceo_id);

		}
	}


	return ceo_id;

}



function search_cfo()
{
	var cfo_id;
	var columns=new Array();
	var filters=new Array();

	columns[0] = new nlobjSearchColumn('internalid');

	filters[0] = new nlobjSearchFilter('role', null, 'is', 1014);


	var	searchresults = nlapiSearchRecord('employee',null , filters, columns);

	if (searchresults != null)
	{
	    //////////////alert(' Number of CFO Employee  ==  ' + searchresults.length);
		for (var k = 0; k < searchresults.length; k++)
		{
			cfo_id = searchresults[k].getValue('internalid');
			//////////////alert('  cfo_id ==' + cfo_id);

		}
	}


	return cfo_id;

}








function finance_group_search()
{
	var finance_id;

	var columns=new Array();
	var filters=new Array();

	columns[0] = new nlobjSearchColumn('internalid');



	var	searchresults = nlapiSearchRecord('employee','customsearch_emp_finance_search' , null , columns);

	if (searchresults != null)
	{
	    ////////////alert(' Serach Finance -->'+searchresults.length)
		for (var k = 0; k < searchresults.length; k++)
		{
			finance_id = searchresults[k].getValue('internalid');

		}
	}


	return finance_id;

}








