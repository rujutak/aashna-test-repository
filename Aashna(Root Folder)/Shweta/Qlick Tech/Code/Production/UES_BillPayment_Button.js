/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_BillPayment_Button.js
	Date        : 15 May 2013
	Description : Create 'Bill Payment Print' Button On Bill Payment


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)


     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{
	
	if( type == 'view')
	{
		var approved = nlapiGetFieldValue('approved');
	    var subsidiary = nlapiGetFieldValue('subsidiary');
		
		nlapiLogExecution('DEBUG','beforeLoadRecord',' Approved --> '+approved);
		nlapiLogExecution('DEBUG','beforeLoadRecord',' Subsidiary -->'+subsidiary);
		
if( subsidiary == 17)
		
	//	if( approved == 'T' && subsidiary == 17)
		{
			 nlapiLogExecution('DEBUG','beforeLoadRecord',' For Button Creation ..........');
			form.setScript('customscript_billpayments_buttonlink');
			form.addButton('custpage_billpayment_print',' Bill Payment Print','openBankPayment()');
		    nlapiLogExecution('DEBUG','beforeLoadRecord',' Button Created Successfully  ..........');
					
		}
			
		
	}
	
	
	
   
	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================