/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_BankPayment_Report.js
	Date        : 15 May 2013
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var s_memo;
	var memo_array=new Array();
	var f_amount_values=new Array();
	var f_amount_cnt=0;
	var location_array=new Array();
	
  try
  {
  	 var i_recordID=request.getParameter('CurrentRecordID');	
	 nlapiLogExecution('DEBUG','suiteletFunction',' Record ID [ Check ] -->'+i_recordID);
	 
	 if(i_recordID!=null && i_recordID!='')
	 {
	 	var o_recordOBJ=nlapiLoadRecord('check',i_recordID);
		nlapiLogExecution('DEBUG','suiteletFunction',' Record Object [ Check ] -->'+o_recordOBJ);
		
		if(o_recordOBJ!=null && o_recordOBJ!='')
		{
			var d_checkDate=o_recordOBJ.getFieldValue('trandate');
			nlapiLogExecution('DEBUG','suiteletFunction',' Date [ Check ] -->'+d_checkDate);
			
			
			
			
			
           var d_checkDepartment=o_recordOBJ.getFieldText('department');
			nlapiLogExecution('DEBUG','suiteletFunction',' Department [ Check ] -->'+d_checkDepartment);

			if(d_checkDepartment==null || d_checkDepartment=='' || d_checkDepartment==undefined)
			{
 				d_checkDepartment='';
			}
			
			
			var s_account=o_recordOBJ.getFieldText('account');
			nlapiLogExecution('DEBUG','suiteletFunction',' Account [ Check ] -->'+s_account);
			
			var i_account=o_recordOBJ.getFieldValue('account');
			nlapiLogExecution('DEBUG','suiteletFunction',' Account [ Check ] -->'+s_account);
			
			//-----------------------------
			
			var o_account_OBJ=nlapiLoadRecord('account',i_account);
			nlapiLogExecution('DEBUG','suiteletFunction',' Account Object [ Check ] -->'+o_account_OBJ);
			
			var i_account_type=o_account_OBJ.getFieldValue('accttype');
			nlapiLogExecution('DEBUG','suiteletFunction',' Account Type [ Check ] -->'+i_account_type);
			//----------------------------
			
			
			var i_check_No=o_recordOBJ.getFieldValue('tranid');
			nlapiLogExecution('DEBUG','suiteletFunction',' Check Number [ Check ] -->'+i_check_No);
					
			if(i_check_No==null || i_check_No==undefined || i_check_No=='')
			{
				i_check_No='';
			}		
			
			
		 // ================================  Start - Address =========================================
			
			var d_subsidiary=o_recordOBJ.getFieldValue('subsidiary');
			nlapiLogExecution('DEBUG','suiteletFunction',' Subsidiary [ Journal Entry ] -->'+d_subsidiary);
			
			if(d_subsidiary!=null && d_subsidiary!= '' && d_subsidiary!= undefined)
			{
				var o_subsidiaryOBJ=nlapiLoadRecord('subsidiary',d_subsidiary);
		        nlapiLogExecution('DEBUG','suiteletFunction',' Subsidiary Object [ Journal Entry ] -->'+o_subsidiaryOBJ);
				
				if(o_subsidiaryOBJ!=null && o_subsidiaryOBJ!= '' && o_subsidiaryOBJ!= undefined)
				{
					address = o_subsidiaryOBJ.getFieldValue('addrtext');
					nlapiLogExecution('DEBUG','suiteletFunction',' Address [ Journal Entry ] -->'+address);
					
					if(address == null || address == '' || address == undefined)
					{
						address = '';
					}
					
				}
				
			}
			
			// ================================  End - Address =========================================
			
			
			
					
			var i_line_count=o_recordOBJ.getLineItemCount('expense');
			nlapiLogExecution('DEBUG','suiteletFunction',' No. Of Expense Lines [ Check ] -->'+i_line_count);
			
			if(i_line_count!=null && i_line_count!='')
			{
				for(var i=1;i<=i_line_count;i++)
				{
					var i_accountID=o_recordOBJ.getLineItemText('expense','account',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Account ID [ Check ] -->'+i_accountID);
					
					var d_checkLocation=o_recordOBJ.getLineItemText('expense','location',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Location [ Check ] -->'+d_checkLocation);
					
					
					if(d_checkLocation ==null || d_checkLocation=='' || d_checkLocation==undefined)
					{
						d_checkLocation='';
					}
					if(d_checkLocation !='')
					{
						location_array.push(d_checkLocation);
					}
					
					
					s_memo=o_recordOBJ.getLineItemValue('expense','memo',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Memo [ Check ] -->'+s_memo);
					
					if(s_memo ==null || s_memo=='' || s_memo==undefined)
					{
						s_memo='';
					}
					if(s_memo !='')
					{
						memo_array.push(s_memo);
					}
					
					
					var f_amount=o_recordOBJ.getLineItemValue('expense','amount',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Account Debit [ Check ] -->'+f_amount);
					
					if(f_amount ==null || f_amount=='' || f_amount==undefined)
					{
						f_amount=0;
					}
					if(f_amount !=null && f_amount!='' && f_amount!=undefined)
					{
						f_amount_values[f_amount_cnt++]=i_accountID+'##'+f_amount+'##'+s_memo;
					}
						 
								
				}
			
				
			}//END-Line Count Check			
			
		}//END-REcord Object
		nlapiLogExecution('DEBUG','suiteletFunction',' Amount Values [ Check ] -->'+f_amount_values);
		nlapiLogExecution('DEBUG','suiteletFunction',' Memo [ Check ] -->'+s_memo);
		bankPaymentReport(response,memo_array,d_checkDate,i_check_No,f_amount_values,s_account,i_account_type,location_array,d_checkDepartment,address);
		
	 		
	 }//END-REcord ID Check
  	
  }//END - TRy 
  catch(e)
  {
  	nlapiLogExecution('DEBUG','suiteletFunction',' Exception Caught -->'+e);
  }	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function bankPaymentReport(response,memo_array,d_checkDate,i_check_No,f_amount_values,s_account,i_account_type,location_array,d_checkDepartment,address)
{
	var total_amount=0;
	var memo_debit;
	var memo_credit; 
		
	var strVar="";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	
	 address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(address));
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+(address)+"<\/b><\/td>";
	strVar += "	<\/tr>";
	
	
   /*
 strVar += "	<tr>";
	strVar += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>QlikTech India Private Limited<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Unit No. 608 "+nlapiEscapeXML('&')+" 609, 6th Floor,<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Prestige Meridian-1, No.29, M.G.Road<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Bangalore<\/b><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>E-Mail :jagadeshwari.k@kpk.in<\/b><\/td>";
	strVar += "	<\/tr>";

*/
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	
	var lowerCase_account=s_account.toLowerCase();
	var check_account_type=lowerCase_account.indexOf('cash')
	nlapiLogExecution('DEBUG','suiteletFunction',' check_account_type -->'+check_account_type);
	
	
	if(i_account_type=='Bank' && check_account_type!=-1)
	{
		strVar += "		<td font-size=\"11\" font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Bank Voucher<\/b><\/td>";
	}
	else
	{
		strVar += "		<td font-size=\"11\" font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Bank Voucher<\/b><\/td>";
	}
	
	
	
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" width=\"70%\"  >No :"+i_check_No+" <\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"30%\" align=\"center\" >Dated :"+d_checkDate+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">";
	strVar += "		<table border=\"0\" width=\"100%\">";
	strVar += "			<tr>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" border-right=\"0.5\" align=\"center\" width=\"60%\" ><b>Particulars<\/b><\/td>";
	//strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" border-right=\"0.5\" align=\"center\" width=\"30%\" ><b>Debit<\/b><\/td>";
	strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" align=\"center\" width=\"40%\" ><b>Amount<\/b><\/td>";
	strVar += "			<\/tr>";
	
	
	//debit_values
	
	
	
	
	var splitDataArray=new Array();
	var splitDataArrayJournal=new Array();
	
	for (var b = 0; b < f_amount_values.length; b++)
	{
		splitDataArray=f_amount_values[b].split('##');
		account_ID=splitDataArray[0];
		account_amount=splitDataArray[1];
		memo_debit =splitDataArray[2];
		if(account_ID=='' || account_ID==null || account_ID==undefined)
		{
			account_ID='';
		}
		if(account_amount=='' || account_amount==null || account_amount==undefined)
		{
			account_amount='';
		}	
		
		strVar += "	<tr>";
		strVar += "	<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(account_ID)+"<\/td>";
		//strVar += "	<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" align=\"center\" >"+account_amount+"<\/td>";
		strVar += " <td font-size=\"9\" font-family=\"Helvetica\"  align=\"center\">"+account_amount+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "			<tr>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Memo - "+nlapiEscapeXML(memo_debit)+"<\/td>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
		strVar += "			<\/tr>";
	
	    
	    total_amount=parseFloat(total_amount)+parseFloat(account_amount);
		total_amount=total_amount.toFixed(2);
	}
	
	var amount_in_words=toWordsFunc(total_amount);
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
//	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Through :<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	if(s_account=='' || s_account==null || s_account==undefined)
	{
		s_account='';
	}
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(s_account)+"<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
//	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Location :<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	var loc_sort=new Array();
	loc_sort=location_array.sort();
	nlapiLogExecution('DEBUG','suiteletFunction',' loc_sort -->'+loc_sort);
	
	loc_sort=loc_sort[0];
	
	if(loc_sort=='' || loc_sort==undefined || loc_sort==null)
	{
		loc_sort=''
	}
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+loc_sort+"<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
//	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Department :<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	/*
    var department_sort=new Array();
	department_sort=department_array.sort();
	nlapiLogExecution('DEBUG','suiteletFunction',' department_sort -->'+department_sort);
	
	department_sort=department_sort[0];
	
	if(department_sort=='' || department_sort==undefined || department_sort==null)
	{
		department_sort=''
	}
   */
	
	if(d_checkDepartment=='' || d_checkDepartment==null || d_checkDepartment==undefined)
	{
		d_checkDepartment='';
	}
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+d_checkDepartment+"<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
		
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
//	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">On Account Of :<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	var memo_sort=new Array();
	memo_sort=memo_array.sort();
	nlapiLogExecution('DEBUG','suiteletFunction',' memo_sort -->'+memo_sort);
	
	memo_sort=memo_sort[0];
	
	if(memo_sort=='' || memo_sort==null || memo_sort==undefined)
	{
		memo_sort='';
	}
	
	
	/*
strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(memo_sort)+"<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
*/
	
	
	
	if(total_amount=='' || total_amount==null || total_amount==undefined)
	{
		total_amount='';
	}
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-top=\"0.5\" border-bottom=\"0.5\" border-right=\"0.5\" ><b>Amount (in words) :<\/b>"+amount_in_words+"only<\/td>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" border-top=\"0.5\" border-bottom=\"0.5\" align=\"center\" >"+total_debit+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-top=\"0.5\" border-bottom=\"0.5\" align=\"center\">"+total_amount+" <\/td>";
	strVar += "			<\/tr>";
	
	strVar += "		<\/table>";
	strVar += "		<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" ><b>Receiver's Signature:<\/b><\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"right\"><b>Authorised Signatory<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"left\"><b>Checked By<\/b> <\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"right\"><b>Verified By<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";


        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
		xml += "<pdf>\n<body>";
		xml += strVar;
		xml += "</body>\n</pdf>";
		// run the BFO library to convert the xml document to a PDF 
		// run the BFO library to convert the xml document to a PDF 
		var file = nlapiXMLToPDF(xml);
		nlapiLogExecution('DEBUG', 'printPDF', 'File =='+file);
		//Create PDF File with TimeStamp in File Name
		
		if(i_account_type=='Bank' && check_account_type!=-1)
		{
			var fileObj=nlapiCreateFile('Cash Check Voucher.pdf', 'PDF', file.getValue());
		}
		else
		{
			var fileObj=nlapiCreateFile('Bank Check Voucher.pdf', 'PDF', file.getValue());
		}
		
		nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj='+ fileObj);
		fileObj.setFolder(270);// Folder PDF File is to be stored
		var fileId=nlapiSubmitFile(fileObj);
		nlapiLogExecution('DEBUG', 'printPDF', '*****===PDF fileId  =='+ fileId);
        // Run the BFO library to convert the xml document to a PDF 
		// Set content type, file name, and content-disposition (inline means display in browser)
		if(i_account_type=='Bank' && check_account_type!=-1)
		{
			response.setContentType('PDF','Cash Check Voucher.pdf', 'inline');
		}
		else
		{
			response.setContentType('PDF','Bank Check Voucher.pdf', 'inline');
		}
	
		// Write response to the client
		response.write( file.getValue());   


	
	
	
	
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================


//--------------------------------Function Begin -  Amount in words----------------------------------------------------------------//



function toWordsFunc(s)
{
 
    var str='Rs.'
    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');
	var dg = new Array ('10000000','100000','1000','100');
	var dem=s.substr(s.lastIndexOf('.')+1)
    s=parseInt(s)
    var d
    var n1,n2
    while(s>=100)
    {
       for(var k=0;k<4;k++)
        {
            d=parseInt(s/dg[k])
            if(d>0)
            {
                if(d>=20)
                {
	                n1=parseInt(d/10)
	                n2=d%10
	                printnum2(n1)
	                printnum1(n2)
                }
     			 else
      			printnum1(d)
     			str=str+th[k]
   			 }
    		s=s%dg[k]
        }
    }
	 if(s>=20)
	 {
	            n1=parseInt(s/10)
	            n2=s%10
	 }
	 else
	 {
	            n1=0
	            n2=s
	 }

	printnum2(n1)
	printnum1(n2)
	if(dem>0)
	{
		decprint(dem)
	}
	return str

	function decprint(nm)
	{
         if(nm>=20)
	     {
            n1=parseInt(nm/10)
            n2=nm%10
	     }
		  else
		  {
		              n1=0
		              n2=parseInt(nm)
		  }
		  str=str+'And '
		  printnum2(n1)
	 	  printnum1(n2)
	}

	function printnum1(num1)
	{
        switch(num1)
        {
			  case 1:str=str+'One '
			         break;
			  case 2:str=str+'Two '
			         break;
			  case 3:str=str+'Three '
			        break;
			  case 4:str=str+'Four '
			         break;
			  case 5:str=str+'Five '
			         break;
			  case 6:str=str+'Six '
			         break;
			  case 7:str=str+'Seven '
			         break;
			  case 8:str=str+'Eight '
			         break;
			  case 9:str=str+'Nine '
			         break;
			  case 10:str=str+'Ten '
			         break;
			  case 11:str=str+'Eleven '
			        break;
			  case 12:str=str+'Twelve '
			         break;
			  case 13:str=str+'Thirteen '
			         break;
			  case 14:str=str+'Fourteen '
			         break;
			  case 15:str=str+'Fifteen '
			         break;
			  case 16:str=str+'Sixteen '
			         break;
			  case 17:str=str+'Seventeen '
			         break;
			  case 18:str=str+'Eighteen '
			         break;
			  case 19:str=str+'Nineteen '
			         break;
		}
	}

	function printnum2(num2)
	{
	    switch(num2)
		{
				  case 2:str=str+'Twenty '
				         break;
				  case 3:str=str+'Thirty '
				        break;
				  case 4:str=str+'Forty '
				         break;
				  case 5:str=str+'Fifty '
				         break;
				  case 6:str=str+'Sixty '
				         break;
				  case 7:str=str+'Seventy '
				         break;
				  case 8:str=str+'Eighty '
				         break;
				  case 9:str=str+'Ninety '
				         break;
        }
    
	}
}

//--------------------------------------------Function End - Amount in Words----------------------------------//







function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);

 return messgaeToBeSendPara;
}

