/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_JournalEntry_Report.js
	Date        : 15 May 2013
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var s_memo;
	var debit_values=new Array();
	var credit_values=new Array();
	var credit_cnt=0;
	var debit_cnt=0;
	var memo_array=new Array();
	var d_checkLocation;
	var location_array=new Array();
	var address;
  try
  {
  	 var i_recordID=request.getParameter('CurrentRecordID');	
	 nlapiLogExecution('DEBUG','suiteletFunction',' Record ID [ Journal Entry ] -->'+i_recordID);
	 
	 if(i_recordID!=null && i_recordID!='')
	 {
	 	var o_recordOBJ=nlapiLoadRecord('journalentry',i_recordID);
		nlapiLogExecution('DEBUG','suiteletFunction',' Record Object [ Journal Entry ] -->'+o_recordOBJ);
		
		if(o_recordOBJ!=null && o_recordOBJ!='')
		{
			var d_journalDate=o_recordOBJ.getFieldValue('trandate');
			nlapiLogExecution('DEBUG','suiteletFunction',' Date [ Journal Entry ] -->'+d_journalDate);
			
			// ================================  Start - Address =========================================
			
			var d_subsidiary=o_recordOBJ.getFieldValue('subsidiary');
			nlapiLogExecution('DEBUG','suiteletFunction',' Subsidiary [ Journal Entry ] -->'+d_subsidiary);
			
			if(d_subsidiary!=null && d_subsidiary!= '' && d_subsidiary!= undefined)
			{
				var o_subsidiaryOBJ=nlapiLoadRecord('subsidiary',d_subsidiary);
		        nlapiLogExecution('DEBUG','suiteletFunction',' Subsidiary Object [ Journal Entry ] -->'+o_subsidiaryOBJ);
				
				if(o_subsidiaryOBJ!=null && o_subsidiaryOBJ!= '' && o_subsidiaryOBJ!= undefined)
				{
					address = o_subsidiaryOBJ.getFieldValue('addrtext');
					nlapiLogExecution('DEBUG','suiteletFunction',' Address [ Journal Entry ] -->'+address);
					
					if(address == null || address == '' || address == undefined)
					{
						address = '';
					}
					
				}
				
			}
			
			// ================================  End - Address =========================================
			
			var d_journalDepartment=o_recordOBJ.getFieldText('department');
			nlapiLogExecution('DEBUG','suiteletFunction',' Department [ Journal Entry ] -->'+d_journalDepartment);

			if(d_journalDepartment==null || d_journalDepartment=='' || d_journalDepartment==undefined)
			{
 				d_journalDepartment='';
			}
			
			
			var i_journal_No=o_recordOBJ.getFieldValue('tranid');
			nlapiLogExecution('DEBUG','suiteletFunction',' Journal Number [ Journal Entry ] -->'+i_journal_No);
					
			var i_line_count=o_recordOBJ.getLineItemCount('line');
			nlapiLogExecution('DEBUG','suiteletFunction',' No. Of Lines [ Journal Entry ] -->'+i_line_count);
			
			if(i_line_count!=null && i_line_count!='')
			{
				for(var i=1;i<=i_line_count;i++)
				{
					var i_accountID=o_recordOBJ.getLineItemText('line','account',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Account ID [ Journal Entry ] -->'+i_accountID);
					
					
					d_checkLocation=o_recordOBJ.getLineItemText('line','location',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Location [ Check ] -->'+d_checkLocation);
					
					
					if(d_checkLocation ==null || d_checkLocation=='' || d_checkLocation==undefined)
					{
						d_checkLocation='';
					}
					if(d_checkLocation !='')
					{
						location_array.push(d_checkLocation);
					}
					
					
					
					s_memo=o_recordOBJ.getLineItemValue('line','memo',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Memo [ Journal Entry ] -->'+s_memo);
					
					if(s_memo ==null || s_memo=='' || s_memo==undefined)
					{
						s_memo='';
					}
					
					if(s_memo ==null || s_memo=='' || s_memo==undefined)
					{
						s_memo='';
					}
					
					if(s_memo !='')
					{
						memo_array.push(s_memo);
					}

					
					var f_debit=o_recordOBJ.getLineItemValue('line','debit',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Account Debit [ Journal Entry ] -->'+f_debit);
					
					if(f_debit ==null || f_debit=='' || f_debit==undefined)
					{
						f_debit=0;
					}
					if(f_debit !=null && f_debit!='' && f_debit!=undefined)
					{
						debit_values[debit_cnt++]=i_accountID+'##'+f_debit+'##'+s_memo;
					}
					var f_credit=o_recordOBJ.getLineItemValue('line','credit',i);
					nlapiLogExecution('DEBUG','suiteletFunction',' Account Credit [ Journal Entry ] -->'+f_credit);
					
					if(f_credit ==null || f_credit=='' || f_credit==undefined)
					{
						f_credit=0;
					}
					
					if(f_credit !=null && f_credit!='' && f_credit!=undefined)
					{
						credit_values[credit_cnt++]=i_accountID+'##'+f_credit+'##'+s_memo;
					}
					
					
					
								
				}
			
				
			}//END-Line Count Check			
			
		}//END-REcord Object
		nlapiLogExecution('DEBUG','suiteletFunction',' Debit Values [ Journal Entry ] -->'+debit_values);
		nlapiLogExecution('DEBUG','suiteletFunction',' Credit Values [ Journal Entry ] -->'+credit_values);
		nlapiLogExecution('DEBUG','suiteletFunction',' Memo [ Journal Entry ] -->'+memo_array);
		journalEntryReport(response,memo_array,d_journalDate,i_journal_No,debit_values,credit_values,location_array,d_journalDepartment,address);
		
	 		
	 }//END-REcord ID Check
  	
  }//END - TRy 
  catch(e)
  {
  	nlapiLogExecution('DEBUG','suiteletFunction',' Exception Caught -->'+e);
  }	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function journalEntryReport(response,memo_array,d_journalDate,i_journal_No,debit_values,credit_values,location_array,d_journalDepartment,address)
{
	var total_credit=0;
	var total_debit=0;
	var memo_debit;
	var memo_credit; 
	var strVar="";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	
 
 /*
 address = address.toString();
  nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address.toString()= " + address);
  address = address.replace(/\n/g,"<br\/>");/// /g
  nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address After Replcing spaces with % =====" + address);
 
*/

    address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(address));


   //address = SplitAddress(address);
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+(address)+"<\/b><\/td>";
	strVar += "	<\/tr>";
	
	
	/*
	 
	strVar += "	<tr>";
	strVar += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>QlikTech India Private Limited<\/b><\/td>";
	strVar += "	<\/tr>";
    strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Unit No. 608 "+nlapiEscapeXML('&')+" 609, 6th Floor,<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Prestige Meridian-1, No.29, M.G.Road<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Bangalore<\/b><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>E-Mail :jagadeshwari.k@kpk.in<\/b><\/td>";
	strVar += "	<\/tr>";


   */

	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"11\" font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>Journal Voucher<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	if(i_journal_No=='' || i_journal_No==undefined || i_journal_No==null)
	{
		i_journal_No=''
	}
	
	if(d_journalDate=='' || d_journalDate==undefined || d_journalDate==null)
	{
		d_journalDate=''
	}
	
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" width=\"70%\"  >No :"+i_journal_No+" <\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"30%\" align=\"center\" >Dated :"+d_journalDate+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">";
	strVar += "		<table border=\"0\" width=\"100%\">";
	strVar += "			<tr>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "				<td>&nbsp;<\/td>";
	strVar += "			<\/tr>";
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" border-right=\"0.5\" align=\"center\" width=\"40%\" ><b>Particulars<\/b><\/td>";
	strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" border-right=\"0.5\" align=\"center\" width=\"30%\" ><b>Debit<\/b><\/td>";
	strVar += "				<td font-size=\"9\"  font-family=\"Helvetica\" border-bottom=\"0.5\" border-top=\"0.5\" align=\"center\" width=\"30%\" ><b>Credit<\/b><\/td>";
	strVar += "			<\/tr>";
	
	
	//debit_values
	
	
	
	
	var splitDataArray=new Array();
	var splitDataArrayJournal=new Array();
	
	var memo_sort=new Array();
	memo_sort=memo_array;
	nlapiLogExecution('DEBUG','suiteletFunction',' memo_sort -->'+memo_sort);
	
	memo_sort=memo_sort[b];
	
	
if(memo_sort==null || memo_sort==undefined || memo_sort=='')
	{
		memo_sort='';
	}
	
	for (var b = 0; b < debit_values.length; b++)
	{
		splitDataArray=debit_values[b].split('##');
		account_ID=splitDataArray[0];
		account_debit=splitDataArray[1];
		memo_debit =splitDataArray[2];
		if(account_ID=='' || account_ID==undefined || account_ID==null)
		{
			account_ID=''
		}
		
		if(account_debit=='' || account_debit==undefined || account_debit==null)
		{
			account_debit=''
		}
		
		
		
		strVar += "	<tr>";
		strVar += "	<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(account_ID)+"<\/td>";
		strVar += "	<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" align=\"center\" >"+account_debit+"<\/td>";
		strVar += " <td font-size=\"9\" font-family=\"Helvetica\"  align=\"center\">&nbsp;<\/td>";
		strVar += "	<\/tr>";
		strVar += "			<tr>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Memo - "+nlapiEscapeXML(memo_debit)+"<\/td>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
		strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
		strVar += "			<\/tr>";
	    
	    total_debit=parseFloat(total_debit)+parseFloat(account_debit);
		total_debit=total_debit.toFixed(2);
	}
	
	
	
	//credit_values
	
	var splitDataArrayC=new Array();
	var splitDataArrayJournalC=new Array();
	
	for (var c = 0; c < credit_values.length; c++)
	{
		splitDataArray=credit_values[c].split('##');
		account_ID_C=splitDataArray[0];
		account_credit=splitDataArray[1];
		memo_credit=splitDataArray[2];
		if(account_ID_C=='' || account_ID_C==undefined || account_ID_C==null)
		{
			account_ID_C=''
		}
		
		if(account_credit=='' || account_credit==undefined || account_credit==null)
		{
			account_credit=''
		}
		
		
		
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(account_ID_C)+" <\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\">"+account_credit+"<\/td>";
	strVar += "			<\/tr>";
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Memo - "+nlapiEscapeXML(memo_credit)+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	total_credit=parseFloat(total_credit)+parseFloat(account_credit);
	total_credit=total_credit.toFixed(2);
	}
	nlapiLogExecution('DEBUG','suiteletFunction',' Total Debit  [ Journal Entry ] -->'+total_debit);	
	nlapiLogExecution('DEBUG','suiteletFunction',' Total Credit [ Journal Entry ] -->'+total_credit);
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Location :<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	var loc_sort=new Array();
	loc_sort=location_array.sort();
	nlapiLogExecution('DEBUG','suiteletFunction',' loc_sort -->'+loc_sort);
	
	loc_sort=loc_sort[0];
	
	if(loc_sort=='' || loc_sort==undefined || loc_sort==null)
	{
		loc_sort=''
	}
		
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+loc_sort+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">Department :<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	if(d_journalDepartment=='' || d_journalDepartment==undefined || d_journalDepartment==null)
	{
		d_journalDepartment=''
	}
		
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+d_journalDepartment+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	//strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">On Account Of :<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
	
	//

	
	/*
strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"left\">"+nlapiEscapeXML(memo_sort)+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"  border-right=\"0.5\" align=\"center\">&nbsp;<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\"   align=\"center\" >&nbsp;<\/td>";
	strVar += "			<\/tr>";
*/
	var amountInWords='';
	if(total_debit==total_credit)
	{
		amountInWords=toWordsFunc(total_debit)
	}
	
	if(amountInWords=='' || amountInWords==undefined || amountInWords==null)
	{
		amountInWords=''
	}
	if(total_credit=='' || total_credit==undefined || total_credit==null)
	{
		total_credit=''
	}
	if(total_debit=='' || total_debit==undefined || total_debit==null)
	{
		total_debit=''
	}
	
	strVar += "			<tr>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" border-top=\"0.5\" border-bottom=\"0.5\" ><b>Amount (in words) :<\/b>"+amountInWords+"only <\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-right=\"0.5\" border-top=\"0.5\" border-bottom=\"0.5\" align=\"center\" >"+total_debit+"<\/td>";
	strVar += "				<td font-size=\"9\" font-family=\"Helvetica\" border-top=\"0.5\" border-bottom=\"0.5\" align=\"center\">"+total_credit+"<\/td>";
	strVar += "			<\/tr>";
	strVar += "		<\/table>";
	strVar += "		<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" >&nbsp;<\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"right\"><b>Authorised Signatory<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "		<td>&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"left\"><b>Checked By<\/b> <\/td>";
	strVar += "		<td font-size=\"9\"  font-family=\"Helvetica\" width=\"50%\" align=\"right\"><b>Verified By<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	strVar += "";


        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
		xml += "<pdf>\n<body>";
		xml += strVar;
		xml += "</body>\n</pdf>";
		// run the BFO library to convert the xml document to a PDF 
		// run the BFO library to convert the xml document to a PDF 
		var file = nlapiXMLToPDF(xml);
		nlapiLogExecution('DEBUG', 'printPDF', 'File =='+file);
		//Create PDF File with TimeStamp in File Name
		var fileObj=nlapiCreateFile('Journal Report.pdf', 'PDF', file.getValue());
		nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj='+ fileObj);
		fileObj.setFolder(270);// Folder PDF File is to be stored
		var fileId=nlapiSubmitFile(fileObj);
		nlapiLogExecution('DEBUG', 'printPDF', '*****===PDF fileId  =='+ fileId);
        // Run the BFO library to convert the xml document to a PDF 
		// Set content type, file name, and content-disposition (inline means display in browser)
		response.setContentType('PDF','Journal Report.pdf', 'inline');
		// Write response to the client
		response.write( file.getValue());   


	
	
	
	
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================





//--------------------------------Function Begin -  Amount in words----------------------------------------------------------------//



function toWordsFunc(s)
{
 
    var str='Rs.'
    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');
	var dg = new Array ('10000000','100000','1000','100');
	var dem=s.substr(s.lastIndexOf('.')+1)
    s=parseInt(s)
    var d
    var n1,n2
    while(s>=100)
    {
       for(var k=0;k<4;k++)
        {
            d=parseInt(s/dg[k])
            if(d>0)
            {
                if(d>=20)
                {
	                n1=parseInt(d/10)
	                n2=d%10
	                printnum2(n1)
	                printnum1(n2)
                }
     			 else
      			printnum1(d)
     			str=str+th[k]
   			 }
    		s=s%dg[k]
        }
    }
	 if(s>=20)
	 {
	            n1=parseInt(s/10)
	            n2=s%10
	 }
	 else
	 {
	            n1=0
	            n2=s
	 }

	printnum2(n1)
	printnum1(n2)
	if(dem>0)
	{
		decprint(dem)
	}
	return str

	function decprint(nm)
	{
         if(nm>=20)
	     {
            n1=parseInt(nm/10)
            n2=nm%10
	     }
		  else
		  {
		              n1=0
		              n2=parseInt(nm)
		  }
		  str=str+'And '
		  printnum2(n1)
	 	  printnum1(n2)
	}

	function printnum1(num1)
	{
        switch(num1)
        {
			  case 1:str=str+'One '
			         break;
			  case 2:str=str+'Two '
			         break;
			  case 3:str=str+'Three '
			        break;
			  case 4:str=str+'Four '
			         break;
			  case 5:str=str+'Five '
			         break;
			  case 6:str=str+'Six '
			         break;
			  case 7:str=str+'Seven '
			         break;
			  case 8:str=str+'Eight '
			         break;
			  case 9:str=str+'Nine '
			         break;
			  case 10:str=str+'Ten '
			         break;
			  case 11:str=str+'Eleven '
			        break;
			  case 12:str=str+'Twelve '
			         break;
			  case 13:str=str+'Thirteen '
			         break;
			  case 14:str=str+'Fourteen '
			         break;
			  case 15:str=str+'Fifteen '
			         break;
			  case 16:str=str+'Sixteen '
			         break;
			  case 17:str=str+'Seventeen '
			         break;
			  case 18:str=str+'Eighteen '
			         break;
			  case 19:str=str+'Nineteen '
			         break;
		}
	}

	function printnum2(num2)
	{
	    switch(num2)
		{
				  case 2:str=str+'Twenty '
				         break;
				  case 3:str=str+'Thirty '
				        break;
				  case 4:str=str+'Forty '
				         break;
				  case 5:str=str+'Fifty '
				         break;
				  case 6:str=str+'Sixty '
				         break;
				  case 7:str=str+'Seventy '
				         break;
				  case 8:str=str+'Eighty '
				         break;
				  case 9:str=str+'Ninety '
				         break;
        }
    
	}
}

//--------------------------------------------Function End - Amount in Words----------------------------------//






 
 
   
  
function SplitAddress(Address)
{
 //***********************************************************    
   Address = Address.toString();
   nlapiLogExecution('DEBUG','In function SplitAddress', "  Address.toString()= " + Address);
   var AddArray=new Array();
   AddArray=Address.split('\n>');
   var Result='';
   for(var i=0;i<AddArray.length;i++)
   {
    if(i==0)
   {
    Result=nlapiEscapeXML(AddArray[i]);
   }
   else
   {
    Result=Result+"<br\/>"+nlapiEscapeXML(AddArray[i]);
   }    
   }   
   
   nlapiLogExecution('DEBUG','In function SplitAddress', "  Address After replace =====" + Result);
  
  return Result;
 //**********************************************************
}




function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);

 return messgaeToBeSendPara;
}


