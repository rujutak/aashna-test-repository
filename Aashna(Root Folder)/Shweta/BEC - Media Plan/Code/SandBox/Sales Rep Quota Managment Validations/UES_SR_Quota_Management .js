// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:USE_SR_Quota_Managment
	Author:
	Company:
	Date:
	Description:On Sales Rep Quota Management record take all data and Create One new Sales Rep Quota Management Record  .


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

    05-03-2013           Rakesh Malvade					     Supriya			Sales Rep Quota Management record take all data and Create One new Sales Rep Quota Management Record  	
    06-03-2013           Supriya Patil                       Supriya            For restricting duplicate record creation.
    29-03-2013           Supriya Patil                       Jitin              For restricting the actual record creation by user
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	//get sales rep quota id
	   
	 var media_type;
	 var item_unit;
	 
      if(type == 'create')
	   {
	   	 nlapiLogExecution('DEBUG', 'afterSubmit', 'type = ' + type);
		
		//get all the record value
		var program_name=nlapiGetFieldValue('custrecord_bec_qm_program');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'program name = ' + program_name);
		
		var sales_rep= nlapiGetFieldValue('custrecord_bec_qm_salesrep');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Sales rep = ' + sales_rep);
		
			
		var month= nlapiGetFieldValue('custrecord_bec_qm_month');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'MOnth = ' + month);
		
		var year= nlapiGetFieldValue('custrecord_bec_qm_year');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Year = ' + year);
		
		
		
		
		 if(program_name!=null && program_name!=undefined && program_name!='')
	  {
	  	var itemOBJ=nlapiLoadRecord('serviceitem',program_name);
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Object='+itemOBJ);
		
		
		if(itemOBJ!=null && itemOBJ!='')
		{
			media_type=itemOBJ.getFieldValue('custitem_bec_mediatype');
			nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Media Type ='+media_type);
			
			
			item_unit=itemOBJ.getFieldValue('custitem3');
			nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Units ='+item_unit);
			
			
		}//If
		
	  }
		
		
		  if(media_type!='' && media_type!= undefined && media_type!=null)
		  {
		  	
				 if (item_unit != '' && item_unit != undefined && item_unit != null) 
				 {
				 	
					
					
					var Quota_Filters = new Array();
					var Quota_Coloumns = new Array();
					
					
					Quota_Filters[0] = new nlobjSearchFilter('custrecord_bec_qm_salesrep',null,'is',sales_rep)
					Quota_Filters[1] = new nlobjSearchFilter('custrecord_bec_qm_type',null,'is',2)
					Quota_Filters[2] = new nlobjSearchFilter('custrecord_bec_qm_month',null,'is',month)
					Quota_Filters[3] = new nlobjSearchFilter('custrecord_bec_qm_year',null,'is',year)
					Quota_Filters[4] = new nlobjSearchFilter('custrecord_quota_unitofmesure',null,'is',item_unit)
					Quota_Filters[5] = new nlobjSearchFilter('custrecord_bec_qm_program',null,'is',program_name)
					Quota_Filters[6] = new nlobjSearchFilter('custrecord_bec_qm_adtype',null,'is',media_type)
					
					Quota_Coloumns[0] = new nlobjSearchColumn('internalid')
					
					var SearchResult = nlapiSearchRecord('customrecord_sales_rep_quota_mngm',null,Quota_Filters,Quota_Coloumns)
					
					if(SearchResult != null && SearchResult != undefined && SearchResult != '')
					{
						var Id = SearchResult[0].getValue('internalid')
						nlapiLogExecution('DEBUG', 'deposit', 'Id = ' + Id);
						
						throw('This Record Is Allready Existing.')
					}
					
					
					if(type == 1)
					{
					   throw('System Will Not Allow To Create Actual Record.')
					}
								
								
					
					
					
					
					
					
					
					
					
					
					
				 }
			 
		   }
		
		
		// =============================================================
		
		
				//try
	{
		var i_media_type;
		var i_unit;
		
	//	var i_salesrep_recordID=nlapiGetRecordId();
	//	nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' i_salesrep_recordID --> ' + i_salesrep_recordID)
		
		
	//	if(i_salesrep_recordID!=null && i_salesrep_recordID!='' && i_salesrep_recordID!=undefined)
		{
		//	var o_salesrepOBJ=nlapiLoadRecord('customrecord_sales_rep_quota_mngm',i_salesrep_recordID);
		 //   nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Object --> ' + o_salesrepOBJ)
		
		//	if(o_salesrepOBJ!=null && o_salesrepOBJ!='' && o_salesrepOBJ!=undefined)
			{
				var i_tv_program=nlapiGetFieldValue('custrecord_bec_qm_program');
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program  --> ' + i_tv_program)
				
			  if(i_tv_program!=null && i_tv_program!=undefined && i_tv_program!='')
			  {
			  	var itemOBJ=nlapiLoadRecord('serviceitem',i_tv_program);
				nlapiLogExecution('DEBUG','afterSubmitRecord','Program Item Object -->'+itemOBJ);
				
				
				if(itemOBJ!=null && itemOBJ!='')
				{
					i_media_type=itemOBJ.getFieldValue('custitem_bec_mediatype');
					nlapiLogExecution('DEBUG','item_unit','Program Item Media Type -->'+i_media_type);
					
					
					i_unit=itemOBJ.getFieldValue('custitem3');
					nlapiLogExecution('DEBUG','item_unit','Program Item Units -->'+i_unit);
					
					
				}//If
				
			  }//TV Program Item
												
				var i_year=nlapiGetFieldValue('custrecord_bec_qm_year');
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Year  --> ' + i_year)
				
				var i_month=nlapiGetFieldValue('custrecord_bec_qm_month');
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Month --> ' + i_month)
				
				
			
				//===========================  1  ===========================
				
				var salesrep_day_1='custrecord_bec_qm_1';
				var tv_day_1='custrecord_adinv_tq_1';
				
				var i_dailyquota_1=nlapiGetFieldValue(salesrep_day_1);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 1 --> ' + i_dailyquota_1);
				
				i_dailyquota_1=validateValue_zero(i_dailyquota_1);
				
				var total_daily_quota_1=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_1);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 1 --> ' + total_daily_quota_1);
								
				var total_1=parseFloat(i_dailyquota_1) + parseFloat(total_daily_quota_1);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 1  ********* --> ' + total_1);
						
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_1)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 1  ********* --> ' + tv_total);
				
				if(total_1 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 1. ');
					throw ' TV Program Total Quota Exceeded for Day 1.';
					
				}
			    //=========================== 1 ============================
				//===========================  2  ===========================
				
				var salesrep_day_2='custrecord_bec_qm_2';
				var tv_day_2='custrecord_adinv_tq_2';
				
				var i_dailyquota_2=nlapiGetFieldValue(salesrep_day_2);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 2 --> ' + i_dailyquota_2);
				
				i_dailyquota_2=validateValue_zero(i_dailyquota_2);
				
				var total_daily_quota_2=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_2 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 2 --> ' + total_daily_quota_2);
				
				
				var total_2=parseFloat(i_dailyquota_2)+parseFloat(total_daily_quota_2);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 2  ********* --> ' + total_2);
								
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_2)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 2  ********* --> ' + tv_total);
				
				if(total_2 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 2. ');
					throw ' TV Program Total Quota Exceeded for Day 2.';
					
				}		
				
				//===========================  2  ===========================
				
				
				
				//===========================  3  ===========================
				
				var salesrep_day_3='custrecord_bec_qm_3';
				var tv_day_3='custrecord_adinv_tq_3';
				
				var i_dailyquota_3=nlapiGetFieldValue(salesrep_day_3);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 3 --> ' + i_dailyquota_3);
				
				i_dailyquota_3=validateValue_zero(i_dailyquota_3);
				
				var total_daily_quota_3=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_3 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 3 --> ' + total_daily_quota_3);
				
				
				var total_3=parseFloat(i_dailyquota_3)+parseFloat(total_daily_quota_3);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 3  ********* --> ' + total_3);
								
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_3)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 3  ********* --> ' + tv_total);
				
				if(total_3 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 3. ');
					throw ' TV Program Total Quota Exceeded for Day 3.';
					
				}		
				
				//===========================  3  ===========================
				
				
				
				//===========================  4  ===========================
				
				var salesrep_day_4='custrecord_bec_qm_4';
				var tv_day_4='custrecord_adinv_tq_4';
				
				var i_dailyquota_4=nlapiGetFieldValue(salesrep_day_4);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 4 --> ' + i_dailyquota_4);
				
				i_dailyquota_4=validateValue_zero(i_dailyquota_4);
				
				var total_daily_quota_4=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_4 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 4 --> ' + total_daily_quota_4);
				
				
				var total_4=parseFloat(i_dailyquota_4)+parseFloat(total_daily_quota_4);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 4  ********* --> ' + total_4);
								
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_4)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 4  ********* --> ' + tv_total);
				
				if(total_4 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 4. ');
					throw ' TV Program Total Quota Exceeded for Day 4.';
					
				}		
				
				//===========================  4  ===========================
				
				
				
				//===========================  5  ===========================
				
				var salesrep_day_5='custrecord_bec_qm_5';
				var tv_day_5='custrecord_adinv_tq_5';
				
				var i_dailyquota_5=nlapiGetFieldValue(salesrep_day_5);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 5 --> ' + i_dailyquota_5);
				
				i_dailyquota_5=validateValue_zero(i_dailyquota_5);
				
				var total_daily_quota_5=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_5 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 5 --> ' + total_daily_quota_5);
				
				
				
               var total_5=parseFloat(i_dailyquota_5)+parseFloat(total_daily_quota_5);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 5  ********* --> ' + total_5);
								
				

				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_5)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 5  ********* --> ' + tv_total);
				
				if(total_5 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 5. ');
					throw ' TV Program Total Quota Exceeded for Day 5.';
					
				}		
				
				//===========================  5  ===========================
				
				
				
				//===========================  6  ===========================
				
				var salesrep_day_6='custrecord_bec_qm_6';
				var tv_day_6='custrecord_adinv_tq_6';
				
				var i_dailyquota_6=nlapiGetFieldValue(salesrep_day_6);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 6 --> ' + i_dailyquota_6);
				
				i_dailyquota_6=validateValue_zero(i_dailyquota_6);
				
				var total_daily_quota_6=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_6 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 6 --> ' + total_daily_quota_6);
				
				
			
	var total_6=parseFloat(i_dailyquota_6)+parseFloat(total_daily_quota_6);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 6  ********* --> ' + total_6);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_6)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 6  ********* --> ' + tv_total);
				
				if(total_6 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 6. ');
					throw ' TV Program Total Quota Exceeded for Day 6.';
					
				}		
				
				//===========================  6  ===========================
				
				
				
				
				//===========================  7  ===========================
				
				var salesrep_day_7='custrecord_bec_qm_7';
				var tv_day_7='custrecord_adinv_tq_7';
				
				var i_dailyquota_7=nlapiGetFieldValue(salesrep_day_7);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 7 --> ' + i_dailyquota_7);
				
				i_dailyquota_7=validateValue_zero(i_dailyquota_7);
				
				var total_daily_quota_7=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_7 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 7 --> ' + total_daily_quota_7);
				
				
			
	            var total_7=parseFloat(i_dailyquota_7)+parseFloat(total_daily_quota_7);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 7  ********* --> ' + total_7);
								
				

				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_7)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 7  ********* --> ' + tv_total);
				
				if(total_7 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 7. ');
					throw ' TV Program Total Quota Exceeded for Day 7.';
					
				}		
				
				//===========================  7  ===========================
				
				
				
				
				//===========================  8  ===========================
				
				var salesrep_day_8='custrecord_bec_qm_8';
				var tv_day_8='custrecord_adinv_tq_8';
				
				var i_dailyquota_8=nlapiGetFieldValue(salesrep_day_8);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 8 --> ' + i_dailyquota_8);
				
				i_dailyquota_8=validateValue_zero(i_dailyquota_8);
				
				var total_daily_quota_8=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_8 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 8 --> ' + total_daily_quota_8);
				
				
			
	var total_8=parseFloat(i_dailyquota_8)+parseFloat(total_daily_quota_8);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 8  ********* --> ' + total_8);

								
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_8)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 8  ********* --> ' + tv_total);
				
				if(total_8 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 8. ');
					throw ' TV Program Total Quota Exceeded for Day 8.';
					
				}		
				
				//===========================  8  ===========================
				
				
				
				//===========================  9  ===========================
				
				var salesrep_day_9='custrecord_bec_qm_9';
				var tv_day_9='custrecord_adinv_tq_9';
				
				var i_dailyquota_9=nlapiGetFieldValue(salesrep_day_9);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 9 --> ' + i_dailyquota_9);
				
				i_dailyquota_9=validateValue_zero(i_dailyquota_9);
				
				var total_daily_quota_9=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_9 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 9 --> ' + total_daily_quota_9);
				
			
	
				var total_9=parseFloat(i_dailyquota_9)+parseFloat(total_daily_quota_9);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 9  ********* --> ' + total_9);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_9)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 9  ********* --> ' + tv_total);
				
				if(total_9 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 9. ');
					throw ' TV Program Total Quota Exceeded for Day 9.';
					
				}		
				
				//===========================  9  ===========================
				
				
				//===========================  10  ===========================
				
				var salesrep_day_10='custrecord_bec_qm_10';
				var tv_day_10='custrecord_adinv_tq_10';
				
				var i_dailyquota_10=nlapiGetFieldValue(salesrep_day_10);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 10 --> ' + i_dailyquota_10);
				
				i_dailyquota_10=validateValue_zero(i_dailyquota_10);
				
				var total_daily_quota_10=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_10 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 10 --> ' + total_daily_quota_10);
				
				
			
	            var total_10=parseFloat(i_dailyquota_10)+parseFloat(total_daily_quota_10);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 10  ********* --> ' + total_10);
								
            
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_10)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 10  ********* --> ' + tv_total);
				
				if(total_10 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 10. ');
					throw ' TV Program Total Quota Exceeded for Day 10.';
					
				}		
				
				//===========================  10  ===========================
				
				//===========================  11  ===========================
				
				var salesrep_day_11='custrecord_bec_qm_11';
				var tv_day_11='custrecord_adinv_tq_11';
				
				var i_dailyquota_11=nlapiGetFieldValue(salesrep_day_11);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 11 --> ' + i_dailyquota_11);
				
				i_dailyquota_11=validateValue_zero(i_dailyquota_11);
				
				var total_daily_quota_11=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_11 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 11 --> ' + total_daily_quota_11);
				
				
			
	var total_11=parseFloat(i_dailyquota_11)+parseFloat(total_daily_quota_11);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 11  ********* --> ' + total_11);

								
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_11)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 11  ********* --> ' + tv_total);
				
				if(total_11 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 11. ');
					throw ' TV Program Total Quota Exceeded for Day 11.';
					
				}		
				
				//===========================  11  ===========================
				
				
				
				
				//===========================  12  ===========================
				
				var salesrep_day_12='custrecord_bec_qm_12';
				var tv_day_12='custrecord_adinv_tq_12';
				
				var i_dailyquota_12=nlapiGetFieldValue(salesrep_day_12);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 12 --> ' + i_dailyquota_12);
				
				i_dailyquota_12=validateValue_zero(i_dailyquota_12);
				
				var total_daily_quota_12=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_12 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 12 --> ' + total_daily_quota_12);
				
				
				
var total_12=parseFloat(i_dailyquota_12)+parseFloat(total_daily_quota_12);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 12  ********* --> ' + total_12);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_12)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 12  ********* --> ' + tv_total);
				
				if(total_12 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 12. ');
					throw ' TV Program Total Quota Exceeded for Day 12.';
					
				}		
				
				//===========================  12  ===========================
				
				
				
				
				//===========================  13  ===========================
				
				var salesrep_day_13='custrecord_bec_qm_13';
				var tv_day_13='custrecord_adinv_tq_13';
				
				var i_dailyquota_13=nlapiGetFieldValue(salesrep_day_13);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 13 --> ' + i_dailyquota_13);
				
				i_dailyquota_13=validateValue_zero(i_dailyquota_13);
				
				var total_daily_quota_13=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_13 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 13 --> ' + total_daily_quota_13);
				
				
				
var total_13=parseFloat(i_dailyquota_13)+parseFloat(total_daily_quota_13);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 13  ********* --> ' + total_13);
								
				

				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_13)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 13  ********* --> ' + tv_total);
				
				if(total_13 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 13. ');
					throw ' TV Program Total Quota Exceeded for Day 13 .';
					
				}		
				
				//===========================  13  ===========================
				
				
				
				//===========================  14  ===========================
				
				var salesrep_day_14='custrecord_bec_qm_14';
				var tv_day_14='custrecord_adinv_tq_14';
				
				var i_dailyquota_14=nlapiGetFieldValue(salesrep_day_14);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 14 --> ' + i_dailyquota_14);
				
				i_dailyquota_14=validateValue_zero(i_dailyquota_14);
				
				var total_daily_quota_14=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_14 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 14 --> ' + total_daily_quota_14);
				
				
			
	var total_14=parseFloat(i_dailyquota_14)+parseFloat(total_daily_quota_14);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 14  ********* --> ' + total_14);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_14)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 14  ********* --> ' + tv_total);
				
				if(total_14 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 14. ');
					throw ' TV Program Total Quota Exceeded for Day 14 .';
					
				}		
				
				//===========================  14  ===========================
				
				
				
				//===========================  15  ===========================
				
				var salesrep_day_15='custrecord_bec_qm_15';
				var tv_day_15='custrecord_adinv_tq_15';
				
				var i_dailyquota_15=nlapiGetFieldValue(salesrep_day_15);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 15 --> ' + i_dailyquota_15);
				
				i_dailyquota_15=validateValue_zero(i_dailyquota_15);
				
				var total_daily_quota_15=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_15 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 15 --> ' + total_daily_quota_15);
				
			
	
				var total_15=parseFloat(i_dailyquota_15)+parseFloat(total_daily_quota_15);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 15  ********* --> ' + total_15);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_15)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 15  ********* --> ' + tv_total);
				
				if(total_15 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 15. ');
					throw ' TV Program Total Quota Exceeded for Day 15 .';
					
				}		
				
				//===========================  15  ===========================
				
				
				
				//===========================  16  ===========================
				
				var salesrep_day_16='custrecord_bec_qm_16';
				var tv_day_16='custrecord_adinv_tq_16';
				
				var i_dailyquota_16=nlapiGetFieldValue(salesrep_day_16);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 16 --> ' + i_dailyquota_16);
				
				i_dailyquota_16=validateValue_zero(i_dailyquota_16);
				
				var total_daily_quota_16=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_16 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 16 --> ' + total_daily_quota_16);
				
			
	var total_16=parseFloat(i_dailyquota_16)+parseFloat(total_daily_quota_16);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 16  ********* --> ' + total_16);
								

				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_16)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 16  ********* --> ' + tv_total);
				
				if(total_16 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 16. ');
					throw ' TV Program Total Quota Exceeded for Day 16. ';
					
				}		
				
				//===========================  16  ===========================
				
				//===========================  17  ===========================
				
				var salesrep_day_17='custrecord_bec_qm_17';
				var tv_day_17='custrecord_adinv_tq_17';
				
				var i_dailyquota_17=nlapiGetFieldValue(salesrep_day_17);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 17 --> ' + i_dailyquota_17);
				
				i_dailyquota_17=validateValue_zero(i_dailyquota_17);
				
				var total_daily_quota_17=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_17 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 17 --> ' + total_daily_quota_17);
				
			
				var total_17=parseFloat(i_dailyquota_17)+parseFloat(total_daily_quota_17);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 17  ********* --> ' + total_17);
						
	
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_17)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 17  ********* --> ' + tv_total);
				
				if(total_17 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 17. ');
					throw ' TV Program Total Quota Exceeded for Day 17 .';
					
				}		
				
				//===========================  17  ===========================
				
				//===========================  18  ===========================
				
				var salesrep_day_18='custrecord_bec_qm_18';
				var tv_day_18='custrecord_adinv_tq_18';
				
				var i_dailyquota_18=nlapiGetFieldValue(salesrep_day_18);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 18 --> ' + i_dailyquota_18);
				
				i_dailyquota_18=validateValue_zero(i_dailyquota_18);
				
				var total_daily_quota_18=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_18 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 18 --> ' + total_daily_quota_18);
				
				
				var total_18=parseFloat(i_dailyquota_18)+parseFloat(total_daily_quota_18);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 18  ********* --> ' + total_18);
				
			
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_18)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 18  ********* --> ' + tv_total);
				
				if(total_18 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 18. ');
					throw ' TV Program Total Quota Exceeded for Day 18 . ';
					
				}		
				
				//===========================  18  ===========================
				
				
				//===========================  19  ===========================
				
				var salesrep_day_19='custrecord_bec_qm_19';
				var tv_day_19='custrecord_adinv_tq_19';
				
				var i_dailyquota_19=nlapiGetFieldValue(salesrep_day_19);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 19 --> ' + i_dailyquota_19);
				
				i_dailyquota_19=validateValue_zero(i_dailyquota_19);
				
				var total_daily_quota_19=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_19 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 19 --> ' + total_daily_quota_19);
				
			
				var total_19=parseFloat(i_dailyquota_19)+parseFloat(total_daily_quota_19);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 19  ********* --> ' + total_19);
			
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_19)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 19  ********* --> ' + tv_total);
				
				if(total_19 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 19. ');
					throw ' TV Program Total Quota Exceeded for Day 19 .';
					
				}		
				
				//===========================  19  ===========================
				
				
				
				
				
				//===========================  20  ===========================
				
				var salesrep_day_20='custrecord_bec_qm_20';
				var tv_day_20='custrecord_adinv_tq_20';
				
				var i_dailyquota_20=nlapiGetFieldValue(salesrep_day_20);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 20 --> ' + i_dailyquota_20);
				
				i_dailyquota_20=validateValue_zero(i_dailyquota_20);
				
				var total_daily_quota_20=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_20 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 20 --> ' + total_daily_quota_20);
				
				
			
	var total_20=parseFloat(i_dailyquota_20)+parseFloat(total_daily_quota_20);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 20  ********* --> ' + total_20);
				
			
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_20)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 20  ********* --> ' + tv_total);
				
				if(total_20 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 20. ');
					throw ' TV Program Total Quota Exceeded for Day 20 .';
					
				}		
				
				//===========================  20  ===========================
				
				
				//===========================  21  ===========================
				
				var salesrep_day_21='custrecord_bec_qm_21';
				var tv_day_21='custrecord_adinv_tq_21';
				
				var i_dailyquota_21=nlapiGetFieldValue(salesrep_day_21);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 21 --> ' + i_dailyquota_21);
				
				i_dailyquota_21=validateValue_zero(i_dailyquota_21);
				
				var total_daily_quota_21=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_21 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 21 --> ' + total_daily_quota_21);
				
			
				var total_21=parseFloat(i_dailyquota_21)+parseFloat(total_daily_quota_21);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 21  ********* --> ' + total_21);
			
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_21)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 21  ********* --> ' + tv_total);
				
				if(total_21 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 21. ');
					throw ' TV Program Total Quota Exceeded for Day 21.';
					
				}		
				
				//===========================  21  ===========================
				
				
				
				//===========================  22  ===========================
				
				var salesrep_day_22='custrecord_bec_qm_22';
				var tv_day_22='custrecord_adinv_tq_22';
				
				var i_dailyquota_22=nlapiGetFieldValue(salesrep_day_22);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 22 --> ' + i_dailyquota_22);
				
				i_dailyquota_22=validateValue_zero(i_dailyquota_22);
				
				var total_daily_quota_22=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_22 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 22 --> ' + total_daily_quota_22);
				
			
	var total_22=parseFloat(i_dailyquota_22)+parseFloat(total_daily_quota_22);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 22  ********* --> ' + total_22);
			
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_22)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 22  ********* --> ' + tv_total);
				
				if(total_22 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 22. ');
					throw ' TV Program Total Quota Exceeded for Day 22 .';
					
				}		
				
				//===========================  22  ===========================
				
				
				//===========================  23  ===========================
				
				var salesrep_day_23='custrecord_bec_qm_23';
				var tv_day_23='custrecord_adinv_tq_23';
				
				var i_dailyquota_23=nlapiGetFieldValue(salesrep_day_23);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 23 --> ' + i_dailyquota_23);
				
				i_dailyquota_23=validateValue_zero(i_dailyquota_23);
				
				var total_daily_quota_23=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_23 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 23 --> ' + total_daily_quota_23);
				
		
				var total_23=parseFloat(i_dailyquota_23)+parseFloat(total_daily_quota_23);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 23  ********* --> ' + total_23);
			
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_23)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 23  ********* --> ' + tv_total);
				
				if(total_23 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 23. ');
					throw ' TV Program Total Quota Exceeded for Day 23 .';
					
				}		
				
				//===========================  23  ===========================
				
				//===========================  24  ===========================
				
				var salesrep_day_24='custrecord_bec_qm_24';
				var tv_day_24='custrecord_adinv_tq_24';
				
				var i_dailyquota_24=nlapiGetFieldValue(salesrep_day_24);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 24 --> ' + i_dailyquota_24);
				
				i_dailyquota_24=validateValue_zero(i_dailyquota_24);
				
				var total_daily_quota_24=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_24 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 24 --> ' + total_daily_quota_24);
				
			
	var total_24=parseFloat(i_dailyquota_24)+parseFloat(total_daily_quota_24);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 24  ********* --> ' + total_24);
			
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_24)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 24  ********* --> ' + tv_total);
				
				if(total_24 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 24. ');
					throw ' TV Program Total Quota Exceeded for Day 24 .';
					
				}		
				
				//===========================  24  ===========================
				
				
				//===========================  25  ===========================
				
				var salesrep_day_25='custrecord_bec_qm_25';
				var tv_day_25='custrecord_adinv_tq_25';
				
				var i_dailyquota_25=nlapiGetFieldValue(salesrep_day_25);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 25 --> ' + i_dailyquota_25);
				
				i_dailyquota_25=validateValue_zero(i_dailyquota_25);
				
				var total_daily_quota_25=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_25 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 25 --> ' + total_daily_quota_25);
				
			
	
				var total_25=parseFloat(i_dailyquota_25)+parseFloat(total_daily_quota_25);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 25  ********* --> ' + total_25);
			
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_25)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 25  ********* --> ' + tv_total);
				
				if(total_25 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 25. ');
					throw ' TV Program Total Quota Exceeded for Day 25.';
					
				}		
				
				//===========================  25  ===========================
				
				
				
				//===========================  26  ===========================
				
				var salesrep_day_26='custrecord_bec_qm_26';
				var tv_day_26='custrecord_adinv_tq_26';
				
				var i_dailyquota_26=nlapiGetFieldValue(salesrep_day_26);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 26 --> ' + i_dailyquota_26);
				
				i_dailyquota_26=validateValue_zero(i_dailyquota_26);
				
				var total_daily_quota_26=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_26 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 26 --> ' + total_daily_quota_26);
				
		
	var total_26=parseFloat(i_dailyquota_26)+parseFloat(total_daily_quota_26);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 26  ********* --> ' + total_26);
				
			
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_26)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 26  ********* --> ' + tv_total);
				
				if(total_26 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 26. ');
					throw ' TV Program Total Quota Exceeded for Day 26 .';
					
				}		
				
				//===========================  26  ===========================
				
				
				
				//===========================  27  ===========================
				
				var salesrep_day_27='custrecord_bec_qm_27';
				var tv_day_27='custrecord_adinv_tq_27';
				
				var i_dailyquota_27=nlapiGetFieldValue(salesrep_day_27);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 27 --> ' + i_dailyquota_27);
				
				i_dailyquota_27=validateValue_zero(i_dailyquota_27);
				
				var total_daily_quota_27=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_27 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 27 --> ' + total_daily_quota_27);
				
			
				var total_27=parseFloat(i_dailyquota_27)+parseFloat(total_daily_quota_27);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 27  ********* --> ' + total_27);
								
				

				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_27)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 27  ********* --> ' + tv_total);
				
				if(total_27 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 27. ');
					throw ' TV Program Total Quota Exceeded for Day 27.';
					
				}		
				
				//===========================  27  ===========================
				
				//===========================  28  ===========================
				
				var salesrep_day_28='custrecord_bec_qm_28';
				var tv_day_28='custrecord_adinv_tq_28';
				
				var i_dailyquota_28=nlapiGetFieldValue(salesrep_day_28);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 28 --> ' + i_dailyquota_28);
				
				i_dailyquota_28=validateValue_zero(i_dailyquota_28);
				
				var total_daily_quota_28=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_28 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 28 --> ' + total_daily_quota_28);
				
				
			
	var total_28=parseFloat(i_dailyquota_28)+parseFloat(total_daily_quota_28);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 28  ********* --> ' + total_28);
								
				
			var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_28)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 28  ********* --> ' + tv_total);
				
				if(total_28 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 28. ');
					throw ' TV Program Total Quota Exceeded for Day 28 .';
					
				}		
				
				//===========================  28  ===========================
				
				
				//===========================  29  ===========================
				
				var salesrep_day_29='custrecord_bec_qm_29';
				var tv_day_29='custrecord_adinv_tq_29';
				
				var i_dailyquota_29=nlapiGetFieldValue(salesrep_day_29);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 29 --> ' + i_dailyquota_29);
				
				i_dailyquota_29=validateValue_zero(i_dailyquota_29);
				
				var total_daily_quota_29=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_29 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 29 --> ' + total_daily_quota_29);
				
			
	
				var total_29=parseFloat(i_dailyquota_29)+parseFloat(total_daily_quota_29);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 29  ********* --> ' + total_29);
					
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_29)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 29  ********* --> ' + tv_total);
				
				if(total_29 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 29. ');
					throw ' TV Program Total Quota Exceeded for Day 29. ';
					
				}		
				
				//===========================  29  ===========================
				
				
				//===========================  30  ===========================
				
				var salesrep_day_30='custrecord_bec_qm_30';
				var tv_day_30='custrecord_adinv_tq_30';
				
				var i_dailyquota_30=nlapiGetFieldValue(salesrep_day_30);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 30 --> ' + i_dailyquota_30);
				
				i_dailyquota_30=validateValue_zero(i_dailyquota_30);
				
				var total_daily_quota_30=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_30 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 30 --> ' + total_daily_quota_30);
		
				var total_30=parseFloat(i_dailyquota_30)+parseFloat(total_daily_quota_30);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 30  ********* --> ' + total_30);
				
				
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_30)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 30  ********* --> ' + tv_total);
				
				if(total_30 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 30. ');
					throw ' TV Program Total Quota Exceeded for Day 30.';
					
				}		
				
				//===========================  30  ===========================
				
				//===========================  31  ===========================
				
				var salesrep_day_31='custrecord_bec_qm_31';
				var tv_day_31='custrecord_adinv_tq_31';
				
				var i_dailyquota_31=nlapiGetFieldValue(salesrep_day_31);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Quota 31 --> ' + i_dailyquota_31);
				
				i_dailyquota_31=validateValue_zero(i_dailyquota_31);
				
				var total_daily_quota_31=sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day_31 );
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Sales Rep Total Daily Quota 31 --> ' + total_daily_quota_31);
				
			
	
				var total_31=parseFloat(i_dailyquota_31)+parseFloat(total_daily_quota_31);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** Sales Rep Total Quota 31  ********* --> ' + total_31);
				
			
				
				var tv_total=tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day_31)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' ******** TV Program Total Quota 31  ********* --> ' + tv_total);
				
				if(total_31 > tv_total)
				{
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program Total Quota Exceeded for Day 31. ');
					throw ' TV Program Total Quota Exceeded for Day 31.';
					
				}		
				
				//===========================  31  ===========================
				
				
				
				
			}//Sales Rep Object
			
		}//Sales REp ID 		
		
	}//Try
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// ==========================================================
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
		
		
	   	
	   }  
		
	
	
		return true;


}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_SR_quota_management(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	
	
	var item_unit;
	var media_type;
	
	
	if (type == 'create' ) 
	{
		
		///==============================================================================
		
		
		
		
		////==============================================================================
		
		
		
	     var SR_quota =nlapiGetRecordId();
		 nlapiLogExecution('DEBUG', 'afterSubmit', 'SR_quota ID = ' + SR_quota);
		
		 //load that Record
				 
		 var Sales_rep_quota_Object_original = nlapiLoadRecord('customrecord_sales_rep_quota_mngm',SR_quota);
		
		//get all the record value
		var program_name=Sales_rep_quota_Object_original.getFieldValue('custrecord_bec_qm_program');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'program name = ' + program_name);
		
		var sales_rep=Sales_rep_quota_Object_original.getFieldValue('custrecord_bec_qm_salesrep');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Sales rep = ' + sales_rep);
								
		
		var month=Sales_rep_quota_Object_original.getFieldValue('custrecord_bec_qm_month');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'MOnth = ' + month);
		
		var year=Sales_rep_quota_Object_original.getFieldValue('custrecord_bec_qm_year');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Year = ' + year);
		
	
	  if(program_name!=null && program_name!=undefined && program_name!='')
	  {
	  	var itemOBJ=nlapiLoadRecord('serviceitem',program_name);
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Object='+itemOBJ);
		
		
		if(itemOBJ!=null && itemOBJ!='')
		{
			media_type=itemOBJ.getFieldValue('custitem_bec_mediatype');
			nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Media Type ='+media_type);
			
			
			item_unit=itemOBJ.getFieldValue('custitem3');
			nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Program Item Units ='+item_unit);
			
			
		}//If
		
	  }
			
		
		Sales_rep_quota_Object_original.setFieldValue('custrecord_bec_qm_type',2);
		
		if(media_type!=null && media_type!='' && media_type!=undefined)
		{
			  Sales_rep_quota_Object_original.setFieldValue('custrecord_bec_qm_adtype',media_type);
		}
		
	  
	     if(item_unit!=null && item_unit!='' && item_unit!=undefined)
		 {
		 	Sales_rep_quota_Object_original.setFieldValue('custrecord_quota_unitofmesure',item_unit);
		 }
		
	    
		
		var id_original = nlapiSubmitRecord(Sales_rep_quota_Object_original, true);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Original Sales rep Quota Record ID  = ' + id_original);	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Craete a new sales rep quota record 
		var new_Sales_rep_quota_Object= nlapiCreateRecord('customrecord_sales_rep_quota_mngm');
		
		//set the all value
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_salesrep',sales_rep);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_type',1);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_month',month);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_year',year);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_quota_unitofmesure',item_unit);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_program',program_name);
		
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_adtype',media_type);
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_1','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_2','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_3','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_4','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_5','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_6','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_7','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_8','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_9','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_10','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_11','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_12','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_13','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_14','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_15','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_16','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_17','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_18','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_19','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_20','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_21','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_22','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_23','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_24','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_25','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_26','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_27','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_28','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_29','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_30','0');
		
		new_Sales_rep_quota_Object.setFieldValue('custrecord_bec_qm_31','0');
		
		var id = nlapiSubmitRecord(new_Sales_rep_quota_Object, true);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'New Sales rep Quota Record ID  = ' + id);		
		
		
		
	     }
		
		
		

		 
		
		
		
		
		
		
		

	return true;

}

// END AFTER SUBMIT ===============================================



function validateValue_zero(value)
{
	var value_res;
	if(value==null || value ==undefined || value=='' || value ==NaN || value.toString() == 'NaN' || value == '.00' || value=='NaN')
	{
		value_res=0;
		return value_res;
	}
	else
	{
		return value;
	}
	
}





function validateValue(value)
{
	var value_res;
	
	if(value==null || value ==undefined || value=='' || value ==NaN || value == 'NaN')
	{
		value_res='';
		return value_res;
	}
	else
	{
		return value;
	}
	
}






function sales_rep_total(i_tv_program,i_media_type,i_unit,i_year,i_month,salesrep_day)
{
	var total_quota=0;
		// =========== Sales Rep Search =====================
						
		 var salesrep_filters = new Array();
		 var salesrep_columns = new Array();
		  
		  
		  salesrep_filters[0]= new nlobjSearchFilter('custrecord_bec_qm_program',null,'is', i_tv_program);
		  salesrep_filters[1]= new nlobjSearchFilter('custrecord_bec_qm_adtype',null,'is',i_media_type);
		  salesrep_filters[2]= new nlobjSearchFilter('custrecord_quota_unitofmesure',null,'is',i_unit);
		  salesrep_filters[3]= new nlobjSearchFilter('custrecord_bec_qm_year',null,'is',i_year);
		  salesrep_filters[4]= new nlobjSearchFilter('custrecord_bec_qm_month',null,'is',i_month);
		  
		  salesrep_columns[0] =  new nlobjSearchColumn('internalid')
		  salesrep_columns[1] =  new nlobjSearchColumn(salesrep_day)
											
		  var search_result_salesrep = nlapiSearchRecord('customrecord_sales_rep_quota_mngm',null,salesrep_filters,salesrep_columns)
		  
		  if (search_result_salesrep != null && search_result_salesrep != undefined && search_result_salesrep != '') 
		  {
		  	nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  No Of Sales Rep Records -->' + search_result_salesrep.length);
		  	
			for(var t=0;t<search_result_salesrep.length;t++)
			{
				 var i_salesrep_ID = search_result_salesrep[t].getValue('internalid');
			     nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Record ID  [ Sales Rep Quota Managment ][' +t + '] -->' + i_salesrep_ID);
				
				 var i_salesrep_daily_quota = search_result_salesrep[t].getValue(salesrep_day);
			     nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Daily Quota  [ Sales Rep Daily Quota ][' +t + '] -->' + i_salesrep_daily_quota);
				
					
				i_salesrep_daily_quota=i_salesrep_daily_quota;
				i_salesrep_daily_quota=validateValue_zero(i_salesrep_daily_quota);	
						
					
				
				 total_quota=parseFloat(total_quota)+parseFloat(i_salesrep_daily_quota);
				 nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Total Daily Quota  [ Sales Rep Daily Quota ][' +t + '] -->' + total_quota);
				
			}//Loop Sales Rep
	
		  }
		  
	 nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Total Quota  [ Sales Rep Daily Quota ][' +t + '] -->' + total_quota);
	
	total_quota=validateValue_zero(total_quota);	
	return total_quota;
	
}










function tv_quota_daywise_total(i_tv_program,i_media_type,i_unit,i_year,i_month,tv_day)
{
	var i_tvquota_totalquota;	
									
	if(i_tv_program!=null && i_tv_program!='' && i_tv_program!=undefined)
	{
		if(i_media_type!=null && i_media_type!='' && i_media_type!=undefined)
		{
			if(i_unit!=null && i_unit!='' && i_unit!=undefined)
			{
				if(i_year!=null && i_year!='' && i_year!=undefined)
				{
					if(i_month!=null && i_month!='' && i_month!=undefined)
					{
						 var filters = new Array();
						  var columns = new Array();
						  
						  
						  filters[0]= new nlobjSearchFilter('custrecord_adinv_programitem',null,'is', i_tv_program);
						  filters[1]= new nlobjSearchFilter('custrecord_adinv_adtype',null,'is',i_media_type);
						  filters[2]= new nlobjSearchFilter('custrecord_adinv_unitofmeasure',null,'is',i_unit);
						  filters[3]= new nlobjSearchFilter('custrecord_adinv_year',null,'is',i_year);
						  filters[4]= new nlobjSearchFilter('custrecord_adinv_month',null,'is',i_month);
						  
						  columns[0] =  new nlobjSearchColumn('internalid');
						  columns[1] =  new nlobjSearchColumn(tv_day)
															
						  var search_result = nlapiSearchRecord('customrecord_ad_inventory',null,filters,columns)
						  
						  if(search_result != null && search_result != undefined && search_result != '')
						  {
						  	 nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  No Of TV Program Quota Managment Records -->' + search_result.length);
							 
								for (var i = 0; i < search_result.length; i++) 
								{
									
								 var i_tvquota_recordID = search_result[i].getValue('internalid');
	                             nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Record ID  [ TV Program Quota Managment ][' + i + '] -->' + i_tvquota_recordID);
	
								 i_tvquota_totalquota = search_result[i].getValue(tv_day);
	                             nlapiLogExecution('DEBUG', 'afterSubmitRecord', '  Total Quota  [ TV Program Quota Managment ][' + i + '] -->' + i_tvquota_totalquota);			
																					
								}//Loop TV Quoata
																	
						  }//Search Result Check
															
					}//Month Check
												
				}//Year Check
												
			}//Unit Check					
			
		}//Media Type Check
							
	}// TV Program	
	i_tvquota_totalquota=validateValue_zero(i_tvquota_totalquota);	
	return i_tvquota_totalquota;
	
}// TV Quota
