/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_SalesRep_Disable_DataType.js
	Date       : 10 May 2013
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================



//411027

// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
	
	
	nlapiDisableField('custrecord_bec_qm_type', true)
	
	
	
	if(type=='create')
	{
	  nlapiSetFieldValue('custrecord_bec_qm_type',2)
	 	
		
	}
	
	
	
	
	
	
	
	
	


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
   var media_type=nlapiGetFieldValue('custrecord_bec_qm_adtype');
   
   var unit=nlapiGetFieldValue('custrecord_quota_unitofmesure');
      
   var data_type=nlapiGetFieldValue('custrecord_bec_qm_type');  
   
   
   
	  
   if(media_type=='' || media_type==null)
   {
   	
	  alert(' Please enter Media Type ')
	  return false;
   }	
   
    if(unit=='' || unit==null)
   {
   	
	  alert(' Please enter Unit');
	  return false;
	
   }	  

	

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_sales(type, name, linenum)
{
   /*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	
	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	var blank='';
	var media_type;
	var item_unit;
	
	if (name == 'custrecord_bec_qm_program') 
	{
		var tv_program = nlapiGetFieldValue('custrecord_bec_qm_program');
		
		
		if (tv_program != null && tv_program != '' && tv_program != undefined)
		{
		
			var itemOBJ = nlapiLoadRecord('serviceitem', tv_program);
			
			
			
			if (itemOBJ != null && itemOBJ != '') 
			{
				media_type = itemOBJ.getFieldValue('custitem_bec_mediatype');
				
				item_unit = itemOBJ.getFieldValue('custitem3');
								
				
			}//If
			
			if(media_type!=null && media_type!='' && media_type!=undefined)
			{
				nlapiSetFieldValue('custrecord_bec_qm_adtype',media_type);
			}
			else
			{
				nlapiSetFieldValue('custrecord_bec_qm_adtype',blank);
			}
			
			if(item_unit!=null && item_unit!='' && item_unit!=undefined)
			{
				nlapiSetFieldValue('custrecord_quota_unitofmesure',item_unit);
				
			}
		    else
			{
				nlapiSetFieldValue('custrecord_quota_unitofmesure',blank);
			}
			
			
			
			
		 }
		
		
	}
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
