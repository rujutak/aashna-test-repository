/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Appendix_I.js
	Date        : 30 April 2013
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		if (request.getMethod() == 'GET')
		{
						
			var form = nlapiCreateForm('Appendix I Print');
	
			var item = form.addField('custpage_i_item', 'select', 'TV Program ', 'item');
			var date = form.addField('custpage_i_date', 'date', 'Date', '');
			
			var submit_button = form.addSubmitButton();
			response.writePage(form);
			
			
		}//GET METHOD
		else if (request.getMethod() == 'POST')
		{
			nlapiLogExecution('DEBUG','In Suitelet Post Method',' ******** POST ************');
			
			var post_item = request.getParameter('custpage_i_item');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Item --> '+post_item);
			
			var post_date = request.getParameter('custpage_i_date');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Date --> '+post_date);
			
			var params=new Array();

	  		params['status']='scheduled';
	  		params['runasadmin']='T';
		  	params['custscript_i_tv_item']=post_item;
		  	params['custscript_i_mp_date']=post_date;
		  	var startDate = new Date();
		  	params['startdate']=startDate.toUTCString();

		  	var status = nlapiScheduleScript('customscript_vtr_appendix_i','customdeploy1',params);
	  		nlapiLogExecution('DEBUG', 'In Suitelet Post Method', ' Status : '+status);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}//GET METHOD
			
	}//try
	catch(er)
	{
		nlapiLogExecution('DEBUG', 'suiteletFunction', 'Exception Caught -->' + er);
	}//catch

	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
