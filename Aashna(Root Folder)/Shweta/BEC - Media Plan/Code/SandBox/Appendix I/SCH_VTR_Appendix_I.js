/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_VTR_Appendix_I.js
	Date       : 30 April 2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	
	var end_date;
	var start_date;
	var alias;
	var old_duration;
	var new_duration;
	var mediaName_text;
	var media_type;
	var salesorder_text;
	var mediaName_text;
	var mediaName_ID;
	var advertiser_text;
	var product_text;
	var salesRep_text;
	var media_type_tv;
	var print_arr=new Array();
	var pnt=0;
	var media_type_tv_txt;
	
    var context = nlapiGetContext();
    var remainingUsage = context.getRemainingUsage();
    
    var item = context.getSetting('SCRIPT', 'custscript_i_tv_item');
    var date = context.getSetting('SCRIPT', 'custscript_i_mp_date');
    
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' TV Program --> ' + item)
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Date --> ' + date) 
	
	
	var filter = new Array();
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp3_tvprogram', 'custrecord_mp3_mp1ref', 'is', item);
    filter[1] = new nlobjSearchFilter('custrecord_mp1_oa_startdate', null, 'onorbefore', date);
    filter[2] = new nlobjSearchFilter('custrecord_mp1_oa_enddate', null, 'onorafter', date);
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_1', 'customsearch622', filter, null);
	
      if (searchresults != null) 
	  {
	  	 nlapiLogExecution('DEBUG', 'schedulerFunction', ' Media Plan Search Results --> ' + searchresults.length);
		
		  for (var t = 0; searchresults != null && t < searchresults.length; t++) 
		  {
		  
		  	var media_plan_result = searchresults[t];
		  	var columns = media_plan_result.getAllColumns();
		  	var columnLen = columns.length;
		  	
		  	//Media Plan Internal ID
			var column1 = columns[0];
			var media_plan_id = media_plan_result.getValue(column1);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Media Record ID [' + t + ' ] --> ' + media_plan_id);
			
			//Sales Rep
			var column2 = columns[1];
			var saleRep_ID = media_plan_result.getValue(column2);
			salesRep_text = media_plan_result.getText(column2);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Sales Rep [' + t + ' ] --> ' + salesRep_text);
			
			
			//Product
            var column4 = columns[2];
            var product_ID = media_plan_result.getValue(column4);
            product_text = media_plan_result.getText(column4);
            nlapiLogExecution('DEBUG', 'schedulerFunction', ' Product [' + t + ' ] --> ' + product_text);
            
            //Media Name
            var column5 = columns[3];
            mediaName_ID = media_plan_result.getValue(column5);
            mediaName_text = media_plan_result.getValue(column5);
            nlapiLogExecution('DEBUG', 'schedulerFunction', ' Media Name [' + t + ' ] --> ' + mediaName_text);
            
			
			   //Alias
            var column10 = columns[4];
            alias = media_plan_result.getValue(column10);
            nlapiLogExecution('DEBUG', 'schedulerFunction', ' Alias  [' + t + ' ] --> ' + alias);
            
			
			
			  //On Air Start Date
            var column11 = columns[5];
            start_date = media_plan_result.getValue(column11);
            nlapiLogExecution('DEBUG', 'schedulerFunction', 'Start Date --> ' + start_date);
            
            //On Air End Date
            var column12 = columns[6];
            end_date = media_plan_result.getValue(column12);
            nlapiLogExecution('DEBUG', 'schedulerFunction', 'End Date --> ' + end_date);
			
			
			
			
			
			//============================= Media Type ===================================
			
					
			if(item!=null && item!=''&& item!=undefined)
			{
				var item_id;
				var tv_programOBJ=nlapiLoadRecord('serviceitem',item);
				nlapiLogExecution('DEBUG','schedulerFunction','  TV Program Object [ Service Item ]--->'+tv_programOBJ);
			    
				if(tv_programOBJ!=null)
				{
					media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
					nlapiLogExecution('DEBUG','schedulerFunction','  TV Program Media Type [ Service Item ]-->'+media_type_tv);
					
					media_type_tv_txt=tv_programOBJ.getFieldText('custitem_bec_mediatype');
					nlapiLogExecution('DEBUG','schedulerFunction','  TV Program Media Type Text[ Service Item ]-->'+media_type_tv_txt);
					
					item_id=tv_programOBJ.getFieldValue('itemid');
					nlapiLogExecution('DEBUG','schedulerFunction','  TV Program Item [ Service Item ]-->'+item_id);
					
					
				}//Null Check
			
			}//Item 
						
			
			//======================= Media Type ===================================
			
			
			//	if(media_type_tv==2)
				{
					var date_arr = new Array();
		            date_arr = date.split('/');
		            var day = date_arr[0];
		            var month = date_arr[1];
		            var year = date_arr[2];
		            nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day  : ' + day + ' Month : ' + month + ' Year :' + year);
						
				
				  var media_sch_alias = moth_year_wisealiascheck(alias, day, month, year, item, media_plan_id);
                  nlapiLogExecution('DEBUG', 'schedulerFunction', ' Media Schedule Alias -->' + media_sch_alias);
				
				
					if(media_sch_alias!='')
					{
						print_arr[pnt++]=product_text+'$$'+start_date+'$$'+end_date+'$$'+mediaName_text+'$$'+salesRep_text+'$$'+media_type_tv;
					}
				
				
				
				
				
					
					
				}//Check Media Type 2...............
					
			}//For 
				
	  }//Search Results 
	  
	
	  
        nlapiLogExecution('DEBUG', 'schedulerFunction', ' Print Array -->' + print_arr);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Print Array Count -->' + pnt);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Print Array -->' + print_arr.length);
				
				
	   appendix_I_print_layout(print_arr,item_id,media_type_tv_txt);

}//Scheduler Function

// END SCHEDULED FUNCTION ===============================================




function appendix_I_print_layout(print_arr,item_id,media_type_tv_txt)
{
var strVar="";


item_id=validateValue(item_id);

media_type_tv_txt=validateValue(media_type_tv_txt);

if(media_type_tv_txt!=null && media_type_tv_txt!='' && media_type_tv_txt!=undefined)
{
	media_type_tv_txt=media_type_tv_txt.toUpperCase();
	
	
}


strVar += "<table border=\"1\" width=\"100%\">";
strVar += "	<tr>";
strVar += "		<td>";
strVar += "		<table border=\"0\" width=\"100%\">";
strVar += "			<tr>";
strVar += "				<td font-size=\"9\" align=\"center\" font-family=\"Helvetica\"><b>"+media_type_tv_txt+" PLAN<\/b><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-size=\"9\" align=\"center\" font-family=\"Helvetica\"><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-size=\"9\" align=\"center\" font-family=\"Helvetica\"><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-size=\"8\" font-family=\"Helvetica\"><b>TV Program : "+ item_id+"<\/b><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td>";
strVar += "				<table border=\"0\" width=\"100%\">";
strVar += "					<tr>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><b>Product<\/b><\/td>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><b>Period<\/b><\/td>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><b>Wording<\/b><\/td>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><b>Sales<\/b><\/td>";
strVar += "					<\/tr>";




if(print_arr.length!=0)
{
	
    var i_array=new Array();

    var i_product;
	var i_start_date;
 	var i_end_date ;
 	var i_mediaName ;
 	var i_sales_rep ;
	var i_media_type ;

 for (var t = 0; t < print_arr.length; t++) 
 {
 	i_array = print_arr[t].split('$$');
 	
 	nlapiLogExecution('DEBUG', 'printPDF', '****************  t *************' + t);
 	
 	
 	i_product = i_array[0];
	i_start_date = i_array[1];
 	i_end_date = i_array[2];
 	i_mediaName = i_array[3];
 	i_sales_rep = i_array[4];
	i_media_type = i_array[5];
	
	
	 var start_day;
	 var start_month;
	 var start_year ;
	 
	 
	if(i_start_date!=null && i_start_date!='' && i_start_date!=undefined)
	{
	// ==================== Period ===========================
	
	var start_date_arr = new Array();
    start_date_arr = i_start_date.split('/');
    start_day = start_date_arr[0];
    start_month = start_date_arr[1];
    start_year = start_date_arr[2];
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Start Day  : ' + start_day + 'Start Month : ' + start_month + ' Start Year :' + start_year);
	
		
		
	}
	var end_day;
	var end_month;
	var end_year;
	
	
	
	if(i_end_date!=null && i_end_date!='' && i_end_date!=undefined)
	{
	var end_date_arr = new Array();
    end_date_arr = i_end_date.split('/');
    end_day = end_date_arr[0];
    end_month = end_date_arr[1];
    end_year = end_date_arr[2];
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' End Day  : ' + end_day + 'End Month : ' + end_month + ' End Year :' + end_year);
	
		
		
	}
	var start_mon;
	var end_mon;
	
	if(start_month!=null && start_month!='' && start_month!=undefined)
	{
		start_mon=month_text(start_month);
		
	}
	if(end_month!=null && end_month!='' && end_month!=undefined)
	{
		end_mon=month_text(end_month);
	}
	
	
	
	var period;
	
	if(start_mon!='' && start_mon!=null && start_mon!=undefined)
	{
			if(start_mon!='' && start_mon!=null && start_mon!=undefined)
			{
				period=start_mon+'-'+end_mon+' '+end_year
				
			}
	}
	
	start_mon=validateValue(start_mon);
	end_mon=validateValue(end_mon);
	period=validateValue(period);
	
	
	nlapiLogExecution('DEBUG', 'printPDF', '****************  start_mon *************' + start_mon);
	nlapiLogExecution('DEBUG', 'printPDF', '****************  end_mon *************' + end_mon);
	nlapiLogExecution('DEBUG', 'printPDF', '****************  period *************' + period);
		
	// ======================= Period ===========================
	
	
	
	i_mediaName=validateValue(i_mediaName);
	i_sales_rep=validateValue(i_sales_rep);
	i_product=validateValue(i_product);
	
	
	
strVar += "					<tr>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\">"+i_product+"<\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\">"+period+"<\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\">"+i_mediaName+"<\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\">"+i_sales_rep+"<\/td>";
strVar += "					<\/tr>";
	
	
 }
	
	
}
else
{
	
		
strVar += "					<tr>";
strVar += "						<td align=\"center\" border-bottom=\"0\" border-left=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "					<\/tr>";

	
strVar += "					<tr>";
strVar += "						<td align=\"center\" border-bottom=\"0\" border-left=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "					<\/tr>";


	
strVar += "					<tr>";
strVar += "						<td align=\"center\" border-bottom=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-size=\"8\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "						<td align=\"center\" font-size=\"8\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\"><\/td>";
strVar += "					<\/tr>";
	
	
	
}







strVar += "				<\/table>";
strVar += "				<\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "<\/table>";
strVar += "";

        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
		xml += "<pdf>\n<body>";
		xml += strVar;
		xml += "</body>\n</pdf>";
		// run the BFO library to convert the xml document to a PDF 
		// run the BFO library to convert the xml document to a PDF 
		var file = nlapiXMLToPDF(xml);
		nlapiLogExecution('DEBUG', 'printPDF', 'File =='+file);
		//Create PDF File with TimeStamp in File Name
		
		
		var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	var fileObj = nlapiCreateFile('Appendix I.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
				
		nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj='+ fileObj);
		fileObj.setFolder(219);// Folder PDF File is to be stored
		var fileId=nlapiSubmitFile(fileObj);
		nlapiLogExecution('DEBUG', 'printPDF', '*************===PDF fileId *********** =='+ fileId);
        // Run the BFO library to convert the xml document to a PDF 
		// Set content type, file name, and content-disposition (inline means display in browser)
	
	
	
	
		
		
		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		

	
	
	
	
}




function moth_year_wisealiascheck(alias, day, month, year, item, media_plan_id)
{
    var day_alias;
    var day_id;
    if (day == 1) 
	{
        day_id = 'custrecord_mp3_1';
    }
    else if (day == 2) 
	{
        day_id = 'custrecord_mp3_2';
        
    }
    else if (day == 3) 
	{
        day_id = 'custrecord_mp3_3';
        
    }
    else if (day == 4)
	 {
        day_id = 'custrecord_mp3_4';
        
    }
    else if (day == 5) 
	{
        day_id = 'custrecord_mp3_5';
        
    }
                    else if (day == 6)
					{
                        day_id = 'custrecord_mp3_6';
                        
                     }
                        else if (day == 7) 
						{
                                day_id = 'custrecord_mp3_7';
                                
                        }
                         else if (day == 8) 
						{
                                    day_id = 'custrecord_mp3_8';
                                    
                          }
                                else if (day == 9)
								{
                                        day_id = 'custrecord_mp3_9';
                                        
                                  }
                                    else if (day == 10)
									{
                                            day_id = 'custrecord_mp3_10';
                                            
                                     }
                                     else if (day == 11) 
									{
                                        day_id = 'custrecord_mp3_11';
                                    }
                                          else 
                                                if (day == 12) 
												{
                                                    day_id = 'custrecord_mp3_12';
                                                }
                                                else 
                                                    if (day == 13) 
													{
                                                        day_id = 'custrecord_mp3_13';
                                                    }
                                                    else 
                                                        if (day == 14) 
														{
                                                            day_id = 'custrecord_mp3_14';
                                                            
                                                        }
                                                        else 
                                                            if (day == 15) 
															{
                                                                day_id = 'custrecord_mp3_15';
                                                                
                                                            }
                                                            else 
                                                                if (day == 16) 
																{
                                                                    day_id = 'custrecord_mp3_16';
                                                                    
                                                                }
                                                                else 
                                                                    if (day == 17)
																	 {
                                                                        day_id = 'custrecord_mp3_17';
                                                                        
                                                                    }
                                                                    else 
                                                                        if (day == 18) 
																		{
                                                                            day_id = 'custrecord_mp3_18';
                                                                            
                                                                        }
                                                                        else 
                                                                            if (day == 19) {
                                                                                day_id = 'custrecord_mp3_19';
                                                                                
                                                                            }
                                                                            else 
                                                                                if (day == 20) 
																				{
                                                                                    day_id = 'custrecord_mp3_20';
                                                                                    
                                                                                }
                                                                                else 
                                                                                    if (day == 21) {
                                                                                    
                                                                                        day_id = 'custrecord_mp3_21';
                                                                                    }
                                                                                    else 
                                                                                        if (day == 22) {
                                                                                            day_id = 'custrecord_mp3_22';
                                                                                            
                                                                                        }
                                                                                        else 
                                                                                            if (day == 23) {
                                                                                                day_id = 'custrecord_mp3_23';
                                                                                                
                                                                                            }
                                                                                            else 
                                                                                                if (day == 24) {
                                                                                                    day_id = 'custrecord_mp3_24';
                                                                                                    
                                                                                                }
                                                                                                else 
                                                                                                    if (day == 25) {
                                                                                                        day_id = 'custrecord_mp3_25';
                                                                                                        
                                                                                                    }
                                                                                                    else 
                                                                                                        if (day == 26) {
                                                                                                            day_id = 'custrecord_mp3_26';
                                                                                                            
                                                                                                        }
                                                                                                        else 
                                                                                                            if (day == 27) {
                                                                                                                day_id = 'custrecord_mp3_27';
                                                                                                                
                                                                                                            }
                                                                                                            else 
                                                                                                                if (day == 28) {
                                                                                                                    day_id = 'custrecord_mp3_28';
                                                                                                                    
                                                                                                                }
                                                                                                                else 
                                                                                                                    if (day == 29) {
                                                                                                                        day_id = 'custrecord_mp3_29';
                                                                                                                        
                                                                                                                    }
                                                                                                                    else 
                                                                                                                        if (day == 30) {
                                                                                                                            day_id = 'custrecord_mp3_30';
                                                                                                                            
                                                                                                                        }
                                                                                                                        else 
                                                                                                                            if (day == 31) {
                                                                                                                                day_id = 'custrecord_mp3_31';
                                                                                                                                
                                                                                                                            }
    
    
    var year_id = year_internalid(year, media_plan_id);
    nlapiLogExecution('DEBUG', 'searchMedialist', 'Year ID -->  ' + year_id);
    
    
    
    
    var ms_alias;
    var ms_recordID;
    
    
    nlapiLogExecution('DEBUG', 'searchMedialist', ' Day ID -->  ' + day_id);
    
    var ml_recordID;
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', media_plan_id);
    filter[1] = new nlobjSearchFilter('custrecord_mp3_tvprogram', null, 'is', item);
    filter[2] = new nlobjSearchFilter('custrecord_mp3_year', null, 'is', year_id);
    filter[3] = new nlobjSearchFilter('custrecord_mp3_month', null, 'is', month);
    //filter[4] = new nlobjSearchFilter(day_id, null, 'contains', alias);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn(day_id);
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, filter, columns);
    
    if (searchresults != null)
	 {
        nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++) 
		{
            ms_recordID = searchresults[j].getValue('internalid');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + ms_recordID);
            
            ms_alias = searchresults[j].getValue(day_id);
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Alias  [ Media Schedule ][' + j + ']==' + ms_alias);
            
            if (ms_alias.indexOf(alias) != -1) 
			{
                ms_alias = ms_alias;
                
            }
            else 
			{
                ms_alias = '';
            }
            
            
        }
    }
    
    
    return ms_alias;
}



function year_internalid(year, media_plan_id)
{
    var recordID;
    var year_ID;
    
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', media_plan_id);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, filter, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++) 
		{
            recordID = searchresults[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + recordID);
        
        }
    }
    
    
    
    var custOBJ = nlapiLoadRecord('customrecord_mediaplan_3', recordID);
    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Media Schedule -->' + custOBJ)
    
    if (custOBJ != null) 
	{
    
        var myFld = custOBJ.getField('custrecord_mp3_year')
        var options = myFld.getSelectOptions();
        nlapiLogExecution('Debug', 'CheckParameters', 'options=' + options.length);
        
        for (var i = 0; i < options.length; i++) 
		{
            var yearId = options[i].getId();
            var yearText = options[i].getText();
        //    nlapiLogExecution('DEBUG', 'suiteletFunction', 'i -->' + i + 'Year ID -->' + yearId + 'Year Text -->' + yearText);
            
            
            if (yearText == year) 
			{
                year_ID = yearId;
                break;
            }
            
            
        }//For
    }//Customer
    return year_ID;
}



function month_text(month)
{
	var monthtxt;
	
	if(month==1)
	{
		monthtxt='Jan';
		
	}
	else if(month==2)
	{
		monthtxt='Feb';
		
	}
	else if(month==3)
	{
	   monthtxt='Mar';	
		
	}
	else if(month==4)
	{
		
		monthtxt='Apr';
	}
	else if(month==5)
	{
		monthtxt='May';
		
	}
	else if(month==6)
	{
		monthtxt='Jun';
		
	}
	
	else if(month==7)
	{
		monthtxt='Jul';
		
	}
	else if(month==8)
	{
		monthtxt='Aug';
		
	}
	else if(month==9)
	{
		monthtxt='Sep';
		
	}
	else if(month==10)
	{
		monthtxt='Oct';
		
	}
	else if(month==11)
	{
		monthtxt='Nov';
		
	}
	else if(month==12)
	{
		monthtxt='Dec';
		
	}
		
	return monthtxt;
	
}//Month 



function validateValue(value)
{
	var value_res;
	
	if(value==null || value ==undefined || value=='' || value ==NaN || value == 'NaN')
	{
		value_res='';
		return value_res;
	}
	else
	{
		return value;
	}
	
}
