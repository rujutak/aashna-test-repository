/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_MediaPlan_ShowItemValidation.js
	
	Date:
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================



var flag;

// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChangednewalias(type, name, linenum)
{
	
	var ml_alias;
	var media_type_tv;
	var blank='';
	var media_ID=nlapiGetRecordId();
	//alert('media_ID'+media_ID);
	
	var alias_day;
	
if(type=='recmachcustrecord_mp3_mp1ref')
{

  if(name=='custrecord_mp3_1')
  {
  	alias_day='custrecord_mp3_1';
	
	
	 	alias_check_validate(alias_day,media_ID);
		
		
		var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
		
	//	alert('alias_value'+alias_value)
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   	    flag=0;
	     	call_function_1(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_2')
  {
  	alias_day='custrecord_mp3_2';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
    // alert('alias_value'+alias_value)
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   	   flag=0;
	     	call_function_2(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_3')
  {
  	alias_day='custrecord_mp3_3';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);	
	//alert('alias_value'+alias_value)
	 
	if (alias_value != null && alias_value != '' && alias_value != undefined) 
   {
   	    flag=0;
     	call_function_3(alias_day);
   }
	
  } 
  else if(name=='custrecord_mp3_4')
  {
  	 alias_day='custrecord_mp3_4';
	
	alias_check_validate(alias_day,media_ID);
	
   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);	
   if (alias_value != null && alias_value != '' && alias_value != undefined) 
   {
   	    flag=0;
     	call_function_4(alias_day);
   }
	
  } 
 else if(name=='custrecord_mp3_5')
  {
  	 alias_day='custrecord_mp3_5';
	 
	alias_check_validate(alias_day,media_ID);
	
	
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	    	flag=0;
	     	call_function_5(alias_day);
	   }
	
	
  } 
  else if(name=='custrecord_mp3_6')
  {
  	alias_day='custrecord_mp3_6';
	
	 alias_check_validate(alias_day,media_ID);
	 
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   	flag=0;
	     	call_function_6(alias_day);
	   }
	
  } 
  else if(name=='custrecord_mp3_7')
  {
  	alias_day='custrecord_mp3_7';
	
	alias_check_validate(alias_day,media_ID);
	
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	     	flag=0;
	     	call_function_7(alias_day);
	   }
	
  }
 else if(name=='custrecord_mp3_8')
  {
  	alias_day='custrecord_mp3_8';
	
	alias_check_validate(alias_day,media_ID);
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_8(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_9')
  {
  	alias_day='custrecord_mp3_9';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_9(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_10')
  {
  	alias_day='custrecord_mp3_10';
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	    	flag=0;
	     	call_function_10(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_11')
  {
  	alias_day='custrecord_mp3_11';
	
	alias_check_validate(alias_day,media_ID);
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	    	flag=0;
	     	call_function_11(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_12')
  {
  	alias_day='custrecord_mp3_12';
	
	 alias_check_validate(alias_day,media_ID);
	 
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   	    flag=0;
	     	call_function_12(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_13')
  {
  	alias_day='custrecord_mp3_13';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_13(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_14')
  {
  	 alias_day='custrecord_mp3_14';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_14(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_15')
  {
  	 alias_day='custrecord_mp3_15';
	 alias_check_validate(alias_day,media_ID);
	 
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_15(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_16')
  {
  	alias_day='custrecord_mp3_16';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_16(alias_day);
	   }
	 
  }
  else if(name=='custrecord_mp3_17')
  {
  	alias_day='custrecord_mp3_17';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_17(alias_day);
	   }
	 
  }
  else if(name=='custrecord_mp3_18')
  {
  	 alias_day='custrecord_mp3_18';
	 
	 alias_check_validate(alias_day,media_ID);
	 
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_18(alias_day);
	   }
	
  }
  else if(name=='custrecord_mp3_19')
  {
  	alias_day='custrecord_mp3_19';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_19(alias_day);
	   }
	 
  }
  else if(name=='custrecord_mp3_20')
  {
  	alias_day='custrecord_mp3_20';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_20(alias_day);
	   }
	 
  }
  else if(name=='custrecord_mp3_21')
  {
  	alias_day='custrecord_mp3_21';
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_21(alias_day);
	   }
	 
  }
  
  else if(name=='custrecord_mp3_22')
  {
  	alias_day='custrecord_mp3_22';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_22(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_23')
  {
  	alias_day='custrecord_mp3_23';
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_23(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_24')
  {
  	alias_day='custrecord_mp3_24';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_24(alias_day);
	   }
	
  }
   else if(name=='custrecord_mp3_25')
  {
  	alias_day='custrecord_mp3_25';
	
	 alias_check_validate(alias_day,media_ID);
	 
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_25(alias_day);
	   }
	
  }
   else if(name=='custrecord_mp3_26')
  {
  	 alias_day='custrecord_mp3_26';
	 
	 alias_check_validate(alias_day,media_ID);
	 
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_26(alias_day);
	   }
	
  }
   else if(name=='custrecord_mp3_27')
  {
  	alias_day='custrecord_mp3_27';
	
	alias_check_validate(alias_day,media_ID);
	
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_27(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_28')
  {
  	 alias_day='custrecord_mp3_28';
	 
	 alias_check_validate(alias_day,media_ID);
	 
	 var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_28(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_29')
  {
  	 alias_day='custrecord_mp3_29';
	 
	 alias_check_validate(alias_day,media_ID);
	 
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_29(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_30')
  {
  	alias_day='custrecord_mp3_30';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_30(alias_day);
	   }
	 
  }
   else if(name=='custrecord_mp3_31')
  {
  	alias_day='custrecord_mp3_31';
	
	alias_check_validate(alias_day,media_ID);
	
	var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
	   if (alias_value != null && alias_value != '' && alias_value != undefined) 
	   {
	   		flag=0;
	     	call_function_31(alias_day);
	   }
	
  }
  		
		
		
		
		
		
		
		 
					
		
		
	}//Type Check
	
	
		
	
   
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================



function alias_check_validate(alias_day,media_ID)
{
	
	
	  var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
			
	
	if(alias_value!=null && alias_value!='')
	{
		var blank='';
		var ms_currentItemID=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram');
		
		if(ms_currentItemID!=null && ms_currentItemID!='')
		{
				var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_currentItemID);
					    
				if(tv_programOBJ!=null)
				{
					media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
				//	alert('media_type_tv =='+media_type_tv)	
				}
		}
				
		if(media_ID!=null && media_ID!='' && media_ID!=undefined)
		{
			var mediaOBJ=nlapiLoadRecord('customrecord_mediaplan_1',media_ID);
					
			 if(mediaOBJ!=null)
			 {
			 	 var line_count_mlst=mediaOBJ.getLineItemCount('recmachcustrecord_mp2_mp1ref');
							 
				 if(line_count_mlst!=null)
				 {
				 	for(var i=1;i<=line_count_mlst;i++)
					{
						var ml_name=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_spotname',i);
														
						var ml_type=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type',i);
					//	alert('ml_type =='+ml_type)				
									
						ml_alias=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',i);
					//	alert('ml_alias =='+ml_alias)						
						
						var splitShowItem=new Array();
											
							var show=alias_value.split(" ")
							show=show[0];
							
							if(ml_type==media_type_tv)
							{
								if(show.indexOf(ml_alias)!=-1)
								{
								  break;	
									
								}
								
								
							}
																				
							else if(ml_type!=media_type_tv)
							{
								alert('Please enter item with correct Media Type');
								nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank);
								break;
							}
										
					}
					
					
				 }
			 					
			 }	
					
		}
		
		
	}//Alias 
	
	
	
	
		
		
		
	
	
	
}


























function call_function_1(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		//========================= 1 ================================================
		
		var totalquota_1=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_1');
	//	alert('totalquota_one'+totalquota_1);
		
		var TvProgramDayDuration1=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_1');
		//alert('TvProgramDayDuration1'+TvProgramDayDuration1);
		
				
				
		var result_1=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_1,TvProgramDayDuration1);
	//	alert('result_1'+result_1)
		
		 if(result_1>totalquota_1)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,false,true);
			flag=1;
			
			//alert('done')
		 }
		
	
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function






function call_function_2(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
	
	//========================= 2 ================================================
		
		var totalquota_2=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_2');
	//	alert('totalquota_2'+totalquota_2);
		
		var TvProgramDayDuration2=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_2');
		////alert('TvProgramDayDuration2'+TvProgramDayDuration2);
		
		if(flag==0)
		{
			// alert('In flagggg..........')
			
			var result_2=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_2,TvProgramDayDuration2);
		 //    alert('result_2'+result_2)
		
		 if(result_2>totalquota_2)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			flag=1;
		 }
			
		}		
				
		
		

		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function





function call_function_3(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
	 //  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','alias_value='+alias_value)
  // alert('alias_value='+alias_value)
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//alert('MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
 // alert('ms_TVProgramRefNo='+ms_TVProgramRefNo)	
		
		
	  if(ms_TVProgramRefNo!=null && ms_TVProgramRefNo!='' && ms_TVProgramRefNo!=undefined)
	  {
	  			
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','tvquota_OBJ='+tvquota_OBJ)	
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 3 ================================================
		
		var totalquota_3=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_3');
	//	alert('totalquota_3='+totalquota_3)
		
		var TvProgramDayDuration3=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_3');
	//alert('TvProgramDayDuration3='+TvProgramDayDuration3)
			
	 if(flag==0)
	  {
	  	var result_3=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_3,TvProgramDayDuration3);
		// alert('result_3='+result_3)
		
		 if(result_3>totalquota_3)
		 {
		 	 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Not sufficient...........')
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			flag=1;
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
	  }  
		

		
			
   	
   } 
   		
	
	
	
}//call function




function call_function_4(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 4 ================================================
		
		var totalquota_4=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_4');
		////alert('totalquota_4'+totalquota_4);
		
		var TvProgramDayDuration4=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_4');
		////alert('TvProgramDayDuration4'+TvProgramDayDuration4);
			
	 if(flag==0)
	  {
	  	var result_4=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_4,TvProgramDayDuration4);
		//alert('result_4'+result_4)
		
		 if(result_4>totalquota_4)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function



function call_function_5(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 5 ================================================
		
		var totalquota_5=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_5');
		////alert('totalquota_5'+totalquota_5);
		
		var TvProgramDayDuration5=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_5');
		////alert('TvProgramDayDuration5'+TvProgramDayDuration5);
			
	 if(flag==0)
	  {
	  	var result_5=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_5,TvProgramDayDuration5);
		//alert('result_5'+result_5)
		
		 if(result_5>totalquota_5)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function

function call_function_6(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 6 ================================================
		
		var totalquota_6=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_6');
		////alert('totalquota_6'+totalquota_6);
		
		var TvProgramDayDuration6=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_6');
		////alert('TvProgramDayDuration6'+TvProgramDayDuration6);
			
	 if(flag==0)
	  {
	  	var result_6=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_6,TvProgramDayDuration6);
		//alert('result_6'+result_6)
		
		 if(result_6>totalquota_6)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function




function call_function_7(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 7 ================================================
		
		var totalquota_7=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_7');
		////alert('totalquota_7'+totalquota_7);
		
		var TvProgramDayDuration7=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_7');
		////alert('TvProgramDayDuration7'+TvProgramDayDuration7);
			
	 if(flag==0)
	  {
	  	var result_7=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_7,TvProgramDayDuration7);
		//alert('result_7'+result_7)
		
		 if(result_7>totalquota_7)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function




function call_function_8(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 8 ================================================
		
		var totalquota_8=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_8');
		////alert('totalquota_8'+totalquota_8);
		
		var TvProgramDayDuration8=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_8');
		////alert('TvProgramDayDuration8'+TvProgramDayDuration8);
			
	 if(flag==0)
	  {
	  	var result_8=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_8,TvProgramDayDuration8);
		//alert('result_8'+result_8)
		
		 if(result_8>totalquota_8)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function










function call_function_9(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 9 ================================================
		
		var totalquota_9=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_9');
		////alert('totalquota_9'+totalquota_9);
		
		var TvProgramDayDuration9=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_9');
		////alert('TvProgramDayDuration9'+TvProgramDayDuration9);
			
	 if(flag==0)
	  {
	  	var result_9=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_9,TvProgramDayDuration9);
		//alert('result_9'+result_9)
		
		 if(result_9>totalquota_9)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function














function call_function_10(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 10 ================================================
		
		var totalquota_10=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_10');
		////alert('totalquota_10'+totalquota_10);
		
		var TvProgramDayDuration10=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_10');
		////alert('TvProgramDayDuration10'+TvProgramDayDuration10);
			
	 if(flag==0)
	  {
	  	var result_10=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_10,TvProgramDayDuration10);
		//alert('result_10'+result_10)
		
		 if(result_10>totalquota_10)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function







function call_function_11(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 11 ================================================
		
		var totalquota_11=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_11');
		////alert('totalquota_11'+totalquota_11);
		
		var TvProgramDayDuration11=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_11');
		////alert('TvProgramDayDuration11'+TvProgramDayDuration11);
			
	 if(flag==0)
	  {
	  	var result_11=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_11,TvProgramDayDuration11);
		//alert('result_11'+result_11)
		
		 if(result_11>totalquota_11)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function









function call_function_12(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 12 ================================================
		
		var totalquota_12=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_12');
		////alert('totalquota_12'+totalquota_12);
		
		var TvProgramDayDuration12=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_12');
		////alert('TvProgramDayDuration12'+TvProgramDayDuration12);
			
	 if(flag==0)
	  {
	  	var result_12=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_12,TvProgramDayDuration12);
		//alert('result_12'+result_12)
		
		 if(result_12>totalquota_12)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function













function call_function_13(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 13 ================================================
		
		var totalquota_13=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_13');
		////alert('totalquota_13'+totalquota_13);
		
		var TvProgramDayDuration13=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_13');
		////alert('TvProgramDayDuration13'+TvProgramDayDuration13);
			
	 if(flag==0)
	  {
	  	var result_13=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_13,TvProgramDayDuration13);
		//alert('result_13'+result_13)
		
		 if(result_13>totalquota_13)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function











function call_function_14(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 14 ================================================
		
		var totalquota_14=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_14');
		////alert('totalquota_14'+totalquota_14);
		
		var TvProgramDayDuration14=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_14');
		////alert('TvProgramDayDuration14'+TvProgramDayDuration14);
			
	 if(flag==0)
	  {
	  	var result_14=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_14,TvProgramDayDuration14);
		//alert('result_14'+result_14)
		
		 if(result_14>totalquota_14)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function








function call_function_15(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 15 ================================================
		
		var totalquota_15=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_15');
		////alert('totalquota_15'+totalquota_15);
		
		var TvProgramDayDuration15=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_15');
		////alert('TvProgramDayDuration15'+TvProgramDayDuration15);
			
	 if(flag==0)
	  {
	  	var result_15=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_15,TvProgramDayDuration15);
		//alert('result_15'+result_15)
		
		 if(result_15>totalquota_15)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function










function call_function_16(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 16 ================================================
		
		var totalquota_16=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_16');
		////alert('totalquota_16'+totalquota_16);
		
		var TvProgramDayDuration16=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_16');
		////alert('TvProgramDayDuration16'+TvProgramDayDuration16);
			
	 if(flag==0)
	  {
	  	var result_16=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_16,TvProgramDayDuration16);
		//alert('result_16'+result_16)
		
		 if(result_16>totalquota_16)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function





function call_function_17(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 17 ================================================
		
		var totalquota_17=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_17');
		////alert('totalquota_17'+totalquota_17);
		
		var TvProgramDayDuration17=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_17');
		////alert('TvProgramDayDuration17'+TvProgramDayDuration17);
			
	 if(flag==0)
	  {
	  	var result_17=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_17,TvProgramDayDuration17);
		//alert('result_17'+result_17)
		
		 if(result_17>totalquota_17)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function





function call_function_18(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 18 ================================================
		
		var totalquota_18=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_18');
		////alert('totalquota_18'+totalquota_18);
		
		var TvProgramDayDuration18=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_18');
		////alert('TvProgramDayDuration18'+TvProgramDayDuration18);
			
	 if(flag==0)
	  {
	  	var result_18=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_18,TvProgramDayDuration18);
		//alert('result_18'+result_18)
		
		 if(result_18>totalquota_18)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function





function call_function_19(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 19 ================================================
		
		var totalquota_19=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_19');
		////alert('totalquota_19'+totalquota_19);
		
		var TvProgramDayDuration19=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_19');
		////alert('TvProgramDayDuration19'+TvProgramDayDuration19);
			
	 if(flag==0)
	  {
	  	var result_19=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_19,TvProgramDayDuration19);
		//alert('result_19'+result_19)
		
		 if(result_19>totalquota_19)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function





function call_function_20(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 20 ================================================
		
		var totalquota_20=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_20');
		////alert('totalquota_20'+totalquota_20);
		
		var TvProgramDayDuration20=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_20');
		////alert('TvProgramDayDuration20'+TvProgramDayDuration20);
			
	 if(flag==0)
	  {
	  	var result_20=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_20,TvProgramDayDuration20);
		//alert('result_20'+result_20)
		
		 if(result_20>totalquota_20)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function






function call_function_21(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 21 ================================================
		
		var totalquota_21=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_21');
		////alert('totalquota_21'+totalquota_21);
		
		var TvProgramDayDuration21=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_21');
		////alert('TvProgramDayDuration21'+TvProgramDayDuration21);
			
	 if(flag==0)
	  {
	  	var result_21=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_21,TvProgramDayDuration21);
		//alert('result_21'+result_21)
		
		 if(result_21>totalquota_21)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function






function call_function_22(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 22 ================================================
		
		var totalquota_22=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_22');
		////alert('totalquota_22'+totalquota_22);
		
		var TvProgramDayDuration22=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_22');
		////alert('TvProgramDayDuration22'+TvProgramDayDuration22);
			
	 if(flag==0)
	  {
	  	var result_22=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_22,TvProgramDayDuration22);
		//alert('result_22'+result_22)
		
		 if(result_22>totalquota_22)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function







function call_function_23(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 23 ================================================
		
		var totalquota_23=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_23');
		////alert('totalquota_23'+totalquota_23);
		
		var TvProgramDayDuration23=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_23');
		////alert('TvProgramDayDuration23'+TvProgramDayDuration23);
			
	 if(flag==0)
	  {
	  	var result_23=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_23,TvProgramDayDuration23);
		//alert('result_23'+result_23)
		
		 if(result_23>totalquota_23)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function









function call_function_24(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 24 ================================================
		
		var totalquota_24=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_24');
		////alert('totalquota_24'+totalquota_24);
		
		var TvProgramDayDuration24=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_24');
		////alert('TvProgramDayDuration24'+TvProgramDayDuration24);
			
	 if(flag==0)
	  {
	  	var result_24=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_24,TvProgramDayDuration24);
		//alert('result_24'+result_24)
		
		 if(result_24>totalquota_24)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function








function call_function_25(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 25 ================================================
		
		var totalquota_25=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_25');
		////alert('totalquota_25'+totalquota_25);
		
		var TvProgramDayDuration25=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_25');
		////alert('TvProgramDayDuration25'+TvProgramDayDuration25);
			
	 if(flag==0)
	  {
	  	var result_25=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_25,TvProgramDayDuration25);
		//alert('result_25'+result_25)
		
		 if(result_25>totalquota_25)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function







function call_function_26(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 26 ================================================
		
		var totalquota_26=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_26');
		////alert('totalquota_26'+totalquota_26);
		
		var TvProgramDayDuration26=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_26');
		////alert('TvProgramDayDuration26'+TvProgramDayDuration26);
			
	 if(flag==0)
	  {
	  	var result_26=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_26,TvProgramDayDuration26);
		//alert('result_26'+result_26)
		
		 if(result_26>totalquota_26)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function








function call_function_27(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 27 ================================================
		
		var totalquota_27=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_27');
		////alert('totalquota_27'+totalquota_27);
		
		var TvProgramDayDuration27=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_27');
		////alert('TvProgramDayDuration27'+TvProgramDayDuration27);
			
	 if(flag==0)
	  {
	  	var result_27=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_27,TvProgramDayDuration27);
		//alert('result_27'+result_27)
		
		 if(result_27>totalquota_27)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function







function call_function_28(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 28 ================================================
		
		var totalquota_28=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_28');
		////alert('totalquota_28'+totalquota_28);
		
		var TvProgramDayDuration28=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_28');
		////alert('TvProgramDayDuration28'+TvProgramDayDuration28);
			
	 if(flag==0)
	  {
	  	var result_28=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_28,TvProgramDayDuration28);
		//alert('result_28'+result_28)
		
		 if(result_28>totalquota_28)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function















function call_function_29(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 29 ================================================
		
		var totalquota_29=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_29');
		////alert('totalquota_29'+totalquota_29);
		
		var TvProgramDayDuration29=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_29');
		////alert('TvProgramDayDuration29'+TvProgramDayDuration29);
			
	 if(flag==0)
	  {
	  	var result_29=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_29,TvProgramDayDuration29);
		//alert('result_29'+result_29)
		
		 if(result_29>totalquota_29)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function
















function call_function_30(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 30 ================================================
		
		var totalquota_30=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_30');
		////alert('totalquota_30'+totalquota_30);
		
		var TvProgramDayDuration30=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_30');
		////alert('TvProgramDayDuration30'+TvProgramDayDuration30);
			
	 if(flag==0)
	  {
	  	var result_30=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_30,TvProgramDayDuration30);
		//alert('result_30'+result_30)
		
		 if(result_30>totalquota_30)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function

















function call_function_31(alias_day)
{
	var blank='';
	
	   var alias_value=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day);
  
   if(alias_value!=null && alias_value!='' &&alias_value!=undefined)
   {
   	 var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
	var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
	var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
	var TV_Program_Status = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')	
		
	var I_MP_InternalId=nlapiGetRecordId();	
		
	var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	
	
	var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
	
   var ms_TVProgramRefNo=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref');		
   //alert('ms_TVProgramRefNo'+ms_TVProgramRefNo)		
		
		
	    
		
		
 	var tvquota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',ms_TVProgramRefNo)    
	
	//alert('tvquota_OBJ'+tvquota_OBJ)
	
	if(tvquota_OBJ!=null)
	{
		
		
		
		//========================= 31 ================================================
		
		var totalquota_31=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_31');
		////alert('totalquota_31'+totalquota_31);
		
		var TvProgramDayDuration31=tvquota_OBJ.getFieldValue('custrecord_adinv_tq_31');
		////alert('TvProgramDayDuration31'+TvProgramDayDuration31);
			
	 if(flag==0)
	  {
	  	var result_31=alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_31,TvProgramDayDuration31);
		//alert('result_31'+result_31)
		
		 if(result_31>totalquota_31)
		 {
		 	
			alert(' Not sufficient...........');
			nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',alias_day,blank,true);
			
		 }
		
	  	
	  }
				
		
		
		
		
		
	
		
		
		
	}
		
			
   	
   } 
   		
	
	
	
}//call function



function alias_TV_quantity(alias_value,I_MP_InternalId,ms_TVProgramRefNo,alias_day,alias_value,totalquota_1,TvProgramDayDuration1)
{
	
	var total_temp=0;
	var total_commit=0;
	var total_duration=0;

	
	var MP3_Filters = new Array();
	var MP3_Coloumns = new Array();
	  
	  
		  
		  MP3_Filters[0]= new nlobjSearchFilter('custrecord_mp3_tvprgqm_ref',null,'is',ms_TVProgramRefNo)
		 
		  
		  MP3_Coloumns[0] =  new nlobjSearchColumn('internalid')
		  MP3_Coloumns[1] =  new nlobjSearchColumn('custrecord_mp3_1')
		  MP3_Coloumns[2] =  new nlobjSearchColumn('custrecord_mp3_mp1ref')
		  MP3_Coloumns[3] =  new nlobjSearchColumn(alias_day)
		  
		  var searchresult = nlapiSearchRecord('customrecord_mediaplan_3',null,MP3_Filters,MP3_Coloumns)
	
	      if (searchresult != null && searchresult != undefined && searchresult != '') 
		  {
		  	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','searchresult='+searchresult.length)
			for (var i = 0; i < searchresult.length; i++) 
			{
				var mediascheduleId = searchresult[i].getValue('internalid')
			  		nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','mediascheduleId='+mediascheduleId)
			  		
			  		var mediaplanId = searchresult[i].getValue('custrecord_mp3_mp1ref')
			  	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','mediaplanId' + mediaplanId)
			  		
			  		var alias_value_day = searchresult[i].getValue(alias_day)
			  	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','mediaplanId' + mediaplanId)
			  		
					
					
					
					
				if(mediaplanId!=null && mediaplanId!='' && mediaplanId!=undefined)
				{
								
					
					
					
		  var mp_OBJ=nlapiLoadRecord('customrecord_mediaplan_1',mediaplanId);
	     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','mp_OBJ'+mp_OBJ);
		 
		 
		 if(mp_OBJ!=null && mp_OBJ!=undefined && mp_OBJ!='')
		 {
		 			
	if(I_MP_InternalId==mediaplanId)
	{
		alias_value_day=alias_value;
	}		
			
			
	//var Dayvalue = mp_OBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', MediaScheduleDayId, j);
	
	var MediaListLineCount = mp_OBJ.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	var blank='';	
			
	  if (alias_value_day != null && alias_value_day != undefined && alias_value_day != '') 
	  {
	  	var AliasQuantity = alias_value_day.split(',')
		  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','AliasQuantity length='+AliasQuantity.length)
					
				 var temp ;	
					var TotalMediaScheduleDuration = 0;
		for (var p = 0; p < AliasQuantity.length; p++) 
		{
			var TotalDeductDuration = 0;
			////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============Inside Alias Quantity  for loop=============')	
			// Begin Code :For Dividing the quantity & alias from the day field of the Media schedule.
			// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','AliasQuantity='+AliasQuantity[p])
			
			var a = AliasQuantity[p]
			
			var letter = "";
			var number = "";
			
			
			for (var q = 0; q < a.length; q++) 
			{
				var b = a.charAt(q);
				if (isNaN(b) == true) 
				{
					letter = letter + b;
				}
				else 
				{
					number = number + b;
				}
			}
			////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
			////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
			
			var I_MediaCategoryquantity = number;
			var S_MediaCategoryAlias = letter;
			
			nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_MediaCategoryquantity'+I_MediaCategoryquantity)
				nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MediaCategoryAlias'+S_MediaCategoryAlias)
			
			 var I_mediaListDuration = 0;
			  var S_MediaListUnit = 0 ;
						  
		            for (var L=1; L<=MediaListLineCount; L++) 	
							
							{
								var I_MediaListAlias =  mp_OBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',L)  
								 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_MediaListAlias='+I_MediaListAlias)
								   
									   if(S_MediaCategoryAlias == I_MediaListAlias )
									   {
									    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============Inside S_MediaCategoryAlias == I_MediaListAlias=============')
									   	I_mediaListDuration =  mp_OBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration',L)  
									   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_mediaListDuration='+I_mediaListDuration)
										
										S_MediaListUnit =  mp_OBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type',L)  
									    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','**********S_MediaListUnit='+S_MediaListUnit)
										// Spot   --- 1
										// VTR    ----2
										// TIE IN   --3
										// PR/SCOOP --4 
										
										if(S_MediaListUnit == 3 || S_MediaListUnit == 4)
										{
										I_mediaListDuration=1;	
										}
										// Begin Code : This code for converting duration from minute to second.
						   
										TotalDeductDuration = parseFloat(I_mediaListDuration) * parseFloat(I_MediaCategoryquantity)
						               nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalDeductDuration='+TotalDeductDuration)
							            		
									    break ;
									   }
													
								
							}
							
							TotalMediaScheduleDuration = parseFloat(TotalMediaScheduleDuration) + parseFloat(TotalDeductDuration)
						   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalMediaScheduleDuration'+TotalMediaScheduleDuration)
					}//Alias
					
					 var SetDuration = 0;
                  if(S_MediaListUnit == 1)
				  {
				  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TvProgramDayDuration1 == 1********============='+TvProgramDayDuration1)
				    if(TvProgramDayDuration1 != null && TvProgramDayDuration1 != undefined && TvProgramDayDuration1 != '')
					{
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******S_MediaListUnit == 1********=============')
						//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******AdInvetoryManagemntId ********============='+AdInvetoryManagemntId)
						
						TvProgramDayDuration = parseFloat(TvProgramDayDuration1) * parseFloat(60)
						SetDuration = TvProgramDayDuration - TotalMediaScheduleDuration
					    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','SetDuration='+SetDuration)
						
						  var I_TVprogram_OnhandDuration =  parseFloat(SetDuration) / parseFloat(60)
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TVprogram_OnhandDuration='+I_TVprogram_OnhandDuration)
						  
						  var I_TvProgram_CommitDuration =  parseFloat(TotalMediaScheduleDuration) / parseFloat(60)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TvProgram_CommitDuration='+I_TvProgram_CommitDuration)
						 		 
						   
						  temp = parseFloat(TvProgramDayDuration1)- parseFloat(I_TvProgram_CommitDuration)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','temp======'+temp)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TvProgram_CommitDuration='+I_TvProgram_CommitDuration)
						  
						  
						  
						  if(temp==null || temp.toString()=='NaN' || temp=='' || temp==undefined)
						  {
						  	temp=0;
							
						  }
						  
						  if(I_TvProgram_CommitDuration==null || I_TvProgram_CommitDuration.toString()=='NaN' || I_TvProgram_CommitDuration=='' || I_TvProgram_CommitDuration==undefined)
						  {
						  	I_TvProgram_CommitDuration=0;
							
						  }
						  
						  
						  
						  total_temp=parseFloat(total_temp)+parseFloat(temp);
						  total_commit=parseFloat(total_commit)+parseFloat(I_TvProgram_CommitDuration);
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***********total_temp in ***********='+total_temp)
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***********total_commit in ***********='+total_commit)
					}
				  
				  }	
				  
				  
				  if(S_MediaListUnit == 3 || S_MediaListUnit==4 )
				  {
				  	if(TvProgramDayDuration1 != null && TvProgramDayDuration1 != undefined && TvProgramDayDuration1 != '')
					{
					   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******S_MediaListUnit == 3 || S_MediaListUnit==4********=============')
					//   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******AdInvetoryManagemntId ********============='+AdInvetoryManagemntId) 
						TvProgramDayDuration = parseFloat(TvProgramDayDuration1) 
						SetDuration = TvProgramDayDuration - TotalMediaScheduleDuration
						nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'SetDuration=' + SetDuration)
						
						
						var I_TVprogram_OnhandDuration = parseFloat(SetDuration) 
						nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_TVprogram_OnhandDuration=' + I_TVprogram_OnhandDuration)
						
						var I_TvProgram_CommitDuration = parseFloat(TotalMediaScheduleDuration)
						nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_TvProgram_CommitDuration=' + I_TvProgram_CommitDuration)
						
						  temp = parseFloat(TvProgramDayDuration1)- parseFloat(I_TvProgram_CommitDuration)
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','temp======'+temp)
						
						
						   if(temp==null || temp.toString()=='NaN' || temp=='' || temp==undefined)
						  {
						  	temp=0;
							
						  }
						  
						  if(I_TvProgram_CommitDuration==null || I_TvProgram_CommitDuration.toString()=='NaN' || I_TvProgram_CommitDuration=='' || I_TvProgram_CommitDuration==undefined)
						  {
						  	I_TvProgram_CommitDuration=0;
							
						  }
						
						
						nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'CommitDuration======' + CommitDuration)
						nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_TvProgram_CommitDuration======' + I_TvProgram_CommitDuration)
						  total_temp=parseFloat(total_temp)+parseFloat(temp);
						  total_commit=parseFloat(total_commit)+parseFloat(I_TvProgram_CommitDuration);
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***********total_temp in ***********='+total_temp)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***********total_commit in ***********='+total_commit)
						
		                //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'temp======' + temp)
					//	O_TVProgramItem.setFieldValue(TVProgramCommitDurationId, temp)//TVProgramCommitDurationId
					
				
							 
							
						   
					}
				  }
				
		
		 
		// total_temp	=  parseFloat(total_temp)-parseFloat(temp);
		 
		////alert('total_temp in funcccccccccc'+total_temp)

   
		
		
		
		
	  }		//alias day
			
		 }
		 
	
			
					
					
				}	//Media Plan....................
					
					
					
					
					
		
				
				
				
			}//for
			
			
		  }//search
	
	
	
	
	total_temp=parseFloat(60)-parseFloat(total_commit);
	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','total_temp ***********======'+total_temp)
	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','total_commit ***********======'+total_commit)
	return total_commit;
	
	
}//alais function



