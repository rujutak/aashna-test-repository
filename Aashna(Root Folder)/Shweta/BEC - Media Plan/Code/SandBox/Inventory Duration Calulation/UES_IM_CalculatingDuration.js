// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:USE_IM_Duration
	Author:Rakesh Malvade	
	Company:Proquest solutions
	Date:14-02-2013
	Description:When User click on Save And Edit button Total Duration And Commited Duration will be calculated.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

    14-02-2013            Rakesh Malvade					  Supriya						User click on Save And Edit button Total Duration And Commited Duration will be calculated	

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	
	
	
		return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	
	
	
	if (type == 'edit' || type == 'create') 
	{
		
		var ImId=nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'afterSubmit', 'IF ID = ' + ImId);
		
		var imObject = nlapiLoadRecord('customrecord_ad_inventory',ImId);
		
		
		//Strat on total on Hand calculation
		var Onhanddate1=imObject.getFieldValue('custrecord_adinv_oh_1'); 
		nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(Onhanddate1== null || Onhanddate1=='' || Onhanddate1==undefined)
		{
			Onhanddate1=0;
			
		}
		var Onhanddate2=imObject.getFieldValue('custrecord_adinv_oh_2'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty2 = ' + Onhanddate2);
		if(Onhanddate2== null || Onhanddate2==''|| Onhanddate2==undefined)
		{
			Onhanddate2=0;
		
		}
		var Onhanddate3=imObject.getFieldValue('custrecord_adinv_oh_3');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate3== null || Onhanddate3=='' || Onhanddate3==undefined)
		{
			Onhanddate3=0;
			
		}
		var Onhanddate4=imObject.getFieldValue('custrecord_adinv_oh_4');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate4== null || Onhanddate4=='' || Onhanddate4==undefined)
		{
			Onhanddate4=0;
			
		}
		var Onhanddate5=imObject.getFieldValue('custrecord_adinv_oh_5');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate5== null || Onhanddate5=='' || Onhanddate5==undefined)
		{
			Onhanddate5=0;
			
		}
		var Onhanddate6=imObject.getFieldValue('custrecord_adinv_oh_6');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate6== null || Onhanddate6=='' || Onhanddate6==undefined)
		{
			Onhanddate6=0;
			
		}
		var Onhanddate7=imObject.getFieldValue('custrecord_adinv_oh_7');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate7== null || Onhanddate7=='' || Onhanddate7==undefined)
		{
			Onhanddate7=0;
			
		}
		var Onhanddate8=imObject.getFieldValue('custrecord_adinv_oh_8');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate8== null || Onhanddate8=='' || Onhanddate8==undefined)
		{
			Onhanddate8=0;
			
		}
		var Onhanddate9=imObject.getFieldValue('custrecord_adinv_oh_9');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate9== null || Onhanddate9=='' || Onhanddate9==undefined)
		{
			Onhanddate9=0;
			
		}
		var Onhanddate10=imObject.getFieldValue('custrecord_adinv_oh_10');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate10== null || Onhanddate10=='' || Onhanddate10==undefined)
		{
			Onhanddate10=0;
			
		}
		var Onhanddate11=imObject.getFieldValue('custrecord_adinv_oh_11');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate11== null || Onhanddate11=='' || Onhanddate11==undefined)
		{
			Onhanddate11=0;
			
		}
		var Onhanddate12=imObject.getFieldValue('custrecord_adinv_oh_12');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate12== null || Onhanddate12=='' || Onhanddate12==undefined)
		{
			Onhanddate12=0;
			
		}
		var Onhanddate13=imObject.getFieldValue('custrecord_adinv_oh_13');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate13== null || Onhanddate13=='' || Onhanddate13==undefined)
		{
			Onhanddate13=0;
			
		}
		var Onhanddate14=imObject.getFieldValue('custrecord_adinv_oh_14');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate14== null || Onhanddate14=='' || Onhanddate14==undefined)
		{
			Onhanddate14=0;
			
		}
		var Onhanddate15=imObject.getFieldValue('custrecord_adinv_oh_15');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate15== null || Onhanddate15=='' || Onhanddate15==undefined)
		{
			Onhanddate15=0;
			
		}
		var Onhanddate16=imObject.getFieldValue('custrecord_adinv_oh_16');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate16== null || Onhanddate16=='' || Onhanddate16==undefined)
		{
			Onhanddate16=0;
			
		}
		var Onhanddate17=imObject.getFieldValue('custrecord_adinv_oh_17');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate17== null || Onhanddate17=='' || Onhanddate17==undefined)
		{
			Onhanddate17=0;
			
		}
		var Onhanddate18=imObject.getFieldValue('custrecord_adinv_oh_18');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate18== null || Onhanddate18=='' || Onhanddate18==undefined)
		{
			Onhanddate18=0;
			
		}
		var Onhanddate19=imObject.getFieldValue('custrecord_adinv_oh_19');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate19== null || Onhanddate19=='' || Onhanddate19==undefined)
		{
			Onhanddate19=0;
			
		}
		var Onhanddate20=imObject.getFieldValue('custrecord_adinv_oh_20');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate20== null || Onhanddate20=='' || Onhanddate20==undefined)
		{
			Onhanddate20=0;
			
		}
		var Onhanddate21=imObject.getFieldValue('custrecord_adinv_oh_21');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate21== null || Onhanddate21=='' || Onhanddate21==undefined)
		{
			Onhanddate21=0;
			
		}
		var Onhanddate22=imObject.getFieldValue('custrecord_adinv_oh_22');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate22== null || Onhanddate22=='' || Onhanddate22==undefined)
		{
			Onhanddate22=0;
			
		}
		var Onhanddate23=imObject.getFieldValue('custrecord_adinv_oh_23');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate23== null || Onhanddate23=='' || Onhanddate23==undefined)
		{
			Onhanddate23=0;
			
		}
		var Onhanddate24=imObject.getFieldValue('custrecord_adinv_oh_24');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate24== null || Onhanddate24=='' || Onhanddate24==undefined)
		{
			Onhanddate24=0;
			
		}
		var Onhanddate25=imObject.getFieldValue('custrecord_adinv_oh_25');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate25== null || Onhanddate25=='' || Onhanddate25==undefined)
		{
			Onhanddate25=0;
			
		}
		var Onhanddate26=imObject.getFieldValue('custrecord_adinv_oh_26');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate26== null || Onhanddate26=='' || Onhanddate26==undefined)
		{
			Onhanddate26=0;
			
		}
		var Onhanddate27=imObject.getFieldValue('custrecord_adinv_oh_27');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate27== null || Onhanddate27=='' || Onhanddate27==undefined)
		{
			Onhanddate27=0;
			
		}
		var Onhanddate28=imObject.getFieldValue('custrecord_adinv_oh_28');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate28== null || Onhanddate28=='' || Onhanddate28==undefined)
		{
			Onhanddate28=0;
			
		}
		var Onhanddate29=imObject.getFieldValue('custrecord_adinv_oh_29');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate29== null || Onhanddate29=='' || Onhanddate29==undefined)
		{
			Onhanddate29=0;
			
		}
		var Onhanddate30=imObject.getFieldValue('custrecord_adinv_oh_30');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate30== null || Onhanddate30=='' || Onhanddate30==undefined)
		{
			Onhanddate30=0;
			
		}
		var Onhanddate31=imObject.getFieldValue('custrecord_adinv_oh_31');
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
		if(Onhanddate31== null || Onhanddate31=='' || Onhanddate31==undefined)
		{
			Onhanddate31=0;
			
		}
		//End Total On Hand Calulation
		
		
		
		//Start On Committed Duraion Value Calculation 
		
		var committeddate1=imObject.getFieldValue('custrecord_adinv_com_1'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate1== null || committeddate1=='' || committeddate1==undefined)
		{
			committeddate1=0;
			
		}
		var committeddate2=imObject.getFieldValue('custrecord_adinv_com_2'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate2== null || committeddate2=='' || committeddate2==undefined)
		{
			committeddate2=0;
			
		}
		var committeddate3=imObject.getFieldValue('custrecord_adinv_com_3'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate3== null || committeddate3=='' || committeddate3==undefined)
		{
			committeddate3=0;
			
		}
		var committeddate4=imObject.getFieldValue('custrecord_adinv_com_4'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate4== null || committeddate4=='' || committeddate4==undefined)
		{
			committeddate4=0;
			
		}
		var committeddate5=imObject.getFieldValue('custrecord_adinv_com_5'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate5== null || committeddate5=='' || committeddate5==undefined)
		{
			committeddate5=0;
			
		}
		var committeddate6=imObject.getFieldValue('custrecord_adinv_com_6'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate6== null || committeddate6=='' || committeddate6==undefined)
		{
			committeddate6=0;
			
		}
		var committeddate7=imObject.getFieldValue('custrecord_adinv_com_7'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate7== null || committeddate7=='' || committeddate7==undefined)
		{
			committeddate7=0;
			
		}
		var committeddate8=imObject.getFieldValue('custrecord_adinv_com_8'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate8== null || committeddate8=='' || committeddate8==undefined)
		{
			committeddate8=0;
			
		}
		var committeddate9=imObject.getFieldValue('custrecord_adinv_com_9'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate9== null || committeddate9=='' || committeddate9==undefined)
		{
			committeddate9=0;
			
		}
		var committeddate10=imObject.getFieldValue('custrecord_adinv_com_10'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate10== null || committeddate10=='' || committeddate10==undefined)
		{
			committeddate10=0;
			
		}
		var committeddate11=imObject.getFieldValue('custrecord_adinv_com_11'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate11== null || committeddate11=='' || committeddate11==undefined)
		{
			committeddate11=0;
			
		}
		var committeddate12=imObject.getFieldValue('custrecord_adinv_com_12'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate12== null || committeddate12=='' || committeddate12==undefined)
		{
			committeddate12=0;
			
		}
		var committeddate13=imObject.getFieldValue('custrecord_adinv_com_13'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate13== null || committeddate13=='' || committeddate13==undefined)
		{
			committeddate13=0;
			
		}
		var committeddate14=imObject.getFieldValue('custrecord_adinv_com_14'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate14== null || committeddate14=='' || committeddate14==undefined)
		{
			committeddate14=0;
			
		}
		var committeddate15=imObject.getFieldValue('custrecord_adinv_com_15'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate15== null || committeddate15=='' || committeddate15==undefined)
		{
			committeddate15=0;
			
		}
		var committeddate16=imObject.getFieldValue('custrecord_adinv_com_16'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate16== null || committeddate16=='' || committeddate16==undefined)
		{
			committeddate16=0;
			
		}
		var committeddate17=imObject.getFieldValue('custrecord_adinv_com_17'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate17== null || committeddate17=='' || committeddate17==undefined)
		{
			committeddate17=0;
			
		}
		var committeddate18=imObject.getFieldValue('custrecord_adinv_com_18'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate18== null || committeddate18=='' || committeddate18==undefined)
		{
			committeddate18=0;
			
		}
		var committeddate19=imObject.getFieldValue('custrecord_adinv_com_19'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate19== null || committeddate19=='' || committeddate19==undefined)
		{
			committeddate19=0;
			
		}
		var committeddate20=imObject.getFieldValue('custrecord_adinv_com_20'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate20== null || committeddate20=='' || committeddate20==undefined)
		{
			committeddate20=0;
			
		}
		var committeddate21=imObject.getFieldValue('custrecord_adinv_com_21'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate21== null || committeddate21=='' || committeddate21==undefined)
		{
			committeddate21=0;
			
		}
		var committeddate22=imObject.getFieldValue('custrecord_adinv_com_22'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate22== null || committeddate22=='' || committeddate22==undefined)
		{
			committeddate22=0;
			
		}
		var committeddate23=imObject.getFieldValue('custrecord_adinv_com_23'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate23== null || committeddate23=='' || committeddate23==undefined)
		{
			committeddate23=0;
			
		}
		var committeddate24=imObject.getFieldValue('custrecord_adinv_com_24'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate24== null || committeddate24=='' || committeddate24==undefined)
		{
			committeddate24=0;
			
		}
		var committeddate25=imObject.getFieldValue('custrecord_adinv_com_25'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate25== null || committeddate25=='' || committeddate25==undefined)
		{
			committeddate25=0;
			
		}
		var committeddate26=imObject.getFieldValue('custrecord_adinv_com_26'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate26== null || committeddate26=='' || committeddate26==undefined)
		{
			committeddate26=0;
			
		}
		var committeddate27=imObject.getFieldValue('custrecord_adinv_com_27'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate27== null || committeddate27=='' || committeddate27==undefined)
		{
			committeddate27=0;
			
		}
		var committeddate28=imObject.getFieldValue('custrecord_adinv_com_28'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate28== null || committeddate28=='' || committeddate28==undefined)
		{
			committeddate28=0;
			
		}
		var committeddate29=imObject.getFieldValue('custrecord_adinv_com_29'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate29== null || committeddate29=='' || committeddate29==undefined)
		{
			committeddate29=0;
			
		}
		var committeddate30=imObject.getFieldValue('custrecord_adinv_com_30'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate30== null || committeddate30=='' || committeddate30==undefined)
		{
			committeddate30=0;
			
		}
		var committeddate31=imObject.getFieldValue('custrecord_adinv_com_31'); 
		//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
		if(committeddate31== null || committeddate31=='' || committeddate31==undefined)
		{
			committeddate31=0;
			
		}
		
				
		//End Get Total Committed Duration.
		
		
		// Begin code to get the Total of Total Quota Duration
		var TotalQuota1=imObject.getFieldValue('custrecord_adinv_tq_1'); 
		
		if(TotalQuota1== null || TotalQuota1=='' || TotalQuota1==undefined)
		{
			TotalQuota1=0;
			
		}
		
		var TotalQuota2=imObject.getFieldValue('custrecord_adinv_tq_2'); 
		
		if(TotalQuota2== null || TotalQuota2=='' || TotalQuota2==undefined)
		{
			TotalQuota2=0;
			
		}
		
		var TotalQuota3=imObject.getFieldValue('custrecord_adinv_tq_3'); 
		
		if(TotalQuota3== null || TotalQuota3=='' || TotalQuota3==undefined)
		{
			TotalQuota3=0;
			
		}
		var TotalQuota4=imObject.getFieldValue('custrecord_adinv_tq_4'); 
		if(TotalQuota4== null || TotalQuota4=='' || TotalQuota4==undefined)
		{
			TotalQuota4=0;
			
		}
		var TotalQuota5=imObject.getFieldValue('custrecord_adinv_tq_5'); 
		if(TotalQuota5== null || TotalQuota5=='' || TotalQuota5==undefined)
		{
			TotalQuota5=0;
			
		}
		var TotalQuota6=imObject.getFieldValue('custrecord_adinv_tq_6'); 
		if(TotalQuota6== null || TotalQuota6=='' || TotalQuota6==undefined)
		{
			TotalQuota6=0;
			
		}
		var TotalQuota7=imObject.getFieldValue('custrecord_adinv_tq_7'); 
		if(TotalQuota7== null || TotalQuota7=='' || TotalQuota7==undefined)
		{
			TotalQuota7=0;
			
		}
		var TotalQuota8=imObject.getFieldValue('custrecord_adinv_tq_8'); 
		if(TotalQuota8== null || TotalQuota8=='' || TotalQuota8==undefined)
		{
			TotalQuota8=0;
			
		}
		var TotalQuota9=imObject.getFieldValue('custrecord_adinv_tq_9'); 
		if(TotalQuota9== null || TotalQuota9=='' || TotalQuota9==undefined)
		{
			TotalQuota9=0;
			
		}
		var TotalQuota10=imObject.getFieldValue('custrecord_adinv_tq_10'); 
		if(TotalQuota10== null || TotalQuota10=='' || TotalQuota10==undefined)
		{
			TotalQuota10=0;
			
		}
		var TotalQuota11=imObject.getFieldValue('custrecord_adinv_tq_11'); 
		if(TotalQuota11== null || TotalQuota11=='' || TotalQuota11==undefined)
		{
			TotalQuota11=0;
			
		}
		var TotalQuota12=imObject.getFieldValue('custrecord_adinv_tq_12'); 
		if(TotalQuota12== null || TotalQuota12=='' || TotalQuota12==undefined)
		{
			TotalQuota12=0;
			
		}
		var TotalQuota13=imObject.getFieldValue('custrecord_adinv_tq_13'); 
		if(TotalQuota13== null || TotalQuota13=='' || TotalQuota13==undefined)
		{
			TotalQuota13=0;
			
		}
		var TotalQuota14=imObject.getFieldValue('custrecord_adinv_tq_14'); 
		if(TotalQuota14== null || TotalQuota14=='' || TotalQuota14==undefined)
		{
			TotalQuota14=0;
			
		}
		var TotalQuota15=imObject.getFieldValue('custrecord_adinv_tq_15'); 
		
		if(TotalQuota15== null || TotalQuota15=='' || TotalQuota15==undefined)
		{
			TotalQuota15=0;
			
		}
		var TotalQuota16=imObject.getFieldValue('custrecord_adinv_tq_16'); 
		
		if(TotalQuota16== null || TotalQuota16=='' || TotalQuota16==undefined)
		{
			TotalQuota16=0;
			
		}
		var TotalQuota17=imObject.getFieldValue('custrecord_adinv_tq_17'); 
		
		if(TotalQuota17== null || TotalQuota17=='' || TotalQuota17==undefined)
		{
			TotalQuota17=0;
			
		}
		var TotalQuota18=imObject.getFieldValue('custrecord_adinv_tq_18'); 
		
		if(TotalQuota18== null || TotalQuota18=='' || TotalQuota18==undefined)
		{
			TotalQuota18=0;
			
		}
		var TotalQuota19=imObject.getFieldValue('custrecord_adinv_tq_19'); 
		
		if(TotalQuota19== null || TotalQuota19=='' || TotalQuota19==undefined)
		{
			TotalQuota19=0;
			
		}
		var TotalQuota20=imObject.getFieldValue('custrecord_adinv_tq_20'); 
		
		if(TotalQuota20== null || TotalQuota20=='' || TotalQuota20==undefined)
		{
			TotalQuota20=0;
			
		}
		var TotalQuota21=imObject.getFieldValue('custrecord_adinv_tq_21'); 
		
		if(TotalQuota21== null || TotalQuota21=='' || TotalQuota21==undefined)
		{
			TotalQuota21=0;
			
		}
		var TotalQuota22=imObject.getFieldValue('custrecord_adinv_tq_22'); 
		
		if(TotalQuota22== null || TotalQuota22=='' || TotalQuota22==undefined)
		{
			TotalQuota22=0;
			
		}
		var TotalQuota23=imObject.getFieldValue('custrecord_adinv_tq_23'); 
		
		if(TotalQuota23== null || TotalQuota23=='' || TotalQuota23==undefined)
		{
			TotalQuota23=0;
			
		}
		var TotalQuota24=imObject.getFieldValue('custrecord_adinv_tq_24'); 
		
		if(TotalQuota24== null || TotalQuota24=='' || TotalQuota24==undefined)
		{
			TotalQuota24=0;
			
		}
		var TotalQuota25=imObject.getFieldValue('custrecord_adinv_tq_25'); 
		
		if(TotalQuota25== null || TotalQuota25=='' || TotalQuota25==undefined)
		{
			TotalQuota25=0;
			
		}
		var TotalQuota26=imObject.getFieldValue('custrecord_adinv_tq_26'); 
		
		if(TotalQuota26== null || TotalQuota26=='' || TotalQuota26==undefined)
		{
			TotalQuota26=0;
			
		}
		var TotalQuota27=imObject.getFieldValue('custrecord_adinv_tq_27'); 
		
		if(TotalQuota27== null || TotalQuota27=='' || TotalQuota27==undefined)
		{
			TotalQuota27=0;
			
		}
		var TotalQuota28=imObject.getFieldValue('custrecord_adinv_tq_28'); 
		
		if(TotalQuota28== null || TotalQuota28=='' || TotalQuota28==undefined)
		{
			TotalQuota28=0;
			
		}
		var TotalQuota29=imObject.getFieldValue('custrecord_adinv_tq_29'); 
		
		if(TotalQuota29== null || TotalQuota29=='' || TotalQuota29==undefined)
		{
			TotalQuota29=0;
			
		}
		var TotalQuota30=imObject.getFieldValue('custrecord_adinv_tq_30'); 
		
		if(TotalQuota30== null || TotalQuota30=='' || TotalQuota30==undefined)
		{
			TotalQuota30=0;
			
		}
		var TotalQuota31=imObject.getFieldValue('custrecord_adinv_tq_31'); 
		
		if(TotalQuota31== null || TotalQuota31=='' || TotalQuota31==undefined)
		{
			TotalQuota31=0;
			
		}
		
		
		
		// End code to get the Total of Total Quota Duration
		
		
		
		//Total Calculation Both On hand And committed
		var TotalQuota = parseFloat(TotalQuota1)+parseFloat(TotalQuota2)+parseFloat(TotalQuota3)+parseFloat(TotalQuota4)+parseFloat(TotalQuota5)+parseFloat(TotalQuota6)+parseFloat(TotalQuota7)+parseFloat(TotalQuota8)+parseFloat(TotalQuota9)+parseFloat(TotalQuota10)+parseFloat(TotalQuota11)+parseFloat(TotalQuota12)+parseFloat(TotalQuota13)+parseFloat(TotalQuota14)+parseFloat(TotalQuota15)+parseFloat(TotalQuota16)+parseFloat(TotalQuota17)+parseFloat(TotalQuota18)+parseFloat(TotalQuota19)+parseFloat(TotalQuota20)+parseFloat(TotalQuota21)+parseFloat(TotalQuota22)+parseFloat(TotalQuota23)+parseFloat(TotalQuota24)+parseFloat(TotalQuota25)+parseFloat(TotalQuota26)+parseFloat(TotalQuota27)+parseFloat(TotalQuota28)+parseFloat(TotalQuota29)+parseFloat(TotalQuota30)+parseFloat(TotalQuota31);
		imObject.setFieldValue('custrecord_bec_im_totalquotaduration',parseFloat(TotalQuota)); 
		
		var totalonhand=parseFloat(Onhanddate1)+parseFloat(Onhanddate2)+parseFloat(Onhanddate3)+parseFloat(Onhanddate4)+parseFloat(Onhanddate5)+parseFloat(Onhanddate6)+parseFloat(Onhanddate7)+parseFloat(Onhanddate8)+parseFloat(Onhanddate9)+parseFloat(Onhanddate10)+parseFloat(Onhanddate11)+parseFloat(Onhanddate12)+parseFloat(Onhanddate13)+parseFloat(Onhanddate14)+parseFloat(Onhanddate15)+parseFloat(Onhanddate16)+parseFloat(Onhanddate17)+parseFloat(Onhanddate18)+parseFloat(Onhanddate19)+parseFloat(Onhanddate20)+parseFloat(Onhanddate21)+parseFloat(Onhanddate22)+parseFloat(Onhanddate23)+parseFloat(Onhanddate24)+parseFloat(Onhanddate25)+parseFloat(Onhanddate26)+parseFloat(Onhanddate27)+parseFloat(Onhanddate28)+parseFloat(Onhanddate29)+parseFloat(Onhanddate30)+parseFloat(Onhanddate31);
		imObject.setFieldValue('custrecord_bec_im_duration',parseFloat(totalonhand)); 
		
		var totalcommitted=parseFloat(committeddate1)+parseFloat(committeddate2)+parseFloat(committeddate3)+parseFloat(committeddate4)+parseFloat(committeddate5)+parseFloat(committeddate6)+parseFloat(committeddate7)+parseFloat(committeddate8)+parseFloat(committeddate9)+parseFloat(committeddate10)+parseFloat(committeddate11)+parseFloat(committeddate12)+parseFloat(committeddate13)+parseFloat(committeddate14)+parseFloat(committeddate15)+parseFloat(committeddate16)+parseFloat(committeddate17)+parseFloat(committeddate18)+parseFloat(committeddate19)+parseFloat(committeddate20)+parseFloat(committeddate21)+parseFloat(committeddate22)+parseFloat(committeddate23)+parseFloat(committeddate24)+parseFloat(committeddate25)+parseFloat(committeddate26)+parseFloat(committeddate27)+parseFloat(committeddate28)+parseFloat(committeddate29)+parseFloat(committeddate30)+parseFloat(committeddate31);
		imObject.setFieldValue('custrecord_bec_im_commitedduration',parseFloat(totalcommitted));
		//End Calucation
		
		//Submite Record
		var UpdatedId = nlapiSubmitRecord(imObject, true, true);
		nlapiLogExecution('DEBUG','AFTER SUBMIT','UpdatedId ='+UpdatedId)
		//End Submittion
		
	}
	
	  if(type == 'create')
	   {
	    var ImId=nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'afterSubmit', 'IF ID = ' + ImId);
		
		var imObject = nlapiLoadRecord('customrecord_ad_inventory',ImId);
		
		var TotalQuota1=imObject.getFieldValue('custrecord_adinv_tq_1'); 
		if(TotalQuota1== null || TotalQuota1=='' || TotalQuota1==undefined)
		{
			TotalQuota1=0;
		}
		imObject.setFieldValue('custrecord_adinv_oh_1', TotalQuota1); 
		
		var TotalQuota2=imObject.getFieldValue('custrecord_adinv_tq_2'); 
		if(TotalQuota2== null || TotalQuota2=='' || TotalQuota2==undefined)
		{
			TotalQuota2=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_2',TotalQuota2); 
		
		var TotalQuota3=imObject.getFieldValue('custrecord_adinv_tq_3'); 
		if(TotalQuota3== null || TotalQuota3=='' || TotalQuota3==undefined)
		{
			TotalQuota3=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_3', TotalQuota3);
		
		var TotalQuota4=imObject.getFieldValue('custrecord_adinv_tq_4'); 
		if(TotalQuota4== null || TotalQuota4=='' || TotalQuota4==undefined)
		{
			TotalQuota4=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_4',TotalQuota4);
		
		
		var TotalQuota5=imObject.getFieldValue('custrecord_adinv_tq_5'); 
		if(TotalQuota5== null || TotalQuota5=='' || TotalQuota5==undefined)
		{
			TotalQuota5=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_5',TotalQuota5);
		
		var TotalQuota6=imObject.getFieldValue('custrecord_adinv_tq_6'); 
		if(TotalQuota6== null || TotalQuota6=='' || TotalQuota6==undefined)
		{
			TotalQuota6=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_6',TotalQuota6);
		
		
		var TotalQuota7=imObject.getFieldValue('custrecord_adinv_tq_7'); 
		if(TotalQuota7== null || TotalQuota7=='' || TotalQuota7==undefined)
		{
			TotalQuota7=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_7',TotalQuota7);
		
		var TotalQuota8=imObject.getFieldValue('custrecord_adinv_tq_8'); 
		if(TotalQuota8== null || TotalQuota8=='' || TotalQuota8==undefined)
		{
			TotalQuota8=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_8',TotalQuota8);
		
		var TotalQuota9=imObject.getFieldValue('custrecord_adinv_tq_9'); 
		if(TotalQuota9== null || TotalQuota9=='' || TotalQuota9==undefined)
		{
			TotalQuota9=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_9',TotalQuota9);
		
		var TotalQuota10=imObject.getFieldValue('custrecord_adinv_tq_10'); 
		if(TotalQuota10== null || TotalQuota10=='' || TotalQuota10==undefined)
		{
			TotalQuota10=0;
		}
		imObject.setFieldValue('custrecord_adinv_oh_10',TotalQuota10);
		
		var TotalQuota11=imObject.getFieldValue('custrecord_adinv_tq_11'); 
		if(TotalQuota11== null || TotalQuota11=='' || TotalQuota11==undefined)
		{
			TotalQuota11=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_11',TotalQuota11);
		
		var TotalQuota12=imObject.getFieldValue('custrecord_adinv_tq_12'); 
		if(TotalQuota12== null || TotalQuota12=='' || TotalQuota12==undefined)
		{
			TotalQuota12=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_12',TotalQuota12);
		
		var TotalQuota13=imObject.getFieldValue('custrecord_adinv_tq_13'); 
		if(TotalQuota13== null || TotalQuota13=='' || TotalQuota13==undefined)
		{
			TotalQuota13=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_13',TotalQuota13);
		 
		var TotalQuota14=imObject.getFieldValue('custrecord_adinv_tq_14'); 
		if(TotalQuota14== null || TotalQuota14=='' || TotalQuota14==undefined)
		{
			TotalQuota14=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_14',TotalQuota14);
		
		var TotalQuota15=imObject.getFieldValue('custrecord_adinv_tq_15'); 
		if(TotalQuota15== null || TotalQuota15=='' || TotalQuota15==undefined)
		{
			TotalQuota15=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_15',TotalQuota15);
		
		var TotalQuota16=imObject.getFieldValue('custrecord_adinv_tq_16'); 
		if(TotalQuota16== null || TotalQuota16=='' || TotalQuota16==undefined)
		{
			TotalQuota16=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_16',TotalQuota16);
		
		var TotalQuota17=imObject.getFieldValue('custrecord_adinv_tq_17'); 
		if(TotalQuota17== null || TotalQuota17=='' || TotalQuota17==undefined)
		{
			TotalQuota17=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_17',TotalQuota17);
		
		var TotalQuota18=imObject.getFieldValue('custrecord_adinv_tq_18'); 
		if(TotalQuota18== null || TotalQuota18=='' || TotalQuota18==undefined)
		{
			TotalQuota18=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_18',TotalQuota18);
		
		var TotalQuota19=imObject.getFieldValue('custrecord_adinv_tq_19'); 
		if(TotalQuota19== null || TotalQuota19=='' || TotalQuota19==undefined)
		{
			TotalQuota19=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_19',TotalQuota19);
		
		var TotalQuota20=imObject.getFieldValue('custrecord_adinv_tq_20'); 
		if(TotalQuota20== null || TotalQuota20=='' || TotalQuota20==undefined)
		{
			TotalQuota20=0;
		}
		imObject.setFieldValue('custrecord_adinv_oh_20',TotalQuota20);
		
		var TotalQuota21=imObject.getFieldValue('custrecord_adinv_tq_21'); 
		if(TotalQuota21== null || TotalQuota21=='' || TotalQuota21==undefined)
		{
			TotalQuota21=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_21',TotalQuota21);
		
		var TotalQuota22=imObject.getFieldValue('custrecord_adinv_tq_22'); 
		if(TotalQuota22== null || TotalQuota22=='' || TotalQuota22==undefined)
		{
			TotalQuota22=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_22',TotalQuota22);
		
		var TotalQuota23=imObject.getFieldValue('custrecord_adinv_tq_23'); 
		if(TotalQuota23== null || TotalQuota23=='' || TotalQuota23==undefined)
		{
			TotalQuota23=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_23',TotalQuota23);
		
		var TotalQuota24=imObject.getFieldValue('custrecord_adinv_tq_24'); 
		if(TotalQuota24== null || TotalQuota24=='' || TotalQuota24==undefined)
		{
			TotalQuota24=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_24',TotalQuota24);
		
		var TotalQuota25=imObject.getFieldValue('custrecord_adinv_tq_25'); 
		if(TotalQuota25== null || TotalQuota25=='' || TotalQuota25==undefined)
		{
			TotalQuota25=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_25',TotalQuota25);
		
		var TotalQuota26=imObject.getFieldValue('custrecord_adinv_tq_26'); 
		
		if(TotalQuota26== null || TotalQuota26=='' || TotalQuota26==undefined)
		{
			TotalQuota26=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_26',TotalQuota26);
		
		var TotalQuota27=imObject.getFieldValue('custrecord_adinv_tq_27'); 
		if(TotalQuota27== null || TotalQuota27=='' || TotalQuota27==undefined)
		{
			TotalQuota27=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_27',TotalQuota27);
		
		var TotalQuota28=imObject.getFieldValue('custrecord_adinv_tq_28'); 
		if(TotalQuota28== null || TotalQuota28=='' || TotalQuota28==undefined)
		{
			TotalQuota28=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_28',TotalQuota28);
		
		var TotalQuota29=imObject.getFieldValue('custrecord_adinv_tq_29'); 
		if(TotalQuota29== null || TotalQuota29=='' || TotalQuota29==undefined)
		{
			TotalQuota29=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_29',TotalQuota29);
		
		var TotalQuota30=imObject.getFieldValue('custrecord_adinv_tq_30'); 
		if(TotalQuota30== null || TotalQuota30=='' || TotalQuota30==undefined)
		{
			TotalQuota30=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_30',TotalQuota30);
		
		var TotalQuota31=imObject.getFieldValue('custrecord_adinv_tq_31'); 
		if(TotalQuota31== null || TotalQuota31=='' || TotalQuota31==undefined)
		{
			TotalQuota31=0;
			
		}
		imObject.setFieldValue('custrecord_adinv_oh_31', TotalQuota31);
		// End code to get the Total of Total Quota Duration
		//Total Calculation Both On hand And committed
		var TotalQuota = parseFloat(TotalQuota1)+parseFloat(TotalQuota2)+parseFloat(TotalQuota3)+parseFloat(TotalQuota4)+parseFloat(TotalQuota5)+parseFloat(TotalQuota6)+parseFloat(TotalQuota7)+parseFloat(TotalQuota8)+parseFloat(TotalQuota9)+parseFloat(TotalQuota10)+parseFloat(TotalQuota11)+parseFloat(TotalQuota12)+parseFloat(TotalQuota13)+parseFloat(TotalQuota14)+parseFloat(TotalQuota15)+parseFloat(TotalQuota16)+parseFloat(TotalQuota17)+parseFloat(TotalQuota18)+parseFloat(TotalQuota19)+parseFloat(TotalQuota20)+parseFloat(TotalQuota21)+parseFloat(TotalQuota22)+parseFloat(TotalQuota23)+parseFloat(TotalQuota24)+parseFloat(TotalQuota25)+parseFloat(TotalQuota26)+parseFloat(TotalQuota27)+parseFloat(TotalQuota28)+parseFloat(TotalQuota29)+parseFloat(TotalQuota30)+parseFloat(TotalQuota31);
		imObject.setFieldValue('custrecord_bec_im_totalquotaduration',parseFloat(TotalQuota)); 
		imObject.setFieldValue('custrecord_bec_im_duration',parseFloat(TotalQuota));
		
		//Submite Record
		var UpdatedId = nlapiSubmitRecord(imObject, true, true);
		nlapiLogExecution('DEBUG','AFTER SUBMIT','UpdatedId ='+UpdatedId)
		//End Submittion 	
	}

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
