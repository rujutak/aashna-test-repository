// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH SalesRep QuotaManagement after approval
	Author:      
	Company:    
	Date:       
	Version:    

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY

   try	
   {
   	   var context = nlapiGetContext();
	   var remainingUsage=context.getRemainingUsage();
	   nlapiLogExecution('DEBUG', 'stockReconciliationPDF', " Remaining Usage : " + remainingUsage);
	
	    var I_MP_InternalId  = nlapiGetContext().getSetting('SCRIPT', 'custscript_media_plan_id')
		nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_MP_InternalId='+I_MP_InternalId)
	
	    var OldStatus = nlapiGetContext().getSetting('SCRIPT', 'custscript_status_array_old')
	    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','OldStatus='+OldStatus)
		
		var OldStatusValue = OldStatus.split(',')
	
	    for(var h = 0 ; h< OldStatusValue.length ; h++)
	    {
	    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','h='+h)	
	    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','OldStatusValue[h]='+OldStatusValue[h])	
	    }
	
	
	   
	   
	   var O_MPObject = nlapiLoadRecord('customrecord_mediaplan_1',I_MP_InternalId)
	   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_MPObject='+O_MPObject)
	
	   var S_MPStatus = O_MPObject.getFieldValue('custrecord_mp1_status')
	   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPStatus='+S_MPStatus)
	
	   var S_MPSalesRep = O_MPObject.getFieldValue('custrecord_mp1_salesrep')
	   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','S_MPSalesRep='+S_MPSalesRep)
	
	   var MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref')	
	   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount)
	
	  var MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref')	 
	  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount)
	
	
   // Begin code : to get the values from media schedule
	
		for (var j = 1; j <= MediaScheduleLineCount; j++) 	
		{
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'j=' + j)
			var TvProgramItem = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_tvprogram', j)
			var TvProgramMonth = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_month', j)
			var TvProgramYear = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_year', j)
			var TvProgramUnit = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecordcustrecord_mp3_tvprogramunit', j)
			var TvProgramStatus = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_ms_status', j)
			
			
			
			var OldTVProgramStatus = 0;
		
			if(OldStatusValue != null && OldStatusValue != undefined && OldStatusValue != '')
			{
			
			OldTVProgramStatus =OldStatusValue[1];
			//O_OldMPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',j) 	
			}
			
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'TvProgramItem=' + TvProgramItem)
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'TvProgramMonth=' + TvProgramMonth)
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'TvProgramYear=' + TvProgramYear)
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'TvProgramUnit=' + TvProgramUnit)
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'TvProgramStatus=' + TvProgramStatus)
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'OldTVProgramStatus=' + OldTVProgramStatus)
			
			
			if( TvProgramStatus == 2) 	
			{
			
			            if(TvProgramStatus != OldTVProgramStatus)
						{
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=======Inside If part====')
					
						var SR_Filters = new Array();
					    var SR_Coloumns = new Array();
					  
					    SR_Filters[0]= new nlobjSearchFilter('custrecord_bec_qm_program',null,'is', TvProgramItem)
					    SR_Filters[1]= new nlobjSearchFilter('custrecord_bec_qm_month',null,'is',TvProgramMonth)
					    SR_Filters[2]= new nlobjSearchFilter('custrecord_bec_qm_year',null,'is',TvProgramYear)
						SR_Filters[3]= new nlobjSearchFilter('custrecord_bec_qm_type',null,'is',1)
						SR_Filters[4]= new nlobjSearchFilter('custrecord_bec_qm_salesrep',null,'is',S_MPSalesRep)
					  
					    SR_Coloumns[0] =  new nlobjSearchColumn('internalid')
					  
					    var searchresult = nlapiSearchRecord('customrecord_sales_rep_quota_mngm',null,SR_Filters,SR_Coloumns)
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','searchresult='+searchresult)	
					  
						  if (searchresult != null && searchresult != undefined && searchresult != '') 	
						  {
						  
						  	var SalesrepQuotaManagemntId = searchresult[0].getValue('internalid')
						  	nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'SalesrepQuotaManagemntId=' + SalesrepQuotaManagemntId)
							
							
							//====================== For Day 1===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_1'
							var MediaScheduleDayId ='custrecord_mp3_1'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							
							//====================== For Day 2 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_2'
							var MediaScheduleDayId ='custrecord_mp3_2'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day3 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_3'
							var MediaScheduleDayId ='custrecord_mp3_3'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 4 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_4'
							var MediaScheduleDayId ='custrecord_mp3_4'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 5 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_5'
							var MediaScheduleDayId ='custrecord_mp3_5'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 6 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_6'
							var MediaScheduleDayId ='custrecord_mp3_6'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 7===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_7'
							var MediaScheduleDayId ='custrecord_mp3_7'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 8===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_8'
							var MediaScheduleDayId ='custrecord_mp3_8'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 9 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_9'
							var MediaScheduleDayId ='custrecord_mp3_9'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 10 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_10'
							var MediaScheduleDayId ='custrecord_mp3_10'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 11===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_11'
							var MediaScheduleDayId ='custrecord_mp3_11'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 12===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_12'
							var MediaScheduleDayId ='custrecord_mp3_12'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 13 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_13'
							var MediaScheduleDayId ='custrecord_mp3_13'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 14===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_14'
							var MediaScheduleDayId ='custrecord_mp3_14'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 15===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_15'
							var MediaScheduleDayId ='custrecord_mp3_15'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 16===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_16'
							var MediaScheduleDayId ='custrecord_mp3_16'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 17 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_17'
							var MediaScheduleDayId ='custrecord_mp3_17'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 18 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_18'
							var MediaScheduleDayId ='custrecord_mp3_18'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 19===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_19'
							var MediaScheduleDayId ='custrecord_mp3_19'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 20 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_20'
							var MediaScheduleDayId ='custrecord_mp3_20'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 21===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_21'
							var MediaScheduleDayId ='custrecord_mp3_21'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 22 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_22'
							var MediaScheduleDayId ='custrecord_mp3_22'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 23 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_23'
							var MediaScheduleDayId ='custrecord_mp3_23'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 24===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_24'
							var MediaScheduleDayId ='custrecord_mp3_24'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 25===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_25'
							var MediaScheduleDayId ='custrecord_mp3_25'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 26===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_26'
							var MediaScheduleDayId ='custrecord_mp3_26'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 27 ===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_27'
							var MediaScheduleDayId ='custrecord_mp3_27'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 28===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_28'
							var MediaScheduleDayId ='custrecord_mp3_28'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 29===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_29'
							var MediaScheduleDayId ='custrecord_mp3_29'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							//====================== For Day 30===================================================================================
							var QuotaDayDuration = 'custrecord_bec_qm_30'
							var MediaScheduleDayId ='custrecord_mp3_30'
							SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount, O_MPObject)
							
						  	
						  }// end of searchresult != null && searchresult != undefined && searchresult != ''
							
						}//  if(TvProgramStatus != OldTVProgramStatus)
						
						
			
			
			}//End Of if TvProgramStatus != 9
		
		}// end of for (var j = 1; j <= MediaScheduleLineCount; j++) 	
		
   	
   }// try end
   catch(ex)
   {
   	
   }

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
function SalesRepQuotaManagementAfterApproval(j, SalesrepQuotaManagemntId , MediaScheduleDayId , QuotaDayDuration , MediaScheduleLineCount , MediaListLineCount , O_MPObject)
							{
							var O_sr_QManagemntRecord = nlapiLoadRecord('customrecord_sales_rep_quota_mngm',SalesrepQuotaManagemntId)
							nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'O_sr_QManagemntRecord=' + O_sr_QManagemntRecord)
							
							var Dayvalue = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref',MediaScheduleDayId,j)
			                nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Dayvalue='+Dayvalue);
							
							if(Dayvalue != null && Dayvalue != undefined && Dayvalue!= '')
							 {
							 	var AliasQuantity = Dayvalue.split(',')
							    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','AliasQuantity length='+AliasQuantity.length)
							
							      
								  var TotalMediaScheduleDuration = 0;
									     for( var p = 0; p< AliasQuantity.length ; p++)
							             {
										   
										   var TotalDeductDuration = 0;
										   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============Inside Alias Quantity  for loop=============')	
										   // Begin Code :For Deviding the quantity & alias from the day field of the Media schedule.
										   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','AliasQuantity='+AliasQuantity[p])
										   
										    var a= AliasQuantity[p]
				
											var letter="";
											var number="";
											
											for(var q=0;q<a.length;q++)
											{
											var b= a.charAt(q);
											if(isNaN(b) == true)
											{
											letter=letter+b;
											}
											else
											{
											number=number+b;
											}
											}
											nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
											nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
											 
											var I_MediaCategoryquantity = number ;
											var S_MediaCategoryAlias = letter ;
											
										   
										   var I_mediaListDuration = 0;
										   var S_MediaListUnit = 0 ;
										   
										 
										      
											  for (var L=1; L<=MediaListLineCount; L++) 	
											  {
								                   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============Inside Media List Line Count=============')
												   var I_MediaListAlias =  O_MPObject.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',L)  
												   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_MediaListAlias='+I_MediaListAlias)
												   
													   if(S_MediaCategoryAlias == I_MediaListAlias )
													   {
													    //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============Inside S_MediaCategoryAlias == I_MediaListAlias=============')
													   	I_mediaListDuration =  O_MPObject.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration',L)  
													    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_mediaListDuration='+I_mediaListDuration)
														
														S_MediaListUnit =  O_MPObject.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type',L)  
													    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_mediaListDuration='+I_mediaListDuration)
														// Spot   --- 1
														// VTR    ----2
														// TIE IN   --3
														// PR/SCOOP --4 
														if(S_MediaListUnit == 3 || S_MediaListUnit == 4)
														{
														I_mediaListDuration=1;	
														}
														
														// Begin Code : This code for converting duration from minute to second.
										   
														TotalDeductDuration = parseFloat(I_mediaListDuration) * parseFloat(I_MediaCategoryquantity)
										                nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalDeductDuration='+TotalDeductDuration)
											            		
													    break ;
													   }
													   
													  
											   };
											  // End Code : This code for converting duration from minute to second.
										    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalMediaScheduleDuration='+TotalMediaScheduleDuration)
											nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalDeductDuration='+TotalDeductDuration)
										    TotalMediaScheduleDuration = parseFloat(TotalMediaScheduleDuration) + parseFloat(TotalDeductDuration)
										    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalMediaScheduleDuration='+TotalMediaScheduleDuration)
										   
										 
										   } // End Code :For Deviding the quantity & alias from the day field of the Media schedule.  
							   
								 
								  if(S_MediaListUnit == 1)
								  {
								  var I_QuotaManagment_Duration =  parseFloat(TotalMediaScheduleDuration) / parseFloat(60)
								  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_QuotaManagment_Duration='+I_QuotaManagment_Duration)
								  
								  I_QuotaManagment_Duration = I_QuotaManagment_Duration.toFixed(2)
								  
							      O_sr_QManagemntRecord.setFieldValue(QuotaDayDuration,I_QuotaManagment_Duration)
								
								 
								 var Updated_TvProgramId = nlapiSubmitRecord(O_sr_QManagemntRecord , true , true)
							     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Updated_TvProgramId='+Updated_TvProgramId)	
								  }
								  
								  
								  if(S_MediaListUnit == 3 || S_MediaListUnit == 4 )
								  {
								  var I_QuotaManagment_Duration =  parseFloat(TotalMediaScheduleDuration) 
								  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_QuotaManagment_Duration='+I_QuotaManagment_Duration)
								  
								  I_QuotaManagment_Duration = I_QuotaManagment_Duration.toFixed(2)
								  
							      O_sr_QManagemntRecord.setFieldValue(QuotaDayDuration,I_QuotaManagment_Duration)
								
								 
								 var Updated_TvProgramId = nlapiSubmitRecord(O_sr_QManagemntRecord , true , true)
							     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Updated_TvProgramId='+Updated_TvProgramId)	
								  }
								 
							 }// End Of Dayvalue != null && Dayvalue != undefined && Dayvalue!= '	
							 
							 else
							 {
							 	  O_sr_QManagemntRecord.setFieldValue(QuotaDayDuration,0)
								  var Updated_TvProgramId = nlapiSubmitRecord(O_sr_QManagemntRecord , true , true)
							     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Updated_TvProgramId='+Updated_TvProgramId)
							 }
							}// End of Function SalesRepQuotaManagementAfterApproval body.


}
// END FUNCTION =====================================================
