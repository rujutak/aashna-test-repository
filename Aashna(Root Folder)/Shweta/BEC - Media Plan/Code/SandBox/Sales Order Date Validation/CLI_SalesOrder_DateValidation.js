/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_SalesOrder_DateValidation.js
	Date        : 10 May 2013
    Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));


     var blank='';
	 var from_date;
	 var to_date;


  /*
  if(name=='startdate')
	{
		var start_date=nlapiGetFieldValue('startdate');
		
		//alert(' Start Date -->'+start_date);
		
		if(start_date!=null && start_date!='' && start_date!=undefined)
		{
			var date_arr = new Array();
            date_arr = start_date.split('/');
            var start_day = date_arr[0];
            var start_month = date_arr[1];
            var start_year = date_arr[2];
			
		//	alert(' Start Day  -->'+start_day);
		//	alert(' Start Month  -->'+start_month);
		//	alert(' Start Year  -->'+start_year);
				
				
				if(start_day!=1)
				{
					if(start_month == 1 || start_month == 3 || start_month == 5 || start_month == 7 || start_month == 8 || start_month == 10 || start_month == 12)
				{
					  from_date=1+'/'+start_month+'/'+start_year;
		      					  
					  nlapiSetFieldValue('startdate',from_date,true)
			
			         				
				}
				else if(start_month == 4 || start_month == 6 || start_month == 9 || start_month == 11)
				{
					from_date='1'+'/'+start_month+'/'+start_year
				
			 		nlapiSetFieldValue('startdate',from_date,true)
			 
	          
				}
				else if(start_month==2)
				{					
					
					from_date='1'+'/'+start_month+'/'+start_year;
					
			 		var mod=start_year % 4;
							 			
			 		nlapiSetFieldValue('startdate',from_date,true)
			 						
				}
				
					
				}		
					  		
				
						
			
		}//Start Date Check
		
	
		
		return true;
				
	}
	
*/

	/*

	if(name=='enddate')
	{
		var start_date=nlapiGetFieldValue('startdate');
		var end_date=nlapiGetFieldValue('enddate');		
				
		if(end_date!=null && end_date!='')
		{
			if(start_date==null || start_date=='')
			{
				alert('Please enter Start Date .')
				nlapiSetFieldValue('enddate',blank,true)
						
			}
		}
		
		
		if (end_date != null && end_date != '' && end_date != undefined)
		{
			var date_arr = new Array();
			date_arr = end_date.split('/');
			var end_day = date_arr[0];
			var end_month = date_arr[1];
			var end_year = date_arr[2];
			
			if(end_day!=30 && end_day!=31 && end_day!=28 && end_day!=29)
			{
				if(start_date!=null && start_date!='' && start_date!=undefined)
				{
					
					
				if(end_month == 1 || end_month == 3 || end_month == 5 || end_month == 7 || end_month == 8 || end_month == 10 || end_month == 12)
				{
					  to_date=31+'/'+end_month+'/'+end_year;
		      					  
					  nlapiSetFieldValue('enddate',to_date,true)
			
			         				
				}
				else if(end_month == 4 || end_month == 6 || end_month == 9 || end_month == 11)
				{
					to_date='30'+'/'+end_month+'/'+end_year
				
			 		nlapiSetFieldValue('enddate',to_date,true)
			 
	          
				}
				else if(end_month==2)
				{					
					
										
			 		var mod=end_year % 4;
					
					if(mod == 0)
			 		{
			 		    to_date='29'+'/'+end_month+'/'+end_year
			 		}
			 		else
			 		{
			 			to_date='28'+'/'+end_month+'/'+end_year
			 		}
							 			
			 		nlapiSetFieldValue('enddate',to_date,true)
			 						
				}
				
					
					
									
										
					
					
				}
							
			}
			
			
			
			
			
			
		}
		
		
		return true;
			
	}
*/
	

	
	
	
	
	
	
	
   if (name == 'startdate') 
	{
		var start_date = nlapiGetFieldValue('startdate');
		var end_date = nlapiGetFieldValue('enddate');
		
	
		
	if (end_date != null && end_date != '' &&  end_date != undefined) 
	{
		if (start_date != null && start_date != '' &&  end_date != undefined)
		{
			end_date=nlapiStringToDate(end_date);	
			start_date=nlapiStringToDate(start_date);	
				
				if(end_date < start_date)
				{
					alert("Invalid Date Range!\nStart Date cannot be after End Date!")
					nlapiSetFieldValue('startdate', blank, true)
					
					
				}
			
			
		}	
			
		
		
	}	
		
		
	
		
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
   if (name == 'enddate') 
	{
		var start_date = nlapiGetFieldValue('startdate');
		var end_date = nlapiGetFieldValue('enddate');
		
		
		
		if (end_date != null && end_date != '') 
		{
			if (start_date == null || start_date == '') 
			{
				alert('Please enter Start Date .')
				nlapiSetFieldValue('enddate', blank, true)
				
			}
		}
		
		
	if (end_date != null && end_date != '' &&  end_date != undefined) 
	{
		if (start_date != null && start_date != '' &&  end_date != undefined)
		{
			end_date=nlapiStringToDate(end_date);	
			start_date=nlapiStringToDate(start_date);	
				
				if(end_date < start_date)
				{
					alert("Invalid Date Range!\nStart Date cannot be after End Date!")
					nlapiSetFieldValue('enddate', blank, true)
					
					
				}
			
			
		}	
			
		
		
	}	
		
		
	
		
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	return true;

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
