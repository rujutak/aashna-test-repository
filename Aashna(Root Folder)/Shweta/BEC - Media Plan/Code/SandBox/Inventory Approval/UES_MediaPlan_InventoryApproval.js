/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_MediaPlan_InventoryApproval.js
	Date       : 27 April 2013
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    var record_ID=nlapiGetRecordId();
	nlapiLogExecution('DEBUG','IN SUITELET','Record  ID --> '+record_ID);
	
	
	  var params=new Array();
	  params['status']='scheduled';
	  params['runasadmin']='T';
	  params['custscript_media_record_id']=record_ID;
	
	  
	  var startDate = new Date();
	  params['startdate']=startDate.toUTCString();
	  
      nlapiLogExecution('DEBUG', 'suiteletFunction',' params[custscript_media_record_id]: ' + params['custscript_media_record_id']);
	
	  			  
	  var status=nlapiScheduleScript('customscript_mediaplan_inventorysch','customdeploy1',params);
	  nlapiLogExecution('DEBUG', 'suiteletFunction', ' status: ' + status);

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
