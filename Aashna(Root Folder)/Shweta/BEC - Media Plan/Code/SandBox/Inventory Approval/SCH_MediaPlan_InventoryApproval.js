/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_MediaPlan_InventoryApproval.js
	Date       : 27 April 2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{ 


	try
	{
  var O_MPObject;	 
  var MediaScheduleLineCount;
  var MediaListLineCount;
  var context = nlapiGetContext();
   
   var usage_begin=context.getRemainingUsage();
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Usage Begin --> " + usage_begin);

     
    
   var media_plan_ID=context.getSetting('SCRIPT','custscript_media_record_id');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Plan ID  --> " + media_plan_ID);
   
   
   var counter=context.getSetting('SCRIPT','custscript_counter');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Counter  --> " + counter);
   
	    if(counter!=null && counter!='')
		{
			counter = counter;
		}//if
		else
		{
			counter=0;
		}
   
   
     var counter_ms=context.getSetting('SCRIPT','custscript_counter_media_sch');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Counter_ms  --> " + counter_ms);
   
	    if(counter_ms!=null && counter_ms!='')
		{
			counter_ms = counter_ms;
		}//if
		else
		{
			counter_ms=0;
		}
   
   if(media_plan_ID!=null)
   {
   	 O_MPObject=nlapiLoadRecord('customrecord_mediaplan_1',media_plan_ID);
	 //nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Plan OBJECT  --> " + O_MPObject);
	 
	 if(O_MPObject!=null)
	 {
	 	
		MediaScheduleLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp3_mp1ref');	
       nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount='+MediaScheduleLineCount);
		 	
		MediaListLineCount = O_MPObject.getLineItemCount('recmachcustrecord_mp2_mp1ref');	 
		//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount='+MediaListLineCount);
		
		for(var k=1 ; k<= MediaScheduleLineCount ; k++)
		{
			
		var total_day1=0;
		var total_day2=0;
		var total_day3=0;
		var total_day4=0;
		var total_day5=0;
		var total_day6=0;
		var total_day7=0;
		var total_day8=0;
		var total_day9=0;
		var total_day10=0;
		var total_day11=0;
		var total_day12=0;
		var total_day13=0;
		var total_day14=0;
		var total_day15=0;
		var total_day16=0;
		var total_day17=0;
		var total_day18=0;
		var total_day19=0;
		
		var total_day20=0;
		var total_day21=0;
		var total_day22=0;
		var total_day23=0;
		var total_day24=0;
		var total_day25=0;
		var total_day26=0;
		var total_day27=0;
		var total_day28=0;
		var total_day29=0;
		var total_day30=0;
		var total_day31=0;
		
		
		
		
		var day_commit1=0;
		var day_commit2=0;
		var day_commit3=0;
		var day_commit4=0;
		var day_commit5=0;
		var day_commit6=0;
		var day_commit7=0;
		var day_commit8=0;
		var day_commit9=0;
		var day_commit10=0;
		var day_commit11=0;
		var day_commit12=0;
		var day_commit13=0;
		var day_commit14=0;
		var day_commit15=0;
		var day_commit16=0;
		var day_commit17=0;
		var day_commit18=0;
		var day_commit19=0;
		
		var day_commit20=0;
		var day_commit21=0;
		var day_commit22=0;
		var day_commit23=0;
		var day_commit24=0;
		var day_commit25=0;
		var day_commit26=0;
		var day_commit27=0;
		var day_commit28=0;
		var day_commit29=0;
		var day_commit30=0;
		var day_commit31=0;
			
			
			
			
		  var TvProgramQuotaID = O_MPObject.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref',k) 
		  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***************** TvProgramQuotaID *************'+TvProgramQuotaID);
			
			
			if(TvProgramQuotaID!=null &&TvProgramQuotaID!='' && TvProgramQuotaID!=undefined)
			{
				 var TV_Quota_OBJ=nlapiLoadRecord('customrecord_ad_inventory',TvProgramQuotaID);
				// nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TV_Quota_OBJ-->'+TV_Quota_OBJ);
							 
				
				
				 var MediaSch_Filters = new Array();
			     var MediaSch_Coloumns = new Array();
			  	  
				  
				  MediaSch_Filters[0]= new nlobjSearchFilter('custrecord_mp3_tvprgqm_ref',null,'is',TvProgramQuotaID)
								  
				  MediaSch_Coloumns[0] =  new nlobjSearchColumn('internalid')
				   MediaSch_Coloumns[1] =  new nlobjSearchColumn('custrecord_mp3_ms_status')
				                 
				  var searchresult_ms = nlapiSearchRecord('customrecord_mediaplan_3',null,MediaSch_Filters,MediaSch_Coloumns)
				
				
				 if (searchresult_ms != null && searchresult_ms != undefined && searchresult_ms != '')
				 {
				    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule SDerach Results  -->'+searchresult_ms.length);
					for (var j = 0; j < searchresult_ms.length; j++) 
					{
						var mediaschedule_record_Id = searchresult_ms[j].getValue('internalid')
					    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule ID -->'+mediaschedule_record_Id);
					
					    var mediaschedule_status = searchresult_ms[j].getValue('custrecord_mp3_ms_status')
					    //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule Status -->'+mediaschedule_status);
					
					     //====================== For Day 1===================================================================================
							 
							 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' *****************1 Day ********************'+'['+j+']')
							 
							 var TvProgramDayDuration = 'custrecord_adinv_tq_1'
							 var MediaScheduleDayId ='custrecord_mp3_1'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_1'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_1'
							 
							
							  var duration_1=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day1=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
				//		      nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day1 -->'+day1);
							 
							  total_day1=parseFloat(total_day1)+parseFloat(day1);
					//		  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day1 *********** '+'['+j+']'+total_day1);
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day1=0;
								
							  }
							 
							 
							 
							 var total_onHand1=parseFloat(duration_1)-parseFloat(total_day1);
						//	 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand1*********** '+'['+j+']'+total_onHand1);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12)
							 {
							 	day1=0;
								
							 }
							 else 
							 {
							 	day1=day1;
							 }
							 
							 day_commit1= parseFloat(day_commit1)+parseFloat(day1);
							 
						//	 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit1*********** '+'['+j+']'+day_commit1);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand1)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit1)//TVProgramCommitDurationId  
							  
							  
							  
						
	                    //====================== For Day 2===================================================================================
							
							nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' *****************2 Day ********************'+'['+j+']')
							
							 var TvProgramDayDuration = 'custrecord_adinv_tq_2'
							 var MediaScheduleDayId ='custrecord_mp3_2'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_2'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_2'
							 
							 var duration_2=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day2=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						  //   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day2 -->'+day2);
							 
							  total_day2=parseFloat(total_day2)+parseFloat(day2);
						//	 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day2 *********** '+'['+j+']'+total_day2);
							 
							 
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day2=0;
								
							  }
							 
							 
							 
							 
							 var total_onHand2=parseFloat(duration_2)-parseFloat(total_day2);
					//		 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand2*********** '+'['+j+']'+total_onHand2);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day2=0;
								
							 }
							 else 
							 {
							 	day2=day2;
							 }
							 
							 day_commit2= parseFloat(day_commit2)+parseFloat(day2);
							 
				//			nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit2*********** '+'['+j+']'+day_commit2);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand2)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit2)//TVProgramCommitDurationId  

					
					   
                           //====================== For Day 3===================================================================================
							  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' *****************3 Day ********************'+'['+j+']')
							 
							 
							 var TvProgramDayDuration = 'custrecord_adinv_tq_3'
						
							 var MediaScheduleDayId ='custrecord_mp3_3'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_3'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_3'
							                                                 
							 var duration_3=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day3=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						      nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day3 -->'+day3);
							 
							  total_day3=parseFloat(total_day3)+parseFloat(day3);
							  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day3 *********** -->'+total_day3);
							 
							 
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day3=0;
								
							  }
							 
							 
							 
							 
							 
							 
							 
							 
							 var total_onHand3=parseFloat(duration_3)-parseFloat(total_day3);
							 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand3*********** -->'+total_onHand3);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day3=0;
								
							 }
							 else 
							 {
							 	day3=day3;
							 }
							 
							 day_commit3= parseFloat(day_commit3)+parseFloat(day3);
							 
							nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit3*********** -->'+day_commit3);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand3)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit3)//TVProgramCommitDurationId  

							 
							
							
							
							
						     //====================== For Day 4===================================================================================
							 var TvProgramDayDuration = 'custrecord_adinv_tq_4'
							nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_4'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_4'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_4'
							
							 
							 
							  var duration_4=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day4=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day4 -->'+day4);
							 
							  total_day4=parseFloat(total_day4)+parseFloat(day4);
							  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day4 *********** -->'+total_day4);
							 
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day4=0;
								
							  }
							 
							 
							 
							 
							 
							 
							 var total_onHand4=parseFloat(duration_4)-parseFloat(total_day4);
							 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand4*********** -->'+total_onHand4);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day4=0;
								
							 }
							 else 
							 {
							 	day4=day4;
							 }
							 
							 day_commit4= parseFloat(day_commit4)+parseFloat(day4);
							 
							nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit4*********** -->'+day_commit4);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand4)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit4)//TVProgramCommitDurationId  

							 
							 
							 
							 
							 
							 
							 
							 
							 //====================== For Day 5===================================================================================
							 
							 var TvProgramDayDuration = 'custrecord_adinv_tq_5'
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 var MediaScheduleDayId ='custrecord_mp3_5'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_5'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_5'
							
							
							  var duration_5=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day5=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day5 -->'+day5);
							 
							  total_day5=parseFloat(total_day5)+parseFloat(day5);
							 // nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day5 *********** -->'+total_day5);
							 
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day5=0;
								
							  }
							 
							 
							 
							 var total_onHand5=parseFloat(duration_5)-parseFloat(total_day5);
							// nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand5*********** -->'+total_onHand5);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day5=0;
								
							 }
							 else 
							 {
							 	day5=day5;
							 }
							 
							 day_commit5= parseFloat(day_commit5)+parseFloat(day5);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit5*********** -->'+day_commit5);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand5)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit5)//TVProgramCommitDurationId  

							
							
							
							 //====================== For Day 6===================================================================================
							 var TvProgramDayDuration = 'custrecord_adinv_tq_6'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 var MediaScheduleDayId ='custrecord_mp3_6'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_6'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_6'
							 
							 var duration_6=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day6=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day6 -->'+day6);
							 
							 				 
							 						 
							 
							  total_day6=parseFloat(total_day6)+parseFloat(day6);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day6 *********** -->'+total_day6);
							 
							 
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day6=0;
								
							  }
							 
							 
							 
							 
							 
							 var total_onHand6=parseFloat(duration_6)-parseFloat(total_day6);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand6*********** -->'+total_onHand6);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day6=0;
								
							 }
							 else 
							 {
							 	day6=day6;
							 }
							 
							 day_commit6= parseFloat(day_commit6)+parseFloat(day6);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit6*********** '+'['+j+']'+day_commit6);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand6)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit6)//TVProgramCommitDurationId  

							 //====================== For Day 7===================================================================================
							 var TvProgramDayDuration = 'custrecord_adinv_tq_7'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_7'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_7'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_7'
							
							
							var duration_7=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day7=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day7 -->'+day7);
							 
							  total_day7=parseFloat(total_day7)+parseFloat(day7);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day7 *********** -->'+total_day7);
							 
							 
							   if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day7=0;
								
							  }
							 
							 
							 
							 var total_onHand7=parseFloat(duration_7)-parseFloat(total_day7);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand7*********** -->'+total_onHand7);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day7=0;
								
							 }
							 else 
							 {
							 	day7=day7;
							 }
							 
							 day_commit7= parseFloat(day_commit7)+parseFloat(day7);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit7*********** '+'['+j+']'+day_commit7);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand7)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit7)//TVProgramCommitDurationId  
							
							
							
							
							
							
							
							 //====================== For Day 8===================================================================================
							 var TvProgramDayDuration  = 'custrecord_adinv_tq_8'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_8'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_8'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_8'
							
                            
							  var duration_8=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day8=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day8 -->'+day8);
							 
							  total_day8=parseFloat(total_day8)+parseFloat(day8);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day8 *********** -->'+total_day8);
							 
							   if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day8=0;
								
							  }
							 
							 
							 
							 var total_onHand8=parseFloat(duration_8)-parseFloat(total_day8);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand8*********** -->'+total_onHand8);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day8=0;
								
							 }
							 else 
							 {
							 	day8=day8;
							 }
							 
							 day_commit8= parseFloat(day_commit8)+parseFloat(day8);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit8*********** '+'['+j+']'+day_commit8);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand8)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit8)//TVProgramCommitDurationId  
							
							


							 //====================== For Day 9===================================================================================
							 var TvProgramDayDuration  = 'custrecord_adinv_tq_9'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_9'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_9'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_9'
							
							
							  var duration_9=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day9=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day9 -->'+day9);
							 
							  total_day9=parseFloat(total_day9)+parseFloat(day9);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day9 *********** -->'+total_day9);
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day9=0;
								
							  }
							 
							 
							 
							 
							 var total_onHand9=parseFloat(duration_9)-parseFloat(total_day9);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand9*********** -->'+total_onHand9);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day9=0;
								
							 }
							 else 
							 {
							 	day9=day9;
							 }
							 
							 day_commit9= parseFloat(day_commit9)+parseFloat(day9);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit9*********** '+'['+j+']'+day_commit9);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand9)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit9)//TVProgramCommitDurationId  
							
							
							
							
							 //====================== For Day 10===================================================================================
							 var TvProgramDayDuration  = 'custrecord_adinv_tq_10'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_10'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_10'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_10'
							 

                                var duration_10=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day10=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day10 -->'+day10);
							 
							  total_day10=parseFloat(total_day10)+parseFloat(day10);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day10 *********** -->'+total_day10);
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day10=0;
								
							  }
							 
							 
							 var total_onHand10=parseFloat(duration_10)-parseFloat(total_day10);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand10*********** -->'+total_onHand10);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day10=0;
								
							 }
							 else 
							 {
							 	day10=day10;
							 }
							 
							 day_commit10= parseFloat(day_commit10)+parseFloat(day10);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit10*********** '+'['+j+']'+day_commit10);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand10)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit10)//TVProgramCommitDurationId  
							





							 //====================== For Day 11===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_11'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_11'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_11'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_11'
							 
                              var duration_11=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day11=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day11 -->'+day11);
							 
							  total_day11=parseFloat(total_day11)+parseFloat(day11);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day11 *********** -->'+total_day11);
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day11=0;
								
							  }
							 
							 
							 
							 var total_onHand11=parseFloat(duration_11)-parseFloat(total_day11);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand11*********** -->'+total_onHand11);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day11=0;
								
							 }
							 else 
							 {
							 	day11=day11;
							 }
							 
							 day_commit11= parseFloat(day_commit11)+parseFloat(day11);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit11*********** '+'['+j+']'+day_commit11);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand11)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit11)//TVProgramCommitDurationId  
							






							 //====================== For Day 12===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_12'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_12'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_12'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_12'
							

                              var duration_12=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day12=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day12 -->'+day12);
							 
							  total_day12=parseFloat(total_day12)+parseFloat(day12);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day12 *********** -->'+total_day12);
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day12=0;
								
							  }
							 
							 
							 
							 var total_onHand12=parseFloat(duration_12)-parseFloat(total_day12);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand12*********** -->'+total_onHand12);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day12=0;
								
							 }
							 else 
							 {
							 	day12=day12;
							 }
							 
							 day_commit12= parseFloat(day_commit12)+parseFloat(day12);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit12*********** -'+'['+j+']'+day_commit12);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand12)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit12)//TVProgramCommitDurationId  
							

							 //====================== For Day 13===================================================================================
							
							 var TvProgramDayDuration ='custrecord_adinv_tq_13'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_13'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_13'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_13'
							
							
							 var duration_13=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day13=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day13 -->'+day13);
							 
							  total_day13=parseFloat(total_day13)+parseFloat(day13);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day13 *********** -->'+total_day13);
							 
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day13=0;
								
							  }
							 
							 
							 var total_onHand13=parseFloat(duration_13)-parseFloat(total_day13);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand13*********** -->'+total_onHand13);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day13=0;
								
							 }
							 else 
							 {
							 	day13=day13;
							 }
							 
							 day_commit13= parseFloat(day_commit13)+parseFloat(day13);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit13*********** '+'['+j+']'+day_commit13);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand13)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit13)//TVProgramCommitDurationId  
							
							
							 
							 //====================== For Day 14===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_14'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_14'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_14'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_14'
							 
                             var duration_14=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day14=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day14 -->'+day14);
							 
							  total_day14=parseFloat(total_day14)+parseFloat(day14);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day14 *********** -->'+total_day14);
							 
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day14=0;
								
							  }
							 
							 
							 
							 var total_onHand14=parseFloat(duration_14)-parseFloat(total_day14);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand14*********** -->'+total_onHand14);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day14=0;
								
							 }
							 else 
							 {
							 	day14=day14;
							 }
							 
							 day_commit14= parseFloat(day_commit14)+parseFloat(day14);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit14*********** '+'['+j+']'+day_commit14);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand14)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit14)//TVProgramCommitDurationId  
							
                           



							//====================== For Day 15===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_15'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_15'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_15'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_15'
							
                              var duration_15=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day15=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day15 -->'+day15);
							 
							  total_day15=parseFloat(total_day15)+parseFloat(day15);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day15 *********** -->'+total_day15);
							 
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day15=0;
								
							  }
							 
							 
							 var total_onHand15=parseFloat(duration_15)-parseFloat(total_day15);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand15*********** -->'+total_onHand15);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day15=0;
								
							 }
							 else 
							 {
							 	day15=day15;
							 }
							 
							 day_commit15= parseFloat(day_commit15)+parseFloat(day15);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit15*********** '+'['+j+']'+day_commit15);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand15)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit15)//TVProgramCommitDurationId  
							




							 //====================== For Day 16===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_16'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_16'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_16'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_16'
							
                              var duration_16=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day16=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day16 -->'+day16);
							 
							  total_day16=parseFloat(total_day16)+parseFloat(day16);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day16 *********** -->'+total_day16);
							 
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day16=0;
								
							  }
							 var total_onHand16=parseFloat(duration_16)-parseFloat(total_day16);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand16*********** -->'+total_onHand16);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day16=0;
								
							 }
							 else 
							 {
							 	day16=day16;
							 }
							 
							 day_commit16= parseFloat(day_commit16)+parseFloat(day16);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit16*********** '+'['+j+']'+day_commit16);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand16)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit16)//TVProgramCommitDurationId  










							 //====================== For Day 17===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_17'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_17'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_17'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_17'
							

                             var duration_17=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day17=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day17 -->'+day17);
							 
							  total_day17=parseFloat(total_day17)+parseFloat(day17);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day17 *********** -->'+total_day17);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day17=0;
								
							  }
							 var total_onHand17=parseFloat(duration_17)-parseFloat(total_day17);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand17*********** -->'+total_onHand17);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day17=0;
								
							 }
							 else 
							 {
							 	day17=day17;
							 }
							 
							 day_commit17= parseFloat(day_commit17)+parseFloat(day17);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit17*********** '+'['+j+']'+day_commit17);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand17)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit17)//TVProgramCommitDurationId  










							 //====================== For Day 18===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_18'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_18'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_18'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_18'
							 

                             var duration_18=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day18=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day18 -->'+day18);
							 
							  total_day18=parseFloat(total_day18)+parseFloat(day18);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day18 *********** -->'+total_day18);
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day18=0;
								
							  }
							 var total_onHand18=parseFloat(duration_18)-parseFloat(total_day18);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand18*********** -->'+total_onHand18);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day18=0;
								
							 }
							 else 
							 {
							 	day18=day18;
							 }
							 
							 day_commit18= parseFloat(day_commit18)+parseFloat(day18);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit18***********'+'['+j+']'+day_commit18);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand18)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit18)//TVProgramCommitDurationId  




							 
							 //====================== For Day 19===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_19'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_19'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_19'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_19'
							
                              var duration_19=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day19=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day19 -->'+day19);
							 
							  total_day19=parseFloat(total_day19)+parseFloat(day19);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day19 *********** -->'+total_day19);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day19=0;
								
							  }
							 var total_onHand19=parseFloat(duration_19)-parseFloat(total_day19);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand19*********** -->'+total_onHand19);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day19=0;
								
							 }
							 else 
							 {
							 	day19=day19;
							 }
							 
							 day_commit19= parseFloat(day_commit19)+parseFloat(day19);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit19*********** '+'['+j+']'+day_commit19);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand19)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit19)//TVProgramCommitDurationId  





							 //====================== For Day 20===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_20'
							/// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_20'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_20'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_20'
							 
                               var duration_20=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day20=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day20 -->'+day20);
							 
							  total_day20=parseFloat(total_day20)+parseFloat(day20);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day20 *********** -->'+total_day20);
							   if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day20=0;
								
							  }
							 var total_onHand20=parseFloat(duration_20)-parseFloat(total_day20);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand20*********** -->'+total_onHand20);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day20=0;
								
							 }
							 else 
							 {
							 	day20=day20;
							 }
							 
							 day_commit20= parseFloat(day_commit20)+parseFloat(day20);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit20*********** '+'['+j+']'+day_commit20);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand20)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit20)//TVProgramCommitDurationId  











							  //====================== For Day 21===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_21'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_21'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_21'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_21'
							
                               var duration_21=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day21=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day21 -->'+day21);
							 
							  total_day21=parseFloat(total_day21)+parseFloat(day21);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day21 *********** -->'+total_day21);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day21=0;
								
							  }
							 var total_onHand21=parseFloat(duration_21)-parseFloat(total_day21);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand21*********** -->'+total_onHand21);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day21=0;
								
							 }
							 else 
							 {
							 	day21=day21;
							 }
							 
							 day_commit21= parseFloat(day_commit21)+parseFloat(day21);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit21*********** '+'['+j+']'+day_commit21);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand21)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit21)//TVProgramCommitDurationId  





							 //====================== For Day 22===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_22'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_22'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_22'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_22'
							 
                                var duration_22=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day22=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day22 -->'+day22);
							 
							  total_day22=parseFloat(total_day22)+parseFloat(day22);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day22 *********** -->'+total_day22);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day22=0;
								
							  }
							 var total_onHand22=parseFloat(duration_22)-parseFloat(total_day22);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand22*********** -->'+total_onHand22);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day22=0;
								
							 }
							 else 
							 {
							 	day22=day22;
							 }
							 
							 day_commit22= parseFloat(day_commit22)+parseFloat(day22);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit22*********** '+'['+j+']'+day_commit22);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand22)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit22)//TVProgramCommitDurationId  







							 //====================== For Day 23===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_23'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_23'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_23'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_23'
							
							
							  var duration_23=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day23=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day23 -->'+day23);
							  
							  total_day23=parseFloat(total_day23)+parseFloat(day23);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day23 *********** -->'+total_day23);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day23=0;
								
							  }
							 var total_onHand23=parseFloat(duration_23)-parseFloat(total_day23);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand23*********** -->'+total_onHand23);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day23=0;
								
							 }
							 else 
							 {
							 	day23=day23;
							 }
							 
							 day_commit23= parseFloat(day_commit23)+parseFloat(day23);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit23*********** '+'['+j+']'+day_commit23);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand23)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit23)//TVProgramCommitDurationId  

							
							
							
							 //====================== For Day 24===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_24'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_24'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_24'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_24'
							 
                             var duration_24=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day24=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day24 -->'+day24);
							 
							  total_day24=parseFloat(total_day24)+parseFloat(day24);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day24 *********** -->'+total_day24);
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day24=0;
								
							  }
							 var total_onHand24=parseFloat(duration_24)-parseFloat(total_day24);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand24*********** -->'+total_onHand24);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day24=0;
								
							 }
							 else 
							 {
							 	day24=day24;
							 }
							 
							 day_commit24= parseFloat(day_commit24)+parseFloat(day24);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit24*********** '+'['+j+']'+day_commit24);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand24)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit24)//TVProgramCommitDurationId  

							




							//====================== For Day 25===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_25'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_25'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_25'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_25'
							
                              var duration_25=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day25=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day25 -->'+day25);
							 
							  total_day25=parseFloat(total_day25)+parseFloat(day25);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day25 *********** -->'+total_day25);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day25=0;
								
							  }
							 var total_onHand25=parseFloat(duration_25)-parseFloat(total_day25);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand25*********** -->'+total_onHand25);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day25=0;
								
							 }
							 else 
							 {
							 	day25=day25;
							 }
							 
							 day_commit25= parseFloat(day_commit25)+parseFloat(day25);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit25***********'+'['+j+']'+day_commit25);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand25)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit25)//TVProgramCommitDurationId  

							


							 //====================== For Day 26===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_26'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_26'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_26'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_26'
							
                               var duration_26=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day26=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day26 -->'+day26);
							 
							  total_day26=parseFloat(total_day26)+parseFloat(day26);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day26 *********** -->'+total_day26);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day26=0;
								
							  }
							 var total_onHand26=parseFloat(duration_26)-parseFloat(total_day26);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand26*********** -->'+total_onHand26);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day26=0;
								
							 }
							 else 
							 {
							 	day26=day26;
							 }
							 
							 day_commit26= parseFloat(day_commit26)+parseFloat(day26);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit26*********** '+'['+j+']'+day_commit26);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand26)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit26)//TVProgramCommitDurationId  

							

							
							 //====================== For Day 27===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_27'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_27'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_27'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_27'
							
                              var duration_27=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day27=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day27 -->'+day27);
							 
							  total_day27=parseFloat(total_day27)+parseFloat(day27);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day27 *********** -->'+total_day27);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day27=0;
								
							  }
							 var total_onHand27=parseFloat(duration_27)-parseFloat(total_day27);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand27*********** -->'+total_onHand27);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day27=0;
								
							 }
							 else 
							 {
							 	day27=day27;
							 }
							 
							 day_commit27= parseFloat(day_commit27)+parseFloat(day27);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit27*********** '+'['+j+']'+day_commit27);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand27)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit27)//TVProgramCommitDurationId  














							 //====================== For Day 28===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_28'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_28'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_28'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_28'
							

                               var duration_28=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day28=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day28 -->'+day28);
							 
							  total_day28=parseFloat(total_day28)+parseFloat(day28);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day28 *********** -->'+total_day28);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day28=0;
								
							  }
							 var total_onHand28=parseFloat(duration_28)-parseFloat(total_day28);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand28*********** -->'+total_onHand28);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day28=0;
								
							 }
							 else 
							 {
							 	day28=day28;
							 }
							 
							 day_commit28= parseFloat(day_commit28)+parseFloat(day28);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit28*********** '+'['+j+']'+day_commit28);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand28)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit28)//TVProgramCommitDurationId  






							 
							 //====================== For Day 29===================================================================================
							
							 var TvProgramDayDuration ='custrecord_adinv_tq_29'
							// ////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_29'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_29'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_29'
							
							  var duration_29=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day29=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day29 -->'+day29);
							 
							  total_day29=parseFloat(total_day29)+parseFloat(day29);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day29 *********** -->'+total_day29);
							   if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day29=0;
								
							  }
							 var total_onHand29=parseFloat(duration_29)-parseFloat(total_day29);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand29*********** -->'+total_onHand29);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day29=0;
								
							 }
							 else 
							 {
							 	day29=day29;
							 }
							 
							 day_commit29= parseFloat(day_commit29)+parseFloat(day29);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit29*********** '+'['+j+']'+day_commit29);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand29)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit29)//TVProgramCommitDurationId  







							 //====================== For Day 30===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_30'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_30'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_30'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_30'
							
                             	  var duration_30=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day30=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day30 -->'+day30);
							 
							  total_day30=parseFloat(total_day30)+parseFloat(day30);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day30 *********** -->'+total_day30);
							  if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day30=0;
								
							  }
							 var total_onHand30=parseFloat(duration_30)-parseFloat(total_day30);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand30*********** -->'+total_onHand30);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day30=0;
								
							 }
							 else 
							 {
							 	day30=day30;
							 }
							 
							 day_commit30= parseFloat(day_commit30)+parseFloat(day30);
							 
							//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit30*********** '+'['+j+']'+day_commit30);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand30)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit30)//TVProgramCommitDurationId  






							 
							 //====================== For Day 31===================================================================================
							 var TvProgramDayDuration ='custrecord_adinv_tq_31'
							 //////nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
							 
							 var MediaScheduleDayId ='custrecord_mp3_31'
							 var TVProgramOnhandDayDurationId = 'custrecord_adinv_oh_31'
							 var TVProgramCommitDurationId = 'custrecord_adinv_com_31'
							 

	                          var duration_31=TV_Quota_OBJ.getFieldValue(TvProgramDayDuration);
							  var day31=ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
						     // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule day31 -->'+day31);
							 
							  total_day31=parseFloat(total_day31)+parseFloat(day31);
							 // //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** Media Schedule total_day31 *********** -->'+total_day31);
							 if(mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							  {
							  	
								total_day31=0;
								
							  }
							 var total_onHand31=parseFloat(duration_31)-parseFloat(total_day31);
							// //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** total_onHand31*********** -->'+total_onHand31);
							 
							 
							 if(mediaschedule_status==2 || mediaschedule_status==3 || mediaschedule_status==5 || mediaschedule_status==6 || mediaschedule_status==11 || mediaschedule_status==12 )
							 {
							 	day31=0;
								
							 }
							 else 
							 {
							 	day31=day31;
							 }
							 
							 day_commit31= parseFloat(day_commit31)+parseFloat(day31);
							 
							 //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******** day_commit31*********** '+'['+j+']'+day_commit31);
							  
							 TV_Quota_OBJ.setFieldValue(TVProgramOnhandDayDurationId,total_onHand31)//TVProgramOnhandDayDurationId
						     TV_Quota_OBJ.setFieldValue(TVProgramCommitDurationId, day_commit31)//TVProgramCommitDurationId  
					
					       
					
					   //Schedulescriptafterusageexceeded_media_sch
					
					  var  usageEnd_ms = context.getRemainingUsage();
		              nlapiLogExecution('DEBUG', 'schedulerFunction',' ********************  usageEnd_ms ['+j+'] **************************' + usageEnd_ms);
						
						if (usageEnd_ms < 100) 
						{
						    nlapiLogExecution('DEBUG', 'schedulerFunction',' ******************Rescheduled ***************** ' +mediaschedule_record_Id);
							Schedulescriptafterusageexceeded_media_sch(mediaschedule_record_Id);
							break;	
						}		
					
					
					
					
					}//For MS
				}//Search MS
								
			}//TV ID Check
			
			
			
			
			
			//-----------------
					
					//Start on total on Hand calculation
					var Onhanddate1=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_1'); 
					//nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(Onhanddate1== null || Onhanddate1=='' || Onhanddate1==undefined)
					{
						Onhanddate1=0;
						
					}
					var Onhanddate2=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_2'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty2 = ' + Onhanddate2);
					if(Onhanddate2== null || Onhanddate2==''|| Onhanddate2==undefined)
					{
						Onhanddate2=0;
					
					}
					var Onhanddate3=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_3');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate3== null || Onhanddate3=='' || Onhanddate3==undefined)
					{
						Onhanddate3=0;
						
					}
					var Onhanddate4=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_4');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate4== null || Onhanddate4=='' || Onhanddate4==undefined)
					{
						Onhanddate4=0;
						
					}
					var Onhanddate5=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_5');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate5== null || Onhanddate5=='' || Onhanddate5==undefined)
					{
						Onhanddate5=0;
						
					}
					var Onhanddate6=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_6');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate6== null || Onhanddate6=='' || Onhanddate6==undefined)
					{
						Onhanddate6=0;
						
					}
					var Onhanddate7=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_7');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate7== null || Onhanddate7=='' || Onhanddate7==undefined)
					{
						Onhanddate7=0;
						
					}
					var Onhanddate8=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_8');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate8== null || Onhanddate8=='' || Onhanddate8==undefined)
					{
						Onhanddate8=0;
						
					}
					var Onhanddate9=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_9');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate9== null || Onhanddate9=='' || Onhanddate9==undefined)
					{
						Onhanddate9=0;
						
					}
					var Onhanddate10=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_10');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate10== null || Onhanddate10=='' || Onhanddate10==undefined)
					{
						Onhanddate10=0;
						
					}
					var Onhanddate11=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_11');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate11== null || Onhanddate11=='' || Onhanddate11==undefined)
					{
						Onhanddate11=0;
						
					}
					var Onhanddate12=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_12');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate12== null || Onhanddate12=='' || Onhanddate12==undefined)
					{
						Onhanddate12=0;
						
					}
					var Onhanddate13=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_13');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate13== null || Onhanddate13=='' || Onhanddate13==undefined)
					{
						Onhanddate13=0;
						
					}
					var Onhanddate14=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_14');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate14== null || Onhanddate14=='' || Onhanddate14==undefined)
					{
						Onhanddate14=0;
						
					}
					var Onhanddate15=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_15');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate15== null || Onhanddate15=='' || Onhanddate15==undefined)
					{
						Onhanddate15=0;
						
					}
					var Onhanddate16=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_16');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate16== null || Onhanddate16=='' || Onhanddate16==undefined)
					{
						Onhanddate16=0;
						
					}
					var Onhanddate17=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_17');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate17== null || Onhanddate17=='' || Onhanddate17==undefined)
					{
						Onhanddate17=0;
						
					}
					var Onhanddate18=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_18');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate18== null || Onhanddate18=='' || Onhanddate18==undefined)
					{
						Onhanddate18=0;
						
					}
					var Onhanddate19=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_19');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate19== null || Onhanddate19=='' || Onhanddate19==undefined)
					{
						Onhanddate19=0;
						
					}
					var Onhanddate20=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_20');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate20== null || Onhanddate20=='' || Onhanddate20==undefined)
					{
						Onhanddate20=0;
						
					}
					var Onhanddate21=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_21');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate21== null || Onhanddate21=='' || Onhanddate21==undefined)
					{
						Onhanddate21=0;
						
					}
					var Onhanddate22=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_22');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate22== null || Onhanddate22=='' || Onhanddate22==undefined)
					{
						Onhanddate22=0;
						
					}
					var Onhanddate23=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_23');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate23== null || Onhanddate23=='' || Onhanddate23==undefined)
					{
						Onhanddate23=0;
						
					}
					var Onhanddate24=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_24');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate24== null || Onhanddate24=='' || Onhanddate24==undefined)
					{
						Onhanddate24=0;
						
					}
					var Onhanddate25=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_25');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate25== null || Onhanddate25=='' || Onhanddate25==undefined)
					{
						Onhanddate25=0;
						
					}
					var Onhanddate26=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_26');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate26== null || Onhanddate26=='' || Onhanddate26==undefined)
					{
						Onhanddate26=0;
						
					}
					var Onhanddate27=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_27');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate27== null || Onhanddate27=='' || Onhanddate27==undefined)
					{
						Onhanddate27=0;
						
					}
					var Onhanddate28=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_28');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate28== null || Onhanddate28=='' || Onhanddate28==undefined)
					{
						Onhanddate28=0;
						
					}
					var Onhanddate29=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_29');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate29== null || Onhanddate29=='' || Onhanddate29==undefined)
					{
						Onhanddate29=0;
						
					}
					var Onhanddate30=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_30');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate30== null || Onhanddate30=='' || Onhanddate30==undefined)
					{
						Onhanddate30=0;
						
					}
					var Onhanddate31=TV_Quota_OBJ.getFieldValue('custrecord_adinv_oh_31');
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty 3= ' + Onhanddate3);
					if(Onhanddate31== null || Onhanddate31=='' || Onhanddate31==undefined)
					{
						Onhanddate31=0;
						
					}
					//End Total On Hand Calulation
					
					
					
					//Start On Committed Duraion Value Calculation 
					
					var committeddate1=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_1'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate1== null || committeddate1=='' || committeddate1==undefined)
					{
						committeddate1=0;
						
					}
					var committeddate2=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_2'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate2== null || committeddate2=='' || committeddate2==undefined)
					{
						committeddate2=0;
						
					}
					var committeddate3=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_3'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate3== null || committeddate3=='' || committeddate3==undefined)
					{
						committeddate3=0;
						
					}
					var committeddate4=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_4'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate4== null || committeddate4=='' || committeddate4==undefined)
					{
						committeddate4=0;
						
					}
					var committeddate5=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_5'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate5== null || committeddate5=='' || committeddate5==undefined)
					{
						committeddate5=0;
						
					}
					var committeddate6=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_6'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate6== null || committeddate6=='' || committeddate6==undefined)
					{
						committeddate6=0;
						
					}
					var committeddate7=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_7'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate7== null || committeddate7=='' || committeddate7==undefined)
					{
						committeddate7=0;
						
					}
					var committeddate8=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_8'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate8== null || committeddate8=='' || committeddate8==undefined)
					{
						committeddate8=0;
						
					}
					var committeddate9=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_9'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate9== null || committeddate9=='' || committeddate9==undefined)
					{
						committeddate9=0;
						
					}
					var committeddate10=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_10'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate10== null || committeddate10=='' || committeddate10==undefined)
					{
						committeddate10=0;
						
					}
					var committeddate11=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_11'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate11== null || committeddate11=='' || committeddate11==undefined)
					{
						committeddate11=0;
						
					}
					var committeddate12=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_12'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate12== null || committeddate12=='' || committeddate12==undefined)
					{
						committeddate12=0;
						
					}
					var committeddate13=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_13'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate13== null || committeddate13=='' || committeddate13==undefined)
					{
						committeddate13=0;
						
					}
					var committeddate14=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_14'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate14== null || committeddate14=='' || committeddate14==undefined)
					{
						committeddate14=0;
						
					}
					var committeddate15=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_15'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate15== null || committeddate15=='' || committeddate15==undefined)
					{
						committeddate15=0;
						
					}
					var committeddate16=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_16'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate16== null || committeddate16=='' || committeddate16==undefined)
					{
						committeddate16=0;
						
					}
					var committeddate17=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_17'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate17== null || committeddate17=='' || committeddate17==undefined)
					{
						committeddate17=0;
						
					}
					var committeddate18=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_18'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate18== null || committeddate18=='' || committeddate18==undefined)
					{
						committeddate18=0;
						
					}
					var committeddate19=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_19'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate19== null || committeddate19=='' || committeddate19==undefined)
					{
						committeddate19=0;
						
					}
					var committeddate20=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_20'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate20== null || committeddate20=='' || committeddate20==undefined)
					{
						committeddate20=0;
						
					}
					var committeddate21=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_21'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate21== null || committeddate21=='' || committeddate21==undefined)
					{
						committeddate21=0;
						
					}
					var committeddate22=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_22'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate22== null || committeddate22=='' || committeddate22==undefined)
					{
						committeddate22=0;
						
					}
					var committeddate23=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_23'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate23== null || committeddate23=='' || committeddate23==undefined)
					{
						committeddate23=0;
						
					}
					var committeddate24=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_24'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate24== null || committeddate24=='' || committeddate24==undefined)
					{
						committeddate24=0;
						
					}
					var committeddate25=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_25'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate25== null || committeddate25=='' || committeddate25==undefined)
					{
						committeddate25=0;
						
					}
					var committeddate26=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_26'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate26== null || committeddate26=='' || committeddate26==undefined)
					{
						committeddate26=0;
						
					}
					var committeddate27=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_27'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate27== null || committeddate27=='' || committeddate27==undefined)
					{
						committeddate27=0;
						
					}
					var committeddate28=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_28'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate28== null || committeddate28=='' || committeddate28==undefined)
					{
						committeddate28=0;
						
					}
					var committeddate29=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_29'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate29== null || committeddate29=='' || committeddate29==undefined)
					{
						committeddate29=0;
						
					}
					var committeddate30=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_30'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate30== null || committeddate30=='' || committeddate30==undefined)
					{
						committeddate30=0;
						
					}
					var committeddate31=TV_Quota_OBJ.getFieldValue('custrecord_adinv_com_31'); 
					////nlapiLogExecution('DEBUG', 'after Submit', 'on Hand qty1 = ' + Onhanddate1);
					if(committeddate31== null || committeddate31=='' || committeddate31==undefined)
					{
						committeddate31=0;
						
					}
												
					//End Get Total Committed Duration.
					
					
					// Begin code to get the Total of Total Quota Duration
					var TotalQuota1=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_1'); 
					
					if(TotalQuota1== null || TotalQuota1=='' || TotalQuota1==undefined)
					{
						TotalQuota1=0;
						
					}
					
					var TotalQuota2=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_2'); 
					
					if(TotalQuota2== null || TotalQuota2=='' || TotalQuota2==undefined)
					{
						TotalQuota2=0;
						
					}
					
					var TotalQuota3=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_3'); 
					
					if(TotalQuota3== null || TotalQuota3=='' || TotalQuota3==undefined)
					{
						TotalQuota3=0;
						
					}
					var TotalQuota4=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_4'); 
					if(TotalQuota4== null || TotalQuota4=='' || TotalQuota4==undefined)
					{
						TotalQuota4=0;
						
					}
					var TotalQuota5=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_5'); 
					if(TotalQuota5== null || TotalQuota5=='' || TotalQuota5==undefined)
					{
						TotalQuota5=0;
						
					}
					var TotalQuota6=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_6'); 
					if(TotalQuota6== null || TotalQuota6=='' || TotalQuota6==undefined)
					{
						TotalQuota6=0;
						
					}
					var TotalQuota7=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_7'); 
					if(TotalQuota7== null || TotalQuota7=='' || TotalQuota7==undefined)
					{
						TotalQuota7=0;
						
					}
					var TotalQuota8=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_8'); 
					if(TotalQuota8== null || TotalQuota8=='' || TotalQuota8==undefined)
					{
						TotalQuota8=0;
						
					}
					var TotalQuota9=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_9'); 
					if(TotalQuota9== null || TotalQuota9=='' || TotalQuota9==undefined)
					{
						TotalQuota9=0;
						
					}
					var TotalQuota10=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_10'); 
					if(TotalQuota10== null || TotalQuota10=='' || TotalQuota10==undefined)
					{
						TotalQuota10=0;
						
					}
					var TotalQuota11=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_11'); 
					if(TotalQuota11== null || TotalQuota11=='' || TotalQuota11==undefined)
					{
						TotalQuota11=0;
						
					}
					var TotalQuota12=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_12'); 
					if(TotalQuota12== null || TotalQuota12=='' || TotalQuota12==undefined)
					{
						TotalQuota12=0;
						
					}
					var TotalQuota13=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_13'); 
					if(TotalQuota13== null || TotalQuota13=='' || TotalQuota13==undefined)
					{
						TotalQuota13=0;
						
					}
					var TotalQuota14=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_14'); 
					if(TotalQuota14== null || TotalQuota14=='' || TotalQuota14==undefined)
					{
						TotalQuota14=0;
						
					}
					var TotalQuota15=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_15'); 
					
					if(TotalQuota15== null || TotalQuota15=='' || TotalQuota15==undefined)
					{
						TotalQuota15=0;
						
					}
					var TotalQuota16=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_16'); 
					
					if(TotalQuota16== null || TotalQuota16=='' || TotalQuota16==undefined)
					{
						TotalQuota16=0;
						
					}
					var TotalQuota17=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_17'); 
					
					if(TotalQuota17== null || TotalQuota17=='' || TotalQuota17==undefined)
					{
						TotalQuota17=0;
						
					}
					var TotalQuota18=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_18'); 
					
					if(TotalQuota18== null || TotalQuota18=='' || TotalQuota18==undefined)
					{
						TotalQuota18=0;
						
					}
					var TotalQuota19=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_19'); 
					
					if(TotalQuota19== null || TotalQuota19=='' || TotalQuota19==undefined)
					{
						TotalQuota19=0;
						
					}
					var TotalQuota20=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_20'); 
					
					if(TotalQuota20== null || TotalQuota20=='' || TotalQuota20==undefined)
					{
						TotalQuota20=0;
						
					}
					var TotalQuota21=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_21'); 
					
					if(TotalQuota21== null || TotalQuota21=='' || TotalQuota21==undefined)
					{
						TotalQuota21=0;
						
					}
					var TotalQuota22=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_22'); 
					
					if(TotalQuota22== null || TotalQuota22=='' || TotalQuota22==undefined)
					{
						TotalQuota22=0;
						
					}
					var TotalQuota23=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_23'); 
					
					if(TotalQuota23== null || TotalQuota23=='' || TotalQuota23==undefined)
					{
						TotalQuota23=0;
						
					}
					var TotalQuota24=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_24'); 
					
					if(TotalQuota24== null || TotalQuota24=='' || TotalQuota24==undefined)
					{
						TotalQuota24=0;
						
					}
					var TotalQuota25=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_25'); 
					
					if(TotalQuota25== null || TotalQuota25=='' || TotalQuota25==undefined)
					{
						TotalQuota25=0;
						
					}
					var TotalQuota26=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_26'); 
					
					if(TotalQuota26== null || TotalQuota26=='' || TotalQuota26==undefined)
					{
						TotalQuota26=0;
						
					}
					var TotalQuota27=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_27'); 
					
					if(TotalQuota27== null || TotalQuota27=='' || TotalQuota27==undefined)
					{
						TotalQuota27=0;
						
					}
					var TotalQuota28=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_28'); 
					
					if(TotalQuota28== null || TotalQuota28=='' || TotalQuota28==undefined)
					{
						TotalQuota28=0;
						
					}
					var TotalQuota29=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_29'); 
					
					if(TotalQuota29== null || TotalQuota29=='' || TotalQuota29==undefined)
					{
						TotalQuota29=0;
						
					}
					var TotalQuota30=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_30'); 
					
					if(TotalQuota30== null || TotalQuota30=='' || TotalQuota30==undefined)
					{
						TotalQuota30=0;
						
					}
					var TotalQuota31=TV_Quota_OBJ.getFieldValue('custrecord_adinv_tq_31'); 
					
					if(TotalQuota31== null || TotalQuota31=='' || TotalQuota31==undefined)
					{
						TotalQuota31=0;
						
					}
					
					
					
					// End code to get the Total of Total Quota Duration
					
					
					
					//Total Calculation Both On hand And committed
					var TotalQuota = parseFloat(TotalQuota1)+parseFloat(TotalQuota2)+parseFloat(TotalQuota3)+parseFloat(TotalQuota4)+parseFloat(TotalQuota5)+parseFloat(TotalQuota6)+parseFloat(TotalQuota7)+parseFloat(TotalQuota8)+parseFloat(TotalQuota9)+parseFloat(TotalQuota10)+parseFloat(TotalQuota11)+parseFloat(TotalQuota12)+parseFloat(TotalQuota13)+parseFloat(TotalQuota14)+parseFloat(TotalQuota15)+parseFloat(TotalQuota16)+parseFloat(TotalQuota17)+parseFloat(TotalQuota18)+parseFloat(TotalQuota19)+parseFloat(TotalQuota20)+parseFloat(TotalQuota21)+parseFloat(TotalQuota22)+parseFloat(TotalQuota23)+parseFloat(TotalQuota24)+parseFloat(TotalQuota25)+parseFloat(TotalQuota26)+parseFloat(TotalQuota27)+parseFloat(TotalQuota28)+parseFloat(TotalQuota29)+parseFloat(TotalQuota30)+parseFloat(TotalQuota31);
					
					TV_Quota_OBJ.setFieldValue('custrecord_bec_im_totalquotaduration',parseFloat(TotalQuota)); 
					
					var totalonhand=parseFloat(Onhanddate1)+parseFloat(Onhanddate2)+parseFloat(Onhanddate3)+parseFloat(Onhanddate4)+parseFloat(Onhanddate5)+parseFloat(Onhanddate6)+parseFloat(Onhanddate7)+parseFloat(Onhanddate8)+parseFloat(Onhanddate9)+parseFloat(Onhanddate10)+parseFloat(Onhanddate11)+parseFloat(Onhanddate12)+parseFloat(Onhanddate13)+parseFloat(Onhanddate14)+parseFloat(Onhanddate15)+parseFloat(Onhanddate16)+parseFloat(Onhanddate17)+parseFloat(Onhanddate18)+parseFloat(Onhanddate19)+parseFloat(Onhanddate20)+parseFloat(Onhanddate21)+parseFloat(Onhanddate22)+parseFloat(Onhanddate23)+parseFloat(Onhanddate24)+parseFloat(Onhanddate25)+parseFloat(Onhanddate26)+parseFloat(Onhanddate27)+parseFloat(Onhanddate28)+parseFloat(Onhanddate29)+parseFloat(Onhanddate30)+parseFloat(Onhanddate31);
					
					TV_Quota_OBJ.setFieldValue('custrecord_bec_im_duration',parseFloat(totalonhand)); 
					
					var totalcommitted=parseFloat(committeddate1)+parseFloat(committeddate2)+parseFloat(committeddate3)+parseFloat(committeddate4)+parseFloat(committeddate5)+parseFloat(committeddate6)+parseFloat(committeddate7)+parseFloat(committeddate8)+parseFloat(committeddate9)+parseFloat(committeddate10)+parseFloat(committeddate11)+parseFloat(committeddate12)+parseFloat(committeddate13)+parseFloat(committeddate14)+parseFloat(committeddate15)+parseFloat(committeddate16)+parseFloat(committeddate17)+parseFloat(committeddate18)+parseFloat(committeddate19)+parseFloat(committeddate20)+parseFloat(committeddate21)+parseFloat(committeddate22)+parseFloat(committeddate23)+parseFloat(committeddate24)+parseFloat(committeddate25)+parseFloat(committeddate26)+parseFloat(committeddate27)+parseFloat(committeddate28)+parseFloat(committeddate29)+parseFloat(committeddate30)+parseFloat(committeddate31);
					
					TV_Quota_OBJ.setFieldValue('custrecord_bec_im_commitedduration',parseFloat(totalcommitted));
					//End Calucation
									 
				 var Updated_TvProgramId = nlapiSubmitRecord(TV_Quota_OBJ , true , true)
			     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','**********************Updated_TvProgramId******************='+Updated_TvProgramId);			
		
		           var  usageEnd = context.getRemainingUsage();
		    nlapiLogExecution('DEBUG', 'schedulerFunction',' ********************  usageEnd ['+k+'] **************************' + usageEnd);
						
			if (usageEnd < 100) 
			{
			    nlapiLogExecution('DEBUG', 'schedulerFunction',' ******************Rescheduled ***************** ' +TvProgramQuotaID);
				Schedulescriptafterusageexceeded(TvProgramQuotaID);
				break;	
			}		
		
		
		}//Media Schedule List Count For
			
	 }//Object Check
	
   }//Media ID 
   
     
      
				 
                 //nlapiLogExecution('DEBUG', 'schedulerFunction', "********************* End****************");
		
	}//try
	catch(ex)
	{
		 nlapiLogExecution('DEBUG', 'schedulerFunction', " Exception Caught --> " + ex);
		
	}
  
   

}

// END SCHEDULED FUNCTION ===============================================


function ReduceDaywiseDuration(j,TvProgramQuotaID,mediaschedule_record_Id,MediaScheduleDayId , TVProgramOnhandDayDurationId, TVProgramCommitDurationId , TvProgramDayDuration  ,  O_MPObject, MediaListLineCount,MediaScheduleLineCount )
 {
 	var total_temp=0;  
	var total_commit=0;
 	var temp;
 	var day_alias;
	var media_sch_status;
	var media_plan_id;
	var media_plan_object;
	var I_TvProgram_CommitDuration;
	
 	 var media_sch_obj=nlapiLoadRecord('customrecord_mediaplan_3',mediaschedule_record_Id);
	 //nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Sch OBJ --> " + media_sch_obj);
	 
	  nlapiLogExecution('DEBUG', 'schedulerFunction', " MediaScheduleDayId --> " + MediaScheduleDayId);
	 
	 if(media_sch_obj!=null)
	 {
	 	day_alias=media_sch_obj.getFieldValue(MediaScheduleDayId);
		nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule ALias  --> " + day_alias);
		
	 	media_plan_id=media_sch_obj.getFieldValue('custrecord_mp3_mp1ref');
		nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Plan ID  --> " + media_plan_id);
		
			
		media_sch_status=media_sch_obj.getFieldValue('custrecord_mp3_ms_status');
		//nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule Status  --> " + media_sch_status);
		
		
		//===========================================
		
		     var O_TVProgramItem = nlapiLoadRecord('customrecord_ad_inventory',TvProgramQuotaID)
			 //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','O_TVProgramItem='+O_TVProgramItem)
			 
			 var TvProgramDayDuration1 = O_TVProgramItem.getFieldValue(TvProgramDayDuration)
			 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration1='+TvProgramDayDuration1)
		
		   
		
		//=============================================
		
		if(media_plan_id!=null && media_plan_id!='' && media_plan_id!=undefined)
		{
			media_plan_object=nlapiLoadRecord('customrecord_mediaplan_1',media_plan_id);
			
			if(media_plan_object!=null && media_plan_object!='' && media_plan_object!=undefined)
			{
				var MediaScheduleLineCount_new = media_plan_object.getLineItemCount('recmachcustrecord_mp3_mp1ref');	
		        //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaScheduleLineCount_new='+MediaScheduleLineCount_new);
				 	
				var MediaListLineCount_new = media_plan_object.getLineItemCount('recmachcustrecord_mp2_mp1ref');	 
				//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaListLineCount_new='+MediaListLineCount_new);
				
				
				
		
		
				
	//	if (media_sch_status != 2) 
		{
			//nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' In Media Sch check 9............');
			
			if (day_alias != null && day_alias != '' && day_alias != undefined) 
			{
				nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' In day_alias  check ............');
				
				var aliasQuantity = day_alias.split(',');
				nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' In aliasQuantity.length check ............'+aliasQuantity.length);
			    var TotalMediaScheduleDuration = 0;
			 for (var k = 0; k < aliasQuantity.length; k++)
			 {
			   var TotalDeductDuration = 0;
			
				var alias_ms = aliasQuantity[k];
				nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
				
				var letter = "";
				var number = "";
				
				for (var q = 0; q < alias_ms.length; q++)
				 {
				 	nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'alias_ms.length --> ' + alias_ms.length)
					
					
					var b = alias_ms.charAt(q);
					if (isNaN(b) == true) 
					{
						letter = letter + b;
					}
					else 
					{
						number = number + b;
					}
					
					
				}//END - For
				
				var I_mediaListDuration=0;	
				var S_MediaListUnit=0;		
				 
				var I_MediaCategoryquantity = number ;
				var S_MediaCategoryAlias = letter ;
				
				
				
				for (var L = 1; L <= MediaListLineCount_new; L++) 
				{
					
					var I_MediaListAlias = media_plan_object.getLineItemValue('recmachcustrecord_mp2_mp1ref', 'custrecord_mp2_alias', L)
					nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_MediaListAlias=' + I_MediaListAlias)
					
					if (S_MediaCategoryAlias == I_MediaListAlias) 
					{
							I_mediaListDuration = media_plan_object.getLineItemValue('recmachcustrecord_mp2_mp1ref', 'custrecord_mp2_duration', L)
							nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_mediaListDuration=' + I_mediaListDuration)
							
							S_MediaListUnit = media_plan_object.getLineItemValue('recmachcustrecord_mp2_mp1ref', 'custrecord_mp2_type', L)
							nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'I_mediaListDuration=' + I_mediaListDuration)
						if(S_MediaListUnit == 3 || S_MediaListUnit == 4)
						{
						I_mediaListDuration=1;	
						}
										// Begin Code : This code for converting duration from minute to second.
						   
						TotalDeductDuration = parseFloat(I_mediaListDuration) * parseFloat(I_MediaCategoryquantity)
		               nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalDeductDuration='+TotalDeductDuration)
						break;
					}//Check Alias
					
					
				};//Media List Count
				 // End Code : This code for converting duration from minute to second.
				 
				 TotalMediaScheduleDuration = parseFloat(TotalMediaScheduleDuration) + parseFloat(TotalDeductDuration)
				nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TotalMediaScheduleDuration == 1********============='+TotalMediaScheduleDuration)				
			}//For Loop Alias
			
			// End Code :For Deviding the quantity & alias from the day field of the Media schedule.  
			
			 var SetDuration = 0;
			 
			  if(S_MediaListUnit == 1)
				  {
				  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TvProgramDayDuration1 == 1********============='+TvProgramDayDuration1)
				    if(TvProgramDayDuration1 != null && TvProgramDayDuration1 != undefined && TvProgramDayDuration1 != '')
					{
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******S_MediaListUnit == 1********=============')
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TvProgramQuotaID ********============='+TvProgramQuotaID)
						
						TvProgramDayDuration = parseFloat(TvProgramDayDuration1) * parseFloat(60)
						SetDuration = TvProgramDayDuration - TotalMediaScheduleDuration
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalMediaScheduleDuration='+TotalMediaScheduleDuration) 
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
					    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','SetDuration='+SetDuration)
						
						  var I_TVprogram_OnhandDuration =  parseFloat(SetDuration) / parseFloat(60)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TVprogram_OnhandDuration='+I_TVprogram_OnhandDuration)
						  
						  I_TvProgram_CommitDuration =  parseFloat(TotalMediaScheduleDuration) / parseFloat(60)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TvProgram_CommitDuration='+I_TvProgram_CommitDuration)
						  
						  temp = parseFloat(TvProgramDayDuration1)- parseFloat(I_TvProgram_CommitDuration)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','temp======'+temp)
						  
						   total_temp=parseFloat(total_temp)+parseFloat(temp);
				
			               total_commit=parseFloat(total_commit)+parseFloat(I_TvProgram_CommitDuration);
						  
						 // O_TVProgramItem.setFieldValue(TVProgramOnhandDayDurationId,temp)//TVProgramOnhandDayDurationId
						 // O_TVProgramItem.setFieldValue(TVProgramCommitDurationId, I_TvProgram_CommitDuration)//TVProgramCommitDurationId
					}
				  
				  }	//Media List Unit 
				  
				   if(S_MediaListUnit == 3 || S_MediaListUnit==4 )
				  {
				  	nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TvProgramDayDuration1 == 1********============='+TvProgramDayDuration1)
					if(TvProgramDayDuration1 != null && TvProgramDayDuration1 != undefined && TvProgramDayDuration1 != '')
					{
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******S_MediaListUnit == 3 || S_MediaListUnit==4********=============')
						nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','=============*******TvProgramQuotaID ********============='+TvProgramQuotaID) 
						 TvProgramDayDuration = parseFloat(TvProgramDayDuration1) 
						 SetDuration = TvProgramDayDuration - TotalMediaScheduleDuration
					     nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TotalMediaScheduleDuration='+TotalMediaScheduleDuration) 
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','TvProgramDayDuration='+TvProgramDayDuration)
						 nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','SetDuration='+SetDuration)
						
						  var I_TVprogram_OnhandDuration =  parseFloat(SetDuration) 
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TVprogram_OnhandDuration='+I_TVprogram_OnhandDuration)
						  
						  I_TvProgram_CommitDuration =  parseFloat(TotalMediaScheduleDuration) 
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','I_TvProgram_CommitDuration='+I_TvProgram_CommitDuration)
						  
						  temp = parseFloat(TvProgramDayDuration1)- parseFloat(I_TvProgram_CommitDuration)
						  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','temp======'+temp)
						  
						  total_temp=parseFloat(total_temp)+parseFloat(temp);
				
			              total_commit=parseFloat(total_commit)+parseFloat(I_TvProgram_CommitDuration);
						  
						//  O_TVProgramItem.setFieldValue(TVProgramOnhandDayDurationId,temp)//TVProgramOnhandDayDurationId
						//  O_TVProgramItem.setFieldValue(TVProgramCommitDurationId, I_TvProgram_CommitDuration)//TVProgramCommitDurationId
					}
				  }//3----4
			
			     
			}// Alias Check
			
			
			    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******************** total_temp ****************'+total_temp)
			   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ********************* total_commit ***************'+total_commit)
			  
			    //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ******************** temp ****************'+temp)
			    //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT',' ********************* I_TvProgram_CommitDuration ***************'+I_TvProgram_CommitDuration)
			
			
		}// Other Than Approved
				
				
				
				
				
				
			}
			
		}
		
		
		
		
		
				
	 }//Media Sch OBJECT 
	
	
	return total_commit;
 }//ReduceDaywiseDuration

 
 
 
function Schedulescriptafterusageexceeded(i)
{
			////Define all parameters to schedule the script for voucher generation.
			 var params=new Array();
			 params['status']='scheduled';
		 	 params['runasadmin']='T';
			 params['custscript_counter']=i;
			 
			 var startDate = new Date();
		 	 params['startdate']=startDate.toUTCString();
			
			 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
			 nlapiLogExecution('DEBUG','After Scheduling','Script scheduled status='+ status);
			 
			 ////If script is scheduled then successfuly then check for if status=queued
			 if (status == 'QUEUED') 
		 	 {
				nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
		 	 }
}//fun close



function Schedulescriptafterusageexceeded_media_sch(i)
{
			////Define all parameters to schedule the script for voucher generation.
			 var params=new Array();
			 params['status']='scheduled';
		 	 params['runasadmin']='T';
			 params['custscript_counter_media_sch']=i;
			 
			 var startDate = new Date();
		 	 params['startdate']=startDate.toUTCString();
			
			 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
			 nlapiLogExecution('DEBUG','After Scheduling','Script scheduled status='+ status);
			 
			 ////If script is scheduled then successfuly then check for if status=queued
			 if (status == 'QUEUED') 
		 	 {
				nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
		 	 }
}//fun close

