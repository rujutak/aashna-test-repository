/**13May20132017
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_RoleWise_Disable_Button.js
	Author:
	Company:
	Date:
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...

   var ms_status_arr=new Array();
   var media_pl_status1 = '';//J

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_rolewise(type)
{


	try
	{
		//J
		var media_pl_status = nlapiGetFieldValue('custrecord_mp1_status');
		if(media_pl_status != null && media_pl_status != '')
		{
			media_pl_status1 = media_pl_status;
		}
		//J


		var line_count=nlapiGetLineItemCount('recmachcustrecord_mp3_mp1ref');
	//	alert(' Line Count -->'+line_count);


		if(line_count!=null)
		{
			for(var x=1;x<=line_count;x++)
			{


			var media_sch_status=nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',x)
		//	alert('media_sch_status'+media_sch_status)

			ms_status_arr.push(media_sch_status);

			}






		}//Line Count






	 }//Try
	 catch(e)
	 {
	 	alert(' Exception Caught -->'+e)


	 }//Catch


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_role()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY

	/*


 var MediaScheduleLineCount = nlapiGetLineItemCount('recmachcustrecord_mp3_mp1ref')
 alert('MediaScheduleLineCount =='+MediaScheduleLineCount)


  var Mp1Status = nlapiGetFieldValue('custrecord_mp1_status')
  alert('Mp1Status =='+Mp1Status)

   for(var i= 1 ; i <= MediaScheduleLineCount ; i++)
   {

	 if(Mp1Status == 2)
	 {
	 	 alert(' In status........')

	 	nlapiSetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',i,2)
	 }


   }

*/











	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{


	 nlapiLogExecution('DEBUG', 'fieldChanged', 'Field Change ........');


     var role = nlapiGetRole();
     nlapiLogExecution('DEBUG', 'fieldChanged', 'role ==' + role);

     var status = nlapiGetFieldValue('custrecord_mp1_status')
	 nlapiLogExecution('DEBUG', 'fieldChanged', 'status ==' + status);

	 var media_sch_status;
	 var media_sch_index;


	 //J
	 var media_plan_status = nlapiGetFieldValue('custrecord_mp1_status')

	 /*
		 * Media Plan Status  --> Rejected All / Rejected Partial / Cancelled by BEC / Cancelled by Client
		 * Role    			  --> TV - Sales Rep
		 * Add     			  --> N
		 * Edit    			  --> N
		 * Delete  			  --> N
		 * View    			  --> Y
		 *
	 */
	 if(role==1089 || role==1082)
	 {
	 	if(media_plan_status == 3 || media_plan_status == 4 || media_plan_status == 5 || media_plan_status == 6)
	 	{
	 		alert('You cannot select this Status.')
	 		nlapiSetFieldValue('custrecord_mp1_status',media_pl_status1)
	 	}
	 }
	 //J

	 /*
		 * Status  --> Rejected
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */

	  if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==3)
			 {
			  	alert(' Status cannot be changed to Rejected .')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role


	 /*
		 * Status  --> Sent to CH3
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */

	if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==4)
			 {
			  	alert(' Status cannot be changed to Sent to CH3 .')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role




	 /*
		 * Status  --> Cancelled by BEC / Cancelled by BEC(after sent)
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */



	if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==5 || media_sch_status==11)
			 {
			  	alert(' Status cannot be changed to Cancelled by BEC .')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role




    /*
		 * Status  --> Cancelled by client / Cancelled by BEC(after sent)
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */


	  if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==6 || media_sch_status==12)
			 {
			  	alert(' Status cannot be changed to Cancelled by client.')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role




	 /*
		 * Status  --> Move
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */


	 if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==7)
			 {
			  	alert(' Status cannot be changed to Move.')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role







    /*
		 * Status  --> Change
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */


	 if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==8)
			 {
			  	alert(' Status cannot be changed to Change.')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role






	 /*
		 * Status  --> Cancelled by BEC (after sent)
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */


	 if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==11)
			 {
			  	alert(' Status cannot be changed to Change.')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role







	 /*
		 * Status  --> Cancelled by client (after sent)
		 * Role    --> TV - Sales Rep
		 * Add     --> N
		 * Edit    --> N
		 * Delete  --> N
		 * View    --> Y
		 *
	 */




	 if(role==1089 || role==1082)
	  {
	  	     media_sch_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');


			// alert(' Index -->'+media_sch_index);


	  	  	 media_sch_status=nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status')
		//	 alert(' Status -->'+media_sch_status);

			 if(media_sch_status==12)
			 {
			  	alert(' Status cannot be changed to Change.')
				if(ms_status_arr!=null)
				{
					for(var d=1;d<=ms_status_arr.length;d++)
					{
						if(media_sch_index==d)
						{

							nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',ms_status_arr[d-1])
							break;
						}

					}
				}



				return false;

			 }//Status Check



	  }//Role
















}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================

