// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES Media Plan Set status Values
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type ,form)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
	
	
	nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','type='+type)
	
	var role = nlapiGetRole();
    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','role='+role)
   
    var Status = nlapiGetFieldValue('custrecord_mp1_status')
    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Status='+Status)
	
	
   
   
   
    try 	
	{
    /*
    * Begin Code 
    * Status : Submitted 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
           if(role == 1089)
			 {
			 	
				 if (Status == 2) 	
				 {
				 						
					if(type == 'view')
					{
					      var buttonOBJ=form.getButton('edit');
						  nlapiLogExecution('DEBUG', 'beforeLoadRecord', ' Button Object ==' + buttonOBJ);
						  
						  var button1 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	      button1.style.visibility = "hidden";
						  
						  if(buttonOBJ!=null)
						  {
						   buttonOBJ.setDisabled(true);
						   
						  }
					 
					 }
					
				 }
			 }
		
    /*
    * End Code
    * Status : Submitted 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
	
	/*
    * Begin Code 
    * Status : Approved 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
           if(role == 1089)
			 {
			 	
				 if (Status == 7) 	
				 {
				 						
					if(type == 'view')
					{
					      var buttonOBJ=form.getButton('edit');
						  nlapiLogExecution('DEBUG', 'beforeLoadRecord', ' Button Object ==' + buttonOBJ);
						  
						  var button1 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	      button1.style.visibility = "hidden";
						  
						  if(buttonOBJ!=null)
						  {
						   buttonOBJ.setDisabled(true);
						  }
					 
					 }
					
				 }
			 }
		
    /*
    * End Code
    * Status : Approved 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
	
	//-------------------------------------------
	
	 /*
    * Begin Code 
    * Status : Approved 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y 
	*/ 
	
	if(role == 1093)
			 {
			 	
				 if (Status == 7) 	
				 {
				 						
					if(type == 'view')
					{
					      var buttonOBJ=form.getButton('edit');
						  nlapiLogExecution('DEBUG', 'beforeLoadRecord', ' Button Object ==' + buttonOBJ);
						  
						  var button1 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	      button1.style.visibility = "hidden";
						  
						  if(buttonOBJ!=null)
						  {
						   buttonOBJ.setDisabled(true);
						  }
					 
					 }
					
					
				 }
			 }
	
	/*
    * End Code 
    * Status : Submitted 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y
	*/  
	
	//------------------------------------
	 /* Begin Code 
    * Status : Exported 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
           if(role == 1089)
			 {
			 	
				 if (Status == 8) 	
				 {
				 						
					if(type == 'view')
					{
					      var buttonOBJ=form.getButton('edit');
						  nlapiLogExecution('DEBUG', 'beforeLoadRecord', ' Button Object ==' + buttonOBJ);
						  
						  var button2 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	      button2.style.visibility = "hidden";
						  
						  if(buttonOBJ!=null)
						  {
						   buttonOBJ.setDisabled(true);
						  }
					 
					 }
					
				 }
			 }
		
    /*
    * End Code
    * Status : Exported 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/ 
	
        
    }
    catch (ex) {
        nlapiLogExecution('debug', 'test', ex);
    }


	return true;

 

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
     /*
    * Begin code: If role is quieng staff & MP1 Status is submitted then
    */
	
	nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'type == '+type);
	
	var CurrentRole = nlapiGetRole();
	nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'CurrentRole == '+CurrentRole);
	
	var Mp1Status = nlapiGetFieldValue('custrecord_mp1_status')
	nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'Mp1Status == '+Mp1Status);
	
    if(Mp1Status == 2)
	{
		if(CurrentRole == 1093)
		{
			
			if(type == 'delete')
			{
			 nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'type delete  inside== '+type);
			 throw('You Can not delete this record')
			 return false;
			}
			else
			{
				return true;
			}
			
		
		}
		else
		{
			return true
		}
	
	}
	else
	{
		return true
	}
	
   
    /*
    * End code: If role is quieng staff & MP1 Status is submitted then
    */

	

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	
	
	
	
	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
