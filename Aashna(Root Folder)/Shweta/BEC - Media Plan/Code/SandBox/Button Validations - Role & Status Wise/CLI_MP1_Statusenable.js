// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInitStatusEnable(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
   
	//alert('.............')				
   
   var role = nlapiGetRole();
 // alert('role='+role)
   
   var Status = nlapiGetFieldValue('custrecord_mp1_status')
  // alert('Status='+Status);
   
   
   
    try 	
	{
    /*
    * Begin Code 
    * Status : Submitted 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Delete : N 
    * Edit   : N 
    * Cancel : N 
	*/
             if(role == 1089)
			 {
			 	
				 if (Status == 2) 	
				 {
				 	var button = window.document.getElementById('tbl__cancel')
				 	button.style.visibility = "hidden";
					
					var button2 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button2.style.visibility = "hidden";
					
				    var button3 = window.document.getElementById('tbl_newrec83')		
				    button3.style.visibility = 'hidden';
					
					var button4 = window.document.getElementById('tbl_newrec85')		
				    button4.style.visibility = 'hidden';
					
					//var Button5 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_addedit')		
				    //Button5.style.visibility = 'hidden';
					
					var Button6 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_buttons')		
                    Button6.style.visibility = 'hidden';
					
					
				 }
			 }
			 
	/*
    * End Code 
    * Status : Submitted 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/
	//------------------------------------------------------------------------------------	
    /*
    * Begin Code 
    * Status : Submitted 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : N 
	*/ 
	
	if(role == 1093)
			 {
			 	
				 if (Status == 2) 	
				 {
					
					var button1 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button1.style.visibility = "hidden";
					
					
				 }
			 }
	
	/*
    * End Code 
    * Status : Submitted 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : N 
	*/  
	//--------------------------------------------------------------------------------------------------
	/*
    * Begin Code 
    * Status : Approved 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/
        if(role == 1089)
			 {
			 	
				 if (Status == 7) 	
				 {
				 	var button= window.document.getElementById('tbl__cancel')
				 	button.style.visibility = "hidden";
					
					var button2 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button2.style.visibility = "hidden";
					
					
					var button3 = window.document.getElementById('tbl_newrec83')		
				    button3.style.visibility = 'hidden';
					
					var button4 = window.document.getElementById('tbl_newrec85')		
				    button4.style.visibility = 'hidden';
					
					//var Button5 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_addedit')		
				    //Button5.style.visibility = 'hidden';
					
					var Button6 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_buttons')		
                    Button6.style.visibility = 'hidden';
					
					
				 }
			 }     
			 
	/*
    * End Code 
    * Status : Approved 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/
	//-------------------------------------------------------------------------------------------------
	 /*
    * Begin Code 
    * Status : Approved 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y 
	*/ 
	
	if(role == 1093)
			 {
			 	
				 if (Status == 7) 	
				 {
				 						
					var button = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button.style.visibility = "hidden";
					
					
				 }
			 }
	
	/*
    * End Code 
    * Status : Submitted 
    * Role   : Queing Staff.
    * Add    : Y 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y 
	*/  
	//--------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------
	/*
    * Begin Code 
    * Status : Exported 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/
        if(role == 1089)
			 {
			 	
				 if (Status == 8) 	
				 {
				 	var button = window.document.getElementById('tbl__cancel')
				 	button.style.visibility = "hidden";
					
					var button2 = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button2.style.visibility = "hidden";
					
					var button3 = window.document.getElementById('tbl_newrec83')		
				    button3.style.visibility = 'hidden';
					
					var button4 = window.document.getElementById('tbl_newrec85')		
				    button4.style.visibility = 'hidden';
					
					//var Button5 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_addedit')		
				    //Button5.style.visibility = 'hidden';
					
					var Button6 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_buttons')		
                    Button6.style.visibility = 'hidden';
					
					
					
				 }
			 }     
			 
	/*
    * End Code 
    * Status : Approved 
    * Role   : Tv Sales Rep v.3
    * Add    : N 
    * Edit   : N 
    * Delete : N 
    * Cancel : N 
	*/
	//-------------------------------------------------------------------------------------------------
	 /*
    * Begin Code 
    * Status : Exported 
    * Role   : Queing Staff.
    * Add    : N 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y 
	*/ 
	
	if(role == 1093)
			 {
			 	
				 if (Status == 8) 	
				 {
				 						
					var button = window.document.getElementById('spn_ACTIONMENU_d1')
				 	button.style.visibility = "hidden";
					
					var button3 = window.document.getElementById('tbl_newrec83')		
				    button3.style.visibility = 'hidden';
					
					var button4 = window.document.getElementById('tbl_newrec85')		
				    button4.style.visibility = 'hidden';
					
					//var Button5 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_addedit')		
				    //Button5.style.visibility = 'hidden';
					
					var Button6 =  window.document.getElementById('recmachcustrecord_mp2_mp1ref_buttons')		
                    Button6.style.visibility = 'hidden';
					
					
				 }
			 }
	
	/*
    * End Code 
    * Status : Submitted 
    * Role   : Queing Staff.
    * Add    : N 
    * Edit   : Y 
    * Delete : N 
    * Cancel : Y 
	*/ 
	//--------------------------------------------------------------------------------------------------	
		
    }
    catch (ex) {
        nlapiLogExecution('debug', 'test', ex);
    }
   
   /*
    * Status : Submitted 
    * Role   : Sales Rep.
    * Add    : N 
    * Delete : N 
    * Edit   : N 
    * Cancel : N 
	*/
  
  
  
  
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
    
   
   
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
/*
	if(name = 'custrecord_mp1_status')
	{
	var Status = nlapiGetFieldValue('custrecord_mp1_status')	
	//alert('Status='+Status)
	
	if(Status == 1 ||Status == 3 || Status == 4)
	{
		alert('You are not allowed to select this status')
		
		nlapiSetFieldValue('custrecord_mp1_status','')
	}
	
	
	
	
	
	
	}
*/
	
	
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================