// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI Media Plan Set SO Values
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...
    var RecordType ;

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInitSOSet(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
	
	
	//function HideCancelButton(type, form){

	
	///End code cancel
  
	
	var Flag = 0;	
	var P = 1;
	RecordType = type;	
	
		
	 /*
	 * Begin Code : For setting The Item , Month , Year , quota & Tv program unit disable
	 */	
	
	nlapiDisableLineItemField('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',true)
	nlapiDisableLineItemField('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',true)
	nlapiDisableLineItemField('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',true)
	nlapiDisableLineItemField('recmachcustrecord_mp3_mp1ref','custrecord_mp3_spotquota',true)
	nlapiDisableLineItemField('recmachcustrecord_mp3_mp1ref','custrecordcustrecord_mp3_tvprogramunit',true)
	nlapiDisableLineItemField('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration_old',true)
	nlapiDisableLineItemField('recmachcustrecord_mp2_mp1ref','custrecord_mp3_tvprgqm_ref',true)
	
	/*
	 * Begin Code : For setting The Item , Month , Year , quota & Tv program unit disable
	 */	
		
		if(type == 'create')
		{
		//Begin Code : for getting sales order from the url
		var urlStr=window.location.href.toLowerCase();
		if((urlStr.indexOf("?")) > -1)
		{
		var parameterList = urlStr.substr((urlStr.indexOf("?")+1), urlStr.length);
		var paramArray=parameterList.split("&");
		
		
		if(paramArray != undefined && paramArray != null && paramArray != '')
		{
			//alert('paramArray length='+paramArray.length)
			
			for(var i= 0; i< paramArray.length ; i++)
			{
			//alert('paramArray []='+ paramArray[i])	
			var Temp = paramArray[i].split("=");
			
			
				if(Temp[0] == 'custrecord_mp1_soref')
				{
				Flag = 1
				break;
				
				}
			
			}
			
			//Begin Code : for setting the Sales order values on the Media Plan Section 1 Record.
			
			if(Flag == 1)
			{
				   
				    if(Temp[1] != null && Temp[1] != undefined && Temp[1] != '' )
					{
					var SOOBj = nlapiLoadRecord('salesorder',Temp[1])
			
					var Customer = SOOBj.getFieldValue('entity')
					nlapiLogExecution('DEBUG','ON SALES ORDER','Customer=>' +Customer)
					
					var Customeraddress = SOOBj.getFieldValue('billaddress')
					nlapiLogExecution('DEBUG','ON SALES ORDER','Customeraddress=>' +Customeraddress)
					
					var OnAirStartDate = SOOBj.getFieldValue('startdate')
					nlapiLogExecution('DEBUG','ON SALES ORDER','OnAirStartDate=>' +OnAirStartDate)
					
					var OnAirEndDate = SOOBj.getFieldValue('enddate')
					nlapiLogExecution('DEBUG','ON SALES ORDER','OnAirEndDate=>' +OnAirEndDate)
					
					var SalesRep = SOOBj.getFieldValue('salesrep')
					nlapiLogExecution('DEBUG','ON SALES ORDER','SalesRep=>' +SalesRep)
					
					
			
	/*

				    Customeraddress = Customeraddress.toString();
					nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address.toString()= " + Customeraddress);
					Customeraddress = Customeraddress.replace(/\n/g,"<br\/>");/// /g
					nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address After Replcing spaces with % =====" + Customeraddress);
									
*/
									
					
					
					var current_user=nlapiGetUser();
					
						
						if(current_user!=null && current_user!='' && current_user!=undefined)
						{
							
							var userOBJ=nlapiLoadRecord('employee',current_user);
							
							
							if(userOBJ!=null && userOBJ!='')
							{
								
								var fisrt_name=userOBJ.getFieldValue('firstname');
								
								
								var last_name=userOBJ.getFieldValue('lastname');
								
								
								var sales_rep_check=userOBJ.getFieldValue('issalesrep');
								
								if(sales_rep_check == 'T')
								{
									
									
									SalesRep = current_user;
									nlapiSetFieldValue('custrecord_mp1_salesrep', SalesRep,true);
													
									
									
								}
								
								
							}//User OBJ 							
							
						}//Current User
					
					
					nlapiSetFieldValue('custrecord_mp1_customer',Customer)	
					nlapiSetFieldValue('custrecord_mp1_custaddress',Customeraddress)	
					nlapiSetFieldValue('custrecord_mp1_oa_startdate', OnAirStartDate)	
					nlapiSetFieldValue('custrecord_mp1_oa_enddate',OnAirEndDate)	
					nlapiSetFieldValue('custrecord_mp1_soref',Temp[1],true)	
					
					
					}
			
			// Begin Code : For setting Media Plan Section 3 - Media Schedule Values.
			
			// Begin Code : For Calculating Month diff
			var StartDate = OnAirStartDate.split('/')
			var StartDay =StartDate[0]
			var StartMonth = StartDate[1]
			var StartYear = StartDate[2]
			nlapiLogExecution('DEBUG','ON SALES ORDER','StartDay=>' +StartDay+'StartMonth=>'+StartMonth+'StartYear='+StartYear)
			
			var EndDate = OnAirEndDate.split('/')
			var EndDay =EndDate[0]
			var EndMonth = EndDate[1]
			var EndYear = EndDate[2]
			nlapiLogExecution('DEBUG','ON SALES ORDER','EndDay=>' +EndDay+'EndMonth=>'+EndMonth+'EndYear='+EndYear)
			
			var TempMonth = StartMonth;
			var TempYear = StartYear;
			
					var Monthdifference=0;
					
					if( parseInt(StartMonth)  > parseInt(EndMonth) )
					{
					var Temp = parseInt(12)- parseInt(StartMonth)
					var EndTemp = parseInt(Temp)+1;
					Monthdifference= parseInt(EndTemp)+parseInt(EndMonth)
					nlapiLogExecution('DEBUG','ON SALES ORDER','Monthdifference if =>' +Monthdifference)
					}
					else
					{
				    Monthdifference =  parseInt(EndMonth)- parseInt(StartMonth)
					var Monthdifference = parseInt(Monthdifference)+1;
					nlapiLogExecution('DEBUG','ON SALES ORDER','Monthdifference Else =>' +Monthdifference)
					}
			
			// End Code : For Calculating Month diff
		
		    var SOItemCount = SOOBj.getLineItemCount('item')
			
		    
			if(StartMonth != EndMonth)
			{
				for(var j= 1 ; j<= SOItemCount ; j++)
				{
				var ProgramItem = SOOBj.getLineItemValue('item','item',j)
				var TempMonth = StartMonth;
			    var TempYear = StartYear;
				
					for(var k = 1 ; k <= Monthdifference ; k++)
					{
						nlapiSelectLineItem('recmachcustrecord_mp3_mp1ref',P)
						nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',ProgramItem)
						nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',TempMonth)
						nlapiSetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',TempYear)
						nlapiCommitLineItem('recmachcustrecord_mp3_mp1ref')
						
						P++;
						TempMonth++;
						if(TempMonth > 12)
						{
						TempYear++;
						TempMonth = 1;	
						}
						
					}
				
				
				}
				
			}
			else
			{
				
				var TempMonth = StartMonth;
			    var TempYear = StartYear;
				for(var j= 1 ; j<= SOItemCount ; j++)
				{
				
				var ProgramItem = SOOBj.getLineItemValue('item','item',j)
				nlapiSelectLineItem('recmachcustrecord_mp3_mp1ref',j)
				nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',ProgramItem)
				nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',TempMonth)
				nlapiSetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',TempYear)
				nlapiCommitLineItem('recmachcustrecord_mp3_mp1ref')
				
				
				}
			
			}
			
		    // Begin Code : For setting Media Plan Section 3 Values.
			
			}
		//End Code : for setting the Sales order values on the Media Plan Section 1 Record.	
		
		
		//Begin Code for setting status on the status field of the media plan 1 record.
		
		nlapiSetFieldValue('custrecord_mp1_status',1)
		//End Code for setting status on the status field of the media plan 1 record.
		}
		
		}
		else
		{
		//No query string in the url so we set return value as blank
		returnValue="";
		}

		
	//End Code : for getting sales order from the url		
		}
		
		
  
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecordSetValue()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY

	
	
	
	
	
	
	
	
	
	
	
	
    
   // Begin code : for setting the rejected & partial status .
   
   var MediaScheduleLineCount = nlapiGetLineItemCount('recmachcustrecord_mp3_mp1ref')
   
   var RejectedAllStatus = 0;
   
   
   for(var i= 1 ; i <= MediaScheduleLineCount ; i++)
   {
	   	var mediaschedulestatus = nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',i)
		if(mediaschedulestatus == 3 )
		{
		RejectedAllStatus = RejectedAllStatus +1;	
		}
   }
   
  
   
   if(MediaScheduleLineCount == RejectedAllStatus )
   {
   	nlapiSetFieldValue('custrecord_mp1_status',3)
   }
   
    if(RejectedAllStatus > 0 && RejectedAllStatus < MediaScheduleLineCount )
   {
   	nlapiSetFieldValue('custrecord_mp1_status',4)
   }
   
   // End code :  for setting the rejected & partial status .
   
   
   var Mp1Status_new = nlapiGetFieldValue('custrecord_mp1_status')
  // alert('Mp1Status_new =='+Mp1Status_new)
   
   
   
   var media_id=nlapiGetRecordId();
   
   
   
   
   
   	 var Filters = new Array();
     var Coloumns = new Array();
  
  
	  Filters[0]= new nlobjSearchFilter('custrecord_mp3_mp1ref',null,'is', media_id)
	 
	  
	  Coloumns[0] =  new nlobjSearchColumn('internalid')
	  
	   var searchresult_ms = nlapiSearchRecord('customrecord_mediaplan_3',null,Filters,Coloumns)
				
				
		 if (searchresult_ms != null && searchresult_ms != undefined && searchresult_ms != '')
		 {
		 	
			if(Mp1Status_new==5)
			{
				for (var j = 0; j < searchresult_ms.length; j++) 
				{
						var mediaschedule_record_Id = searchresult_ms[j].getValue('internalid')
			            nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule ID -->'+mediaschedule_record_Id);
			            
						 if (mediaschedule_record_Id != null && mediaschedule_record_Id != undefined && mediaschedule_record_Id != '')
						 {
						 	var md_obj=nlapiLoadRecord('customrecord_mediaplan_3',mediaschedule_record_Id);
							
							 md_obj.setFieldValue('custrecord_mp3_ms_status',5);
							 nlapiSubmitRecord(md_obj,true,true)
							
						 }
					
				}
				
				
				
			}
		 	if(Mp1Status_new==2)
			{
				for (var j = 0; j < searchresult_ms.length; j++) 
				{
						var mediaschedule_record_Id = searchresult_ms[j].getValue('internalid')
			            nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule ID -->'+mediaschedule_record_Id);
			            
						 if (mediaschedule_record_Id != null && mediaschedule_record_Id != undefined && mediaschedule_record_Id != '')
						 {
						 	var md_obj=nlapiLoadRecord('customrecord_mediaplan_3',mediaschedule_record_Id);
							
							 md_obj.setFieldValue('custrecord_mp3_ms_status',2);
							 nlapiSubmitRecord(md_obj,true,true)
							
						 }
					
				}
				
				
				
			}
			
			
			if(Mp1Status_new==6)
			{
				for (var j = 0; j < searchresult_ms.length; j++) 
				{
						var mediaschedule_record_Id = searchresult_ms[j].getValue('internalid')
			            nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media Schedule ID -->'+mediaschedule_record_Id);
			            
						 if (mediaschedule_record_Id != null && mediaschedule_record_Id != undefined && mediaschedule_record_Id != '')
						 {
						 	var md_obj=nlapiLoadRecord('customrecord_mediaplan_3',mediaschedule_record_Id);
							
							 md_obj.setFieldValue('custrecord_mp3_ms_status',6);
							 nlapiSubmitRecord(md_obj,true,true)
							
						 }
					
				}
				
				
				
			}
				
		 	
		
		 	
		 }
				
	 
	 //customrecord_mediaplan_3
   
   
   
 
 // Begin code : for setting the cancelled by bec & Cancelled by client status & Submitted.
   
   
   /*
for(var y = 1 ; y <= MediaScheduleLineCount ; y++)
   {
   	 var media_sch_id=nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','id',y)
	
*/
	
	 /*
if(Mp1Status_new == 5)
	 {
	 	 alert(' set 1.........')
	 	 nlapiSetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',y,5)   
	 }
*/
	
	 /*
     else if(Mp1Status_new == 6)
	 {
	 	 alert(' set. 2 ........')
	 	 nlapiSetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',y,6)   
	 }
	 else if(Mp1Status_new == 2)
	 {
	 	 alert(' set.........')
	 	 nlapiSetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',y,2)   
	 }
    */
	 
	
   //}
 
   
   // Begin code : for setting the cancelled by bec & Cancelled by client status .
   
   
    for(var k= 1 ; k <= MediaScheduleLineCount ; k++)
   {
   	
	
	    var Month = nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',k)
		var Year = nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',k)
		var Item = nlapiGetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',k)
		
		
		
		var Month_text = nlapiGetLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',k)
		var Year_text = nlapiGetLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',k)
		var Item_text = nlapiGetLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',k)
		
			
		
		
		 var MP3_Filters = new Array();
	     var MP3_Coloumns = new Array();
	  
	  
		  MP3_Filters[0]= new nlobjSearchFilter('custrecord_adinv_programitem',null,'is', Item)
		  MP3_Filters[1]= new nlobjSearchFilter('custrecord_adinv_month',null,'is',Month)
		  MP3_Filters[2]= new nlobjSearchFilter('custrecord_adinv_year',null,'is',Year)
		  
		  MP3_Coloumns[0] =  new nlobjSearchColumn('internalid')
		  MP3_Coloumns[1] =  new nlobjSearchColumn('custrecord_bec_im_duration')
		  MP3_Coloumns[2] =  new nlobjSearchColumn('custrecord_adinv_unitofmeasure')
		  
		  var searchresult = nlapiSearchRecord('customrecord_ad_inventory',null,MP3_Filters,MP3_Coloumns)
		  
		  
			  if(searchresult != null && searchresult != undefined && searchresult != '')
			   {
			     return true;
			
			  }
			    else
				{
			//	nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_spotquota',0,true);
				alert(' TV Program Quota Management Record does not exists for '+Item_text+' for '+Month_text+' '+Year_text);
			//	alert(' TV Program Quota Management Record does not exists');
				
				return false;
				}
		
				
		
   }
   
   
  
   
   return true;
   
	

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateFieldSetValue(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY
	
	
	
	
	 
		
	
	
	
	
	//Begin Code : To avoid duplicate alias Entries.
     if(name =='custrecord_mp2_alias' && type =='recmachcustrecord_mp2_mp1ref')
	{
		
		var S_Alias =  nlapiGetCurrentLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias')
		//alert('S_Alias='+S_Alias)
		
		var I_MediaListLineCount = nlapiGetLineItemCount('recmachcustrecord_mp2_mp1ref')
		//salert('I_MediaListLineCount='+I_MediaListLineCount)
		
		var A_AliasList = new Array();
		var Temp=0;
		
		for(var k= 1 ; k <= I_MediaListLineCount ; k++)
		{
			A_AliasList[Temp]= nlapiGetLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',k)
			Temp++;
	    }
		
		
		
		for(var j= 0 ; j <= A_AliasList.length ; j++)
		{
			if(A_AliasList[j] == S_Alias)
			{
				alert('This Alias Is Allready Existing')
				nlapiCancelLineItem('recmachcustrecord_mp2_mp1ref')
				break;
			}
	    }
		
		
	}
	//End Code : To avoid duplicate alias Entries.
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//Begin Code: For 29 th Feb  validation on days.
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_29')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day29 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_29')
		
		if(Day29 != null && Day29 != undefined && Day29 != '')
		{
			if(Month == 'February')
			{
				if(Year != null && Year != '' && Year != undefined)
				{
					var Temp = Year % 2	
					
					if(Temp != 0)
					{
						alert('Can Not Add Item For The Day')
					    nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_29','', false)
					}
				}
			}
			
		}
	}
	//End Code: For  29 th Feb  validation on days.
	
	
	//Begin Code: For 30 th Feb  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_30')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day30 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_30')
		
		if(Day30 != null && Day30 != undefined && Day30 != '')
		{
			if(Month == 'February')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_30','', false)
			}
			
		}
	}
	//End Code: For  30 th Feb  validation .
	
	
	//Begin Code: For 31 th Feb  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_31')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day31 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31')
		
		if(Day31 != null && Day31 != undefined && Day31 != '')
		{
			if(Month == 'February')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31','', false)
			}
			
		}
	}
	//End Code: For  31 th Feb  validation .
	
	
	
	//Begin Code: For 31 th Apr  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_31')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day31 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31')
		
		if(Day31 != null && Day31 != undefined && Day31 != '')
		{
			if(Month == 'April')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31','', false)
			}
			
		}
	}
	//End Code: For  31 th Apr  validation .
	
	//Begin Code: For 31 th June  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_31')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day31 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31')
		
		if(Day31 != null && Day31 != undefined && Day31 != '')
		{
			if(Month == 'June')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31','', false)
			}
			
		}
	}
	//End Code: For  31 th June  validation .
	
	
	//Begin Code: For 31 th September  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_31')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day31 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31')
		
		if(Day31 != null && Day31 != undefined && Day31 != '')
		{
			if(Month == 'September')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31','', false)
			}
			
		}
	}
	//End Code: For  31 th September  validation .
	
	//Begin Code: For 31 th November  validation .
	if(type == 'recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_31')
	{
		var Month = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Day31 = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31')
		
		if(Day31 != null && Day31 != undefined && Day31 != '')
		{
			if(Month == 'November')
			{
			  alert('Can Not Add Item For The Day')
			  nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31','', false)
			}
			
		}
	}
	//End Code: For  31 th September  validation .
	
	
	//Begin Code : To Copy new changed duration in to the new Duration.
	
	
	if(type == 'recmachcustrecord_mp2_mp1ref' && name == 'custrecord_mp2_duration')
	{
	var NewDuration = nlapiGetCurrentLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration')
	
	
	var LineIndex = nlapiGetCurrentLineItemIndex('recmachcustrecord_mp2_mp1ref')
	
	
	var OldDuration = nlapiGetLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration',LineIndex)
	
	
		
		if(OldDuration  != null && OldDuration  != undefined && OldDuration  != '')
		{
		
		
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration_old',OldDuration)
		}
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// End Code :  To Copy new changed duration in to the new Duration.

	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChangedSetValue(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	
	
	
	/*
if( name == 'custrecord_mp1_status')
	{
		
		
		
   var MediaScheduleLineCount = nlapiGetLineItemCount('recmachcustrecord_mp3_mp1ref')
  
	
	
  var Mp1Status = nlapiGetFieldValue('custrecord_mp1_status')
 
   
   for(var i= 1 ; i <= MediaScheduleLineCount ; i++)
   {
	 
	 if(Mp1Status == 2)
	 {
	 
		
	 	nlapiSetLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',i,2)   
	 }
	 
	
   }
	
		
		
	}
	
	
*/
	
	
	
	
	
	
	
	// Begin Code : for setting the Duration for the tv item from Tv Program Inventory record.
	if(type =='recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_year') 
	{
		var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
		var Line= nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref')
		
		
		var Month_text = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year_text = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Item_text = nlapiGetCurrentLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
		
		
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Line'+Line)
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Month'+Month)
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Year'+Year)
		nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Item'+Item)
		
		
		
		
		if(Item!=null && Item!='' && Item!=undefined && Item!=' ' )
		{
			if(Month!=null && Month!='' && Month!=undefined && Month!=' ' )
			{
				if(Year!=null && Year!='' && Year!=undefined && Year!=' ' )
				{
					
					
		 var MP3_Filters = new Array();
	     var MP3_Coloumns = new Array();
	  
	  
		  MP3_Filters[0]= new nlobjSearchFilter('custrecord_adinv_programitem',null,'is', Item)
		  MP3_Filters[1]= new nlobjSearchFilter('custrecord_adinv_month',null,'is',Month)
		  MP3_Filters[2]= new nlobjSearchFilter('custrecord_adinv_year',null,'is',Year)
		  
		  MP3_Coloumns[0] =  new nlobjSearchColumn('internalid')
		  MP3_Coloumns[1] =  new nlobjSearchColumn('custrecord_bec_im_duration')
		  MP3_Coloumns[2] =  new nlobjSearchColumn('custrecord_adinv_unitofmeasure')
		  
		  var searchresult = nlapiSearchRecord('customrecord_ad_inventory',null,MP3_Filters,MP3_Coloumns)
		  
		  
			  if(searchresult != null && searchresult != undefined && searchresult != '')
			  {
			  	var AdInvetoryManagemntId = searchresult[0].getValue('internalid')
			    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','AdInvetoryManagemntId'+AdInvetoryManagemntId)
				
				var Duration = searchresult[0].getValue('custrecord_bec_im_duration')
			    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Duration='+Duration)
				
							
				var media_unit = searchresult[0].getValue('custrecord_adinv_unitofmeasure')
			    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','media_unit='+media_unit)
				
					if(Duration != undefined && Duration != null && Duration !='')
					{
					nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_spotquota',Duration, true)
					}
					
					if(media_unit != undefined && media_unit != null && media_unit !='')
					{
					nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecordcustrecord_mp3_tvprogramunit',media_unit, true)
					}
					if(AdInvetoryManagemntId != undefined && AdInvetoryManagemntId != null && AdInvetoryManagemntId !='')
					{
					nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprgqm_ref',AdInvetoryManagemntId, true)
					}
					
			    }
			    else
				{
				nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_spotquota',0,true);
				alert(' TV Program Quota Management Record does not exists for '+Item_text+' for '+Month_text+' '+Year_text);
				
				}
		
					
					
					
				}
				
				
			}
			
			
		}
		
		
		
		
	
	}
	// Begin Code : for setting the Duration for the tv item from Tv Program Inventory record.
	
	// Begin Code : For setting Status open on page load.
	
		
        if(RecordType == 'create')
		{
			if (type =='recmachcustrecord_mp3_mp1ref' && name == 'custrecord_mp3_year')
			{
				nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',1,true)
			}
		}
	

	
	// End Code : For setting Status open on page load.
	   
	
	
	//Begin Code : For Setting Items on Media Plan depending on TV Program Inventory Record.
	   // ===========================For Day 1===============================================
	   if(name == 'custrecord_mp3_1' && type == 'recmachcustrecord_mp3_mp1ref' )
	   {
	   	var FieldId ='custrecord_mp3_1'
		var InventoryDuration = 'custrecord_adinv_oh_1'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 2===============================================
		if(name == 'custrecord_mp3_2' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_2'
		var InventoryDuration = 'custrecord_adinv_oh_2'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 3===============================================
		if(name == 'custrecord_mp3_3' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_3'
		var InventoryDuration = 'custrecord_adinv_oh_3'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 4===============================================
		if(name == 'custrecord_mp3_4' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_4'
		var InventoryDuration ='custrecord_adinv_oh_4'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 5===============================================
		if(name == 'custrecord_mp3_5' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_5'
		var InventoryDuration ='custrecord_adinv_oh_5'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 6===============================================
		if(name == 'custrecord_mp3_6' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_6'
		var InventoryDuration ='custrecord_adinv_oh_6'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 7 ===============================================
		if(name == 'custrecord_mp3_7' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_7'
		var InventoryDuration ='custrecord_adinv_oh_7'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 8===============================================
		if(name == 'custrecord_mp3_8' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_8'
		var InventoryDuration ='custrecord_adinv_oh_8'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 9 ===============================================
		if(name == 'custrecord_mp3_9' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_9'
		var InventoryDuration ='custrecord_adinv_oh_9'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 10===============================================
		if(name == 'custrecord_mp3_10' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_10'
		var InventoryDuration ='custrecord_adinv_oh_10'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 11 ===============================================
		if(name == 'custrecord_mp3_11' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_11'
		var InventoryDuration ='custrecord_adinv_oh_11'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 12===============================================
		if(name == 'custrecord_mp3_12' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_12'
		var InventoryDuration ='custrecord_adinv_oh_12'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 13 ===============================================
		if(name == 'custrecord_mp3_13' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_13'
		var InventoryDuration ='custrecord_adinv_oh_13'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 14 ===============================================
		if(name == 'custrecord_mp3_14' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_14'
		var InventoryDuration ='custrecord_adinv_oh_14'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 15===============================================
		if(name == 'custrecord_mp3_15' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_15'
		var InventoryDuration ='custrecord_adinv_oh_15'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 16 ===============================================
		if(name == 'custrecord_mp3_16' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_16'
		var InventoryDuration ='custrecord_adinv_oh_16'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 17===============================================
		if(name == 'custrecord_mp3_17' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_17'
		var InventoryDuration ='custrecord_adinv_oh_17'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 18===============================================
		if(name == 'custrecord_mp3_18' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_18'
		var InventoryDuration ='custrecord_adinv_oh_18'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 19===============================================
		if(name == 'custrecord_mp3_19' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_19'
		var InventoryDuration ='custrecord_adinv_oh_19'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 20 ===============================================
		if(name == 'custrecord_mp3_20' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_20'
		var InventoryDuration ='custrecord_adinv_oh_20'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 21===============================================
		if(name == 'custrecord_mp3_21' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_21'
		var InventoryDuration ='custrecord_adinv_oh_21'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 22===============================================
		if(name == 'custrecord_mp3_22' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_22'
		var InventoryDuration ='custrecord_adinv_oh_22'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 23===============================================
		if(name == 'custrecord_mp3_23' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_23'
		var InventoryDuration ='custrecord_adinv_oh_23'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 24===============================================
		if(name == 'custrecord_mp3_24' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_24'
		var InventoryDuration ='custrecord_adinv_oh_24'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 25 ===============================================
		if(name == 'custrecord_mp3_25' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_25'
		var InventoryDuration ='custrecord_adinv_oh_25'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 26===============================================
		if(name == 'custrecord_mp3_26' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_26'
		var InventoryDuration ='custrecord_adinv_oh_26'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 27===============================================
		if(name == 'custrecord_mp3_27' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_27'
		var InventoryDuration ='custrecord_adinv_oh_27'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 28 ===============================================
		if(name == 'custrecord_mp3_28' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_28'
		var InventoryDuration ='custrecord_adinv_oh_28'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 29 ===============================================
		if(name == 'custrecord_mp3_29' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_29'
		var InventoryDuration ='custrecord_adinv_oh_29'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 30===============================================
		if(name == 'custrecord_mp3_30' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_30'
		var InventoryDuration ='custrecord_adinv_oh_30'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		// ===========================For Day 31===============================================
		if(name == 'custrecord_mp3_31' && type == 'recmachcustrecord_mp3_mp1ref' )
	    {
	   	var FieldId ='custrecord_mp3_31'
		var InventoryDuration ='custrecord_adinv_oh_31'
		ForSettingDayItemAsQuota(FieldId,InventoryDuration)
	    }
		
	   
	   
	//End Code : For Setting Items on Media Plan depending on TV Program Inventory Record.
	
	
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_item_values(type)
{
	   
     if(type=='recmachcustrecord_mp2_mp1ref')
	  {
	  	var media_listtype=nlapiGetCurrentLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type')
		
				
		var new_duration=nlapiGetCurrentLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration')
		
		if(media_listtype==1 && new_duration=='')
		{
			alert(' Please enter New Duration.');
			return false;
			
		}
		
		
		
	  }
 return true;
    	
}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================


function getUrlParams() 	
   {
  var params = {};
  window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
    params[key] = value;
  });
 
  return params;
}

 function MinutesToSecondConversion(Minutes) 	
 {
	var Seconds;
	Seconds = (parseInt(Minutes)* parseInt(60)).toFixed(3)
	return (Seconds);	
 }
 
 
 function SecondsToMinuteConversion(Seconds) 	
 {
	var Minutes;
	Minutes = (parseInt (Seconds)/ parseInt(60)).toFixed(3)
	return (Minutes);	
 }		
 
 function SecondsToDecimalConversion(Seconds)	
 {
 	var Decimal;
	Decimal = (parseInt (Seconds)/ parseInt(3600)).toFixed(3)
	return (Decimal);

 }	
 
  function MinuteSecondToSecondConversion(Minutes) 	
 {      var parts = Minutes.split(':'),
        minutes = +parts[0],
        seconds = +parts[1];
        return ((parseInt(minutes) * parseInt( 60))  +parseInt(seconds) ).toFixed(3);
 }
 function ForSettingDayItemAsQuota(FieldId,InventoryDuration) 
{
       
		var Month = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month')
		var Year = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year')
		var Item = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram')
		var ItemValue = nlapiGetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',FieldId)
		
		
		if (Item != null && Item != '' && Item != undefined && Item != ' ') 
		{
			if (Month != null && Month != '' && Month != undefined && Month != ' ') 
			{
				if (Year != null && Year != '' && Year != undefined && Year != ' ') 
				{
					
					
					
				
		
		 var MP3_Filters = new Array();
	     var MP3_Coloumns = new Array();
	  
	  
		  MP3_Filters[0]= new nlobjSearchFilter('custrecord_adinv_programitem',null,'is', Item)
		  MP3_Filters[1]= new nlobjSearchFilter('custrecord_adinv_month',null,'is',Month)
		  MP3_Filters[2]= new nlobjSearchFilter('custrecord_adinv_year',null,'is',Year)
		  
		 // alert('InventoryDuration='+InventoryDuration)
		  MP3_Coloumns[0] =  new nlobjSearchColumn('internalid')
		  MP3_Coloumns[1] =  new nlobjSearchColumn('custrecord_bec_im_duration')
		  MP3_Coloumns[2] =  new nlobjSearchColumn(InventoryDuration)
		  
		  var searchresult = nlapiSearchRecord('customrecord_ad_inventory',null,MP3_Filters,MP3_Coloumns)
		  
		  
			  if(searchresult != null && searchresult != undefined && searchresult != '')
			  {
			  	var AdInvetoryManagemntId = searchresult[0].getValue('internalid')
			    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','AdInvetoryManagemntId'+AdInvetoryManagemntId)
				//alert('AdInvetoryManagemntId='+AdInvetoryManagemntId)
				
				var Duration = searchresult[0].getValue(InventoryDuration)
			    nlapiLogExecution('DEBUG','BEFORE SUBMIT ADINVENTORYRECORD','Duration='+Duration)
				//alert('Duration='+Duration)
				
				
					if(Duration != undefined && Duration != null && Duration !='' && Duration != 0)
					{
					nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',FieldId,ItemValue, false)
					}
					else
					{
					    
						 alert('Tv program Quota Is Not Available For The day')
						 nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',FieldId,'',false)
					}
					
			    }
			    else
				{
				     
					 //begin code for setting the total duration :
					 alert('Tv program Quota Record Is Not Available For The Given Duration')
					 nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref',FieldId,'',false)
					 //End code for setting the total duration :
					 
				}	
	
					
					
					
					
					
					
					
					
					
					
				}
			}
		}
		
		

}
 
       //var seconds = MinutesToSecondConversion(3)
	   //alert('3 Minutes to Sec ='+seconds)
	   
	   // var Minutes = SecondsToMinuteConversion(145)
	   //alert(' 145 Sec to Min ='+Minutes)
	   
	    // var DEcimal= SecondsToDecimalConversion(60)
        //alert(' 60 Sec To Decimal ='+DEcimal)
	   
	    //var minute='3:15'
		//var seconds= MinuteSecondToSecondConversion(minute)
		//alert(' 3:15 Minutes to seconds='+ seconds)

// END FUNCTION =====================================================
