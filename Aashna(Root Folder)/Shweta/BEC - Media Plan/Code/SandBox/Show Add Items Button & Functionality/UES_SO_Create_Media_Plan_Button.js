// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_SO_Create_Media_Plan_Button.js
	Author:Rakesh Malvade	
	Company:Proquest solutions
	Date:07-03-2013
	Description:before Load On media plan Dispaly add button.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

    05-03-2013           Rakesh Malvade					     Supriya			before Load On media plan Dispaly add button.  	

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_addbutton(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
	

	  
		
		
		if(type=='edit')
		{
			
			var MediaplanID=nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'Record ID-->'+MediaplanID);
			form.addButton('custpage_Button_add','Add','CreateMediaPlan('+MediaplanID+')');
			
			
		}
		
		
		nlapiLogExecution('DEBUG', 'beforeLoadRecord' ,'Button Added Successfully');



	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	
	
	
		return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_SR_quota_management(type)
{
   
   var media_id=nlapiGetRecordId();
   var media_type=nlapiGetRecordType();
   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','media_id .............'+media_id)
   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','media_type.............'+media_type)
   
   if(media_id!=null && media_id!='' && media_id!=undefined)
   {
   	 if(media_type!=null && media_type!='' && media_type!=undefined)
	 {
	 	 var mediaOBJ=nlapiLoadRecord(media_type,media_id);
		  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','mediaOBJ.............'+mediaOBJ)
		 
		 
		 
		 if(mediaOBJ!=null )
		 {
		 	var MediaScheduleLineCount = mediaOBJ.getLineItemCount('recmachcustrecord_mp3_mp1ref')
		 
		 if(MediaScheduleLineCount!=null && MediaScheduleLineCount!='' && MediaScheduleLineCount!=undefined)
		 {
		 	 var Mp1Status = mediaOBJ.getFieldValue('custrecord_mp1_status')
		  nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Mp1Status.............'+Mp1Status)
		   
		   for(var i= 1 ; i <= MediaScheduleLineCount ; i++)
		   {
			 if(Mp1Status == 2)
			 {
			 	 mediaOBJ.setLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_ms_status',i,2)   
			 }
			   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','line set.............')
			
			
		   }
			
		 }
		 
			
			
		 }
		 
		 
		 
		 
		 
		 
		 
		 
		
	 }
   	
	 
   	
	
   }
   
   
   
	
		
	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
