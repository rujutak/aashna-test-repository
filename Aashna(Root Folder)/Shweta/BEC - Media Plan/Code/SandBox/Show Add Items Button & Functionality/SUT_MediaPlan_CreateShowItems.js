// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

   
	 try
	 {

     if (request.getMethod() == 'GET') 	
	 {
	 	
		/*
        * Begin :Code for Creating add Items Button
		*/
		  var CurrentItemId = request.getParameter('custscript_MediaPlan');
	      nlapiLogExecution('DEBUG','IN SUITELET','CurrentItemId =='+CurrentItemId);
	 
	      var MediaCategoryLineNo = request.getParameter('custscript_CurrentLineItem');
	      nlapiLogExecution('DEBUG','IN SUITELET','MediaCategoryLineNo =='+MediaCategoryLineNo); 
		  
		 
		  
		  
		  var MediaListValue = request.getParameter('custscript_MediaListArray');
	      nlapiLogExecution('DEBUG','IN SUITELET','MediaListValue =='+MediaListValue); 
		  
		  var S_NewALiasSpotname = MediaListValue.split(',')
		  nlapiLogExecution('DEBUG','IN SUITELET','S_NewALiasSpotname length =='+S_NewALiasSpotname.length);
		  
		  
		  nlapiLogExecution('DEBUG','IN SUITELET','S_NewALiasSpotname[0] =='+S_NewALiasSpotname[0]);
		  nlapiLogExecution('DEBUG','IN SUITELET','S_NewALiasSpotname[1] =='+S_NewALiasSpotname[1]);
		  
		 
		var f_form = nlapiCreateForm('Show Items');
		 f_form.setScript('customscript_mediaplan_showitemsvalidate');
		 
	 	var I_Day = f_form.addField('day', 'select', 'Day' ,'customlist_day_show_list');
		
		var SampleTab = f_form.addTab('custpage_sample_tab', 'Item');
		
		var ExpenseSubList = f_form.addSubList('custpage_expense_sublist', 'inlineeditor', 'Expense', 'custpage_sample_tab');
		
		var l_select = ExpenseSubList.addField('custpage_category', 'select', 'Category');
		
		
		/*
        * Begin :For creating virtual list of Media list "alias Spotname".
		*/
		
		var O_MediaPlan1Rec = nlapiLoadRecord('customrecord_mediaplan_1',CurrentItemId)
		nlapiLogExecution('DEBUG','IN SUITELET','O_MediaPlan1Rec =='+O_MediaPlan1Rec);
		
				
		var i_MediaListLineCount = O_MediaPlan1Rec.getLineItemCount('recmachcustrecord_mp2_mp1ref')
		nlapiLogExecution('DEBUG','IN SUITELET','i_MediaListLineCount =='+i_MediaListLineCount);
		
		l_select.addSelectOption('','');
		    
			/*
       		* Begin :For setting Alias & SpotName in the Category field
			*/
			
		   
				  for (var k=0; k< S_NewALiasSpotname.length; k++) 
				  {
			       S_String =  S_NewALiasSpotname[k]
				  
				   l_select.addSelectOption(S_String, S_String);	
				   nlapiLogExecution('DEBUG','IN SUITELET','S_String =='+S_String);
				  };
		  
		   /*
       		* End : For setting Alias & SpotName in the Category field
			*/
		
		
		/*
        * End :For creating virtual list of Media Schedules Item.
		*/
		
		ExpenseSubList.addField('custpage_quantity', 'text', 'Quantity');
		
		/*
        * End :Code for Creating add Items Button
		*/
		var checkBtn = f_form.addSubmitButton('Submit');
		response.writePage(f_form);
	 }
	 else if(request.getMethod() == 'POST')
	 {
	 	/*
        * Begin :Code to pick the day , category & quantity value.
		*/
		
		var I_DayNo = request.getParameter('day');
		nlapiLogExecution('DEBUG','IN SUITELET','I_DayNo =='+I_DayNo);
		
		var I_ExpenseLineCount = request.getLineItemCount('custpage_expense_sublist')
		nlapiLogExecution('DEBUG','IN SUITELET','I_ExpenseLineCount =='+I_ExpenseLineCount);
		
		/*
        * End :Code to pick the day , category & quantity value.
		*/
		
		/*
        * Begin :Code to Isolate the Alias & Spot name also to concatinate the Alias with Quantity.
		*/
		
		var S_FinalStringToprint='';
		
				for(var k = 1; k<= I_ExpenseLineCount; k++)
				{
				var S_AliasSpotName = request.getLineItemValue('custpage_expense_sublist','custpage_category',k)	
				nlapiLogExecution('DEBUG','IN SUITELET','S_AliasSpotName =='+S_AliasSpotName)
				
				
				var I_Quantity = request.getLineItemValue('custpage_expense_sublist','custpage_quantity',k)	
				nlapiLogExecution('DEBUG','IN SUITELET','I_Quantity =='+I_Quantity)
				
				
				var S_AliasSplit = S_AliasSpotName.split(' ')
				var S_Alias = S_AliasSplit[0]
				nlapiLogExecution('DEBUG','IN SUITELET','S_Alias =='+S_Alias)
				
				
				var S_FinalString = I_Quantity+S_Alias+','	
				
				S_FinalStringToprint=S_FinalStringToprint+S_FinalString
							
				}
		
		     
		     nlapiLogExecution('DEBUG','IN SUITELET','S_FinalStringToprint =='+S_FinalStringToprint);
			 
		/*
        * Begin :Code to Isolate the Alias & Spot name also to concatinate the Alias with Quantity.
		*/
			 
		/*
        * Begin :For creating the FinalString to print.
		*/	 
			 var I_length =  parseInt(S_FinalStringToprint.length) - parseInt(1) 
			 var S_FinalStringToprint = S_FinalStringToprint.substring(0,I_length) 
			 nlapiLogExecution('DEBUG','IN SUITELET','S_FinalStringToprint =='+S_FinalStringToprint); 
			 
	    	 
		/*
        * Begin :For creating the FinalString to print.
		*/		 
			  
			
		/*
        * Begin :To Set the value in the media Category record on seleceted field.
		*/	
			
			  if (S_FinalStringToprint != null && S_FinalStringToprint != '' && S_FinalStringToprint != undefined) 
				{
					
					S_FinalStringToprint = S_FinalStringToprint+"#"+I_DayNo;
					response.write('<html><head><script>window.opener.addComments("' + S_FinalStringToprint + '");self.close();</script></head><body></body></html>');
				}
			 
			
		/*
        * End :To Set the value in the media Category record on seleceted field.
		*/	 
	        
		
	 	
	 }// else request.getMethod() == 'POST' end
	 
	 }//End-Try
	catch (e) 
	{
			nlapiLogExecution('DEBUG', 'IN SUITELET', 'Error -->' + e);
	}

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
