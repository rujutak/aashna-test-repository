/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_MediaPlan_ShowItemsValidate.js
	Date       : 17 April 2013
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================


var current_line;
var media_ID;


// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    //Begin Code : for getting sales order from the url
		var urlStr=window.location.href.toLowerCase();
		if ((urlStr.indexOf("?")) > -1) 
		{
			var parameterList = urlStr.substr((urlStr.indexOf("?") + 1), urlStr.length);
						
			var paramArray = parameterList.split("&");
					
			if (paramArray != undefined && paramArray != null && paramArray != '') 
			{
				var Temp=new Array();
				for (var i = 0; i < paramArray.length; i++) 
				{
				
					Temp = paramArray[i].split("=");
									
					if (Temp[0] == 'custscript_currentlineitem') 
					{
						current_line=Temp[1];
					//	alert('Current Line =='+current_line);
						
					}
					else if (Temp[0] == 'custscript_mediaplan') 
					{
						media_ID=Temp[1];
				//		alert('Media ID =='+media_ID);
												
					}
				}
			}
		}

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
	var media_type_tv;
	var ml_alias;
	var blank='';
	if(name=='custpage_category')
	{
		var showitem_category=nlapiGetCurrentLineItemValue('custpage_expense_sublist','custpage_category');
	  //  alert('Show Item Category'+showitem_category);
		
		
		
		var mediaOBJ=nlapiLoadRecord('customrecord_mediaplan_1',media_ID);
	//	alert('mediaOBJ'+mediaOBJ);
		
	//	alert('current_line'+current_line)
		
	    var ms_currentItemID=mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',current_line);
	//	alert('ms_currentItemID'+ms_currentItemID)
	  
	  
	    if(ms_currentItemID!=null && ms_currentItemID!='')
		{
			var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_currentItemID);
		//	alert('tv_programOBJ --->'+tv_programOBJ);
		    
			if(tv_programOBJ!=null)
			{
				media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
		//		alert('media_type_tv --->'+media_type_tv);
			}
		
		}
		
		var line_count_mlst=mediaOBJ.getLineItemCount('recmachcustrecord_mp2_mp1ref');
	//	alert('line_count_mlst'+line_count_mlst);
		
		if(line_count_mlst!=null)
		{
			for(var i=1;i<=line_count_mlst;i++)
			{
				var ml_name=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_spotname',i);
		//		alert('ml_name'+ml_name)
				
				
				var ml_type=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type',i);
		//		alert('ml_type'+ml_type)
				
				ml_alias=mediaOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',i);
		//		alert('ml_alias'+ml_alias)
				
				var splitShowItem=new Array();
								
				var show=showitem_category.split(" ")
				show=show[0];
			//	alert('show'+show);
				
				if(show==ml_alias)
				{
					if(ml_type!=media_type_tv)
					{
						alert('Please enter item with correct Media Type .');
						nlapiSetCurrentLineItemValue('custpage_expense_sublist','custpage_category',blank);
						break;
					}
								
					
				}

				
				
			}
		}
	
	
	}

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
