// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
    Description:



	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


05-march-2013				Rakesh.M						Supriya						On Item Coloumn field changePick the program Item Type Field Value If it is Of type TV Allow to set the item 
                																		else  Show one alert message �Please Enter TV Program Items � & clear the line.

	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





//function to be excute when show item button press.
function CreateShowItemButton(MediaplanID)  
{

  
	var V_CurrentLineItem = nlapiGetCurrentLineItemIndex('recmachcustrecord_mp3_mp1ref');
	
	var V_MediaListLineCountItem = nlapiGetLineItemCount ('recmachcustrecord_mp2_mp1ref');
	
	
	var A_ItemArray = new Array(); 
	var P = 0;
	
	for (var i=1; i<=V_MediaListLineCountItem; i++) 	
			{
				
						
						var S_SpotName = nlapiGetLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_spotname',i)	
						nlapiLogExecution('DEBUG','IN SUITELET','S_SpotName =='+S_SpotName);
						
						var S_SpotAliasText = nlapiGetLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',i)	
						nlapiLogExecution('DEBUG','IN SUITELET','S_SpotAliasText =='+S_SpotAliasText);
						
						var S_String = S_SpotAliasText+' '+S_SpotName
						
						A_ItemArray[P] = S_String;
						
						
				P = P+1;	
			
	        };// var i=1; i<=i_MediaScheduleLineCount; i++
	
	
	
	
	if(V_MediaListLineCountItem > 0)
	{
	 /*
     * Backend Suitelet Deployment Link : https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=111&deploy=1
     */   
	
	    

		if(V_CurrentLineItem!=null && V_CurrentLineItem!='')
		{
			var temp='https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=111&deploy=1&custscript_CurrentLineItem='+V_CurrentLineItem+'&custscript_MediaPlan='+MediaplanID+'&custscript_MediaListArray='+A_ItemArray; 
    		var objWind=window.open(temp,"_blank","toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=500,Height=450");
		}
		else
		{
			alert('Select Line Item');
		}
		
	 
	}
	else
	{
		alert('Please Enter Items In The Media List & Save the record. The Click on the Add Items Button')
	}
	
	
				
     

   

}



// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY

    
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField_onitem(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_onitem(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	


    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_onitem(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY
	


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================



function addComments(S_FinalStringToprint)
{
	
	if (S_FinalStringToprint != null && S_FinalStringToprint != undefined)
	 {
		var S_String = S_FinalStringToprint.split('#')
		var S_ItemString = S_String[0]
		var I_Day = S_String[1]
		
		
		if(I_Day == 1)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_1', S_ItemString, true);
		}
		
		if(I_Day == 2)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_2', S_ItemString, true);
		}
		
		if(I_Day == 3)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_3', S_ItemString, true);
		}
		
		if(I_Day == 4)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_4', S_ItemString, true);
		}
		
		if(I_Day == 5)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_5', S_ItemString, true);
		}
		
		if(I_Day == 6)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_6', S_ItemString, true);
		}
		
		if(I_Day == 7 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_7', S_ItemString, true);
		}
		if(I_Day == 8)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_8', S_ItemString, true);
		}
		
		if(I_Day == 9)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_9', S_ItemString, true);
		}
		
		if(I_Day == 10 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_10', S_ItemString, true);
		}
		
		if(I_Day == 11 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_11', S_ItemString, true);
		}
		
		if(I_Day == 12)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_12', S_ItemString, true);
		}
		
		if(I_Day == 13)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_13', S_ItemString, true);
		}
		
		if(I_Day == 14 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_14', S_ItemString, true);
		}
		
		if(I_Day == 15)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_15', S_ItemString, true);
		}
		
		if(I_Day == 16)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_16', S_ItemString, true);
		}
		
		if(I_Day == 17 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_17', S_ItemString, true);
		}
		
		if(I_Day == 18)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_18', S_ItemString, true);
		}
		
		if(I_Day == 19)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_19', S_ItemString, true);
		}
		
		if(I_Day == 20)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_20', S_ItemString, true);
		}
		
		if(I_Day == 21)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_21', S_ItemString, true);
		}
		
		if(I_Day == 22 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_22', S_ItemString, true);
		}
		
		if(I_Day == 23)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_23', S_ItemString, true);
		}
		
		if(I_Day == 24)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_24', S_ItemString, true);
		}
		
		if(I_Day == 25)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_25', S_ItemString, true);
		}
		
		if(I_Day == 26)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_26', S_ItemString, true);
		}
		
		if(I_Day == 27)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_27', S_ItemString, true);
		}
		
		if(I_Day == 28 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_28', S_ItemString, true);
		}
		
		if(I_Day == 29 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_29', S_ItemString, true);
		}
		
		if(I_Day == 30 )
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_30', S_ItemString, true);
		}
		
		if(I_Day == 31)
		{
		nlapiSetCurrentLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_31', S_ItemString, true);
		}
		
		
		
		
		nlapiCommitLineItem('recmachcustrecord_mp3_mp1ref');	
		
		
	}
}



// END FUNCTION =====================================================




