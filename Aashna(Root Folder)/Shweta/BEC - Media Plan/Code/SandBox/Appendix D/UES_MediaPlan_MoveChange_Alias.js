/**
 * @author Shweta
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_MediaPlan_MoveChange_Alias.js
	Date       : 19 April 2013
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
	var result;
	var ms_Array=new Array();
	//var ms_Array;
	var ms_cnt=0;
	var new_mediaOBJ;
	
	var ml_Array=new Array();
	var ml_cnt=0;
	 //=================== New Details =============================
   
    var new_mediaID=nlapiGetRecordId();
    nlapiLogExecution('DEBUG','afterSubmitRecord',' New Media Record ID --> '+new_mediaID);
	
	
	 var new_media_recordtype=nlapiGetRecordType();
    nlapiLogExecution('DEBUG','afterSubmitRecord',' New Media Record Type --> '+nlapiGetRecordType);
	
	
	
	if(new_mediaID!=null && new_mediaID!='' && new_mediaID!=undefined)
	{
		new_mediaOBJ=nlapiLoadRecord(new_media_recordtype,new_mediaID);
        nlapiLogExecution('DEBUG','afterSubmitRecord',' New Media Record Object --> '+new_mediaOBJ);
		
		
		
		
	}
	
	
	
	
	

	var total='';
	
	var old_media_type_tv;
	//================= Old Details =============================
    var old_mediaOBJ=nlapiGetOldRecord();
    nlapiLogExecution('DEBUG','afterSubmitRecord',' Old Media Record Object --> '+old_mediaOBJ);
	
	
	
	
	if(new_mediaOBJ!=null && new_mediaOBJ!='' && new_mediaOBJ!=undefined)
	{
		
	if(old_mediaOBJ!=null && old_mediaOBJ!='' &&old_mediaOBJ!=undefined)
	{
		var old_linecount_mediaschedule=old_mediaOBJ.getLineItemCount('recmachcustrecord_mp3_mp1ref');
	//nlapiLogExecution('DEBUG','suiteletFunction','  Line Count Media SChedule [ Media Plan ]--->'+old_linecount_mediaschedule);
	
	
	if(old_linecount_mediaschedule!=null && old_linecount_mediaschedule!='')
	{
		
			if(old_linecount_mediaschedule!=null)
	{
		
		for (var i = 1; i <= old_linecount_mediaschedule; i++)
		{
			
			var old_status = new_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_old_status', i);
			old_status = validateValue(old_status);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Old Status [ Media Schedule ][' + i + ']--->' + old_status);
			
			
			//new_mediaID,old_status,
			
			var old_tvProgram_ID = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_tvprogram', i);
			old_tvProgram_ID = validateValue(old_tvProgram_ID);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  TV Program ID [ Media Schedule ][' + i + ']--->' + old_tvProgram_ID);
			
			if (old_tvProgram_ID != null && old_tvProgram_ID != '') 
			{
				var tv_programOBJ = nlapiLoadRecord('serviceitem', old_tvProgram_ID);
				nlapiLogExecution('DEBUG', 'suiteletFunction', '  TV Program Object [ Media Schedule ][' + i + ']--->' + tv_programOBJ);
				
				if (tv_programOBJ != null)
				{
					old_media_type_tv = tv_programOBJ.getFieldValue('custitem_bec_mediatype');
					nlapiLogExecution('DEBUG', 'suiteletFunction', '  TV Program Media Type [ Media Schedule ][' + i + ']--->' + old_media_type_tv);
				}
				
			}
			
			
			if(old_media_type_tv==1 && old_status!=7 && old_status!=8)
			{
			var old_tvProgram = old_mediaOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_tvprogram', i);
			old_tvProgram = validateValue(old_tvProgram);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  TV Program [ Media Schedule ][' + i + ']--->' + old_tvProgram);
			
		
			var old_month = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_month', i);
			old_month = validateValue(old_month);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Month [ Media Schedule ][' + i + ']--->' + old_month);
			
			var old_year = old_mediaOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_year', i);
			old_year = validateValue(old_year);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Year [ Media Schedule ][' + i + ']--->' + old_year);
			
			
			var old_one = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_1', i);
			old_one = validateValue(old_one);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  One [ Media Schedule ][' + i + ']--->' + old_one);
			
			
		//	var result_medialist_one= medialist_values(old_one,old_media_type_tv,new_mediaID);
		//	nlapiLogExecution('DEBUG', 'suiteletFunction', '  Result Media List [ Media Schedule ][' + i + ']--->' + result_medialist_one);
						
					
			
			var old_two = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_2', i);
			old_two = validateValue(old_two);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Two [ Media Schedule ][' + i + ']--->' + old_two)
			
			
		//	var result_medialist_two= medialist_values(old_two,old_media_type_tv,new_mediaID);
		//	nlapiLogExecution('DEBUG', 'suiteletFunction', '  Result Media List [ Media Schedule ][' + i + ']--->' + result_medialist_two);
			
			
			var old_three = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_3', i);
			old_three = validateValue(old_three);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Three [ Media Schedule ][' + i + ']--->' + old_three);
			
			
		//	var result_medialist_three= medialist_values(old_three,old_media_type_tv,new_mediaID);
			
			var old_four = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_4', i);
			old_four = validateValue(old_four);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Four [ Media Schedule ][' + i + ']--->' + old_four);
						
			var result_medialist_four= medialist_values(old_four,old_media_type_tv,new_mediaID);
					
			
			
			
			var old_five = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_5', i);
			old_five = validateValue(old_five);
			nlapiLogExecution('DEBUG', 'suiteletFunction', '  Five [ Media Schedule ][' + i + ']--->' + old_five);
			var result_medialist_five= medialist_values(old_five,old_media_type_tv,new_mediaID);
			
			var old_six = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_6', i);
			old_six = validateValue(old_six);
			var result_medialist_six= medialist_values(old_six,old_media_type_tv,new_mediaID);
			
			
			var old_seven = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_7', i);
			old_seven = validateValue(old_seven);
			var result_medialist_seven= medialist_values(old_seven,old_media_type_tv,new_mediaID);
			
			var old_eight = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_8', i);
			old_eight = validateValue(old_eight);
			var result_medialist_eight= medialist_values(old_eight,old_media_type_tv,new_mediaID);
			
			var old_nine = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_9', i);
			old_nine = validateValue(old_nine);
			var result_medialist_nine= medialist_values(old_nine,old_media_type_tv,new_mediaID);
			
			var old_ten = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_10', i);
			old_ten = validateValue(old_ten);
			var result_medialist_ten= medialist_values(old_ten,old_media_type_tv,new_mediaID);
			
			var old_eleven = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_11', i);
			old_eleven = validateValue(old_eleven);
			var result_medialist_eleven= medialist_values(old_eleven,old_media_type_tv,new_mediaID);
			
			var old_twelve = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_12', i);
			old_twelve = validateValue(old_twelve);
			var result_medialist_twelve= medialist_values(old_twelve,old_media_type_tv,new_mediaID);
			
			var old_thirteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_13', i);
			old_thirteen = validateValue(old_thirteen);
			var result_medialist_thirteen= medialist_values(old_thirteen,old_media_type_tv,new_mediaID);
			
			var old_fourteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_14', i);
			old_fourteen = validateValue(old_fourteen);
			var result_medialist_fourteen= medialist_values(old_fourteen,old_media_type_tv,new_mediaID);
			
			var old_fifteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_15', i);
			old_fifteen = validateValue(old_fifteen);
			var result_medialist_fifteen= medialist_values(old_fifteen,old_media_type_tv,new_mediaID);
			
			var old_sixteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_16', i);
			old_sixteen = validateValue(old_sixteen);
			var result_medialist_sixteen= medialist_values(old_sixteen,old_media_type_tv,new_mediaID);
			
			var old_seventenn = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_17', i);
			old_seventenn = validateValue(old_seventenn);
			var result_medialist_seventenn= medialist_values(old_seventenn,old_media_type_tv,new_mediaID);
			
			
			var old_eighteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_18', i);
			old_eighteen = validateValue(old_eighteen);
			var result_medialist_eighteen= medialist_values(old_eighteen,old_media_type_tv,new_mediaID);
			
			var old_nineteen = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_19', i);
			old_nineteen = validateValue(old_nineteen);
			var result_medialist_nineteen= medialist_values(old_nineteen,old_media_type_tv,new_mediaID);
			
			var old_twenty = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_20', i);
			old_twenty = validateValue(old_twenty);
			var result_medialist_twenty= medialist_values(old_twenty,old_media_type_tv,new_mediaID);
			
			var old_twentyone = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_21', i);
			old_twentyone = validateValue(old_twentyone);
			var result_medialist_twentyone= medialist_values(old_twentyone,old_media_type_tv,new_mediaID);
			
			var old_twentytwo = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_22', i);
			old_twentytwo = validateValue(old_twentytwo);
			var result_medialist_twentytwo= medialist_values(old_twentytwo,old_media_type_tv,new_mediaID);
			
			var old_twentythree = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_23', i);
			old_twentythree = validateValue(old_twentythree);
			var result_medialist_twentythree= medialist_values(old_twentythree,old_media_type_tv,new_mediaID);
			
			var old_twentyfour = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_24', i);
			old_twentyfour = validateValue(old_twentyfour);
			var result_medialist_twentyfour= medialist_values(old_twentyfour,old_media_type_tv,new_mediaID);
			
			var old_twentyfive = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_25', i);
			old_twentyfive = validateValue(old_twentyfive);
			var result_medialist_twentyfive= medialist_values(old_twentyfive,old_media_type_tv,new_mediaID);
			
			var old_twentysix = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_26', i);
			old_twentysix = validateValue(old_twentysix);
			var result_medialist_twentysix= medialist_values(old_twentysix,old_media_type_tv,new_mediaID);
			
			var old_twentyseven = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_27', i);
			old_twentyseven = validateValue(old_twentyseven);
			var result_medialist_twentyseven= medialist_values(old_twentyseven,old_media_type_tv,new_mediaID);
			
			var old_twentyeight = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_28', i);
			old_twentyeight = validateValue(old_twentyeight);
			var result_medialist_twentyeight= medialist_values(old_twentyeight,old_media_type_tv,new_mediaID);
			
			var old_twentynine = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_29', i);
			old_twentynine = validateValue(old_twentynine);
			var result_medialist_twentynine= medialist_values(old_twentynine,old_media_type_tv,new_mediaID);
			
			var old_thirty = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_30', i);
			old_thirty = validateValue(old_thirty);
			var result_medialist_thirty= medialist_values(old_thirty,old_media_type_tv,new_mediaID);
			
			var old_thirtyone = old_mediaOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref', 'custrecord_mp3_31', i);
			old_thirtyone = validateValue(old_thirtyone);
			var result_medialist_thirtyone= medialist_values(old_thirtyone,old_media_type_tv,new_mediaID);		
		
				
		   //ml_Array[ml_cnt++] = new_mediaID+'/'+result_medialist_one +'/'+result_medialist_two + '##' +result_medialist_three + '##' +result_medialist_four + '##' +result_medialist_five + '##' +result_medialist_six + '##' +result_medialist_seven + '##' +result_medialist_eight + '##' +result_medialist_nine + '##' +result_medialist_ten	+ '##' +result_medialist_eleven	+ '##' +result_medialist_twelve	+ '##' +result_medialist_thirteen+ '##' +result_medialist_fourteen	+ '##' +result_medialist_fifteen+ '##' +result_medialist_sixteen+ '##' +result_medialist_seventenn+ '##' +result_medialist_eighteen+ '##' +result_medialist_nineteen+ '##' +result_medialist_twenty+ '##' +result_medialist_twentyone+ '##' +result_medialist_twentytwo+ '##' +result_medialist_twentythree+ '##' +result_medialist_twentyfour+ '##' +result_medialist_twentyfive+ '##' +result_medialist_twentysix+ '##' +result_medialist_twentyseven+ '##' +result_medialist_twentyeight+ '##' +result_medialist_twentynine+ '##' +result_medialist_thirty+ '##' +result_medialist_thirtyone;
				
		   ms_Array[ms_cnt++] = new_mediaID+'$'+old_media_type_tv+'$'+old_status +'$'+old_tvProgram_ID + '$' +old_month + '$' +old_year + '$' +old_one + '$' +old_two + '$' +old_three + '$' +old_four+ '$' +old_five + '$' +old_six + '$' +old_seven + '$' +old_eight + '$' +old_nine + '$' +old_ten+ '$' +old_eleven + '$' +old_twelve + '$' +old_thirteen + '$' +old_fourteen + '$' +old_fifteen + '$' +old_sixteen+ '$' +old_seventenn + '$' +old_eighteen + '$' +old_nineteen + '$' +old_twenty + '$' +old_twentyone + '$' +old_twentytwo+ '$' +old_twentythree + '$' +old_twentyfour + '$' +old_twentyfive + '$' +old_twentysix+ '$' +old_twentyseven + '$' +old_twentyeight + '$' +old_twentynine + '$' +old_thirty+ '$' +old_thirtyone;	
			
			// ms_Array = new_mediaID+'$'+old_media_type_tv+'$'+old_status +'$'+old_tvProgram_ID + '$' +old_month + '$' +old_year + '$' +old_one + '$' +old_two + '$' +old_three + '$' +old_four+ '$' +old_five + '$' +old_six + '$' +old_seven + '$' +old_eight + '$' +old_nine + '$' +old_ten+ '$' +old_eleven + '$' +old_twelve + '$' +old_thirteen + '$' +old_fourteen + '$' +old_fifteen + '$' +old_sixteen+ '$' +old_seventenn + '$' +old_eighteen + '$' +old_nineteen + '$' +old_twenty + '$' +old_twentyone + '$' +old_twentytwo+ '$' +old_twentythree + '$' +old_twentyfour + '$' +old_twentyfive + '$' +old_twentysix+ '$' +old_twentyseven + '$' +old_twentyeight + '$' +old_twentynine + '$' +old_thirty+ '$' +old_thirtyone;	
			
			
	//		  ml_Array[ml_cnt++] = new_mediaID+'$'+result_medialist_one +'$'+result_medialist_two + '/' +result_medialist_three + '$' +result_medialist_four 
			
	//ms_Array[ms_cnt++] = new_mediaID+'/'+old_media_type_tv+'/'+old_status +'$'+old_tvProgram_ID + '##' +old_month + '##' +old_year + '##' +old_one + '##' +old_two + '##' +old_three + '##' +old_four;
			
						
			}//END - Spot Check
				
			
		}
		
	nlapiLogExecution('DEBUG', 'suiteletFunction', '  ms_Array[' + i + ']--->' + ms_Array);	
	nlapiLogExecution('DEBUG', 'suiteletFunction', '  ms_cnt[' + i + ']--->' + ms_cnt);	
	
	 var params=new Array();
	  params['status']='scheduled';
	  params['runasadmin']='T';
	  params['custscript_mediascheduleold']=ms_Array;
	  params['custscript_medialistold']=ml_Array;
	  params['custscript_mscount']=ms_cnt;
	  
	  var startDate = new Date();
	  params['startdate']=startDate.toUTCString();
	  
      nlapiLogExecution('DEBUG', 'suiteletFunction',' params[custscript_mscount]: ' + params['custscript_mscount']);
	  nlapiLogExecution('DEBUG', 'suiteletFunction',' params[custscript_mediascheduleold]: ' + params['custscript_mediascheduleold']);
	  nlapiLogExecution('DEBUG', 'suiteletFunction',' params[custscript_medialistold]: ' + params['custscript_medialistold']);	 
	  			  
	  var status=nlapiScheduleScript('customscript_mediaschedule_changeinmedia','customdeploy1',params);
	  nlapiLogExecution('DEBUG', 'suiteletFunction', ' status: ' + status);
			
				
		
	}//end
		
		
	}//Check old count
	
	
		
		
	}//obj check
		
		
		
	}
	
	
	
	
	
	
	

	
  
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================

function validateValue(value)
{
	if(value==null || value ==undefined || value=='')
	{
		value=''
	}
	return value;
}






function searchMedialist(letter, media_type_tv,recordID)
{
	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
	
	
	var ml_recordID;
	var ml_productname;
	var ml_medianame;
	var ml_newduration;
	var result;
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
	filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
	filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
	columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
	columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
	
	var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);

	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
		
		for (var j = 0; j < searchresults.length; j++) 
		{
			ml_recordID = searchresults[j].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
			
			ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
		
		    ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
			nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
			
			ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' +j + ']==' + ml_newduration);
		
		    result=ml_recordID+'%%'+ml_productname+'%%'+ml_medianame+'%%'+ml_newduration;
		   
		}
		
	}
	
   	return result;
	
}




function medialist_values(ms_one,media_type_tv,mediaplanID)
{
	var medialist_result;
	var aliasQuantity = ms_one.split(',')
    nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
	
	for (var k = 0; k < aliasQuantity.length; k++) 
	{
		var alias_ms = aliasQuantity[k];
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
		
		var letter = "";
		var number = "";
		
		for (var q = 0; q < alias_ms.length; q++) 
		{
			var b = alias_ms.charAt(q);
			if (isNaN(b) == true) 
			{
				letter = letter + b;
			}
			else
			{
				number = number + b;
			}
		}
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'letter=' + letter)
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'number=' + number)
		
		medialist_result = searchMedialist(letter, media_type_tv, mediaplanID);
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
		
		
		
	}
	
	
	
	return medialist_result;
	
}











// END FUNCTION =====================================================
