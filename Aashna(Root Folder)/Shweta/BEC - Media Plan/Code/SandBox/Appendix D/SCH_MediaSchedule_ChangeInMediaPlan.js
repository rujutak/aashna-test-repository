/**
 * @author Shweta
 */
//

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_MediaSchedule_ChangeInMediaPlan.js
    Date       : 19 APril 2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	var old_tv_prog ;
    var old_quota ;
	var old_planunit ;
    var old_month;
    var old_year ;
	var old_one ;
    var old_two ;
    var old_three ;
    var old_four ;
	var old_five ;
    var old_six ;
    var old_seven;
    var old_eight ;
	var old_nine;
    var old_ten ;
    var old_eleven;
    var old_twelve;
	var old_thirteen ;
    var old_fourteen;
    var old_fifteen ;
    var old_sixteen ;
	var old_seventeen ;
    var old_eightteen ;
    var old_nineteen ;
    var old_twenty ;
	var old_twentyone ;
    var old_twentytwo;
    var old_twentythree ;
    var old_twentyfour ;
	var old_twentyfive ;
    var old_twentysix;
    var old_twentyseven ;
    var old_twentyeight;
	var old_twentynine ;
    var old_thirty ;
    var old_thirtyone;
    var old_tv_type; 
    var old_mediaID ;
    var old_media_type;
    var old_old_status; 
  
  var ms_status;
	var ms_day;
	var ms_month;
	var ms_year;
	var ms_tv_program;
	var mediaplanID;
	var ms_one ;
    var ms_two ;
    var ms_three ;
    var ms_four ;
	var ms_five ;
    var ms_six ;
    var ms_seven;
    var ms_eight ;
	var ms_nine;
    var ms_ten ;
    var ms_eleven;
    var ms_twelve;
	var ms_thirteen ;
    var ms_fourteen;
    var ms_fifteen ;
    var ms_sixteen ;
	var ms_seventeen ;
    var ms_eightteen ;
    var ms_nineteen ;
    var ms_twenty ;
	var ms_twentyone ;
    var ms_twentytwo;
    var ms_twentythree ;
    var ms_twentyfour ;
	var ms_twentyfive ;
    var ms_twentysix;
    var ms_twentyseven ;
    var ms_twentyeight;
	var ms_twentynine ;
    var ms_thirty ;
    var ms_thirtyone;
  
  
  var old_medialist_tv_prog ;
    var old_medialist_quota ;
	var old_medialist_planunit ;
    var old_medialist_month;
    var old_medialist_year ;
	var old_medialist_one ;
    var old_medialist_two ;
    var old_medialist_three ;
    var old_medialist_four ;
	var old_medialist_five ;
    var old_medialist_six ;
    var old_medialist_seven;
    var old_medialist_eight ;
	var old_medialist_nine;
    var old_medialist_ten ;
    var old_medialist_eleven;
    var old_medialist_twelve;
	var old_medialist_thirteen ;
    var old_medialist_fourteen;
    var old_medialist_fifteen ;
    var old_medialist_sixteen ;
	var old_medialist_seventeen ;
    var old_medialist_eightteen ;
    var old_medialist_nineteen ;
    var old_medialist_twenty ;
	var old_medialist_twentyone ;
    var old_medialist_twentytwo;
    var old_medialist_twentythree ;
    var old_medialist_twentyfour ;
	var old_medialist_twentyfive ;
    var old_medialist_twentysix;
    var old_medialist_twentyseven ;
    var old_medialist_twentyeight;
	var old_medialist_twentynine ;
    var old_medialist_thirty ;
    var old_medialist_thirtyone;
    var old_medialist_tv_type; 
    var old_medialist_mediaID ;
    var old_medialist_media_type;
    var old_medialist_status; 
    
  	
	 var ms_status;
	 var ms_tv_program;
	
	
   var context = nlapiGetContext();
   
   var usage_begin=context.getRemainingUsage();
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Usage Begin --> " + usage_begin);

   var media_schedule_array_r=context.getSetting('SCRIPT','custscript_mediascheduleold');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule Array  --> " + media_schedule_array_r);
   
   var media_schedule_count=context.getSetting('SCRIPT','custscript_mscount');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule Count  --> " + media_schedule_count);
   
   //custscript_mscount   
    
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule Array Initial Length --> " + media_schedule_array_r.length);	
	

	
	
	
  var media_schedule_array=media_schedule_array_r.toString();	
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Schedule Array Final Length --> " + media_schedule_array.length);	
	
   //================================ Media Schedule Old =========================

   
var split_ms_array_old=new Array();

 for (var y = 0; y < media_schedule_count; y++) 
 {
 
 	split_ms_array_old = media_schedule_array.split('$');
 	
	old_mediaID = split_ms_array_old[0];
	
	old_media_type = split_ms_array_old[1];
	
	old_old_status = split_ms_array_old[2];
	
 	old_tv_prog = split_ms_array_old[3];
	
 	old_month = split_ms_array_old[4];
	
 	old_year = split_ms_array_old[5];
		
 	old_one = split_ms_array_old[6];
	
 	old_two = split_ms_array_old[7];
	
 	old_three = split_ms_array_old[8];
	
 	old_four = split_ms_array_old[9];
	
 	old_five = split_ms_array_old[10];
	
 	old_six = split_ms_array_old[11];
	
 	old_seven = split_ms_array_old[12];
	
 	old_eight = split_ms_array_old[13];
	
 	old_nine = split_ms_array_old[14];
	
 	old_ten = split_ms_array_old[15];
	
 	old_eleven = split_ms_array_old[16];
	
 	old_twelve = split_ms_array_old[17];
	
 	old_thirteen = split_ms_array_old[18];
	
 	old_fourteen = split_ms_array_old[19];
	
 	old_fifteen = split_ms_array_old[20];
	
 	old_sixteen = split_ms_array_old[21];
	
 	old_seventeen = split_ms_array_old[22];
	
 	old_eightteen = split_ms_array_old[23];
	
 	old_nineteen = split_ms_array_old[24];
	
 	old_twenty = split_ms_array_old[25];
	
 	old_twentyone = split_ms_array_old[26];
	
 	old_twentytwo = split_ms_array_old[27];
	
 	old_twentythree = split_ms_array_old[28];
	
 	old_twentyfour = split_ms_array_old[29];
	
 	old_twentyfive = split_ms_array_old[30];
	
 	old_twentysix = split_ms_array_old[31];
	
 	old_twentyseven = split_ms_array_old[32];
	
 	old_twentyeight = split_ms_array_old[33];
	
 	old_twentynine = split_ms_array_old[34];
	
 	old_thirty = split_ms_array_old[35];
	
 	old_thirtyone = split_ms_array_old[36];
	
	
	nlapiLogExecution('DEBUG', 'searchMedialist', ' old_tv_prog-->  ' + old_tv_prog);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' old_old_status-->  ' + old_old_status);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' old_media_type-->  ' + old_media_type);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' old_one-->  ' + old_one);
	
	
	
	
	
	var approve_statusopen=7;
	approve_statuschange=8;
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', old_mediaID);
		
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
		
	var searchresults = nlapiSearchRecord('customrecord_mediaplan_3','customsearch_mediaschedulesearchchan', filter, columns);
	
	if (searchresults != null)
	 {
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  Media Schedule -->  ' + searchresults.length);
		
		for (var i = 0; i < searchresults.length; i++) 
		{
			recordID = searchresults[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + i + ']==' + recordID);
				   
		
		     var recordOBJ=nlapiLoadRecord('customrecord_mediaplan_3',recordID);
	    //     nlapiLogExecution('DEBUG','afterSubmitRecord',' Record Object --> '+recordOBJ);
			
			   if (recordOBJ != null)
			   {
			  	ms_status = recordOBJ.getFieldValue('custrecord_mp3_ms_status');
			//  	nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Status [ Media Schedule ] --> ' + ms_status);
			  				
				
				ms_tv_program = recordOBJ.getFieldValue('custrecord_mp3_tvprogram');
			//  	nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' TV Program [ Media Schedule ] --> ' + ms_tv_program);
			   
			  
			    if(ms_tv_program!=null && ms_tv_program!='')
				{
					var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_tv_program);
				//	nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Object [ Media Schedule ]--->'+tv_programOBJ);
				    
					if(tv_programOBJ!=null)
					{
						media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
					//	nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Media Type [ Media Schedule ]-->'+media_type_tv);
						
						media_duration_onairdate=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
					//	nlapiLogExecution('DEBUG','suiteletFunction','  TV Program On Air Date  [ Media Schedule ]--->'+media_duration_onairdate);
					}
				
		
				}
	
	if((ms_status==7 || ms_status==8) && media_type_tv==1  && (old_old_status!=7 || old_old_status==8))
	{
				
		nlapiLogExecution('DEBUG','afterSubmitRecord',' Changed ............ Moved ............');
		
					
		 ms_month=recordOBJ.getFieldValue('custrecord_mp3_month');
	//	 nlapiLogExecution('DEBUG','afterSubmitRecord',' Month [ Media Schedule ] --> '+ms_month);
		 
		 ms_year=recordOBJ.getFieldText('custrecord_mp3_year');
	//	 nlapiLogExecution('DEBUG','afterSubmitRecord',' Year [ Media Schedule ] --> '+ms_year);
		 
		 
		 ms_one=recordOBJ.getFieldValue('custrecord_mp3_1');
		 ms_one=validateValue(ms_one);
		 nlapiLogExecution('DEBUG','suiteletFunction','  One [ Media Schedule ]--->'+ms_one);
		 var onair_date_one=1+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_one=changeinmediaplan_record_status(recordID,onair_date_one);
         nlapiLogExecution('DEBUG','afterSubmitRecord',' record_changeinmediaplan_status --> '+record_changeinmediaplan_status_one);	
		 
		 if(record_changeinmediaplan_status_one==false && ms_one!='')
		 {
		 	
			var check_one=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_one,old_one,ms_one,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_one [ Media Schedule ]--->'+check_one);

		 	
		 }


         ms_two=recordOBJ.getFieldValue('custrecord_mp3_2');
		 ms_two=validateValue(ms_two);
		 nlapiLogExecution('DEBUG','suiteletFunction','  One [ Media Schedule ]--->'+ms_two);
		 var onair_date_two=2+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_two=changeinmediaplan_record_status(recordID,onair_date_two);
         nlapiLogExecution('DEBUG','afterSubmitRecord',' record_changeinmediaplan_status two--> '+record_changeinmediaplan_status_two);	
		 
		 if(record_changeinmediaplan_status_two==false && ms_two!='')
		 {
		 	
			var check_two=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_two,old_two,ms_two,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_two [ Media Schedule ]--->'+check_two);
		 	
		 }
		 
		 
		 
		ms_three=recordOBJ.getFieldValue( 'custrecord_mp3_3');
		ms_three=validateValue(ms_three);
		nlapiLogExecution('DEBUG','suiteletFunction','  Three [ Media Schedule ]--->'+ms_three);
		
		
		var onair_date_three=3+'/'+ms_month+'/'+ms_year;

		 
		var record_changeinmediaplan_status_three=changeinmediaplan_record_status(recordID,onair_date_three);
        nlapiLogExecution('DEBUG','afterSubmitRecord',' record_changeinmediaplan_status_three --> '+record_changeinmediaplan_status_three);	

		 
		 if(record_changeinmediaplan_status_three==false && ms_three!='')
		 {
		 	
			var check_three=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_three,old_three,ms_three,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_three [ Media Schedule ]--->'+check_three);

		 	
		 }
		 
		 
		
		ms_four=recordOBJ.getFieldValue( 'custrecord_mp3_4');
		ms_four=validateValue(ms_four);
		nlapiLogExecution('DEBUG','suiteletFunction','  Four [ Media Schedule ]--->'+ms_four);
 
		var onair_date_four=4+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_four=changeinmediaplan_record_status(recordID,onair_date_four);
        
		  
		 if(record_changeinmediaplan_status_four==false && ms_four!='')
		 {
		 	
			var check_four=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_four,old_four,ms_four,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_four [ Media Schedule ]--->'+check_four);
		 	
		 }
		 
		 
		ms_five=recordOBJ.getFieldValue( 'custrecord_mp3_5');
		ms_five=validateValue(ms_five);
		nlapiLogExecution('DEBUG','suiteletFunction','  Five [ Media Schedule ]--->'+ms_five);
				
		var onair_date_five=5+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_five=changeinmediaplan_record_status(recordID,onair_date_five);
        
		  
		 if(record_changeinmediaplan_status_five==false && ms_five!='')
		 {
		 	
			var check_five=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_five,old_five,ms_five,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_five [ Media Schedule ]--->'+check_five);
		 	
		 }		

		 
		 
		ms_six=recordOBJ.getFieldValue( 'custrecord_mp3_6' );
		ms_six=validateValue(ms_six);
        nlapiLogExecution('DEBUG','suiteletFunction','  Six [ Media Schedule ]--->'+ms_six);
		 
		var onair_date_six=6+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_six=changeinmediaplan_record_status(recordID,onair_date_six);
        
		  
		 if(record_changeinmediaplan_status_six==false && ms_six!='')
		 {
		 	
			var check_six=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_six,old_six,ms_six,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_six [ Media Schedule ]--->'+check_six);
		 	
		 }		 
		 
		 
		 
		 ms_seven=recordOBJ.getFieldValue( 'custrecord_mp3_7');
		ms_seven=validateValue(ms_seven);
        var onair_date_seven=7+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_seven=changeinmediaplan_record_status(recordID,onair_date_seven);
        
		  
		 if(record_changeinmediaplan_status_seven==false && ms_seven!='')
		 {
		 	
			var check_seven=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_seven,old_seven,ms_seven,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_seven [ Media Schedule ]--->'+check_seven);
		 	
		 }		
		 
		 
		 
		 ms_eight=recordOBJ.getFieldValue( 'custrecord_mp3_8' );
		 ms_eight=validateValue(ms_eight);
				
         var onair_date_eight=8+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_eight=changeinmediaplan_record_status(recordID,onair_date_eight);
        
		  
		 if(record_changeinmediaplan_status_eight==false && ms_eight!='')
		 {
		 	
			var check_eight=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_eight,old_eight,ms_eight,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_eight [ Media Schedule ]--->'+check_eight);
		 	
		 }		
		 
		 
		 
		 
		 ms_nine=recordOBJ.getFieldValue( 'custrecord_mp3_9' );
		 ms_nine=validateValue(ms_nine);
				
         var onair_date_nine=9+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_nine=changeinmediaplan_record_status(recordID,onair_date_nine);
        
		  
		 if(record_changeinmediaplan_status_nine==false && ms_nine!='')
		 {
		 	
			var check_nine=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_nine,old_nine,ms_nine,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_nine [ Media Schedule ]--->'+check_nine);
		 	
		 }		
		 
		 
		 
		 
		 ms_ten=recordOBJ.getFieldValue( 'custrecord_mp3_10' );
		 ms_ten=validateValue(ms_ten);
				
         var onair_date_ten=10+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_ten=changeinmediaplan_record_status(recordID,onair_date_ten);
        
		  
		 if(record_changeinmediaplan_status_ten==false && ms_ten!='')
		 {
		 	
			var check_ten=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_ten,old_ten,ms_ten,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_ten [ Media Schedule ]--->'+check_ten);
		 	
		 }		
		 
		 
		  ms_eleven=recordOBJ.getFieldValue( 'custrecord_mp3_11' );
		 ms_eleven=validateValue(ms_eleven);
				
         var onair_date_eleven=11+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_eleven=changeinmediaplan_record_status(recordID,onair_date_eleven);
        
		  
		 if(record_changeinmediaplan_status_eleven==false && ms_eleven!='')
		 {
		 	
			var check_eleven=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_eleven,old_eleven,ms_eleven,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_eleven [ Media Schedule ]--->'+check_eleven);
		 	
		 }		
		 
         
		   ms_twelve=recordOBJ.getFieldValue( 'custrecord_mp3_12' );
		 ms_twelve=validateValue(ms_twelve);
				
         var onair_date_twelve=12+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twelve=changeinmediaplan_record_status(recordID,onair_date_twelve);
        
		  
		 if(record_changeinmediaplan_status_twelve==false && ms_twelve!='')
		 {
		 	
			var check_twelve=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twelve,old_twelve,ms_twelve,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twelve [ Media Schedule ]--->'+check_twelve);
		 	
		 }		

           
		 ms_thirteen=recordOBJ.getFieldValue( 'custrecord_mp3_13' );
		 ms_thirteen=validateValue(ms_thirteen);
				
         var onair_date_thirteen=13+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_thirteen=changeinmediaplan_record_status(recordID,onair_date_thirteen);
        
		  
		 if(record_changeinmediaplan_status_thirteen==false && ms_thirteen!='')
		 {
		 	
			var check_thirteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_thirteen,old_thirteen,ms_thirteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_thirteen [ Media Schedule ]--->'+check_thirteen);
		 	
		 }		


          ms_fourteen=recordOBJ.getFieldValue( 'custrecord_mp3_14' );
		 ms_fourteen=validateValue(ms_fourteen);
				
         var onair_date_fourteen=14+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_fourteen=changeinmediaplan_record_status(recordID,onair_date_fourteen);
        
		  
		 if(record_changeinmediaplan_status_fourteen==false && ms_fourteen!='')
		 {
		 	
			var check_fourteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_fourteen,old_fourteen,ms_fourteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_fourteen [ Media Schedule ]--->'+check_fourteen);
		 	
		 }		



            ms_fifteen=recordOBJ.getFieldValue( 'custrecord_mp3_15' );
		 ms_fifteen=validateValue(ms_fifteen);
				
         var onair_date_fifteen=15+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_fifteen=changeinmediaplan_record_status(recordID,onair_date_fifteen);
        
		  
		 if(record_changeinmediaplan_status_fifteen==false && ms_fifteen!='')
		 {
		 	
			var check_fifteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_fifteen,old_fifteen,ms_fifteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_fifteen [ Media Schedule ]--->'+check_fifteen);
		 	
		 }		



         ms_sixteen=recordOBJ.getFieldValue( 'custrecord_mp3_16' );
		 ms_sixteen=validateValue(ms_sixteen);
				
         var onair_date_sixteen=16+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_sixteen=changeinmediaplan_record_status(recordID,onair_date_sixteen);
        
		  
		 if(record_changeinmediaplan_status_sixteen==false && ms_sixteen!='')
		 {
		 	
			var check_sixteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_sixteen,old_sixteen,ms_sixteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_sixteen [ Media Schedule ]--->'+check_sixteen);
		 	
		 }		

		 ms_seventeen=recordOBJ.getFieldValue( 'custrecord_mp3_17' );
		 ms_seventeen=validateValue(ms_seventeen);
				
         var onair_date_seventeen=17+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_seventeen=changeinmediaplan_record_status(recordID,onair_date_seventeen);
        
		  
		 if(record_changeinmediaplan_status_seventeen==false && ms_seventeen!='')
		 {
		 	
			var check_seventeen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_seventeen,old_seventeen,ms_seventeen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_seventeen [ Media Schedule ]--->'+check_seventeen);
		 	
		 }		
		 
		 
		  ms_eighteen=recordOBJ.getFieldValue( 'custrecord_mp3_18' );
		 ms_eighteen=validateValue(ms_eighteen);
				
         var onair_date_eighteen=18+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_eighteen=changeinmediaplan_record_status(recordID,onair_date_eighteen);
        
		  
		 if(record_changeinmediaplan_status_eighteen==false && ms_eighteen!='')
		 {
		 	
			var check_eighteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_eighteen,old_eighteen,ms_eighteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_eighteen [ Media Schedule ]--->'+check_eighteen);
		 	
		 }		
		 
		 
		   ms_nineteen=recordOBJ.getFieldValue( 'custrecord_mp3_19' );
		 ms_nineteen=validateValue(ms_nineteen);
				
         var onair_date_nineteen=19+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_nineteen=changeinmediaplan_record_status(recordID,onair_date_nineteen);
        
		  
		 if(record_changeinmediaplan_status_nineteen==false && ms_nineteen!='')
		 {
		 	
			var check_nineteen=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_nineteen,old_nineteen,ms_nineteen,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_nineteen [ Media Schedule ]--->'+check_nineteen);
		 	
		 }		
		 
		 
		 ms_twenty=recordOBJ.getFieldValue( 'custrecord_mp3_20' );
		 ms_twenty=validateValue(ms_twenty);
				
         var onair_date_twenty=20+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twenty=changeinmediaplan_record_status(recordID,onair_date_twenty);
        
		  
		 if(record_changeinmediaplan_status_twenty==false && ms_twenty!='')
		 {
		 	
			var check_twenty=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twenty,old_twenty,ms_twenty,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twenty [ Media Schedule ]--->'+check_twenty);
		 	
		 }	
		 
		 
		 ms_twentyone=recordOBJ.getFieldValue( 'custrecord_mp3_21' );
		 ms_twentyone=validateValue(ms_twentyone);
				
         var onair_date_twentyone=21+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentyone=changeinmediaplan_record_status(recordID,onair_date_twentyone);
        
		  
		 if(record_changeinmediaplan_status_twentyone==false && ms_twentyone!='')
		 {
		 	
			var check_twentyone=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentyone,old_twentyone,ms_twentyone,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentyone [ Media Schedule ]--->'+check_twentyone);
		 	
		 }	
		 
		 
		  ms_twentytwo=recordOBJ.getFieldValue( 'custrecord_mp3_22' );
		 ms_twentytwo=validateValue(ms_twentytwo);
				
         var onair_date_twentytwo=22+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentytwo=changeinmediaplan_record_status(recordID,onair_date_twentytwo);
        
		  
		 if(record_changeinmediaplan_status_twentytwo==false && ms_twentytwo!='')
		 {
		 	
			var check_twentytwo=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentytwo,old_twentytwo,ms_twentytwo,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentytwo [ Media Schedule ]--->'+check_twentytwo);
		 	
		 }	
		 
		 
		 
		 ms_twentythree=recordOBJ.getFieldValue( 'custrecord_mp3_23' );
		 ms_twentythree=validateValue(ms_twentythree);
				
         var onair_date_twentythree=23+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentythree=changeinmediaplan_record_status(recordID,onair_date_twentythree);
        
		  
		 if(record_changeinmediaplan_status_twentythree==false && ms_twentythree!='')
		 {
		 	
			var check_twentythree=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentythree,old_twentythree,ms_twentythree,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentythree [ Media Schedule ]--->'+check_twentythree);
		 	
		 }	
		 
		 
		 
		 ms_twentyfour=recordOBJ.getFieldValue( 'custrecord_mp3_24' );
		 ms_twentyfour=validateValue(ms_twentyfour);
				
         var onair_date_twentyfour=24+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentyfour=changeinmediaplan_record_status(recordID,onair_date_twentyfour);
        
		  
		 if(record_changeinmediaplan_status_twentyfour==false && ms_twentyfour!='')
		 {
		 	
			var check_twentyfour=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentyfour,old_twentyfour,ms_twentyfour,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentyfour [ Media Schedule ]--->'+check_twentyfour);
		 	
		 }	
		 
		 
		 
		 
		  ms_twentyfive=recordOBJ.getFieldValue( 'custrecord_mp3_25' );
		 ms_twentyfive=validateValue(ms_twentyfive);
				
         var onair_date_twentyfive=25+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentyfive=changeinmediaplan_record_status(recordID,onair_date_twentyfive);
        
		  
		 if(record_changeinmediaplan_status_twentyfive==false && ms_twentyfive!='')
		 {
		 	
			var check_twentyfive=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentyfive,old_twentyfive,ms_twentyfive,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentyfive [ Media Schedule ]--->'+check_twentyfive);
		 	
		 }	
		 
		 
		   ms_twentysix=recordOBJ.getFieldValue( 'custrecord_mp3_26' );
		 ms_twentysix=validateValue(ms_twentysix);
				
         var onair_date_twentysix=26+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentysix=changeinmediaplan_record_status(recordID,onair_date_twentysix);
        
		  
		 if(record_changeinmediaplan_status_twentysix==false && ms_twentysix!='')
		 {
		 	
			var check_twentysix=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentysix,old_twentysix,ms_twentysix,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentysix [ Media Schedule ]--->'+check_twentysix);
		 	
		 }	
		 
		 
		 ms_twentyseven=recordOBJ.getFieldValue( 'custrecord_mp3_27' );
		 ms_twentyseven=validateValue(ms_twentyseven);
				
         var onair_date_twentyseven=27+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentyseven=changeinmediaplan_record_status(recordID,onair_date_twentyseven);
        
		  
		 if(record_changeinmediaplan_status_twentyseven==false && ms_twentyseven!='')
		 {
		 	
			var check_twentyseven=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentyseven,old_twentyseven,ms_twentyseven,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentyseven [ Media Schedule ]--->'+check_twentyseven);
		 	
		 }	
		 
		 
		 ms_twentyeight=recordOBJ.getFieldValue( 'custrecord_mp3_28' );
		 ms_twentyeight=validateValue(ms_twentyeight);
				
         var onair_date_twentyeight=28+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentyeight=changeinmediaplan_record_status(recordID,onair_date_twentyeight);
        
		  
		 if(record_changeinmediaplan_status_twentyeight==false && ms_twentyeight!='')
		 {
		 	
			var check_twentyeight=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentyeight,old_twentyeight,ms_twentyeight,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentyeight [ Media Schedule ]--->'+check_twentyeight);
		 	
		 }	
		 
		 
		 
		 
		 ms_twentynine=recordOBJ.getFieldValue( 'custrecord_mp3_29' );
		 ms_twentynine=validateValue(ms_twentynine);
				
         var onair_date_twentynine=29+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_twentynine=changeinmediaplan_record_status(recordID,onair_date_twentynine);
        
		  
		 if(record_changeinmediaplan_status_twentynine==false && ms_twentynine!='')
		 {
		 	
			var check_twentynine=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_twentynine,old_twentynine,ms_twentynine,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_twentynine [ Media Schedule ]--->'+check_twentynine);
		 	
		 }	
		 
		 
		 
		  ms_thirty=recordOBJ.getFieldValue( 'custrecord_mp3_30' );
		 ms_thirty=validateValue(ms_thirty);
				
         var onair_date_thirty=30+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_thirty=changeinmediaplan_record_status(recordID,onair_date_thirty);
        
		  
		 if(record_changeinmediaplan_status_thirty==false && ms_thirty!='')
		 {
		 	
			var check_thirty=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_thirty,old_thirty,ms_thirty,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_thirty [ Media Schedule ]--->'+check_thirty);
		 	
		 }	
		 
		   ms_thirtyone=recordOBJ.getFieldValue( 'custrecord_mp3_31' );
		 ms_thirtyone=validateValue(ms_thirtyone);
				
         var onair_date_thirtyone=31+'/'+ms_month+'/'+ms_year;
		 
		 var record_changeinmediaplan_status_thirtyone=changeinmediaplan_record_status(recordID,onair_date_thirtyone);
        
		  
		 if(record_changeinmediaplan_status_thirtyone==false && ms_thirtyone!='')
		 {
		 	
			var check_thirtyone=changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_thirtyone,old_thirtyone,ms_thirtyone,media_type_tv, old_mediaID);
			nlapiLogExecution('DEBUG','suiteletFunction','  check_thirtyone [ Media Schedule ]--->'+check_thirtyone);
		 	
		 }	
		 
		 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		
			  
			  }
		   
			
		}
	}
	
		
		
		
 }

  


}

// END SCHEDULED FUNCTION ===============================================




function validateValue(value)
{
	if(value==null || value ==undefined || value=='')
	{
		value=''
	}
	return value;
}




//function changeInmediaPlanRecord(old_one,ms_one,media_type_tv, mediaplanID)




function changeInmediaPlanRecord(ms_status,old_tv_prog,old_month,old_year,ms_tv_program,onair_date_one,old_one,ms_one,media_type_tv, mediaplanID)
{
	 nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias old_one --> ' + old_one)
	  nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias ms_one --> ' + ms_one)
	
	
	var submit_F_ID;
	var aliasQuantity = ms_one.split(',')
    nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
	
	for (var k = 0; k < aliasQuantity.length; k++) 
	{
		var alias_ms = aliasQuantity[k];
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
		
		var letter = "";
		var number = "";
		
		for (var q = 0; q < alias_ms.length; q++) 
		{
			var b = alias_ms.charAt(q);
			if (isNaN(b) == true) 
			{
				letter = letter + b;
			}
			else 
			{
				number = number + b;
			}
		}
		
		
		
		
		
	var aliasQuantity_old = old_one.split(',')
    nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias aliasQuantity_old Length --> ' + aliasQuantity_old.length)
	
	for (var hh = 0; hh < aliasQuantity_old.length; hh++) 
	{
		var alias_ms_old = aliasQuantity_old[hh];
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms_old --> ' + alias_ms_old)
		
		var letterold = "";
		var numberold = "";
		
		for (var jq = 0; jq < alias_ms_old.length; jq++)
		{
			var bj = alias_ms_old.charAt(jq);
			if (isNaN(bj) == true)
			 {
				letterold = letterold + bj;
			}
			else
			 {
				numberold = numberold + bj;
			}
		}
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'letterold=' + letterold)
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'numberold=' + numberold)
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'letter=' + letter)
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'number=' + number)
		
		
		if((letterold==letter) && (numberold!=number))
		{
			 var medialist_result=searchMedialist(letter, media_type_tv,mediaplanID);
	         nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media List Result ='+medialist_result);
			 
			  if (medialist_result != null && medialist_result != '' && medialist_result != undefined)
			  {
			  	var mlArr = new Array();
			  	mlArr = medialist_result.split('##');
			  	var ml_recordID = mlArr[0];
			  	var ml_productname = mlArr[1];
			  	var ml_medianame = mlArr[2];
			  	var ml_newduration = mlArr[3];
			  	
			  	nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
			  	nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
			  	nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
			  	nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration);
				
				
				var old_duration=ml_newduration*numberold;
				var new_duration=ml_newduration*number;
				
				
				
				
				
				//================= Create REcord ============================
					
			
				 var record_f=nlapiCreateRecord('customrecord_chnge_mediaplan');
				 nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
								 
				 record_f.setFieldValue('custrecord_chnge_mp_tvprogram', old_tv_prog);
				 record_f.setFieldValue('custrecord_chnge_mp_oldproductname', ml_productname);
				 record_f.setFieldValue('custrecord_chnge_mp_changetype', ms_status);
				 record_f.setFieldValue('custrecord_chnge_mp_olddate', onair_date_one);
				 record_f.setFieldValue('custrecord_chnge_mp_oldspotname', ml_medianame);
				 record_f.setFieldValue('custrecord_chnge_mp_oldlength', old_duration);
				 record_f.setFieldValue('custrecord_chnge_mp_newdate', onair_date_one);
				 record_f.setFieldValue('custrecord_chnge_mp_newspotname', ml_medianame);
				 record_f.setFieldValue('custrecord_chnge_mp_newlength', new_duration);
				 record_f.setFieldValue('custrecord_chnge_mp_msref', recordID);
				 
				 
				 submit_F_ID = nlapiSubmitRecord(record_f, true, true);
				 nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
				
				//=================== Create Record ==============================
				
			  }
			
		}
		
		else if ((letterold != letter)&& letter != '' )
		{
			
			
				
			// ===============================  New Letter ====================================
			
			var medialist_result = searchMedialist(letter, media_type_tv, mediaplanID);
			nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
			
			if (medialist_result != null && medialist_result != '' && medialist_result != undefined) 
			{
				var mlArr = new Array();
				mlArr = medialist_result.split('##');
				var ml_recordID = mlArr[0];
				var ml_productname = mlArr[1];
				var ml_medianame = mlArr[2];
				var ml_newduration = mlArr[3];
				
				nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
				nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
				nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
				nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration);
				
				
				var old_duration = ml_newduration * numberold;
				var new_duration = ml_newduration * number;
				
				
				
				//================= Create REcord ============================
				
				
				var record_f = nlapiCreateRecord('customrecord_chnge_mediaplan');
				nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
				
				record_f.setFieldValue('custrecord_chnge_mp_tvprogram', old_tv_prog);
				record_f.setFieldValue('custrecord_chnge_mp_oldproductname', ml_productname);
				record_f.setFieldValue('custrecord_chnge_mp_changetype', ms_status);
				record_f.setFieldValue('custrecord_chnge_mp_olddate', '');
				record_f.setFieldValue('custrecord_chnge_mp_oldspotname', '');
				record_f.setFieldValue('custrecord_chnge_mp_oldlength', '');
				record_f.setFieldValue('custrecord_chnge_mp_newdate', onair_date_one);
				record_f.setFieldValue('custrecord_chnge_mp_newspotname', ml_medianame);
				record_f.setFieldValue('custrecord_chnge_mp_newlength', new_duration);
				record_f.setFieldValue('custrecord_chnge_mp_msref', recordID);
				
				submit_F_ID = nlapiSubmitRecord(record_f, true, true);
				nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
				
				
				
			//=================== Create Record ==============================
			
			
		if(old_one.indexOf(ms_one)!=-1)
			{	
					
			//================= Create REcord ============================
				
				
				var medialist_result_old = searchMedialist(letterold, media_type_tv, mediaplanID);
				nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
				
				if (medialist_result_old != null && medialist_result_old != '' && medialist_result_old != undefined) 
				{
					var mlArr_old = new Array();
					mlArr_old = medialist_result.split('##');
					var ml_recordID_old = mlArr_old[0];
					var ml_productname_old = mlArr_old[1];
					var ml_medianame_old = mlArr_old[2];
					var ml_newduration_old = mlArr_old[3];
					
					nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID_old = ' + ml_recordID_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_productname_old = ' + ml_productname_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame_old = ' + ml_medianame_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration_old = ' + ml_newduration_old);
					
					
					
					
					
					
					if (letterold != '') 
					{
					
					
						var record_f = nlapiCreateRecord('customrecord_chnge_mediaplan');
						nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
						
						record_f.setFieldValue('custrecord_chnge_mp_tvprogram', old_tv_prog);
						record_f.setFieldValue('custrecord_chnge_mp_oldproductname', ml_productname_old);
						record_f.setFieldValue('custrecord_chnge_mp_changetype', ms_status);
						record_f.setFieldValue('custrecord_chnge_mp_olddate', onair_date_one);
						record_f.setFieldValue('custrecord_chnge_mp_oldspotname', ml_medianame_old);
						record_f.setFieldValue('custrecord_chnge_mp_oldlength', old_duration);
						record_f.setFieldValue('custrecord_chnge_mp_newdate', '');
						record_f.setFieldValue('custrecord_chnge_mp_newspotname', '');
						record_f.setFieldValue('custrecord_chnge_mp_newlength', '');
						record_f.setFieldValue('custrecord_chnge_mp_msref', recordID);
						
						submit_F_ID = nlapiSubmitRecord(record_f, true, true);
						nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
						
						
					}
				//=================== Create Record ==============================
				}
			
			}
				
			}
			
		
		}
		
		else 
			if ((letterold != letter) && letterold != '')
			 {
			 	
					
				// ===============================  Old Letter ====================================
				
				
				
				var medialist_result_old = searchMedialist(letterold, media_type_tv, mediaplanID);
				nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
				
				if (medialist_result_old != null && medialist_result_old != '' && medialist_result_old != undefined) 
				{
					var mlArr_old = new Array();
					mlArr_old = medialist_result.split('##');
					var ml_recordID_old = mlArr_old[0];
					var ml_productname_old = mlArr_old[1];
					var ml_medianame_old = mlArr_old[2];
					var ml_newduration_old = mlArr_old[3];
					
					nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID_old = ' + ml_recordID_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_productname_old = ' + ml_productname_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame_old = ' + ml_medianame_old);
					nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration_old = ' + ml_newduration_old);
					
					
					var old_duration = ml_newduration * numberold;
					var new_duration = ml_newduration * number;
					
				
			
				var record_f = nlapiCreateRecord('customrecord_chnge_mediaplan');
					nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
					
					record_f.setFieldValue('custrecord_chnge_mp_tvprogram', old_tv_prog);
					record_f.setFieldValue('custrecord_chnge_mp_oldproductname', ml_productname_old);
					record_f.setFieldValue('custrecord_chnge_mp_changetype', ms_status);
					record_f.setFieldValue('custrecord_chnge_mp_olddate', onair_date_one);
					record_f.setFieldValue('custrecord_chnge_mp_oldspotname', ml_medianame_old);
					record_f.setFieldValue('custrecord_chnge_mp_oldlength', old_duration);
					record_f.setFieldValue('custrecord_chnge_mp_newdate', '');
					record_f.setFieldValue('custrecord_chnge_mp_newspotname', '');
					record_f.setFieldValue('custrecord_chnge_mp_newlength', '');
					record_f.setFieldValue('custrecord_chnge_mp_msref', recordID);
					
					submit_F_ID = nlapiSubmitRecord(record_f, true, true);
					nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
				
				
			    if(old_one.indexOf(ms_one)!=-1)
				{
				if (letter != '') 
				{
					var medialist_result = searchMedialist(letter, media_type_tv, mediaplanID);
					nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
					
					if (medialist_result != null && medialist_result != '' && medialist_result != undefined) 
					{
						var mlArr = new Array();
						mlArr = medialist_result.split('##');
						var ml_recordID = mlArr[0];
						var ml_productname = mlArr[1];
						var ml_medianame = mlArr[2];
						var ml_newduration = mlArr[3];
						
						nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
						nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
						nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
						nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration);
						
						
						var old_duration = ml_newduration * numberold;
						var new_duration = ml_newduration * number;
						
						
						//================= Create REcord ============================
						
						
						var record_f = nlapiCreateRecord('customrecord_chnge_mediaplan');
						nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
						
						record_f.setFieldValue('custrecord_chnge_mp_tvprogram', old_tv_prog);
						record_f.setFieldValue('custrecord_chnge_mp_oldproductname',ml_productname);
						record_f.setFieldValue('custrecord_chnge_mp_changetype', ms_status);
						record_f.setFieldValue('custrecord_chnge_mp_olddate', '');
						record_f.setFieldValue('custrecord_chnge_mp_oldspotname', '');
						record_f.setFieldValue('custrecord_chnge_mp_oldlength', '');
						record_f.setFieldValue('custrecord_chnge_mp_newdate', onair_date_one);
						record_f.setFieldValue('custrecord_chnge_mp_newspotname', ml_medianame);
						record_f.setFieldValue('custrecord_chnge_mp_newlength', new_duration);
						record_f.setFieldValue('custrecord_chnge_mp_msref', recordID);
						
						submit_F_ID = nlapiSubmitRecord(record_f, true, true);
						nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
						
						
					//=================== Create Record ==============================
					
					
					}
				}	
				
				}//If
					
				}
				
			
				
				
				
			}//If-1
					
			

		}//number old
		
		
	}//number new
		
		
		
		return submit_F_ID;
			
}//Main


function searchMedialist(letter, media_type_tv,recordID)
{
//	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
//	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
//	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
//	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
	
	
	var ml_recordID;
	var ml_productname;
	var ml_medianame;
	var ml_newduration;
	var result;
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
	filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
	filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
	columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
	columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
	
	var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);

	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
		
		for (var j = 0; j < searchresults.length; j++) 
		{
			ml_recordID = searchresults[j].getValue('internalid');
		//	nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
			
			ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
		//	nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
		
		    ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
		//	nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
			
			ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
		//	nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' +j + ']==' + ml_newduration);
		
		    result=ml_recordID+'##'+ml_productname+'##'+ml_medianame+'##'+ml_newduration;
		   
		}
		
	}
	
   	return result;
	
}



function changeinmediaplan_record_status(recordID,onair_date_one)
{
	var result=false;
	if(onair_date_one!=null && onair_date_one!='' )
	{
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('custrecord_chnge_mp_msref', null, 'is', recordID);
		filter[1] = new nlobjSearchFilter('custrecord_chnge_mp_changetype', null, 'is', 8);
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_chnge_mp_newdate');
		columns[2] = new nlobjSearchColumn('custrecord_chnge_mp_olddate');
		
		var searchresults = nlapiSearchRecord('customrecord_chnge_mediaplan', null, filter, columns);
		
		if (searchresults != null)
		{
			nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
			
			for (var h = 0; h < searchresults.length; h++) 
			{
				f_recordID = searchresults[h].getValue('internalid');
				nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ F Appendix][' + h + ']==' + f_recordID);
				
				f_newdate = searchresults[h].getValue('custrecord_chnge_mp_newdate');
				nlapiLogExecution('DEBUG', 'searchMedialist', '  New Date ID  [ F Appendix][' + h + ']==' + f_newdate);
				
				f_olddate = searchresults[h].getValue('custrecord_chnge_mp_olddate');
				nlapiLogExecution('DEBUG', 'searchMedialist', '  f_olddate Date ID  [ F Appendix][' + h + ']==' + f_olddate);
				
				 if(f_newdate!=null && f_olddate!=null)
			   {
			   	 if(f_newdate==onair_date_one  || f_olddate==onair_date_one)
				{
					result=true;
				}
			   	
			  }
				
			}
			return result;
		}
		return false;
	}
	
	
	
	
	
}















/*

function changeinmediaplan_record_status(recordID,onair_date_one)
{
	nlapiLogExecution('DEBUG', 'searchMedialist', ' In media change function...........');
	var result;
	var f_recordID;
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_chnge_mp_msref', null, 'is', recordID);
	filter[1] = new nlobjSearchFilter('custrecord_chnge_mp_changetype', null, 'is', 8);
	//filter[1] = new nlobjSearchFilter('custrecord_chnge_mp_olddate', null, 'is', onair_date_one);
	//filter[2] = new nlobjSearchFilter('custrecord_chnge_mp_newdate', null, 'is', onair_date_one);	
	//filter[3] = new nlobjSearchFilter('custrecord_chnge_mp_changetype', null, 'is', 8);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	
	
	
	var searchresults = nlapiSearchRecord('customrecord_chnge_mediaplan', null, filter, columns);
	
	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
		
		for (var h = 0; h < searchresults.length; h++) 
		{
			f_recordID = searchresults[h].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ F Appendix][' + h + ']==' + f_recordID);
			
			f_newdate = searchresults[h].getValue('custrecord_chnge_mp_newdate');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  New Date ID  [ F Appendix][' + h + ']==' + f_newdate);
		   
		    f_olddate = searchresults[h].getValue('custrecord_chnge_mp_olddate');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  f_olddate Date ID  [ F Appendix][' + h + ']==' + f_olddate);
		
		
		   if(f_newdate!=null && f_olddate!=null)
		   {
		   	 if(f_newdate==onair_date_one  && f_olddate==onair_date_one)
			{
				result=true;
			}
		   	
		  }
			
		}
		
		return result;
	}
	else
	{
		return false;
	}
		
}

*/


