/**
 * @author Shweta
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Appendix_K.js
	Date        : 5 May 2013
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================


function suiteletFunction(request, response)
{
	try
	{
		if (request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm('Appendix K Print');
	
	        
	         
			var department = form.addField('custpage_department', 'select', 'Department','department');
			department.setDefaultValue(8);
			department.setDisplayType('inline');
					
			
			var year = form.addField('custpage_year', 'select', 'Year', 'customlist65');
			year.setMandatory(true);
			
			var start_date = form.addField('custpage_startdate', 'date', 'Start Date');
			start_date.setDisplayType('disabled');
			start_date.setDisplayType('hidden');
			
			var end_date = form.addField('custpage_enddate', 'date', 'End Date');
			end_date.setDisplayType('disabled');
			end_date.setDisplayType('hidden');
			
			form.setScript('customscript_selectmonth_appendix_k');
			
			var submit_button = form.addSubmitButton();
			response.writePage(form);
			
			
		}//GET
		else if (request.getMethod() == 'POST')
		{
			var post_department = request.getParameter('custpage_department');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Department --> '+post_department);
						
			var post_year = request.getParameter('custpage_year');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Year --> '+post_year);
			
			var post_start_date = request.getParameter('custpage_startdate');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Start Date --> '+post_start_date);
			
			var post_end_date = request.getParameter('custpage_enddate');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' End Date --> '+post_end_date);
			
			
			var params=new Array();

	  		params['status']='scheduled';
	  		params['runasadmin']='T';
		  	params['custscript_k_department']=post_department;
		  	params['custscript_k_year']=post_year;
			params['custscript_k_start_date']=post_start_date;
			params['custscript_k_end_date']=post_end_date;
		  	var startDate = new Date();
		  	params['startdate']=startDate.toUTCString();

		  	var status = nlapiScheduleScript('customscript_sch_appendix_k_report','customdeploy1',params);
	  		nlapiLogExecution('DEBUG', 'In Suitelet Post Method', ' Status : '+status);
			
			
		}//POST
	}//TRY
	catch(er)
	{
		nlapiLogExecution('DEBUG', 'suiteletFunction', 'Exception Caught -->' + er);
	}//CATCH
	

}// Suitelet Function

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
