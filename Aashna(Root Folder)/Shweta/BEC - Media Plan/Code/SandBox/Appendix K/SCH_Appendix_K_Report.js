/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Appendix_K_Report.js
	Author:
	Company:
	Date:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	
	var media_type_tv;
	var ms_tv_program_item;
	
	var alias_1;
	var alias_2;
	var alias_3;
	var alias_4;
	var alias_5;
	var alias_6;
	var alias_7;
	var alias_8;
	var alias_9;
	var alias_10;
	
	var alias_11;
	var alias_12;
	var alias_13;
	var alias_14;
	var alias_15;
	var alias_16;
	var alias_17;
	var alias_18;
	var alias_19;
	var alias_20;
	
	var alias_21;
	var alias_22;
	var alias_23;
	var alias_24;
	var alias_25;
	var alias_26;
	var alias_27;
	var alias_28;
	var alias_29;
	var alias_30;
	
	var alias_31;
	
	var sales_order_id;
	var media_plan_id;

	var opputunity_id;
	var sales_rep_text;								
	var sales_result;	
	var sales_net_amount;	
	var sales_net_quantity;											
	var ms_month;
	var alias;
	
	var print_arr=new Array();
	var pnt=0;
	
	var print_opp_arr=new Array();
	var opp_pnt=0;
	
	var opp_date;
				
													
	
	try
	{
		var context = nlapiGetContext();
    var remainingUsage = context.getRemainingUsage();
    
    var department = context.getSetting('SCRIPT', 'custscript_k_department');
    var year = context.getSetting('SCRIPT', 'custscript_k_year');
	var start_date = context.getSetting('SCRIPT', 'custscript_k_start_date');
	var end_date = context.getSetting('SCRIPT', 'custscript_k_end_date');
  
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Department --> ' + department)
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Year --> ' + year)
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' Start Date --> ' + start_date)
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' End Date --> ' + end_date)
	
	 if(department!=null && department!='' && department!=undefined)
	 {
	 	if(year!=null && year!='' && year!=undefined)
		{
			if(start_date!=null && start_date!='' && start_date!=undefined)
			{
				if(end_date!=null && end_date!='' && end_date!=undefined)
				{
					
				var  searchresults = nlapiSearchRecord('opportunity','customsearch625', null, null);
					
				for (var i = 0; searchresults != null && i < searchresults.length; i++) 
				{
					
				   
					
				nlapiLogExecution('DEBUG', 'Opportunity: Results - 625 ', ' Opportunity Search Results --> ' + searchresults.length);
				
				var search_opp_result = searchresults[i];
                var columns = search_opp_result.getAllColumns();
                var columnLen = columns.length;
                //nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', 'i -->' + i);
				
				var column1 = columns[0];
                sales_rep_ID = search_opp_result.getValue(column1);
                //nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', 'Sales Rep ID -->' + sales_rep_ID);
              				
                sales_rep_text = search_opp_result.getText(column1);
                nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', ' Sales Rep Text -->' + sales_rep_text);              
			
			    var year_txt=year_text(year);
				nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', ' Year Text -->' + year_txt);              
				
				if(sales_rep_ID!=null && sales_rep_ID!='')
				{
					 var total_alias_amount=0;
					 
					var jan_total=0 ;
					var feb_total=0 ;
					var mar_total=0 ;
					var apr_total=0 ;
					var may_total =0;
					var jun_total=0 ;
					var july_total=0 ;
					var aug_total=0 ;
					var sep_total=0 ;
					var oct_total=0 ;
					var nov_total =0;
					var dec_total =0;	
					
					var jan_opp_total=0 ;
					var feb_opp_total=0 ;
					var mar_opp_total=0 ;
					var apr_opp_total=0 ;
					var may_opp_total =0;
					var jun_opp_total=0 ;
					var july_opp_total=0 ;
					var aug_opp_total=0 ;
					var sep_opp_total=0 ;
					var oct_opp_total=0 ;
					var nov_opp_total =0;
					var dec_opp_total =0;	            
																			
					var O_filters = new Array();
						                    
	                O_filters[0] = new nlobjSearchFilter('salesrep', null, 'is', sales_rep_ID)
					O_filters[1] = new nlobjSearchFilter('trandate',null, 'onorafter', start_date)
					O_filters[2] = new nlobjSearchFilter('trandate',null, 'onorbefore', end_date) 
									
					var search_opportunity_results = nlapiSearchRecord('opportunity', 'customsearch626', O_filters, null);
					
					if (search_opportunity_results != null) 
					{
						nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', ' Opportunity Search Results --> ' + search_opportunity_results.length);
						
						for (var g = 0; search_opportunity_results != null && g < search_opportunity_results.length; g++) 
						{
							var opputunity_amount=0;
							
							var opp_result = search_opportunity_results[g];
				            var columns_opp = opp_result.getAllColumns();
				            var column_opp_len = columns_opp.length;
				            
				            // Opportunity Internal ID
				            var column_opp_1 = columns_opp[0];
				            opputunity_id = opp_result.getValue(column_opp_1);
				           nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', '**************** Oppurtunity Record ID [' + g + ' ] ****************--> ' + opputunity_id);
							
							
							// Amount 
				            var column_opp_2 = columns_opp[2];
				            opputunity_amount = opp_result.getValue(column_opp_2);
				          //  opputunity_amount=opputunity_amount.toFixed(2);
							nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', '**************** Oppurtunity Amount [' + g + ' ] ****************--> ' + opputunity_amount);
											
							var opp_OBJ=nlapiLoadRecord('opportunity',opputunity_id)
							
							opp_date=opp_OBJ.getFieldValue('trandate');
							//nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', ' Oppurtunity Date [' + g + ' ] --> ' + opp_date);
							
							var MPfilters = new Array();
				
							var MPColoumns = new Array();
			               
			                    
			                MPfilters[0] = new nlobjSearchFilter('custrecord_mp1_salesrep', null, 'is', sales_rep_ID)
							MPfilters[1] = new nlobjSearchFilter('custrecord_mp1_oa_startdate',null, 'onorafter', start_date)
							MPfilters[2] = new nlobjSearchFilter('custrecord_mp1_oa_startdate',null, 'onorbefore', end_date) 
							MPfilters[3] = new nlobjSearchFilter('custrecord_mp1_oa_enddate',null, 'onorafter', start_date)
							MPfilters[4] = new nlobjSearchFilter('custrecord_mp1_oa_enddate',null, 'onorbefore', end_date) 
							
							var search_media_plan_results = nlapiSearchRecord('customrecord_mediaplan_1', 'customsearch628', MPfilters, null);
							
							 if (search_media_plan_results != null) 
							 {
							 	nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Media Plan Search Results --> ' + search_media_plan_results.length);
							 	
								 for (var t = 0; search_media_plan_results != null && t < search_media_plan_results.length; t++) 
								 {
								 								 
								  
									
								 	var media_plan_result = search_media_plan_results[t];
						            var columns_mp = media_plan_result.getAllColumns();
						            var column_mp_len = columns_mp.length;
						            
						            //Media Plan Internal ID
						            var column_mp_1 = columns_mp[0];
						            media_plan_id = media_plan_result.getValue(column_mp_1);
						            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', '**************** Media Record ID [' + t + ' ] ****************--> ' + media_plan_id);
									 	
							 	   //SO Reference
						            var column_mp_4 = columns_mp[5];
						            sales_order_id = media_plan_result.getValue(column_mp_4);
						            //////nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' ************ Sales order ID [' + t + ' ] --> ' + sales_order_id);
								
									
									 // Alias
							           var column_mp_7 = columns_mp[10];
							           alias = media_plan_result.getValue(column_mp_7);
							           ////nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Alias [' + t + ' ] --> ' + alias);	
											
									
									
									var MS_filters = new Array();
						
									var MS_coloumns = new Array();
					               		                    
					               
									MS_filters[0] = new nlobjSearchFilter('custrecord_mp3_year',null,'is',year);
									MS_filters[1] = new nlobjSearchFilter('custrecord_mp3_mp1ref',null,'is',media_plan_id);
									
									
									var search_media_sch_results = nlapiSearchRecord('customrecord_mediaplan_3', 'customsearch_media_schedule_year_wise', MS_filters, null);
									
									if (search_media_sch_results != null)
									{
										//nlapiLogExecution('DEBUG', 'Media Schedule : Results - 627 ', ' Media Schedule Search Results --> ' + search_media_sch_results.length);
										
										for (var z = 0; search_media_sch_results != null && z < search_media_sch_results.length; z++)
										{
											
											 
											
												
												    var amount_1=0;
													var amount_2=0;
													var amount_3=0;
													var amount_4=0;
													var amount_5=0;
													var amount_6=0;
													var amount_7=0;
													var amount_8=0;
													var amount_9=0;
													var amount_10=0;
												
													var amount_11=0;
													var amount_12=0;
													var amount_13=0;
													var amount_14=0;
													var amount_15=0;
													var amount_16=0;
													var amount_17=0;
													var amount_18=0;
													var amount_19=0;
													var amount_20=0;
													
													var amount_21=0;
													var amount_22=0;
													var amount_23=0;
													var amount_24=0;
													var amount_25=0;
													var amount_26=0;
													var amount_27=0;
													var amount_28=0;
													var amount_29=0;
													var amount_30=0;
													var amount_31=0;
																								
											
											
											var media_sch_result = search_media_sch_results[z];
									 		var columns_msch = media_sch_result.getAllColumns();
									 		var column_msch_len = columns_msch.length;
											
											
											//================= Media Schedule ID ===============
											
											var column_msch_0 = columns_msch[0];
											var media_sch_id = media_sch_result.getValue(column_msch_0);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', 'Media Schedule  ID [' + z + ' ] -->' + media_sch_id);
														
											
											//==================================  1  ==================================
											
											var column_msch_1 = columns_msch[8];
											alias_1 = media_sch_result.getValue(column_msch_1);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 1 [' + z + ' ] -->' + alias_1);
											
											
											//==================================  2  ==================================
											
											var column_msch_2 = columns_msch[9];
											alias_2 = media_sch_result.getValue(column_msch_2);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 2 [' + z + ' ] -->' + alias_2);
											
											
											//==================================  3  ==================================
											
											var column_msch_3 = columns_msch[10];
											alias_3 = media_sch_result.getValue(column_msch_3);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 3 [' + z + ' ] -->' + alias_3);
											
											
											
											//==================================  4  ==================================
											
											var column_msch_4 = columns_msch[11];
											alias_4 = media_sch_result.getValue(column_msch_4);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 4 [' + z + ' ] -->' + alias_4);
											
											
											
											//==================================  5  ==================================
											
											var column_msch_5 = columns_msch[12];
											alias_5 = media_sch_result.getValue(column_msch_5);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 5 [' + z + ' ] -->' + alias_5);
											
											
											
											//==================================  6  ==================================
											
											var column_msch_6 = columns_msch[13];
											alias_6 = media_sch_result.getValue(column_msch_6);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 6 [' + z + ' ] -->' + alias_6);
											
											
											//==================================  7  ==================================
											
											var column_msch_7 = columns_msch[14];
											alias_7 = media_sch_result.getValue(column_msch_7);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 7 [' + z + ' ] -->' + alias_7);
											
											
											
											//==================================  8  ==================================
											
											var column_msch_8 = columns_msch[15];
											alias_8 = media_sch_result.getValue(column_msch_8);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 8 [' + z + ' ] -->' + alias_8);
											
											
											//==================================  9  ==================================
											
											var column_msch_9 = columns_msch[16];
											alias_9 = media_sch_result.getValue(column_msch_9);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 9 [' + z + ' ] -->' + alias_9);
											
											
											//==================================  10  ==================================
											
											var column_msch_10 = columns_msch[17];
											alias_10 = media_sch_result.getValue(column_msch_10);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 10 [' + z + ' ] -->' + alias_10);
											
											
											
											
											//==================================  11  ==================================
											
											var column_msch_11 = columns_msch[18];
											alias_11 = media_sch_result.getValue(column_msch_11);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 11 [' + z + ' ] -->' + alias_11);
											
											
											
											//==================================  12  ==================================
											
											var column_msch_12 = columns_msch[19];
											alias_12 = media_sch_result.getValue(column_msch_12);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 12 [' + z + ' ] -->' + alias_12);
											
											
											//==================================  13  ==================================
											
											var column_msch_13 = columns_msch[20];
											alias_13 = media_sch_result.getValue(column_msch_13);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 13 [' + z + ' ] -->' + alias_13);
											
											
											//==================================  14  ==================================
											
											var column_msch_14 = columns_msch[21];
											alias_14 = media_sch_result.getValue(column_msch_14);
											//////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 14 [' + z + ' ] -->' + alias_14);
											
											
											//==================================  15  ==================================
											
											var column_msch_15 = columns_msch[22];
											alias_15 = media_sch_result.getValue(column_msch_15);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 15 [' + z + ' ] -->' + alias_15);
											
											
											//==================================  16  ==================================
											
											var column_msch_16 = columns_msch[23];
											alias_16 = media_sch_result.getValue(column_msch_16);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 16 [' + z + ' ] -->' + alias_16);
											
											
											//==================================  17  ==================================
											
											var column_msch_17 = columns_msch[24];
											alias_17 = media_sch_result.getValue(column_msch_17);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 17 [' + z + ' ] -->' + alias_17);
											
											//==================================  18  ==================================
											
											var column_msch_18 = columns_msch[25];
											alias_18 = media_sch_result.getValue(column_msch_18);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 18 [' + z + ' ] -->' + alias_18);
											
											
											
											//==================================  19  ==================================
											
											var column_msch_19 = columns_msch[26];
											alias_19 = media_sch_result.getValue(column_msch_19);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 19 [' + z + ' ] -->' + alias_19);
											
											
											//==================================  20  ==================================
											
											var column_msch_20 = columns_msch[27];
											alias_20 = media_sch_result.getValue(column_msch_20);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 20 [' + z + ' ] -->' + alias_20);
											
											
											//==================================  21  ==================================
											
											var column_msch_21 = columns_msch[28];
											alias_21 = media_sch_result.getValue(column_msch_21);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 21 [' + z + ' ] -->' + alias_21);
											
											
											
											
											
											//==================================  22  ==================================
											
											var column_msch_22 = columns_msch[29];
											alias_22 = media_sch_result.getValue(column_msch_22);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 22 [' + z + ' ] -->' + alias_22);
											
											
											
												
											//==================================  23  ==================================
											
											var column_msch_23 = columns_msch[30];
											alias_23 = media_sch_result.getValue(column_msch_23);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 23 [' + z + ' ] -->' + alias_23);
											
											
											
											
											//==================================  24  ==================================
											
											var column_msch_24 = columns_msch[31];
											alias_24 = media_sch_result.getValue(column_msch_24);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 24 [' + z + ' ] -->' + alias_24);
											
											
											//==================================  25  ==================================
											
											var column_msch_25 = columns_msch[32];
											alias_25 = media_sch_result.getValue(column_msch_25);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 25 [' + z + ' ] -->' + alias_25);
											
											
											//==================================  26  ==================================
											
											var column_msch_26 = columns_msch[33];
											alias_26 = media_sch_result.getValue(column_msch_26);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 26 [' + z + ' ] -->' + alias_26);
											
											//==================================  27  ==================================
											
											var column_msch_27 = columns_msch[34];
											alias_27 = media_sch_result.getValue(column_msch_27);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 27 [' + z + ' ] -->' + alias_27);
											
											
											//==================================  28  ==================================
											
											var column_msch_28 = columns_msch[35];
											alias_28 = media_sch_result.getValue(column_msch_28);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 28 [' + z + ' ] -->' + alias_28);
											
											
											//==================================  29  ==================================
											
											var column_msch_29 = columns_msch[36];
											alias_29 = media_sch_result.getValue(column_msch_29);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 29 [' + z + ' ] -->' + alias_29);
											
											
											//==================================  30  ==================================
											
											var column_msch_30 = columns_msch[37];
											alias_30 = media_sch_result.getValue(column_msch_30);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 30 [' + z + ' ] -->' + alias_30);
											
											
											
											//==================================  31  ==================================
											
											var column_msch_31 = columns_msch[38];
											alias_31 = media_sch_result.getValue(column_msch_31);
											////nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Alias 31 [' + z + ' ] -->' + alias_31);
											
																						
											//================= TV Program ID ===============
											
											var column_msch_32 = columns_msch[2];
											ms_tv_program_item = media_sch_result.getValue(column_msch_32);
											//nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' TV Program   ID [' + z + ' ] -->' + ms_tv_program_item);				
														
											
											
										 	//=================  Month ===============
											
											var column_msch_33 = columns_msch[7];
											ms_month = media_sch_result.getValue(column_msch_33);
											//nlapiLogExecution('DEBUG', 'Media Schedule : Results   ', ' Month [' + z + ' ] -->' + ms_month);				
														
											
											
											
											// ==================================  TV Item Type ===================================
																		
																										
											if(ms_tv_program_item!=null && ms_tv_program_item!=''&& ms_tv_program_item!=undefined)
											{
												var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_tv_program_item);
																				    
												if(tv_programOBJ!=null)
												{
													media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
													//nlapiLogExecution('DEBUG',' TV Type','  TV Program Media Type [ Media Schedule ]-->'+media_type_tv);
													
													
												}//TV Program Object
											
											}//TV Item Type
											
											
											//========================      Sales Order Amount ===================
																		
											sales_result=sales_order_amount(sales_order_id,ms_tv_program_item);
				                            //nlapiLogExecution('DEBUG',' Sales Order ',' Sales Result --> '+sales_result);
											
											
											var sales_Arr = new Array();
											
											if(sales_result!=null && sales_result!=undefined && sales_result!='')
											{
												sales_Arr = sales_result.split('%%');
								                sales_net_amount = sales_Arr[0];
								                sales_net_quantity = sales_Arr[1];
													
												
											}
											
							               
							               	//nlapiLogExecution('DEBUG','Media Schedule',' Sales Net Amount--> '+sales_net_amount);
											//nlapiLogExecution('DEBUG','Media Schedule',' Sales Net Quantity  --> '+sales_net_quantity);	
																									
											
											
											
											 if (sales_net_quantity != undefined && sales_net_quantity != '' && sales_net_quantity != null) 
											 {
											 	 if (sales_net_quantity != undefined && sales_net_quantity != '' && sales_net_quantity != null) 
												 {
												 	
													
													//========================= 1 ====================================
													
													if(alias_1!=null && alias_1!='' && alias_1!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_1=get_aliasduration(alias_1, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 1-->'+alias_duration_1)
														
														var alias_number_1=get_alias(alias_1, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 1 -->'+alias_number_1)
							
														amount_1=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_1,alias_number_1)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 1 -->'+amount_1)
																																			
													}//1.............
													
													
													//========================= 2 ====================================
													
													if(alias_2!=null && alias_2!='' && alias_2!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_2=get_aliasduration(alias_2, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 2-->'+alias_duration_2)
														
														var alias_number_2=get_alias(alias_2, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 2 -->'+alias_number_2)
							
														amount_2=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_2,alias_number_2)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 2 -->'+amount_2)
																																			
													}//2.............
													
													
													//========================= 3 ====================================
													
													if(alias_3!=null && alias_3!='' && alias_3!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_3=get_aliasduration(alias_3, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 3-->'+alias_duration_3)
														
														var alias_number_3=get_alias(alias_3, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 3 -->'+alias_number_3)
							
														amount_3=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_3,alias_number_3)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 3 -->'+amount_3)
																																			
													}//3.............
													
													
													//========================= 4 ====================================
													
													if(alias_4!=null && alias_4!='' && alias_4!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_4=get_aliasduration(alias_4, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 4-->'+alias_duration_4)
														
														var alias_number_4=get_alias(alias_4, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 4 -->'+alias_number_4)
							
														amount_4=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_4,alias_number_4)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 4 -->'+amount_4)
																																			
													}//4.............
													
													
													//========================= 5 ====================================
													
													if(alias_5!=null && alias_5!='' && alias_5!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_5=get_aliasduration(alias_5, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 5-->'+alias_duration_5)
														
														var alias_number_5=get_alias(alias_5, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 5 -->'+alias_number_5)
							
														amount_5=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_5,alias_number_5)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 5 -->'+amount_5)
																																			
													}//5.............
													
													
													//========================= 6 ====================================
													
													if(alias_6!=null && alias_6!='' && alias_6!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_6=get_aliasduration(alias_6, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 6-->'+alias_duration_6)
														
														var alias_number_6=get_alias(alias_6, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 6 -->'+alias_number_6)
							
														amount_6=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_6,alias_number_6)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 6 -->'+amount_6)
																																			
													}//6.............
													
													//========================= 7 ====================================
													
													if(alias_7!=null && alias_7!='' && alias_7!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_7=get_aliasduration(alias_7, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 7-->'+alias_duration_7)
														
														var alias_number_7=get_alias(alias_7, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 7 -->'+alias_number_7)
							
														amount_7=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_7,alias_number_7)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 7 -->'+amount_7)
																																			
													}//7.............
													
													
													
													//========================= 8 ====================================
													
													if(alias_8!=null && alias_8!='' && alias_8!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_8=get_aliasduration(alias_8, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 8-->'+alias_duration_8)
														
														var alias_number_8=get_alias(alias_8, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 8 -->'+alias_number_8)
							
														amount_8=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_8,alias_number_8)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 8 -->'+amount_8)
																																			
													}//8.............
													
													
													
													//========================= 9 ====================================
													
													if(alias_9!=null && alias_9!='' && alias_9!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_9=get_aliasduration(alias_9, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 9-->'+alias_duration_9)
														
														var alias_number_9=get_alias(alias_9, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 9 -->'+alias_number_9)
							
														amount_9=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_9,alias_number_9)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 9 -->'+amount_9)
																																			
													}//9.............
													
													
													
														//========================= 10 ====================================
													
													if(alias_10!=null && alias_10!='' && alias_10!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_10=get_aliasduration(alias_10, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 10-->'+alias_duration_10)
														
														var alias_number_10=get_alias(alias_10, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 10 -->'+alias_number_10)
							
														amount_10=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_10,alias_number_10)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 10 -->'+amount_10)
																																			
													}//10.............
													
													
													//========================= 11 ====================================
													
													if(alias_11!=null && alias_11!='' && alias_11!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_11=get_aliasduration(alias_11, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 11-->'+alias_duration_11)
														
														var alias_number_11=get_alias(alias_11, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 11 -->'+alias_number_11)
							
														amount_11=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_11,alias_number_11)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 11 -->'+amount_11)
																																			
													}//11.............
													
													
													
													//========================= 13 ====================================
													
													if(alias_13!=null && alias_13!='' && alias_13!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_13=get_aliasduration(alias_13, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 13-->'+alias_duration_13)
														
														var alias_number_13=get_alias(alias_13, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 13 -->'+alias_number_13)
							
														amount_13=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_13,alias_number_13)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 13 -->'+amount_13)
																																			
													}//13.............
													
													
													//========================= 14 ====================================
													
													if(alias_14!=null && alias_14!='' && alias_14!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_14=get_aliasduration(alias_14, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 14-->'+alias_duration_14)
														
														var alias_number_14=get_alias(alias_14, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 14 -->'+alias_number_14)
							
														amount_14=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_14,alias_number_14)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 14 -->'+amount_14)
																																			
													}//14.............
													
													
													
													//========================= 15 ====================================
													
													if(alias_15!=null && alias_15!='' && alias_15!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_15=get_aliasduration(alias_15, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 15-->'+alias_duration_15)
														
														var alias_number_15=get_alias(alias_15, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 15 -->'+alias_number_15)
							
														amount_15=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_15,alias_number_15)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 15 -->'+amount_15)
																																			
													}//15.............
													
													
													//========================= 16 ====================================
													
													if(alias_16!=null && alias_16!='' && alias_16!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_16=get_aliasduration(alias_16, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 16-->'+alias_duration_16)
														
														var alias_number_16=get_alias(alias_16, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 16 -->'+alias_number_16)
							
														amount_16=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_16,alias_number_16)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 16 -->'+amount_16)
																																			
													}//16.............
													
													
													
													//========================= 17 ====================================
													
													if(alias_17!=null && alias_17!='' && alias_17!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_17=get_aliasduration(alias_17, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 17-->'+alias_duration_17)
														
														var alias_number_17=get_alias(alias_17, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 17 -->'+alias_number_17)
							
														amount_17=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_17,alias_number_17)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 17 -->'+amount_17)
																																			
													}//17.............
													
													
													//========================= 18 ====================================
													
													if(alias_18!=null && alias_18!='' && alias_18!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_18=get_aliasduration(alias_18, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 18-->'+alias_duration_18)
														
														var alias_number_18=get_alias(alias_18, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 18 -->'+alias_number_18)
							
														amount_18=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_18,alias_number_18)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 18 -->'+amount_18)
																																			
													}//18.............
													
													
													//========================= 19 ====================================
													
													if(alias_19!=null && alias_19!='' && alias_19!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_19=get_aliasduration(alias_19, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 19-->'+alias_duration_19)
														
														var alias_number_19=get_alias(alias_19, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 19 -->'+alias_number_19)
							
														amount_19=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_19,alias_number_19)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 19 -->'+amount_19)
																																			
													}//19.............
													
													
													
													//========================= 20 ====================================
													
													if(alias_20!=null && alias_20!='' && alias_20!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_20=get_aliasduration(alias_20, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 20-->'+alias_duration_20)
														
														var alias_number_20=get_alias(alias_20, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 20 -->'+alias_number_20)
							
														amount_20=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_20,alias_number_20)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 20 -->'+amount_20)
																																			
													}//20.............
													
													
													//========================= 21 ====================================
													
													if(alias_21!=null && alias_21!='' && alias_21!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_21=get_aliasduration(alias_21, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 21-->'+alias_duration_21)
														
														var alias_number_21=get_alias(alias_21, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 21 -->'+alias_number_21)
							
														amount_21=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_21,alias_number_21)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 21 -->'+amount_21)
																																			
													}//21.............
													
													//========================= 22 ====================================
													
													if(alias_22!=null && alias_22!='' && alias_22!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_22=get_aliasduration(alias_22, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 22-->'+alias_duration_22)
														
														var alias_number_22=get_alias(alias_22, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 22 -->'+alias_number_22)
							
														amount_22=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_22,alias_number_22)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 22 -->'+amount_22)
																																			
													}//22.............
													
													
													
													
													
													//========================= 23 ====================================
													
													if(alias_23!=null && alias_23!='' && alias_23!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_23=get_aliasduration(alias_23, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 23-->'+alias_duration_23)
														
														var alias_number_23=get_alias(alias_23, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 23 -->'+alias_number_23)
							
														amount_23=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_23,alias_number_23)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 23 -->'+amount_23)
																																			
													}//23.............
													
													
													
													//========================= 24 ====================================
													
													if(alias_24!=null && alias_24!='' && alias_24!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_24=get_aliasduration(alias_24, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 24-->'+alias_duration_24)
														
														var alias_number_24=get_alias(alias_24, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 24 -->'+alias_number_24)
							
														amount_24=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_24,alias_number_24)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 24 -->'+amount_24)
																																			
													}//24.............
													
													
													//========================= 25 ====================================
													
													if(alias_25!=null && alias_25!='' && alias_25!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_25=get_aliasduration(alias_25, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 25-->'+alias_duration_25)
														
														var alias_number_25=get_alias(alias_25, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 25 -->'+alias_number_25)
							
														amount_25=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_25,alias_number_25)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 25 -->'+amount_25)
																																			
													}//25.............
													
													
													
													//========================= 26 ====================================
													
													if(alias_26!=null && alias_26!='' && alias_26!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_26=get_aliasduration(alias_26, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 26-->'+alias_duration_26)
														
														var alias_number_26=get_alias(alias_26, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 26 -->'+alias_number_26)
							
														amount_26=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_26,alias_number_26)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 26 -->'+amount_26)
																																			
													}//26.............
													
													
													//========================= 27 ====================================
													
													if(alias_27!=null && alias_27!='' && alias_27!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_27=get_aliasduration(alias_27, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 27-->'+alias_duration_27)
														
														var alias_number_27=get_alias(alias_27, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 27 -->'+alias_number_27)
							
														amount_27=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_27,alias_number_27)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 27 -->'+amount_27)
																																			
													}//27.............
													
													
													
														
													//========================= 28 ====================================
													
													if(alias_28!=null && alias_28!='' && alias_28!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_28=get_aliasduration(alias_28, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 28-->'+alias_duration_28)
														
														var alias_number_28=get_alias(alias_28, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 28 -->'+alias_number_28)
							
														amount_28=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_28,alias_number_28)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 28 -->'+amount_28)
																																			
													}//28.............
													
													
													
													//========================= 29 ====================================
													
													if(alias_29!=null && alias_29!='' && alias_29!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_29=get_aliasduration(alias_29, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 29-->'+alias_duration_29)
														
														var alias_number_29=get_alias(alias_29, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 29 -->'+alias_number_29)
							
														amount_29=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_29,alias_number_29)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 29 -->'+amount_29)
																																			
													}//29.............
													
													
													
													//========================= 30 ====================================
													
													if(alias_30!=null && alias_30!='' && alias_30!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_30=get_aliasduration(alias_30, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 30-->'+alias_duration_30)
														
														var alias_number_30=get_alias(alias_30, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 30 -->'+alias_number_30)
							
														amount_30=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_30,alias_number_30)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 30 -->'+amount_30)
																																			
													}//30.............
													
													
													//========================= 31 ====================================
													
													if(alias_31!=null && alias_31!='' && alias_31!=undefined )
													{
														//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
						
														var alias_duration_31=get_aliasduration(alias_31, media_type_tv, media_plan_id, alias);
														//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration 31-->'+alias_duration_31)
														
														var alias_number_31=get_alias(alias_31, media_type_tv, media_plan_id, alias);
						                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number 31 -->'+alias_number_31)
							
														amount_31=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_31,alias_number_31)
														//nlapiLogExecution('DEBUG','Media Schedule',' Amount 31 -->'+amount_31)
																																			
													}//31.............
													
													
													
													     amount_1=validateValue_zero(amount_1);
														  amount_2=validateValue_zero(amount_2);
														  amount_3=validateValue_zero(amount_3);
														  amount_4=validateValue_zero(amount_4);
														  amount_5=validateValue_zero(amount_5);
														  amount_6=validateValue_zero(amount_6);
														  amount_7=validateValue_zero(amount_7);
														  amount_8=validateValue_zero(amount_8);
														  amount_9=validateValue_zero(amount_9);
														  amount_10=validateValue_zero(amount_10);
													
														  amount_11=validateValue_zero(amount_11);
														  amount_12=validateValue_zero(amount_12);
														  amount_13=validateValue_zero(amount_13);
														  amount_14=validateValue_zero(amount_14);
														  amount_15=validateValue_zero(amount_15);
														  amount_16=validateValue_zero(amount_16);
														  amount_17=validateValue_zero(amount_17);
														  amount_18=validateValue_zero(amount_18);
														  amount_19=validateValue_zero(amount_19);
														  amount_20=validateValue_zero(amount_20);
														
														  amount_21=validateValue_zero(amount_21);
														  amount_22=validateValue_zero(amount_22);
														  amount_23=validateValue_zero(amount_23);
														  amount_24=validateValue_zero(amount_24);
														  amount_25=validateValue_zero(amount_25);
														  amount_26=validateValue_zero(amount_26);
														  amount_27=validateValue_zero(amount_27);
														  amount_28=validateValue_zero(amount_28);
														  amount_29=validateValue_zero(amount_29);
														  amount_30=validateValue_zero(amount_30);
														  amount_31=validateValue_zero(amount_31);
															
														nlapiLogExecution('DEBUG', '605', '*********  amount_1 ****************'+amount_1);	
														nlapiLogExecution('DEBUG', '605', '*********  amount_2 ****************'+amount_2);	
														nlapiLogExecution('DEBUG', '605', '*********  amount_3 ****************'+amount_3);	
														nlapiLogExecution('DEBUG', '605', '*********  amount_4 ****************'+amount_4);	
														nlapiLogExecution('DEBUG', '605', '*********  amount_5 ****************'+amount_5);	
														nlapiLogExecution('DEBUG', '605', '*********  amount_6 ****************'+amount_6);	
														nlapiLogExecution('DEBUG', '605', '*********  total_alias_amount ****************'+total_alias_amount);	
															
															
																										
													total_alias_amount=parseFloat(total_alias_amount)+parseFloat(amount_1) +  parseFloat(amount_2) + parseFloat(amount_3) + parseFloat(amount_4) + parseFloat(amount_5) +  parseFloat(amount_6 ) + parseFloat(amount_7) +  parseFloat(amount_8 ) + parseFloat(amount_9 ) + parseFloat(amount_10 ) + parseFloat(amount_11 ) +	   parseFloat(amount_12 ) +parseFloat(amount_13 ) +	  parseFloat(amount_14 ) + parseFloat(amount_15 ) + parseFloat(amount_16) +  parseFloat(amount_17 ) + parseFloat(amount_18) + parseFloat(amount_19) +	   parseFloat(amount_20) +  parseFloat(amount_21) + parseFloat(amount_22) + parseFloat(amount_23) + parseFloat(amount_24) + parseFloat(amount_25) + parseFloat(amount_26) + parseFloat(amount_27) +  parseFloat(amount_28) +  parseFloat(amount_29) +  parseFloat(amount_30 ) +  parseFloat(amount_31 ) ;
		                                            //nlapiLogExecution('DEBUG', '605', '*********  total_alias ****************'+total_alias);	
													
													
													//total_alias_amount=parseFloat(total_alias_amount)+parseFloat(total_alias);
													nlapiLogExecution('DEBUG', '605', '*********  total_alias_amount ****************'+total_alias_amount);	
													
												if (ms_month == 1)
												{
																
													jan_total=parseFloat(jan_total)+parseFloat(total_alias_amount);
													nlapiLogExecution('DEBUG', '605', '*********  January ****************');	
													
												}
												else if(ms_month == 2) 
												{
													
												
													feb_total=parseFloat(feb_total)+parseFloat(total_alias_amount);
												    nlapiLogExecution('DEBUG', '605', '*********  February ****************');	
													
												}
												else if(ms_month ==3)
												{
												
													mar_total=parseFloat(mar_total)+parseFloat(total_alias_amount);
												    nlapiLogExecution('DEBUG', '605', '*********  March ****************');	
													
													
												}
												else if( ms_month == 4)
												{
													
													apr_total=parseFloat(apr_total)+parseFloat(total_alias_amount);
												    nlapiLogExecution('DEBUG', '605', '*********  April ****************');	
													
													
												}
												else if(ms_month == 5)
												{
												
													may_total=parseFloat(may_total)+parseFloat(total_alias_amount);
												     nlapiLogExecution('DEBUG', '605', '*********  May ****************');	
													
													
												}
												else if( ms_month == 6)
												{
													
													jun_total=parseFloat(jun_total)+parseFloat(total_alias_amount);
												    nlapiLogExecution('DEBUG', '605', '*********  June ****************');	
													
													
												}
												else if( ms_month == 7)
												{
													
													july_total=parseFloat(july_total)+parseFloat(total_alias_amount);
												    nlapiLogExecution('DEBUG', '605', '*********  July ****************');	
													
													
												}
												else if( ms_month == 8)
												{
																						
													aug_total=parseFloat(aug_total)+parseFloat(total_alias_amount);
											        nlapiLogExecution('DEBUG', '605', '*********  August ****************');	
													
												}
												else if(ms_month == 9)
												{
													
												sep_total=parseFloat(sep_total)+parseFloat(total_alias_amount);
												nlapiLogExecution('DEBUG', '605', '*********  September ****************');	
													
													
												}
												else if( ms_month == 10)
												{
												oct_total=parseFloat(oct_total)+parseFloat(total_alias_amount);
												nlapiLogExecution('DEBUG', '605', '*********  October ****************');	
																	
												}
												else if(ms_month == 11)
												{
												nov_total=parseFloat(nov_total)+parseFloat(total_alias_amount);
												nlapiLogExecution('DEBUG', '605', '*********  November ****************');	
													
													
												}
												else if(ms_month == 12) 
												{
													
												dec_total=parseFloat(dec_total)+parseFloat(total_alias_amount);
												nlapiLogExecution('DEBUG', '605', '*********  December ****************');	
																								
												}
																					
																				
																					
											nlapiLogExecution('DEBUG', '605', '*********  January Total :  ******* ' + jan_total);		
											nlapiLogExecution('DEBUG', '605', '*********  February Total :  ******* ' + feb_total);	
											nlapiLogExecution('DEBUG', '605', '*********  March Total :  ******* ' + mar_total);	
											nlapiLogExecution('DEBUG', '605', '*********  April Total :  ******* ' + apr_total);	
											nlapiLogExecution('DEBUG', '605', '*********  May Total :  ******* ' + may_total);	
											nlapiLogExecution('DEBUG', '605', '*********  June Total :  ******* ' + jun_total);	
											nlapiLogExecution('DEBUG', '605', '*********  July Total :  ******* ' + july_total);	
											nlapiLogExecution('DEBUG', '605', '*********  August Total :  ******* ' + aug_total);	
											nlapiLogExecution('DEBUG', '605', '*********  September Total :  ******* ' + sep_total);	
											nlapiLogExecution('DEBUG', '605', '*********  October Total :  ******* ' + oct_total);	
											nlapiLogExecution('DEBUG', '605', '*********  November Total :  ******* ' + nov_total);		
											nlapiLogExecution('DEBUG', '605', '*********  December Total :  ******* ' + dec_total);										
																					
																					
											
									        
							 		
													
													
											
													
													
												 }//Sales Amount											
												
											 }//Sales Quantity
																												
											
											//opputunity_amount
																		
									
									
	 				 	       
											
											
											
											
											
											
											
										}//Loop - Media Schedule Results
																														
									}//Search - Media Schedule
									
								
								 }//Loop - Media Plan 
								
								
							 }//Search Media Plan Results
						 
							// ======================== Oppurtunity Total ======================
											
											//opputunity_amount		
													
											//opp_date		
													
											  var date_arr = new Array();
											  if (opp_date != null && opp_date != '' && opp_date != undefined)
											   {
											  
											  
											  
											  
											  	date_arr = opp_date.split('/');
											  	var opp_day = date_arr[0];
											  	var opp_month = date_arr[1];
											  	var opp_year = date_arr[2];
											  	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day  : ' + opp_day + ' Month : ' + opp_month + ' Year :' + opp_year);
											  	
											  	
											  	if (opp_month == 1) {
											  	
											  		jan_opp_total = parseFloat(jan_opp_total) + parseFloat(opputunity_amount);
											  		nlapiLogExecution('DEBUG', '605', '*********  January ****************');
											  		
											  	}
											  	else 
											  		if (opp_month == 2) {
											  		
											  		
											  			feb_opp_total = parseFloat(feb_opp_total) + parseFloat(opputunity_amount);
											  			nlapiLogExecution('DEBUG', '605', '*********  February ****************');
											  			
											  		}
											  		else 
											  			if (opp_month == 3) {
											  			
											  				mar_opp_total = parseFloat(mar_opp_total) + parseFloat(opputunity_amount);
											  				nlapiLogExecution('DEBUG', '605', '*********  March ****************');
											  				
											  				
											  			}
											  			else 
											  				if (opp_month == 4) {
											  				
											  					apr_opp_total = parseFloat(apr_opp_total) + parseFloat(opputunity_amount);
											  					nlapiLogExecution('DEBUG', '605', '*********  April ****************');
											  					
											  					
											  				}
											  				else 
											  					if (opp_month == 5) {
											  					
											  						may_opp_total = parseFloat(may_opp_total) + parseFloat(opputunity_amount);
											  						nlapiLogExecution('DEBUG', '605', '*********  May ****************');
											  						
											  						
											  					}
											  					else 
											  						if (opp_month == 6) {
											  						
											  							jun_opp_total = parseFloat(jun_opp_total) + parseFloat(opputunity_amount);
											  							nlapiLogExecution('DEBUG', '605', '*********  June ****************');
											  							
											  							
											  						}
											  						else 
											  							if (opp_month == 7) {
											  							
											  								july_opp_total = parseFloat(july_opp_total) + parseFloat(opputunity_amount);
											  								nlapiLogExecution('DEBUG', '605', '*********  July ****************');
											  								
											  								
											  							}
											  							else 
											  								if (opp_month == 8) {
											  								
											  									aug_opp_total = parseFloat(aug_opp_total) + parseFloat(opputunity_amount);
											  									nlapiLogExecution('DEBUG', '605', '*********  August ****************');
											  									
											  								}
											  								else 
											  									if (opp_month == 9) {
											  									
											  										sep_opp_total = parseFloat(sep_opp_total) + parseFloat(opputunity_amount);
											  										nlapiLogExecution('DEBUG', '605', '*********  September ****************');
											  										
											  										
											  									}
											  									else 
											  										if (opp_month == 10) {
											  											oct_opp_total = parseFloat(oct_opp_total) + parseFloat(opputunity_amount);
											  											nlapiLogExecution('DEBUG', '605', '*********  October ****************');
											  											
											  										}
											  										else 
											  											if (opp_month == 11) {
											  												nov_opp_total = parseFloat(nov_opp_total) + parseFloat(opputunity_amount);
											  												nlapiLogExecution('DEBUG', '605', '*********  November ****************');
											  												
											  												
											  											}
											  											else 
											  												if (opp_month == 12) {
											  												
											  													dec_opp_total = parseFloat(dec_opp_total) + parseFloat(opputunity_amount);
											  													nlapiLogExecution('DEBUG', '605', '*********  December ****************');
											  													
											  												}
											  	
											  	
											  }	
												
											nlapiLogExecution('DEBUG', '605', '********* Opp January Total :  ******* ' + jan_opp_total);		
											nlapiLogExecution('DEBUG', '605', '*********  Opp February Total :  ******* ' + feb_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp  March Total :  ******* ' + mar_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp April Total :  ******* ' + apr_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp May Total :  ******* ' + may_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp June Total :  ******* ' + jun_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp July Total :  ******* ' + july_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp August Total :  ******* ' + aug_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp September Total :  ******* ' + sep_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp October Total :  ******* ' + oct_opp_total);	
											nlapiLogExecution('DEBUG', '605', '********* Opp November Total :  ******* ' + nov_opp_total);		
											nlapiLogExecution('DEBUG', '605', '********* Opp December Total :  ******* ' + dec_opp_total);										
												
												
							//  print_arr[pnt++]=sales_rep_text+'##'+jan_total+'##'+feb_total+'##'+mar_total+'##'+apr_total+'##'+may_total+'##'+jun_total+'##'+july_total+'##'+aug_total+'##'+sep_total+'##'+oct_total+'##'+nov_total+'##'+dec_total+'##'+jan_opp_total+'##'+feb_opp_total+'##'+mar_opp_total+'##'+apr_opp_total+'##'+may_opp_total+'##'+jun_opp_total+'##'+july_opp_total+'##'+aug_opp_total+'##'+sep_opp_total+'##'+oct_opp_total+'##'+nov_opp_total+'##'+dec_opp_total;						 
					
							
						}// Loop - Grouped Opportunity
						print_arr[pnt++]=sales_rep_text+'##'+jan_total+'##'+feb_total+'##'+mar_total+'##'+apr_total+'##'+may_total+'##'+jun_total+'##'+july_total+'##'+aug_total+'##'+sep_total+'##'+oct_total+'##'+nov_total+'##'+dec_total+'##'+jan_opp_total+'##'+feb_opp_total+'##'+mar_opp_total+'##'+apr_opp_total+'##'+may_opp_total+'##'+jun_opp_total+'##'+july_opp_total+'##'+aug_opp_total+'##'+sep_opp_total+'##'+oct_opp_total+'##'+nov_opp_total+'##'+dec_opp_total;						 
					
						
					}//Search Opportunity						
					
					
				}//Sales Rep ID Check					
					
						
				}//Loop - Opputunity 625 - Ungrouped 					
					
					
				}//End Date Check 
								
				
			}//Start Date Check 
			
			
		}//Year Check
	 		
	 }//Department Check
	
	nlapiLogExecution('DEBUG', '605', '********* Print Array   ******* ' + print_arr);//pnt										
	nlapiLogExecution('DEBUG', '605', '********* Print Array Length   ******* ' + print_arr.length);//pnt	
	nlapiLogExecution('DEBUG', '605', '********* pnt    ******* ' + pnt);//pnt	 

	appendix_K_print_layout(year,print_arr);
		
		
	}//Try
	catch(er)
	{
		nlapiLogExecution('DEBUG', ' ERROR ', ' Exception Caught  --> ' + er)
		
	}
	

}//Scheduler Function

// END SCHEDULED FUNCTION ===============================================




function appendix_K_print_layout(year,print_arr)
{
	
	
	
	var year_txt=year_text(year);
	
	var department_text='TV BEC-TERO';
	
	
	
	
	
	
	
	
	
var strVar="";
strVar += "<table border=\"1\" width=\"100%\">";
strVar += "	<tr>";
strVar += "		<td font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b> Revenue vs Target<\/b>  <\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td>&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-family=\"Helvetica\" font-size=\"7\"><b>Date : "+nlapiEscapeXML(year_txt)+"<\/b> <\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-family=\"Helvetica\" font-size=\"7\"><b>Department :"+nlapiEscapeXML(department_text)+" <\/b><\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td>";


strVar += "		<table border=\"0\" width=\"100%\">";
strVar += "			<tr>";
strVar += "				<td border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"1\">&nbsp;<\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Jan<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Feb<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Mar<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Apr<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>May<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Jun<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Jul<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Aug<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Sep<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Oct<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Nov<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Dec<\/b> <\/td>";
strVar += "				<td border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"6\" colspan=\"3\"><b>Total<\/b> <\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Sales&nbsp; Rep<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Revenue<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>Target<\/b><\/td>";
strVar += "				<td  border-top=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><b>%<\/b><\/td>";
strVar += "			<\/tr>";








if(print_arr.length!=0)
{
	

	
	var k_array=new Array();
	var k_sales_rep;
	
	var k_mp_jan;
	var k_mp_feb;
	var k_mp_mar;
	var k_mp_apr;
	var k_mp_may;
	var k_mp_jun;
	var k_mp_july;
	var k_mp_aug;
	var k_mp_sep;
	var k_mp_oct;
	var k_mp_nov;
	var k_mp_dec;
	
	
	
	var k_opp_jan;
	var k_opp_feb;
	var k_opp_mar;
	var k_opp_apr;
	var k_opp_may;
	var k_opp_jun;
	var k_opp_july;
	var k_opp_aug;
	var k_opp_sep;
	var k_opp_oct;
	var k_opp_nov;
	var k_opp_dec;
	
	
	
	
	var total_revenue;
	var total_target;
	

for (var w = 0; w < print_arr.length; w++) 
{
	
	if(print_arr!=null && print_arr!='' && print_arr!=undefined)
	{
		var k_percent_jan=0;
	var k_percent_feb=0;
	var k_percent_mar=0;
	var k_percent_apr=0;
	var k_percent_may=0;
	var k_percent_jun=0;
	var k_percent_july=0;
	var k_percent_aug=0;
	var k_percent_sep=0;
	var k_percent_oct=0;
	var k_percent_nov=0;
	var k_percent_dec=0;
	var total_percent=0;
	
	
	
	k_array = print_arr[w].split('##');
	k_sales_rep = k_array[0];
	k_mp_jan = k_array[1];
	k_mp_feb = k_array[2];
	k_mp_mar = k_array[3];
	k_mp_apr = k_array[4];
	k_mp_may = k_array[5];
	k_mp_jun = k_array[6];
	k_mp_july = k_array[7];
	k_mp_aug = k_array[8];
	k_mp_sep = k_array[9];
	k_mp_oct = k_array[10];
	k_mp_nov = k_array[11];
	k_mp_dec = k_array[12];
	k_opp_jan = k_array[13];
	k_opp_feb = k_array[14];
	k_opp_mar = k_array[15];
	k_opp_apr = k_array[16];
	k_opp_may = k_array[17];
	k_opp_jun = k_array[18];
	k_opp_july = k_array[19];
	k_opp_aug = k_array[20];
	k_opp_sep = k_array[21];
	k_opp_oct = k_array[22];
	k_opp_nov = k_array[23];
	k_opp_dec = k_array[24];
	
	
	
	k_sales_rep=nlapiEscapeXML(k_sales_rep);
	
	k_mp_jan=nlapiEscapeXML(k_mp_jan);
	k_mp_feb=nlapiEscapeXML(k_mp_feb);
	k_mp_mar=nlapiEscapeXML(k_mp_mar);
	k_mp_apr=nlapiEscapeXML(k_mp_apr);
	k_mp_may=nlapiEscapeXML(k_mp_may);
	k_mp_jun=nlapiEscapeXML(k_mp_jun);
	k_mp_july=nlapiEscapeXML(k_mp_july);
	k_mp_aug=nlapiEscapeXML(k_mp_aug);
	k_mp_sep=nlapiEscapeXML(k_mp_sep);
	k_mp_oct=nlapiEscapeXML(k_mp_oct);
	k_mp_nov=nlapiEscapeXML(k_mp_nov);
	k_mp_dec=nlapiEscapeXML(k_mp_dec);
		
	
	k_opp_jan=nlapiEscapeXML(k_opp_jan);
	k_opp_feb=nlapiEscapeXML(k_opp_feb);
	k_opp_mar=nlapiEscapeXML(k_opp_mar);
	k_opp_apr=nlapiEscapeXML(k_opp_apr);
	k_opp_may=nlapiEscapeXML(k_opp_may);
	k_opp_jun=nlapiEscapeXML(k_opp_jun);
	k_opp_july=nlapiEscapeXML(k_opp_july);
	k_opp_aug=nlapiEscapeXML(k_opp_aug);
	k_opp_sep=nlapiEscapeXML(k_opp_sep);
	k_opp_oct=nlapiEscapeXML(k_opp_oct);
	k_opp_nov=nlapiEscapeXML(k_opp_nov);
	k_opp_dec=nlapiEscapeXML(k_opp_dec);
		
	
	k_mp_jan=validateValue_zero(k_mp_jan);
	k_mp_feb=validateValue_zero(k_mp_feb);
	k_mp_mar=validateValue_zero(k_mp_mar);
	k_mp_apr=validateValue_zero(k_mp_apr);
	k_mp_may=validateValue_zero(k_mp_may);
	k_mp_jun=validateValue_zero(k_mp_jun);
	k_mp_july=validateValue_zero(k_mp_july);
	k_mp_aug=validateValue_zero(k_mp_aug);
	k_mp_sep=validateValue_zero(k_mp_sep);
	k_mp_oct=validateValue_zero(k_mp_oct);
	k_mp_nov=validateValue_zero(k_mp_nov);
	k_mp_dec=validateValue_zero(k_mp_dec);
	
	
	k_opp_jan=validateValue_zero(k_opp_jan);
	k_opp_feb=validateValue_zero(k_opp_feb);
	k_opp_mar=validateValue_zero(k_opp_mar);
	k_opp_apr=validateValue_zero(k_opp_apr);
	k_opp_may=validateValue_zero(k_opp_may);
	k_opp_jun=validateValue_zero(k_opp_jun);
	k_opp_july=validateValue_zero(k_opp_july);
	k_opp_aug=validateValue_zero(k_opp_aug);
	k_opp_sep=validateValue_zero(k_opp_sep);
	k_opp_oct=validateValue_zero(k_opp_oct);
	k_opp_nov=validateValue_zero(k_opp_nov);
	k_opp_dec=validateValue_zero(k_opp_dec);
	
	
    if(k_mp_jan!=0)
	{
		 k_mp_jan= parseFloat(k_mp_jan).toFixed(2);
		
	}
	
	if(k_mp_feb!=0)
	{
		k_mp_feb= parseFloat(k_mp_feb).toFixed(2);
		
	}
	if(k_mp_mar!=0)
	{
		 k_mp_mar=  parseFloat(k_mp_mar).toFixed(2);
		
	}
	if(k_mp_apr!=0)
	{
		k_mp_apr= parseFloat(k_mp_apr).toFixed(2);
		
	}
	if(k_mp_may!=0)
	{
		 k_mp_may=  parseFloat(k_mp_may).toFixed(2);
		
	}
	if(k_mp_jun!=0)
	{
		 k_mp_jun=  parseFloat(k_mp_jun).toFixed(2);
		
	}
	if(k_mp_july!=0)
	{
		 k_mp_july= parseFloat(k_mp_july).toFixed(2);
		
	}
	if(k_mp_aug!=0)
	{
		k_mp_aug=  parseFloat(k_mp_aug).toFixed(2);
		
	}
	if(k_mp_sep!=0)
	{
		 k_mp_sep=  parseFloat(k_mp_sep).toFixed(2);
		
	}
	if(k_mp_oct!=0)
	{
		 k_mp_oct=  parseFloat(k_mp_oct).toFixed(2);
		
	}
	if(k_mp_nov!=0)
	{
		 k_mp_nov=  parseFloat(k_mp_nov).toFixed(2);
		
	}
	if(k_mp_dec!=0)
	{
		 k_mp_dec=  parseFloat(k_mp_dec).toFixed(2);
		
	}
		
if(k_opp_jan!=0)
{
	 k_percent_jan=parseFloat(k_mp_jan)/parseFloat(k_opp_jan);
	 k_percent_jan= parseFloat(k_percent_jan)*100;
	
}		
if(k_opp_feb!=0)
{
	k_percent_feb=parseFloat(k_mp_feb)/parseFloat(k_opp_feb);
	k_percent_feb= parseFloat(k_percent_feb)*100;
}			
if(k_opp_mar!=0)
{
	k_percent_mar=parseFloat(k_mp_mar)/parseFloat(k_opp_mar);
	k_percent_mar= parseFloat(k_percent_mar)*100;
}	
 if(k_opp_apr!=0)
{
	 k_percent_apr=parseFloat(k_mp_apr)/parseFloat(k_opp_apr);
	 k_percent_apr= parseFloat(k_percent_apr)*100;
}	
 if(k_opp_may!=0)
{
	  k_percent_may=parseFloat(k_mp_may)/parseFloat(k_opp_may);
	  k_percent_may= parseFloat(k_percent_may)*100;
}	

 if(k_opp_jun!=0)
{
	   k_percent_jun=parseFloat(k_mp_jun)/parseFloat(k_opp_jun);
	   k_percent_jun= parseFloat(k_percent_jun)*100;
}	

if(k_opp_july!=0)
{
	 k_percent_july=parseFloat(k_mp_july)/parseFloat(k_opp_july);
	 k_percent_july= parseFloat(k_percent_july)*100;
}	

if(k_opp_aug!=0)
{
	k_percent_aug=parseFloat(k_mp_aug)/parseFloat(k_opp_aug);
	k_percent_aug= parseFloat(k_percent_aug)*100;
}
 if(k_opp_sep!=0)
{
	 k_percent_sep=parseFloat(k_mp_sep)/parseFloat(k_opp_sep);
	k_percent_sep= parseFloat(k_percent_sep)*100;
}
 if(k_opp_oct!=0)
{
	 k_percent_oct=parseFloat(k_mp_oct)/parseFloat(k_opp_oct);
	k_percent_oct= parseFloat(k_percent_oct)*100;
}
if(k_opp_nov!=0)
{
	 k_percent_nov=parseFloat(k_mp_nov)/parseFloat(k_opp_nov);
	k_percent_nov= parseFloat(k_percent_nov)*100;
}
if(k_opp_dec!=0)
{
   k_percent_dec=parseFloat(k_mp_dec)/parseFloat(k_opp_dec);
	k_percent_dec= parseFloat(k_percent_dec)*100;	
}

 
 
	
	
    k_percent_jan=validateValue_zero(k_percent_jan);
	k_percent_feb=validateValue_zero(k_percent_feb);
	k_percent_mar=validateValue_zero(k_percent_mar);
	k_percent_apr=validateValue_zero(k_percent_apr);
	k_percent_may=validateValue_zero(k_percent_may);
	k_percent_jun=validateValue_zero(k_percent_jun);
	k_percent_july=validateValue_zero(k_percent_july);
	k_percent_aug=validateValue_zero(k_percent_aug);
	k_percent_sep=validateValue_zero(k_percent_sep);
	k_percent_oct=validateValue_zero(k_percent_oct);
	k_percent_nov=validateValue_zero(k_percent_nov);
	k_percent_dec=validateValue_zero(k_percent_dec);	
	
	
	
	
    if(k_percent_jan!=0)
	{
		 k_percent_jan= parseFloat(k_percent_jan).toFixed(2);
		
	}
	
	if(k_percent_feb!=0)
	{
		k_percent_feb= parseFloat(k_percent_feb).toFixed(2);
		
	}
	if(k_percent_mar!=0)
	{
		 k_percent_mar=  parseFloat(k_percent_mar).toFixed(2);
		
	}
	if(k_percent_apr!=0)
	{
		k_percent_apr= parseFloat(k_percent_apr).toFixed(2);
		
	}
	if(k_percent_may!=0)
	{
		 k_percent_may=  parseFloat(k_percent_may).toFixed(2);
		
	}
	if(k_percent_jun!=0)
	{
		 k_percent_jun=  parseFloat(k_percent_jun).toFixed(2);
		
	}
	if(k_percent_july!=0)
	{
		 k_percent_july= parseFloat(k_percent_july).toFixed(2);
		
	}
	if(k_percent_aug!=0)
	{
		k_percent_aug=  parseFloat(k_percent_aug).toFixed(2);
		
	}
	if(k_percent_sep!=0)
	{
		 k_percent_sep=  parseFloat(k_percent_sep).toFixed(2);
		
	}
	if(k_percent_oct!=0)
	{
		 k_percent_oct=  parseFloat(k_percent_oct).toFixed(2);
		
	}
	if(k_percent_nov!=0)
	{
		 k_percent_nov=  parseFloat(k_percent_nov).toFixed(2);
		
	}
	if(k_percent_dec!=0)
	{
		 k_percent_dec=  parseFloat(k_percent_dec).toFixed(2);
		
	}
    
	
	
	
	
  total_revenue=parseFloat(k_mp_jan)+parseFloat(k_mp_feb)+parseFloat(k_mp_mar)+parseFloat(k_mp_apr)+parseFloat(k_mp_may)+parseFloat(k_mp_jun)+parseFloat(k_mp_july)+parseFloat(k_mp_aug)+parseFloat(k_mp_sep)+parseFloat(k_mp_oct)+parseFloat(k_mp_nov)+ parseFloat(k_mp_dec)
  if(total_revenue!=0)
  {
  	 total_revenue=parseFloat(total_revenue).toFixed(2);
  }
  

  total_target= parseFloat(k_opp_jan)+parseFloat(k_opp_feb)+parseFloat(k_opp_mar)+parseFloat(k_opp_apr)+parseFloat(k_opp_may)+parseFloat(k_opp_jun)+parseFloat(k_opp_july)+parseFloat(k_opp_aug)+parseFloat(k_opp_sep)+parseFloat(k_opp_oct)+parseFloat(k_opp_nov)+parseFloat(k_opp_dec)
   if(total_target!=0)
   {
   	  total_target=parseFloat(total_target).toFixed(2);
   }
  
   if(total_target!=0)
   {
   	 total_percent=parseFloat(total_revenue)/parseFloat(total_target);
	 total_percent=parseFloat(total_percent)*100; 
	 total_percent=parseFloat(total_percent).toFixed(2);	
   }
  
    total_revenue=validateValue_zero(total_revenue);
	total_target=validateValue_zero(total_target);
	total_percent=validateValue_zero(total_percent);
 
  
 
	
	
	strVar += "			<tr>";
	strVar += "				<td border-left=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >"+k_sales_rep+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_jan+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_jan+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_jan+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_feb+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_feb+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_feb+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_mar+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_mar+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_mar+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_apr+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_apr+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_apr+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_may+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_may+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_may+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_jun+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_jun+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_jun+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_july+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_july+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_july+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_aug+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >"+k_opp_aug+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >"+k_percent_aug+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_sep+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >"+k_opp_sep+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_sep+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_oct+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_oct+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >"+k_percent_oct+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_nov+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_nov+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_nov+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_mp_dec+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_opp_dec+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+k_percent_dec+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+total_revenue+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+total_target+"<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">"+total_percent+"<\/td>";
	strVar += "			<\/tr>";
	
		
		
		
	}
	
	
	
	
	
	
	
}



	
	
	strVar += "			<tr>";
	strVar += "				<td border-left=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">Team<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"> &nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" >&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\">&nbsp;<\/td>";
	strVar += "			<\/tr>";
		
	
	
}
else
{
	
	
	strVar += "			<tr>";
	strVar += "				<td border-left=\"0.3\" border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "			<\/tr>";
	
	
	
	
	strVar += "			<tr>";
	strVar += "				<td border-left=\"0.3\" border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "			<\/tr>";
	
	strVar += "			<tr>";
	strVar += "				<td border-left=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\" ><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "				<td border-bottom=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"4\"><\/td>";
	strVar += "			<\/tr>";
	
	
	
}




strVar += "			<tr>";
strVar += "				<td colspan=\"40\">&nbsp;<\/td>";
strVar += "			<\/tr>";

strVar += "		<\/table>";













strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "<\/table>";
strVar += "";

	
	
	
	
    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf>\n<body>";
    xml += strVar;
    xml += "</body>\n</pdf>";
    // run the BFO library to convert the xml document to a PDF
    var file = nlapiXMLToPDF(xml);
    //Create PDF File with TimeStamp in File Name
	
	var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	var fileObj = nlapiCreateFile('Appendix K.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
	
   // var fileObj = nlapiCreateFile('Appendix K.pdf', 'PDF', file.getValue());
    nlapiLogExecution('DEBUG', 'Appendix K PDF', ' PDF fileObj = ' + fileObj);
    fileObj.setFolder(219);// Folder PDF File is to be stored
    var fileId = nlapiSubmitFile(fileObj);
    nlapiLogExecution('DEBUG', 'Appendix J PDF', '*************===PDF fileId *********** ==' + fileId);
		
	
	
		
		
		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		

	
	
	
	
}//appendix_K_print_layout()







function year_text(year)
{
    var recordID;
    var year_ID;
    
    
   
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, null, columns);
    
    if (searchresults != null) 
	{
        //nlapiLogExecution('DEBUG', 'Year ID', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++)
		{
            recordID = searchresults[0].getValue('internalid');
            ////nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + recordID);
        
        }
    }
    
    
    
    var custOBJ = nlapiLoadRecord('customrecord_mediaplan_3', recordID);
    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Media Schedule -->' + custOBJ)
    
    if (custOBJ != null) 
	{
    
        var myFld = custOBJ.getField('custrecord_mp3_year')
        var options = myFld.getSelectOptions();
        //nlapiLogExecution('Debug', 'CheckParameters', 'options=' + options.length);
        
        for (var i = 0; i < options.length; i++)
		{
            var yearId = options[i].getId();
            var yearText = options[i].getText();
          //  //nlapiLogExecution('DEBUG', 'suiteletFunction', 'i -->' + i + 'Year ID -->' + yearId + 'Year Text -->' + yearText);
            
            
            if (yearId == year)
			{
                year_ID = yearText;
                break;
            }
            
            
        }//For
    }//Customer
    return year_ID;
}





function sales_order_amount(sales_order_id,tv_program_item)
{
	var sales_OBJ;
	var net_amount;
	var net_quantity;
	var sales_result;
    if (sales_order_id != null && sales_order_id != '') 
	{
		sales_OBJ = nlapiLoadRecord('salesorder', sales_order_id);
		//nlapiLogExecution('DEBUG', ' Sales Order ', 'Sales Order Object -->' + sales_OBJ)
		     				 
            
	
	 if (sales_OBJ != null && sales_OBJ != '') 
	{
    
        var item_count = sales_OBJ.getLineItemCount('item');
       //nlapiLogExecution('DEBUG',' Sales Order ','Item Count-->'+item_count);
        
        if (item_count != null) 
		{
            for (var p = 1; p <= item_count; p++) 
			{
                var item_id = sales_OBJ.getLineItemValue('item', 'item', p);
                //nlapiLogExecution('DEBUG', 'Sales Order', ' Item -->' + item_id);
                
                item_grossamount = sales_OBJ.getLineItemValue('item', 'amount', p);
				item_grossamount=validateValue_zero(item_grossamount);
                //nlapiLogExecution('DEBUG', 'Sales Order', ' Gross Amount -->' + item_grossamount);
                
                
                item_quantity = sales_OBJ.getLineItemValue('item', 'quantity', p);
                item_quantity=validateValue_zero(item_quantity);
				//nlapiLogExecution('DEBUG', 'Sales Order', ' Quantity -->' + item_quantity);
                
                if (item_id == tv_program_item) 
				{
                    net_amount = item_grossamount;
                    net_quantity = item_quantity;
                    sales_result=net_amount+'%%'+net_quantity;
					//nlapiLogExecution('DEBUG', 'Sales Order', ' Net Amount -->' + net_amount);
                    //nlapiLogExecution('DEBUG', 'Sales Order', ' Net Quantity -->' + net_quantity);
                    break;
                }
             
            }//Sales For
			
			
        }//Line Count Check
		
    }//Sales OBJ
	}//Sales ID Check
	
	return sales_result;
}


function validateValue(value)
{
	var value_res;
	
	if(value==null || value ==undefined || value=='' || value ==NaN || value == 'NaN')
	{
		value_res='';
		return value_res;
	}
	else
	{
		return value;
	}
	
}



function validateValue_zero(value)
{
	var value_res;
	if(value==null || value ==undefined || value=='' || value ==NaN || value.toString() == 'NaN' || value == '.00' || value=='NaN' ||  value=='Infinity' || value.toString()=='Infinity' )
	{
		value_res=0;
		return value_res;
	}
	else
	{
		return value;
	}
	
}



function get_aliasduration(ms_alias, media_type_tv, mediaplanID, MediaPlan_Alias)
{
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    
    
    var aliasQuantity = ms_alias.split(',')
    //  //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
    
    for (var k = 0; k < aliasQuantity.length; k++)
	{
    
        var alias_ms = aliasQuantity[k];
        //	//nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
        
        var letter = "";
        var number = "";
        
        for (var q = 0; q < alias_ms.length; q++)
		 {
            var b = alias_ms.charAt(q);
            if (isNaN(b) == true) 
			{
                letter = letter + b;
            }
            else 
			{
                number = number + b;
            }
            
            
        }//END - For
        //   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
        //    //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
        //   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaPlan_Alias ='+MediaPlan_Alias);
      //  if (MediaPlan_Alias == letter) 
		{
        
            var medialist_result = searchMedialist(letter, media_type_tv, mediaplanID);
            //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
            
            if (medialist_result != null && medialist_result != '' && medialist_result != undefined) 
			{
            
                var mlArr = new Array();
                mlArr = medialist_result.split('##');
                ml_recordID = mlArr[0];
                ml_productname = mlArr[1];
                ml_medianame = mlArr[2];
                ml_newduration = mlArr[3];
                
        	    //nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
        	    //nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
        		//nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
                //nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration); 
            
            
            }//ENMD- Media Result
        }//Media Alias Check
    }//For Loop Alias
    return ml_newduration;
    
}//END Function Alias
//get_alias(media_sch_alias,media_type,media_plan_id,alias);
function searchMedialist(letter, media_type_tv, recordID)
{
	if(letter != '' && letter != null && letter != undefined)
	{
		if(media_type_tv != '' && media_type_tv != null && media_type_tv != undefined)
		{
			if(recordID != '' && recordID != null && recordID != undefined)
			{
				
					
	
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
    
    
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    var result;
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
    filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
    filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
    columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
    columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++) 
		{
            ml_recordID = searchresults[j].getValue('internalid');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
            
            ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
            
            ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
            //		nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
            
            ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' + j + ']==' + ml_newduration);
            
            result = ml_recordID + '##' + ml_productname + '##' + ml_medianame + '##' + ml_newduration;
            
        }
        
    }
    
    return result;
				
				
			}
				
		}
		
	}
	
	

    
}




function get_alias(ms_alias, media_type_tv, mediaplanID, MediaPlan_Alias)
{
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    var aliasnumber;
    
    
    var aliasQuantity = ms_alias.split(',')
    //  //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
    
    for (var k = 0; k < aliasQuantity.length; k++)
	 {
    
        var alias_ms = aliasQuantity[k];
        ////nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
        
        var letter = "";
        var number = "";
        
        for (var q = 0; q < alias_ms.length; q++) 
		{
            var b = alias_ms.charAt(q);
            if (isNaN(b) == true) 
			{
                letter = letter + b;
            }
            else 
			{
                number = number + b;
            }
            
            
        }//END - For
        //   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
        //   //nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
        
        if (MediaPlan_Alias == letter)
		 {
            aliasnumber = number;
            //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', '***********aliasnumber **********=' + aliasnumber)
            
        }//Media Alias Check
    }//For Loop Alias
    return aliasnumber;
    
}//END Function Alias



function calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration,alias_number)
{
	var amount;
	 if (media_type_tv == 1) 
	 {
	 	sales_net_quantity = parseFloat(sales_net_quantity) * 60;
        spot_rate = parseFloat(sales_net_amount) / parseFloat(sales_net_quantity);
		spot_rate = parseFloat(spot_rate) * parseFloat(alias_number) * parseFloat(alias_duration);
        //nlapiLogExecution('DEBUG', 'calculation_amount_spot_other_than_spot', ' ************ Spot Rate *************** -->' + spot_rate);
	 	amount=spot_rate;
		amount=amount.toFixed(2);
		
	 }//1 ................
	 
	  else if (media_type_tv == 3 || media_type_tv==4) 
	  {
	  	other_than_rate = parseFloat(sales_net_amount) / parseFloat(sales_net_quantity);
		
        other_than_rate = parseFloat(other_than_rate) * parseFloat(alias_number);
        
		//nlapiLogExecution('DEBUG', 'calculation_amount_spot_other_than_spot', ' ************* Other Than  Spot Rate **************-->' + other_than_rate);
									  	
		amount=other_than_rate;
		amount=amount.toFixed(2);
	  }//3...4....
	
	return amount;
	
}//calculation_amount_spot_other_than_spot

