// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
//29April20131029
/*
   	Script Name: SCH Media Plan Appendix H Print Report
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.


    SUITELET
		- scheduledFunction(request, response)


    SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================
function schedulerFunction(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    try
	{
	   var context = nlapiGetContext();
	   var remainingUsage=context.getRemainingUsage();

	   var salesRep = context.getSetting('SCRIPT','custscript_hsalesrep');
	   var selectedDate = context.getSetting('SCRIPT','custscript_hdate');

	   var filter = new Array();
	   filter[0] = new nlobjSearchFilter('custrecord_mp1_salesrep', null, 'is', salesRep);
	   filter[1] = new nlobjSearchFilter('custrecord_mp1_oa_startdate', null, 'onorbefore', selectedDate);
	   filter[2] = new nlobjSearchFilter('custrecord_mp1_oa_enddate', null, 'onorafter', selectedDate);

	   var searchresults = nlapiSearchRecord('customrecord_mediaplan_1', 'customsearch618', filter, null);

	   if(searchresults != null)
	   {
	   	  nlapiLogExecution('DEBUG', 'schedulerFunction', 'searchresults length = '+searchresults.length);
	   }

	   var durationNewTotal = '';
	   var durationOldTotal = '';
	   var AmountTotal = '';

	   var strVar = "";
	   strVar += "";
	   strVar += "<table border=\"0\" width=\"100%\">";
	   strVar += "	<tr>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Sales Rep<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Advertiser<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Product<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Media Name<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Media Type<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >SO No.<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Duration (New)<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Duration (Old)<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >B/Q<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Amount<\/td>";
	   strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >Notes<\/td>";
	   strVar += "	<\/tr>";

	   for(var i = 0; searchresults != null && i < searchresults.length; i++)
	   {
	   	  var salesrepNameText = '';
		  var advertiserNameText = '';
		  var productNameText = '';
		  var mediaNameText = '';
		  var mediaTypetext = '';
		  var soNameText = '';
		  var durationNew = '';
		  var durationOld = '';
		  var Amount = '';
		  var Alias = '';
		  var oaStartDate = '';
		  var oaEndDate = '';

	   	  var SearchMPResult = searchresults[i];
          var columns = SearchMPResult.getAllColumns();
          var columnLen = columns.length;

          //Media Plan Internal ID
          var column1 = columns[0];
          var mpInternalID = SearchMPResult.getValue(column1);

          //Sales Rep
          var column2 = columns[1];
          var salesrepNameValue = SearchMPResult.getValue(column2);
          salesrepNameText = SearchMPResult.getText(column2);

          //Advertiser
          var column3 = columns[2];
          var advertiserNameValue = SearchMPResult.getValue(column3);
          advertiserNameText = SearchMPResult.getText(column3);

          //Product
          var column4 = columns[3];
          var productNameValue =  SearchMPResult.getValue(column4);
          productNameText = SearchMPResult.getText(column4);

          //Media Name
          var column5 = columns[4];
          mediaNameText =  SearchMPResult.getValue(column5);

          //Media Type
          var column6 = columns[5];
          var mediaTypeValue =  SearchMPResult.getValue(column6);
          mediaTypetext = SearchMPResult.getText(column6);

          //Sales Order (SO)
          var column7 = columns[6];
          var soNameValue = SearchMPResult.getValue(column7);
          soNameText = SearchMPResult.getText(column7);

          //Duration New
          var column8 = columns[7];
		  durationNew = SearchMPResult.getValue(column8);

		  //Duration Old
          var column9 = columns[8];
		  durationOld = SearchMPResult.getValue(column9);

		  //Alias
		  var column10 = columns[9];
		  Alias = SearchMPResult.getValue(column10);

		  //On Air Start Date
		  var column11 = columns[10];
		  oaStartDate = SearchMPResult.getValue(column11);
		  nlapiLogExecution('DEBUG', 'schedulerFunction', 'oaStartDate = '+oaStartDate);

		  //On Air End Date
		  var column12 = columns[11];
		  oaEndDate = SearchMPResult.getValue(column12);
		  nlapiLogExecution('DEBUG', 'schedulerFunction', 'oaEndDate = '+oaEndDate);

		  var BQ = '';
		  var notes = '';

		  strVar += "	<tr>";
	   	  strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+salesrepNameText+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+advertiserNameText+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+productNameText+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+mediaNameText+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+mediaTypetext+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+soNameText+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+durationNew+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+durationOld+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+BQ+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+Amount+"<\/td>";
	      strVar += "		<td font-size=\"8\"  border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >"+notes+"<\/td>";
	      strVar += "	<\/tr>";
	   }

       strVar += "<\/table>";
       strVar += "";

	   var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
	   xml += "<pdf>\n<body>";
	   xml += strVar;
	   xml += "</body>\n</pdf>";
	   // run the BFO library to convert the xml document to a PDF
	   var file = nlapiXMLToPDF(xml);

	   //Create PDF File with TimeStamp in File Name
	   var fileObj = nlapiCreateFile('Appendix H.pdf', 'PDF', file.getValue());
	   nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj = '+fileObj);
	   fileObj.setFolder(219);// Folder PDF File is to be stored
	   var fileId = nlapiSubmitFile(fileObj);
	   nlapiLogExecution('DEBUG', 'printPDF', '*************===PDF fileId *********** =='+ fileId);
	}
	catch(ex)
	{
		nlapiLogExecution('DEBUG','In Suitelet Post Method','error = '+ex)
	}
}
// END SCHEDULED FUNCTION =============================================