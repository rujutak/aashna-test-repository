/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_MediaPlan_Appendix_H.js
	Date       : 29 April 2013
	Description: 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		if (request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm('Appendix H Print');
	
			var item = form.addField('custpage_item', 'select', 'Item', 'item');
			var date = form.addField('custpage_date', 'date', 'Date', '');
			
			var submit_button = form.addSubmitButton();
			response.writePage(form);
			
			
		}//GET
		else if (request.getMethod() == 'POST')
		{
			
			nlapiLogExecution('DEBUG','In Suitelet Post Method',' ******** POST ************');
			
			var post_item = request.getParameter('custpage_item');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Item --> '+post_item);
			
			var post_date = request.getParameter('custpage_date');
	   		nlapiLogExecution('DEBUG','In Suitelet Post Method',' Date --> '+post_date);
			
			var params=new Array();

	  		params['status']='scheduled';
	  		params['runasadmin']='T';
		  	params['custscript_h_item']=post_item;
		  	params['custscript_h_date']=post_date;
		  	var startDate = new Date();
		  	params['startdate']=startDate.toUTCString();

		  	var status = nlapiScheduleScript('customscript_sch_appendix_h_printlayout','customdeploy1',params);
	  		nlapiLogExecution('DEBUG', 'In Suitelet Post Method', ' Status : '+status);
			
		}//POSt
		
	}
	catch(er)
	{
		nlapiLogExecution('DEBUG', 'suiteletFunction', 'Exception Caught -->' + er);
	}
	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
