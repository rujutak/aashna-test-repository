/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:  SCH_MediaSchedule_AppendixF.js
	Date       :  19 April 2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	var recordID;
	
	var ms_status;
	var ms_day;
	var ms_month;
	var ms_year;
	var ms_tv_program;
	var mediaplanID;
	var ms_one ;
    var ms_two ;
    var ms_three ;
    var ms_four ;
	var ms_five ;
    var ms_six ;
    var ms_seven;
    var ms_eight ;
	var ms_nine;
    var ms_ten ;
    var ms_eleven;
    var ms_twelve;
	var ms_thirteen ;
    var ms_fourteen;
    var ms_fifteen ;
    var ms_sixteen ;
	var ms_seventeen ;
    var ms_eightteen ;
    var ms_nineteen ;
    var ms_twenty ;
	var ms_twentyone ;
    var ms_twentytwo;
    var ms_twentythree ;
    var ms_twentyfour ;
	var ms_twentyfive ;
    var ms_twentysix;
    var ms_twentyseven ;
    var ms_twentyeight;
	var ms_twentynine ;
    var ms_thirty ;
    var ms_thirtyone;
	var media_type_tv;
	var media_duration_onairdate;
	var onair_date_one;
	var onair_date_two;
	var onair_date_three;
	var onair_date_four;
	var onair_date_five;
	var onair_date_six;
	var onair_date_seven;
	var onair_date_eight;
	var onair_date_nine;
	var onair_date_ten;
	var onair_date_eleven;
	var onair_date_twelve;
	var onair_date_thirteen;
	var onair_date_fourteen;
	var onair_date_fifteen;
	var onair_date_sixteen;
	var onair_date_seventeen;
	var onair_date_eighteen;
	var onair_date_nineteen;
	var onair_date_twenty;
	var onair_date_twentyone;
	var onair_date_twentytwo;
	var onair_date_twentythree;
	var onair_date_twentyfour;
	var onair_date_twentyfive;
	var onair_date_twentysix;
	var onair_date_twentyseven;
	var onair_date_twentyeight;
	var onair_date_twentynine;
	var onair_date_thirty;
	var onair_date_thirtyone;
	
	
	
	var mp_salesorder_ID;

	
   var context = nlapiGetContext();
   
   var usage_begin=context.getRemainingUsage();
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Usage Begin --> " + usage_begin);

   var media_RecordID=context.getSetting('SCRIPT','custscript_mediarecordid');
   nlapiLogExecution('DEBUG', 'schedulerFunction', " Media Record ID --> " + media_RecordID);
   
   
   //=================================== Search Media Schedule With Status - Approved ================
   
   var approve_status=2
   
    var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', media_RecordID);
	filter[1] = new nlobjSearchFilter('custrecord_mp3_ms_status', null, 'is', approve_status);
   
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	
	var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, filter, columns);
	
	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  Media Schedule -->  ' + searchresults.length);
		
		for (var i = 0; i < searchresults.length; i++) 
		{
			recordID = searchresults[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + i + ']==' + recordID);
	
	
	
	
	
	
			
		   var recordOBJ=nlapiLoadRecord('customrecord_mediaplan_3',recordID);
	       nlapiLogExecution('DEBUG','afterSubmitRecord',' Record Object --> '+recordOBJ);
		   
		   
		    var record_f_status=appendix_f_record(recordID)
		    nlapiLogExecution('DEBUG','afterSubmitRecord',' record_f_status --> '+record_f_status);
		   
		   
		   
	
	

    if(recordOBJ!=null)
	{
		   ms_status=recordOBJ.getFieldValue('custrecord_mp3_ms_status');
		   nlapiLogExecution('DEBUG','afterSubmitRecord',' Status [ Media Schedule ] --> '+ms_status);
		
		
		    ms_tv_program=recordOBJ.getFieldValue('custrecord_mp3_tvprogram');
			 nlapiLogExecution('DEBUG','afterSubmitRecord',' TV Program [ Media Schedule ] --> '+ms_tv_program);
			 
			 	if(ms_tv_program!=null && ms_tv_program!='')
				{
					var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_tv_program);
					nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Object [ Media Schedule ]--->'+tv_programOBJ);
				    
					if(tv_programOBJ!=null)
					{
						media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
						nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Media Type [ Media Schedule ]-->'+media_type_tv);
						
						media_duration_onairdate=tv_programOBJ.getFieldValue('custitem12');
						nlapiLogExecution('DEBUG','suiteletFunction','  TV Program On Air Date  [ Media Schedule ]--->'+media_duration_onairdate);
					}
				
				}
		
		
		if(media_duration_onairdate==null || media_duration_onairdate=='' || media_duration_onairdate==undefined || media_duration_onairdate==' ')
		{
			
			media_duration_onairdate='';
			
		}
		
		
		
		if(ms_status==2 && media_type_tv==1 && record_f_status==false)
		{
			nlapiLogExecution('DEBUG','afterSubmitRecord',' Approved ......');
			
			//================================== Media Schedule Data ========================================
							 			 
					 
			 ms_month=recordOBJ.getFieldValue('custrecord_mp3_month');
			 nlapiLogExecution('DEBUG','afterSubmitRecord',' Month [ Media Schedule ] --> '+ms_month);
			 
			 ms_year=recordOBJ.getFieldText('custrecord_mp3_year');
			 nlapiLogExecution('DEBUG','afterSubmitRecord',' Year [ Media Schedule ] --> '+ms_year);
					 
			    mediaplanID=recordOBJ.getFieldValue('custrecord_mp3_mp1ref');
			    nlapiLogExecution('DEBUG','afterSubmitRecord',' Media Plan ID [ Media Schedule ] --> '+mediaplanID);
			
			   if(mediaplanID!=null)
			   {
			   	var mediaplan_OBJ=nlapiLoadRecord('customrecord_mediaplan_1',mediaplanID);
				nlapiLogExecution('DEBUG','suiteletFunction','  Media Plan Object--->'+mediaplan_OBJ);
				
				mp_salesorder_ID=mediaplan_OBJ.getFieldValue('custrecord_mp1_soref');
			   	nlapiLogExecution('DEBUG','suiteletFunction','  Media Plan Sales Order ID --->'+mp_salesorder_ID);
			   }
			   
			    
				 var usageEnd = context.getRemainingUsage();
				 nlapiLogExecution('DEBUG', 'searchStockReconciliation','usageEnd -->' + usageEnd+ ' for Media Schedule Record Number =='+i+'***********************************************'); 
				    
								
				if (usageEnd < 100) 
				{
				    nlapiLogExecution('DEBUG', 'schedulerFunction',' ******************Rescheduled ***************** ' +stockRecID);
					Schedulescriptafterusageexceeded(i);
					break;	
				}//End-If Usage End		
				
			   
			    ms_one=recordOBJ.getFieldValue( 'custrecord_mp3_1');
				ms_one=validateValue(ms_one);
				nlapiLogExecution('DEBUG','suiteletFunction','  One [ Media Schedule ]--->'+ms_one);
				  
				onair_date_one=1+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date One [ Media Schedule ]--->'+onair_date_one);
				
				var appendix_f_ID_1=appendix_F_CreateRecord(onair_date_one,ms_one,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 1 ]**************** --->'+appendix_f_ID_1);
			
				ms_two=recordOBJ.getFieldValue( 'custrecord_mp3_2');
				ms_two=validateValue(ms_two);
				nlapiLogExecution('DEBUG','suiteletFunction','  Two [ Media Schedule ]--->'+ms_two)
				
				onair_date_two=2+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date Two [ Media Schedule ]--->'+onair_date_two);
				
				var appendix_f_ID_2=appendix_F_CreateRecord(onair_date_two,ms_two,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 2 ]**************** --->'+appendix_f_ID_2);
							
				ms_three=recordOBJ.getFieldValue( 'custrecord_mp3_3');
				ms_three=validateValue(ms_three);
				nlapiLogExecution('DEBUG','suiteletFunction','  Three [ Media Schedule ]--->'+ms_three);
				
				onair_date_three=3+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date Three [ Media Schedule ]--->'+onair_date_three);
				
				var appendix_f_ID_3=appendix_F_CreateRecord(onair_date_three,ms_three,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 3 ]**************** --->'+appendix_f_ID_3);
						
				
				ms_four=recordOBJ.getFieldValue( 'custrecord_mp3_4');
				ms_four=validateValue(ms_four);
				nlapiLogExecution('DEBUG','suiteletFunction','  Four [ Media Schedule ]--->'+ms_four);
				
				
				onair_date_three=4+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date Four [ Media Schedule ]--->'+onair_date_three);
				
				var appendix_f_ID_4=appendix_F_CreateRecord(onair_date_four,ms_four,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 4 ]**************** --->'+appendix_f_ID_4);
				
				
			    ms_five=recordOBJ.getFieldValue( 'custrecord_mp3_5');
				ms_five=validateValue(ms_five);
				nlapiLogExecution('DEBUG','suiteletFunction','  Five [ Media Schedule ]--->'+ms_five);
				
				onair_date_five=5+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date Five [ Media Schedule ]--->'+onair_date_three);
				
				var appendix_f_ID_5=appendix_F_CreateRecord(onair_date_five,ms_five,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 5 ]**************** --->'+appendix_f_ID_5);
							
				ms_six=recordOBJ.getFieldValue( 'custrecord_mp3_6' );
				ms_six=validateValue(ms_six);
				
				onair_date_six=6+'/'+ms_month+'/'+ms_year;
				nlapiLogExecution('DEBUG','suiteletFunction','  On Air Date Five [ Media Schedule ]--->'+onair_date_three);
				
				var appendix_f_ID_6=appendix_F_CreateRecord(onair_date_six,ms_six,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				nlapiLogExecution('DEBUG','suiteletFunction','  **************** Record Creted ID  [ 5 ]**************** --->'+appendix_f_ID_6);
				
							
				ms_seven=recordOBJ.getFieldValue( 'custrecord_mp3_7' );
				ms_seven=validateValue(ms_seven);
						
						
				onair_date_seven=7+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_7=appendix_F_CreateRecord(onair_date_seven,ms_seven,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				
						
								
				ms_eight=recordOBJ.getFieldValue( 'custrecord_mp3_8' );
				ms_eight=validateValue(ms_eight);
				
				onair_date_eight=8+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_8=appendix_F_CreateRecord(onair_date_eight,ms_eight,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				
				
				ms_nine=recordOBJ.getFieldValue( 'custrecord_mp3_9' );
			 	ms_nine=validateValue(ms_nine);
			    
				onair_date_nine=9+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_9=appendix_F_CreateRecord(onair_date_nine,ms_nine,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
			
						
			
			
			
			
				ms_ten=recordOBJ.getFieldValue( 'custrecord_mp3_10' );
				ms_ten=validateValue(ms_ten);
				
				onair_date_ten=10+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_10=appendix_F_CreateRecord(onair_date_ten,ms_ten,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
								
				ms_eleven=recordOBJ.getFieldValue( 'custrecord_mp3_11' );
				ms_eleven=validateValue(ms_eleven);
				
				onair_date_eleven=11+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_11=appendix_F_CreateRecord(onair_date_eleven,ms_eleven,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);
				
				
				ms_twelve=recordOBJ.getFieldValue( 'custrecord_mp3_12' );
				ms_twelve=validateValue(ms_twelve);
					
				onair_date_twelve=12+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_12=appendix_F_CreateRecord(onair_date_twelve,ms_twelve,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);	
					
							
				ms_thirteen=recordOBJ.getFieldValue( 'custrecord_mp3_13' );
				ms_thirteen=validateValue(ms_thirteen);
					
				onair_date_thirteen=13+'/'+ms_month+'/'+ms_year;
								
				var appendix_f_ID_13=appendix_F_CreateRecord(onair_date_thirteen,ms_thirteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);		
					
								
				ms_fourteen=recordOBJ.getFieldValue( 'custrecord_mp3_14' );
				ms_fourteen=validateValue(ms_fourteen);
				onair_date_fourteen=14+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_14=appendix_F_CreateRecord(onair_date_fourteen,ms_fourteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);		
				
				
				
				ms_fifteen=recordOBJ.getFieldValue( 'custrecord_mp3_15' );
				ms_fifteen=validateValue(ms_fifteen);			
			    onair_date_fifteen=15+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_15=appendix_F_CreateRecord(onair_date_fifteen,ms_fifteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);		
			
			
			
				ms_sixteen=recordOBJ.getFieldValue( 'custrecord_mp3_16' );
				ms_sixteen=validateValue(ms_sixteen);	
					
				onair_date_sixteen=16+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_16=appendix_F_CreateRecord(onair_date_sixteen,ms_sixteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);			
					
								
				ms_seventeen=recordOBJ.getFieldValue( 'custrecord_mp3_17' );
				ms_seventeen=validateValue(ms_seventeen);	
				
				onair_date_seventeen=17+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_17=appendix_F_CreateRecord(onair_date_seventeen,ms_seventeen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);			
					
				
				
				ms_eighteen=recordOBJ.getFieldValue( 'custrecord_mp3_18' );
				ms_eighteen=validateValue(ms_eighteen);
						
				onair_date_eighteen=18+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_18=appendix_F_CreateRecord(onair_date_eighteen,ms_eighteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);					
						
								
				ms_nineteen=recordOBJ.getFieldValue( 'custrecord_mp3_19' );
				ms_nineteen=validateValue(ms_nineteen);
							
							
				onair_date_nineteen=19+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_19=appendix_F_CreateRecord(onair_date_nineteen,ms_nineteen,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);								
								
				ms_twenty=recordOBJ.getFieldValue( 'custrecord_mp3_20' );
				ms_twenty=validateValue(ms_twenty);
				
				onair_date_twenty=20+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_20=appendix_F_CreateRecord(onair_date_twenty,ms_twenty,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);								

				ms_twentyone=recordOBJ.getFieldValue( 'custrecord_mp3_21' );
				ms_twentyone=validateValue(ms_twentyone);
						
				onair_date_twentyone=21+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_21=appendix_F_CreateRecord(onair_date_twentyone,ms_twentyone,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);										
								
				ms_twentytwo=recordOBJ.getFieldValue( 'custrecord_mp3_22' );
				ms_twentytwo=validateValue(ms_twentytwo);
					
				onair_date_twentytwo=22+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_22=appendix_F_CreateRecord(onair_date_twentytwo,ms_twentytwo,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
								
				ms_twentythree=recordOBJ.getFieldValue( 'custrecord_mp3_23' );
				ms_twentythree=validateValue(ms_twentythree);
				
				onair_date_twentythree=23+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_23=appendix_F_CreateRecord(onair_date_twentythree,ms_twentythree,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
				
				
				ms_twentyfour=recordOBJ.getFieldValue( 'custrecord_mp3_24' );
				ms_twentyfour=validateValue(ms_twentyfour);
				
				onair_date_twentyfour=24+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_24=appendix_F_CreateRecord(onair_date_twentyfour,ms_twentyfour,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
				
				
				
				ms_twentyfive=recordOBJ.getFieldValue( 'custrecord_mp3_25' );
				ms_twentyfive=validateValue(ms_twentyfive);
					
				onair_date_twentyfive=25+'/'+ms_month+'/'+ms_year;
				
				var appendix_f_ID_25=appendix_F_CreateRecord(onair_date_twentyfive,ms_twentyfive,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
					
					
								
				ms_twentysix=recordOBJ.getFieldValue( 'custrecord_mp3_26' );
				ms_twentysix=validateValue(ms_twentysix);
				
				onair_date_twentysix=26+'/'+ms_month+'/'+ms_year;
				
				var onair_date_twentysix=appendix_F_CreateRecord(onair_date_twentysix,ms_twentysix,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
				
				
				
				ms_twentyseven=recordOBJ.getFieldValue( 'custrecord_mp3_27' );
				ms_twentyseven=validateValue(ms_twentyseven);	
				
				onair_date_twentyseven=27+'/'+ms_month+'/'+ms_year;
				
				var onair_date_twentysix=appendix_F_CreateRecord(onair_date_twentyseven,ms_twentyseven,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);											
				
				
							
				ms_twentyeight=recordOBJ.getFieldValue( 'custrecord_mp3_28' );
				ms_twentyeight=validateValue(ms_twentyeight);	
						
			   onair_date_twentyeight=28+'/'+ms_month+'/'+ms_year;
				
				var onair_date_twentyeight=appendix_F_CreateRecord(onair_date_twentyeight,ms_twentyeight,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);														
						
								
				ms_twentynine=recordOBJ.getFieldValue( 'custrecord_mp3_29' );
				ms_twentynine=validateValue(ms_twentynine);	
					
				 onair_date_twentynine=29+'/'+ms_month+'/'+ms_year;
				
				var onair_date_twentyeight=appendix_F_CreateRecord(onair_date_twentynine,ms_twentynine,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);															
					
							
				ms_thirty=recordOBJ.getFieldValue( 'custrecord_mp3_30' );
				ms_thirty=validateValue(ms_thirty);	
					
				onair_date_thirty=30+'/'+ms_month+'/'+ms_year;
				
				var onair_date_twentyeight=appendix_F_CreateRecord(onair_date_thirty,ms_thirty,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);																
					
						
				ms_thirtyone=recordOBJ.getFieldValue( 'custrecord_mp3_31' );
			    ms_thirtyone=validateValue(ms_thirtyone);	
				
				onair_date_thirtyone=31+'/'+ms_month+'/'+ms_year;
								
				var onair_date_twentyeight=appendix_F_CreateRecord(onair_date_thirtyone,ms_thirtyone,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID);																
				
				//================================== Media List Data ========================================
			
		}
		
		
	}//end
  		 
			

		
		}
	}
   //customrecord_mediaplan_3
   
   
    //=================================== Search Media Schedule With Status - Approved ================


}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================

function searchMedialist(letter, media_type_tv,recordID)
{
	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
	
	
	var ml_recordID;
	var ml_productname;
	var ml_medianame;
	var ml_newduration;
	var result;
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
	filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
	filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
	columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
	columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
	
	var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);

	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
		
		for (var j = 0; j < searchresults.length; j++) 
		{
			ml_recordID = searchresults[j].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
			
			ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
		
		    ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
			nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
			
			ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' +j + ']==' + ml_newduration);
		
		    result=ml_recordID+'##'+ml_productname+'##'+ml_medianame+'##'+ml_newduration;
		   
		}
		
	}
	
   	return result;
	
}





function validateValue(value)
{
	if(value==null || value ==undefined || value=='')
	{
		value=''
	}
	return value;
}


function appendix_F_CreateRecord(onair_date_one,ms_one,mp_salesorder_ID,media_duration_onairdate,mediaplanID,media_type_tv,ms_tv_program,recordID)
{
	 var submit_F_ID;						
	var aliasQuantity = ms_one.split(',')
    nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
	
	for(var k=0;k<aliasQuantity.length;k++)
	{
		var alias_ms=aliasQuantity[k];
		nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
						
	     var letter="";
	       var number="";
	       
	       for(var q=0;q<alias_ms.length;q++)
	       {
	       var b= alias_ms.charAt(q);
	       if(isNaN(b) == true)
	       {
	       letter=letter+b;
	       }
	       else
	       {
	       number=number+b;
	       }
	       }
	       nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
	       nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
	      var medialist_result=searchMedialist(letter, media_type_tv,mediaplanID);
	      nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media List Result ='+medialist_result);
	   
	   
	   if(medialist_result!=null && medialist_result!='' && medialist_result!=undefined)
	   {
	   	var mlArr=new Array();
		mlArr = medialist_result.split('##');
		var ml_recordID = mlArr[0];
		var ml_productname = mlArr[1];
		var ml_medianame = mlArr[2];
		var ml_newduration = mlArr[3];
		
	    nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
	    nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
		nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
	    nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration); 
		
		
		//========================== Create Record ==========================
		
		 var record_f=nlapiCreateRecord('customrecord_queueinglist');
		 nlapiLogExecution('DEBUG', 'IF record', 'record_f = ' + record_f);
		 
		
		 
		 record_f.setFieldValue('custrecord_ql_soref', mp_salesorder_ID);
		 record_f.setFieldValue('custrecord_ql_time', media_duration_onairdate);
		 record_f.setFieldValue('custrecord_ql_tvprogram', ms_tv_program);
		 record_f.setFieldValue('custrecord_ql_productname', ml_productname);
		 record_f.setFieldValue('custrecord_ql_spotname', ml_medianame);
		 record_f.setFieldValue('custrecord_ql_length', ml_newduration);
		 record_f.setFieldValue('custrecord_ql_onairdate', onair_date_one);
		 record_f.setFieldValue('custrecord_ql_medialistref', ml_recordID);
		 record_f.setFieldValue('custrecord_ql_mediascheduleref', recordID);
			   
		 
		 submit_F_ID = nlapiSubmitRecord(record_f, true, true);
		 nlapiLogExecution('DEBUG', 'IF record', ' *********************  Submit F ID ************** == ' + submit_F_ID);
		
		
		//========================== Create Record ==========================   	
	   }
	   
	  		
	}


   return  submit_F_ID;
  
}




function appendix_f_record(recordID)
{
	var f_recordID;
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_ql_mediascheduleref', null, 'is', recordID);
		
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	
	
	
	var searchresults = nlapiSearchRecord('customrecord_queueinglist', null, filter, columns);
	
	if (searchresults != null) 
	{
		nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
		
		for (var h = 0; h < searchresults.length; h++) 
		{
			f_recordID = searchresults[h].getValue('internalid');
			nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ F Appendix][' + h + ']==' + f_recordID);
		}
		
		return true;
	}
	else
	{
		return false;
	}
	
	
}






function Schedulescriptafterusageexceeded(i)
{
			//Define all parameters to schedule the script for voucher generation.
			 var params=new Array();
			 params['status']='scheduled';
		 	 params['runasadmin']='T';
			 params['custscript_counter_msch']=i;
			 
			 var startDate = new Date();
		 	 params['startdate']=startDate.toUTCString();
			
			 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
			 nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status-->'+ status);
			 
			 //If Script Is Scheduled Successfuly Then Check for Status = Queued
			 if (status == 'QUEUED') 
		 	 {
				nlapiLogExecution('DEBUG', 'RESCHEDULED', '********************Script Is Rescheduled ************');
		 	 }
}//fun close





// END FUNCTION =====================================================
