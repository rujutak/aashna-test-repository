/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_Appendix_J_Report.js
	Date       : 5 May 2013 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	try
	{
	var sales_rep_ID;
	var sales_rep_text;
	var sales_rep_total;
	var media_plan_id;
	var tv_program_id;
	var customer_id;	
	var sales_order_id;	
	var mp_start_date;	
	var mp_end_date;
	var net_quantity;
	var  net_amount;
	var alias;
	var sales_OBJ;
	var net_quantity;
	var net_amount;
	var media_type_tv;
	var sales_result;	
	var sales_net_quantity;
	var sales_net_amount;	
	var other_than_rate;
	var spot_rate;	
	var amount;
	var target=0;
	var booked=0;
	var potential=0;	
	var print_arr=new Array();
	var pnt=0;
	var quotation_amount;
	var opputunity_id;
	var opputunity_amount;
	var jan_amount;
	var feb_amount;
	var mar_amount;
	var april_amount;
	var may_amount ;
	var june_amount;
	var july_amount;
	var aug_amount ;
	var sep_amount 	
	var oct_amount;
	var nov_amount
	var dec_amount;
	var ms_recordID;
	var amount_one=0;
	var amount_two=0;
	var amount_three=0; 
	var amount_four=0;
	var amount_five=0;
	var amount_six=0; 
	var amount_seven=0; 
	var amount_eight=0; 
	var amount_nine=0; 
	var amount_ten=0; 
	var amount_eleven=0; 
	var amount_twelve=0; 
	var amount_thirteen=0; 
	var amount_fourteen=0;
	var amount_fifteen=0;
	var amount_sixteen=0;
	var amount_seventeen=0; 
	var amount_eighteen=0;
	var amount_ninteen=0;
	var amount_twenty=0;
	var amount_twentyone=0;
	var amount_twentytwo=0;
	var amount_twentythree=0; 
	var amount_twentyfour=0; 
	var amount_twentyfive=0; 
	var amount_twentysix=0;
	var amount_twentyseven=0; 
	var amount_twentyeight=0; 
	var amount_twentynine=0;
	var amount_thirty=0;
	var amount_thirtyone=0;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	var alias_total;
	var month_quotation_amount;		
											
	var ms_tv_program_item;
	
	
	var ms_one ;
    var ms_two ;
    var ms_three ;
    var ms_four ;
	var ms_five ;
    var ms_six ;
    var ms_seven;
    var ms_eight ;
	var ms_nine;
    var ms_ten ;
    var ms_eleven;
    var ms_twelve;
	var ms_thirteen ;
    var ms_fourteen;
    var ms_fifteen ;
    var ms_sixteen ;
	var ms_seventeen ;
    var ms_eighteen ;
    var ms_ninteen ;
    var ms_twenty ;
	var ms_twentyone ;
    var ms_twentytwo;
    var ms_twentythree ;
    var ms_twentyfour ;
	var ms_twentyfive ;
    var ms_twentysix;
    var ms_twentyseven ;
    var ms_twentyeight;
	var ms_twentynine ;
    var ms_thirty ;
    var ms_thirtyone;
  
	
	var total_quotation_amount=0;
	var total_media_plan_amount=0;
	var total_opputunity_amount=0;
	
				
	var context = nlapiGetContext();
    var remainingUsage = context.getRemainingUsage();
    
    var department = context.getSetting('SCRIPT', 'custscript_j_department');
    var month = context.getSetting('SCRIPT', 'custscript_j_month');
	var year = context.getSetting('SCRIPT', 'custscript_j_year');
	var start_date = context.getSetting('SCRIPT', 'custscript_j_start_date');
	var end_date = context.getSetting('SCRIPT', 'custscript_j_end_date');
  
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Department --> ' + department)
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Year --> ' + year)
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' Month --> ' + month)
    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Start Date --> ' + start_date)
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' End Date --> ' + end_date)
	
	
	  if(department!=null && department!='' && department!=undefined)
	  {
	  	 if(start_date!=null && start_date!='' && start_date!=undefined)
		 {
		 	 if(end_date!=null && end_date!='' && end_date!=undefined)
			 {
			 	var  searchresults = nlapiSearchRecord('opportunity','customsearch625', null, null);		
				
				 for (var i = 0; searchresults != null && i < searchresults.length; i++) 	
				 {
				 	
					
								
				 	nlapiLogExecution('DEBUG', 'Opportunity: Results - 625 ', ' Opportunity Search Results --> ' + searchresults.length);
					
					var search_opp_result = searchresults[i];
	                var columns = search_opp_result.getAllColumns();
	                var columnLen = columns.length;
	                //nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', 'i -->' + i);
					
					var column1 = columns[0];
	                sales_rep_ID = search_opp_result.getValue(column1);
	                //nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', 'Sales Rep ID -->' + sales_rep_ID);
	              				
	                sales_rep_text = search_opp_result.getText(column1);
	                nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Grouped - 625', ' Sales Rep Text -->' + sales_rep_text);              
				
					
					if(sales_rep_ID!=null && sales_rep_ID!='')
					{
						var O_filters = new Array();
										
		                    
		                O_filters[0] = new nlobjSearchFilter('salesrep', null, 'is', sales_rep_ID)
						O_filters[1] = new nlobjSearchFilter('trandate',null, 'onorafter', start_date)
						O_filters[2] = new nlobjSearchFilter('trandate',null, 'onorbefore', end_date) 
						
						
						var search_opportunity_results = nlapiSearchRecord('opportunity', 'customsearch626', O_filters, null);
						
						 if (search_opportunity_results != null) 
						 {
						 	nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', ' Opportunity Search Results --> ' + search_opportunity_results.length);
							
							 for (var g = 0; search_opportunity_results != null && g < search_opportunity_results.length; g++) 
							 {
							 									
								
							 	
								var opp_result = search_opportunity_results[g];
					            var columns_opp = opp_result.getAllColumns();
					            var column_opp_len = columns_opp.length;
					            
					            // Opportunity Internal ID
					            var column_opp_1 = columns_opp[0];
					            opputunity_id = opp_result.getValue(column_opp_1);
					            nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', '**************** Oppurtunity Record ID [' + g + ' ] ****************--> ' + opputunity_id);
										
								
								// Amount 
					            var column_opp_2 = columns_opp[2];
					            opputunity_amount = opp_result.getValue(column_opp_2);
					            nlapiLogExecution('DEBUG', 'Opportunity : Sales Rep Ungrouped - 626 ', '**************** Oppurtunity Amount [' + g + ' ] ****************--> ' + opputunity_amount);
								
								
								
								var MPfilters = new Array();
				
								var MPColoumns = new Array();
				               
				                    
				                MPfilters[0] = new nlobjSearchFilter('custrecord_mp1_salesrep', null, 'is', sales_rep_ID)
								MPfilters[1] = new nlobjSearchFilter('custrecord_mp1_oa_startdate',null, 'onorafter', start_date)
								MPfilters[2] = new nlobjSearchFilter('custrecord_mp1_oa_startdate',null, 'onorbefore', end_date) 
								MPfilters[3] = new nlobjSearchFilter('custrecord_mp1_oa_enddate',null, 'onorafter', start_date)
								MPfilters[1] = new nlobjSearchFilter('custrecord_mp1_oa_enddate',null, 'onorbefore', end_date) 
								
																
								 var search_media_plan_results = nlapiSearchRecord('customrecord_mediaplan_1', 'customsearch628', MPfilters, null);
				
								 if (search_media_plan_results != null) 
								 {
								 	nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Media Plan Search Results --> ' + search_media_plan_results.length);
									
									 for (var t = 0; search_media_plan_results != null && t < search_media_plan_results.length; t++) 
									 {
									 	
										var total_opputunity_amount=0;
					                    var total_media_plan_amount=0;
										
									 	var media_plan_result = search_media_plan_results[t];
							            var columns_mp = media_plan_result.getAllColumns();
							            var column_mp_len = columns_mp.length;
							            
							            //Media Plan Internal ID
							            var column_mp_1 = columns_mp[0];
							            media_plan_id = media_plan_result.getValue(column_mp_1);
							            nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', '**************** Media Record ID [' + t + ' ] ****************--> ' + media_plan_id);
									 	
										
									   //TV Program Plan Internal ID
							            var column_mp_2 = columns_mp[2];
							            tv_program_id = media_plan_result.getValue(column_mp_2);
							            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' TV Program ID [' + t + ' ] --> ' + tv_program_id);
										
									    //Customer
							            var column_mp_3 = columns_mp[3];
							            customer_id = media_plan_result.getValue(column_mp_3);
							            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Customer ID [' + t + ' ] --> ' + customer_id);
										
									     //SO Reference
							            var column_mp_4 = columns_mp[5];
							            sales_order_id = media_plan_result.getValue(column_mp_4);
							            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' ************ Sales order ID [' + t + ' ] --> ' + sales_order_id);
										
										//On Air Start Date
							            var column_mp_5 = columns_mp[6];
							            mp_start_date = media_plan_result.getValue(column_mp_5);
							            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Start Date [' + t + ' ] --> ' + mp_start_date);
										
										//On Air End Date
							            var column_mp_6 = columns_mp[7];
							            mp_end_date = media_plan_result.getValue(column_mp_6);
							            //nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' End Date [' + t + ' ] --> ' + mp_end_date);
											
											
										// Alias
							            var column_mp_7 = columns_mp[10];
							            alias = media_plan_result.getValue(column_mp_7);
							            nlapiLogExecution('DEBUG', 'Media Plan: Results - 628 ', ' Alias [' + t + ' ] --> ' + alias);	
											
										
											var Q_filters = new Array();
						
											var Q_coloumns = new Array();
							               		                    
							                Q_filters[0] = new nlobjSearchFilter('salesrep', null, 'is', sales_rep_ID)
											Q_filters[1] = new nlobjSearchFilter('department', null, 'is', department)
											Q_filters[2] = new nlobjSearchFilter('custbody_pipeline_year',null, 'is', year)
											
											
											var search_quotation_results = nlapiSearchRecord('estimate', 'customsearch627', Q_filters, null);
											
											if (search_quotation_results != null) 
											{
												nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' Quotation Search Results --> ' + search_quotation_results.length);
												
													for (var z = 0; search_quotation_results != null && z < search_quotation_results.length; z++)
													{
														var total_quotation_amount=0;
														
														var quotation_result = search_quotation_results[z];
												 		var columns_qt = quotation_result.getAllColumns();
												 		var column_qt_len = columns_qt.length;
														
														
														//=================Quotation ID ===============
														
														var column_q_0 = columns_qt[1];
														var quotation_id = quotation_result.getValue(column_q_0);
														nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', 'Quotation ID [' + z + ' ] -->' + quotation_id);
														
														
														
														//================= January Amount ===============
														
														 var column_q_1 = columns_qt[9];
														 jan_amount = quotation_result.getValue(column_q_1);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' January Amount [' + z + ' ] -->' + jan_amount);
														 
														 //================= February Amount ===============
														
														 var column_q_2 = columns_qt[10];
														 feb_amount = quotation_result.getValue(column_q_2);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' Febuary Amount [' + z + ' ] -->' + feb_amount);
														 
														 //================= March Amount ===============
														
														 var column_q_3 = columns_qt[11];
														 mar_amount = quotation_result.getValue(column_q_3);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' March Amount [' + z + ' ] -->' + mar_amount);
														 
														 //================= April Amount ===============
														
														 var column_q_4 = columns_qt[12];
														 april_amount = quotation_result.getValue(column_q_4);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' April Amount [' + z + ' ] -->' + april_amount);
														 
														 //================= May Amount ===============
														
														 var column_q_5 = columns_qt[13];
														 may_amount = quotation_result.getValue(column_q_5);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' May Amount [' + z + ' ] -->' + may_amount);
														 
														 
														 //================= June Amount ===============
														
														 var column_q_6 = columns_qt[14];
														 june_amount = quotation_result.getValue(column_q_6);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' June Amount [' + z + ' ] -->' + june_amount);
														 
														 
														 //================= July Amount ===============
														
														 var column_q_7 = columns_qt[15];
														 july_amount = quotation_result.getValue(column_q_7);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' July Amount [' + z + ' ] -->' + july_amount);
														 
														 //================= August Amount ===============
														
														 var column_q_8 = columns_qt[16];
														 aug_amount = quotation_result.getValue(column_q_8);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' August Amount [' + z + ' ] -->' + aug_amount);
														 
														 //================= September Amount ===============
														
														 var column_q_9 = columns_qt[17];
														 sep_amount = quotation_result.getValue(column_q_9);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' September Amount [' + z + ' ] -->' + sep_amount);
														 
														 //================= October Amount ===============
														
														 var column_q_10 = columns_qt[18];
														 oct_amount = quotation_result.getValue(column_q_10);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' October Amount [' + z + ' ] -->' + oct_amount);
														
														
														
														//================= November Amount ===============
														
														 var column_q_11 = columns_qt[19];
														 nov_amount = quotation_result.getValue(column_q_11);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' November Amount [' + z + ' ] -->' + nov_amount);
														 
														 //================= December Amount ===============
														
														 var column_q_12 = columns_qt[20];
														 dec_amount = quotation_result.getValue(column_q_12);
														 //nlapiLogExecution('DEBUG', 'Quotation: Results - 627 ', ' December Amount [' + z + ' ] -->' + dec_amount);
														
														
														
														 if(month==1)
														 {
														 	month_quotation_amount=jan_amount;
															
														 }
														 else if(month==2)
														 {
														 	month_quotation_amount=feb_amount;
															
														 }
														 else if(month==3)
														 {
														 	month_quotation_amount=mar_amount;
															
														 }														
														 else if(month==4)
														 {
														 	month_quotation_amount=april_amount;
															
														 }
														 else if(month==5)
														 {
														 	month_quotation_amount=may_amount;
															
														 }		
														  else if(month==6)
														 {
														 	month_quotation_amount=june_amount;
															
														 }
														 else if(month==7)
														 {
														    month_quotation_amount=july_amount;	
															
														 }														
														 else if(month==8)
														 {
														 	month_quotation_amount=aug_amount;
															
														 }
														 else if(month==9)
														 {
														 	month_quotation_amount=sep_amount;
															
														 }
														  else if(month==10)
														 {
														 	month_quotation_amount=oct_amount;
															
														 }														
														 else if(month==11)
														 {
														 	month_quotation_amount=nov_amount;
															
														 }
														 else if(month==12)
														 {
														 	month_quotation_amount=dec_amount;
															
														 }				
													//========================== Media Schedule ==================================	
														
														
														
														var MS_filters = new Array();
				           
										                MS_filters[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', media_plan_id)
														MS_filters[1] = new nlobjSearchFilter('custrecord_mp3_month',null, 'is', month)
														MS_filters[2] = new nlobjSearchFilter('custrecord_mp3_year',null, 'is', year) 
														 
														 
														var MS_coloumns = new Array();
													    MS_coloumns[0] = new nlobjSearchColumn('internalid');
													   												 
															
											           var search_media_schedule_results = nlapiSearchRecord('customrecord_mediaplan_3',null, MS_filters, MS_coloumns);
														 	
														
														
														if (search_media_schedule_results != null) 
											           {
												         //nlapiLogExecution('DEBUG', 'Media Schedule: Results ', ' Media Schedule Search Results --> ' + search_media_schedule_results.length);
												
															for (var n = 0; n < search_media_schedule_results.length; n++)
															{
																
															   ms_recordID = search_media_schedule_results[n].getValue('internalid');
                                                               //nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Record ID  [ Media Schedule ][' + n + ']==' + ms_recordID);	
																
																if(ms_recordID!=null && ms_recordID!='')
																{
																	  var media_sch_OBJ=nlapiLoadRecord('customrecord_mediaplan_3',ms_recordID);
																	  //nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Record Object  [ Media Schedule ][' + n + ']==' + media_sch_OBJ);	
																	
																      if(media_sch_OBJ!=null)
																	  {
																	  
																		ms_tv_program_item=media_sch_OBJ.getFieldValue('custrecord_mp3_tvprogram');
																		
																		ms_one=media_sch_OBJ.getFieldValue('custrecord_mp3_1');
																	    ms_two=media_sch_OBJ.getFieldValue('custrecord_mp3_2');
																	    ms_three=media_sch_OBJ.getFieldValue('custrecord_mp3_3');
																	    ms_four=media_sch_OBJ.getFieldValue('custrecord_mp3_4');
																		ms_five=media_sch_OBJ.getFieldValue('custrecord_mp3_5');
																	    ms_six=media_sch_OBJ.getFieldValue('custrecord_mp3_6');
																	    ms_seven=media_sch_OBJ.getFieldValue('custrecord_mp3_7');
																	    ms_eight=media_sch_OBJ.getFieldValue('custrecord_mp3_8');
																		ms_nine=media_sch_OBJ.getFieldValue('custrecord_mp3_9');
																	    ms_ten=media_sch_OBJ.getFieldValue('custrecord_mp3_10');
																	    ms_eleven=media_sch_OBJ.getFieldValue('custrecord_mp3_11');
																	    ms_twelve=media_sch_OBJ.getFieldValue('custrecord_mp3_12');
																		ms_thirteen=media_sch_OBJ.getFieldValue('custrecord_mp3_13');
																	    ms_fourteen=media_sch_OBJ.getFieldValue('custrecord_mp3_14');
																	    ms_fifteen=media_sch_OBJ.getFieldValue('custrecord_mp3_15');
																	    ms_sixteen =media_sch_OBJ.getFieldValue('custrecord_mp3_16');
																		ms_seventeen =media_sch_OBJ.getFieldValue('custrecord_mp3_17');
																	    ms_eighteen=media_sch_OBJ.getFieldValue('custrecord_mp3_18');
																	    ms_ninteen =media_sch_OBJ.getFieldValue('custrecord_mp3_19');
																	    ms_twenty=media_sch_OBJ.getFieldValue('custrecord_mp3_20');
																		ms_twentyone=media_sch_OBJ.getFieldValue('custrecord_mp3_21');
																	    ms_twentytwo=media_sch_OBJ.getFieldValue('custrecord_mp3_22');
																	    ms_twentythree=media_sch_OBJ.getFieldValue('custrecord_mp3_23');
																	    ms_twentyfour =media_sch_OBJ.getFieldValue('custrecord_mp3_24');
																		ms_twentyfive =media_sch_OBJ.getFieldValue('custrecord_mp3_25');
																	    ms_twentysix=media_sch_OBJ.getFieldValue('custrecord_mp3_26');
																	    ms_twentyseven=media_sch_OBJ.getFieldValue('custrecord_mp3_27');
																	    ms_twentyeight=media_sch_OBJ.getFieldValue('custrecord_mp3_28');
																		ms_twentynine =media_sch_OBJ.getFieldValue('custrecord_mp3_29');
																	    ms_thirty =media_sch_OBJ.getFieldValue('custrecord_mp3_30');
																	    ms_thirtyone=media_sch_OBJ.getFieldValue('custrecord_mp3_31');
  
																		
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias One  [ Media Schedule ][' + n + ']==' + ms_one);	
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias Two  [ Media Schedule ][' + n + ']==' + ms_two);	
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias Three  [ Media Schedule ][' + n + ']==' + ms_three);	
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias Four  [ Media Schedule ][' + n + ']==' + ms_four);	
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias Five  [ Media Schedule ][' + n + ']==' + ms_five);	
																		nlapiLogExecution('DEBUG', 'Media Schedule: Results', '  Alias Six  [ Media Schedule ][' + n + ']==' + ms_six);	
																		
																																				
																		
																		// ==================================  TV Item Type ===================================
																		
																										
																		if(ms_tv_program_item!=null && ms_tv_program_item!=''&& ms_tv_program_item!=undefined)
																		{
																			var tv_programOBJ=nlapiLoadRecord('serviceitem',ms_tv_program_item);
																											    
																			if(tv_programOBJ!=null)
																			{
																				media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
																				//nlapiLogExecution('DEBUG',' TV Type','  TV Program Media Type [ Media Schedule ]-->'+media_type_tv);
																				
																				
																			}//TV Program Object
																		
																		}//TV Item Type
																		
																		
																		//========================      Sales Order Amount ===================
																		
																		sales_result=sales_order_amount(sales_order_id,ms_tv_program_item);
											                            //nlapiLogExecution('DEBUG',' Sales Order ',' Sales Result --> '+sales_result);
																		
																		
																		if(sales_result!=null && sales_result!='' &&sales_result!=undefined)
																		{
																			var sales_Arr = new Array();
														                sales_Arr = sales_result.split('%%');
														                sales_net_amount = sales_Arr[0];
														                sales_net_quantity = sales_Arr[1];
																			
																			
																		}
																		
																		
														               	//nlapiLogExecution('DEBUG','Media Schedule',' Sales Net Amount--> '+sales_net_amount);
																		//nlapiLogExecution('DEBUG','Media Schedule',' Sales Net Quantity  --> '+sales_net_quantity);	
																									
																		
																		
																		
																		 if (sales_net_quantity != undefined || sales_net_quantity != '' || sales_net_quantity != null) 
																		 {
																		 	 if (sales_net_amount != undefined || sales_net_amount != '' || sales_net_amount != null) 
																			 {
																			 	
																				
																				if(ms_one!=null && ms_one!='' && ms_one!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_one=get_aliasduration(ms_one, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration One-->'+alias_duration_one)
																					
																					var alias_number_one=get_alias(ms_one, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number One -->'+alias_number_one)
														
																					amount_one=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_one,alias_number_one)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount One -->'+amount_one)
																																										
																				}//One
																				
																				if(ms_two!=null && ms_two!='' && ms_two!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_two=get_aliasduration(ms_two, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration two-->'+alias_duration_two)
																					
																					var alias_number_two=get_alias(ms_two, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number two -->'+alias_number_two)
														
																					amount_two=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_two,alias_number_two)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount two -->'+amount_two)
																																										
																				}//two
																				if(ms_three!=null && ms_three!='' && ms_three!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_three=get_aliasduration(ms_three, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration three-->'+alias_duration_three)
																					
																					var alias_number_three=get_alias(ms_three, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number three -->'+alias_number_three)
														
																					amount_three=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_three,alias_number_three)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount three -->'+amount_three)
																																										
																				}//three
																				
																				if(ms_four!=null && ms_four!='' && ms_four!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_four=get_aliasduration(ms_four, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration four-->'+alias_duration_four)
																					
																					var alias_number_four=get_alias(ms_four, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number four -->'+alias_number_four)
														
																					amount_four=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_four,alias_number_four)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount four -->'+amount_four)
																																										
																				}//four
																				if(ms_five!=null && ms_five!='' && ms_five!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_five=get_aliasduration(ms_five, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration five-->'+alias_duration_five)
																					
																					var alias_number_five=get_alias(ms_five, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number five -->'+alias_number_five)
														
																					amount_five=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_five,alias_number_five)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount five -->'+amount_five)
																																										
																				}//five
																				
																				if(ms_six!=null && ms_six!='' && ms_six!=undefined )
																				{
																					//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_six=get_aliasduration(ms_six, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration six-->'+alias_duration_six)
																					
																					var alias_number_six=get_alias(ms_six, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number six -->'+alias_number_six)
														
																					amount_six=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_six,alias_number_six)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount six -->'+amount_six)
																																										
																				}//six
																				
																				if(ms_seven!=null && ms_seven!='' && ms_seven!=undefined )
																				{
																					//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_seven=get_aliasduration(ms_seven, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration seven-->'+alias_duration_seven)
																					
																					var alias_number_seven=get_alias(ms_seven, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number seven -->'+alias_number_seven)
														
																					amount_seven=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_seven,alias_number_seven)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount seven -->'+amount_seven)
																																										
																				}//seven
																				if(ms_eight!=null && ms_eight!='' && ms_eight!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_eight=get_aliasduration(ms_eight, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration eight-->'+alias_duration_eight)
																					
																					var alias_number_eight=get_alias(ms_eight, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number eight -->'+alias_number_eight)
														
																					amount_eight=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_eight,alias_number_eight)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount eight -->'+amount_eight)
																																										
																				}//eight
																				if(ms_nine!=null && ms_nine!='' && ms_nine!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_nine=get_aliasduration(ms_nine, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration nine-->'+alias_duration_nine)
																					
																					var alias_number_nine=get_alias(ms_nine, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number nine -->'+alias_number_nine)
														
																					amount_nine=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_nine,alias_number_nine)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount nine -->'+amount_nine)
																																										
																				}//nine
																				if(ms_ten!=null && ms_ten!='' && ms_ten!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_ten=get_aliasduration(ms_ten, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration ten-->'+alias_duration_ten)
																					
																					var alias_number_ten=get_alias(ms_ten, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number ten -->'+alias_number_ten)
														
																					amount_ten=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_ten,alias_number_ten)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount ten -->'+amount_ten)
																																										
																				}//ten
																				if(ms_eleven!=null && ms_eleven!='' && ms_eleven!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_eleven=get_aliasduration(ms_eleven, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration eleven-->'+alias_duration_eleven)
																					
																					var alias_number_eleven=get_alias(ms_eleven, media_type_tv, media_plan_id, alias);
													                               nlapiLogExecution('DEBUG','Media Schedule',' Alias Number eleven -->'+alias_number_eleven)
														
																					amount_eleven=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_eleven,alias_number_eleven)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount eleven -->'+amount_eleven)
																																										
																				}//eleven
																				if(ms_twelve!=null && ms_twelve!='' && ms_twelve!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_twelve=get_aliasduration(ms_twelve, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twelve-->'+alias_duration_twelve)
																					
																					var alias_number_twelve=get_alias(ms_twelve, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twelve -->'+alias_number_twelve)
														
																					amount_twelve=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twelve,alias_number_twelve)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount twelve -->'+amount_twelve)
																																										
																				}//twelve
																				if(ms_thirteen!=null && ms_thirteen!='' && ms_thirteen!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_thirteen=get_aliasduration(ms_thirteen, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration thirteen-->'+alias_duration_thirteen)
																					
																					var alias_number_thirteen=get_alias(ms_thirteen, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number thirteen -->'+alias_number_thirteen)
														
																					var amount_thirteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_thirteen,alias_number_thirteen)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount thirteen -->'+amount_thirteen)
																																										
																				}//thirteen
																				if(ms_fourteen!=null && ms_fourteen!='' && ms_fourteen!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_fourteen=get_aliasduration(ms_fourteen, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration fourteen-->'+alias_duration_fourteen)
																					
																					var alias_number_fourteen=get_alias(ms_fourteen, media_type_tv, media_plan_id, alias);
													                               nlapiLogExecution('DEBUG','Media Schedule',' Alias Number fourteen -->'+alias_number_fourteen)
														
																					amount_fourteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_fourteen,alias_number_fourteen)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount fourteen -->'+amount_fourteen)
																																										
																				}//fourteen
																				if(ms_fifteen!=null && ms_fifteen!='' && ms_fifteen!=undefined )
																				{
																					nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
													
																					var alias_duration_fifteen=get_aliasduration(ms_fifteen, media_type_tv, media_plan_id, alias);
																					nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration fifteen-->'+alias_duration_fifteen)
																					
																					var alias_number_fifteen=get_alias(ms_fifteen, media_type_tv, media_plan_id, alias);
													                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number fifteen -->'+alias_number_fifteen)
														
																					amount_fifteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_fifteen,alias_number_fifteen)
																					nlapiLogExecution('DEBUG','Media Schedule',' Amount fifteen -->'+amount_fifteen)
																																										
																				}//fifteen
																					if(ms_sixteen!=null && ms_sixteen!='' && ms_sixteen!=undefined )
																					{
																						nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_sixteen=get_aliasduration(ms_sixteen, media_type_tv, media_plan_id, alias);
																						nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration sixteen-->'+alias_duration_sixteen)
																						
																						var alias_number_sixteen=get_alias(ms_sixteen, media_type_tv, media_plan_id, alias);
														                                nlapiLogExecution('DEBUG','Media Schedule',' Alias Number sixteen -->'+alias_number_sixteen)
															
																						amount_sixteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_sixteen,alias_number_sixteen)
																						nlapiLogExecution('DEBUG','Media Schedule',' Amount sixteen -->'+amount_sixteen)
																																											
																					}//sixteen
																					
																					
																					if(ms_seventeen!=null && ms_seventeen!='' && ms_seventeen!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_seventeen=get_aliasduration(ms_seventeen, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration seventeen-->'+alias_duration_seventeen)
																						
																						var alias_number_seventeen=get_alias(ms_seventeen, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number seventeen -->'+alias_number_seventeen)
															
																						amount_seventeen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_seventeen,alias_number_seventeen)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount seventeen -->'+amount_seventeen)
																																											
																					}//seventeen
																					if(ms_eighteen!=null && ms_eighteen!='' && ms_eighteen!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_eighteen=get_aliasduration(ms_eighteen, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration eighteen-->'+alias_duration_eighteen)
																						
																						var alias_number_eighteen=get_alias(ms_eighteen, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number eighteen -->'+alias_number_eighteen)
															
																						amount_eighteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_eighteen,alias_number_eighteen)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount eighteen -->'+amount_eighteen)
																																											
																					}//eighteen
																					if(ms_ninteen!=null && ms_ninteen!='' && ms_ninteen!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_ninteen=get_aliasduration(ms_ninteen, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration ninteen-->'+alias_duration_ninteen)
																						
																						var alias_number_ninteen=get_alias(ms_ninteen, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number ninteen -->'+alias_number_ninteen)
															
																						amount_ninteen=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_ninteen,alias_number_ninteen)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount ninteen -->'+amount_ninteen)
																																											
																					}//ninteen
																					if(ms_twenty!=null && ms_twenty!='' && ms_twenty!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twenty=get_aliasduration(ms_twenty, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twenty-->'+alias_duration_twenty)
																						
																						var alias_number_twenty=get_alias(ms_twenty, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twenty -->'+alias_number_twenty)
															
																						amount_twenty=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twenty,alias_number_twenty)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twenty -->'+amount_twenty)
																																											
																					}//twenty
																					if(ms_twentyone!=null && ms_twentyone!='' && ms_twentyone!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentyone=get_aliasduration(ms_twentyone, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentyone-->'+alias_duration_twentyone)
																						
																						var alias_number_twentyone=get_alias(ms_twentyone, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentyone -->'+alias_number_twentyone)
															
																						amount_twentyone=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentyone,alias_number_twentyone)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentyone -->'+amount_twentyone)
																																											
																					}//twentyone
																					if(ms_twentytwo!=null && ms_twentytwo!='' && ms_twentytwo!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentytwo=get_aliasduration(ms_twentytwo, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentytwo-->'+alias_duration_twentytwo)
																						
																						var alias_number_twentytwo=get_alias(ms_twentytwo, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentytwo -->'+alias_number_twentytwo)
															
																						amount_twentytwo=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentytwo,alias_number_twentytwo)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentytwo -->'+amount_twentytwo)
																																											
																					}//twentytwo
																					if(ms_twentythree!=null && ms_twentythree!='' && ms_twentythree!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentythree=get_aliasduration(ms_twentythree, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentythree-->'+alias_duration_twentythree)
																						
																						var alias_number_twentythree=get_alias(ms_twentythree, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentythree -->'+alias_number_twentythree)
															
																						amount_twentythree=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentythree,alias_number_twentythree)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentythree -->'+amount_twentythree)
																																											
																					}//twentythree
																					if(ms_twentyfour!=null && ms_twentyfour!='' && ms_twentyfour!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentyfour=get_aliasduration(ms_twentyfour, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentyfour-->'+alias_duration_twentyfour)
																						
																						var alias_number_twentyfour=get_alias(ms_twentyfour, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentyfour -->'+alias_number_twentyfour)
															
																						amount_twentyfour=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentyfour,alias_number_twentyfour)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentyfour -->'+amount_twentyfour)
																																											
																					}//twentyfour
																					if(ms_twentyfive!=null && ms_twentyfive!='' && ms_twentyfive!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentyfive=get_aliasduration(ms_twentyfive, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentyfive-->'+alias_duration_twentyfive)
																						
																						var alias_number_twentyfive=get_alias(ms_twentyfive, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentyfive -->'+alias_number_twentyfive)
															
																						amount_twentyfive=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentyfive,alias_number_twentyfive)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentyfive -->'+amount_twentyfive)
																																											
																					}//twentyfive
																					if(ms_twentysix!=null && ms_twentysix!='' && ms_twentysix!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentysix=get_aliasduration(ms_twentysix, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentysix-->'+alias_duration_twentysix)
																						
																						var alias_number_twentysix=get_alias(ms_twentysix, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentysix -->'+alias_number_twentysix)
															
																						amount_twentysix=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentysix,alias_number_twentysix)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentysix -->'+amount_twentysix)
																																											
																					}//twentysix
																					if(ms_twentyseven!=null && ms_twentyseven!='' && ms_twentyseven!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentyseven=get_aliasduration(ms_twentyseven, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentyseven-->'+alias_duration_twentyseven)
																						
																						var alias_number_twentyseven=get_alias(ms_twentyseven, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentyseven -->'+alias_number_twentyseven)
															
																						amount_twentyseven=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentyseven,alias_number_twentyseven)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentyseven -->'+amount_twentyseven)
																																											
																					}//twentyseven
																					if(ms_twentyeight!=null && ms_twentyeight!='' && ms_twentyeight!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentyeight=get_aliasduration(ms_twentyeight, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentyeight-->'+alias_duration_twentyeight)
																						
																						var alias_number_twentyeight=get_alias(ms_twentyeight, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentyeight -->'+alias_number_twentyeight)
															
																						amount_twentyeight=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentyeight,alias_number_twentyeight)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentyeight -->'+amount_twentyeight)
																																											
																					}//twentyeight
																					if(ms_twentynine!=null && ms_twentynine!='' && ms_twentynine!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_twentynine=get_aliasduration(ms_twentynine, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration twentynine-->'+alias_duration_twentynine)
																						
																						var alias_number_twentynine=get_alias(ms_twentynine, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number twentynine -->'+alias_number_twentynine)
															
																						amount_twentynine=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_twentynine,alias_number_twentynine)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount twentynine -->'+amount_twentynine)
																																											
																					}//twentynine
																					if(ms_thirty!=null && ms_thirty!='' && ms_thirty!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_thirty=get_aliasduration(ms_thirty, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration thirty-->'+alias_duration_thirty)
																						
																						var alias_number_thirty=get_alias(ms_thirty, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number thirty -->'+alias_number_thirty)
															
																						amount_thirty=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_thirty,alias_number_thirty)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount thirty -->'+amount_thirty)
																																											
																					}//thirty
																					if(ms_thirtyone!=null && ms_thirtyone!='' && ms_thirtyone!=undefined )
																					{
																						//nlapiLogExecution('DEBUG','Media Schedule','  ********************* Alias Duration - Number ***************** ')
														
																						var alias_duration_thirtyone=get_aliasduration(ms_thirtyone, media_type_tv, media_plan_id, alias);
																						//nlapiLogExecution('DEBUG','Media Schedule',' Alias Duration thirtyone-->'+alias_duration_thirtyone)
																						
																						var alias_number_thirtyone=get_alias(ms_thirtyone, media_type_tv, media_plan_id, alias);
														                                //nlapiLogExecution('DEBUG','Media Schedule',' Alias Number thirtyone -->'+alias_number_thirtyone)
															
																						amount_thirtyone=calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration_thirtyone,alias_number_thirtyone)
																						//nlapiLogExecution('DEBUG','Media Schedule',' Amount thirtyone -->'+amount_thirtyone)
																																											
																					}//thirtyone
																					
																					
																					
																					
																			
																					
																					
																			//================= Quotation Calculation ======================		
																					
																																						
																			total_quotation_amount=parseFloat(total_quotation_amount)+parseFloat(month_quotation_amount);	
																			nlapiLogExecution('DEBUG',' Opputunity Calculation',' *********************************** Total Amount Quotation *********************************** -->'+total_quotation_amount)
																					
																					
																					
																			//================= Opputunity Calculation ======================
																									
																			
																			
																			total_opputunity_amount=parseFloat(total_opputunity_amount)+parseFloat(opputunity_amount);
																			nlapiLogExecution('DEBUG',' Opputunity Calculation',' *********************************** Total Amount Opputunity *********************************** -->'+total_opputunity_amount)
																			
																			
																			//================= Media Plan Calculation ======================		
																			
																			
																			//validateValue_zero(value)
																			
																			  amount_one=validateValue_zero(amount_one);
																			  amount_two=validateValue_zero(amount_two);
																			  amount_three=validateValue_zero(amount_three); 
																			  amount_four=validateValue_zero(amount_four);
																			  amount_five=validateValue_zero(amount_five);
																			  amount_six=validateValue_zero(amount_six); 
																			  amount_seven=validateValue_zero(amount_seven); 
																			  amount_eight=validateValue_zero(amount_eight); 
																			  amount_nine=validateValue_zero(amount_nine); 
																			  amount_ten=validateValue_zero(amount_ten); 
																			  amount_eleven=validateValue_zero(amount_eleven); 
																			  amount_twelve=validateValue_zero(amount_twelve); 
																			  amount_thirteen=validateValue_zero(amount_thirteen); 
																			  amount_fourteen=validateValue_zero(amount_fourteen);
																			  amount_fifteen=validateValue_zero(amount_fifteen);
																			  amount_sixteen=validateValue_zero(amount_sixteen);
																			  amount_seventeen=validateValue_zero(amount_seventeen); 
																			  amount_eighteen=validateValue_zero(amount_eighteen);
																			  amount_ninteen=validateValue_zero(amount_ninteen);
																			  amount_twenty=validateValue_zero(amount_twenty);
																			  amount_twentyone=validateValue_zero(amount_twentyone);
																			  amount_twentytwo=validateValue_zero(amount_twentytwo);
																			  amount_twentythree=validateValue_zero(amount_twentythree); 
																			  amount_twentyfour=validateValue_zero(amount_twentyfour); 
																			  amount_twentyfive=validateValue_zero(amount_twentyfive); 
																			  amount_twentysix=validateValue_zero(amount_twentysix);
																			  amount_twentyseven=validateValue_zero(amount_twentyseven); 
																			  amount_twentyeight=validateValue_zero(amount_twentyeight); 
																			  amount_twentynine=validateValue_zero(amount_twentynine);
																			  amount_thirty=validateValue_zero(amount_thirty);
																			  amount_thirtyone=validateValue_zero(amount_thirtyone);
																			
																			
																			
																																					
																																					
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount One   -->'+amount_one)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Two   -->'+amount_two)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Three   -->'+amount_three)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Four   -->'+amount_four)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Five   -->'+amount_five)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Six   -->'+amount_six)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Seven   -->'+amount_seven)
																			nlapiLogExecution('DEBUG',' Opputunity Calculation','  Amount Eight   -->'+amount_eight)
																			
	                                                                       alias_total= parseFloat(amount_one)+parseFloat(amount_two)+parseFloat(amount_three)+ parseFloat(amount_four)+parseFloat(amount_five)+parseFloat(amount_six)+ parseFloat(amount_seven)+parseFloat(amount_eight)+parseFloat(amount_nine)+ parseFloat(amount_ten)+ parseFloat(amount_eleven)+parseFloat(amount_twelve )+parseFloat(amount_thirteen)+parseFloat(amount_fourteen)+parseFloat(amount_fifteen)+parseFloat(amount_sixteen)+parseFloat(amount_seventeen)+ parseFloat(amount_eighteen)+ parseFloat(amount_ninteen)+parseFloat(amount_twenty)+ parseFloat(amount_twentyone)+parseFloat(amount_twentytwo)+parseFloat(amount_twentythree)+parseFloat(amount_twentyfour)+parseFloat(amount_twentyfive)+parseFloat(amount_twentysix)+parseFloat(amount_twentyseven)+parseFloat(amount_twentyeight)+ parseFloat(amount_twentynine)+parseFloat(amount_thirty)+parseFloat(amount_thirtyone)
	                                                                       nlapiLogExecution('DEBUG',' Opputunity Calculation',' *********************************** Total Alias  *********************************** -->'+alias_total)
																		   
																		   total_media_plan_amount=parseFloat(total_media_plan_amount)+parseFloat(alias_total);
	                                                                       nlapiLogExecution('DEBUG',' Opputunity Calculation',' *********************************** Total Amount Media Plan  *********************************** -->'+total_media_plan_amount)
																				
																					
																				
																			 }//Sales Amount																		 	
																			
																			
																		 }//Sales Quantity
																			
																		
																	  }//Media Schedule Object
																																
																}//IF
															 															  																
																
															}//For - Media Schedule Results												
													   }//Search Media Schedule
														
														
													//========================== Media Schedule ==================================	
														
														
														
														
														
														
														
														
														
														
														
														
													}//For- Serach Results Quotation 
												
																								
											}//Search Quotation Results
											
										
									 }//For - Search Media Plan
									
									print_arr[pnt++]=sales_rep_text+'$$'+total_opputunity_amount+'$$'+total_media_plan_amount+'$$'+total_quotation_amount;	
									
								 }//Search Media Plan  Results
								 
																
							 }// For -Search Ungrouped Opputunity
							
							
							
						 }// If- Search Opportunity Results
						
					}//Sales Rep ID Check
					
										
				 }//For - Search Grouped Opputunity
				 				
				
			 }//End Date			
			
		 }//Start Date
	  	
	  }//Department Check
	
	
	
	
	
	
	
	
 
	
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' ****** Print Array ******-->' + print_arr);
	nlapiLogExecution('DEBUG', 'schedulerFunction', ' ****** Print Length ******-->-->' + print_arr.length);
	
	appendix_J_print_layout(month,year,print_arr);
	
		
	}//Try
	catch(er)
	{
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' ****** Exception Caught ******-->' + er);
		
	}//Catch	
	
    
}//Scheduler Function

// END SCHEDULED FUNCTION ===============================================




function validateValue(value)
{
	var value_res;
	
	if(value==null || value ==undefined || value=='' || value ==NaN || value == 'NaN')
	{
		value_res='';
		return value_res;
	}
	else
	{
		return value;
	}
	
}



function validateValue_zero(value)
{
	var value_res;
	if(value==null || value ==undefined || value=='' || value ==NaN || value.toString() == 'NaN' || value == '.00' || value=='NaN')
	{
		value_res=0;
		return value_res;
	}
	else
	{
		return value;
	}
	
}





function appendix_J_print_layout(month,year,print_arr)
{
	
	var department_text='TV BEC-TERO';
	var month_var;
	
	            if (month ==1 )
				{
					
					month_var='January';
					
				}
				else if(month ==2) 
				{
					
					month_var= 'February';
					
				}
				else if(month  == 3)
				{
					month_var='March';
					
					
				}
				else if( month ==4 )
				{
					month_var='April';
					
					
				}
				else if(month ==5)
				{
					month_var= 'May';
					
					
				}
				else if( month ==6 )
				{
					month_var='June';
					
					
				}
				else if( month == 7)
				{
					month_var='July';
					
					
				}
				else if( month ==8 )
				{
					
					month_var='August';
					
				}
				else if(month == 9)
				{
					month_var='September';
					
					
				}
				else if( month == 10)
				{
					month_var='October';
									
				}
				else if(month == 11)
				{
					month_var='November';
					
					
				}
				else if(month == 12) 
				{
					month_var='December';
				}
				
			     
	var year_txt=year_text(year);
	
	
	
var strVar="";
strVar += "<table border=\"1\" width=\"100%\">";
strVar += "	<tr>";
strVar += "		<td>";
strVar += "		<table border=\"0\" width=\"100%\">";
strVar += "			<tr>";
strVar += "				<td font-family=\"Helvetica\" align=\"center\" font-size=\"10\" ><b>Monthly Sales Dashboard<\/b><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td>&nbsp;<\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-family=\"Helvetica\" font-size=\"8\" ><b>Month : "+nlapiEscapeXML(month_var)+"<\/b><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-family=\"Helvetica\" font-size=\"8\" ><b>Year : "+nlapiEscapeXML(year_txt)+"<\/b><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td font-family=\"Helvetica\" font-size=\"8\"><b>Department : "+nlapiEscapeXML(department_text)+"<\/b> <\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td>&nbsp;<\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
strVar += "				<td>";
strVar += "				<table border=\"0\" width=\"100%\">";
strVar += "					<tr>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>Sales Rep<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>Target<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>Booked<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>%<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>Potential<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>%<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>Potential+Booked<\/b><\/td>";
strVar += "						<td border-bottom=\"0.3\" border-top=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><b>%<\/b><\/td>";
strVar += "					<\/tr>";


    
	
   if(print_arr.length!=0)
   {
   		
	var j_array = new Array();
    
    
    var j_sales_rep;
    var j_oppurtunity_amount;
    var j_media_plan_amount;
    var j_quotation_total;
	
	var percent_1=0;
	var percent_2=0;
	var percent_3=0;
	var potential_plus_booked=0;
   	
	for(var w = 0; w < print_arr.length; w++)
	{
		j_array = print_arr[w].split('$$');
		j_sales_rep = j_array[0];
	    j_oppurtunity_amount = j_array[1];
	    j_media_plan_amount = j_array[2];
	    j_quotation_total = j_array[3];
				
		 j_sales_rep=validateValue(j_sales_rep);
		 j_oppurtunity_amount=validateValue_zero(j_oppurtunity_amount);
		 j_media_plan_amount=validateValue_zero(j_media_plan_amount);
		 j_quotation_total=validateValue_zero(j_quotation_total);
		 
		
		 nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_oppurtunity_amount -->' + j_oppurtunity_amount); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_quotation_total -->' + j_quotation_total); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_media_plan_amount -->' + j_media_plan_amount);
				
			
		 percent_1=parseFloat(j_media_plan_amount)/parseFloat(j_oppurtunity_amount);
		 percent_1=parseFloat(percent_1)*100;
		 percent_1=parseFloat(percent_1)	
		 percent_1=validateValue_zero(percent_1);
		 
		
		 percent_2=parseFloat(j_quotation_total)/parseFloat(j_oppurtunity_amount);
		 percent_2=parseFloat(percent_2)*100;
		 percent_2=parseFloat(percent_2)	
		 percent_2=validateValue_zero(percent_2);
		 		
		 potential_plus_booked=parseFloat(j_media_plan_amount)+parseFloat(j_quotation_total);
				 
		 potential_plus_booked=validateValue_zero(potential_plus_booked);
		  		 
		 percent_3=parseFloat(potential_plus_booked)/parseFloat(j_oppurtunity_amount);
		 
		 percent_3=parseFloat(percent_3)*100;
		 percent_3=parseFloat(percent_3)		
		 percent_3=validateValue_zero(percent_3);
		 
			
		 percent_1=percent_1.toFixed(2);
		 percent_2=percent_2.toFixed(2);
		 percent_3=percent_3.toFixed(2);
		 potential_plus_booked=potential_plus_booked.toFixed(2);
		 
			
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_1 -->' + percent_1); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_2 -->' + percent_2); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_3 -->' + percent_3); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  potential_plus_booked -->' + potential_plus_booked); 
	
		 j_sales_rep=nlapiEscapeXML(j_sales_rep);
		 j_oppurtunity_amount=nlapiEscapeXML(j_oppurtunity_amount);
		 j_media_plan_amount=nlapiEscapeXML(j_media_plan_amount);
		 j_quotation_total=nlapiEscapeXML(j_quotation_total);
		 
		 percent_1=nlapiEscapeXML(percent_1);
		 percent_2=nlapiEscapeXML(percent_2);
		 percent_3=nlapiEscapeXML(percent_3);
		 potential_plus_booked=nlapiEscapeXML(potential_plus_booked);
		   
		   
		
		strVar += "					<tr>";
		strVar += "						<td border-bottom=\"0.3\"  border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+j_sales_rep+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+j_oppurtunity_amount+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+j_media_plan_amount+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+percent_1+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+j_quotation_total+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+percent_2+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+potential_plus_booked+"<\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\">"+percent_3+"<\/td>";
		strVar += "					<\/tr>";
	
		
	}//for  print_arr
	
} 
else
{
	   
		
		
		 strVar += "					<tr>";
		strVar += "						<td border-bottom=\"0\"  border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "					<\/tr>";
		
		
		
		 strVar += "					<tr>";
		strVar += "						<td border-bottom=\"0\"  border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "					<\/tr>";
		
		
		 strVar += "					<tr>";
		strVar += "						<td border-bottom=\"0.3\"  border-left=\"0.3\" border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "						<td border-bottom=\"0.3\"  border-right=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\"><\/td>";
		strVar += "					<\/tr>";
		
	
}   







strVar += "				<\/table>";
strVar += "				<\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "<\/table>";
strVar += "";






    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf>\n<body>";
    xml += strVar;
    xml += "</body>\n</pdf>";
    // run the BFO library to convert the xml document to a PDF
    var file = nlapiXMLToPDF(xml);
    //Create PDF File with TimeStamp in File Name
	
	var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	var fileObj = nlapiCreateFile('Appendix J.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
	
	
  //  var fileObj = nlapiCreateFile('Appendix J.pdf', 'PDF', file.getValue());
    nlapiLogExecution('DEBUG', 'Appendix J PDF', ' PDF fileObj = ' + fileObj);
    fileObj.setFolder(219);// Folder PDF File is to be stored
    var fileId = nlapiSubmitFile(fileObj);
    nlapiLogExecution('DEBUG', 'Appendix J PDF', '*************===PDF fileId *********** ==' + fileId);
	
	
	
	
		
		
		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		
		
		
		
		
		
		
		
		
		// ======================= Excel ============================
		
		
		




 var xml = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
 xml +='<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"   xmlns:o="urn:schemas-microsoft-com:office:office"   xmlns:x="urn:schemas-microsoft-com:office:excel"   xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"   xmlns:html="http://www.w3.org/TR/REC-html40">';
 xml += '<Worksheet ss:Name="Sheet1">';
 xml +=   '<Table>';
 
 
 
 
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String"> Monthly Sales Dashboard </Data></Cell>';
 xml +=    '</Row>'
 
 
 
 
  
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String">  </Data></Cell>';
 xml +=    '</Row>'
 
 
 
  
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String"> Month </Data></Cell>';
  xml +=       '<Cell><Data ss:Type="String">  '+ nlapiEscapeXML(month_var) +' </Data></Cell>';
 xml +=    '</Row>'
 
 
 
  
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String"> Year </Data></Cell>';
  xml +=       '<Cell><Data ss:Type="String"> '+ nlapiEscapeXML(year_txt) +' </Data></Cell>';
 xml +=    '</Row>'
 
 
 
 
  
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String"> Department </Data></Cell>';
  xml +=       '<Cell><Data ss:Type="String"> '+ nlapiEscapeXML(department_text) +' </Data></Cell>';
 xml +=    '</Row>'
 
 
 
 
  
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String">  </Data></Cell>';
 
 xml +=    '</Row>'
 
 
 
 xml +=    '<Row>';
 xml +=       '<Cell><Data ss:Type="String"> Sales Rep </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> Target </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> Booked  </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> % </Data></Cell>'; 
 xml +=       '<Cell><Data ss:Type="String"> Potential </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> % </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> Potential + Booked  </Data></Cell>';
 xml +=       '<Cell><Data ss:Type="String"> % </Data></Cell>'; 
 xml +=    '</Row>'
 
 
 
 
   if(print_arr.length!=0)
   {
   		
	var j_array = new Array();
    
    
    var j_sales_rep;
    var j_oppurtunity_amount;
    var j_media_plan_amount;
    var j_quotation_total;
	
	var percent_1=0;
	var percent_2=0;
	var percent_3=0;
	var potential_plus_booked=0;
   	
	for(var w = 0; w < print_arr.length; w++)
	{
		j_array = print_arr[w].split('$$');
		j_sales_rep = j_array[0];
	    j_oppurtunity_amount = j_array[1];
	    j_media_plan_amount = j_array[2];
	    j_quotation_total = j_array[3];
				
		 j_sales_rep=validateValue(j_sales_rep);
		 j_oppurtunity_amount=validateValue_zero(j_oppurtunity_amount);
		 j_media_plan_amount=validateValue_zero(j_media_plan_amount);
		 j_quotation_total=validateValue_zero(j_quotation_total);
		 
		/*

		 nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_oppurtunity_amount -->' + j_oppurtunity_amount); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_quotation_total -->' + j_quotation_total); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  j_media_plan_amount -->' + j_media_plan_amount);
				
*/
			
		 percent_1=parseFloat(j_media_plan_amount)/parseFloat(j_oppurtunity_amount);
		 percent_1=parseFloat(percent_1)*100;
		 percent_1=parseFloat(percent_1)	
		 percent_1=validateValue_zero(percent_1);
		 
		
		 percent_2=parseFloat(j_quotation_total)/parseFloat(j_oppurtunity_amount);
		 percent_2=parseFloat(percent_2)*100;
		 percent_2=parseFloat(percent_2)	
		 percent_2=validateValue_zero(percent_2);
		 		
		 potential_plus_booked=parseFloat(j_media_plan_amount)+parseFloat(j_quotation_total);
				 
		 potential_plus_booked=validateValue_zero(potential_plus_booked);
		  		 
		 percent_3=parseFloat(potential_plus_booked)/parseFloat(j_oppurtunity_amount);
		 
		 percent_3=parseFloat(percent_3)*100;
		 percent_3=parseFloat(percent_3)		
		 percent_3=validateValue_zero(percent_3);
		 
			
		 percent_1=percent_1.toFixed(2);
		 percent_2=percent_2.toFixed(2);
		 percent_3=percent_3.toFixed(2);
		 potential_plus_booked=potential_plus_booked.toFixed(2);
		 
			
		/*
nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_1 -->' + percent_1); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_2 -->' + percent_2); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  percent_3 -->' + percent_3); 
		nlapiLogExecution('DEBUG', 'schedulerFunction', '  potential_plus_booked -->' + potential_plus_booked); 
	
*/
		 j_sales_rep=nlapiEscapeXML(j_sales_rep);
		 j_oppurtunity_amount=nlapiEscapeXML(j_oppurtunity_amount);
		 j_media_plan_amount=nlapiEscapeXML(j_media_plan_amount);
		 j_quotation_total=nlapiEscapeXML(j_quotation_total);
		 
		 percent_1=nlapiEscapeXML(percent_1);
		 percent_2=nlapiEscapeXML(percent_2);
		 percent_3=nlapiEscapeXML(percent_3);
		 potential_plus_booked=nlapiEscapeXML(potential_plus_booked);
		   
		 
	
	
	    xml += '<Row>';
        xml += ' <Cell><Data ss:Type="String">' + j_sales_rep + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="String">' + j_oppurtunity_amount + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="Number">' + j_media_plan_amount + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="String">' + percent_1 + '</Data></Cell>';
		xml += ' <Cell><Data ss:Type="String">' + j_quotation_total + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="String">' + percent_2 + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="Number">' + potential_plus_booked + '</Data></Cell>';
        xml += ' <Cell><Data ss:Type="String">' + percent_3 + '</Data></Cell>';
        
        xml += '</Row>';                
	
	
	
	
	
		
	}//for  print_arr
	
} 
 
          
 xml +=  '</Table>';
 xml += '</Worksheet>';
 xml += '</Workbook>';
 
    var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	//var fileObj = nlapiCreateFile('Appendix J.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
	
 
 
 
 var file = nlapiCreateFile('Appendix J_'+timestamp+'.xls', 'XMLDOC', xml);
 nlapiLogExecution('DEBUG', 'schedulerFunction', '  file -->' + file); 
 
 file.setFolder(219);
 
 var fileId_xls = nlapiSubmitFile(file);
 nlapiLogExecution('DEBUG', 'schedulerFunction', '    Excel File Id -->' + fileId); 
 



		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId_xls);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Excel Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		
		
		
		
		






		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	
	
	
}

function get_aliasduration(ms_alias, media_type_tv, mediaplanID, MediaPlan_Alias)
{
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    
    
    var aliasQuantity = ms_alias.split(',')
    //  nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
    
    for (var k = 0; k < aliasQuantity.length; k++)
	{
    
        var alias_ms = aliasQuantity[k];
        //	nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
        
        var letter = "";
        var number = "";
        
        for (var q = 0; q < alias_ms.length; q++)
		 {
            var b = alias_ms.charAt(q);
            if (isNaN(b) == true) 
			{
                letter = letter + b;
            }
            else 
			{
                number = number + b;
            }
            
            
        }//END - For
        //   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
        //    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
        //   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','MediaPlan_Alias ='+MediaPlan_Alias);
        if (MediaPlan_Alias == letter) 
		{
        
            var medialist_result = searchMedialist(letter, media_type_tv, mediaplanID);
            nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Media List Result =' + medialist_result);
            
            if (medialist_result != null && medialist_result != '' && medialist_result != undefined) 
			{
            
                var mlArr = new Array();
                mlArr = medialist_result.split('##');
                ml_recordID = mlArr[0];
                ml_productname = mlArr[1];
                ml_medianame = mlArr[2];
                ml_newduration = mlArr[3];
                
        	    nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
        	    nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
        		nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
                nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration); 
            
            
            }//ENMD- Media Result
        }//Media Alias Check
    }//For Loop Alias
    return ml_newduration;
    
}//END Function Alias
//get_alias(media_sch_alias,media_type,media_plan_id,alias);
function searchMedialist(letter, media_type_tv, recordID)
{
	if(letter != '' && letter != null && letter != undefined)
	{
		if(media_type_tv != '' && media_type_tv != null && media_type_tv != undefined)
		{
			if(recordID != '' && recordID != null && recordID != undefined)
			{
				
					
	
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
    
    
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    var result;
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
    filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
    filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
    columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
    columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++) 
		{
            ml_recordID = searchresults[j].getValue('internalid');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
            
            ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
            
            ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
            //		nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
            
            ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' + j + ']==' + ml_newduration);
            
            result = ml_recordID + '##' + ml_productname + '##' + ml_medianame + '##' + ml_newduration;
            
        }
        
    }
    
    return result;
				
				
			}
				
		}
		
	}
	
	

    
}



function year_internalid(year, media_plan_id)
{
    var recordID;
    var year_ID;
    
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', media_plan_id);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, filter, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'Year ID', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++)
		{
            recordID = searchresults[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + recordID);
        
        }
    }
    
    
    
    var custOBJ = nlapiLoadRecord('customrecord_mediaplan_3', recordID);
    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Media Schedule -->' + custOBJ)
    
    if (custOBJ != null) 
	{
    
        var myFld = custOBJ.getField('custrecord_mp3_year')
        var options = myFld.getSelectOptions();
        nlapiLogExecution('Debug', 'CheckParameters', 'options=' + options.length);
        
        for (var i = 0; i < options.length; i++)
		{
            var yearId = options[i].getId();
            var yearText = options[i].getText();
            nlapiLogExecution('DEBUG', 'suiteletFunction', 'i -->' + i + 'Year ID -->' + yearId + 'Year Text -->' + yearText);
            
            
            if (yearText == year)
			{
                year_ID = yearId;
                break;
            }
            
            
        }//For
    }//Customer
    return year_ID;
}




function year_text(year)
{
    var recordID;
    var year_ID;
    
    
   
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, null, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'Year ID', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++)
		{
            recordID = searchresults[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + recordID);
        
        }
    }
    
    
    
    var custOBJ = nlapiLoadRecord('customrecord_mediaplan_3', recordID);
    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Media Schedule -->' + custOBJ)
    
    if (custOBJ != null) 
	{
    
        var myFld = custOBJ.getField('custrecord_mp3_year')
        var options = myFld.getSelectOptions();
        nlapiLogExecution('Debug', 'CheckParameters', 'options=' + options.length);
        
        for (var i = 0; i < options.length; i++)
		{
            var yearId = options[i].getId();
            var yearText = options[i].getText();
            nlapiLogExecution('DEBUG', 'suiteletFunction', 'i -->' + i + 'Year ID -->' + yearId + 'Year Text -->' + yearText);
            
            
            if (yearId == year)
			{
                year_ID = yearText;
                break;
            }
            
            
        }//For
    }//Customer
    return year_ID;
}




function sales_order_amount(sales_order_id,tv_program_item)
{
	var sales_OBJ;
	var net_amount;
	var net_quantity;
	var sales_result;
    if (sales_order_id != null && sales_order_id != '') 
	{
		sales_OBJ = nlapiLoadRecord('salesorder', sales_order_id);
		nlapiLogExecution('DEBUG', ' Sales Order ', 'Sales Order Object -->' + sales_OBJ)
		     				 
            
	
	 if (sales_OBJ != null && sales_OBJ != '') 
	{
    
        var item_count = sales_OBJ.getLineItemCount('item');
       nlapiLogExecution('DEBUG',' Sales Order ','Item Count-->'+item_count);
        
        if (item_count != null) 
		{
            for (var p = 1; p <= item_count; p++) 
			{
                var item_id = sales_OBJ.getLineItemValue('item', 'item', p);
                nlapiLogExecution('DEBUG', 'Sales Order', ' Item -->' + item_id);
                
                item_grossamount = sales_OBJ.getLineItemValue('item', 'amount', p);
				item_grossamount=validateValue_zero(item_grossamount);
                nlapiLogExecution('DEBUG', 'Sales Order', ' Gross Amount -->' + item_grossamount);
                
                
                item_quantity = sales_OBJ.getLineItemValue('item', 'quantity', p);
                item_quantity=validateValue_zero(item_quantity);
				nlapiLogExecution('DEBUG', 'Sales Order', ' Quantity -->' + item_quantity);
                
                if (item_id == tv_program_item) 
				{
                    net_amount = item_grossamount;
                    net_quantity = item_quantity;
                    sales_result=net_amount+'%%'+net_quantity;
					nlapiLogExecution('DEBUG', 'Sales Order', ' Net Amount -->' + net_amount);
                    nlapiLogExecution('DEBUG', 'Sales Order', ' Net Quantity -->' + net_quantity);
                    break;
                }
             
            }//Sales For
			
			
        }//Line Count Check
		
    }//Sales OBJ
	}//Sales ID Check
	
	return sales_result;
}






function get_alias(ms_alias, media_type_tv, mediaplanID, MediaPlan_Alias)
{
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    var aliasnumber;
    
    
    var aliasQuantity = ms_alias.split(',')
    //  nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
    
    for (var k = 0; k < aliasQuantity.length; k++)
	 {
    
        var alias_ms = aliasQuantity[k];
        //nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
        
        var letter = "";
        var number = "";
        
        for (var q = 0; q < alias_ms.length; q++) 
		{
            var b = alias_ms.charAt(q);
            if (isNaN(b) == true) 
			{
                letter = letter + b;
            }
            else 
			{
                number = number + b;
            }
            
            
        }//END - For
        //   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
        //   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
        
        if (MediaPlan_Alias == letter)
		 {
            aliasnumber = number;
            nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', '***********aliasnumber **********=' + aliasnumber)
            
        }//Media Alias Check
    }//For Loop Alias
    return aliasnumber;
    
}//END Function Alias



function calculation_amount_spot_other_than_spot(media_type_tv,sales_net_quantity,sales_net_amount,alias_duration,alias_number)
{
	var amount;
	 if (media_type_tv == 1) 
	 {
	 	sales_net_quantity = parseFloat(sales_net_quantity) * 60;
        spot_rate = parseFloat(sales_net_amount) / parseFloat(sales_net_quantity);
		spot_rate = parseFloat(spot_rate) * parseFloat(alias_number) * parseFloat(alias_duration);
        nlapiLogExecution('DEBUG', 'calculation_amount_spot_other_than_spot', ' ************ Spot Rate *************** -->' + spot_rate);
	 	amount=spot_rate;
		
	 }//1 ................
	 
	  else if (media_type_tv == 3 || media_type_tv==4) 
	  {
	  	other_than_rate = parseFloat(sales_net_amount) / parseFloat(sales_net_quantity);
		
        other_than_rate = parseFloat(other_than_rate) * parseFloat(alias_number);
        
		nlapiLogExecution('DEBUG', 'calculation_amount_spot_other_than_spot', ' ************* Other Than  Spot Rate **************-->' + other_than_rate);
									  	
		amount=other_than_rate;
	  }//3...4....
	
	return amount;
	
}//calculation_amount_spot_other_than_spot












