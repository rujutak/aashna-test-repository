// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : 
	Author      : 
	Company     : 
	Date        : 
	Description : 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)


     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{
	/*
   * Function Description : Set the client-script internal ID  
   * customscript_deliverychallan_printbutton --> Internal Script ID of Client SCript , for which it is to be redirected.               
   * custpage_appendixc_print -- > Internal ID of DC Print Button
   * Appendix C Print --> Button Name
   *  --> Script Internal ID of Client Script
   * openAppendixC() --> Function Name of Client Script  whose Script Internal id is passed
   * Variable Description : 
   */	
   
    if(type=='view')
	{
	nlapiLogExecution('DEBUG','suiteletFunction','In User Event');
	form.setScript('customscript_cli_mp_createappendixcprint');
	form.addButton('custpage_appendixc_print','Appendix C Print','openAppendixC()');	
	}
   
	return true;


}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
