// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var month_var=''
	var s_terms;
	var s_tv_credit;
	var s_remark;
	var s_termsOfPayment='';
	var ml_Array=new Array();
	var ms_Array=new Array();
	var ml_cnt=0;
	var ms_cnt=0;
	
	var ms_Array_feb=new Array();
	var ms_cnt_feb=0;	
	var ms_Array_mar=new Array();
	var ms_cnt_mar=0;
	var janVar="";
	var febVar="";
	var marVar="";
	var aprVar="";
	var mayVar="";
	var junVar="";
	var julyVar="";
	var augVar="";
	var sepVar="";
	var octVar="";
	var novVar="";
	var decVar="";
	var month_arr=new Array();
	
    var recordID=request.getParameter('CurrentRecordID');	
	nlapiLogExecution('DEBUG','suiteletFunction','  Record ID [ Media Plan ]--->'+recordID);
	
	var recordOBJ=nlapiLoadRecord('customrecord_mediaplan_1',recordID);
	nlapiLogExecution('DEBUG','suiteletFunction',' Record OBJ [ Media Plan ]--->'+recordOBJ);
	
	if(recordOBJ!=null)
	{
		var s_media_plan=recordOBJ.getFieldValue('custrecord3');
		s_media_plan=validateValue(s_media_plan);
		nlapiLogExecution('DEBUG','suiteletFunction','  Number [ Media Plan ]--->'+s_media_plan);
			
		
		
		var d_issueDate=recordOBJ.getFieldValue('created');
		d_issueDate=validateValue(d_issueDate);
		nlapiLogExecution('DEBUG','suiteletFunction','  Issue Date [ Media Plan ]--->'+d_issueDate);
		
		var s_advertiser=recordOBJ.getFieldText('custrecord_mp1_customer');
		s_advertiser=validateValue(s_advertiser);
		nlapiLogExecution('DEBUG','suiteletFunction','  Advertiser [ Media Plan ]--->'+s_advertiser);
		
		var s_advertiser_address=recordOBJ.getFieldValue('custrecord_mp1_custaddress');
		s_advertiser_address=validateValue(s_advertiser_address);
		nlapiLogExecution('DEBUG','suiteletFunction','  Advertiser Address[ Media Plan ]--->'+s_advertiser_address);
		
		var i_salesorder_id=recordOBJ.getFieldValue('custrecord_mp1_soref');
		nlapiLogExecution('DEBUG','suiteletFunction','  Sales Order ID [ Media Plan ]--->'+i_salesorder_id);
	
	    var s_salesrep=recordOBJ.getFieldText('custrecord_mp1_salesrep');
		s_salesrep=validateValue(s_salesrep);
		nlapiLogExecution('DEBUG','suiteletFunction','  Sales REp [ Media Plan ]--->'+s_salesrep);
	 
	   var media_type_tv='';
	   
	
	
	    var i_linecount_mediaschedule=recordOBJ.getLineItemCount('recmachcustrecord_mp3_mp1ref');
		nlapiLogExecution('DEBUG','suiteletFunction','  Line Count Media SChedule [ Media Plan ]--->'+i_linecount_mediaschedule);
	   
	    if(i_linecount_mediaschedule!=null)
		{
			for(var i=1;i<=i_linecount_mediaschedule;i++)
			{
				var s_tvProgram_ID=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',i);
				s_tvProgram_ID=validateValue(s_tvProgram_ID);
				nlapiLogExecution('DEBUG','suiteletFunction','  TV Program ID [ Media Schedule ]['+i+']--->'+s_tvProgram_ID);
				
				if(s_tvProgram_ID!=null && s_tvProgram_ID!='')
				{
					var tv_programOBJ=nlapiLoadRecord('serviceitem',s_tvProgram_ID);
					nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Object [ Media Schedule ]['+i+']--->'+tv_programOBJ);
				    
					if(tv_programOBJ!=null)
					{
						media_type_tv=tv_programOBJ.getFieldText('custitem_bec_mediatype');
						nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Media Type [ Media Schedule ]['+i+']--->'+media_type_tv);
					}
				
				}
				
				var s_tvProgram=recordOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_tvprogram',i);
				s_tvProgram=validateValue(s_tvProgram);
				nlapiLogExecution('DEBUG','suiteletFunction','  TV Program [ Media Schedule ]['+i+']--->'+s_tvProgram);
			
			    var s_spotQuota=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_spotquota',i);
				s_spotQuota=validateValue(s_spotQuota);
				nlapiLogExecution('DEBUG','suiteletFunction','  Spot Quota [ Media Schedule ]['+i+']--->'+s_spotQuota);
			
			    var s_planunit=recordOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref','custrecordcustrecord_mp3_tvprogramunit',i);
				s_planunit=validateValue(s_planunit);
				nlapiLogExecution('DEBUG','suiteletFunction','  Plan Unit [ Media Schedule ]['+i+']--->'+s_planunit);
			
			     var s_month_number=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',i);
				 s_month_number=validateValue(s_month_number);
				 nlapiLogExecution('DEBUG','suiteletFunction','  Month Number[ Media Schedule ]['+i+']--->'+s_month_number);
			      
			    var s_month=recordOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_month',i);
				s_month=validateValue(s_month);
				nlapiLogExecution('DEBUG','suiteletFunction','  Month [ Media Schedule ]['+i+']--->'+s_month);
			    
				
				 
			   
			    if (s_month == 'January')
				{
					
					month_var=1;
					
				}
				else if(s_month == 'February') 
				{
					
					month_var=2;
					
				}
				else if(s_month  == 'March')
				{
					month_var=3;
					
					
				}
				else if( s_month == 'April')
				{
					month_var=4;
					
					
				}
				else if(s_month == 'May')
				{
					month_var=5;
					
					
				}
				else if( s_month == 'June')
				{
					month_var=6;
					
					
				}
				else if( s_month == 'July')
				{
					month_var=7;
					
					
				}
				else if( s_month == 'August')
				{
					
					month_var=8;
					
				}
				else if(s_month == 'September')
				{
					month_var=9;
					
					
				}
				else if( s_month == 'October')
				{
					month_var=10;
									
				}
				else if(s_month == 'November')
				{
					month_var=11;
					
					
				}
				else if(s_month == 'December') 
				{
					month_var=12;
				}
				
			     
			
			
			
			
			    var s_year=recordOBJ.getLineItemText('recmachcustrecord_mp3_mp1ref','custrecord_mp3_year',i);
				s_year=validateValue(s_year);
				nlapiLogExecution('DEBUG','suiteletFunction','  Year [ Media Schedule ]['+i+']--->'+s_year);
			
			
			    var s_one=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_1',i);
				s_one=validateValue(s_one);
				nlapiLogExecution('DEBUG','suiteletFunction','  One [ Media Schedule ]['+i+']--->'+s_one);
				
				var s_two=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_2',i);
				s_two=validateValue(s_two);
				nlapiLogExecution('DEBUG','suiteletFunction','  Two [ Media Schedule ]['+i+']--->'+s_two)
				
				
				var s_three=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_3',i);
				s_three=validateValue(s_three);
				nlapiLogExecution('DEBUG','suiteletFunction','  Three [ Media Schedule ]['+i+']--->'+s_three);
				
				var s_four=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_4',i);
				s_four=validateValue(s_four);
				nlapiLogExecution('DEBUG','suiteletFunction','  Four [ Media Schedule ]['+i+']--->'+s_four);
				
			    var s_five=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_5',i);
				s_five=validateValue(s_five);
				nlapiLogExecution('DEBUG','suiteletFunction','  Five [ Media Schedule ]['+i+']--->'+s_five);
				
				var s_six=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_6',i);
				s_six=validateValue(s_six);
							
				var s_seven=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_7',i);
				s_seven=validateValue(s_seven);
								
				var s_eight=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_8',i);
				s_eight=validateValue(s_eight);
				
				var s_nine=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_9',i);
				s_nine=validateValue(s_nine);
			
				var s_ten=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_10',i);
				s_ten=validateValue(s_ten);
								
				var s_eleven=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_11',i);
				s_eleven=validateValue(s_eleven);
				
				var s_twelve=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_12',i);
				s_twelve=validateValue(s_twelve);
							
				var s_thirteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_13',i);
				s_thirteen=validateValue(s_thirteen);
								
				var s_fourteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_14',i);
				s_fourteen=validateValue(s_fourteen);
				
				var s_fifteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_15',i);
				s_fifteen=validateValue(s_fifteen);			
			
				var s_sixteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_16',i);
				s_sixteen=validateValue(s_sixteen);	
								
				var s_seventenn=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_17',i);
				s_seventenn=validateValue(s_seventenn);	
				
				var s_eighteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_18',i);
				s_eighteen=validateValue(s_eighteen);
								
				var s_nineteen=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_19',i);
				s_nineteen=validateValue(s_nineteen);
								
				var s_twenty=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_20',i);
				s_twenty=validateValue(s_twenty);

				var s_twentyone=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_21',i);
				s_twentyone=validateValue(s_twentyone);
								
				var s_twentytwo=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_22',i);
				s_twentytwo=validateValue(s_twentytwo);
								
				var s_twentythree=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_23',i);
				s_twentythree=validateValue(s_twentythree);
				
				var s_twentyfour=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_24',i);
				s_twentyfour=validateValue(s_twentyfour);
				
				var s_twentyfive=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_25',i);
				s_twentyfive=validateValue(s_twentyfive);
								
				var s_twentysix=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_26',i);
				s_twentysix=validateValue(s_twentysix);
				
				var s_twentyseven=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_27',i);
				s_twentyseven=validateValue(s_twentyseven);	
							
				var s_twentyeight=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_28',i);
				s_twentyeight=validateValue(s_twentyeight);	
								
				var s_twentynine=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_29',i);
				s_twentynine=validateValue(s_twentynine);	
							
				var s_thirty=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_30',i);
				s_thirty=validateValue(s_thirty);	
						
				var s_thirtyone=recordOBJ.getLineItemValue('recmachcustrecord_mp3_mp1ref','custrecord_mp3_31',i);
			    s_thirtyone=validateValue(s_thirtyone);	


              var s_month_arr=s_month_number+"%%"+s_month+"%%"+s_year;


			    month_arr.push(s_month_arr);
			  
			  
	
		
		if (s_month == 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			janVar=monthwise_schedule(janVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Jan	    
		if (s_month != 'January' && s_month == 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			febVar=monthwise_schedule(febVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Feb	  
		if (s_month != 'January' && s_month != 'February'  && s_month == 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			marVar=monthwise_schedule(marVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Mar	    
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month == 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			aprVar=monthwise_schedule(aprVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//April	   	  
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month == 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			mayVar=monthwise_schedule(mayVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//May	    
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month == 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			junVar=monthwise_schedule(junVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//June	  
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month == 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			julyVar=monthwise_schedule(julyVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//July	    
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month == 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			augVar=monthwise_schedule(augVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//August	   	  	  
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month == 'September'&& s_month != 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			sepVar=monthwise_schedule(sepVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Sep	    
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month == 'October'&& s_month != 'November'&& s_month != 'December') 
		{
					
			octVar=monthwise_schedule(octVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Oct		
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month == 'November'&& s_month != 'December') 
		{
					
			novVar=monthwise_schedule(novVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Nov	    
		if (s_month != 'January' && s_month != 'February'  && s_month != 'March' && s_month != 'April' && s_month != 'May'&& s_month != 'June'&& s_month != 'July'&& s_month != 'August'&& s_month != 'September'&& s_month != 'October'&& s_month != 'November'&& s_month == 'December') 
		{
					
			decVar=monthwise_schedule(decVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
		
		}//Dec		  
			  
			  
			  
			    ms_Array[ms_cnt++] = s_tvProgram + '##' +s_spotQuota + '##' +s_planunit + '##' +s_month + '##' +s_year + '##' +s_one + '##' +s_two + '##' +s_three + '##' +s_four+ '##' +s_five + '##' +s_six + '##' +s_seven + '##' +s_eight + '##' +s_nine + '##' +s_ten+ '##' +s_eleven + '##' +s_twelve + '##' +s_thirteen + '##' +s_fourteen + '##' +s_fifteen + '##' +s_sixteen+ '##' +s_seventenn + '##' +s_eighteen + '##' +s_nineteen + '##' +s_twenty + '##' +s_twentyone + '##' +s_twentytwo+ '##' +s_twentythree + '##' +s_twentyfour + '##' +s_twentyfive + '##' +s_twentysix+ '##' +s_twentyseven + '##' +s_twentyeight + '##' +s_twentynine + '##' +s_thirty+ '##' +s_thirtyone+'##'+media_type_tv;
			
			}
			
			
		}
	
	     var i_linecount_medialist=recordOBJ.getLineItemCount('recmachcustrecord_mp2_mp1ref');
		 nlapiLogExecution('DEBUG','suiteletFunction','  Line Count Media List [ Media Plan ]--->'+i_linecount_medialist);
	   
	     if(i_linecount_medialist!=null)
		 {
		 	for(j=1;j<=i_linecount_medialist;j++)
			{
				var i_alias=recordOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_alias',j);
				i_alias=validateValue(i_alias);	
				nlapiLogExecution('DEBUG','suiteletFunction','  Alias [ Media Plan ]--->'+i_alias);
				
				var i_productname=recordOBJ.getLineItemText('recmachcustrecord_mp2_mp1ref','custrecord_mp2_productname',j);
				i_productname=validateValue(i_productname);
				nlapiLogExecution('DEBUG','suiteletFunction','  Product Name [ Media Plan ]--->'+i_productname);
				
				var i_oldduration=recordOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration_old',j);
				i_oldduration=validateValue(i_oldduration);
				nlapiLogExecution('DEBUG','suiteletFunction','  Old Duration [ Media Plan ]--->'+i_oldduration);
				
				var i_newduration=recordOBJ.getLineItemValue('recmachcustrecord_mp2_mp1ref','custrecord_mp2_duration',j);
				i_newduration=validateValue(i_newduration);
				nlapiLogExecution('DEBUG','suiteletFunction','  New Duration [ Media Plan ]--->'+i_newduration);
			
			
			     var i_type=recordOBJ.getLineItemText('recmachcustrecord_mp2_mp1ref','custrecord_mp2_type',j);
				i_type=validateValue(i_type);	
				nlapiLogExecution('DEBUG','suiteletFunction','  Type [ Media Plan ]--->'+i_type);
			   
			    ml_Array[ml_cnt++] = i_alias + '##' +i_productname + '##' +i_oldduration + '##' +i_newduration + '##' +i_type;
			}
		 }
	
	
	var o_salesorder_OBJ;
	
	   if(i_salesorder_id!=null && i_salesorder_id!='')
	   {
	   	  o_salesorder_OBJ=nlapiLoadRecord('salesorder',i_salesorder_id);
		  nlapiLogExecution('DEBUG','suiteletFunction','  Sales Order Object [ Sales Order ]--->'+o_salesorder_OBJ);
	   }
	
	   var s_product='';
	   var s_termsOfPayment;
	   var s_remark='';
	    
	    if(o_salesorder_OBJ!=null)
		{
			s_product=o_salesorder_OBJ.getFieldValue('custbody_product');
		    s_product=validateValue(s_product);
			nlapiLogExecution('DEBUG','suiteletFunction','  Product [ Sales Order ]--->'+s_product);
			
			s_terms=o_salesorder_OBJ.getFieldText('terms');
		     s_terms=validateValue(s_terms);
			nlapiLogExecution('DEBUG','suiteletFunction','  Terms [ Sales Order ]--->'+s_terms);
			
			s_tv_credit=o_salesorder_OBJ.getFieldValue('custbody12');
		    s_tv_credit=validateValue(s_tv_credit);
			nlapiLogExecution('DEBUG','suiteletFunction','  TV Quotation Credit Cards Add [ Sales Order ]--->'+s_tv_credit);
		   
		    s_remark=o_salesorder_OBJ.getFieldValue('custbody11');
		    s_remark=validateValue(s_remark);
			nlapiLogExecution('DEBUG','suiteletFunction','  Remarks [ Sales Order ]--->'+s_remark);
		 
		    s_termsOfPayment=s_terms+' '+s_tv_credit;
			nlapiLogExecution('DEBUG','suiteletFunction','  Terms Of Payment [ Sales Order ]--->'+s_termsOfPayment);
		    
		}
	   
	
	}
	
	
	
     AppendixCPrintLayout(recordID,s_advertiser,s_advertiser_address,s_salesrep,s_media_plan,d_issueDate,s_product,s_remark,s_termsOfPayment,ml_Array,ms_Array,ms_Array_feb,ms_Array_mar, janVar,febVar,marVar,aprVar, mayVar,junVar, julyVar,augVar,sepVar,octVar,novVar,decVar,month_arr); 


}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

function AppendixCPrintLayout(recordID,s_advertiser,s_advertiser_address,s_salesrep,s_media_plan,d_issueDate,s_product,s_remark,s_termsOfPayment,ml_Array,ms_Array,ms_Array_feb,ms_Array_mar, janVar,febVar,marVar,aprVar, mayVar,junVar, julyVar,augVar,sepVar,octVar,novVar,decVar,month_arr)
{
    var a_tv_prog ;
    var a_quota ;
	 var a_planunit ;
    var a_month;
    var a_year ;
	var a_one ;
    var a_two ;
    var a_three ;
    var a_four ;
	var a_five ;
    var a_six ;
    var a_seven;
    var a_eight ;
	var a_nine;
    var a_ten ;
    var a_eleven;
    var a_twelve;
	var a_thirteen ;
    var a_fourteen;
    var a_fifteen ;
    var a_sixteen ;
	var a_seventeen ;
    var a_eightteen ;
    var a_nineteen ;
    var a_twenty ;
	var a_twentyone ;
    var a_twentytwo;
    var a_twentythree ;
    var a_twentyfour ;
	var a_twentyfive ;
    var a_twentysix;
    var a_twentyseven ;
    var a_twentyeight;
	var a_twentynine ;
    var a_thirty ;
    var a_thirtyone;
    var a_tv_type; 


    var a_alias ;
 	var a_productname;
 	var a_oldduration;
 	var a_newduration ;


var strVar="";
strVar += "<table border=\"1\" width=\"100%\">";


strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"3\"><\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"3\"><\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">";
strVar += "<table border=\"0\" width=\"100%\">";


strVar += "	<tr>";


var headeImageResult='https://system.sandbox.netsuite.com/core/media/media.nl?id=253&c=1269103&h=195b16f99bcbac09dd6a';

var headImage=checkURL(headeImageResult)
//nlapiLogExecution('DEBUG','suiteletFunction','headImage ===='+headImage);

if(headImage==false)
{
	headeImageResult='';
}

if (headeImageResult != null && headeImageResult != '' && headeImageResult != 'undefined' &&  headeImageResult != ' ' &&  headeImageResult != '&nbsp') 
{
 headeImageResult = nlapiEscapeXML(headeImageResult);
}

if (headeImageResult != '' && headeImageResult != null && headeImageResult != '' && headeImageResult != 'undefined' &&  headeImageResult != ' ' &&  headeImageResult != '&nbsp') 
{
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\"  width=\"30%\"><img width=\"100px\" height=\"80px\" src=\""  +headeImageResult+"\"><\/img><\/td>";
}
else
{
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\"  width=\"30%\">"+headeImageResult+"<\/td>";
}
strVar += "		<td  font-size=\"18\" font-family=\"Helvetica\" align=\"center\" width=\"40%\"><b>Media Plan<\/b><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" width=\"30%\"><\/td>";
strVar += "	<\/tr>";



strVar += "			<\/table>";

strVar += "	<\/td>";
strVar += "	<\/tr>";





/*

strVar += "	<tr>";



var headeImageResult='https://system.sandbox.netsuite.com/core/media/media.nl?id=253&c=1269103&h=195b16f99bcbac09dd6a';

var headImage=checkURL(headeImageResult)
//nlapiLogExecution('DEBUG','suiteletFunction','headImage ===='+headImage);

if(headImage==false)
{
	headeImageResult='';
}

if (headeImageResult != null && headeImageResult != '' && headeImageResult != 'undefined' &&  headeImageResult != ' ' &&  headeImageResult != '&nbsp') 
{
 headeImageResult = nlapiEscapeXML(headeImageResult);
}


if (headeImageResult != '' && headeImageResult != null && headeImageResult != '' && headeImageResult != 'undefined' &&  headeImageResult != ' ' &&  headeImageResult != '&nbsp') 
{
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"center\" colspan=\"4\"><img width=\"100px\" height=\"80px\" src=\""  +headeImageResult+"\"><\/img><\/td>";
}
else
{
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"center\" colspan=\"4\">"+headeImageResult+"<\/td>";
}

strVar += "		<td  font-size=\"18\" font-family=\"Helvetica\" align=\"center\" colspan=\"4\"><b>Media Plan<\/b><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"4\"><\/td>";
strVar += "	<\/tr>";

*/






strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"3\">Doc No<\/td>";
strVar += "	<\/tr>";

strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\"><\/td>";
strVar += "		<td  font-size=\"12\" font-family=\"Helvetica\" align=\"center\" colspan=\"7\"><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"3\">Date : "+d_issueDate+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"3\">Created By: "+s_salesrep+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"center\" colspan=\"3\"><b>Customer Name<\/b><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"left\" colspan=\"9\">"+s_advertiser+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\"  colspan=\"5\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"center\" colspan=\"3\"><b>Customer Address<\/b><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"left\" colspan=\"9\">"+s_advertiser_address+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"5\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"7\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"center\" colspan=\"3\"><b>Product<\/b><\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" align=\"left\" colspan=\"9\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\">";
strVar += "		<table border=\"0\" width=\"100%\">";


strVar += "			<tr>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" ><\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"4\" >";




strVar += "		<table border=\"1\" width=\"100%\">";


var split_ml_array=new Array();


 if(ml_Array[0]!=null && ml_Array[0]!='')
 {
 	split_ml_array = ml_Array[0].split('##');
 	
 	
 	a_alias = split_ml_array[0];
 	a_productname = split_ml_array[1];
 	a_oldduration = split_ml_array[2];
 	a_newduration = split_ml_array[3];
	a_type = split_ml_array[4];
 	
 	
strVar += "			<tr>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >Spot&nbps;Name&nbps;:<\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >"+a_alias+"<\/td>";
var appendType;

	if(a_type=='Spot')
	{
		if(a_newduration!='')
		{
			appendType="( Spot"+" "+a_newduration+" "+" seconds )";
		}
		else
		{
			appendType="( Spot )";
		}
	}
	else if(a_type=='VTR')
	{
		if(a_newduration!='')
		{
			appendType="( VTR"+" "+a_newduration+" "+" months )";
		}
		else
		{
			appendType="( VTR )";
		}
		
	}
	else if(a_type=='Tie In')
	{
		if(a_newduration!='')
		{
			appendType="( Tie In"+" "+a_newduration+" "+" times )";
		}
		else
		{
			appendType="( Tie In )";
		}
		
	}
	else if(a_type=='PR/Scoop')
	{
		if(a_newduration!='')
		{
			appendType="( PR/Scoop"+" "+a_newduration+" "+" times )";
		}
		else
		{
			appendType="( PR/Scoop )";
		}
		
	}
	

	

strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\" >"+a_productname+" "+appendType+"<\/td>";
strVar += "			<\/tr>";
 	
 }

var split_ml_array=new Array();
 for (var q = 1; q <ml_Array.length; q++)
 {
 	
	
		split_ml_array = ml_Array[q].split('##');
		a_alias = split_ml_array[0];
	 	a_productname = split_ml_array[1];
	 	a_oldduration = split_ml_array[2];
	 	a_newduration = split_ml_array[3];
		a_type = split_ml_array[4];

 	
 	 	
 	
 	
 	
strVar += "			<tr>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" ><\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >"+a_alias+"<\/td>";
var appendType;


	if(a_type=='Spot')
	{
		if(a_newduration!='')
		{
			appendType="( Spot"+" "+a_newduration+" "+" seconds )";
		}
		else
		{
			appendType="( Spot )";
		}
	}
	else if(a_type=='VTR')
	{
		if(a_newduration!='')
		{
			appendType="( VTR"+" "+a_newduration+" "+" months )";
		}
		else
		{
			appendType="( VTR )";
		}
		
	}
	else if(a_type=='Tie In')
	{
		if(a_newduration!='')
		{
			appendType="( Tie In"+" "+a_newduration+" "+" times )";
		}
		else
		{
			appendType="( Tie In )";
		}
		
	}
	else if(a_type=='PR/Scoop')
	{
		if(a_newduration!='')
		{
			appendType="( PR/Scoop"+" "+a_newduration+" "+" times )";
		}
		else
		{
			appendType="( PR/Scoop )";
		}
		
	}
	




strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\" >"+a_productname+" "+appendType+"<\/td>";
strVar += "			<\/tr>";
 	
 }


strVar += "			<\/table>";
strVar += "<\/td>";


strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\"  ><\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"5\" >"

strVar += "		<table border=\"1\" width=\"100%\">";

var split_ms_array=new Array();
if(ms_Array[0]!=null && ms_Array[0]!='')
 {
 	split_ms_array = ms_Array[0].split('##');
 	
 	
 	a_tv_prog = split_ms_array[0];
 	a_quota = split_ms_array[1];
 	a_planunit == split_ms_array[2];
 	a_month = split_ms_array[3];
 	a_year = split_ms_array[4];
 	a_one = split_ms_array[5];
 	a_two = split_ms_array[6];
 	a_three = split_ms_array[7];
 	a_four = split_ms_array[8];
 	a_five = split_ms_array[9];
 	a_six = split_ms_array[10];
 	a_seven = split_ms_array[11];
 	a_eight = split_ms_array[12];
 	a_nine = split_ms_array[13];
 	a_ten = split_ms_array[14];
 	a_eleven = split_ms_array[15];
 	a_twelve = split_ms_array[16];
 	a_thirteen = split_ms_array[17];
 	a_fourteen = split_ms_array[18];
 	a_fifteen = split_ms_array[19];
 	a_sixteen = split_ms_array[20];
 	a_seventeen = split_ms_array[21];
 	a_eightteen = split_ms_array[22];
 	a_nineteen = split_ms_array[23];
 	a_twenty = split_ms_array[24];
 	a_twentyone = split_ms_array[25];
 	a_twentytwo = split_ms_array[26];
 	a_twentythree = split_ms_array[27];
 	a_twentyfour = split_ms_array[28];
 	a_twentyfive = split_ms_array[29];
 	a_twentysix = split_ms_array[30];
 	a_twentyseven = split_ms_array[31];
 	a_twentyeight = split_ms_array[32];
 	a_twentynine = split_ms_array[33];
 	a_thirty = split_ms_array[34];
 	a_thirtyone = split_ms_array[35];
 	a_tv_type = split_ms_array[36];
 	
strVar += "			<tr>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >Summary&nbps;Spot&nbps;:<\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >"+1+"<\/td>";

var appendTotal;


	if(a_tv_type=='Spot')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'minutes'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
		
	}
	else if(a_tv_type=='VTR')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}
	else if(a_tv_type=='Tie In')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}
	else if(a_tv_type=='PR/Scoop')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}
	

   

strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\" >"+a_tv_prog+""+appendTotal+"<\/td>";
strVar += "			<\/tr>";
 	
 	
 }


var split_ms_array=new Array();
 for (var y = 1; y<ms_Array.length; y++)
 {
 	
	split_ms_array = ms_Array[y].split('##');
  	
 	a_tv_prog = split_ms_array[0];
 	a_quota = split_ms_array[1];
 	a_planunit == split_ms_array[2];
 	a_month = split_ms_array[3];
 	a_year = split_ms_array[4];
 	a_one = split_ms_array[5];
 	a_two = split_ms_array[6];
 	a_three = split_ms_array[7];
 	a_four = split_ms_array[8];
 	a_five = split_ms_array[9];
 	a_six = split_ms_array[10];
 	a_seven = split_ms_array[11];
 	a_eight = split_ms_array[12];
 	a_nine = split_ms_array[13];
 	a_ten = split_ms_array[14];
 	a_eleven = split_ms_array[15];
 	a_twelve = split_ms_array[16];
 	a_thirteen = split_ms_array[17];
 	a_fourteen = split_ms_array[18];
 	a_fifteen = split_ms_array[19];
 	a_sixteen = split_ms_array[20];
 	a_seventeen = split_ms_array[21];
 	a_eightteen = split_ms_array[22];
 	a_nineteen = split_ms_array[23];
 	a_twenty = split_ms_array[24];
 	a_twentyone = split_ms_array[25];
 	a_twentytwo = split_ms_array[26];
 	a_twentythree = split_ms_array[27];
 	a_twentyfour = split_ms_array[28];
 	a_twentyfive = split_ms_array[29];
 	a_twentysix = split_ms_array[30];
 	a_twentyseven = split_ms_array[31];
 	a_twentyeight = split_ms_array[32];
 	a_twentynine = split_ms_array[33];
 	a_thirty = split_ms_array[34];
 	a_thirtyone = split_ms_array[35];
	a_tv_type = split_ms_array[36];
	
strVar += "			<tr>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" ><\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" >"+(y+1)+"<\/td>";

var appendTotal;


	if(a_tv_type=='Spot')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'minutes'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
		
	}
	else if(a_tv_type=='VTR')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}
	else if(a_tv_type=='Tie In')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}
	else if(a_tv_type=='PR/Scoop')
	{
		if(a_quota!='')
		{
			appendTotal=" "+' total'+" "+a_quota+" "+'times'+" "+a_month+" "+a_year;
		}
		else
		{
		  appendTotal=" ";	
		}
	}


strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\" >"+a_tv_prog+""+appendTotal+"<\/td>";

strVar += "			<\/tr>";
 	
 }
strVar += "			<\/table>";




strVar += "	<\/td>";
strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" ><\/td>";

strVar += "			<\/tr>";


strVar += "			<\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\" width=\"18%\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"1\">&nbsp;<\/td>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">&nbsp;<\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\"  align=\"center\" colspan=\"3\"><b>Budget<\/b><\/td>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\"  align=\"left\" colspan=\"9\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";


strVar += "	<tr>";
strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\">&nbsp;<\/td>";
strVar += "	<\/tr>";



var month_length
month_arr=month_arr.sort();
nlapiLogExecution('DEBUG', 'getItemName', ' SOrted Month Array--> ' +month_arr);

var remove_dup=removearrayduplicate(month_arr);
nlapiLogExecution('DEBUG', 'getItemName', ' Remove Duplicates = ' +remove_dup);

if(remove_dup!=null && remove_dup!='')
{
	 month_length=remove_dup.length;
	 
}
else
{
 	month_length=0;
}
nlapiLogExecution('DEBUG', 'getItemName', ' month_length month_length = ' +month_length);

for (var mth = 0; mth < month_length; mth++) 
{
	var split_month_array = new Array();
//	for (var g = 1; g < month_length; g++) 
	{
	
		split_month_array = remove_dup[mth].split('%%');
		
		var index_mon = split_month_array[0];
		var index_month_text = split_month_array[1];
		var index_year = split_month_array[2];
		
		
		
		strVar += "	<tr>";
		strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\">";
		
		
		//=================================================================================================
		
		strVar += "		<table border=\"0\" width=\"100%\">";
		
		strVar += "	<tr>";
		strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" colspan=\"12\"><b>Period :&nbsp;&nbsp;&nbsp; " + index_month_text + "" + index_year + " " + "<\/b><\/td>";
		strVar += "	<\/tr>";
		
		
		
		
		strVar += "			<tr>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" rowspan=\"2\">&nbsp;<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-left=\"0.2\" border-top=\"0.2\" border-right=\"0.2\" rowspan=\"2\">TV&nbps;Program&nbps;<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" rowspan=\"2\"  border-top=\"0.2\" border-right=\"0.2\" >Spot&nbps;Qty<p>&nbps;Quota&nbps;<\/p>";
		strVar += "				<p>&nbps;(Minute)&nbps;<\/p><\/td>";
		
        strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">1<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">2<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">3<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">4<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">5<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">6<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">7<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">8<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">9<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">10<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">11<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">12<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">13<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">14<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">15<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">16<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">17<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">18<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">19<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">20<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">21<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">22<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">23<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">24<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">25<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">26<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">27<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">28<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">29<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\" >30<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\" >31<\/td>";

		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\">&nbsp;<\/td>";
		strVar += "			<\/tr>";
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	/*
	
		strVar += "			<tr>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" rowspan=\"2\">&nbsp;<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-left=\"0.2\" border-top=\"0.2\" border-right=\"0.2\" rowspan=\"2\">TV&nbps;Program&nbps;<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" rowspan=\"2\"  border-top=\"0.2\" border-right=\"0.2\" >Spot&nbps;Qty<p>&nbps;Quota&nbps;<\/p>";
		strVar += "				<p>&nbps;(Minute)&nbps;<\/p><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Fri<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sun<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Mon<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Tue<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Wed<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Thu<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Fri<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sun<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Mon<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Tue<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Wed<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Thu<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Fri<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sun<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Mon<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Tue<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Wed<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Thu<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Fri<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Sun<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Mon<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Tue<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Wed<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Thu<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\">Fri<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\" width=\"4\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  border-top=\"0.2\" border-right=\"0.2\" width=\"4\">Sat<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\">&nbsp;<\/td>";
		strVar += "			<\/tr>";
		
*/
		
		
		
		
		/*
strVar += "			<tr>";
				
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">1<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">2<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">3<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">4<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">5<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">6<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">7<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">8<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">9<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">10<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">11<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">12<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">13<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">14<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">15<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">16<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">17<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">18<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">19<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">20<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">21<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">22<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">23<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">24<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">25<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">26<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">27<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">28<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\">29<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\" width=\"4\">30<\/td>";
	    strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-top=\"0.2\" border-right=\"0.2\" width=\"4\">31<\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  >&nbsp;<\/td>";
		strVar += "			<\/tr>";
		
*/
	
	
	strVar += "			<tr>";
				
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\" width=\"4\"><\/td>";
	    strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\"  border-right=\"0.2\" width=\"4\"><\/td>";
		strVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\"  >&nbsp;<\/td>";
		strVar += "			<\/tr>";
		
		if (index_month_text == 'January') 
		{
			strVar += janVar;
		}
		else 
			if (index_month_text == 'February')
			 {
				strVar += febVar;
			}
			else 
				if (index_month_text== 'March') 
				{
					strVar += marVar;
				}
				else 
					if (index_month_text == 'April')
					 {
						strVar += aprVar;
					}
					else 
						if (index_month_text == 'May') 
						{
							 nlapiLogExecution('DEBUG', 'suiteletFunction', ' May Before: ' );
							strVar += mayVar;
							 nlapiLogExecution('DEBUG', 'suiteletFunction', ' May After: ' );
						}
						else 
							if (index_month_text == 'June') {
								strVar += junVar;
							}
							else 
								if (index_month_text == 'July') {
									strVar += julyVar;
								}
								else 
									if (index_month_text == 'August') {
										strVar += augVar;
									}
									else 
										if (index_month_text == 'September') {
											strVar += sepVar;
										}
										else 
											if (index_month_text == 'October') {
												strVar += octVar;
											}
											else 
												if (index_month_text == 'November') {
													strVar += novVar;
												}
												else 
													if (index_month_text == 'December') {
														strVar += decVar;
													}
		strVar += "			<tr>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td width=\"4\">&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		strVar += "				<td>&nbsp;<\/td>";
		
		strVar += "			<\/tr>";
		
		
		strVar += "			<\/table>";
		
		strVar += "		<\/td>";
		strVar += "	<\/tr>";
		
	}
}
	//=================================================================================================
	
	
	strVar += "	<tr>";
	strVar += "		<td width=\"1111\" colspan=\"12\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\"  align=\"right\" colspan=\"3\"><b>Term Of Payment<\/b> <\/td>";
	strVar += "		<td font-size=\"8\"  align=\"left\"  colspan=\"9\">" + (s_termsOfPayment) + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"556\" colspan=\"6\">&nbsp;<\/td>";
	strVar += "		<td width=\"555\" colspan=\"6\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\"  align=\"right\" colspan=\"3\"><b>Remark<\/b><\/td>";
	strVar += "		<td font-size=\"8\"  align=\"left\" colspan=\"9\">" + (s_remark) + "<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"1111\" colspan=\"12\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"371\" colspan=\"4\" height=\"24\" align=\"center\">_____________________ <\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" colspan=\"4\" height=\"24\" align=\"center\"> _____________________	<\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" colspan=\"4\" height=\"24\" align=\"center\">_____________________	<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"4\" align=\"center\" height=\"24\">" + s_salesrep + "<\/td>";
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"4\" align=\"center\" height=\"24\">";
	strVar += "		<p>	Account Executive<\/p><\/td>";
	strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"4\" align=\"center\" height=\"24\">	Marketing &amp; Sales Director<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"371\" align=\"center\" colspan=\"4\">&nbsp;<\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" align=\"center\" colspan=\"4\">BEC-TERO TV Program<\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" align=\"center\" colspan=\"4\"> BEC-TERO TV Program<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"371\" align=\"center\" colspan=\"4\">&nbsp;<\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" align=\"center\" colspan=\"4\">&nbsp;<\/td>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"370\" align=\"center\" colspan=\"4\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td  colspan=\"12\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	
	
	var headeImageResult1 = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=254&c=1269103&h=ea1e9235e6299eb5bc1d';
	
	var headImage1 = checkURL(headeImageResult1)
	//nlapiLogExecution('DEBUG','suiteletFunction','headImage1 ===='+headImage1);
	
	if (headImage1 == false) 
	{
		headeImageResult1 = '';
	}
	
	if (headeImageResult1 != null && headeImageResult1 != '' && headeImageResult1 != 'undefined' && headeImageResult1 != ' ' && headeImageResult1 != '&nbsp') {
		headeImageResult1 = nlapiEscapeXML(headeImageResult1);
	}
	
	
	var headeImageResult2 = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=255&c=1269103&h=3b30a07b14359ac2710a';
	
	var headImage2 = checkURL(headeImageResult2)
	//nlapiLogExecution('DEBUG','suiteletFunction','headImage2 ===='+headImage2);
	
	if (headImage2 == false)
	{
		headeImageResult2 = '';
	}
	
	if (headeImageResult2 != null && headeImageResult2 != '' && headeImageResult2 != 'undefined' && headeImageResult2 != ' ' && headeImageResult2 != '&nbsp') {
		headeImageResult2 = nlapiEscapeXML(headeImageResult2);
	}
	
	
	strVar += "	<tr>";
	
	if (headeImageResult1 != '' && headeImageResult1 != null && headeImageResult1 != '' && headeImageResult1 != 'undefined' && headeImageResult1 != ' ' && headeImageResult1 != '&nbsp') {
		strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\"><img width=\"80px\" height=\"60px\" src=\"" + headeImageResult1 + "\"><\/img><\/td>";
		strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"8\"><\/td>";
		strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\"><img width=\"80px\" height=\"60px\" src=\"" + headeImageResult2 + "\"><\/img><\/td>";
	}
	else {
		strVar += "		<td  font-size=\"8\" font-family=\"Helvetica\" colspan=\"2\">" + headeImageResult1 + "<\/td>";
	}
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	var addr1 = 'BEC-TERO ENTERTAINMENT PUBLIC COMPANY LIMITED';
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\" align=\"center\"><b>" + addr1 + "<\/b><\/td>";
	
	strVar += "	<\/tr>";
	
	var addr2 = '3199 MALEENONT TOWER 25th - 28th FL., RAMA 4 RD., KLONGTON, KLONGTOEY, BANGKOK 10110, THAILAND';
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\" align=\"center\">" + nlapiEscapeXML(addr2) + "<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	var addr3 = 'TEL. (662) 262-3800 FAX. (662)262-3801-2 WWW.BECTERO.COM';
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\" align=\"center\">	" + nlapiEscapeXML(addr3) + "<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"70%\" colspan=\"12\" align=\"center\">A SUBSIDIARY OF BEC WORLD PUBLIC COMPANY ";
	strVar += "		LIMITED<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" colspan=\"12\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	
	strVar += "	<\/table>";
	
	nlapiLogExecution('DEBUG', 'printPDF', ' After HTML');
	
	
	// End of generation HTML Format For PDF Required=================================================================
	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
	xml += "<pdf>\n<body>";
	xml += strVar;
	
	xml += "</body>\n</pdf>";
	// run the BFO library to convert the xml document to a PDF 
	var file = nlapiXMLToPDF(xml);
	nlapiLogExecution('DEBUG', 'printPDF', 'File ==' + file);
	
	
	var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	var fileObj = nlapiCreateFile('Appendix C.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
	
	//Create PDF File with TimeStamp in File Name
	//var fileObj = nlapiCreateFile('AppendixC.pdf', 'PDF', file.getValue());
	nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj=' + fileObj);
	fileObj.setFolder(219);// Folder PDF File is to be stored
	var fileId = nlapiSubmitFile(fileObj);
	nlapiLogExecution('DEBUG', 'printPDF', '*****===PDF fileId  ==' + fileId);
	// Run the BFO library to convert the xml document to a PDF 
	// Set content type, file name, and content-disposition (inline means display in browser)
	response.setContentType('PDF', 'Appendix C.pdf', 'inline');
	// Write response to the client
	response.write(file.getValue());
	
	
	
	
	
		
		
		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		

	
	
	
	
	
	
	
	
	
	
	
	
}//End For

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================








function checkURL(value) 
{
    var itemImageURL = new RegExp("^(http:\/\/system.|https:\/\/system.|ftp:\/\/system.|system.){1}([0-9A-Za-z]+\.)");
	//nlapiLogExecution('DEBUG', 'getItemName', 'value = ' +value);
	//nlapiLogExecution('DEBUG', 'getItemName', 'After reg exp = ' +itemImageURL.test(value));
    if (itemImageURL.test(value))
	{
        return (true);
		
    }
    return (false);
}



function validateValue(value)
{
	if(value==null || value ==undefined || value=='')
	{
		value=''
	}
	return value;
}



function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
      {  
     for(var j=0; j<array.length;j++ )
       {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}


function monthwise_schedule(monthVar,s_tvProgram,s_spotQuota,s_planunit,s_month,s_year,s_one,s_two,s_three,s_four,s_five,s_six,s_seven,s_eight,s_nine,s_ten,s_eleven	,s_twelve,s_thirteen,s_fourteen,s_fifteen,s_sixteen	,s_seventenn,s_eighteen,s_nineteen,s_twenty	,s_twentyone,s_twentytwo,s_twentythree,s_twentyfour,s_twentyfive,s_twentysix,s_twentyseven	,s_twentyeight	,s_twentynine	,s_thirty  ,s_thirtyone)
{
	monthVar += "			<tr>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0\" border-right=\"0.2\"><\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_tvProgram + "&nbps;<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_spotQuota + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_one + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_two + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_three + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_four + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_five + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_six + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_seven + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_eight + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_nine + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_ten + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_eleven + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twelve + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_thirteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_fourteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_fifteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_sixteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_seventenn + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_eighteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_nineteen + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twenty + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentyone + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentytwo + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentythree + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentyfour + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentyfive + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentysix + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentyseven + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentyeight + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_twentynine + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_thirty + "<\/td>";
	monthVar += "				<td align=\"center\" font-size=\"8\" font-family=\"Helvetica\" border-bottom=\"0.2\" border-right=\"0.2\">" + s_thirtyone + "<\/td>";
	monthVar += "				<td>&nbsp;<\/td>";
	monthVar += "			<\/tr>";
	
		
	return monthVar;
}
