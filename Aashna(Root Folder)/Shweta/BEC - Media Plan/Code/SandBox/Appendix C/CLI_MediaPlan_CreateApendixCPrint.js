function openAppendixC()
{
  /*
   * Function Description : Get the Record ID of Media Plan & open window of Back End Suitelet with retrived Record ID.  
   * Variable Description : currentMPID  --> Get the record ID for Media Plan Record , on which Appendix C Print Button is pressed.
   */	
   
	var currentMPID = nlapiGetRecordId();
	
	window.open('/app/site/hosting/scriptlet.nl?script=108&deploy=1&CurrentRecordID='+currentMPID);
	
	
	
	
}