// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: 
	Author:      
	Company:    
	Date:       
	Version:    

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	
		           
	var customer_arr=new Array();
						var product_arr=new Array();
						var prod_cust=new Array();
						var cust_prod=new Array();
						var pcd=0;
	
	var ms_year_txt;
var duration_one;
var duration_two;
var media_type_tv;



var duration_two_alias;
var duration_one_alias;

var pd=0;

var duration_one_alias;
var duration_two_alias;
var duration_three_alias;
var duration_four_alias;
var duration_five_alias;
var duration_six_alias;
var duration_seven_alias;
var duration_eight_alias;
var duration_nine_alias;
var duration_ten_alias;
var duration_eleven_alias;
var duration_twelve_alias;
var duration_thirteen_alias;
var duration_fourteen_alias;
var duration_fifteen_alias;
var duration_sixteen_alias;
var duration_seventeen_alias;
var duration_eightteen_alias;
var duration_ninteen_alias;	
var duration_twenty_alias;
var duration_twentyone_alias;
var duration_twentytwo_alias;
var duration_twentythree_alias;
var duration_twentyfour_alias;
var duration_twentyfive_alias;
var duration_twentysix_alias;
var duration_twentyseven_alias;
var duration_twentyeight_alias;
var duration_twentynine_alias;
var duration_thirty_alias;
var duration_thirtyone_alias;

					
					
var one_rate;
var two_rate;
three_rate



var  one_rate;
var  two_rate;
var  three_rate;
var  four_rate;
var  five_rate;
var  six_rate;
var  seven_rate;
var  eight_rate;
var  nine_rate;
var  ten_rate;
var  eleven_rate;
var  twelve_rate;
var  thirteen_rate;
var  fourteen_rate;
var  fifteen_rate;
var  sixteen_rate;
var  seventeen_rate;
var  eightteen_rate;
var  ninteen_rate;	
var  twenty_rate;
var  twentyone_rate;
var  twentytwo_rate;
var  twentythree_rate;
var  twentyfour_rate;
var  twentyfive_rate;
var  twentysix_rate;
var  twentyseven_rate;
var  twentyeight_rate;
var  twentynine_rate;
var  thirty_rate;
var  thirtyone_rate;

 var SalesREPText;
 var EndCustomerNameText;
  var CustomerNameText;
  var ProductText;
var array_customer=new Array();
				var arr_cnt=0;
		  	


	//  SCHEDULED FUNCTION CODE BODY
	
	
	try
	{
	  
	   var context = nlapiGetContext();
	   var remainingUsage=context.getRemainingUsage();
	 //  nlapiLogExecution('DEBUG', 'stockReconciliationPDF', " Remaining Usage : " + remainingUsage);
	
	   var SalesRep=context.getSetting('SCRIPT','custscript_sales_rep');
	   var Year=context.getSetting('SCRIPT','custscript_year');
	  
	    
	  
	  var filter = new Array();
	  filter[0] = new nlobjSearchFilter('custrecord_mp1_salesrep', null, 'is', SalesRep);
	 
	  var columns = new Array();
	  columns[0] = new nlobjSearchColumn('internalid');
	  
	  var  searchresults = nlapiSearchRecord('customrecord_mediaplan_1','customsearch600', filter, null);
	 
	  
		  for (var i = 0; searchresults != null && i < searchresults.length; i++) 	
		  {
		  	
			        var jan_total=0 ;
					var feb_total=0 ;
					var mar_total=0 ;
					var apr_total=0 ;
					var may_total =0;
					var jun_total=0 ;
					var july_total=0 ;
					var aug_total=0 ;
					var sep_total=0 ;
					var oct_total=0 ;
					var nov_total =0;
					var dec_total =0;
			
			
			
			  nlapiLogExecution('DEBUG','In Suitelet Post Method','seasearchresultsrch =='+searchresults.length)  
				
				var SearchMPResult = searchresults[i];
                var columns = SearchMPResult.getAllColumns();
                var columnLen = columns.length;
              //  nlapiLogExecution('DEBUG', '126', 'i:' + i);
				
				var column1 = columns[0];
                var SalesREpID = SearchMPResult.getValue(column1);
            nlapiLogExecution('DEBUG', '126', 'SalesREpID:' + SalesREpID);
                //SalesREpID = nlapiEscapeXML(SalesREpID)
				
				
                SalesREPText = SearchMPResult.getText(column1);
                nlapiLogExecution('DEBUG', '126', '*************** SalesREPText *****************' + SalesREPText);
                //SalesREpID = nlapiEscapeXML(SalesREpID)
				
				
				
				
                
                var column2 = columns[1];
                var EndCustomerName = SearchMPResult.getValue(column2);
            //    nlapiLogExecution('DEBUG', '126', 'EndCustomerName:' + EndCustomerName);
                //EndCustomerName = nlapiEscapeXML(EndCustomerName)
				
				
				EndCustomerNameText = SearchMPResult.getText(column2);
                nlapiLogExecution('DEBUG', '126', '***************** EndCustomerNameText*****************' + EndCustomerNameText);
				
               
                var column3 = columns[2];
                var CustomerName = SearchMPResult.getValue(column3);
           //     nlapiLogExecution('DEBUG', '126', 'CustomerName:' + CustomerName);
                //TransactionNumber = nlapiEscapeXML(CustomerName)
				
				
				CustomerNameText = SearchMPResult.getText(column3);
                nlapiLogExecution('DEBUG', '126', 'CustomerNameText:' + CustomerNameText);
				
			    var column5 = columns[3];
                var ProductName = SearchMPResult.getValue(column5);
                nlapiLogExecution('DEBUG', '126', 'ProductName:' + ProductName);
                //ProductName = nlapiEscapeXML(ProductName)
				
				
				ProductText = SearchMPResult.getText(column5);
                nlapiLogExecution('DEBUG', '126', '***************** ProductText*****************' + ProductText);
				
                
                var column6 = columns[4];
                var Alias = SearchMPResult.getValue(column6);
                nlapiLogExecution('DEBUG', '126', 'Alias:' + Alias);
                //Alias = nlapiEscapeXML(Alias)
				
						
				var MPfilters = new Array();
				
				var MPColoumns = new Array();
               
                    
                MPfilters[0] = new nlobjSearchFilter('custrecord_mp1_salesrep', null, 'is', SalesREpID)
				MPfilters[1] = new nlobjSearchFilter('custrecord_mp2_endcustomer', 'custrecord_mp2_mp1ref', 'is', EndCustomerName)
				MPfilters[2] = new nlobjSearchFilter('custrecord_mp1_customer', null, 'is', CustomerName)
				MPfilters[3] = new nlobjSearchFilter('custrecord_mp2_productname', 'custrecord_mp2_mp1ref', 'is', ProductName)
			//  MPfilters[4] = new nlobjSearchFilter('custrecord_mp2_alias', 'custrecord_mp2_mp1ref', 'is', Alias)
				
				
				
			    var MPsearchpaymentresults = nlapiSearchRecord('customrecord_mediaplan_1', 'customsearch602', MPfilters, null);
				
				
				for (var k = 0; MPsearchpaymentresults != null && k < MPsearchpaymentresults.length; k++) 	
				{
				 var total_duration=0;
					nlapiLogExecution('DEBUG', '126', 'MPsearchpaymentresults.length==' + MPsearchpaymentresults.length);
					var MPsearchresult = MPsearchpaymentresults[k];
					var MPColoumn = MPsearchresult.getAllColumns();
					nlapiLogExecution('DEBUG', '126', 'MPColoumn.length==' + MPColoumn.length);
					
					var MPColoumn1 = MPColoumn[0];
					var MediaPlanID = MPsearchresult.getValue(MPColoumn1);
					nlapiLogExecution('DEBUG', '126', 'MediaPlanID==' + MediaPlanID);
					
					
					
					var MPColoumn2 = MPColoumn[6];
					var MediaPlan_Alias = MPsearchresult.getValue(MPColoumn2);
					nlapiLogExecution('DEBUG', '126', 'MediaPlan_Alias==' + MediaPlan_Alias);
									
					
					
					var O_MediaRecord = nlapiLoadRecord('customrecord_mediaplan_1', MediaPlanID)
					nlapiLogExecution('DEBUG', '126', 'Year==' + Year);
				   // Begin Code : For searching the Media schedule for given year & sales Rep  
					
					var MediaSchedulefilter = new Array();
	                
					MediaSchedulefilter[0] = new nlobjSearchFilter('custrecord_mp3_mp1ref', null, 'is', MediaPlanID);
	                MediaSchedulefilter[1] = new nlobjSearchFilter('custrecord_mp3_year', null, 'is', Year);
	  				
					
					var  MediaSchedulesearchresults = nlapiSearchRecord('customrecord_mediaplan_3',605, MediaSchedulefilter,null);
	               
					
					for(var t = 0 ; MediaSchedulesearchresults != null &&t< MediaSchedulesearchresults.length ; t++)
					{	
					
				  nlapiLogExecution('DEBUG','In Suitelet Post Method','MediaSchedulesearchresults =='+MediaSchedulesearchresults.length)
				
						
						
						
						var MediaScheduleResult = MediaSchedulesearchresults[t];
		                var columns = MediaScheduleResult.getAllColumns();
		                var columnLen = columns.length;
		                nlapiLogExecution('DEBUG', '605', 't:' + t);
						
						
						var Coloumn_status= columns[39];
						var ms_status = MediaScheduleResult.getValue(Coloumn_status);
						ms_status=validateValue(ms_status);
		                nlapiLogExecution('DEBUG', '605', 'ms_status:' + ms_status);
						
						var column1 = columns[0];
		                var MediaScheduleId = MediaScheduleResult.getValue(column1);
		               nlapiLogExecution('DEBUG', '605', 'MediaScheduleId:' + MediaScheduleId);
						
						var Coloumn2= columns[1];
						var MediaPlanID = MediaScheduleResult.getValue(Coloumn2);
		                nlapiLogExecution('DEBUG', '605', 'MediaPlanID:' + MediaPlanID);
						
						var media_OBJ;
						var salesorder_id;
						
						if(MediaPlanID!=null)
						{
							media_OBJ = nlapiLoadRecord('customrecord_mediaplan_1', MediaPlanID)
							 //nlapiLogExecution('DEBUG', '126', 'media_OBJ==' + media_OBJ);
								
							
							if(media_OBJ!=null)
							{
								salesorder_id=media_OBJ.getFieldValue('custrecord_mp1_soref');
							    //nlapiLogExecution('DEBUG', '126', 'salesorder_id==' + salesorder_id);
							
													
							}
					
											
						}
						
					    if(ms_status==2)
						{		
															
						
						var Coloumn3= columns[3];
						var tv_program = MediaScheduleResult.getValue(Coloumn3);
		                //nlapiLogExecution('DEBUG', '605', 'tv_program:' + tv_program);
					
				
						var item_rate;
						var item_quantity;
						var sales_result=new Array();
						var sales_cnt=0;
						if(salesorder_id!=null && salesorder_id!='')
						{
							var sales_OBJ=nlapiLoadRecord('salesorder',salesorder_id);
							//nlapiLogExecution('DEBUG','salesorder_rate','sales_OBJ =='+sales_OBJ)
							
							if(sales_OBJ!=null && sales_OBJ!='')
							{
								
								var item_count=sales_OBJ.getLineItemCount('item');
								//nlapiLogExecution('DEBUG','salesorder_rate','item_count =='+item_count);
								
								if(item_count!=null)
								{
									for(var p=1;p<=item_count;p++)
									{
										var item_id=sales_OBJ.getLineItemValue('item','item',p);
										//nlapiLogExecution('DEBUG','salesorder_rate','item_id =='+item_id);
										
										item_rate=sales_OBJ.getLineItemValue('item','rate',p);
										//nlapiLogExecution('DEBUG','salesorder_rate','item_rate =='+item_rate);
										
										
										item_quantity=sales_OBJ.getLineItemValue('item','quantity',p);
										//nlapiLogExecution('DEBUG','salesorder_rate','item_quantity =='+item_quantity);
										
										if(item_rate==tv_program)
										{
											item_rate=item_rate;
											break;
										}
										
										
									}
									
									
									
								}
								
						
			
		}//Sales OBJ
	
		
	}//Sales ID Check
	
				  
				 	//nlapiLogExecution('DEBUG','salesorder_rate','Sales order Item Rate =='+item_rate);
						
						
						
						
						
						if(tv_program!=null && tv_program!=''&& tv_program!=undefined)
						{
							var tv_programOBJ=nlapiLoadRecord('serviceitem',tv_program);
							//nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Object [ Media Schedule ]--->'+tv_programOBJ);
						    
							if(tv_programOBJ!=null)
							{
								media_type_tv=tv_programOBJ.getFieldValue('custitem_bec_mediatype');
								//nlapiLogExecution('DEBUG','suiteletFunction','  TV Program Media Type [ Media Schedule ]-->'+media_type_tv);
								
								
							}
						
						}
												
						var Coloumn4= columns[4];
						var spot_quota = MediaScheduleResult.getValue(Coloumn4);
		                nlapiLogExecution('DEBUG', '605', 'spot_quota:' + spot_quota);
						
						var Coloumn5= columns[6];
						var ms_year = MediaScheduleResult.getValue(Coloumn5);
		               nlapiLogExecution('DEBUG', '605', 'ms_year:' + ms_year);
						
						
						
						 ms_year_txt = MediaScheduleResult.getText(Coloumn5);
		                nlapiLogExecution('DEBUG', '605', 'ms_year_txt:' + ms_year_txt);
						
						var Coloumn6= columns[7];
						var ms_month = MediaScheduleResult.getValue(Coloumn6);
		                nlapiLogExecution('DEBUG', '605', 'ms_month:' + ms_month);
						
						var ms_month_txt = MediaScheduleResult.getText(Coloumn6);
		                nlapiLogExecution('DEBUG', '605', 'ms_month_txt:' + ms_month_txt);
												
						
						var Coloumn7= columns[8];
						var ms_one = MediaScheduleResult.getValue(Coloumn7);
						ms_one=validateValue(ms_one);
		                nlapiLogExecution('DEBUG', '605', 'ms_one:' + ms_one);
						
						if(ms_one.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_one_alias=get_alias(ms_one,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_one_alias:' + duration_one_alias);
							
							
							duration_one=get_aliasduration(ms_one,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_one:' + duration_one);
							  		
						
							
					        if(media_type_tv==1)
							{
								one_rate=spotTotal(item_rate,duration_one,duration_one_alias)
								nlapiLogExecution('DEBUG', '605', 'Spot Rate One :' + one_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								one_rate=other_than_spot_Total(item_rate,duration_one,duration_one_alias)
								nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate One :' + one_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(one_rate);
						    nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
							
											
						}
						
					
						
						var Coloumn8= columns[9];
						var ms_two = MediaScheduleResult.getValue(Coloumn8);
						ms_two=validateValue(ms_two);
		                nlapiLogExecution('DEBUG', '605', 'ms_two:' + ms_two);
						
						if(ms_two.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_two_alias=get_alias(ms_two,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_two_alias:' + duration_two_alias);
							
							duration_two=get_aliasduration(ms_two,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_two:' + duration_two);
							
							
							
							  if(media_type_tv==1)
							{
								two_rate=spotTotal(item_rate,duration_two,duration_two_alias)
								nlapiLogExecution('DEBUG', '605', 'Spot Rate Two :' + two_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								two_rate=other_than_spot_Total(item_rate,duration_two,duration_two_alias)
								nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate Two :' + two_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(two_rate);
						    nlapiLogExecution('DEBUG', '605', 'total_duration Two:' + total_duration);
											
							
						 }//two
						
						
						
						var Coloumn9= columns[10];
						var ms_three = MediaScheduleResult.getValue(Coloumn9);
						ms_three=validateValue(ms_three);
		                nlapiLogExecution('DEBUG', '605', 'ms_three:' + ms_three);
				  
				  
				      if(ms_three.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_three_alias=get_alias(ms_three,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_three_alias:' + duration_three_alias);
							
							duration_three=get_aliasduration(ms_three,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_three:' + duration_three);
							
							
							
							  if(media_type_tv==1)
							{
								three_rate=spotTotal(item_rate,duration_three,duration_three_alias)
								nlapiLogExecution('DEBUG', '605', 'Spot Rate three :' + three_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								three_rate=other_than_spot_Total(item_rate,duration_three,duration_three_alias)
								nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate three :' + three_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(three_rate);
						    nlapiLogExecution('DEBUG', '605', 'total_duration Three:' + total_duration);
											
							
						 }//three
				        
				  
				  
						
						
						var Coloumn10= columns[11];
						var ms_four = MediaScheduleResult.getValue(Coloumn10);
						ms_four=validateValue(ms_four);
		              nlapiLogExecution('DEBUG', '605', 'ms_four:' + ms_four);
						
						
							  
				      if(ms_four.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_four_alias=get_alias(ms_four,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_four_alias:' + duration_four_alias);
							
							duration_four=get_aliasduration(ms_four,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_four:' + duration_four);
							
							
							
							  if(media_type_tv==1)
							{
								four_rate=spotTotal(item_rate,duration_four,duration_four_alias)
								nlapiLogExecution('DEBUG', '605', 'Spot Rate four :' + four_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								four_rate=other_than_spot_Total(item_rate,duration_four,duration_four_alias)
								nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate four :' + four_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(four_rate);
						    nlapiLogExecution('DEBUG', '605', 'total_duration Four:' + total_duration);
											
							
						 }//four
				        
						
						
						var Coloumn11= columns[12];
						var ms_five = MediaScheduleResult.getValue(Coloumn11);
						ms_five=validateValue(ms_five);
		                nlapiLogExecution('DEBUG', '605', 'ms_five:' + ms_five);
						
						    if(ms_five.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_five_alias=get_alias(ms_five,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_five_alias:' + duration_five_alias);
							
							duration_five=get_aliasduration(ms_five,media_type_tv,MediaPlanID,MediaPlan_Alias);
							nlapiLogExecution('DEBUG', '605', 'duration_five:' + duration_five);
							
							
							
							  if(media_type_tv==1)
							{
								five_rate=spotTotal(item_rate,duration_five,duration_five_alias)
								nlapiLogExecution('DEBUG', '605', 'Spot Rate five :' + five_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								five_rate=other_than_spot_Total(item_rate,duration_five,duration_five_alias)
								nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate five :' + five_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(five_rate);
						    nlapiLogExecution('DEBUG', '605', 'total_duration Four:' + total_duration);
											
							
						 }//five
										
						
						
						
						
						var Coloumn12= columns[13];
						var ms_six = MediaScheduleResult.getValue(Coloumn12);
						ms_six=validateValue(ms_six);
		      //          //nlapiLogExecution('DEBUG', '605', 'ms_six:' + ms_six);
						
						 if(ms_six.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_six_alias=get_alias(ms_six,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_six_alias:' + duration_six_alias);
							
							duration_six=get_aliasduration(ms_six,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_six:' + duration_six);
							
							
							
							  if(media_type_tv==1)
							{
								six_rate=spotTotal(item_rate,duration_six,duration_six_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate six :' + six_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								six_rate=other_than_spot_Total(item_rate,duration_six,duration_six_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate six :' + six_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(six_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//six
						
												
						
						var Coloumn13= columns[14];
						var ms_seven = MediaScheduleResult.getValue(Coloumn13);
						ms_seven=validateValue(ms_seven);
		                //nlapiLogExecution('DEBUG', '605', 'ms_seven:' + ms_seven);
						
						 if(ms_seven.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_seven_alias=get_alias(ms_seven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_seven_alias:' + duration_seven_alias);
							
							duration_seven=get_aliasduration(ms_seven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_seven:' + duration_seven);
							
							
							
							  if(media_type_tv==1)
							{
								seven_rate=spotTotal(item_rate,duration_seven,duration_seven_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate seven :' + seven_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								seven_rate=other_than_spot_Total(item_rate,duration_seven,duration_seven_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate seven :' + seven_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(seven_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//seven
						
						
						
						var Coloumn14= columns[15];
						var ms_eight = MediaScheduleResult.getValue(Coloumn14);
						ms_eight=validateValue(ms_eight);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_eight:' + ms_eight);
						
						 if(ms_eight.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_eight_alias=get_alias(ms_eight,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eight_alias:' + duration_eight_alias);
							
							duration_eight=get_aliasduration(ms_eight,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eight:' + duration_eight);
							
							
							
							  if(media_type_tv==1)
							{
								eight_rate=spotTotal(item_rate,duration_eight,duration_eight_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate eight :' + eight_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								eight_rate=other_than_spot_Total(item_rate,duration_eight,duration_eight_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate eight :' + eight_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(eight_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//eight
						
						
						var Coloumn15= columns[16];
						var ms_nine = MediaScheduleResult.getValue(Coloumn15);
						ms_nine=validateValue(ms_nine);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_nine:' + ms_nine);
						
							 if(ms_nine.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_nine_alias=get_alias(ms_nine,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_nine_alias:' + duration_nine_alias);
							
							duration_nine=get_aliasduration(ms_nine,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_nine:' + duration_nine);
							
							
							
							  if(media_type_tv==1)
							{
								nine_rate=spotTotal(item_rate,duration_nine,duration_nine_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate nine :' + nine_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								nine_rate=other_than_spot_Total(item_rate,duration_nine,duration_nine_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate nine :' + nine_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(nine_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//nine
						
						
						var Coloumn16= columns[17];
						var ms_ten = MediaScheduleResult.getValue(Coloumn16);
						ms_ten=validateValue(ms_ten);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_ten:' + ms_ten);
						
						 if(ms_ten.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_ten_alias=get_alias(ms_ten,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_ten_alias:' + duration_ten_alias);
							
							duration_ten=get_aliasduration(ms_ten,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_ten:' + duration_ten);
							
							
							
							  if(media_type_tv==1)
							{
								ten_rate=spotTotal(item_rate,duration_ten,duration_ten_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate ten :' + ten_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								ten_rate=other_than_spot_Total(item_rate,duration_ten,duration_ten_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate ten :' + ten_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(ten_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//ten
						
						
						
						var Coloumn17= columns[18];
						var ms_eleven = MediaScheduleResult.getValue(Coloumn17);
						ms_eleven=validateValue(ms_eleven);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_eleven:' + ms_eleven);
						
						 if(ms_eleven.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_eleven_alias=get_alias(ms_eleven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eleven_alias:' + duration_eleven_alias);
							
							duration_eleven=get_aliasduration(ms_eleven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eleven:' + duration_eleven);
							
							
							
							  if(media_type_tv==1)
							{
								eleven_rate=spotTotal(item_rate,duration_eleven,duration_eleven_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate eleven :' + eleven_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								eleven_rate=other_than_spot_Total(item_rate,duration_eleven,duration_eleven_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate eleven :' + eleven_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(eleven_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//eleven
						
						
						
						
						var Coloumn18= columns[19];
						var ms_twelve = MediaScheduleResult.getValue(Coloumn18);
						ms_twelve=validateValue(ms_twelve);
		          //      //nlapiLogExecution('DEBUG', '605', 'ms_twelve:' + ms_twelve);
						
						
						 if(ms_twelve.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twelve_alias=get_alias(ms_twelve,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twelve_alias:' + duration_twelve_alias);
							
							duration_twelve=get_aliasduration(ms_twelve,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twelve:' + duration_twelve);
							
							
							
							  if(media_type_tv==1)
							{
								twelve_rate=spotTotal(item_rate,duration_twelve,duration_twelve_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twelve :' + twelve_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twelve_rate=other_than_spot_Total(item_rate,duration_twelve,duration_twelve_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twelve :' + twelve_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twelve_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twelve
						
						
						
						
						
						var Coloumn19= columns[20];
						var ms_thirteen = MediaScheduleResult.getValue(Coloumn19);
						ms_thirteen=validateValue(ms_thirteen);
		                //nlapiLogExecution('DEBUG', '605', 'ms_thirteen:' + ms_thirteen);
						
						
						 if(ms_thirteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_thirteen_alias=get_alias(ms_thirteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirteen_alias:' + duration_thirteen_alias);
							
							duration_thirteen=get_aliasduration(ms_thirteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirteen:' + duration_thirteen);
							
							
							
							  if(media_type_tv==1)
							{
								thirteen_rate=spotTotal(item_rate,duration_thirteen,duration_thirteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate thirteen :' + thirteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								thirteen_rate=other_than_spot_Total(item_rate,duration_thirteen,duration_thirteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate thirteen :' + thirteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(thirteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//thirteen
						
						
						
						
						var Coloumn20= columns[21];
						var ms_fourteen = MediaScheduleResult.getValue(Coloumn20);
						ms_fourteen=validateValue(ms_fourteen);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_fourteen:' + ms_fourteen);
						
								
						 if(ms_fourteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_fourteen_alias=get_alias(ms_fourteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_fourteen_alias:' + duration_fourteen_alias);
							
							duration_fourteen=get_aliasduration(ms_fourteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_fourteen:' + duration_fourteen);
							
							
							
							  if(media_type_tv==1)
							{
								fourteen_rate=spotTotal(item_rate,duration_fourteen,duration_fourteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate fourteen :' + fourteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								fourteen_rate=other_than_spot_Total(item_rate,duration_fourteen,duration_fourteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate fourteen :' + fourteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(fourteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//fourteen
						
						
						
						
						
						var Coloumn21= columns[22];
						var ms_fifteen = MediaScheduleResult.getValue(Coloumn21);
						ms_fifteen=validateValue(ms_fifteen);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_fifteen:' + ms_fifteen);
						
								
						 if(ms_fifteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_fifteen_alias=get_alias(ms_fifteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_fifteen_alias:' + duration_fifteen_alias);
							
							duration_fifteen=get_aliasduration(ms_fifteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_fifteen:' + duration_fifteen);
							
							
							
							  if(media_type_tv==1)
							{
								fifteen_rate=spotTotal(item_rate,duration_fifteen,duration_fifteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate fifteen :' + fifteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								fifteen_rate=other_than_spot_Total(item_rate,duration_fifteen,duration_fifteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate fifteen :' + fifteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(fifteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//fifteen
						
						
						
						
						
						var Coloumn22= columns[23];
						var ms_sixteen = MediaScheduleResult.getValue(Coloumn22);
						ms_sixteen=validateValue(ms_sixteen);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_sixteen:' + ms_sixteen);
						
									
						 if(ms_sixteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_sixteen_alias=get_alias(ms_sixteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_sixteen_alias:' + duration_sixteen_alias);
							
							duration_sixteen=get_aliasduration(ms_sixteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_sixteen:' + duration_sixteen);
							
							
							
							  if(media_type_tv==1)
							{
								sixteen_rate=spotTotal(item_rate,duration_sixteen,duration_sixteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate sixteen :' + sixteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								sixteen_rate=other_than_spot_Total(item_rate,duration_sixteen,duration_sixteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate sixteen :' + sixteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(sixteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//sixteen
						
						
						
						
						
						
						
						
						var Coloumn23= columns[24];
						var ms_seventeen = MediaScheduleResult.getValue(Coloumn23);
						ms_seventeen=validateValue(ms_seventeen);
		                //nlapiLogExecution('DEBUG', '605', 'ms_seventeen:' + ms_seventeen);
						
						
											
						 if(ms_seventeen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_seventeen_alias=get_alias(ms_seventeen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_seventeen_alias:' + duration_seventeen_alias);
							
							duration_seventeen=get_aliasduration(ms_seventeen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_seventeen:' + duration_seventeen);
							
							
							
							  if(media_type_tv==1)
							{
								seventeen_rate=spotTotal(item_rate,duration_seventeen,duration_seventeen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate seventeen :' + seventeen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								seventeen_rate=other_than_spot_Total(item_rate,duration_seventeen,duration_seventeen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate seventeen :' + seventeen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(seventeen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//seventeen
						
						
						
						
						
						var Coloumn24= columns[25];
						var ms_eighteen = MediaScheduleResult.getValue(Coloumn24);
						ms_eighteen=validateValue(ms_eighteen);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_eighteen:' + ms_eighteen);
						
					   						
						 if(ms_eighteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_eighteen_alias=get_alias(ms_eighteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eighteen_alias:' + duration_eighteen_alias);
							
							duration_eighteen=get_aliasduration(ms_eighteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_eighteen:' + duration_eighteen);
							
							
							
							  if(media_type_tv==1)
							{
								eighteen_rate=spotTotal(item_rate,duration_eighteen,duration_eighteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate eighteen :' + eighteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								eighteen_rate=other_than_spot_Total(item_rate,duration_eighteen,duration_eighteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate eighteen :' + eighteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(eighteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//eighteen	
						
						
						
						
						var Coloumn25= columns[26];
						var ms_ninteen = MediaScheduleResult.getValue(Coloumn25);
						ms_ninteen=validateValue(ms_ninteen);
		                //nlapiLogExecution('DEBUG', '605', 'ms_ninteen:' + ms_ninteen);
						
							   						
						 if(ms_ninteen.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_ninteen_alias=get_alias(ms_ninteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_ninteen_alias:' + duration_ninteen_alias);
							
							duration_ninteen=get_aliasduration(ms_ninteen,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_ninteen:' + duration_ninteen);
							
							
							
							  if(media_type_tv==1)
							{
								ninteen_rate=spotTotal(item_rate,duration_ninteen,duration_ninteen_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate ninteen :' + ninteen_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								ninteen_rate=other_than_spot_Total(item_rate,duration_ninteen,duration_ninteen_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate ninteen :' + ninteen_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(ninteen_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//ninteen	
												
						
						
						var Coloumn26= columns[27];
						var ms_twenty = MediaScheduleResult.getValue(Coloumn26);
						ms_twenty=validateValue(ms_twenty);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_twenty:' + ms_twenty);
						
							   						
						 if(ms_twenty.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twenty_alias=get_alias(ms_twenty,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twenty_alias:' + duration_twenty_alias);
							
							duration_twenty=get_aliasduration(ms_twenty,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twenty:' + duration_twenty);
							
							
							
							  if(media_type_tv==1)
							{
								twenty_rate=spotTotal(item_rate,duration_twenty,duration_twenty_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twenty :' + twenty_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twenty_rate=other_than_spot_Total(item_rate,duration_twenty,duration_twenty_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twenty :' + twenty_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twenty_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twenty	
						
						
						
						
						
						
						
						
						
						
						var Coloumn27= columns[28];
						var ms_twentyone = MediaScheduleResult.getValue(Coloumn27);
						ms_twentyone=validateValue(ms_twentyone);
		                //nlapiLogExecution('DEBUG', '605', 'ms_twentyone:' + ms_twentyone);
						
						
							   						
						 if(ms_twentyone.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentyone_alias=get_alias(ms_twentyone,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyone_alias:' + duration_twentyone_alias);
							
							duration_twentyone=get_aliasduration(ms_twentyone,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyone:' + duration_twentyone);
							
							
							
							  if(media_type_tv==1)
							{
								twentyone_rate=spotTotal(item_rate,duration_twentyone,duration_twentyone_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentyone :' + twentyone_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentyone_rate=other_than_spot_Total(item_rate,duration_twentyone,duration_twentyone_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentyone :' + twentyone_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentyone_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentyone	
						
										
						
						
						var Coloumn28= columns[29];
						var ms_twentytwo = MediaScheduleResult.getValue(Coloumn28);
						ms_twentytwo=validateValue(ms_twentytwo);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_twentytwo:' + ms_twentytwo);
				
				         		   						
						 if(ms_twentytwo.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentytwo_alias=get_alias(ms_twentytwo,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentytwo_alias:' + duration_twentytwo_alias);
							
							duration_twentytwo=get_aliasduration(ms_twentytwo,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentytwo:' + duration_twentytwo);
							
							
							
							  if(media_type_tv==1)
							{
								twentytwo_rate=spotTotal(item_rate,duration_twentytwo,duration_twentytwo_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentytwo :' + twentytwo_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentytwo_rate=other_than_spot_Total(item_rate,duration_twentytwo,duration_twentytwo_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentytwo :' + twentytwo_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentytwo_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentytwo	
						
						
						
						var Coloumn29= columns[30];
						var ms_twentythree = MediaScheduleResult.getValue(Coloumn29);
						ms_twentythree=validateValue(ms_twentythree);
		                //nlapiLogExecution('DEBUG', '605', 'ms_twentythree:' + ms_twentythree);
						
								   						
						 if(ms_twentythree.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentythree_alias=get_alias(ms_twentythree,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentythree_alias:' + duration_twentythree_alias);
							
							duration_twentythree=get_aliasduration(ms_twentythree,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentythree:' + duration_twentythree);
							
							
							
							  if(media_type_tv==1)
							{
								twentythree_rate=spotTotal(item_rate,duration_twentythree,duration_twentythree_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentythree :' + twentythree_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentythree_rate=other_than_spot_Total(item_rate,duration_twentythree,duration_twentythree_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentythree :' + twentythree_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentythree_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentythree
						
						var Coloumn30= columns[31];
						var ms_twentyfour = MediaScheduleResult.getValue(Coloumn30);
						ms_twentyfour=validateValue(ms_twentyfour);
		       //         //nlapiLogExecution('DEBUG', '605', 'ms_twentyfour:' + ms_twentyfour);
						
								   						
						 if(ms_twentyfour.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentyfour_alias=get_alias(ms_twentyfour,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyfour_alias:' + duration_twentyfour_alias);
							
							duration_twentyfour=get_aliasduration(ms_twentyfour,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyfour:' + duration_twentyfour);
							
							
							
							  if(media_type_tv==1)
							{
								twentyfour_rate=spotTotal(item_rate,duration_twentyfour,duration_twentyfour_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentyfour :' + twentyfour_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentyfour_rate=other_than_spot_Total(item_rate,duration_twentyfour,duration_twentyfour_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentyfour :' + twentyfour_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentyfour_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentyfour
						
						
						
						var Coloumn31= columns[32];
						var ms_twentyfive = MediaScheduleResult.getValue(Coloumn31);
						ms_twentyfive=validateValue(ms_twentyfive);
		        //        //nlapiLogExecution('DEBUG', '605', 'ms_twentyfive:' + ms_twentyfive);
						
									   						
						 if(ms_twentyfive.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentyfive_alias=get_alias(ms_twentyfive,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyfive_alias:' + duration_twentyfive_alias);
							
							duration_twentyfive=get_aliasduration(ms_twentyfive,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyfive:' + duration_twentyfive);
							
							
							
							  if(media_type_tv==1)
							{
								twentyfive_rate=spotTotal(item_rate,duration_twentyfive,duration_twentyfive_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentyfive :' + twentyfive_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentyfive_rate=other_than_spot_Total(item_rate,duration_twentyfive,duration_twentyfive_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentyfive :' + twentyfive_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentyfive_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentyfive
						
						
						
						
						
						
						var Coloumn32= columns[33];
						var ms_twentysix = MediaScheduleResult.getValue(Coloumn32);
						ms_twentysix=validateValue(ms_twentysix);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_twentysix:' + ms_twentysix);
							 if(ms_twentysix.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentysix_alias=get_alias(ms_twentysix,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentysix_alias:' + duration_twentysix_alias);
							
							duration_twentysix=get_aliasduration(ms_twentysix,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentysix:' + duration_twentysix);
							
							
							
							  if(media_type_tv==1)
							{
								twentysix_rate=spotTotal(item_rate,duration_twentysix,duration_twentysix_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentysix :' + twentysix_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentysix_rate=other_than_spot_Total(item_rate,duration_twentysix,duration_twentysix_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentysix :' + twentysix_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentysix_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentysix
						
						
						var Coloumn33= columns[34];
						var ms_twentyseven = MediaScheduleResult.getValue(Coloumn33);
						ms_twentyseven=validateValue(ms_twentyseven);
		         //       //nlapiLogExecution('DEBUG', '605', 'ms_twentyseven:' + ms_twentyseven);
						
						 if(ms_twentyseven.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentyseven_alias=get_alias(ms_twentyseven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyseven_alias:' + duration_twentyseven_alias);
							
							duration_twentyseven=get_aliasduration(ms_twentyseven,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyseven:' + duration_twentyseven);
							
							
							
							  if(media_type_tv==1)
							{
								twentyseven_rate=spotTotal(item_rate,duration_twentyseven,duration_twentyseven_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentyseven :' + twentyseven_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentyseven_rate=other_than_spot_Total(item_rate,duration_twentyseven,duration_twentyseven_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentyseven :' + twentyseven_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentyseven_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentyseven
						
						var Coloumn34= columns[35];
						var ms_twentyeight = MediaScheduleResult.getValue(Coloumn34);
						ms_twentyeight=validateValue(ms_twentyeight);
		          //      //nlapiLogExecution('DEBUG', '605', 'ms_twentyeight:' + ms_twentyeight);
						if(ms_twentyeight.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentyeight_alias=get_alias(ms_twentyeight,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyeight_alias:' + duration_twentyeight_alias);
							
							duration_twentyeight=get_aliasduration(ms_twentyeight,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentyeight:' + duration_twentyeight);
							
							
							
							  if(media_type_tv==1)
							{
								twentyeight_rate=spotTotal(item_rate,duration_twentyeight,duration_twentyeight_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentyeight :' + twentyeight_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentyeight_rate=other_than_spot_Total(item_rate,duration_twentyeight,duration_twentyeight_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentyeight :' + twentyeight_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentyeight_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentyeight
						
						
						var Coloumn35= columns[36];
						var ms_twentynine = MediaScheduleResult.getValue(Coloumn35);
						ms_twentynine=validateValue(ms_twentynine);
		          //      //nlapiLogExecution('DEBUG', '605', 'ms_twentynine:' + ms_twentynine);
							if(ms_twentynine.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_twentynine_alias=get_alias(ms_twentynine,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentynine_alias:' + duration_twentynine_alias);
							
							duration_twentynine=get_aliasduration(ms_twentynine,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_twentynine:' + duration_twentynine);
							
							
							
							  if(media_type_tv==1)
							{
								twentynine_rate=spotTotal(item_rate,duration_twentynine,duration_twentynine_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate twentynine :' + twentynine_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								twentynine_rate=other_than_spot_Total(item_rate,duration_twentynine,duration_twentynine_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate twentynine :' + twentynine_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(twentynine_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//twentynine
						
						
						var Coloumn36= columns[37];
						var ms_thirty = MediaScheduleResult.getValue(Coloumn36);
						ms_thirty=validateValue(ms_thirty);
		             //   //nlapiLogExecution('DEBUG', '605', 'ms_thirty:' + ms_thirty);
						
						
						if(ms_thirty.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_thirty_alias=get_alias(ms_thirty,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirty_alias:' + duration_thirty_alias);
							
							duration_thirty=get_aliasduration(ms_thirty,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirty:' + duration_thirty);
							
							
							
							  if(media_type_tv==1)
							{
								thirty_rate=spotTotal(item_rate,duration_thirty,duration_thirty_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate thirty :' + thirty_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								thirty_rate=other_than_spot_Total(item_rate,duration_thirty,duration_thirty_alias)
								//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate thirty :' + thirty_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(thirty_rate);
						    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//thirty
						
						
						var Coloumn37= columns[38];
						var ms_thirtyone = MediaScheduleResult.getValue(Coloumn37);
						ms_thirtyone=validateValue(ms_thirtyone);
		          //      //nlapiLogExecution('DEBUG', '605', 'ms_thirtyone:' + ms_thirtyone);
						
						if(ms_thirtyone.indexOf(MediaPlan_Alias)!=-1)
						{
							
							duration_thirtyone_alias=get_alias(ms_thirtyone,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirtyone_alias:' + duration_thirtyone_alias);
							
							duration_thirtyone=get_aliasduration(ms_thirtyone,media_type_tv,MediaPlanID,MediaPlan_Alias);
							//nlapiLogExecution('DEBUG', '605', 'duration_thirtyone:' + duration_thirtyone);
							
							
							
							  if(media_type_tv==1)
							{
								thirtyone_rate=spotTotal(item_rate,duration_thirtyone,duration_thirtyone_alias)
								//nlapiLogExecution('DEBUG', '605', 'Spot Rate thirtyone :' + thirtyone_rate);
															
							}
			                else if(media_type_tv==2 || media_type_tv==3 || media_type_tv==4)
							{
								
								thirtyone_rate=other_than_spot_Total(item_rate,duration_thirtyone,duration_thirtyone_alias)
					//			//nlapiLogExecution('DEBUG', '605', ' Other Than Spot Rate thirtyone :' + thirtyone_rate);
								
							}
							
							
							total_duration=parseFloat(total_duration)+parseFloat(thirtyone_rate);
				//		    //nlapiLogExecution('DEBUG', '605', 'total_duration One:' + total_duration);
											
							
						 }//thirtyone
						
						
						
			//			//nlapiLogExecution('DEBUG', '605', '*********     Final total_duration ******* ' + total_duration);
			
	//if(ms_month_txt == 'January' && ms_month_txt != 'February' && ms_month_txt != 'March' &&  ms_month_txt != 'April' && ms_month_txt != 'May' && ms_month_txt != 'June')			
										
				if (ms_month == 1)
				{
								
					jan_total=parseFloat(jan_total)+parseFloat(total_duration);
					//nlapiLogExecution('DEBUG', '605', '*********  January ****************');	
					
				}
				else if(ms_month == 2) 
				{
					
				
					feb_total=parseFloat(feb_total)+parseFloat(total_duration);
				//	//nlapiLogExecution('DEBUG', '605', '*********  February ****************');	
					
				}
				else if(ms_month ==3)
				{
				
					mar_total=parseFloat(mar_total)+parseFloat(total_duration);
				//		//nlapiLogExecution('DEBUG', '605', '*********  March ****************');	
					
					
				}
				else if( ms_month == 4)
				{
					
					apr_total=parseFloat(apr_total)+parseFloat(total_duration);
				//		//nlapiLogExecution('DEBUG', '605', '*********  April ****************');	
					
					
				}
				else if(ms_month == 5)
				{
				
					may_total=parseFloat(may_total)+parseFloat(total_duration);
				nlapiLogExecution('DEBUG', '605', '*********  May ****************');	
					
					
				}
				else if( ms_month == 6)
				{
					
					jun_total=parseFloat(jun_total)+parseFloat(total_duration);
				nlapiLogExecution('DEBUG', '605', '*********  June ****************');	
					
					
				}
				else if( ms_month == 7)
				{
					
					july_total=parseFloat(july_total)+parseFloat(total_duration);
				//		//nlapiLogExecution('DEBUG', '605', '*********  July ****************');	
					
					
				}
				else if( ms_month == 8)
				{
					
					
					aug_total=parseFloat(aug_total)+parseFloat(total_duration);
			//		//nlapiLogExecution('DEBUG', '605', '*********  August ****************');	
					
				}
				else if(ms_month == 9)
				{
					
					sep_total=parseFloat(sep_total)+parseFloat(total_duration);
				//	//nlapiLogExecution('DEBUG', '605', '*********  September ****************');	
					
					
				}
				else if( ms_month == 10)
				{
					oct_total=parseFloat(oct_total)+parseFloat(total_duration);
				//	//nlapiLogExecution('DEBUG', '605', '*********  October ****************');	
									
				}
				else if(ms_month == 11)
				{
					nov_total=parseFloat(nov_total)+parseFloat(total_duration);
				//	//nlapiLogExecution('DEBUG', '605', '*********  November ****************');	
					
					
				}
				else if(ms_month == 12) 
				{
					
					dec_total=parseFloat(dec_total)+parseFloat(total_duration);
				//	//nlapiLogExecution('DEBUG', '605', '*********  December ****************');	
					
					
				}
				
			     	
					
			//	//nlapiLogExecution('DEBUG', '605', '*********  January Total :  ******* ' + jan_total);		
			//	//nlapiLogExecution('DEBUG', '605', '*********  February Total :  ******* ' + feb_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  March Total :  ******* ' + mar_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  April Total :  ******* ' + apr_total);	
			nlapiLogExecution('DEBUG', '605', '*********  May Total :  ******* ' + may_total);	
			nlapiLogExecution('DEBUG', '605', '*********  June Total :  ******* ' + jun_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  July Total :  ******* ' + july_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  August Total :  ******* ' + aug_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  September Total :  ******* ' + sep_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  October Total :  ******* ' + oct_total);	
			//	//nlapiLogExecution('DEBUG', '605', '*********  November Total :  ******* ' + nov_total);		
			//	//nlapiLogExecution('DEBUG', '605', '*********  December Total :  ******* ' + dec_total);
							
				total_duration=0;	
			
		//	array_customer[arr_cnt++]=SalesREPText+'##'+EndCustomerNameText+'##'+CustomerNameText+'##'+ProductText+'##'+jan_total+'##'+feb_total+'##'+mar_total+'##'+apr_total+'##'+may_total+'##'+jun_total+'##'+july_total+'##'+aug_total+'##'+sep_total+'##'+oct_total+'##'+nov_total+'##'+dec_total;				
			
						
						}//Approved Status Check	
								
						
					}//Media Schedule REsult 
					
					
						
						
						prod_cust[pd++]=ProductText+'%%'+EndCustomerNameText;
						cust_prod[pcd++]=EndCustomerNameText+'%%'+ProductText;
						
						//product_arr.push();
						//customer_arr.push();
							
					// End Code : For searching the Media schedule for given year & sales Rep 
			//	nlapiLogExecution('DEBUG','In Suitelet Post Method','array_customer in Media Plan Search =='+array_customer)
				}
				
				
			 array_customer[arr_cnt++]=SalesREPText+'##'+EndCustomerNameText+'##'+CustomerNameText+'##'+ProductText+'##'+jan_total+'##'+feb_total+'##'+mar_total+'##'+apr_total+'##'+may_total+'##'+jun_total+'##'+july_total+'##'+aug_total+'##'+sep_total+'##'+oct_total+'##'+nov_total+'##'+dec_total;				
			
		//	nlapiLogExecution('DEBUG','In Suitelet Post Method','array_customer in 600 Search =='+array_customer)	
				
				
		  }
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','prod_cust =='+prod_cust);	
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','pd =='+pd);	
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','cust_prod =='+cust_prod);	
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','pcd =='+pcd);
			
			
		cust_prod=  removearrayduplicate(cust_prod);
			
			
			  
		 prod_cust=  removearrayduplicate(prod_cust);
		 nlapiLogExecution('DEBUG','In Suitelet Post Method','After Sorting======== =='+cust_prod);
		 nlapiLogExecution('DEBUG','In Suitelet Post Method','After Sorting Length======== =='+cust_prod.length);	
		 nlapiLogExecution('DEBUG','In Suitelet Post Method','After Sorting======== =='+prod_cust);
		 nlapiLogExecution('DEBUG','In Suitelet Post Method','After Sorting Length======== =='+prod_cust.length);	
		 nlapiLogExecution('DEBUG','In Suitelet Post Method','array_customer =='+array_customer);	
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','arr_cnt =='+arr_cnt);	
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','array_customer Length =='+array_customer.length);
		  
		  nlapiLogExecution('DEBUG','In Suitelet Post Method','array_customer Last =='+array_customer)
		 // Appendix_B_PDF_Layout( SalesREPText,EndCustomerNameText,CustomerNameText,ProductText,jan_total,feb_total,mar_total,apr_total,may_total,jun_total,july_total,aug_total,sep_total,oct_total,nov_total,dec_total);
	      Appendix_B_PDF_Layout(SalesREPText,Year,prod_cust,array_customer);
	}
	catch(ex)
	{
	nlapiLogExecution('DEBUG','In Suitelet Post Method','erro =='+ex)
	  	
	}

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================





function salesorder_rate(salesorder_ID,tv_program)
{
	var item_rate;
	var item_quantity;
	var sales_result=new Array();
	var sales_cnt=0;
	if(salesorder_ID!=null && salesorder_ID!='')
	{
		var sales_OBJ=nlapiLoadRecord('salesorder',salesorder_ID);
		nlapiLogExecution('DEBUG','salesorder_rate','sales_OBJ =='+sales_OBJ)
		
		if(sales_OBJ!=null && sales_OBJ!='')
		{
			
			var item_count=sales_OBJ.getLineItemCount('item');
			nlapiLogExecution('DEBUG','salesorder_rate','item_count =='+item_count);
			
			if(item_count!=null)
			{
				for(var p=1;p<=item_count;p++)
				{
					var item_id=sales_OBJ.getLineItemValue('item','item',p);
				//	nlapiLogExecution('DEBUG','salesorder_rate','item_id =='+item_id);
					
					item_rate=sales_OBJ.getLineItemValue('item','rate',p);
				//	nlapiLogExecution('DEBUG','salesorder_rate','item_rate =='+item_rate);
					
					
					item_quantity=sales_OBJ.getLineItemValue('item','quantity',p);
				//	nlapiLogExecution('DEBUG','salesorder_rate','item_quantity =='+item_quantity);
					
					
					
				}
				
				
				
			}
			
			
			
			
			
			
			
			
			
			
		}//Sales OBJ
	
		
	}//Sales ID Check
	
	
	return sales_result;
	
}//Sales Rate Chaeck




function validateValue(value)
{
	if(value==null || value ==undefined || value=='')
	{
		value=''
	}
	return value;
}


function get_aliasduration(ms_alias,media_type_tv,mediaplanID,MediaPlan_Alias)
{
	var ml_recordID ;
	var ml_productname ;
	var ml_medianame ;
	var ml_newduration ;
	
	
	var aliasQuantity = ms_alias.split(',')
  //  nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
	
	for(var k=0;k<aliasQuantity.length;k++)
	{
		
		var alias_ms=aliasQuantity[k];
	//	nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
		
		var letter="";
	    var number="";
		
		  for(var q=0;q<alias_ms.length;q++)
		  {
		   var b= alias_ms.charAt(q);
	       if(isNaN(b) == true)
	       {
	       letter=letter+b;
	       }
	       else
	       {
	       number=number+b;
	       }
		  	
			
		  }//END - For
		  
		//   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
	   //    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
		   
		    if(MediaPlan_Alias==letter)
			{
				 var medialist_result=searchMedialist(letter, media_type_tv,mediaplanID);
			      nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','Media List Result ='+medialist_result);
				  
				   if(medialist_result!=null && medialist_result!='' && medialist_result!=undefined)
				   {
				   	
					var mlArr=new Array();
					mlArr = medialist_result.split('##');
					 ml_recordID = mlArr[0];
					 ml_productname = mlArr[1];
					 ml_medianame = mlArr[2];
					 ml_newduration = mlArr[3];
					
			//	    nlapiLogExecution('DEBUG', 'IF record', 'ml_recordID = ' + ml_recordID);
			//	    nlapiLogExecution('DEBUG', 'IF record', 'ml_productname = ' + ml_productname);
			//		nlapiLogExecution('DEBUG', 'IF record', 'ml_medianame = ' + ml_medianame);
				//    nlapiLogExecution('DEBUG', 'IF record', 'ml_newduration = ' + ml_newduration); 
		   	
			
			
			
			
			
			
		             }//ENMD- Media Result
				
				
			}//Media Alias Check
		
		
	}//For Loop Alias

	return ml_newduration;
	
}//END Function Alias




//get_aliasduration





function get_alias(ms_alias,media_type_tv,mediaplanID,MediaPlan_Alias)
{
	var ml_recordID ;
	var ml_productname ;
	var ml_medianame ;
	var ml_newduration ;
	var aliasnumber;
	
	
	var aliasQuantity = ms_alias.split(',')
  //  nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias Quantity Length --> ' + aliasQuantity.length)
	
	for(var k=0;k<aliasQuantity.length;k++)
	{
		
		var alias_ms=aliasQuantity[k];
	//	nlapiLogExecution('DEBUG', 'AFTER RECORD SUBMIT', 'Alias alias_ms --> ' + alias_ms)
		
		var letter="";
	    var number="";
		
		  for(var q=0;q<alias_ms.length;q++)
		  {
		   var b= alias_ms.charAt(q);
	       if(isNaN(b) == true)
	       {
	       letter=letter+b;
	       }
	       else
	       {
	       number=number+b;
	       }
		  	
			
		  }//END - For
		  
		//   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','letter='+letter)
	   //    nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','number='+number)
		   
		    if(MediaPlan_Alias==letter)
			{
			   aliasnumber=	number;
			   nlapiLogExecution('DEBUG','AFTER RECORD SUBMIT','***********aliasnumber **********='+aliasnumber)
				
			}//Media Alias Check
			
		
		
	}//For Loop Alias

	return aliasnumber;
	
}//END Function Alias



function searchMedialist(letter, media_type_tv, recordID)
{
	if(letter != '' && letter != null && letter != undefined)
	{
		if(media_type_tv != '' && media_type_tv != null && media_type_tv != undefined)
		{
			if(recordID != '' && recordID != null && recordID != undefined)
			{
				
					
	
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' searchMedialist');
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' letter  ==  ' + letter);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' media_type_tv  ==  ' + media_type_tv);
    //	nlapiLogExecution('DEBUG', 'searchMedialist', ' recordID  ==  ' + recordID);
    
    
    var ml_recordID;
    var ml_productname;
    var ml_medianame;
    var ml_newduration;
    var result;
    
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_mp2_alias', null, 'is', letter);
    filter[1] = new nlobjSearchFilter('custrecord_mp2_type', null, 'is', media_type_tv);
    filter[2] = new nlobjSearchFilter('custrecord_mp2_mp1ref', null, 'is', recordID);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('custrecord_mp2_productname');
    columns[2] = new nlobjSearchColumn('custrecord_mp2_spotname');
    columns[3] = new nlobjSearchColumn('custrecord_mp2_duration');
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_2', null, filter, columns);
    
    if (searchresults != null) 
	{
        nlapiLogExecution('DEBUG', 'searchMedialist', ' Number of Searches  ==  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++) 
		{
            ml_recordID = searchresults[j].getValue('internalid');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media List ][' + j + ']==' + ml_recordID);
            
            ml_productname = searchresults[j].getValue('custrecord_mp2_productname');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Product Name [ Media List ] [' + j + ']==' + ml_productname);
            
            ml_medianame = searchresults[j].getValue('custrecord_mp2_spotname');
            //		nlapiLogExecution('DEBUG', 'searchMedialist', ' Media Name  [ Media List ][' + j + ']==' + ml_medianame);
            
            ml_newduration = searchresults[j].getValue('custrecord_mp2_duration');
            nlapiLogExecution('DEBUG', 'searchMedialist', '  Media Duration [ Media List ] [' + j + ']==' + ml_newduration);
            
            result = ml_recordID + '##' + ml_productname + '##' + ml_medianame + '##' + ml_newduration;
            
        }
        
    }
    
    return result;
				
				
			}
				
		}
		
	}
	
	

    
}




function month_total(alias,s_month)
{
	
	
var jan_total=0;
var feb_total=0;
var mar_total=0;
var apr_total=0;
var may_total=0;
var june_total=0;
var july_total=0;
var aug_total=0;
var sep_total=0;
var oct_total=0;
var nov_total=0;
var dec_total=0
	
	
	if (s_month == 'January')
	{
		
		jan_total=parseFloat(jan_total)+parseFloat(alias);
		
	}
	else if(s_month == 'February') 
	{
		
		feb_total=parseFloat(feb_total)+parseFloat(alias);
		
	}
	else if(s_month == 'March')
	{
		mar_total=parseFloat(mar_total)+parseFloat(alias);
		
		
	}
	else if( s_month  == 'April')
	{
		apr_total=parseFloat(apr_total)+parseFloat(alias);
		
		
	}
	else if(s_month  == 'May')
	{
		may_total=parseFloat(may_total)+parseFloat(alias);
		
		
	}
	else if( s_month  == 'June')
	{
		june_total=parseFloat(june_total)+parseFloat(alias);
		
		
	}
	else if( s_month == 'July')
	{
		july_total=parseFloat(july_total)+parseFloat(alias);
		
		
	}
	else if( s_month  == 'August')
	{
		
		aug_total=parseFloat(aug_total)+parseFloat(alias);
		
	}
	else if(s_month == 'September')
	{
		sep_total=parseFloat(sep_total)+parseFloat(alias);
		
		
	}
	else if( s_month  == 'October')
	{
		oct_total=parseFloat(oct_total)+parseFloat(alias);
						
	}
	else if(s_month  == 'November')
	{
		nov_total=parseFloat(nov_total)+parseFloat(alias);
		
		
	}
	else if(s_month  == 'December') 
	{
		dec_total=parseFloat(dec_total)+parseFloat(alias);
	}
				
			     
			
	
}



function jan_total_rate(alias,s_month)
{
	
var jan_total=0;
	
	if (s_month == 'January')
	{
		
		jan_total=parseFloat(jan_total)+parseFloat(alias);
		
	}
	
 return jan_total;
	
}//Jan





function feb_total_rate(alias,s_month)
{
   var feb_total=0;
	
	if (s_month == 'February')
	{
		
		feb_total=parseFloat(feb_total)+parseFloat(alias);
		
	}
	
 return feb_total;
	
}//Feb



function mar_total_rate(alias,s_month)
{
   var mar_total=0;
	
	if (s_month == 'March')
	{
		
		mar_total=parseFloat(mar_total)+parseFloat(alias);
		
	}
	
 return mar_total;
	
}//mar



function apr_total_rate(alias,s_month)
{
   var apr_total=0;
	
	if (s_month == 'April')
	{
		
		apr_total=parseFloat(apr_total)+parseFloat(alias);
		
	}
	
 return apr_total;
	
}//apr




function may_total_rate(alias,s_month)
{
   var may_total=0;
	
	if (s_month == 'May')
	{
		
		may_total=parseFloat(may_total)+parseFloat(alias);
		
	}
	
 return may_total;
	
}//may



function jun_total_rate(alias,s_month)
{
   var jun_total=0;
	
	if (s_month == 'June')
	{
		
		jun_total=parseFloat(jun_total)+parseFloat(alias);
		
	}
	
 return jun_total;
	
}//jun


function jul_total_rate(alias,s_month)
{
   var jul_total=0;
	
	if (s_month == 'July')
	{
		
		jul_total=parseFloat(jul_total)+parseFloat(alias);
		
	}
	
 return jul_total;
	
}//jul




function aug_total_rate(alias,s_month)
{
   var aug_total=0;
	
	if (s_month == 'August')
	{
		
		aug_total=parseFloat(aug_total)+parseFloat(alias);
		
	}
	
 return aug_total;
	
}//aug




function sep_total_rate(alias,s_month)
{
   var sep_total=0;
	
	if (s_month == 'September')
	{
		
		sep_total=parseFloat(sep_total)+parseFloat(alias);
		
	}
	
 return sep_total;
	
}//sep




function oct_total_rate(alias,s_month)
{
   var oct_total=0;
	
	if (s_month == 'October')
	{
		
		oct_total=parseFloat(oct_total)+parseFloat(alias);
		
	}
	
 return oct_total;
	
}//oct




function nov_total_rate(alias,s_month)
{
   var nov_total=0;
	
	if (s_month == 'November')
	{
		
		nov_total=parseFloat(nov_total)+parseFloat(alias);
		
	}
	
 return nov_total;
	
}//nov





function dec_total_rate(alias,s_month)
{
   var dec_total=0;
	
	if (s_month == 'December')
	{
		
		dec_total=parseFloat(dec_total)+parseFloat(alias);
		
	}
	
 return dec_total;
	
}//dec



function spotTotal(sales_rate,aliasduration,alias)
{
	var spot_rate;
	
	spot_rate=(parseFloat(sales_rate)/60);
	nlapiLogExecution('DEBUG', '605', 'parseFloat(sales_rate)/60 :' + spot_rate);
	
	spot_rate=parseFloat(spot_rate)*parseFloat(aliasduration);
	nlapiLogExecution('DEBUG', '605', 'parseFloat(spot_rate)*parseFloat(aliasduration) :' + spot_rate);
	
	spot_rate=parseFloat(alias)*parseFloat(spot_rate);
	nlapiLogExecution('DEBUG', '605', 'parseFloat(alias)*parseFloat(spot_rate) :' + spot_rate);
	
	return  spot_rate;
	
}



function other_than_spot_Total(sales_rate,aliasduration,alias)
{
	var other_rate;
	
	other_rate=(parseFloat(sales_rate)/alias);
	nlapiLogExecution('DEBUG', '605', 'parseFloat(sales_rate)/alias :' + other_rate);
		
	return  other_rate;
	
}





function Appendix_B_PDF_Layout(SalesREPText,year,prod_cust,array_customer)
{
	
year_txt=year_text(year)	
	
	
var strVar="";
strVar += "";

strVar += "<table border=\"0\" width=\"100%\">";




strVar += "	<tr>";
strVar += "		<td font-size=\"8\" width=\"100%\"   align=\"left\" border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Sales Rep : "+SalesREPText+"<\/b> <\/td>";
strVar += "	<\/tr>";

strVar += "	<tr>";
strVar += "		<td font-size=\"8\" width=\"100%\"  align=\"left\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Year : "+year_txt+"<\/b><\/td>";
strVar += "	<\/tr>";

strVar += "<\/table>";

strVar += "<table border=\"0\" width=\"100%\">";

strVar += "	<tr>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\"  align=\"center\" border-top=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Sales Rep<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Customer<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Advertiser<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Product<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>January<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>February<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>March<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>April<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>May<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>June<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>July<\/b><\/td>";
strVar += "		<td font-size=\"6\" border-bottom=\"0.3\"  align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>August<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>September<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>October<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>November<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>December<\/b><\/td>";
strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-top=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" ><b>Total<\/b><\/td>";
strVar += "	<\/tr>";


var b_sales_rep;
var b_end_customer;
var b_customer;
var b_product;
var b_jan ;
var b_feb ;
var b_mar ;
var b_apr ;
var b_may ;
var b_june ;
var b_july ;
var b_aug ;
var b_sep ;
var b_oct ;
var b_nov ;
var b_dec ;



 	
 var prod_cust_Arr=new Array();
 var unique_prod_cust=new Array();
	 
 for (var z = 0; z < prod_cust.length; z++) 
 {
 
var total_jan=0;
var total_feb=0;
var total_mar=0;
var total_apr=0;
var total_may=0;
var total_june=0;
var total_july=0;
var total_aug=0;
var total_sep=0;
var total_oct=0;
var total_nov=0;
var total_dec=0;
var total_months=0;
 
 	nlapiLogExecution('DEBUG', 'printPDF', '  z=' + z);
 	
 	
 	unique_prod_cust = prod_cust[z].split('%%');
 	b_product_unique = unique_prod_cust[0];
 	b_customer_unique = unique_prod_cust[1];
 	
 	nlapiLogExecution('DEBUG', 'printPDF', '  b_product_unique=' + b_product_unique);
 	nlapiLogExecution('DEBUG', 'printPDF', '  b_customer_unique=' + b_customer_unique);
 	
 	 for(var t=0;t<array_customer.length;t++)
	 {
	 prod_cust_Arr = array_customer[t].split('##');
	 b_sales_rep = prod_cust_Arr[0];
	 b_end_customer = prod_cust_Arr[1];
	 b_customer = prod_cust_Arr[2];
	 b_product = prod_cust_Arr[3];
	 b_jan = prod_cust_Arr[4];
     b_feb = prod_cust_Arr[5];
	 b_mar = prod_cust_Arr[6];
	 b_apr = prod_cust_Arr[7];
	 b_may = prod_cust_Arr[8];
     b_june = prod_cust_Arr[9];
     b_july = prod_cust_Arr[10];
	 b_aug = prod_cust_Arr[11];
	 b_sep = prod_cust_Arr[12];
	 b_oct = prod_cust_Arr[13];
     b_nov = prod_cust_Arr[14];
	 b_dec = prod_cust_Arr[15];
	 
	 nlapiLogExecution('DEBUG', 'printPDF', '  t='+ t);
	 nlapiLogExecution('DEBUG', 'printPDF', '  prod_cust_Arr='+ prod_cust_Arr);
	 nlapiLogExecution('DEBUG', 'printPDF', '  b_end_customer='+ b_end_customer);
	 
	 if (b_product_unique == b_product && b_customer_unique == b_end_customer) 
	 {
	 	total_jan = parseFloat(total_jan) + parseFloat(b_jan);
	 	total_feb = parseFloat(total_feb) + parseFloat(b_feb);
	 	total_mar = parseFloat(total_mar) + parseFloat(b_mar);
	 	total_apr = parseFloat(total_apr) + parseFloat(b_apr);
	 	total_may = parseFloat(total_may) + parseFloat(b_may);
	 	total_june = parseFloat(total_june) + parseFloat(b_june);
	 	total_july = parseFloat(total_july) + parseFloat(b_july);
	 	total_aug = parseFloat(total_aug) + parseFloat(b_aug);
	 	total_sep = parseFloat(total_sep) + parseFloat(b_sep);
	 	total_oct = parseFloat(total_oct) + parseFloat(b_oct);
	 	total_nov = parseFloat(total_nov) + parseFloat(b_nov);
	 	total_dec = parseFloat(total_dec) + parseFloat(b_dec);
		
		 nlapiLogExecution('DEBUG', 'printPDF', '  ************* total_may= ************* '+ total_may);
	    nlapiLogExecution('DEBUG', 'printPDF', '  ************* total_june= *************'+ total_june);
	 	 	
	 	
	 }
		
		
	 }
	 total_months = parseFloat(total_months)+parseFloat(total_jan) + parseFloat(total_feb) + parseFloat(total_mar) + parseFloat(total_apr) + parseFloat(total_may) + parseFloat(total_june) + parseFloat(total_july) + parseFloat(total_aug) + parseFloat(total_sep) + parseFloat(total_oct) + parseFloat(total_nov) + parseFloat(total_dec);
	 
 	    strVar += "	<tr>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" >" + b_sales_rep + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + b_customer_unique + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + b_customer + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + b_product_unique + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_jan.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_feb.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_mar.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_apr.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_may.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_june.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_july.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_aug.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_sep.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_oct.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_nov.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_dec.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "		<td font-size=\"6\"  border-bottom=\"0.3\" align=\"center\" border-right=\"0.3\" font-family=\"Helvetica\" >" + total_months.toFixed(2) + "&nbsp;<\/td>";
	 	strVar += "	<\/tr>";
 	   
 }

strVar += "<\/table>";
strVar += "";
	
        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
		xml += "<pdf>\n<body>";
		xml += strVar;
		xml += "</body>\n</pdf>";
		// run the BFO library to convert the xml document to a PDF 
		// run the BFO library to convert the xml document to a PDF 
		var file = nlapiXMLToPDF(xml);
		nlapiLogExecution('DEBUG', 'printPDF', 'File =='+file);
		//Create PDF File with TimeStamp in File Name
		
		
		var d_currentTime = new Date();    
    var timestamp=d_currentTime.getTime();
	//Create PDF File with TimeStamp in File Name
	var fileObj = nlapiCreateFile('Appendix B.pdf_'+timestamp+'.pdf', 'PDF', file.getValue());
		//var fileObj=nlapiCreateFile('Appendix I.pdf', 'PDF', file.getValue());
	//	var fileObj=nlapiCreateFile('Appendix B.pdf', 'PDF', file.getValue());
				
		nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj='+ fileObj);
		fileObj.setFolder(219);// Folder PDF File is to be stored
		var fileId=nlapiSubmitFile(fileObj);
		nlapiLogExecution('DEBUG', 'printPDF', '*************===PDF fileId *********** =='+ fileId);
        // Run the BFO library to convert the xml document to a PDF 
		// Set content type, file name, and content-disposition (inline means display in browser)
	
	
	
		
		
		
		
	// ===================== Create Appendix Details =================================
		
		
	// Get the Current Date
	var date = new Date();
    nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Date ==' + date);

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'IST Date ==' + istdate);
    
	// Get the Day  
     day = istdate.getDate();
	 
     nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Day ==' + day);
	 // Get the  Month  
     month = istdate.getMonth()+1;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Month ==' + month);
	 
      // Get the Year 
	 year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', 'Year ==' +year);
	  
	 // Get the String Date in dd/mm/yyy format 
	 pdfDate= day + '/' + month + '/' + year;
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport',' Todays Date =='+pdfDate);	
	 
	 // Get the Hours , Minutes & Seconds for IST Date
	 var timePeriod;
	 var hours=istdate.getHours();
	 var mins=istdate.getMinutes();
	 var secs=istdate.getSeconds();
	 
	 if(hours>12)
	 {
	 	hours=hours-12;
	 	timePeriod='PM'
	 }
	 else if (hours == 12) 
	 {
	   timePeriod = 'PM';
	 }
	 else
	 {
	 	timePeriod='AM'
	 }
	 
	 // Get Time in hh:mm:ss: AM/PM format
	 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
		
	
		
	 nlapiLogExecution('DEBUG', 'stockRec_PDFReport', ' Todays Date Time  ==' + pdfTime);
	
		
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' User ID --> ' + userId);
		
		 
	    var appRecord=nlapiCreateRecord('customrecord_appendix_details');
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Appendix Create Record : " + appRecord); 
	     
		appRecord.setFieldValue('custrecord_app_user', userId);
		appRecord.setFieldValue('custrecord_app_date', pdfDate);
		appRecord.setFieldValue('custrecord_app_time', pdfTime);
		appRecord.setFieldValue('custrecord_app_fileid', fileId);
		appRecord.setFieldValue('custrecord_is_printed', 'T'); 
		var appRecSubmitID = nlapiSubmitRecord(appRecord, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', ' Appendix Submit Record  ID : ' + appRecSubmitID);
			
		
		
	//======================================= End =====================================================	
		
		
		

	
	
	
}



function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
      {  
     for(var j=0; j<array.length;j++ )
       {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}




function year_text(year)
{
    var recordID;
    var year_ID;
    
    
   
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    
    var searchresults = nlapiSearchRecord('customrecord_mediaplan_3', null, null, columns);
    
    if (searchresults != null) 
	{
        //nlapiLogExecution('DEBUG', 'Year ID', ' Number of Searches Media Schedule -->  ' + searchresults.length);
        
        for (var j = 0; j < searchresults.length; j++)
		{
            recordID = searchresults[0].getValue('internalid');
            ////nlapiLogExecution('DEBUG', 'searchMedialist', '  Record ID  [ Media Schedule ][' + j + ']==' + recordID);
        
        }
    }
    
    
    
    var custOBJ = nlapiLoadRecord('customrecord_mediaplan_3', recordID);
    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Media Schedule -->' + custOBJ)
    
    if (custOBJ != null) 
	{
    
        var myFld = custOBJ.getField('custrecord_mp3_year')
        var options = myFld.getSelectOptions();
        //nlapiLogExecution('Debug', 'CheckParameters', 'options=' + options.length);
        
        for (var i = 0; i < options.length; i++)
		{
            var yearId = options[i].getId();
            var yearText = options[i].getText();
          //  //nlapiLogExecution('DEBUG', 'suiteletFunction', 'i -->' + i + 'Year ID -->' + yearId + 'Year Text -->' + yearText);
            
            
            if (yearId == year)
			{
                year_ID = yearText;
                break;
            }
            
            
        }//For
    }//Customer
    return year_ID;
}


