// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT Media Plan Appendix B Print Report
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY


 try
	 {

     if (request.getMethod() == 'GET') 	
	 {
	 	
		/*
        * Begin :Code for Creating Media Plan Appendix B
		*/
		  
		  
		 
		var f_form = nlapiCreateForm('Appendix B Print');
	 	var s_salesRep = f_form.addField('custpage_salesrep', 'select', 'Sales Rep','employee');
		
		var d_year =  f_form.addField('custpage_year', 'select', 'Year','customlist65');
		
	    var checkBtn = f_form.addSubmitButton();
		response.writePage(f_form);
		
		
		
		
		
		/*
        * end :Code for Creating Media Plan Appendix B
		*/
		  
		
	 }
	 else if(request.getMethod() == 'POST')
	 {
	  
	   var SalesRep =  request.getParameter('custpage_salesrep');
	   nlapiLogExecution('DEBUG','In Suitelet Post Method','SalesRep =='+SalesRep)
	   var Year = request.getParameter('custpage_year');
	   nlapiLogExecution('DEBUG','In Suitelet Post Method','Year =='+Year)
	  
	  var params=new Array();
	 
	  params['status']='scheduled';
	  params['runasadmin']='T';
	  params['custscript_sales_rep']=SalesRep;
	  params['custscript_year']=Year;
	  var startDate = new Date();
	  params['startdate']=startDate.toUTCString();
	  
	  var status=nlapiScheduleScript('customscript_sch_mp_appnedixb_print','customdeploy1',params);
	  nlapiLogExecution('DEBUG', 'suiteletFunction', ' status: ' + status);
						  
	//  nlapiSetRedirectURL('https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl', null, null, null);
	 
//	 nlapiSetRedirectURL('https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl?sortcol=dcreated&sortdir=DESC&csv=HTML&OfficeXML=F&pdf=&datemodi=WITHIN&daterange=ALL&datefrom=&dateto=&date=ALL&scripttype=&primarykey=&scripttype=129',null,null,null);
//	  nlapiLogExecution('DEBUG', 'suiteletFunction', ' suitelet Called : ' );
	  
	  
	  
	  
	 
	  
	 	
	 }// else request.getMethod() == 'POST' end
	 
	 }//End-Try
	catch (e) 
	{
			nlapiLogExecution('DEBUG', 'IN SUITELET', 'Error -->' + e);
	}



}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
