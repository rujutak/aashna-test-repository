// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_Create_SingleInvoice_and_JV.js
	Author: Chandrakant Patil
	Company: Aashna Cloud Tech
	Date: 22July2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function Witecc_CreateSingle_InvoiceAnd_JV(type)
{
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'In Schedule Script');
	var context=nlapiGetContext();
    var usageBegin = context.getRemainingUsage();
	nlapiLogExecution('DEBUG', 'Script Usage', 'usageBegin= ' + usageBegin);
     	
	var Proctype=context.getSetting('SCRIPT','custscript_proctype');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Proctype ==' + Proctype);
	
	var Period=context.getSetting('SCRIPT','custscript_period');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Period ==' + Period);
	
	var Subsidiary=context.getSetting('SCRIPT','custscript_subsidiary');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Subsidiary ==' + Subsidiary);
	
	var customerid=context.getSetting('SCRIPT','custscript_customerid');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'customerid ==' + customerid);
	
	var Jobid=context.getSetting('SCRIPT','custscript_jobidinvoice');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Jobid ==' + Jobid);
	
	//===================================================================================================
	if(_logValidation(Jobid))
	{
		var o_wrecordObj = nlapiLoadRecord('customrecord_job', Jobid);
		var JOBCount=o_wrecordObj.getLineItemCount('recmachcustrecord_jcbill_job');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'JOB Line Count= ' + JOBCount);
		var flag=0;
		var a_searchResult = new Array();
		var arraycny = 0;
		for (var k = 1; JOBCount != null && k <= JOBCount; k++)
		{
	        //var JobId=o_wrecordObj.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_job',k);
			var CustomerId1=o_wrecordObj.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_vendor',k);
	        var preFactura=o_wrecordObj.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_bill', k); //custrecord_jcbill_bill
	        var processed =o_wrecordObj.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_processed', k);
			if (processed == 'F')
			{
				a_searchResult[arraycny++] = CustomerId1 + '%$%' + preFactura;
			}
		}
	}
	
	for (var k = 0; a_searchResult != null && k < a_searchResult.length; k++)
	{
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult['+k+']=' + a_searchResult[k]);
	}
	var InvArray = new Array();
	var cnt = 0;
	if (a_searchResult != null && _logValidation(Jobid))
	{
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult length=' + a_searchResult.length);
		
		//for loop for Invoices
		for (var k = 0; a_searchResult != null && k < a_searchResult.length; k++)
		{
			var Splita_searchResult = new Array();
			Splita_searchResult = a_searchResult[k].split('%$%');
			
			var s_invoiceID = Splita_searchResult[1];
		    nlapiLogExecution('DEBUG', 'CustomerSearch', 's_invoiceID['+k+']=' + s_invoiceID);
			
			var s_CustomerName = Splita_searchResult[0];
		    nlapiLogExecution('DEBUG', 'CustomerSearch', 's_CustomerName['+k+']=' + s_CustomerName);
			
			if(InvArray.length==0)
			{
				InvArray[cnt++]=s_CustomerName+'#$'+s_invoiceID;
			}
			else if(InvArray.length!=0)
			{														
				var flag=0;
				for(var j=0;InvArray!=null && j<InvArray.length;j++)
				{
					var SplitInvArray=new Array();
					SplitInvArray=InvArray[j].split('#$');
					if(SplitInvArray[0]==s_CustomerName)	
					{						
						InvArray[j]=InvArray[j]+'#$'+s_invoiceID;
						flag=1;
					}										
				} // for j
				if(flag!=1)
				{
					InvArray[cnt++]=s_CustomerName+'#$'+s_invoiceID;
				}
			}//else if(InvArray.length!=0)
		}
		var b_jobStatusFlag = 0;
		//Get the data from the invoice and create the single Invoice
		for(var d=0;d<InvArray.length;d++)
		{
			try
			{
				nlapiLogExecution('DEBUG', 'InvArray', 'InvArray['+d+']=' + InvArray[d]);
				
				var a_SplitInvArray=new Array();
				a_SplitInvArray=InvArray[d].split('#$');
				//if(a_SplitInvArray.length > 2)
				{
					var a_singleInvData = new Array();
					var i_count = 0;
					var a_JV_Array = new Array();
					var i_JVcnt = 0;				
					var a_sourceInvoiceArray = new Array();
					var s_invoiceData = '';
					var i_invoiceVendor = '';
					var i_newInvoiceCustomer = '';
					var a_billPaymentArray = new Array();
					var i_billJvcnt = 0;
					var a_billArray = new Array();
					var i_billcount = 0;
					var a_vendorPay = new Array();
					var i_vendorPaycnt = 0;
					for(var j=1;a_SplitInvArray!=null && j<a_SplitInvArray.length;j++)
					{
						// If condition to check that customes has more than 1 invoice					
						var o_invoiceObj = nlapiLoadRecord('customrecord_bill', a_SplitInvArray[j]);
						a_sourceInvoiceArray.push(a_SplitInvArray[j]);
						if(o_invoiceObj!=null)
						{
							if(j==1)
							{
								var i_invoiceStdPeriod = nlapiLookupField('customrecord_period', Period, 'custrecord_period_postingperiod')
							}
							i_newInvoiceCustomer = o_invoiceObj.getFieldValue('custrecord_bill_beneficiarycomiprepay');
							var i_invoicePeriod = o_invoiceObj.getFieldValue('custrecord_bill_period');
							//newly added
							//var i_invoiceStdPeriod = o_invoiceObj.getFieldValue('postingperiod');
							//=============================
							var i_invVendor = o_invoiceObj.getFieldValue('custrecord_bill_vendor');
							if(_logValidation(i_invVendor))
							{
								i_invoiceVendor = i_invVendor;
							}
							var i_location = o_invoiceObj.getFieldValue('custrecord_bill_location');
							var i_subsidiary = o_invoiceObj.getFieldValue('custrecord_bill_subsidiary');
							var i_InvoiceAmount = o_invoiceObj.getFieldValue('custrecord_bill_commission');
							a_billPaymentArray[i_billJvcnt++] = o_invoiceObj.getFieldValue('custrecord_bill_liquidation');
							s_invoiceData = a_SplitInvArray[0] +'#$%'+ i_invoicePeriod +'#$%'+ i_location +'#$%'+ i_invoiceVendor;
							//a_singleInvData[i_count++] = a_SplitInvArray[0] +'%%'+ i_location
							var i_lineCount = o_invoiceObj.getLineItemCount('recmachcustrecord_cbill_bill');
							
							//Getting the Bill and total Bill Amount for the Pre-Factura
							var i_billID = '';
							for(var n=0;n<a_billPaymentArray.length;n++)
							{
								var o_billObj = nlapiLoadRecord('customrecord_liquidation', a_billPaymentArray[0]);
								var i_billLineCount = o_billObj.getLineItemCount('recmachcustrecord_jbill_liquidation');								
								for(var x=1;x<=i_billLineCount;x++)
								{
									i_lineBillID = o_billObj.getLineItemValue('recmachcustrecord_jbill_liquidation', 'custrecord_jbill_vendorbill', x);
									i_pdid = o_billObj.getLineItemValue('recmachcustrecord_jbill_liquidation', 'custrecord_jbill_bill', x);
									if(i_pdid == a_SplitInvArray[j])
									{
										i_billID = i_lineBillID;
										break;
									}									
								}
							}
							//----------------------------------------------------------
							
							var f_totalAmount = 0;
							for(a=1;a<=i_lineCount;a++)
							{
								var i_item = o_invoiceObj.getLineItemValue('recmachcustrecord_cbill_bill','custrecord_cbill_item',a);
								var i_quantity = 1;
								var i_rate = o_invoiceObj.getLineItemValue('recmachcustrecord_cbill_bill','custrecord_cbill_amount',a);
								//var i_item = o_invoiceObj.getLineItemValue('item','item',a);
								a_singleInvData[i_count++] = i_item +'%%'+ i_quantity +'%%'+ i_rate + '%%'+ i_billID;
								f_totalAmount = parseFloat(f_totalAmount) + parseFloat(i_rate);
								//nlapiLogExecution('DEBUG', 'InvArray', 'a_singleInvData['+a+']=' + a_SplitInvArray[0] +'%%'+ i_location +'%%'+ i_item +'%%'+ i_quantity +'%%'+ i_rate);
							}
							if(_logValidation(i_billID))
							{
								a_billArray[i_billcount++] = a_SplitInvArray[j] +'%#@'+ i_billID +'%#@'+ f_totalAmount;
								nlapiLogExecution('DEBUG', 'a_billArray', 'a_billArray= ' + a_SplitInvArray[j] +'%#@'+ i_billID +'%#@'+ f_totalAmount);
							}							
							//a_JV_Array[i_JVcnt++] = a_SplitInvArray[j] + '##'+ i_newInvoiceCustomer +'##'+ i_invoiceVendor + '##' + f_totalAmount+'##'+ i_invoiceStdPeriod +'##'+ i_subsidiary;
						}
					//}
				}
				if(a_singleInvData != null)
				{
					var o_newInvoiceObj = nlapiCreateRecord('invoice');
					var a_SplitInvData=new Array();
					a_SplitInvData = s_invoiceData.split('#$%');
					//nlapiLogExecution('DEBUG', 'InvArray', 's_invoiceData= ' + s_invoiceData);
					if(_logValidation(a_SplitInvData[0]))
					{					
						o_newInvoiceObj.setFieldValue('entity', i_newInvoiceCustomer);
					}
					if(_logValidation(a_SplitInvData[1]))
					{
						o_newInvoiceObj.setFieldValue('custbody_period', a_SplitInvData[1]);
					}
					if(_logValidation(a_SplitInvData[2]))
					{
						o_newInvoiceObj.setFieldValue('location', a_SplitInvData[2]);
					}
					if(_logValidation(a_SplitInvData[3]))
					{
						o_newInvoiceObj.setFieldValue('custbody_commissionvendor', a_SplitInvData[3]);
					}
					
					for(var t=0;a_singleInvData!=null && t<a_singleInvData.length;t++)
					{
						var a_SplitInvData=new Array();
						a_SplitInvData=a_singleInvData[t].split('%%');
						o_newInvoiceObj.setLineItemValue('item','item',(t+1),a_SplitInvData[0]);
						o_newInvoiceObj.setLineItemValue('item','quantity',(t+1),a_SplitInvData[1]);
						o_newInvoiceObj.setLineItemValue('item','rate',(t+1),a_SplitInvData[2]);
						o_newInvoiceObj.setLineItemValue('item','custcol_vendor_bill',(t+1),a_SplitInvData[3]);
					}
					if(_logValidation(a_sourceInvoiceArray))
					{
						//o_newInvoiceObj.setFieldValue('custbody_sourceinvoices', a_sourceInvoiceArray);
					}
					o_newInvoiceObj.setFieldValue('custbody_commissioninvoice', 'T');
					o_newInvoiceObj.setFieldValue('custbody_commissionprocessed', 'T');
					var i_InvId = nlapiSubmitRecord(o_newInvoiceObj);
					nlapiLogExecution('DEBUG', 'InvArray', 'Created Invoice =' + i_InvId);
					if(_logValidation(i_InvId))
					{
						var o_newInvoiceObj = nlapiLoadRecord('invoice', i_InvId);
						var i_newInvoiceCustomer = o_newInvoiceObj.getFieldValue('entity');
						var i_invoiceVendor = o_newInvoiceObj.getFieldValue('custbody_commissionvendor');
						var f_totalAmount = o_newInvoiceObj.getFieldValue('total');
						var i_invoiceStdPeriod = o_newInvoiceObj.getFieldValue('postingperiod');
						var i_subsidiary = o_newInvoiceObj.getFieldValue('subsidiary');
						var i_InvLineCount = o_newInvoiceObj.getLineItemCount('item');
						for(var t=1;t<=i_InvLineCount;t++)
						{
							var i_invItem = o_newInvoiceObj.getLineItemValue('item','item',t);
							var i_invRate = o_newInvoiceObj.getLineItemValue('item','grossamt',t);
							a_JV_Array[i_JVcnt++] = i_InvId + '##'+ i_newInvoiceCustomer +'##'+ i_invoiceVendor + '##' + f_totalAmount+'##'+ i_invoiceStdPeriod +'##'+ i_subsidiary +'##'+ i_invItem +'##'+ i_invRate;
							var i_invLineBillid = o_newInvoiceObj.getLineItemValue('item','custcol_vendor_bill',t);
							if(a_vendorPay.length==0 && _logValidation(i_invLineBillid))
							{
								a_vendorPay[i_vendorPaycnt++]=i_invLineBillid+'#$##'+i_invRate;
							}
							else if(a_vendorPay.length!=0 && _logValidation(i_invLineBillid))
							{
								var flag=0;
								for(var j=0;a_vendorPay!=null && j<a_vendorPay.length;j++)
								{
									var SplitVendorPay=new Array();
									SplitVendorPay=a_vendorPay[j].split('#$##');
									if(SplitVendorPay[0]==i_invLineBillid)
									{						
										a_vendorPay[j]=a_vendorPay[j]+'#$##'+i_invRate;
										flag=1;
									}										
								} // for j
								if(flag!=1 && _logValidation(i_invLineBillid))
								{
									a_vendorPay[i_vendorPaycnt++]=i_invLineBillid+'#$##'+i_invRate;
								}
							}//else if(a_vendorPay.length!=0)													
						}
						
					}
					for(var c=0;a_vendorPay!=null && c<a_vendorPay.length;c++)
					{
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'a_vendorPay['+c+']= ' + a_vendorPay[c]);
					}
					
					if(_logValidation(i_InvId))
					{
						for(var c=0;a_sourceInvoiceArray!=null && c<a_sourceInvoiceArray.length;c++)
						{
							nlapiSubmitField('customrecord_bill', a_sourceInvoiceArray[c], 'custrecord_procesada', 'T');
							////Custom Job Record 
							//var o_wrecord = nlapiLoadRecord('customrecord_job', Jobid);
						}
					}
					
					//Create Journal
					var i_JVID = '';
					if (a_JV_Array != null && a_JV_Array.length !=0)
					{
	                    var o_JVObj = nlapiCreateRecord('journalentry');
	                    //var f_creditTotal = 0;
						//for (var f1 = 0; a_JV_Array != null && f1 < a_JV_Array.length; f1++)
						//{
						//	var a_SplitJV_Array1 = new Array();
						//	a_SplitJV_Array1 = a_JV_Array[f1].split('##');
						//	f_creditTotal = parseFloat(f_creditTotal) + parseFloat(a_SplitJV_Array1[3]);
						//}
						
	                    for (var f = 0; a_JV_Array != null && f < a_JV_Array.length; f++)
						{
	                        var a_SplitJV_Array = new Array();
	                        a_SplitJV_Array = a_JV_Array[f].split('##');
							if(f==0)
							{
								o_JVObj.setFieldValue('postingperiod', a_SplitJV_Array[4]);
								nlapiLogExecution('DEBUG', 'AfterSubmit', 'subsidiary in array= ' + a_SplitJV_Array[5]);
								o_JVObj.setFieldValue('subsidiary', a_SplitJV_Array[5]);
								var o_LineItem = o_JVObj.selectNewLineItem('line');
		                        o_JVObj.setCurrentLineItemValue('line', 'account', 1190);
		                        var f_debitAmount = parseFloat(a_SplitJV_Array[3]);
								nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_debitAmount= ' + f_debitAmount);
		                        o_JVObj.setCurrentLineItemValue('line', 'credit', f_debitAmount.toFixed(2));
		                        
								if (_logValidation(a_SplitJV_Array[1])) {
									o_JVObj.setCurrentLineItemValue('line', 'entity', a_SplitJV_Array[1]);
								}
		                        o_JVObj.commitLineItem('line');
							}
	                       
							var o_LineItem = o_JVObj.selectNewLineItem('line');
	                        o_JVObj.setCurrentLineItemValue('line', 'account', 1306);
	                        var f_creditAmount = parseFloat(a_SplitJV_Array[7]);
	                        o_JVObj.setCurrentLineItemValue('line', 'debit', f_creditAmount.toFixed(2));
	                        nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_creditAmount= ' + f_creditAmount);
							if (_logValidation(a_SplitJV_Array[2])) {
								o_JVObj.setCurrentLineItemValue('line', 'entity', i_invoiceVendor);
							}
	                        o_JVObj.commitLineItem('line');
	                    }//for f
	                    
	                    i_JVID = nlapiSubmitRecord(o_JVObj);
	                    nlapiLogExecution('DEBUG', 'AfterSubmit', 'Created Journal= ' + i_JVID);
	                }
					////Custom Job Record
					if(_logValidation(i_InvId) && _logValidation(i_JVID))
					{
						//a_billPaymentArray[i_billJvcnt++] = i_JVID;
						CreateInvoicePayment(i_InvId,i_JVID);
						var wrecord = nlapiLoadRecord('customrecord_job', Jobid);
						for(var c1=0;a_sourceInvoiceArray!=null && c1<a_sourceInvoiceArray.length;c1++)
						{
							var JOBCount=wrecord.getLineItemCount('recmachcustrecord_jcbill_job');
							//nlapiLogExecution('DEBUG', 'AfterSubmit', 'JOB Line Count= ' + JOBCount);
							var flag=0;
							for (var k = 1; JOBCount != null && k <= JOBCount; k++) 
							{											            
				                 var JobId=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_job',k);
				                 var preFactura=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_bill', k); //custrecord_jcbill_bill
				                 //nlapiLogExecution('DEBUG', 'AfterSubmit', 'IF= ' + 'preFactura=' +preFactura+'=='+a_sourceInvoiceArray[c1]+' && '+' JobId='+JobId+' == Jobid='+Jobid);
								 if(preFactura==a_sourceInvoiceArray[c1] && JobId==Jobid)
							     {								 	
									var CustomerId1=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_customer',k);
									if(CustomerId1!=null && CustomerId1!='' && CustomerId1!=undefined)
									wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_customer', k,CustomerId1);
							    	wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_commissioninvoice', k, i_InvId);
							   	    wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_commissionjournal', k, i_JVID);
									wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_processed', k, 'T');
							        //flag=1;
							        //break;
							     }
							 }
							 //setFieldValue('custrecord_job_status',3)
							//if(flag==1)						 
						}
						var SubmitedJobID = nlapiSubmitRecord(wrecord, true);
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'SubmitedJobID= ' + SubmitedJobID);										
					}
					//recmachcustrecord_jbill_liquidation
					if(_logValidation(i_InvId) && _logValidation(i_JVID) && a_vendorPay != null)
					{						
						try
						{
							//a_vendorPay
							for(var k=0;a_vendorPay!=null && k<a_vendorPay.length;k++)
							{
								var a_SplitbillArray = new Array();
	                        	a_SplitbillArray = a_vendorPay[k].split('#$##');
								nlapiLogExecution('DEBUG', 'a_SplitbillArray', '******* a_vendorPay['+k+']='+a_vendorPay[k]);
								var f_totalPFamount = 0;	
								for (var z = 1; z < a_SplitbillArray.length; z++) 
								{
									f_totalPFamount = parseFloat(f_totalPFamount) + parseFloat(a_SplitbillArray[z]);
								}	
								var vendorPaymentObj=nlapiTransformRecord('vendorbill',a_SplitbillArray[0],'vendorpayment');
								var vendorPaymentCount=vendorPaymentObj.getLineItemCount('apply');
								for(var kk=1;vendorPaymentCount!=null && kk<=vendorPaymentCount;kk++)
								{
									var billid=vendorPaymentObj.getLineItemValue('apply','internalid',kk);
									//var JVAmount=vendorPaymentObj.getLineItemValue('apply','due',kk);
									if(billid == i_JVID)
									{
										vendorPaymentObj.setLineItemValue('apply','apply',kk,'T');
									}								
								}					
								for(var kk=1;vendorPaymentCount!=null && kk<=vendorPaymentCount;kk++)
								{
									var billid=vendorPaymentObj.getLineItemValue('apply','internalid',kk);
									//var JVAmount=0;//vendorPaymentObj.getLineItemValue('apply','due',kk);
									if(billid == a_SplitbillArray[0])
									{
										vendorPaymentObj.setLineItemValue('apply','amount',kk,Math.abs(f_totalPFamount));
										break;
									}
																						            									
								}//for
								var VendorPaymentNewId=nlapiSubmitRecord(vendorPaymentObj, false,true);
				        		nlapiLogExecution('DEBUG', 'VendorPaymentNewId', '******* VendorPaymentNewId='+VendorPaymentNewId);
							}
						}
						catch(billException)
						{
							nlapiLogExecution('DEBUG', 'VendorPaymentNewId', '*** Try catch Vendor Payment='+billException);
						}							
																				
					}
				}
				}					
			}
			catch(exception)
			{
				nlapiLogExecution('DEBUG', 'Try-Catch Exception', 'Exception= ' + exception);
			}
			var context=nlapiGetContext();
		    var usage = context.getRemainingUsage();
			nlapiLogExecution('DEBUG', 'Script Usage', 'usage= ' + usage);
			if(usage < 1200)
			{
				nlapiLogExecution('DEBUG', 'Reschedule scripe', 'Reschedule scripe');
				Schedulescriptafterusageexceeded(Jobid);
				b_jobStatusFlag = 1;
				break;
			}			
		}//Main for loop
		if(b_jobStatusFlag != 1 && Jobid != null)
		{
			nlapiSubmitField('customrecord_job', Jobid, 'custrecord_job_status', 3);
			nlapiLogExecution('DEBUG', 'Job Status', 'Job status set as Completado');
		}		
	}//if (a_searchResult != null)
	var context=nlapiGetContext();
    var endUsage = context.getRemainingUsage();
	nlapiLogExecution('DEBUG', 'Script Usage', 'endUsage= ' + endUsage);
}


////=======================Reschedule Code Logic =========================

function Schedulescriptafterusageexceeded(Jobid)
{
     ////Define all parameters to schedule the script for voucher generation.
     var params=new Array();
     params['status']='scheduled';
     params['runasadmin']='T';
     var startDate = new Date();
     params['startdate']=startDate.toUTCString();
     params['custscript_jobidinvoice']=Jobid;	 
     var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);   
     ////If script is scheduled then successfuly then check for if status=queued
     if (status == 'QUEUED') 
     {
     	//nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
     }
}//fun close

function CreateInvoicePayment(CommissionInvoiceId,JVRecordID)
{
     if(CommissionInvoiceId!=null && CommissionInvoiceId!='' && CommissionInvoiceId!=undefined)
		{ 
		  try{
		  	var flag=0;// Journal flag
		  	var CustomerPaymentObj=nlapiTransformRecord('invoice',CommissionInvoiceId,'customerpayment') ; 										
			var CustomerPaymentCount=CustomerPaymentObj.getLineItemCount('credit');
			for(var k=1;CustomerPaymentCount!=null && k<=CustomerPaymentCount;k++)
			{
				var JVID=CustomerPaymentObj.getLineItemValue('credit','internalid',k);
				//nlapiLogExecution('DEBUG', 'CustomerPaymentNewId', '******* JVID='+JVID);				
				if(JVID==JVRecordID)
				{
					CustomerPaymentObj.setLineItemValue('credit','apply',k,'T');
					flag=1;
					break;
				}
			}//for k
			nlapiLogExecution('DEBUG', 'CustomerPaymentNewId', '***flag='+flag);										
			if(flag==1)
			{
				 var CustomerPaymentNewId=nlapiSubmitRecord(CustomerPaymentObj, false,true);
		         nlapiLogExecution('DEBUG', 'CustomerPaymentNewId', '******* CustomerPaymentNewId='+CustomerPaymentNewId);
			}
		   
		  }catch(exception)
		  {
		  	 nlapiLogExecution('DEBUG', 'CustomerPaymentNewId', '*** Try catch Customer Payment='+exception);
		  }
		  
		}//if billid
	
}//fun CreateInvoicePayment close

// BEGIN FUNCTION ===================================================

function _logValidation(value)
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		//value = 0.0;
		return false;
	}
}


// END FUNCTION =====================================================