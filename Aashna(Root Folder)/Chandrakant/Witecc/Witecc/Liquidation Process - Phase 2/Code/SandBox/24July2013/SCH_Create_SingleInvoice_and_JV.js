// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_Create_SingleInvoice_and_JV.js
	Author: Chandrakant Patil
	Company: Aashna Cloud Tech
	Date: 22July2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function Witecc_CreateSingle_InvoiceAnd_JV(type)
{
	var context=nlapiGetContext();
     	
	var Proctype=context.getSetting('SCRIPT','custscript_proctype');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Proctype ==' + Proctype);
	
	var Period=context.getSetting('SCRIPT','custscript_period');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Period ==' + Period);
	
	var Subsidiary=context.getSetting('SCRIPT','custscript_subsidiary');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Subsidiary ==' + Subsidiary);
	
	var customerid=context.getSetting('SCRIPT','custscript_customerid');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'customerid ==' + customerid);
	
	var Jobid=context.getSetting('SCRIPT','custscript_jobidinvoice');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Jobid ==' + Jobid);
	
	var filters=new Array();
	var columns=new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('entity');
	columns[2]= columns[0].setSort();
	//columns[0] = new nlobjSearchColumn('internalid');
	//columns[0] = new nlobjSearchColumn('internalid');
	
	filters.push(new nlobjSearchFilter('mainline',null,'is','T'));
	//filters.push(new nlobjSearchFilter('custbody_commissionprocessed', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custbody_period', null, 'anyof', Period));
	filters.push(new nlobjSearchFilter('custbody_commissioninvoice', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', Subsidiary));
	if(Proctype == 2 && _logValidation(customerid))
	{
		filters.push(new nlobjSearchFilter('entity', null, 'is', customerid));
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'Filter for the Customer');
	}
	var a_searchResult= nlapiSearchRecord('invoice', null, filters, columns);
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult length=' + a_searchResult.length);
	var InvArray = new Array();
	var cnt = 0;
	if (a_searchResult != null && _logValidation(Jobid)) 
	{
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult length=' + a_searchResult.length);
		
		//for loop for Invoices		
		for (var k = 0; a_searchResult != null && k < a_searchResult.length; k++) 
		{
			//nlapiLogExecution('DEBUG', 'columnLen == ');		
			var s_result = a_searchResult[k];
			// return all columns associated with this search
			var a_columns = s_result.getAllColumns();
			
			var s_column0 = a_columns[0];
			var s_invoiceID = s_result.getValue(s_column0);
		    nlapiLogExecution('DEBUG', 'CustomerSearch', 's_invoiceID['+k+']=' + s_invoiceID);
			
			var s_column1 = a_columns[1];
			var s_CustomerName = s_result.getValue(s_column1);
		    nlapiLogExecution('DEBUG', 'CustomerSearch', 's_CustomerName['+k+']=' + s_CustomerName);
			
			if(InvArray.length==0)
			{
				InvArray[cnt++]=s_CustomerName+'#$'+s_invoiceID;
			}
			else if(InvArray.length!=0)
			{														
				var flag=0;
				for(var j=0;InvArray!=null && j<InvArray.length;j++)
				{
					var SplitInvArray=new Array();
					SplitInvArray=InvArray[j].split('#$');
					if(SplitInvArray[0]==s_CustomerName)	
					{						
						InvArray[j]=InvArray[j]+'#$'+s_invoiceID;
						flag=1;
					}										
				} // for j
				if(flag!=1)
				{
					InvArray[cnt++]=s_CustomerName+'#$'+s_invoiceID;
				}
			}//else if(InvArray.length!=0)			
		}
		
		//Get the data from the invoice and create the single Invoice
		for(var d=0;d<InvArray.length;d++)
		{
			nlapiLogExecution('DEBUG', 'InvArray', 'InvArray['+d+']=' + InvArray[d]);
			
			var a_SplitInvArray=new Array();
			a_SplitInvArray=InvArray[d].split('#$');
			var a_singleInvData = new Array();
			var i_count = 0;
			var a_JV_Array = new Array();
			var i_JVcnt = 0;
			//var f_totalAmount = 0;
			var a_sourceInvoiceArray = new Array();
			var s_invoiceData = '';
			var i_invoiceVendor = '';
			for(var j=1;a_SplitInvArray!=null && j<a_SplitInvArray.length;j++)
			{
				// If condition to check that customes has more than 1 invoice
				if(a_SplitInvArray.length > 2)
				{
					var o_invoiceObj = nlapiLoadRecord('invoice', a_SplitInvArray[j]);
					a_sourceInvoiceArray.push(a_SplitInvArray[j]);
					if(o_invoiceObj!=null)
					{						
						var i_invoicePeriod = o_invoiceObj.getFieldValue('custbody_period');
						//newly added
						var i_invoiceStdPeriod = o_invoiceObj.getFieldValue('postingperiod');
						//=============================
						var i_invVendor = o_invoiceObj.getFieldValue('custbody_commissionvendor');
						if(_logValidation(i_invVendor))
						{
							i_invoiceVendor = i_invVendor;
						}
						var i_location = o_invoiceObj.getFieldValue('location');
						var i_subsidiary = o_invoiceObj.getFieldValue('subsidiary');
						var i_InvoiceAmount = o_invoiceObj.getFieldValue('total');
						//f_totalAmount = parseFloat(f_totalAmount) + parseFloat(i_InvoiceAmount);
						a_JV_Array[i_JVcnt++] = a_SplitInvArray[j] + '##'+ a_SplitInvArray[0] +'##'+ i_invoiceVendor + '##' + i_InvoiceAmount+'##'+ i_invoiceStdPeriod +'##'+ i_subsidiary;
						s_invoiceData = a_SplitInvArray[0] +'#$%'+ i_invoicePeriod +'#$%'+ i_location +'#$%'+ i_invoiceVendor;
						//a_singleInvData[i_count++] = a_SplitInvArray[0] +'%%'+ i_location
						var i_lineCount = o_invoiceObj.getLineItemCount('item');
						for(a=1;a<=i_lineCount;a++)
						{
							var i_item = o_invoiceObj.getLineItemValue('item','item',a);
							var i_quantity = o_invoiceObj.getLineItemValue('item','quantity',a);
							var i_rate = o_invoiceObj.getLineItemValue('item','rate',a);
							//var i_item = o_invoiceObj.getLineItemValue('item','item',a);
							a_singleInvData[i_count++] = i_item +'%%'+ i_quantity +'%%'+ i_rate;
							nlapiLogExecution('DEBUG', 'InvArray', 'a_singleInvData['+a+']=' + a_SplitInvArray[0] +'%%'+ i_location +'%%'+ i_item +'%%'+ i_quantity +'%%'+ i_rate);
						}
					}
				}									
			}
			if(a_singleInvData != null)
			{
				var o_newInvoiceObj = nlapiCreateRecord('invoice');
				var a_SplitInvData=new Array();
				a_SplitInvData = s_invoiceData.split('#$%');
				nlapiLogExecution('DEBUG', 'InvArray', 's_invoiceData= ' + s_invoiceData);
				if(_logValidation(a_SplitInvData[0]))
				{					
					o_newInvoiceObj.setFieldValue('entity', a_SplitInvData[0]);
				}
				if(_logValidation(a_SplitInvData[1]))
				{
					o_newInvoiceObj.setFieldValue('custbody_period', a_SplitInvData[1]);
				}
				if(_logValidation(a_SplitInvData[2]))
				{
					o_newInvoiceObj.setFieldValue('location', a_SplitInvData[2]);
				}
				if(_logValidation(a_SplitInvData[3]))
				{
					o_newInvoiceObj.setFieldValue('custbody_commissionvendor', a_SplitInvData[3]);
				}
				
				for(var t=0;a_singleInvData!=null && t<a_singleInvData.length;t++)
				{
					var a_SplitInvData=new Array();
					a_SplitInvData=a_singleInvData[t].split('%%');
					o_newInvoiceObj.setLineItemValue('item','item',(t+1),a_SplitInvData[0]);
					o_newInvoiceObj.setLineItemValue('item','quantity',(t+1),a_SplitInvData[1]);
					o_newInvoiceObj.setLineItemValue('item','rate',(t+1),a_SplitInvData[2]);
				}
				if(_logValidation(a_sourceInvoiceArray))
				{
					o_newInvoiceObj.setFieldValue('custbody_sourceinvoices', a_sourceInvoiceArray);
				}
				o_newInvoiceObj.setFieldValue('custbody_commissioninvoice', 'T');
				o_newInvoiceObj.setFieldValue('custbody_commissionprocessed', 'T');
				var i_InvId = nlapiSubmitRecord(o_newInvoiceObj);
				nlapiLogExecution('DEBUG', 'InvArray', 'Created Invoice =' + i_InvId);
				
				if(_logValidation(i_InvId))
				{
					for(var c=0;a_sourceInvoiceArray!=null && c<a_sourceInvoiceArray.length;c++)
					{
						nlapiSubmitField('invoice', a_sourceInvoiceArray[c], 'custbody_commissionprocessed', 'T');
						////Custom Job Record 
						//var o_wrecord = nlapiLoadRecord('customrecord_job', Jobid);
					}					
				}
				
				//Create Journal
				if (a_JV_Array != null && a_JV_Array.length !=0) 
				{
                    var o_JVObj = nlapiCreateRecord('journalentry');
                    var f_creditTotal = 0;
					for (var f1 = 0; a_JV_Array != null && f1 < a_JV_Array.length; f1++) 
					{
						var a_SplitJV_Array1 = new Array();
						a_SplitJV_Array1 = a_JV_Array[f1].split('##');
						f_creditTotal = parseFloat(f_creditTotal) + parseFloat(a_SplitJV_Array1[3]);
					}
					
                    for (var f = 0; a_JV_Array != null && f < a_JV_Array.length; f++) 
					{
                        var a_SplitJV_Array = new Array();
                        a_SplitJV_Array = a_JV_Array[f].split('##');
						if(f==0)
						{							
							o_JVObj.setFieldValue('postingperiod', a_SplitJV_Array[4]);
							o_JVObj.setFieldValue('subsidiary', a_SplitJV_Array[5]);
							var o_LineItem = o_JVObj.selectNewLineItem('line');
	                        o_JVObj.setCurrentLineItemValue('line', 'account', 1190);
	                        var f_debitAmount = parseFloat(f_creditTotal);
	                        o_JVObj.setCurrentLineItemValue('line', 'credit', f_debitAmount.toFixed(2));
	                        
							if (_logValidation(a_SplitJV_Array[1])) {
								o_JVObj.setCurrentLineItemValue('line', 'entity', a_SplitJV_Array[1]);
							}						                     
	                        o_JVObj.commitLineItem('line');
						}
                       
						var o_LineItem = o_JVObj.selectNewLineItem('line');
                        o_JVObj.setCurrentLineItemValue('line', 'account', 1306);
                        var f_creditAmount = parseFloat(a_SplitJV_Array[3]);
                        o_JVObj.setCurrentLineItemValue('line', 'debit', f_creditAmount.toFixed(2));
                        
						if (_logValidation(a_SplitJV_Array[2])) {
							o_JVObj.setCurrentLineItemValue('line', 'entity', i_invoiceVendor);
						}						                     
                        o_JVObj.commitLineItem('line');                        
                    }
                    
                    var i_JVID = nlapiSubmitRecord(o_JVObj);
                    nlapiLogExecution('DEBUG', 'AfterSubmit', 'Created Journal= ' + i_JVID);                    
                }
				////Custom Job Record
				if(_logValidation(i_InvId) && _logValidation(i_JVID))
				{
					var wrecord = nlapiLoadRecord('customrecord_job', Jobid);
					for(var c1=0;a_sourceInvoiceArray!=null && c1<a_sourceInvoiceArray.length;c1++)
					{
						var JOBCount=wrecord.getLineItemCount('recmachcustrecord_jcbill_job');
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'JOB Line Count= ' + JOBCount);
						var flag=0;
						for (var k = 1; JOBCount != null && k <= JOBCount; k++) 
						{
										            
			                 var JobId=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_job',k);
			                 var invoiceId=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_bill', k); //custrecord_jcbill_bill
			                 nlapiLogExecution('DEBUG', 'AfterSubmit', 'IF= ' + 'invoiceId=' +invoiceId+'=='+a_sourceInvoiceArray[c1]+' && '+' JobId='+JobId+' == Jobid='+Jobid);
							 if(invoiceId==a_sourceInvoiceArray[c1] && JobId==Jobid)
						     {
							 	
								var CustomerId1=wrecord.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_customer',k);
								if(CustomerId1!=null && CustomerId1!='' && CustomerId1!=undefined)
								wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_customer', k,CustomerId1);
						    	 wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_commissioninvoice', k,i_InvId);
						   	     wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_commissionjournal', k,i_JVID);
						         //flag=1;
						         //break;
						     }  						            
						 }
						 //setFieldValue('custrecord_job_status',3)
						//if(flag==1)						 
					}
					var SubmitedJobID = nlapiSubmitRecord(wrecord, true);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'SubmitedJobID= ' + SubmitedJobID);										
				}
			}
		}//Main for loop
		if(Jobid!=null)
		nlapiSubmitField('customrecord_job',Jobid,'custrecord_job_status',3);
	}//if (a_searchResult != null)
}







// BEGIN FUNCTION ===================================================

function _logValidation(value) 
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) 
	{
		return true;
	} 
	else 
	{
		//value = 0.0;
		return false;
	}
}


// END FUNCTION =====================================================