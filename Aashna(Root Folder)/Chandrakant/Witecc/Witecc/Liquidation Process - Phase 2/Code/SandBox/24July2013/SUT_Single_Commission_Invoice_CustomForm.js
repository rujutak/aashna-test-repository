/*24072013
* Script File  : TIAmer_Commission_Form_SLV.js
* Script Type  : Suitelet
* Description  : Launcher El Salvador's Commission Process
* Author       : Gerardo A. Luna
* Created      : 13-09-2012
* Event        : GET, POST
* Main Method  : displayform
* ScriptId     : customdeploy_commission_form_slv
* DeploymentId : customdeploy_commission_form_slv
*/

var PROC_TYPE_MASIVE = 1;
var PROC_TYPE_VENDOR = 2;

var NUM_DEPLOYMENTS_SCHEDULED_SCRIPT = 5;

var TRACKING_LIQ_LIQUIDATION = 2;

//---------------------
// MÃƒÂ¯Ã‚Â¿Ã‚Â½todo principal ---
//---------------------
function displayform(request, response) {

    var subsidiaryid = request.getParameter('subsidiary');
    var periodid = request.getParameter('period');
    var vendorid = request.getParameter('vendor');
    var proctype = request.getParameter('proctype');
    proctype = (fnIsNull(proctype) == '') ? PROC_TYPE_MASIVE : proctype;
    var trandate = request.getParameter('trandate');
    var tracking = request.getParameter('tracking');

    if (request.getMethod() == "GET") {

        displayCommissionsForm(proctype, periodid, subsidiaryid, vendorid, trandate, tracking);
    }

    if (request.getMethod() == "POST") {
		
		var Proctype=request.getParameter('proctype');	
	    nlapiLogExecution('DEBUG', ' In Suitelet Post  ' ,'Proctype '+ Proctype);
		
		var Period=request.getParameter('period');	
	    nlapiLogExecution('DEBUG', ' In Suitelet Post  ' ,'Period '+ Period);
		
		var Vendorid=request.getParameter('vendor');	
	    nlapiLogExecution('DEBUG', ' In Suitelet Post  ' ,'Vendorid '+ Vendorid);
		
		var Subsidiary=request.getParameter('subsidiary');	
	    nlapiLogExecution('DEBUG', ' In Suitelet Post  ' ,'Subsidiary '+ Subsidiary);

        var periodid = getCustomPeriod(periodid);

        //--> Validate filters
        validateFilters(periodid, subsidiaryid, vendorid);

        //--> Search bills to process
		var results=new Array();
        results = searchBillsToProcess(periodid, subsidiaryid, vendorid); 
        //nlapiLogExecution('DEBUG', ' In Suitelet Post  ' ,'results '+ results.length);
        if (results != null && results.length != 0) 
		{

            subsidiaryid = results[0].getValue('subsidiary');
            //periodid = results[0].getValue('postingperiod');
			periodid = results[0].getValue('custbody_period');
			

            var wid = createJobRecord(results, periodid, subsidiaryid, trandate);
			//var wid = 12;
            var binqueue = false;
			
			var params=new Array();
		    params['status']='scheduled';
		    params['runasadmin']='T';
			params['custscript_proctype']=Proctype;
			params['custscript_period']=periodid;
			params['custscript_subsidiary']=Subsidiary;
			params['custscript_jobidinvoice']=wid;
			params['custscript_customerid']=Vendorid;
			
		    var startDate = new Date();
		    params['startdate']=startDate.toUTCString();//	   
		    //var status=nlapiScheduleScript('customscript_sch_trevco_commission','customdeploy1',params);
		
            for (var j = 1; j <= NUM_DEPLOYMENTS_SCHEDULED_SCRIPT; j++) 
			{
                var wstatus = nlapiScheduleScript('customscript_sch_create_singleinvoice_jv', 'customdeploy1', params);
				
                if (wstatus == "QUEUED") {
                    binqueue = true;
                    break;
                }
            }

            if (binqueue == true) {

                redirectTOProcessList(request, response);
            }
            else {
                throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecuci" + String.fromCharCode(243) + "n, espere a que terminen para iniciar un nuevo proceso!!!", false);
            } 
        }
        else {
            throw nlapiCreateError("ERROR", "No se encontraron registros por procesar!", false);
        }

    }
}


//-------------------------------
//-- Display Liquidations Form --
//-------------------------------
function displayCommissionsForm(proctype, periodid, subsidiaryid, vendorid, trandate, tracking) 
{
    //--> Create Form
    var form = nlapiCreateForm("Facturaci" + String.fromCharCode(243) + "n Comisiones - El Salvador");
    form.setScript('customscript_commission_client_slv');

    //--> Create Radio Buttons: Process Type
    form.addField('lblproctype1', 'label', 'Proceso').setLayoutType('startrow');
    form.getField('lblproctype1').setBreakType('startcol');
    form.addField('proctype', 'radio', 'Masivo', PROC_TYPE_MASIVE).setLayoutType('endrow');
    form.addField('lblproctype2', 'label', '').setLayoutType('startrow');
    form.addField('proctype', 'radio', 'Proveedor', PROC_TYPE_VENDOR).setLayoutType('endrow');
    form.getField('proctype').setDefaultValue(proctype);
    form.addField('lblproctype3', 'label', '').setLayoutType('startrow');

    //--> Create combobox to periods
    field = form.addField('period', 'select', 'Periodo', 'accountingperiod', null);
    field.setDefaultValue(periodid);
    field.setBreakType('startrow');
    field.setMandatory(true);

    //--> Create combobox to subsidiaries
    field = form.addField('subsidiary', 'select', 'Subsidiaria', 'subsidiary', null);
    field.setDefaultValue(subsidiaryid);
    field.setBreakType('startrow');
    field.setMandatory(true);

    //--> Create Date Field
    field = form.addField("trandate", "date", "Fecha Facturaci" + String.fromCharCode(243) + "n", null);
    var date = nlapiDateToString(new Date());
    trandate = (fnIsNull(trandate) == '' ? date : trandate);
    field.setDefaultValue(trandate);
    field.setBreakType('startrow');
    field.setMandatory(true);
    
    ////--> Create Tracking Field
    //field = form.addField("tracking", "select", "Seguimiento", 'customlist_tipoproceso');
    //field.setDefaultValue(tracking);
    //field.setBreakType('startrow');
    //field.setMandatory(true);    
    
    //--> Create textbox to vendor
    var disabled = (proctype == PROC_TYPE_MASIVE) ? 'disabled' : 'normal';
    //field = form.addField('vendor', 'select', 'Proveedor', 'vendor', null);
	field = form.addField('vendor', 'select', 'Proveedor', 'customer', null);
    field.setDefaultValue(vendorid);
    field.setBreakType('startrow');
    field.setDisplayType(disabled);    

    //--> Add buttons to form
    form.addSubmitButton('Procesar');

    //--> Response to client
    response.writePage(form);
}

function redirectTOProcessList(request, response) {
    var host = request.getHeader("HOST");
    var url = nlapiResolveURL('RECORD', 'customrecord_job', null, null);
    url = url.replace(/custrecordentry.nl/g, 'custrecordentrylist.nl');
    url = "https://" + host + url;
    var html = "";
    html += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
    html += "<html>";
    html += "<head>";
    html += "<title>Redirecting...</title>";
    html += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">";
    html += "</head>";
    html += "<script>";
    html += "window.location = \"" + url + "\";";
    html += "<\/script>";
    html += "<body>";
    html += "</body>";
    html += "</html>";
    response.write(html);
}

function searchBillsToProcess(period, subsidiary, vendor) {

    var index = 0;

    var filters = new Array();

    if (fnIsNull(subsidiary) != '') {

        filters[index] = new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary);
        index++;
    }

    if (fnIsNull(vendor) != '') {

        filters[index] = new nlobjSearchFilter('entity', null, 'anyof', vendor);
        index++;
    }

    if (fnIsNull(period) != '') {
        filters[index] = new nlobjSearchFilter('custbody_period', null, 'anyof', period);
        index++;
    }

    //filters[index] = new nlobjSearchFilter('custbody_chkbilledcommission', null, 'is', 'F'); //Facturas no procesadas
    //index++;
	
	filters[index] = new nlobjSearchFilter('custbody_commissionprocessed', null, 'is', 'F'); //Facturas no procesadas
    index++;
	
	
    filters[index] = new nlobjSearchFilter('mainline', null, 'is', 'T');
    index++;
    //filters[index] = new nlobjSearchFilter('custbody_medios', null, 'is', 'T'); //Facturas de medios
    //index++;
    //filters[index] = new nlobjSearchFilter('custbody_trackliquidation', null, 'anyof', TRACKING_LIQ_LIQUIDATION); //Seguimiento de liquidaciÃƒÂ¯Ã‚Â¿Ã‚Â½n
    //index++;
	filters[index] = new nlobjSearchFilter('custbody_commissioninvoice', null, 'is', 'T');
    index++;

    var columns = new Array();
    columns[columns.length] = new nlobjSearchColumn('internalid');
    columns[columns.length] = new nlobjSearchColumn('tranid');
    //columns[columns.length] = new nlobjSearchColumn('entity');	
    columns[columns.length] = new nlobjSearchColumn('postingperiod');
    columns[columns.length] = new nlobjSearchColumn('subsidiary');
    columns[columns.length] = new nlobjSearchColumn('currency');
    columns[columns.length] = new nlobjSearchColumn('amount');
    //columns[7] = new nlobjSearchColumn('fxamount');
    //columns[8] = new nlobjSearchColumn('custbody_referenciafactura');
    //columns[9] = new nlobjSearchColumn('custbody_customer');
    columns[columns.length] = new nlobjSearchColumn('custbody_period');
    columns[columns.length] = new nlobjSearchColumn('location');
	columns[columns.length] = new nlobjSearchColumn('entity');
	columns[columns.length]= columns[columns.length].setSort();

    var results = nlapiSearchRecord('invoice', null, filters, columns);

    return results;
}

function createJobRecord(results, periodid, subsidiaryid, trandate) {

    var wid = null;

    if (results != null && results.length != 0) {

        //--> Create Record Job
        var wrecord = nlapiCreateRecord("customrecord_job");
        wrecord.setFieldValue("custrecord_job_subsidiary", subsidiaryid);
        wrecord.setFieldValue("custrecord_job_period", periodid);
        wrecord.setFieldValue("custrecord_job_type", PROCESS_TYPE_BILLCOMMISSIONS);
        wrecord.setFieldValue("custrecord_job_liqdate", trandate);

        wid = nlapiSubmitRecord(wrecord, true);

        wrecord = nlapiLoadRecord('customrecord_job', wid);

        for (var i = 0; results != null && i < results.length; i++) {

            var wbill       = results[i].getValue('internalid');
            var wvendor     = results[i].getValue('entity');
            var wsubsidiary = results[i].getValue('subsidiary');
            var wperiod     = results[i].getValue('custbody_period');
            var wamount     = results[i].getValue('amount');
            //var wfxamount   = results[i].getValue('fxamount');
            //var wpauta      = results[i].getValue('custbody_referenciafactura');
            //var wcustomer   = results[i].getValue('custbody_customer');
            var wcurrency = results[i].getValue('currency');
            var wlocation = results[i].getValue('location');
            
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_bill', (i + 1), wbill); //custrecord_jcbill_bill
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_job', (i + 1), wid);
            //wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_vendor', (i + 1), wvendor);
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_customer', (i + 1), wvendor);
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_period', (i + 1), wperiod);
            //wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_pauta', (i + 1), wpauta);
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_amount', (i + 1), wamount);
            //wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_fxamount', (i + 1), wfxamount);
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_currency', (i + 1), wcurrency);
            wrecord.setLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_location', (i + 1), wlocation);

        }

        nlapiSubmitRecord(wrecord, true);
    }

    return wid;
}

function validateFilters(periodid, subsidiaryid, vendorid){

    var retval = true;

    var msg = '';

    if (fnIsNull(periodid) == '') {
        msg += 'periodo,';
    }

    if (fnIsNull(periodid) != '' && fnIsNull(subsidiaryid) == '' && fnIsNull(vendorid) == '') {
        msg += 'subsidiaria o proveedor,';
    }

    if (msg.length > 0) {

        msg = msg.substring(0, msg.length - 1);

        throw nlapiCreateError("ERROR", "Favor de seleccionar: " + msg, false);
    }

    return retval;

}

function getCustomPeriod(period) {

    var retval = null;

    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_period_postingperiod', null, 'is', period);

    var columns = new Array();

    var results = nlapiSearchRecord('customrecord_period', null, filters, columns);

    if (results != null && results.length > 0) {
        retval = results[0].getId();
    }

    return retval;
}


