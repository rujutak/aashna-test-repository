// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SCH_CreatecheckforVendorBill.js
     Author: Rujuta k.
     Company:
     Date:09/08/2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type){
    /*  On scheduled function:
     - PURPOSE
     -This Schedule Script creates Customer Payments for all the Open Invoices
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SCHEDULED FUNCTION CODE BODY
    try 
	{
        var contextObj = nlapiGetContext();
        var fileId_Param = contextObj.getSetting('SCRIPT', 'custscript_vbdetalis');
        nlapiLogExecution('DEBUG', 'Search Results', 'fileId_Param :' + fileId_Param);
        if (_logValidation(fileId_Param)) {
            //	nlapiLogExecution('DEBUG', 'Search Results', 'In Iff loop :' + fileId_Param);
            var split = fileId_Param.toString().split(',');
            nlapiLogExecution('DEBUG', 'Search Results', 'split== :' + split);
            var count = split.length;
            nlapiLogExecution('DEBUG', 'Search Results', 'count== :' + count);
            var details = split[0];
            nlapiLogExecution('DEBUG', 'Search Results', 'details== :' + details);
            var deails_split = details.toString().split('#')
            nlapiLogExecution('DEBUG', 'Search Results', 'deails_split== :' + deails_split);
            var billtranid = deails_split[0];
            nlapiLogExecution('DEBUG', 'Search Results', 'billtranid== :' + billtranid);
            var customerid = deails_split[3];
            nlapiLogExecution('DEBUG', 'Search Results', 'customerid== :' + customerid);
            
            var Paymentcurreny = deails_split[2];
            nlapiLogExecution('DEBUG', 'Search Results', 'Paymentcurreny== :' + Paymentcurreny);
            
            var BillCurrency = deails_split[4];
            nlapiLogExecution('DEBUG', 'Search Results', 'BillCurrency== :' + BillCurrency);
            
            var BankAccount = deails_split[5];
            nlapiLogExecution('DEBUG', 'Search Results', 'BankAccount== :' + BankAccount);
            
            var exchangeRate1 = CalculateExchangeRate(Paymentcurreny, BillCurrency);
            nlapiLogExecution('DEBUG', 'Search Results', 'exchangeRate== :' + exchangeRate1);
            
            var exchangeRate = Math.round(exchangeRate1);
            nlapiLogExecution('DEBUG', 'Search Results', 'exchangeRate== :' + exchangeRate);
            if (exchangeRate <= 0) {
                exchangeRate = 1;
            }
            else {
                exchangeRate = exchangeRate;
            }
            
            var CheckRecord = nlapiCreateRecord('check')
            nlapiLogExecution('DEBUG', 'Search Results', 'CheckRecord== :' + CheckRecord);
			
			CheckRecord.setFieldValue('customform', 104);
            if (_logValidation(customerid)) {
                var entity = CheckRecord.setFieldValue('entity', customerid)
                nlapiLogExecution('DEBUG', 'Search Results', 'entity== :' + entity);
            }
			
			if (_logValidation(BankAccount)) 
			{
                CheckRecord.setFieldValue('account', BankAccount)
                nlapiLogExecution('DEBUG', 'Search Results', 'bankaccount Set== :' + BankAccount);
            }
            
            if (_logValidation(Paymentcurreny)) 
			{
                var currency = CheckRecord.setFieldValue('currency', Paymentcurreny)
                nlapiLogExecution('DEBUG', 'Search Results', 'currency== :' + currency);
                
                
            }
			
            
			//Set To be printed true Cheque
			CheckRecord.setFieldValue('tobeprinted', 'T')
			
            var formcurrency=CheckRecord.getFieldValue('currency')
			nlapiLogExecution('DEBUG', 'Search Results', 'formcurrency == :' + formcurrency);
			
			var formAccount=CheckRecord.getFieldValue('account')
			nlapiLogExecution('DEBUG', 'Search Results', 'formAccount == :' + formAccount);
			
			 
            
            for (var i = 0; i < count; i++) 
			{
                var Checkdetails = split[i];
                nlapiLogExecution('DEBUG', 'Search Results', 'Checkdetails== :' + Checkdetails);
                var amount = Checkdetails.toString().split('#');
                nlapiLogExecution('DEBUG', 'Search Results', 'amount== :' + amount);
                var billtranid_1 = amount[0];
                nlapiLogExecution('DEBUG', 'Search Results', 'billtranid== :' + billtranid);
                var i_billid = searchbill(billtranid_1);
                nlapiLogExecution('DEBUG', 'vendor bill', 'billid==' + i_billid);
                var set_amount = parseFloat(amount[1]) / parseFloat(exchangeRate);
                nlapiLogExecution('DEBUG', 'Search Results', 'set_amount== :' + set_amount);
				
                CheckRecord.selectNewLineItem('expense');
                CheckRecord.setCurrentLineItemValue('expense', 'account', 2381);
                CheckRecord.setCurrentLineItemValue('expense', 'amount', parseFloat(set_amount.toFixed(2)));
                CheckRecord.setCurrentLineItemValue('expense', 'taxcode', 32);
                CheckRecord.setCurrentLineItemValue('expense', 'custcolmulticurrencybill', i_billid);
                CheckRecord.commitLineItem('expense');
            }
            
            try {
            
			var formcurrency1=CheckRecord.getFieldValue('currency')
			nlapiLogExecution('DEBUG', 'Search Results', 'formcurrency1 == :' + formcurrency1);
			
			var formAccount1=CheckRecord.getFieldValue('account')
			nlapiLogExecution('DEBUG', 'Search Results', 'formAccount1 == :' + formAccount1);
                var Check_ID = nlapiSubmitRecord(CheckRecord, true, true);
                nlapiLogExecution('DEBUG', 'Create check', 'Check id==' + Check_ID);
                
                if (Check_ID != null) 
				{
                    for (var i = 0; i < count; i++) 
					{
                        var Checkdetails1 = split[i];
                        var amount1 = Checkdetails1.toString().split('#');
                        nlapiLogExecution('DEBUG', 'Search Results', 'amount== :' + amount1);
                        var billtranid_11 = amount1[0];
					    nlapiLogExecution('DEBUG', 'Search Results', 'billtranid_11== :' + billtranid_11);				
						var i_billid1 = searchbill(billtranid_11);
						nlapiLogExecution('DEBUG', 'Search Results', 'i_billid1== :' + i_billid1);	
					
                        var id = CreateCustomRecord(Check_ID, customerid, parseFloat(set_amount.toFixed(2)), i_billid1)
                        nlapiLogExecution('DEBUG', 'Create check', 'Check id==' + id);
						
						
                      
                    }
                    nlapiLogExecution('DEBUG', 'Create check', 'Check id==' + Check_ID + "*******" + "  bill tranid_1==  " + billtranid_1);
                    createJV(Check_ID, billtranid_1)
                }
            } 
            
            catch (e1) {
                var message = e1.message;
                nlapiLogExecution('DEBUG', 'Create check', 'message=' + message);
            }
        }
    } 
    catch (e) {
    
    }
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined) {
        return true;
    }
    else {
        return false;
    }
}

function CalculateExchangeRate(PaymentCurrency, billCurrecy){
    var todaydate = new Date();
    nlapiLogExecution('DEBUG', 'Create check', 'todaydate=' + todaydate);
    
    var date = getTodaysDate(todaydate);
    
    nlapiLogExecution('DEBUG', 'Create check', 'date=' + date);
    
    if (_logValidation(billCurrecy) && _logValidation(PaymentCurrency)) {
        var rate = nlapiExchangeRate(PaymentCurrency, billCurrecy, date)
        nlapiLogExecution('DEBUG', 'Create check', 'rate=' + rate);
        
    }
    return rate;
}

function getTodaysDate(todaydate){
    var day = todaydate.getDate();
    
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranday==' + day);
    var month = todaydate.getMonth() + 1;
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranmonth==' + month);
    var year = todaydate.getFullYear();
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranyear==' + year);
    
    //Set the date format in MM/DD/YYYY
    var date1 = day + '/' + month + '/' + year;
    nlapiLogExecution('DEBUG', 'Schedule Script', 'date1==' + date1);
    
    
    
    //To convert to UTC datetime by subtracting the current Timezone offset
    
    return date1
}


function createJV(Check_ID, billtranid){

    var o_checkobj = nlapiLoadRecord('check', Check_ID);
    var i_subsidiary = o_checkobj.getFieldValue('subsidiary');
    
    var i_entity = o_checkobj.getFieldValue('entity');
    var i_currency = o_checkobj.getFieldValue('currency');
    var i_linecount = o_checkobj.getLineItemCount('expense');
    var i_billid = searchbill(billtranid);
    nlapiLogExecution('DEBUG', 'vendor bill', 'billid==' + i_billid);
    var o_vbillobj = nlapiLoadRecord('vendorbill', i_billid);
    var i_account = o_vbillobj.getFieldValue('account');
    nlapiLogExecution('DEBUG', 'vendor bill', 'account==' + i_account);
    var i_currency = o_vbillobj.getFieldValue('currency');
    
    for (var i_i = 1; i_i <= i_linecount; i_i++) {
        var i_amount = o_checkobj.getLineItemValue('expense', 'amount', i_i);
        var billinternailid = o_checkobj.getLineItemValue('expense', 'custcolmulticurrencybill', i_i);
        var jvObject = nlapiCreateRecord('journalentry');
        jvObject.setFieldValue('subsidiary', i_subsidiary);
        jvObject.setFieldValue('currency', i_currency);
        //jvObject.setFieldValue('custbodyrefmulticurrency', billtranid);
        
        jvObject.selectNewLineItem('line');
        jvObject.setCurrentLineItemValue('line', 'account', i_account);
        jvObject.setCurrentLineItemValue('line', 'debit', i_amount);
        jvObject.setCurrentLineItemValue('line', 'credit', 0.0);
        jvObject.setCurrentLineItemValue('line', 'entity', i_entity);
        jvObject.commitLineItem('line');
        
        jvObject.selectNewLineItem('line');
        jvObject.setCurrentLineItemValue('line', 'account', 2381);
        jvObject.setCurrentLineItemValue('line', 'debit', 0.0);
        jvObject.setCurrentLineItemValue('line', 'credit', i_amount);
        jvObject.commitLineItem('line');
        
        try {
            var i_JVID = nlapiSubmitRecord(jvObject, true, true)
            nlapiLogExecution('DEBUG', 'New JV', 'JV ID==' + i_JVID);
            
            if (i_JVID != null) {
            
                createPayment(i_JVID, billinternailid)
            }
            
        } 
        
        catch (e1) {
            var message = e1.message;
            nlapiLogExecution('DEBUG', 'Create check', 'message=' + message);
            
            
        }
    }
}

function searchbill(tranid){

    if (_logValidation(tranid)) {
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var filters = new Array();
        filters.push(new nlobjSearchFilter('tranid', null, 'is', tranid));
        //filters.push(new nlobjSearchFilter('mainline',null,'is','T'));
        
        var results = nlapiSearchRecord('vendorbill', null, filters, columns);
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'searchProject', 'Results Length :' + results.length);
            
            return results[0].getValue('internalid');
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

function createPayment(jvid, billid){
    nlapiLogExecution('DEBUG', 'JV', 'jvid --------' + jvid);
    nlapiLogExecution('DEBUG', 'JV', 'i_billid -----------' + billid);
    
    var flag = 0;
    var o_jvObject = nlapiLoadRecord('journalentry', jvid);
    var i_jvcurrency = o_jvObject.getFieldValue('currency');
    
    nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'jvcurrency -->' + i_jvcurrency);
    var i_jvrefno = o_jvObject.getFieldValue('tranid');
    nlapiLogExecution('DEBUG', 'JV', 'i_jvrefno -->' + i_jvrefno);
    
    var o_vbillobj = nlapiLoadRecord('vendorbill', billid);
    var i_tranid = o_vbillobj.getFieldValue('tranid');
    nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_tranid -->' + i_tranid);
    
    var i_billcurrency = o_vbillobj.getFieldValue('currency');
    nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_billcurrency -->' + i_billcurrency);
    
    try {
        var o_paymentobj = nlapiTransformRecord('vendorbill', billid, 'vendorpayment', {
            recordmode: 'dynamic'
        })
        o_paymentobj.setFieldValue('currency', i_billcurrency);
        nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'o_paymentobj -->' + o_paymentobj);
        var setCurrency = o_paymentobj.getFieldValue('currency')
        nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'setCurrency -->' + setCurrency);
        
        
    } 
    catch (e2) {
        var message = e2.message;
        nlapiLogExecution('DEBUG', 'Create Payement', 'message=' + message);
        nlapiLogExecution('DEBUG', 'Create Payement', 'message=' + e2.getCode());
    }
    
    
    var i_paylinecount = o_paymentobj.getLineItemCount('apply');
    nlapiLogExecution('DEBUG', 'payment', 'i_refno length-->' + i_paylinecount);
    
    //o_paymentobj.setFieldValue('currency',i_billcurrency);
    
    for (var i_i = 1; i_i <= i_paylinecount; i_i++) {
        //var b_applyvalue = o_paymentobj.getLineItemValue('apply', 'apply', i_i);
        //nlapiLogExecution('DEBUG', 'payment', 'b_applyvalue -->' + b_applyvalue);
        
        var i_refno = o_paymentobj.getLineItemValue('apply', 'refnum', i_i);
        nlapiLogExecution('DEBUG', 'payment', 'i_refno -->' + i_refno);
        
        if (i_refno == i_tranid) {
            nlapiLogExecution('DEBUG', 'payment', 'i_refno == i_tranid' + i_refno + "==" + i_tranid);
            //o_paymentobj.setLineItemValue('apply', 'apply', i_i, 'T');
            o_paymentobj.selectLineItem('apply', i_i);
            o_paymentobj.setCurrentLineItemValue('apply', 'apply', 'T')
            flag = 1;
            var b_applyvalue1 = o_paymentobj.getLineItemValue('apply', 'apply', i_i);
            nlapiLogExecution('DEBUG', 'payment', 'b_applyvalue1 -->' + b_applyvalue1 + '*******i_i************' + i_i);
            
        }
        if (i_refno == i_jvrefno) {
            nlapiLogExecution('DEBUG', 'payment', 'i_refno==i_jvrefno' + i_refno + "==" + i_jvrefno);
            //o_paymentobj.setLineItemValue('apply', 'apply', i_i, 'T');
            o_paymentobj.selectLineItem('apply', i_i);
            o_paymentobj.setCurrentLineItemValue('apply', 'apply', 'T')
            flag = 1;
            var b_applyvalue1 = o_paymentobj.getLineItemValue('apply', 'apply', i_i);
            nlapiLogExecution('DEBUG', 'payment', 'b_applyvalue1 -->' + b_applyvalue1 + '*******i_i********' + i_i);
            
        }
        
    }
    try {
        if (flag == 1) {
            var i_PaymentID = nlapiSubmitRecord(o_paymentobj, true, true)
            nlapiLogExecution('DEBUG', 'New payment', 'payment ID==' + i_PaymentID);
        }
    } 
    catch (e1) {
        var message = e1.message;
        nlapiLogExecution('DEBUG', 'New payment', 'message=' + message);
    }
    
    
}

function CreateCustomRecord(CheckId, Payee, amount, billtranid){
    var custRecordObj = nlapiCreateRecord('customrecord_multicurrencypayments');
    nlapiLogExecution('DEBUG', 'CreateCustomRecord', 'custRecordObj :' + custRecordObj);
    
    custRecordObj.setFieldValue('custrecord_checkinternalid', CheckId);
    custRecordObj.setFieldValue('custrecord_payee', Payee);
    custRecordObj.setFieldValue('custrecord_amount', amount);
    custRecordObj.setFieldValue('custrecord_vendorbillnumber', billtranid);
    
    
    
    var id = nlapiSubmitRecord(custRecordObj, false, true);
    nlapiLogExecution('DEBUG', 'New Custom Record', 'New Record ID :' + id);
    
    return id;
    
}

// END FUNCTION =====================================================
