// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SUT_Multiple_Currency
     Author:Rujuta Karandikar.
     Company:Aashna cloutech PVT Ltd.
     Date:8 August 2013
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function getVendorBill(request, response)
{
    var return_value = null;
    
    if (request.getMethod() == 'GET') 
	{
        var form = nlapiCreateForm('Pago de Proveedores Multimoneda');
		
        var vendorname = ValidateValue(request.getParameter('custpage_vendor'));
        //nlapiLogExecution('DEBUG', 'Search results ', ' custpage_vendor =' + vendorname)
        var BillCurrency = ValidateValue(request.getParameter('custpage_billcurrency'));
        nlapiLogExecution('DEBUG', 'Search results ', ' BillCurrency =' + BillCurrency)
        var todate1 = ValidateValue(request.getParameter('custpage_todate'));
        //nlapiLogExecution('DEBUG', 'Search results ', ' todate1 =' + todate1)
        var fromdate1 = ValidateValue(request.getParameter('custpage_fromdate'));
        //nlapiLogExecution('DEBUG', 'Search results ', ' fromdate1 =' + fromdate1)
        var paymentcurrency1 = ValidateValue(request.getParameter('custpage_paymentcurrency'));
         nlapiLogExecution('DEBUG', 'Search results ', ' paymentcurrency =' + paymentcurrency1)
        var operator3 = ValidateValue(request.getParameter('custpage_bankaccount'));
        var vendorbillobj = nlapiCreateRecord('vendorbill');
        var a_results_new = new Array();
        
        var VendorSublist = form.addSubList('vendorbilllist', 'list', 'Detalle de facturas', 'tab1');
        
        var oldvalues = false;
        
        if (vendorname == '' && BillCurrency == '') {
            oldvalues = false;
        }
        else {
            oldvalues = true;
        }
        
        var vendor = form.addField('custpage_vendor', 'select', 'Proveedor:', 'vendor');
        vendor.setMandatory(true);
        
        if (oldvalues) {
            vendor.setDefaultValue(vendorname);
        }
        else {
            vendor.setDefaultValue('');
        }
        
        
        var FromDate = form.addField('custpage_fromdate', 'date', 'Fecha desde');
        FromDate.setMandatory(true);
        if (oldvalues) {
            FromDate.setDefaultValue(fromdate1);
        }
        else {
            FromDate.setDefaultValue('');
        }
        
        
        // var Bankaccount = form.addField('custpage_bankaccount', 'select', 'Check Bank Account :', 'account');
        
        
        
        
        var myFld = vendorbillobj.getField('currency');
        var options = myFld.getSelectOptions();
        var select1 = form.addField('custpage_paymentcurrency', 'select', 'Moneda del cheque:');
        for (var i = 0; i < options.length; i++) 
		{
            if (paymentcurrency1 == options[i].getId() && oldvalues) 
			{
                select1.addSelectOption(options[i].getId(), options[i].getText(), true);
            }
            else 
			{
                select1.addSelectOption(options[i].getId(), options[i].getText());
            }
        }
        select1.setMandatory(true);
		
           var exchangeRate=form.addField('custpage_exangerate','text','ExchangeRate');
		   if(oldvalues)
		   {
		   	nlapiLogExecution('DEBUG', 'exchangeRate results ', ' paymentcurrency1 =' + paymentcurrency1)
			nlapiLogExecution('DEBUG', 'exchangeRate results ', ' billCurrency =' + billCurrency)
		   	var rate=CalculateExchangeRate(paymentcurrency1,BillCurrency)
			nlapiLogExecution('DEBUG', 'Search results ', ' rate =' + rate)
			exchangeRate.setDefaultValue(parseFloat(rate));
			
		   }
		   else
		   {
		   	exchangeRate.setDefaultValue('');
		   }
        
        var myFld1 = vendorbillobj.getField('currency');
        var options1 = myFld1.getSelectOptions();
        var select = form.addField('custpage_billcurrency', 'select', 'Moneda factura de Proveedor:');
        for (var i = 0; i < options1.length; i++) {
        
            if ((parseInt(BillCurrency) == parseInt(options1[i].getId())) && oldvalues) 
			{
                select.addSelectOption(options1[i].getId(), options1[i].getText(), true);
            }
            else 
			{
            
                select.addSelectOption(options1[i].getId(), options1[i].getText());
            }
            
        }
        select.setMandatory(true);
		
		
		  
        var ToDate = form.addField('custpage_todate', 'date', 'Fecha hasta');
        ToDate.setMandatory(true);
        if (oldvalues) {
            ToDate.setDefaultValue(todate1);
        }
        else {
            ToDate.setDefaultValue('');
        }
        var select3 = form.addField('custpage_bankaccount', 'select', 'Cuenta bancaria:');
		select3.setMandatory(true);
        var filters = new Array();
        
        filters[0] = new nlobjSearchFilter('type', null, 'is', 'Bank');
        //filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
        
        var a_columns = new Array();
        a_columns[0] = new nlobjSearchColumn('name');
        a_columns[1] = new nlobjSearchColumn('internalid');
        
      
        var searchresults = nlapiSearchRecord('account', null, filters, a_columns);
        nlapiLogExecution('DEBUG', 'Search results ', ' searchresults =' + searchresults.length)
        for (var i = 0; searchresults != null && i < searchresults.length; i++) {
            var id = searchresults[i].getId();
            //var text=searchresults[i].getText();
            //nlapiLogExecution('DEBUG', 'Search results ', ' getText =' + text)
            if (operator3 == id && oldvalues) {
                select3.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('name'), true)
            }
            else {
                select3.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('name'))
            }
        }
        
       
        var field3 = form.addField('checkedsumbit', 'checkbox', 'Mensaje');
        
        field3.setDisplayType('hidden');
        //field3.setDisplayType('inline');
		
		
		
		//exchangeRate.setDisplayType('inline');
		
        if (oldvalues) 
		{
            field3.setDefaultValue('T');
            
        }
        else {
            field3.setDefaultValue('F');
        }
        
        
        
        if (oldvalues) 
		{
            var InvoiceResult = searchInvoice(vendorname, fromdate1, todate1, BillCurrency)
			
			
            nlapiLogExecution('DEBUG', 'InvoiceResult', 'InvoiceResult**************' + InvoiceResult);
            if (InvoiceResult != null) {
                nlapiLogExecution('DEBUG', 'InvoiceResult', 'InvoiceResult length**************' + InvoiceResult.length);
                var columns = InvoiceResult[0].getAllColumns()
                nlapiLogExecution('DEBUG', 'InvoiceResult', 'InvoiceResult**************' + columns);
                return_value = setSubList(VendorSublist, form, 1, InvoiceResult, request, response)
                InvoiceResult = null;
                vendorname = '';
                fromdate1 = '';
                todate1 = '';
                BillCurrency = '';
            }
            else {
                oldvalues = false;
                field3.setDefaultValue('F');
            }
            
        }
        
        
        if (oldvalues) {
            form.addSubmitButton('Crear Pago');
        }
        else {
            form.addSubmitButton('Buscar registros');
        }
        
        response.writePage(form);
    }
    else 
	{
        var params = new Array();
        var customer = request.getParameter('custpage_vendor');
        var fromdate = request.getParameter('custpage_fromdate')
        var todate = request.getParameter('custpage_todate')
        var currency = request.getParameterValues('custpage_billcurrency');
        // PaymentCurrency = request.getParameterValues('custpage_paymentcurrency');
        var PaymentCurrency = GetOperator(ValidateValue(request.getParameter('custpage_paymentcurrency'), 'num'));
        var VendorSublist = request.getParameterValues('vendorbilllist')
        var billCurrency = GetOperator(ValidateValue(request.getParameter('custpage_billcurrency'), 'num'));
        var operator3 = GetOperator(ValidateValue(request.getParameter('custpage_bankaccount'), 'num'));
        
        //operator3=request.getParameterValues('custpage_bankaccount');
        var checkbox = ValidateValue(request.getParameter('checkedsumbit'), 'str1');
        
        var count = request.getLineItemCount('vendorbilllist');
        var checked = request.getParameterValues('custpage_attd');
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'from date' + fromdate);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'from date' + fromdate);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'to date' + todate);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'customer' + customer);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'PaymentCurrency' + PaymentCurrency);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'count**************' + count);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'Bank Account' + operator3);
        nlapiLogExecution('DEBUG', '<searchInvoice>', 'checked**************' + checkbox);
        
       
        
        
        var checkbox_value = ''
        var flag = 0;
        if (checkbox == 'T') {
            var lineItem = new Array();
            for (var i = 1; i <= count; i++) 
			{
                checkbox_value = request.getLineItemValue('vendorbilllist', 'custpage_attd', i);
                nlapiLogExecution('DEBUG', 'sub list count', 'checkbox_value==' + checkbox_value);
                if (checkbox_value == 'T') {
                    var Billname = request.getLineItemValue('vendorbilllist', 'tranid', i)
                    nlapiLogExecution('DEBUG', 'sub list count', 'Billname==' + Billname);
                    var total = request.getLineItemValue('vendorbilllist', 'total', i)
                    nlapiLogExecution('DEBUG', 'sub list count', 'total==' + total);
                    lineItem.push(Billname + "#" + total + "#" + PaymentCurrency + "#" + customer + "#" + billCurrency + "#" + operator3)
                    flag = 1;
                    
                }
                
            }
            // nlapiLogExecution('DEBUG', 'sub list count', 'lineItem==' + lineItem);
            //  params['custpage_vendor'] = customer;
            //  params['custpage_billcurrency'] = currency;
            //  params['custpage_todate'] = todate;
            //   params['custpage_fromdate'] = fromdate;
            //   params['custpage_paymentcurrency'] = PaymentCurrency;
            if (flag == 1) {
                params['custscript_vbdetalis'] = lineItem.toString();
                nlapiLogExecution('DEBUG', 'sub list count', 'lineItem==' + lineItem);
                nlapiSetRedirectURL('SUITELET', 'customscript_sut_multiple_currency', 'customdeploy1', false, params);
                nlapiScheduleScript('customscript_sch_createcheckforvendorbil', 'customdeploycreatecheckforvendorbill', params);
            }
            else 
			{
                nlapiSetRedirectURL('SUITELET', 'customscript_sut_multiple_currency', 'customdeploy1', false, params);
            }
        }
        else 
		{
            params['custpage_vendor'] = customer;
            params['custpage_billcurrency'] = currency;
            params['custpage_todate'] = todate;
            params['custpage_fromdate'] = fromdate;
            params['custpage_paymentcurrency'] = PaymentCurrency;
            params['custpage_bankaccount'] = operator3;
            nlapiSetRedirectURL('SUITELET', 'customscript_sut_multiple_currency', 'customdeploy1', false, params);
            
        }
    }
}

function searchInvoice(customer, fromdate, todate, currency){
    var Flag = 0;
    
    var status_temp = 'Open';
    
    if (fromdate != null && fromdate != '' && fromdate != 'undefined' && todate != null && todate != '' && todate != 'undefined' && customer != null && customer != '' && customer != 'undefined') {
        Flag = 1
    }
    
    
    if (Flag == 1) {
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('name', null, 'is', customer)
        filters[1] = new nlobjSearchFilter('trandate', null, 'onorafter', fromdate)
        filters[2] = new nlobjSearchFilter('trandate', null, 'onorbefore', todate)
        filters[3] = new nlobjSearchFilter('currency', null, 'is', currency)
        filters[4] = new nlobjSearchFilter('mainline', null, 'is', 'T')
        /* var column = new Array();
         column[0] = new nlobjSearchColumn('internalid')
         column[1] = new nlobjSearchColumn('tranid')
         column[2] = new nlobjSearchColumn('entity')
         column[3] = new nlobjSearchColumn('trandate')
         column[4] = new nlobjSearchColumn('usertotal')
         */
        var invoiceResult = nlapiSearchRecord('vendorbill', 'customsearch190', filters, null)
        return invoiceResult;
    }
    
    
    
}

function ValidateValue(value, opr){
    if (value != '' && value != null && value != 'undefined' && value != 'NaN') {
        return value;
    }
    if (opr == 'str1') {
        return 'F';
    }
    else 
        if (opr == 'num') {
            return 0;
        }
    return ''
}

function setSubList(sublist, form, isForm, results, request, response){

    try 
	{
    
    
        if (isForm == 1) {
            if (results != null) {
            
                var SelectCheckBox = sublist.addField('custpage_attd', 'checkbox', 'Seleccione');
                
                var id = sublist.addField('tranid', 'text', 'Id')
                id.setDisplayType('inline');
                var invno = sublist.addField('entity', 'select', 'Vendor', 'Proveedor')
                invno.setDisplayType('inline');
                var Date = sublist.addField('trandate', 'date', 'Fecha');
                Date.setDisplayType('inline');
                var Total = sublist.addField('total', 'text', 'Total');
                Total.setDisplayType('inline');
                
                for (var h = 0; h < results.length; h++) {
                
                    sublist.setLineItemValue('tranid', h + 1, results[h].getValue('tranid'));
                    sublist.setLineItemValue('entity', h + 1, results[h].getValue('entity'));
                    sublist.setLineItemValue('trandate', h + 1, results[h].getValue('trandate'));
                    sublist.setLineItemValue('total', h + 1, parseFloat(results[h].getValue('fxamount')));
                }
                return sublist;
            }
            else {
                var SelectCheckBox = sublist.addField('custpage_attd', 'checkbox', 'Seleccione');
                
                var id = sublist.addField('tranid', 'text', 'Id')
                id.setDisplayType('inline');
                var invno = sublist.addField('entity', 'select', 'Vendor', 'Proveedor')
                invno.setDisplayType('inline');
                var Date = sublist.addField('trandate', 'date', 'Fecha');
                Date.setDisplayType('inline');
                var Total = sublist.addField('total', 'text', 'Total');
                Total.setDisplayType('inline');
                
            }
            
            
            
        }
    } 
    catch (e) {
        nlapiLogExecution('DEBUG', 'added sub list ', 'ERROR' + e);
    }
}

function GetOperator(opr){
    if (opr != null) {
        return opr;
    }
    
    return '';
}

function CalculateExchangeRate(PaymentCurrency, billCurrecy)
{
    var todaydate = new Date();
    nlapiLogExecution('DEBUG', 'Create check', 'todaydate=' + todaydate);
    
    var date = getTodaysDate(todaydate);
    
    nlapiLogExecution('DEBUG', 'Create check', 'date=' + date);
    
    if (_logValidation(billCurrecy) && _logValidation(PaymentCurrency)) 
	{
        var rate = nlapiExchangeRate(PaymentCurrency, billCurrecy, date)
        nlapiLogExecution('DEBUG', 'Create check', 'rate=' + rate);
        
    }
    return rate;
}

function getTodaysDate(todaydate){
    var day = todaydate.getDate();
    
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranday==' + day);
    var month = todaydate.getMonth() + 1;
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranmonth==' + month);
    var year = todaydate.getFullYear();
    nlapiLogExecution('DEBUG', 'Schedule Script', 'tranyear==' + year);
    
    //Set the date format in MM/DD/YYYY
    var date1 = day + '/' + month + '/' + year;
    nlapiLogExecution('DEBUG', 'Schedule Script', 'date1==' + date1);
    
    
    
    //To convert to UTC datetime by subtracting the current Timezone offset
    
    return date1
}
function _logValidation(value){
    if (value != null && value != '' && value != undefined) {
        return true;
    }
    else {
        return false;
    }
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
