// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_LiquidationProcessPhase_3.js
	Author: Chandrakant Patil
	Company: Aashna Cloud Tech
	Date: 3Sept2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

var a_salesOrderData = new Array();
var i_arrcounter = 0;
var a_prebillArray = new Array();
var i_prebillcnt = 0;
function WITECH_LiquidationProcesssPhase3(type)
{
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'In Schedule Script');
	var o_context=nlapiGetContext();
    var usageBegin = o_context.getRemainingUsage();
	nlapiLogExecution('DEBUG', 'Script Usage', 'usageBegin= ' + usageBegin);     	
	var s_Proctype=o_context.getSetting('SCRIPT','custscript_proctypephase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_Proctype ==' + s_Proctype);	
	var s_Period=o_context.getSetting('SCRIPT','custscript_periodphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_Period ==' + s_Period);	
	var s_Subsidiary=o_context.getSetting('SCRIPT','custscript_subsidiaryphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_Subsidiary ==' + s_Subsidiary);		
	var s_tracking=o_context.getSetting('SCRIPT','custscript_trackingphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_tracking ==' + s_tracking);	
	var s_budjet=o_context.getSetting('SCRIPT','custscript_budgetphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_budjet ==' + s_budjet);
	var s_customerid=o_context.getSetting('SCRIPT','custscript_customerphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_customerid ==' + s_customerid);
	var s_currency=o_context.getSetting('SCRIPT','custscript_currencyphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_currency ==' + s_currency);
	var s_liwdate=o_context.getSetting('SCRIPT','custscript_liqdatephase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_liwdate ==' + s_liwdate);
	var s_Jobid=o_context.getSetting('SCRIPT','custscript_jobidphase3');
	nlapiLogExecution('DEBUG', 'schedulerFunction', 's_Jobid ==' + s_Jobid);
	
	//===================================================================================================
	if(_logValidation(s_Jobid))
	{
		var o_wrecordObj = nlapiLoadRecord('customrecord_job', s_Jobid);
		var i_JOBCount=o_wrecordObj.getLineItemCount('recmachcustrecord_jbill_job');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'JOB Line Count= ' + i_JOBCount);
		var b_flag=0;
		var a_searchResult = new Array();
		var i_arraycnt = 0;
		for (var k = 1; i_JOBCount != null && k <= i_JOBCount; k++)
		{
	        //var JobId=o_wrecordObj.getLineItemValue('recmachcustrecord_jcbill_job', 'custrecord_jcbill_job',k);
			var i_CustomerId=o_wrecordObj.getLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_customer',k);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_CustomerId ==' + i_CustomerId);
	        var i_preFactura=o_wrecordObj.getLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_bill', k);
	        nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_preFactura ==' + i_preFactura);
			//var i_budget=o_wrecordObj.getLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_budget', k);
	        //nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_budget ==' + i_budget);
			
	        var b_processed =o_wrecordObj.getLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_processed', k);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'b_processed ==' + b_processed);
			if (b_processed == 'F')
			{
				a_searchResult[i_arraycnt++] = i_CustomerId + '%$%' + i_preFactura;
			}
		}
	}
	
	for (var k = 0; a_searchResult != null && k < a_searchResult.length; k++)
	{
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult['+k+']=' + a_searchResult[k]);
	}
	if (a_searchResult != null && _logValidation(s_Jobid))
	{
		try
		{
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'a_searchResult length=' + a_searchResult.length);
			
			for (var k = 0; a_searchResult != null && k < a_searchResult.length; k++)
			{
				var Splita_searchResult = new Array();
				Splita_searchResult = a_searchResult[k].split('%$%');
				//function call for create Vendor bill and journal entry
				var f_vendorBillID = createVendorBillandJournal(Splita_searchResult[1],s_tracking)
				
			}
		//i_budget+'#$##'+i_customer+'#$##'+prebill+'####'+i_item+'####'+i_quantity+'####'+i_unitcost+'####'+f_amount+'####'+i_description+'####'+i_taxcode+'####'+i_taxamount+'####'+i_total+'####'+i_No_Factura+'####'+i_class+'####'+i_amtsintax+'####'+i_taxamtgcc1+'####'+i_taxamtgcc2+'####'+i_taxcodegcc1+'####'+i_taxcodegcc2+'####'+i_location+'####'+i_brand;		
			//Creating Sales Order
			if(a_salesOrderData != null)
			{
				nlapiLogExecution('DEBUG', 'Sales order', 'in Sales order creation function');
				for(var s=0;s<a_salesOrderData.length;s++)
				{
					nlapiLogExecution('DEBUG', 'Sales order', 'a_salesOrderData['+s+']' + a_salesOrderData[s]);
					var SplitSOData = new Array();
					SplitSOData = a_salesOrderData[s].split('#$##');
					
					var o_salesOrderObj = nlapiCreateRecord('salesorder');
					o_salesOrderObj.setFieldValue('entity', SplitSOData[1]);
					//set Sales Order line item data
					for(var t=2;SplitSOData != null && t<SplitSOData.length;t++)
					{
						var SplitLineSOData = new Array();
						SplitLineSOData = SplitSOData[t].split('####');
						if(_logValidation(SplitLineSOData[16]))
						{
							o_salesOrderObj.setFieldValue('location', SplitLineSOData[16]);
						}					
						o_salesOrderObj.selectNewLineItem('item');
					    o_salesOrderObj.setCurrentLineItemValue('item', 'item', SplitLineSOData[1]);
					    o_salesOrderObj.setCurrentLineItemValue('item', 'quantity', SplitLineSOData[2]);
						o_salesOrderObj.setCurrentLineItemValue('item', 'amount', SplitLineSOData[4]);
					    o_salesOrderObj.setCurrentLineItemValue('item', 'description', SplitLineSOData[5]);
					    o_salesOrderObj.setCurrentLineItemValue('item', 'taxcode', SplitLineSOData[6]);
						o_salesOrderObj.setCurrentLineItemValue('item', 'tax1amt', SplitLineSOData[7]);
						//o_salesOrderObj.setCurrentLineItemValue('item', 'taxcode', SplitLineSOData[8]);
						o_salesOrderObj.setCurrentLineItemValue('item', 'class', SplitLineSOData[10]);
						o_salesOrderObj.setCurrentLineItemValue('item', 'custcol_gcc_prebill', SplitLineSOData[0]);
						o_salesOrderObj.setCurrentLineItemValue('item', 'custcol_prebill_budget', SplitSOData[0]);
						//o_salesOrderObj.setCurrentLineItemValue('item', 'custcolmarca', SplitLineSOData[17]);
						
						o_salesOrderObj.setCurrentLineItemValue('item', 'isclosed', 'T');
						
						//o_salesOrderObj.setCurrentLineItemValue('item', 'taxcode', SplitLineSOData[8]);
						//o_salesOrderObj.setCurrentLineItemValue('item', 'taxcode', SplitLineSOData[8]);
						//o_salesOrderObj.setCurrentLineItemValue('item', 'taxcode', SplitLineSOData[8]);
						o_salesOrderObj.commitLineItem('item');
					}
					var i_soid = nlapiSubmitRecord(o_salesOrderObj,true);
					nlapiLogExecution('DEBUG', 'Sales order', 'Submited Sales Order ==' + i_soid);
				}
				for(var g=0;g<a_prebillArray.length;g++)
				{
					try
					{
						//nlapiSubmitField('customrecord_bill',a_prebillArray[g],'custrecord_procesada','T')
					}
					catch(o_exception4)
					{
						nlapiLogExecution('DEBUG', 'Try - Catch', 'Set status for prebill Try catch Exception= ' + o_exception4);
					}
					
				}
			}
		}
		catch(o_exception)
		{
			nlapiLogExecution('DEBUG', 'Try - Catch', 'Main Try catch Exception= ' + o_exception);
		}			
	}
}


function createVendorBillandJournal(prebill,s_tracking)
{
	nlapiLogExecution('DEBUG', 'createVendorBillandJournalFunction', 'function call for create Vendor bill and journal entry');
	var o_prebillRecord = nlapiLoadRecord('customrecord_bill', prebill);
	
	//GET DATA
	var i_vendor = o_prebillRecord.getFieldValue('custrecord_bill_vendor');
	var i_customer = o_prebillRecord.getFieldValue('custrecord_bill_customer');	
    var i_location = o_prebillRecord.getFieldValue('custrecord_bill_location');
    var i_numref = o_prebillRecord.getFieldValue('custrecord_bill_tranid');
    var i_dateprebill = o_prebillRecord.getFieldValue('custrecord_bill_trandate');
	var i_liquidation = o_prebillRecord.getFieldValue('custrecord_bill_liquidation');
	var i_currency = o_prebillRecord.getFieldValue('custrecord_bill_currency');
	var i_conceptbill = o_prebillRecord.getFieldValue('custrecord_bill_concept');
	var i_budget = o_prebillRecord.getFieldValue('custrecord_bill_pauta');
	var i_period = o_prebillRecord.getFieldValue('custrecord_bill_period');
	var i_subsidiary = o_prebillRecord.getFieldValue('custrecord_bill_subsidiary');
	var o_record = nlapiCreateRecord('vendorbill', { recordmode: 'dynamic' });
	
	//setting values for Vendor bill
	if(_logValidation(i_vendor))
	{
		o_record.setFieldValue('entity', i_vendor);
	}
    if(_logValidation(i_customer))
	{
		o_record.setFieldValue('custbody_customer', i_customer);
	}
    if(_logValidation(i_numref))
	{
		//o_record.setFieldValue('custbody_referenciafactura', i_numref);
	}
    if(_logValidation(i_liquidation))
	{
		o_record.setFieldValue('custbody_gcc_liquidation', i_liquidation);
	}
	if(_logValidation(i_currency))
	{
		o_record.setFieldValue('currency', i_currency);
	}
    o_record.setFieldValue('custbody_medios', 'T');
	if(_logValidation(i_location))
	{
		o_record.setFieldValue('location', i_location);
	}
	if(_logValidation(i_numref))
	{
		o_record.setFieldValue('tranid', i_numref);
	}
	if(_logValidation(i_dateprebill))
	{
		o_record.setFieldValue('trandate', i_dateprebill);
	}
    if(_logValidation(i_budget))
	{
		o_record.setFieldValue('custbody_referenciafactura', i_budget);
	}
	
    var f_prebillAmount = 0;
	for (var i = 1; i <= o_prebillRecord.getLineItemCount('recmachcustrecord_itembill_bill'); i++)
	{
		var i_item = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_item', i);
        var i_quantity = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_quantity', i);
        var i_description = i_conceptbill;
        var i_unitcost = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_unitcost', i);
        var f_amount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_amount', i);
        var i_total = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_total', i);
        var i_taxcode = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_taxcode', i);
        var i_taxcodegcc1 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxcode1', i);
        var i_taxcodegcc2 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxcode2', i);
        var i_taxamtgcc1 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxamt1', i);
        var i_taxamtgcc2 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxamt2', i);
        var i_amtsintax = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_amtcom', i);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_item ==' + i_item);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_quantity ==' + i_quantity);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_description ==' + i_description);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_taxcode ==' + i_taxcode);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'f_amount ==' + f_amount);
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_amtsintax ==' + i_amtsintax);
		o_record.selectNewLineItem('item');
	    o_record.setCurrentLineItemValue('item', 'item', i_item);
	    o_record.setCurrentLineItemValue('item', 'quantity', i_quantity);
	    o_record.setCurrentLineItemValue('item', 'description', i_description);
	    o_record.setCurrentLineItemValue('item', 'taxcode', i_taxcode);
		if(_logValidation(f_amount))
		{
			o_record.setCurrentLineItemValue('item', 'rate', parseFloat(f_amount));
			f_prebillAmount = parseFloat(f_prebillAmount) + parseFloat(f_amount);
		}	    
	    o_record.setCurrentLineItemValue('item', 'custcol_amount', i_amtsintax);
	    o_record.commitLineItem('item');
	}
	if (o_record.getLineItemCount('item') > 0)
	{
		if(s_tracking != 1 && s_tracking != 2)
		{
			try
			{
				var i_vendorBillid = nlapiSubmitRecord(o_record, true);
				nlapiLogExecution('DEBUG', 'schedulerFunction', 'i_vendorBillid ==' + i_vendorBillid);
				a_prebillArray[i_prebillcnt++]=prebill;
			}
			catch(o_exception2)
			{
				nlapiLogExecution('DEBUG', 'Try catch', 'Try Catch Vendor creation=' + o_exception2);
			}				
		}	
		
		//Create Journal Entry
		//if(_logValidation(i_vendorBillid) || (s_tracking == 1 || s_tracking == 2))
		{
			var o_journalRecord = nlapiCreateRecord('journalentry');
			//setting values for Journal Entry
			//setting custom form			
			o_journalRecord.setFieldValue('customform', 133);
			if(_logValidation(i_period))
			{
				var i_Std_period = nlapiLookupField('customrecord_period',i_period,'custrecord_period_postingperiod');
				o_journalRecord.setFieldValue('postingperiod', i_Std_period);
			}
			nlapiLogExecution('DEBUG', 'journal', 'i_subsidiary ==' + i_subsidiary);
			if(_logValidation(i_subsidiary))
			{
				o_journalRecord.setFieldValue('subsidiary', i_subsidiary);
				
			}
			if(_logValidation(prebill))
			{
				o_journalRecord.setFieldValue('custbody6', prebill);
			}
			var i_lineCount = o_prebillRecord.getLineItemCount('recmachcustrecord_itembill_bill')
			nlapiLogExecution('DEBUG', 'journal', 'i_lineCount ==' + i_lineCount);
			for(var i = 1; i <=i_lineCount ; i++)
			{
				var f_totalAmount = 0.0;
		        var f_amount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_amount', i);        
		        var i_class = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_category', i);
		        var i_brand = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_brand', i);
		        var i_department = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_department', i);
		        var i_location = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_location', i);
				nlapiLogExecution('DEBUG', 'journal', 'i_location ==' + i_location);
		        nlapiLogExecution('DEBUG', 'journal', 'f_amount ==' + f_amount);
				nlapiLogExecution('DEBUG', 'journal', 'i_class ==' + i_class);
				nlapiLogExecution('DEBUG', 'journal', 'i_brand ==' + i_brand);
				nlapiLogExecution('DEBUG', 'journal', 'i_department ==' + i_department);
				var o_LineItem = o_journalRecord.selectNewLineItem('line');
		        o_journalRecord.setCurrentLineItemValue('line', 'account', 1190);
				f_amount = parseFloat(f_amount);
				f_totalAmount = parseFloat(f_totalAmount) + parseFloat(f_amount);
				o_journalRecord.setCurrentLineItemValue('line', 'debit', f_amount.toFixed(2));
				if (_logValidation(i_customer)) {
					o_journalRecord.setCurrentLineItemValue('line', 'entity', i_customer);
					nlapiLogExecution('DEBUG', 'journal', 'i_customer ==' + i_customer);
				}
				if (_logValidation(i_class))
				{
					o_journalRecord.setCurrentLineItemValue('line', 'class', i_class);
				}
				if (_logValidation(i_brand))
				{
					//o_journalRecord.setCurrentLineItemValue('line', 'brand', i_brand);
				}
				if (_logValidation(i_department))
				{
					o_journalRecord.setCurrentLineItemValue('line', 'department', i_department);
				}
				nlapiLogExecution('DEBUG', 'journal', 'i_location ==' + i_location);
				if (_logValidation(i_location))
				{
					o_journalRecord.setCurrentLineItemValue('line', 'location', i_location);
				}
				nlapiLogExecution('DEBUG', 'journal', 'prebill ==' + prebill);
				if (_logValidation(prebill))
				{
					o_journalRecord.setCurrentLineItemValue('line', 'custcol_gcc_prebill', prebill);
				}
				o_journalRecord.commitLineItem('line');
				
				if(i == i_lineCount)
				{
					var o_LineItem = o_journalRecord.selectNewLineItem('line');
			        o_journalRecord.setCurrentLineItemValue('line', 'account', 1069);					
					o_journalRecord.setCurrentLineItemValue('line', 'credit', f_totalAmount.toFixed(2));
					if (_logValidation(i_class))
					{
						o_journalRecord.setCurrentLineItemValue('line', 'class', i_class);
					}
					if (_logValidation(i_brand))
					{
						//o_journalRecord.setCurrentLineItemValue('line', 'brand', i_brand);
					}
					if (_logValidation(i_department))
					{
						o_journalRecord.setCurrentLineItemValue('line', 'department', i_department);
					}
					if (_logValidation(i_location))
					{
						o_journalRecord.setCurrentLineItemValue('line', 'location', i_location);
					}
					if (_logValidation(prebill))
					{
						o_journalRecord.setCurrentLineItemValue('line', 'custcol_gcc_prebill', prebill);
					}
					o_journalRecord.commitLineItem('line');
				}
				//Code to insert the data in to sales order array
				var i_location = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_location', i);
				var i_description = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_description', i);
				var i_No_Factura = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_numbill', i);
				var i_taxamount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_taxamount', i);
				var i_taxamount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_taxamount', i);
				var i_taxamount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_taxamount', i);
				var i_unitcost = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_unitcost', i);
		        var f_amount = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_amount', i);
		        var i_total = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_total', i);
		        var i_taxcode = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_taxcode', i);
		        var i_taxcodegcc1 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxcode1', i);
		        var i_taxcodegcc2 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxcode2', i);
		        var i_taxamtgcc1 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxamt1', i);
		        var i_taxamtgcc2 = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_gcc_taxamt2', i);
		        var i_amtsintax = o_prebillRecord.getLineItemValue('recmachcustrecord_itembill_bill', 'custrecord_itembill_amtcom', i);
				i_budget = _valid(i_budget)
				i_customer= _valid(i_customer)
				i_description= _valid(i_description)
				i_taxcode= _valid(i_taxcode)
				//i_taxamount= _valid(i_taxamount)
				//i_total= _valid(i_total)
				i_No_Factura= _valid(i_No_Factura)
				i_class= _valid(i_class)
				i_amtsintax= _valid(i_amtsintax)
				i_taxamtgcc1= _valid(i_taxamtgcc1)
				i_taxamtgcc2= _valid(i_taxamtgcc2)
				i_taxcodegcc1= _valid(i_taxcodegcc1)
				i_taxcodegcc2= _valid(i_taxcodegcc2)
				i_location= _valid(i_location)
				i_brand = _valid(i_brand)
				if(a_salesOrderData.length==0)
				{
					a_salesOrderData[i_arrcounter++]=i_budget+'#$##'+i_customer+'#$##'+prebill+'####'+i_item+'####'+i_quantity+'####'+i_unitcost+'####'+f_amount+'####'+i_description+'####'+i_taxcode+'####'+i_taxamount+'####'+i_total+'####'+i_No_Factura+'####'+i_class+'####'+i_amtsintax+'####'+i_taxamtgcc1+'####'+i_taxamtgcc2+'####'+i_taxcodegcc1+'####'+i_taxcodegcc2+'####'+i_location+'####'+i_brand;
				}
				else if(a_salesOrderData.length!=0)
				{
					var flag=0;
					for(var j=0;a_salesOrderData!=null && j<a_salesOrderData.length;j++)
					{
						var a_splitSOData=new Array();
						a_splitSOData=a_salesOrderData[j].split('#$##');
						if(a_splitSOData[0]==i_budget && a_splitSOData[1]==i_customer)	
						{						
							//a_salesOrderData[j]=a_salesOrderData[j]+'#$##'+prebill+'####'+a_splitSOData[3]+'####'+a_splitSOData[4]+'####'+a_splitSOData[5]+'####'+a_splitSOData[6]+'####'+a_splitSOData[7]+'####'+a_splitSOData[8]+'####'+a_splitSOData[9]+'####'+a_splitSOData[10]+'####'+a_splitSOData[11]+'####'+a_splitSOData[12]+'####'+a_splitSOData[13]+'####'+a_splitSOData[14]+'####'+a_splitSOData[15]+'####'+a_splitSOData[16]+'####'+a_splitSOData[17]+'####'+a_splitSOData[18]+'####'+a_splitSOData[19]+'####'+a_splitSOData[20];
							a_salesOrderData[j]=a_salesOrderData[j]+'#$##'+prebill+'####'+i_item+'####'+i_quantity+'####'+i_unitcost+'####'+f_amount+'####'+i_description+'####'+i_taxcode+'####'+i_taxamount+'####'+i_total+'####'+i_No_Factura+'####'+i_class+'####'+i_amtsintax+'####'+i_taxamtgcc1+'####'+i_taxamtgcc2+'####'+i_taxcodegcc1+'####'+i_taxcodegcc2+'####'+i_location+'####'+i_brand;
							flag=1;
						}										
					} // for j
					if(flag!=1)
					{
						a_salesOrderData[i_arrcounter++]=i_budget+'#$##'+i_customer+'#$##'+prebill+'####'+i_item+'####'+i_quantity+'####'+i_unitcost+'####'+f_amount+'####'+i_description+'####'+i_taxcode+'####'+i_taxamount+'####'+i_total+'####'+i_No_Factura+'####'+i_class+'####'+i_amtsintax+'####'+i_taxamtgcc1+'####'+i_taxamtgcc2+'####'+i_taxcodegcc1+'####'+i_taxcodegcc2+'####'+i_location+'####'+i_brand;
					}
				}//else if(InvArray.length!=0)
			}//end of for
			if(_logValidation(f_totalAmount) && (s_tracking != 1 && s_tracking != 2))
			{
				try
				{
					var i_JVID = nlapiSubmitRecord(o_journalRecord);
                 	nlapiLogExecution('DEBUG', 'journal', 'Created Journal= ' + i_JVID);
				}
				catch(o_exception3)
				{
					nlapiLogExecution('DEBUG', 'Try catch', 'Try Catch Journal creation=' + o_exception3);
				}
			}
		}//end of journal creation
	}
}


// BEGIN FUNCTION ===================================================

function _logValidation(value)
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		//value = 0.0;
		return false;
	}
}

function _valid(variable)
{
	if(variable != null && variable != '' && variable != undefined && variable.toString() != 'NaN' && variable != NaN)
	{
		return variable;
	}
	else
	{		
		return '';
	}
}

// END FUNCTION =====================================================