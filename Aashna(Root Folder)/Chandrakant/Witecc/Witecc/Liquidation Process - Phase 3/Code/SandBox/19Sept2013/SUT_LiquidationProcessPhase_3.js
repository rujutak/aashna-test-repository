// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
    
     Script Name: SUT_LiquidationProcessPhase_3.js
    
     Author: 
    
     Company:
    
     Date: 02Sept2013
    
     Description: Suitelet for design and generating form for Liquidation process phase 3 development
    
     Script Modification Log:
    
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    
     */
    
}

var PROC_TYPE_BUDGET = '1';
var PROC_TYPE_MASIVE = '2';

var NUM_DEPLOYMENTS_SCHEDULED_SCRIPT = 5;

nlapiLogExecution('DEBUG', ' In Suitelet  ', ' In Suitelet ');
function displayformLiquidationPhase3(request, response)
{
    var proctype = request.getParameter('proctype');
	nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'proctype= '+proctype);
    var period = request.getParameter('period');
	nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'period= '+period);
    var subsidiary = request.getParameter('subsidiary');
    var tracking = request.getParameter('tracking'); //--> 1-Anticipado, 2-Canje, 3-Normal
    nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'tracking= '+tracking);
    var budget = request.getParameter('budget');
    var customer = request.getParameter('customer');
    var currency = request.getParameter('currency'); //--> Moneda LiquidaciÃƒÆ’Ã‚Â³n
    var liqdate = request.getParameter('liqdate');
	nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'liqdate= '+liqdate);
    nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'request.getMethod()= '+request.getMethod());
    if (request.getMethod() == "GET") {
    
        displayLiquidationsForm(proctype, subsidiary, period, tracking, budget, customer, liqdate);
		nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'End Suitelet GET');
    }
    
    if (request.getMethod() == "POST")	
	{
        nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'In Suitelet Post');		
        var period = getCustomPeriod(period);
        
        //--> Validate filters
        validateFilters(proctype, subsidiary, period, tracking, budget, currency);
        nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'before calling result function');
        //--> Search bills to process
        var results = searchPreBillsToProcess(subsidiary, period, tracking, budget);
        
        if (results != null && results.length != 0) {
        
            subsidiary = results[0].getValue('custrecord_bill_subsidiary');
            period = results[0].getValue('custrecord_bill_period');
            tracking = results[0].getValue('custrecord_bill_tracking');
            
            var wid = createJobRecord(results, subsidiary, period, tracking, currency, liqdate);
            nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'wid ' + wid);
            /*
             if (proctype == PROC_TYPE_MASIVE) {
             */
            var binqueue = false;
            
			
			var params=new Array();
		    params['status']='scheduled';
		    params['runasadmin']='T';
			params['custscript_proctypephase3']=proctype;
			params['custscript_periodphase3']=period;
			params['custscript_subsidiaryphase3']=subsidiary;
			params['custscript_trackingphase3']=tracking;
			params['custscript_budgetphase3']=budget;
			params['custscript_customerphase3']=customer;
			params['custscript_currencyphase3']=currency;
			params['custscript_liqdatephase3']=liqdate;
			params['custscript_jobidphase3']=wid;
			
		    var startDate = new Date();
		    params['startdate']=startDate.toUTCString();
		    
            for (var j = 1; j <= NUM_DEPLOYMENTS_SCHEDULED_SCRIPT; j++) {
            
                //var wstatus = nlapiScheduleScript('customscript_liquidation_job_cr', 'customdeploy_liquidation_job_cr' + j.toString(), { custscript_jobid_cr: wid });
                var wstatus = nlapiScheduleScript('customscriptsch_liquidationprocessphase3', 'customdeploy1', params);
                if (wstatus == "QUEUED") {
                    binqueue = true;
                    break;
                }
            }
            
            if (binqueue == true) {
            
                redirectTOProcessList(request, response);
            }
            else {
                throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecuci" + String.fromCharCode(243) + "n, espere a que terminen para iniciar un nuevo proceso!!!", false);
            }
            /*
             }
             if (proctype == PROC_TYPE_BUDGET) {
             var objSetup = new nlobjSetupLiq();
             var account  = objSetup.account;
             var email    = objSetup.email;
             var password = objSetup.password;
             var role     = objSetup.role;
             var url      = objSetup.getUrlLiq(subsidiary);
             var resp = callLiqProc(account, email, password, role, url, wid);
             redirectTOProcessList(request, response);
             }
             */
        }
        else {
            throw nlapiCreateError("ERROR", "No se encontraron registros por procesar!", false);
        }
        
    }
}

//-------------------------------
//-- Display Liquidations Form --
//-------------------------------
function displayLiquidationsForm(proctype, subsidiaryid, periodid, tracking, budget, customer, liqdate)
{
	nlapiLogExecution('DEBUG', ' displayLiquidationsForm  ', 'In displayLiquidationsForm');
    //--> Get Data Fields
    var context = nlapiGetContext();
    var subsidiary = nlapiGetSubsidiary(); //-->  context.getSubsidiary();
    var subsidiaryRecord = nlapiLoadRecord('subsidiary', subsidiary);
    var subsidiarychilds = searchChildsOfSubsidiary(subsidiary);
    var customers = searchCustomersOfBudget(budget);
    var currencies = searchCurrenciesOfCustomer(customer);
    var prebill = getPrebillRecord(budget);
    proctype = (fnIsNull(proctype) == '') ? PROC_TYPE_MASIVE : proctype;
    tracking = (proctype == PROC_TYPE_MASIVE && fnIsNull(tracking) == '') ? '3' : tracking;
    tracking = (proctype == PROC_TYPE_BUDGET && prebill != null) ? prebill.getFieldValue('custrecord_bill_tracking') : tracking;
    
    //--> Create Form
    var form = nlapiCreateForm("Liquidaciones Costa Rica");
    form.setScript('customscript_cli_liquidationprocessphas3');
    
    //--> Create Radio Buttons: Process Type
    form.addField('lblproctype1', 'label', 'LiquidaciÃƒÆ’Ã‚Â³n').setLayoutType('startrow');
    form.getField('lblproctype1').setBreakType('startcol');
    form.addField('proctype', 'radio', 'Masiva', PROC_TYPE_MASIVE).setLayoutType('endrow');
    form.addField('lblproctype2', 'label', '').setLayoutType('startrow');
    form.addField('proctype', 'radio', 'Presupuesto', PROC_TYPE_BUDGET).setLayoutType('endrow');
    form.getField('proctype').setDefaultValue(proctype);
    form.addField('lblproctype3', 'label', '').setLayoutType('startrow');
    
    //--> Create Combobox Periods
    field = form.addField('period', 'select', 'Periodo', 'accountingperiod'); //customrecord_period
    field.setDefaultValue(periodid);
    field.setBreakType('startrow');
    field.setMandatory(true);
    
    //--> Create Combobox Subsidiaries
    field = form.addField('subsidiary', 'select', 'Subsidiaria', null);
    field.setDefaultValue(subsidiaryid);
    field.setBreakType('startrow');
    field.setMandatory(true);
    
    field.addSelectOption(subsidiaryRecord.getId(), subsidiaryRecord.getFieldValue('name'));
    
    for (var i = 0; subsidiarychilds != null && i < subsidiarychilds.length; i++) {
        field.addSelectOption(subsidiarychilds[i].getValue('internalid'), '...' + subsidiarychilds[i].getValue('name'));
    }
    
    //--> Create Tracking Field
    field = form.addField("tracking", "select", "Seguimiento", 'customlist_tipoproceso');
    field.setDefaultValue(tracking);
    field.setBreakType('startrow');
    field.setMandatory(true);
    
    //--> Create Date Field
    field = form.addField("liqdate", "date", "Fecha LiquidaciÃƒÆ’Ã‚Â³n", null);
    var date = nlapiDateToString(new Date());
    liqdate = (fnIsNull(liqdate) == '' ? date : liqdate);
    field.setDefaultValue(liqdate);
    field.setBreakType('startrow');
    field.setMandatory(true);
    
    //--> Create Textbox Budget
    var disabled = (proctype == PROC_TYPE_MASIVE) ? 'disabled' : 'normal';
    field = form.addField('budget', 'select', 'Presupuesto', 'customrecord_pauta');
    field.setDefaultValue(budget);
    field.setBreakType('startrow');
    field.setDisplayType(disabled);
    
    //--> Create Combox Customer
    field = form.addField('customer', 'select', 'Cliente', null);
    
    field.addSelectOption('', '');
    
    for (var i = 0; customers != null && i < customers.length; i++) {
        field.addSelectOption(customers[i].getValue('custrecord_bill_customer', null, 'group'), customers[i].getText('custrecord_bill_customer', null, 'group'));
    }
    
    field.setDefaultValue(customer);
    field.setBreakType('startrow');
    field.setDisplayType(disabled);
    
    //--> Create Combobox Currency
    field = form.addField('currency', 'select', 'Moneda Factura', null);
    
    field.addSelectOption('', '');
    
    for (var i = 0; currencies != null && i < currencies.length; i++) {
        field.addSelectOption(currencies[i].id, currencies[i].name);
    }
    
    field.setBreakType('startrow');
    field.setDisplayType(disabled);
    
    //--> Add buttons to form
    form.addSubmitButton('Procesar');
	//form.addSubmitButton('Submit');
	//form.addButton('custpage_procesar','Procesar');
    
    //--> Response to client
    response.writePage(form);
	nlapiLogExecution('DEBUG', ' displayLiquidationsForm  ', 'END displayLiquidationsForm');
}

function searchChildsOfSubsidiary(id){

    if (id == null || id == '') {
        return;
    }
    
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_parent_subsidiary', null, 'is', id);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('name');
    
    var results = nlapiSearchRecord('subsidiary', null, filters, columns);
    
    return results;
}

function searchCustomersOfBudget(budget){

    if (budget == null || budget == '') {
        return;
    }
    
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_bill_pauta', null, 'anyof', budget);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_bill_customer', null, 'group');
    
    var results = nlapiSearchRecord('customrecord_bill', null, filters, columns);
    
    return results;
}

function searchCurrenciesOfCustomer(customer){

    if (customer == null || customer == '') {
        return;
    }
    
    var currencies = new Array();
    
    var customerRecord = fnGetEntityRecord(customer);
    
    for (var i = 1; customerRecord != null && i <= customerRecord.getLineItemCount('currency'); i++) {
    
        var currency = customerRecord.getLineItemValue('currency', 'currency', i);
        var currencytxt = customerRecord.getLineItemText('currency', 'currency', i);
        currencies[currencies.length] = {
            'id': currency,
            'name': currencytxt
        };
    }
    
    return currencies;
}

function getPrebillRecord(budget){

    if (budget == null || budget == '') {
        return;
    }
    
    var record = null;
    
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_bill_pauta', null, 'anyof', budget);
    
    var results = nlapiSearchRecord('customrecord_bill', null, filters, null);
    
    if (results != null && results.length > 0) {
    
        record = nlapiLoadRecord(results[0].getRecordType(), results[0].getId());
    }
    
    return record;
}

function getCustomPeriod(period){

    var retval = null;
    
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_period_postingperiod', null, 'is', period);
    
    var columns = new Array();
    
    var results = nlapiSearchRecord('customrecord_period', null, filters, columns);
    
    if (results != null && results.length > 0) {
        retval = results[0].getId();
    }
    
    return retval;
}

function redirectTOProcessList(request, response){
    var host = request.getHeader("HOST");
    var url = nlapiResolveURL('RECORD', 'customrecord_job', null, null);
    url = url.replace(/custrecordentry.nl/g, 'custrecordentrylist.nl');
    url = "https://" + host + url;
    var html = "";
    html += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
    html += "<html>";
    html += "<head>";
    html += "<title>Redirecting...</title>";
    html += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">";
    html += "</head>";
    html += "<script>";
    html += "window.location = \"" + url + "\";";
    html += "<\/script>";
    html += "<body>";
    html += "</body>";
    html += "</html>";
    response.write(html);
}

function searchPreBillsToProcess(subsidiary, period, tracking, budget)
{

    var index = 0;
    
    var filters = new Array();
    
    if (fnIsNull(subsidiary) != '') {
    
        filters[index] = new nlobjSearchFilter('custrecord_bill_subsidiary', null, 'anyof', subsidiary);
        index++;
    }
    
    if (fnIsNull(period) != '') {
        filters[index] = new nlobjSearchFilter('custrecord_bill_period', null, 'anyof', period);
        index++;
    }
    
    if (fnIsNull(tracking) != '') {
        filters[index] = new nlobjSearchFilter('custrecord_bill_tracking', null, 'is', tracking);
        index++;
    }
    
    if (fnIsNull(budget) != '') {
        filters[index] = new nlobjSearchFilter('custrecord_bill_pauta', null, 'is', budget);
        index++;
    }
    
    filters[index] = new nlobjSearchFilter('custrecord_procesada', null, 'is', 'F'); //Facturas no liquidadas
    index++;
    
    //filters[index] = new nlobjSearchFilter('custrecord_bill_liquidation', null, 'noneof', '@NONE@'); //Facturas sin referncias liquidaciones
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('custrecord_bill_vendor');
    columns[2] = new nlobjSearchColumn('custrecord_bill_customer');
    columns[3] = new nlobjSearchColumn('custrecord_bill_subsidiary');
    columns[4] = new nlobjSearchColumn('custrecord_bill_period');
    columns[5] = new nlobjSearchColumn('custrecord_bill_purchaseorder');
    columns[6] = new nlobjSearchColumn('custrecord_bill_tracking');
    columns[7] = new nlobjSearchColumn('custrecord_bill_pauta');
    columns[8] = new nlobjSearchColumn('custrecord_bill_currency');
    columns[9] = new nlobjSearchColumn('custrecord_bill_trandate');
    columns[10] = new nlobjSearchColumn('custrecord_bill_amount');
    columns[11] = new nlobjSearchColumn('custrecord_bill_location');
    columns[12] = new nlobjSearchColumn('currency', 'custrecord_bill_customer');
    columns[13] = new nlobjSearchColumn('custrecord_bill_liquidation');
    
    var results = nlapiSearchRecord('customrecord_bill', null, filters, columns);
    //nlapiLogExecution('DEBUG', ' In Suitelet Post  ', 'results ' + results[0].getValue('internalid'));
    return results;
}

/*
 function createJobRecord(results, subsidiaryid, periodid, trackingid, currencyid, liqdate) {
 var wid = null;
 if (results != null && results.length != 0) {
 //--> Create Record Job
 var wrecord = nlapiCreateRecord("customrecord_job");
 wrecord.setFieldValue("custrecord_job_subsidiary", subsidiaryid);
 wrecord.setFieldValue("custrecord_job_period", periodid);
 wrecord.setFieldValue("custrecord_job_type", PROCESS_TYPE_LIQUIDATIONS);
 wrecord.setFieldValue("custrecord_job_tracking", trackingid);
 wrecord.setFieldValue("custrecord_job_currency", currencyid);
 wrecord.setFieldValue("custrecord_job_liqdate", liqdate);
 
 wid = nlapiSubmitRecord(wrecord, true);
 wrecord = nlapiLoadRecord('customrecord_job', wid);
 for (var i = 0; results != null && i < results.length; i++) {
 var wbill = results[i].getValue('internalid');
 var wvendor = results[i].getValue('custrecord_bill_vendor');
 var wcustomer = results[i].getValue('custrecord_bill_customer');
 var wsubsidiary = results[i].getValue('custrecord_bill_subsidiary');
 var wperiod = results[i].getValue('custrecord_bill_period');
 var wporder = results[i].getValue('custrecord_bill_purchaseorder');
 var wtracking = results[i].getValue('custrecord_bill_tracking');
 var wpauta = results[i].getValue('custrecord_bill_pauta');
 var wcurrency = results[i].getValue('custrecord_bill_currency');
 var wtrandate = results[i].getValue('custrecord_bill_trandate');
 var wamount = results[i].getValue('custrecord_bill_amount');
 var wlocation = results[i].getValue('custrecord_bill_location');
 var wcustcurrency = results[i].getValue('currency', 'custrecord_bill_customer');
 var wliquidation = results[i].getValue('custrecord_bill_liquidation');
 if (fnIsNull(wliquidation) == '') {
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_bill', (i + 1), wbill);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_job', (i + 1), wid);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_vendor', (i + 1), wvendor);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_customer', (i + 1), wcustomer);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_period', (i + 1), wperiod);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_porder', (i + 1), wporder);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_budget', (i + 1), wpauta);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_location', (i + 1), wlocation);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_currency', (i + 1), wcurrency);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_amount', (i + 1), wamount);
 wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_customercurrency', (i + 1), wcustcurrency);
 }
 }
 nlapiSubmitRecord(wrecord, true);
 }
 return wid;
 }
 */
function createJobRecord(results, subsidiaryid, periodid, trackingid, currencyid, liqdate){
    var wid = null;
    if (results != null && results.length != 0) {
        //--> Create Record Job
        var wrecord = nlapiCreateRecord("customrecord_job");
        wrecord.setFieldValue("custrecord_job_subsidiary", subsidiaryid);
        wrecord.setFieldValue("custrecord_job_period", periodid);
        wrecord.setFieldValue("custrecord_job_type", PROCESS_TYPE_LIQUIDATIONS);
        wrecord.setFieldValue("custrecord_job_tracking", trackingid);
        wrecord.setFieldValue("custrecord_job_currency", currencyid);
        wrecord.setFieldValue("custrecord_job_liqdate", liqdate);
        wid = nlapiSubmitRecord(wrecord, true);
        wrecord = nlapiLoadRecord('customrecord_job', wid);
        var index = 1;
        for (var i = 0; results != null && i < results.length; i++) {
            var wbill = results[i].getValue('internalid');
            var wvendor = results[i].getValue('custrecord_bill_vendor');
            var wcustomer = results[i].getValue('custrecord_bill_customer');
            var wsubsidiary = results[i].getValue('custrecord_bill_subsidiary');
            var wperiod = results[i].getValue('custrecord_bill_period');
            var wporder = results[i].getValue('custrecord_bill_purchaseorder');
            var wtracking = results[i].getValue('custrecord_bill_tracking');
            var wpauta = results[i].getValue('custrecord_bill_pauta');
            var wcurrency = results[i].getValue('custrecord_bill_currency');
            var wtrandate = results[i].getValue('custrecord_bill_trandate');
            var wamount = results[i].getValue('custrecord_bill_amount');
            var wlocation = results[i].getValue('custrecord_bill_location');
            var wcustcurrency = results[i].getValue('currency', 'custrecord_bill_customer');
            var wliquidation = results[i].getValue('custrecord_bill_liquidation');
            if (fnIsNull(wliquidation) == '') {
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_bill', index, wbill);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_job', index, wid);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_vendor', index, wvendor);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_customer', index, wcustomer);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_period', index, wperiod);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_porder', index, wporder);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_budget', index, wpauta);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_location', index, wlocation);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_currency', index, wcurrency);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_amount', index, wamount);
                wrecord.setLineItemValue('recmachcustrecord_jbill_job', 'custrecord_jbill_customercurrency', index, wcustcurrency);
                index++;
            }
        }
        nlapiSubmitRecord(wrecord, true);
    }
    return wid;
}

function validateFilters(proctype, subsidiary, period, tracking, budget, currency){

    var msg = '';
    
    if (proctype == PROC_TYPE_BUDGET) {
    
        if (fnIsNull(budget) == '') {
            msg += 'presupuesto,'
        }
        
        if (fnIsNull(currency) == '') {
            msg += 'moneda,'
        }
        
        if (msg.length > 0) {
            msg = msg.substr(0, msg.length - 1);
            msg = 'Favor de proporcionar valor(es) para: ' + msg + '.';
        }
        
        if (msg.length > 0) {
        
            throw nlapiCreateError("ERROR", msg, false);
        }
    }
    
    if (proctype == PROC_TYPE_MASIVE) {
    
    }
    
}

//---------------------------------------------------------------------------------------------------------------

function callLiqProc(account, email, password, role, url, jobid){

    //--> Setting up Headers 
    var headers = new Array();
    headers['User-Agent-x'] = 'SuiteScript-Call';
    headers['Authorization'] = 'NLAuth nlauth_account=' + account + ', nlauth_email=' + email + ', nlauth_signature=' + password + ', nlauth_role=' + role;
    headers['Content-Type'] = 'application/json';
    
    //--> Setting up Datainput
    var jsonobj = new Object();
    jsonobj.job = jobid;
    var myJSONText = JSON.stringify(jsonobj, fnReplacer);
    
    //--> Request URL
    var resp = nlapiRequestURL(url, myJSONText, headers, null);
    
    //--> Get response body
    var myobj = eval('(' + resp.body + ')');
    
    return myobj;
}
