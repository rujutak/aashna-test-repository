/**
 * @author Shweta
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{

/*
 * Script File  : SCH_GCC_ChequePrint_PDF.js
 * Script Type  : Schedule
 * Description  : Create pdf file to print massively checks
 * Author       : Shweta
 * Created      : 24 July 2013
 * Event        : Scheduler
 * Main Method  : schedulerFunction(type)
 * ScriptId     : customscript_sch_gcc_chequeprint_pdf 
 * DeploymentId : customdeploy1  
 */
 /*
   
	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	// ******************* Start -  Try / Catch  Block *************************
	
	try
	{
		var retxml = "";
		
		var context = nlapiGetContext();
	
		var usageBegin = context.getRemainingUsage();
		nlapiLogExecution('DEBUG', 'schedulerFunction',' Usage At Begin -->' + usageBegin);
		
		var a_checks_arr =context.getSetting('SCRIPT','custscript_checksgcc');
	    nlapiLogExecution('DEBUG', 'schedulerFunction',' Checks -->' + a_checks_arr);
		
		var i_number_of_checks = context.getSetting('SCRIPT','custscript_num_checks');
	    nlapiLogExecution('DEBUG', 'schedulerFunction',' No Of Checks -->' + i_number_of_checks);
		
		var i_format_id = context.getSetting('SCRIPT','custscript_format_id');
	    nlapiLogExecution('DEBUG', 'schedulerFunction',' Format ID -->' + i_format_id);
		
	    var objformat = getFormatCheck(i_format_id);
	    nlapiLogExecution('DEBUG', 'schedulerFunction',' Object  Format  -->' + objformat);
	 
	  
	   // ******************* Check Array *******************  
	    if(a_checks_arr != null && a_checks_arr != undefined && a_checks_arr != '')
		{
			 a_checks_arr = a_checks_arr.split(',');
			 nlapiLogExecution('DEBUG', 'schedulerFunction','   Array Checks  -->' + a_checks_arr);
			 
			 if(i_number_of_checks != null && i_number_of_checks != undefined && i_number_of_checks != '')
			 {
			 	
				     i_number_of_checks = i_number_of_checks.split(',');
			 nlapiLogExecution('DEBUG', 'schedulerFunction',' Number Of Checks -->' + i_number_of_checks);
			
			 var o_checks_OBJ = getArrayObjChecks(a_checks_arr, i_number_of_checks);
			 nlapiLogExecution('DEBUG', 'schedulerFunction',' Check Object  -->' + o_checks_OBJ);
			
			 if(o_checks_OBJ != '' && o_checks_OBJ!= null && o_checks_OBJ!= undefined)
			 {
				o_checks_OBJ = o_checks_OBJ.sort(sortchecks);
			    nlapiLogExecution('DEBUG', 'schedulerFunction',' Sorted Checks Object  -->' + o_checks_OBJ);
						
			    var datain = new TIAObjRequestChecksToPrint();
			    datain.checks = o_checks_OBJ;
				nlapiLogExecution('DEBUG', 'schedulerFunction',' datain.checks  -->' + datain.checks);
							
			    datain.fromindex = 0;
				nlapiLogExecution('DEBUG', 'schedulerFunction',' datain.fromindex  -->' + datain.fromindex);
				
			    datain.format = objformat;
				nlapiLogExecution('DEBUG', 'schedulerFunction',' datain.format  -->' + datain.format);
				
				var objResponse = new TIAObjResponseChecksToPrint();
				
				 if (datain.checks == null || datain.checks.length == 0) 
			    {
			      // objResponse.success = false;
			      // objResponse.message = "Debe proporcionar almenos el id de un registro!";
			      // return objResponse;
			    }
						
				
				
			    var xml = '';
			    var lastindex = -1;
			    var objformat_new = datain.format == null ? getFormatCheck(1) : datain.format;
				nlapiLogExecution('DEBUG', 'schedulerFunction',' objformat_new -->' + objformat_new);
			
			    for (var i = datain.fromindex; i < datain.checks.length; i++) 
			    {
			        var context = nlapiGetContext();
			
			        if (context.getRemainingUsage() > 1000)
			        {
			            xml += getXmlToCheck(datain.checks[i].type, datain.checks[i].id, datain.checks[i].tranid, objformat_new);
			              nlapiLogExecution('DEBUG', 'schedulerFunction',' xmllllllll -->' + xml);
			
					    if(xml != "" && i < datain.checks.length - 1)
					    {
						    xml += "<PBR/>";
							
							 nlapiLogExecution('DEBUG', 'schedulerFunction',' xmll Inside -->' + xml);
					    }
			
			            lastindex = i;
						nlapiLogExecution('DEBUG', 'schedulerFunction',' lastindex -->' + lastindex);
			        }
			    }
				
				 objResponse.body = xml;
				 nlapiLogExecution('DEBUG', 'schedulerFunction',' objResponse.body -->' + objResponse.body);
				 
                 objResponse.lastindex = lastindex;
				 nlapiLogExecution('DEBUG', 'schedulerFunction',' objResponse -->' + objResponse);
				 				
                  var myobj = objResponse.body;
				  nlapiLogExecution('DEBUG', 'schedulerFunction',' myobj -->' + myobj);
				  								  
				  var xml = getXmlToPrint(myobj, objformat);
				  nlapiLogExecution('DEBUG', 'schedulerFunction',' xml -->' + xml);
				  
				   var file = nlapiXMLToPDF(xml);
                   nlapiLogExecution('DEBUG', 'PDF File ', ' File ID -->' + file);
				   
				   //Create PDF File with TimeStamp in File Name
	
					var d_currentTime = new Date();    
				    var timestamp=d_currentTime.getTime();
					
					//Create PDF File with TimeStamp in File Name
				   				   
				   var fileObj = nlapiCreateFile('ChequePrint_'+timestamp+'.pdf', 'PDF', file.getValue());
				    
					nlapiLogExecution('DEBUG', 'PDF File', ' PDF File Object -->' + fileObj);
				    fileObj.setFolder(338); // Folder PDF File is to be stored
				   
				    var fileId = nlapiSubmitFile(fileObj);
				    nlapiLogExecution('DEBUG', 'PDF File', '********** PDF File ID ********* -->' + fileId);
			
		
					// ===================== Create Cheque Print Record Details =================================
						
						
					// Get the Current Date
					var date = new Date();
				    nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date);
				
				    var offsetIST = 5.5;
				
				    //To convert to UTC datetime by subtracting the current Timezone offset
				     var utcdate =  new Date(date.getTime() + (date.getTimezoneOffset()*60000));
				     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
				
				
				    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
				     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
				     nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
				    
					// Get the Day  
				     day = istdate.getDate();
					 
				     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
					 // Get the  Month  
				     month = istdate.getMonth()+1;
					 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
					 
				      // Get the Year 
					 year=istdate.getFullYear();
					 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
					  
					 // Get the String Date in dd/mm/yyy format 
					 pdfDate= day + '/' + month + '/' + year;
					 nlapiLogExecution('DEBUG', 'schedulerFunction',' Todays Date =='+pdfDate);	
					 
					 // Get the Hours , Minutes & Seconds for IST Date
					 var timePeriod;
					 var hours=istdate.getHours();
					 var mins=istdate.getMinutes();
					 var secs=istdate.getSeconds();
					 
					 if(hours>12)
					 {
					 	hours=hours-12;
					 	timePeriod='PM'
					 }
					 else if (hours == 12) 
					 {
					   timePeriod = 'PM';
					 }
					 else
					 {
					 	timePeriod='AM'
					 }
					 
					 // Get Time in hh:mm:ss: AM/PM format
					 var pdfTime=hours +':'+mins+':'+secs+' '+timePeriod; 
											
					 nlapiLogExecution('DEBUG', 'schedulerFunction', ' Todays Date Time  ==' + pdfTime);
					
						
					var userId = nlapiGetUser();
					nlapiLogExecution('DEBUG', 'schedulerFunction', ' User ID --> ' + userId);
											 
				    var chequeRecord=nlapiCreateRecord('customrecord_cheque_print_record');
					nlapiLogExecution('DEBUG', 'schedulerFunction', " Cheque Print Create Record : " + chequeRecord); 
				     
					chequeRecord.setFieldValue('custrecord_user_id', userId);
					chequeRecord.setFieldValue('custrecord_date_created', pdfDate);
					chequeRecord.setFieldValue('custrecord_current_time', pdfTime);
					chequeRecord.setFieldValue('custrecord_file_id', fileId);
					chequeRecord.setFieldValue('custrecord_is_printed', 'T'); 
					var chequeRecSubmitID = nlapiSubmitRecord(chequeRecord, true, true);
					nlapiLogExecution('DEBUG', 'schedulerFunction', ' Cheque Print Submit Record  ID : ' + chequeRecSubmitID);
				
						
					//======================================= End =====================================================	
					
					
					
					// ============================== Updation Of Checks ==============================
					
					    for (var i = 0; i < o_checks_OBJ.length; i++) 
					    {
					        var record = nlapiLoadRecord(o_checks_OBJ[i].type, o_checks_OBJ[i].id);
							nlapiLogExecution('DEBUG', 'schedulerFunction', " record : " + record); 
							
							nlapiLogExecution('DEBUG', 'schedulerFunction', " o_checks_OBJ[i].type : " + o_checks_OBJ[i].type); 
							nlapiLogExecution('DEBUG', 'schedulerFunction', " o_checks_OBJ[i].id : " + o_checks_OBJ[i].id); 
							nlapiLogExecution('DEBUG', 'schedulerFunction', " o_checks_OBJ[i].tranid : " + o_checks_OBJ[i].tranid); 
							
					        record.setFieldValue('tobeprinted', 'F');
					        record.setFieldValue('tranid', o_checks_OBJ[i].tranid);
							
					        var submitID = nlapiSubmitRecord(record,true,true);
							
							nlapiLogExecution('DEBUG', 'schedulerFunction', ' ********** Cheques Submit ID ********** ' + submitID);
					    }
					
					
			 }// Checks OBJ
				
				
			 }//Number Of Checks
			 
			 
	    
			
		}//If  
				
		
	}//Try
	catch(ex)
	{
		 nlapiLogExecution('DEBUG', ' ERROR ',' Exception Caught -->' + ex);
		  nlapiLogExecution('DEBUG', ' ERROR ',' Exception Details -->' + ex.message);
		
	}//Catch
	// ******************* End -  Try / Catch Block *************************
	
   
	

}// Scheduler Function

// END SCHEDULED FUNCTION ===============================================





function getArrayObjChecks(arr_checks, arr_numchecks) 
{
    var arrobjs = new Array();

    var arrtype = new Array();
    arrtype["VendPymt"] = "vendorpayment";
    arrtype["Check"] = "check";
    arrtype["employee"] = "employee";

    var arrcheckstoprint = searchChecksToPrint(arr_checks);
	
	if(arrcheckstoprint!= "" && arrcheckstoprint!= null && arrcheckstoprint != undefined)
	{
		for (var i = 0; arrcheckstoprint != null && i < arrcheckstoprint.length; i++) 
	    {
	        var id = arrcheckstoprint[i].getValue('internalid', null, 'group');
	        var type = arrcheckstoprint[i].getValue('type', null, 'group');
	        var index = fnGetIndexItemInArray(arr_checks, id);
	        var folio = arr_numchecks[index];
	        type = arrtype[type];
	        arrobjs[i] = new TIAObjCheckToPrint(id, folio, type);
	    }
		
	}
	

    

    return arrobjs;
}




function getFormatCheck(formatid)
{

    var objFormat = null;

    if (formatid != null && formatid != "")
	{

        var record = nlapiLoadRecord("customrecord_checkformat", formatid);

        var xml = record.getFieldValue("custrecord_chkfrmt_xml");

        var header1 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_header1"));
        var header2 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_header2"));
        var header3 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_header3"));

        var body1 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_body1"));
        var body2 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_body2"));
        var body3 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_body3"));

        var footer1 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_footer1"));
        var footer2 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_footer2"));
        var footer3 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_footer3"));

        var detail1 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_detail1"));
        var detail2 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_detail2"));
        var detail3 = fnIsNull(record.getFieldValue("custrecord_chkfrmt_detail3"));

        var numlines = fnIsNull(record.getFieldValue("custrecord_chkfrmt_numlines"));
        var showlines = fnIsNull(record.getFieldValue("custrecord_chkfrmt_showlines"));

        //--> Temporal
        var top = record.getFieldValue("custrecord_chkfrmt_top");
        var bottom = record.getFieldValue("custrecord_chkfrmt_bottom");
        var left = record.getFieldValue("custrecord_chkfrmt_left");
        var right = record.getFieldValue("custrecord_chkfrmt_right");

        objFormat = new TIAObjFormatCheck(xml, header1, header2, header3, body1, body2, body3, footer1, footer2, footer3, detail1, detail2, detail3, numlines, showlines, top, bottom, left, right);
    }

    return objFormat;
}



function sortchecks(a, b)
{
    var retval = 0;
    var value_a = fnParseIntOrZero(a.tranid);
    var value_b = fnParseIntOrZero(b.tranid);
    if (value_a < value_b) { retval = -1; }
    if (value_a > value_b) { retval = 1; }
    if (value_a == value_b) { retval = 0; }
    return retval;
}


function searchChecksToPrint(checks) 
{
	var results;
	
	if(checks !=null && checks!= '' && checks!= undefined)
	{
		 var arrtypes = new Array();
	    arrtypes[0] = "VendPymt";
	    arrtypes[1] = "Check";
	    arrtypes[2] = "employee";
	
	    var filters = new Array();
	    filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', checks);
	    filters[1] = new nlobjSearchFilter('type', null, 'anyof', arrtypes);
	    //filters[1]    	= new nlobjSearchFilter('account', null, 'anyof', idaccount)
	
	    var columns = new Array();
	    columns[0] = new nlobjSearchColumn('internalid', null, 'group');
	    columns[1] = new nlobjSearchColumn('type', null, 'group');
	
	    results = nlapiSearchRecord("transaction", null, filters, columns);
		
		
	}// Check Validation 
	

    return results;
}



function getXmlToCheck(type, id, chknumber, format) 
{

    var top = format.numlines; //NÃºmero mÃ¡ximo de lÃ­neas a imprimir
    var showlines = format.showlines;

    var header1 = format.header1;
    var header2 = format.header2;
    var header3 = format.header3;

    var body1 = format.body1;
    var body2 = format.body2;
    var body3 = format.body3;

    var footer1 = format.footer1;
    var footer2 = format.footer2;
    var footer3 = format.footer3;

    var detail1 = format.detail1;
    var detail2 = format.detail2;
    var detail3 = format.detail3;

    var record_id = id;
    var type = type;
    var number = chknumber;

    var objCheck = new TIAObjCheck(type, record_id);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' objCheck -->' + objCheck );
   
   
	if(header1 != null &&  header1 != '' && header1 != undefined)
	{
		header1 = header1.replace(/\$date/g, objCheck.General.Fecha);
	    header1 = header1.replace(/\$payee/g, objCheck.General.Nombre);
	    header1 = header1.replace(/\$checknum/g,number);
	    header1 = header1.replace(/\$amountwords/g, objCheck.General.CantidadLetra);
	    header1 = header1.replace(/\$amount/g, fnFormatCurrency(objCheck.General.Cantidad, objCheck.General.Symbol));
	    header1 = header1.replace(/\$concepto/g, objCheck.General.ConceptoPago);

		
	}
   
   
    var detail_impact = "";

    for (var i = 0; objCheck.Impacto.length <= top && objCheck.Impacto != null && i < objCheck.Impacto.length; i++)
	{
        var aux = detail1;
				
		if(aux != null &&  aux != '' && aux != undefined)
		{
			    aux = aux.replace(/\$detail_account/g, objCheck.Impacto[i].Cuenta);
		        aux = aux.replace(/\$detail_concept/g, objCheck.Impacto[i].Nombre);
		        aux = aux.replace(/\$detail_credit/g, fnFormatCurrency(objCheck.Impacto[i].Cargo, objCheck.General.Symbol));
		        aux = aux.replace(/\$detail_debit/g, fnFormatCurrency(objCheck.Impacto[i].Abono, objCheck.General.Symbol));
		        detail_impact += aux
		}
				       
    }

    for (var res = objCheck.Impacto.length + 1; res <= top; res++) 
	{
		var aux = detail1;
		if(aux != null &&  aux != '' && aux != undefined)
		{
			aux = aux.replace(/\$detail_account/g, "");
	        aux = aux.replace(/\$detail_concept/g, "");
	        aux = aux.replace(/\$detail_credit/g, "");
	        aux = aux.replace(/\$detail_debit/g, "");
	        detail_impact += aux;				
		}
     }

   if(body1 != null &&  body1 != '' && body1 != undefined)
   {
   	 body1 = body1.replace(/\$detail_impact/g, detail_impact);
     body1 = body1.replace(/\$checknum/g, number);
   	
   }
   
    if (objCheck.ApliPago != null && objCheck.ApliPago.length > 0) 
	{
        var detail_apply = "";

        for (var i = 0; objCheck.ApliPago != null && i < objCheck.ApliPago.length; i++) 
		{
            var aux = detail2;
					
			if(aux != null &&  aux != '' && aux != undefined)
			{
				    aux = aux.replace(/\$apply_date/g, objCheck.ApliPago[i].date);
		            aux = aux.replace(/\$apply_tranid/g, objCheck.ApliPago[i].refnum);
		            aux = aux.replace(/\$apply_amount/g, fnFormatCurrency(objCheck.ApliPago[i].amount, objCheck.General.Symbol));
		            detail_apply += aux;
				
			}
					
          
        }
      
	   if(body2 != null &&  body2 != '' && body2 != undefined)
	   {
	    	body2 = body2.replace(/\$detail_apply/g, detail_apply);
	   }
               
    }
    else 
	{
        body2 = "";
    }
 
      if(footer1 != null &&  footer1 != '' && footer1 != undefined)
	  {
	  	footer1 = footer1.replace(/\$total_credit/g, fnFormatCurrency(objCheck.Poliza.Cargo, objCheck.General.Symbol));
        footer1 = footer1.replace(/\$total_debit/g, fnFormatCurrency(objCheck.Poliza.Abono, objCheck.General.Symbol));
       
	  }
    
if (showlines == "F") 
{
	
	if(header1 != null &&  header1 != '' && header1 != undefined)
	{
		 header1 = header1.replace(/\"0\.4\"/g, '"0.0"');
	}
    if(footer1 != null &&  footer1 != '' && footer1 != undefined)
	{
		 footer1 = footer1.replace(/\"0\.4\"/g, '"0.0"');
	}
	if(body1 != null &&  body1 != '' && body1 != undefined)
	{
	   body1 = body1.replace(/\"0\.4\"/g, '"0.0"');
	}
	if(body2 != null &&  body2 != '' && body2 != undefined)
	{
		 body2 = body2.replace(/\"0\.4\"/g, '"0.0"');
	}
}
   	
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' HEADER 1  -->' + header1);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' BODY 1  -->' + body1);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' BODY 2  -->' + body2);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' FOOTER 1  -->' + footer1);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' FOOTER 2  -->' + footer2);
	nlapiLogExecution('DEBUG', 'getXmlToCheck',' FOOTER 3  -->' + footer3);
		 
    var retxml = "";
	
	if(header1 != null &&  header1 != '' && header1 != undefined)
	{
		retxml += header1;
	}
	if(body1 != null &&  body1 != '' && body1 != undefined)
	{
		 retxml += body1;
	}
	if(body2 != null &&  body2 != '' && body2 != undefined)
	{
		 retxml += body2;
	}
	if(footer1 != null &&  footer1 != '' && footer1 != undefined)
	{
		retxml += fnIsNull(footer1);
	}
	if(footer2 != null &&  footer2 != '' && footer2 != undefined)
	{
	   retxml += fnIsNull(footer2);	
	}
	if(footer3 != null &&  footer3 != '' && footer3 != undefined)
	{
	   retxml += fnIsNull(footer3);
	}
		   
    return retxml;

}

function getFormatCheck(formatid)
{
	var objFormat = null;
	
	if(formatid != null && formatid != "")
	{
		var record = nlapiLoadRecord("customrecord_checkformat", formatid);

        var xml = record.getFieldValue("custrecord_chkfrmt_xml");

		var header1 = record.getFieldValue("custrecord_chkfrmt_header1");
		var header2 = record.getFieldValue("custrecord_chkfrmt_header2");
		var header3 = record.getFieldValue("custrecord_chkfrmt_header3");
		
		var body1 = record.getFieldValue("custrecord_chkfrmt_body1");
		var body2 = record.getFieldValue("custrecord_chkfrmt_body2");
		var body3 = record.getFieldValue("custrecord_chkfrmt_body3");
		
		var footer1 = record.getFieldValue("custrecord_chkfrmt_footer1");
		var footer2 = record.getFieldValue("custrecord_chkfrmt_footer2");
		var footer3 = record.getFieldValue("custrecord_chkfrmt_footer3");
		
		var detail1 = record.getFieldValue("custrecord_chkfrmt_detail1");
		var detail2 = record.getFieldValue("custrecord_chkfrmt_detail2");
		var detail3 = record.getFieldValue("custrecord_chkfrmt_detail3");
		
		var numlines = record.getFieldValue("custrecord_chkfrmt_numlines");
		var showlines = record.getFieldValue("custrecord_chkfrmt_showlines");
		
		//--> Temporal
		var top = record.getFieldValue("custrecord_chkfrmt_top");
		var bottom = record.getFieldValue("custrecord_chkfrmt_bottom");
		var left = record.getFieldValue("custrecord_chkfrmt_left");
		var right = record.getFieldValue("custrecord_chkfrmt_right");
		
		objFormat = new TIAObjFormatCheck(xml, header1, header2, header3, body1, body2, body3, footer1, footer2, footer3, detail1, detail2, detail3, numlines, showlines, top, bottom, left, right);
	}
	
	return objFormat;
}



function getXmlToPrint(html, objformat) 
{
    var xml = objformat.xml;
    xml = xml.replace(/\$xml_body/g, html);
    xml = xml.replace(/\&/g, "\&amp;");
    return xml;
}

