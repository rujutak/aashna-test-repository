/*
 *---------------------------------------------------------------------------
 * Script File  : CLI_ValidateFunction.js
 * Script Type  : Library
 * Description  : Function of general use on suite scripts
 * Created      : 29 July 2013
 * Event        : NA
 *---------------------------------------------------------------------------
 */

/*--------------- Utility Functions To Manage Strings & Numbers ------------*/

function fnParseFloatOrZero(f)
{
   var r=parseFloat(f);
   return isNaN(r) ? 0 : r;
}

function fnParseIntOrZero(i)
{
   var r=parseInt(i);
   return isNaN(r) ? 0 : r;
}

function fnReplace(texto,s1,s2){
	return texto.split(s1).join(s2);
}

function fnIsNull(value)
{
	return (value == null) ? '' : value;
}

function fnIsNull2(value, replaceby)
{
	return (value == null) ? replaceby : value;
}

function fnLTrim(s){
	// Devuelve una cadena sin los espacios del principio
	var i=0;
	var j=0;
	
	// Busca el primer caracter <> de un espacio
	for(i=0; i<=s.length-1; i++)
		if(s.substring(i,i+1) != ' ' && s.substring(i,i+1) != ''){
			j=i;
			break;
		}
	return s.substring(j, s.length);
}

function fnRTrim(s)
{
	// Quita los espacios en blanco del final de la cadena
	var j=0;
	
	// Busca el �ltimo caracter <> de un espacio
	for(var i=s.length-1; i>-1; i--)
		if(s.substring(i,i+1) != ' ' &&  s.substring(i,i+1) != ''){
			j=i;
			break;
		}
	return s.substring(0, j+1);
}

function fnTrim(s)
{
	return nsoLTrim(nsoRTrim(s));
}

function fnRight(str, n)
{
      if (n <= 0)
          return "";
      else if (n > String(str).length)
          return str;
      else
   {
          var iLen = String(str).length;
          return String(str).substring(iLen, iLen - n);
      }
}

function fnLeft(str, n)
{
   if (n <= 0)
         return "";
   else if (n > String(str).length)
         return str;
   else
         return String(str).substring(0,n);

}

function fnFormatMoney(n, c, d, t) {
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? "," : d;
    t = t == undefined ? "." : t;
    s = n < 0 ? "-" : "";
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function fnFormatCurrency(number, symbol) {
    var value = fnIsNull(symbol) + fnFormatMoney(number, 2, '.', ',');
    return value;
}

/*--------------- Utility Functions To Getting Records ---------------*/

function fnGetEntityRecord(id)
{
	var itemrecord = null;

	var searchresults = nlapiSearchRecord('entity', null, new nlobjSearchFilter('internalid', null, 'is', id));
	
	if (searchresults != null && searchresults.length > 0)
	{
		itemrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId());
	}
	
	return itemrecord;
}

function fnGetItemRecord(id)
{
	var itemrecord = null;

	var searchresults = nlapiSearchRecord('item', null, new nlobjSearchFilter('internalid', null, 'is', id));
	
	if (searchresults != null && searchresults.length > 0)
	{
		itemrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId());
	}
	
	return itemrecord;
}

function fnGetTranRecord(id)
{
	var itemrecord = null;

	if (id != null && id != "")
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('internalid', null, 'is', id);
		filters[1] = new nlobjSearchFilter('mainline', null, 'is', "T");		
		var searchresults = nlapiSearchRecord('transaction', null, filters, null);
		
		if (searchresults != null && searchresults.length > 0)
		{
			itemrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId());
		}
	}
	
	return itemrecord;
}

function fnGetSubsidiaryRecord(id)
{
	var subsidiaryrecord = null;

	if (id != null && id != "")
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('internalid', null, 'is', id);
		var searchresults = nlapiSearchRecord('subsidiary', null, filters, null);
		
		if (searchresults != null && searchresults.length > 0)
		{
			subsidiaryrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId());
		}
	}
	
	return subsidiaryrecord;
}

function fnGetAccountingPeriod(id)
{
	var periodRecord = null;
	
	var filters = new Array();
	
	filters[0] = new nlobjSearchFilter("internalid", null, "anyof", id, null);
	
	var results = nlapiSearchRecord("accountingperiod", null, filters, null);	
	
	if(results != null && results.length > 0)
	{
		periodRecord = nlapiLoadRecord(results[0].getRecordType(), results[0].getId());
	}
	
	return periodRecord;	
}

function fnSearchAccountingPeriod(year, month)
{
    var period = null;
    var startdate = new Date();
    startdate.setDate(1);
    startdate.setFullYear(year);
    startdate.setMonth((month - 1))
    var enddate = nlapiAddMonths(startdate, 1);
    enddate = nlapiAddDays(enddate, -1);
    var filters = new Array();
    filters[0] = new nlobjSearchFilter("startdate", null, "onorafter", startdate, null);
    filters[1] = new nlobjSearchFilter("enddate", null, "onorbefore", enddate, null);
    var results = nlapiSearchRecord("accountingperiod", null, filters, null);
    if(results != null && results.length > 0)
    {
        period = results[0].getId();
    }
    return period;
}

/*--------------- Utility Functions To Manage Virtual Machines ---------------*/

function fnExistsItem(type, field, id)
{
	var wretval = false;

	var wtot_items = nlapiGetLineItemCount(type);
	
	for ( var i = 1; i <= wtot_items ; i++ )
	{
		var wid = nlapiGetLineItemValue(type, field, i);
		
		if(wid == id)
		{
			wretval = true;
			break;
		}
	}
	
	return wretval;
}

function fnIsCurrentLineItemDuplicated(type, fields)
{
	var isduplicated = false;
	var index = nlapiGetCurrentLineItemIndex(type);
	var count = nlapiGetLineItemCount(type);
	
	for ( var i = 1; i <= count; i++ )
	{
		if ( i != index)
		{
			isduplicated = true;
		
			for(var field in fields)
			{
				var value = nlapiGetLineItemValue(type, field, i);
				
				if ( value != fields[field])
				{
					isduplicated = false;
					break;
				}
			}
			
			if(isduplicated == true) break;
		}
	}
	
	return isduplicated;
}

/*--------------- Utility Functions To Manage Records ---------------*/

function fnExistsItemOnRecord(record, type, field, value)
{
	var retval = false;
	
	for(var i = 1; record != null && i <= record.getLineItemCount(type); i++)
	{
		var val = record.getLineItemValue(type, field, i);
		
		if(value == val)
		{
			retval = true;
			break;
		}
	}
	
	return retval;
}

function fnGetLineItemIndex(record, type, fields)
{
	var retval = -1;
	
	for ( var i = 1; record != null && i <= record.getLineItemCount(type); i++ )
	{
		var bexists = true;
		
		for(var field in fields)
		{
			var value = record.getLineItemValue(type, field, i);
			
			if ( value != fields[field])
			{
				bexists = false;
				break;
			}
		}
		
		if(bexists == true)
		{
			retval = i;
			break;
		}
	}
	
	return retval;
	
}

/*------------ Utility Functions To Manage Search Results ------------*/

function fnExistsItemInSearchResults(searchresults, field, value)
{
	var retval = false;

	for ( var i = 0; searchresults != null && i < searchresults.length ; i++ )
	{
		var fieldvalue = searchresults[i].getValue(field);
		
		if (fieldvalue == value)
		{
			retval = true;
			break;
		}
	}
	
	return retval;
}

function fnGetIndexInSearchResults(searchresults, field, value)
{
	var retval = -1;

	for ( var i = 0; searchresults != null && i < searchresults.length ; i++ )
	{
		var fieldvalue = searchresults[i].getValue(field);
		
		if (fieldvalue == value)
		{
			retval = i;
			break;
		}
	}
	
	return retval;
}

function fnGetValueOfSearchResults(searchresults, field, value, retfield)
{
	var retval = "";

	for ( var i = 0; searchresults != null && i < searchresults.length ; i++ )
	{
		var fieldvalue = searchresults[i].getValue(field);
		
		if (fieldvalue == value)
		{
			retval = searchresults[i].getValue(retfield);
			break;
		}
	}
	
	return retval;
}

/*---------------- Utility Functions To Manage Search Columns ----------------*/

function fnGetSearchColumn(arrcols, name, formula)
{
	var col = null;
	
	for(var k in arrcols)
	{
		if(arrcols[k].name == name && nsoIsNull(arrcols[k].formula) == nsoIsNull(formula))
		{
			col = arrcols[k];
			break;
		}
	}
	
	return col;
}

/*----------------- Utility Functions To Manage Year Records -----------------*/

function fnGetYearRecords()
{
	var filters = new Array();
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('name');	
	var searchresults = nlapiSearchRecord( 'customlist_year', null, null, columns );
	return searchresults;
}

function fnGetYearId(years, year)
{
	var retval = "";
	
	for(var i = 0; years != null && i < years.length; i++)
	{
		var wname = years[i].getValue("name");
		
		if(wname == year)
		{
			retval = years[i].getId();
			break;
		}
	}
	
	return retval;
}

/*----------------- Utility Functions To Manage Arrays -----------------*/

function fnDiffArrays(v, c, m)
{
    var d = [], e = -1, h, i, j, k;
    for(i = c.length, k = v.length; i--;){
        for(j = k; j && (h = c[i] !== v[--j]););
        h && (d[++e] = m ? i : c[i]);
    }
    return d;
}

function fnGetIndexItemInArray(array, value) 
{
    var retval = -1;

    for (var i = 0; array != null && i < array.length; i++) 
    {
        if (array[i] == value)
        {
            retval = i;
            break;
        }
    }

    return retval;
}

/*
diff = function(v, c, m){
    var d = [], e = -1, h, i, j, k;
    for(i = c.length, k = v.length; i--;){
        for(j = k; j && (h = c[i] !== v[--j]););
        h && (d[++e] = m ? i : c[i]);
    }
    return d;
};
*/

/*--------------------- Utility Functions To Manage Dates --------------------*/

function fnFormatDate(_fecha)
{
	var Fecha = '';
	
	if(_fecha != '')
	{
		var fecha = nlapiStringToDate(_fecha); 
		var dia = fecha.getDate();
		var mes = fecha.getMonth();
		var ano = fecha.getFullYear();
		//Fecha = dia + "    " + fnGetMonthName(mes) + "    "+ ano;
		Fecha = dia + " de " + fnGetMonthName(mes) + " de "+ ano;
		
	}
	
	return  Fecha;
}

function fnGetMonthName(Mont)
{
   var Month = "";

   switch(Mont)
  {
	  case 0: Month = "Enero";
	  		break;
	  case 1: Month = "Febrero";
	  		break;
	  case 2: Month = "Marzo";
	  		break;
	  case 3: Month = "Abril";
	  		break;
	  case 4: Month = "Mayo";
	  		break;
	  case 5: Month = "Junio";
	  		break;
	  case 6: Month = "Julio";
	  		break;
	  case 7: Month = "Agosto";
	  		break;
	  case 8: Month = "Septiembre";
	  		break;
	  case 9: Month = "Octubre";
	  		break;
	  case 10: Month = "Noviembre";
	  		break;
	  case 11: Month = "Diciembre";
	  		break;
   }
   return Month;
}

function fnInsertSpace(Num)
{
	var nbsp = "";
	
	for(var i = 1; i <=Num; i++ )
	{
		nbsp += "\xa0";
	}
	
	return nbsp;
}

/*--------------------- Utility Functions To Manage Files --------------------*/

function fnLoadFile(name) 
{ 
      var objFile = null; 
       
      var searchresults = nlapiSearchGlobal(name); 
       
      for(var i = 0; searchresults != null && i < searchresults.length; i++) 
      { 
         if (searchresults[i].getRecordType() == "file" && searchresults[i].getValue("name") == name) 
         { 
             objFile = nlapiLoadFile(searchresults[i].getId()); 
             break; 
         } 
      } 
       
      return objFile; 
}


/*------------------------------------------------------------------------------*/
function fnReplacer(key, value) {
    if (typeof value === 'number' && !isFinite(value)) {
        return String(value);
    }
    return value;
}
