/*
* Script File  : CLI_PrintCheck_ObjectCheck.js
* Script Type  : Library
* Description  : Object Transaction 
* Created      : 28-07-2012
* Event        : NA
* Main Method  : TIAObjCheck
*/

//--------------------------------------
//-- Objects to manage checks records --
//--------------------------------------
function TIAObjCheck(type, id) {
    this.type = type;
    this.id = id;
    this.NumfCheck = null;
    this.General = null;
    this.Impacto = null;
    this.ApliPago = null;
    this.Poliza = null;
    this.initObject = nlInitObjCheck;
    this.initObject(this.type, this.id);
}

function TIAObjGeneral() {
    this.Fecha = "";
    this.Nombre = "";
    this.Cantidad = "";
    this.CantidadLetra = "";
    this.ConceptoPago = "";
    this.Currency = "";
    this.Symbol = "";
}

function TIAObjPaymentApply(date, refnum, amount, total) {
    this.date = date;
    this.refnum = refnum;
    this.amount = amount;
    this.total = total;
}

function TIAObjImpact(cuenta, nombre, cargo, abono) {
    this.Cuenta = cuenta;
    this.Nombre = nombre;
    this.Cargo = cargo;
    this.Abono = abono;
}

function TIAObjPoliza() {
    this.Numero = "";
    this.Hecha = "";
    this.Remitida = "";
    this.Autorizada = "";
    this.Diario = "";
    this.Cargo = 0;
    this.Abono = 0;
}

//------------------------------------------------------
//-- Objects to manage requests-reponses from RESTlet --
//------------------------------------------------------

function TIAObjRequestChecksToPrint() {
    this.checks = new Array();
    this.fromindex = 0;
    this.format = null;
}

function TIAObjResponseChecksToPrint() {
    this.success = true;
    this.message = '';
    this.body = '';
    this.lastindex = 0;
}

function TIAObjCheckToPrint(id, tranid, type) {
    this.id = id;
    this.tranid = tranid;
    this.type = type;
}

//--------------------------------------
//-- Obtects to manage format records --
//--------------------------------------
function TIAObjFormatCheck(xml, header1, header2, header3, body1, body2, body3, footer1, footer2, footer3, detail1, detail2, detail3, numlines, showlines, top, bottom, left, right) {
    this.xml = xml;
    this.header1 = header1;
    this.header2 = header2;
    this.header3 = header3;
    this.body1 = body1;
    this.body2 = body2;
    this.body3 = body3;
    this.footer1 = footer1;
    this.footer2 = footer2;
    this.footer3 = footer3;
    this.detail1 = detail1;
    this.detail2 = detail2;
    this.detail3 = detail3;
    this.numlines = numlines;
    this.showlines = showlines;

    //--> Temporales
    this.top = top;
    this.bottom = bottom;
    this.left = left;
    this.right = right;
}


//------------------------------------------
//-- Auxiliary functions to check objects --
//------------------------------------------

function nlInitObjCheck(type, id) {
    this.General = nlGetGeneral(type, id);
    this.Impacto = nlGetImpacto(type, id);
    this.ApliPago = nlGetPayApply(id);
    this.Poliza = nlGetPoliza(type, id);

    if (this.Impacto != null && this.Impacto.length > 0) {
        var total_cargos = 0, total_abonos = 0;

        for (var i = 0; i < this.Impacto.length; i++) {
            total_cargos += fnParseFloatOrZero(this.Impacto[i].Cargo);
            total_abonos += fnParseFloatOrZero(this.Impacto[i].Abono);
        }

        this.Poliza.Cargo = total_cargos;
        this.Poliza.Abono = total_abonos;
    }
}

function nlGetGeneral(type, id) {

    var objGnrl = new TIAObjGeneral();
    var record = nlapiLoadRecord(type, id);
    var apply_multicurrency = record.getFieldValue('custbody_apply_multicurrency');
    var date = fnIsNull(record.getFieldValue('trandate'));
	var subsidiary = record.getFieldValue('subsidiary')
	//Agregado para quitar la conjunción "de" de la fecha JZG 07-08-2013
	if (subsidiary == 11 || subsidiary == 5 || subsidiary == 16 || subsidiary == 14 )
	{
	    var fecha = date.getDate();
			fecha+= '    ';
            fecha+= months[date.getMonth()];
            fecha+= '    ';
            fecha+= date.getFullYear();
	}	
	else {
	var fecha = fnIsNull(record.getFieldValue('trandate'));}
    
	//Agregado para quitar el prefijo MHER 08-02-2013
    var payee = fnIsNull(record.getFieldValue('entityname'))
    var payeesub = payee.substring(0,5);
    if (payeesub == 'HN-PC' || payeesub == 'HN-CR' || payeesub == 'HN-RP' || payeesub == 'NIPC-'){
    	var payee_long = payee.length;
        payee = payee.substring(6,payee_long);
    }

    /*
    var payee_long = payee.length;
    payee = payee.substring(6,payee_long);
    */
    //************************************************
    
    var alterpayee = fnIsNull(record.getFieldValue('custbody_beneficiary'))
    var arrpayee = payee.split('_');
    payee = (arrpayee.length > 0) ? arrpayee[arrpayee.length - 1] : '';
    if (alterpayee != '') { payee = alterpayee; }
    if  (type == 'vendorpayment') {
	    if (apply_multicurrency == 'F') {
		    var currency = fnIsNull(record.getFieldValue('currency'));
		    //GLUNA 25-10-2012. var currRecord = nlapiLoadRecord('currency', currency);
		
		    objGnrl.Fecha = fnIsNull(fecha);
		    objGnrl.Nombre = fnIsNull(payee);
		    objGnrl.Cantidad = fnIsNull(record.getFieldValue('usertotal'));
		    objGnrl.CantidadLetra = fnIsNull(record.getFieldValue('custbody_amount_words'));
		    objGnrl.ConceptoPago = fnIsNull(record.getFieldValue('memo'));
		
		    if (type == 'vendorpayment') {
		        objGnrl.Cantidad = fnIsNull(record.getFieldValue('total'));
		    }
	    }
	    else{
	        var currency = fnIsNull(record.getFieldValue('custbody_multicurrency_pay_currency'));
		    //GLUNA 25-10-2012. var currRecord = nlapiLoadRecord('currency', currency);
		
		    objGnrl.Fecha = fnIsNull(fecha);
		    objGnrl.Nombre = fnIsNull(payee);
		    objGnrl.Cantidad = fnIsNull(record.getFieldValue('custbody_multicurrency_pay_amount'));
		    objGnrl.CantidadLetra = fnIsNull(record.getFieldValue('custbody_multicurrency_ammount_letters'));
		    objGnrl.ConceptoPago = fnIsNull(record.getFieldValue('memo'));
		
		    if (type == 'vendorpayment') {
		        objGnrl.Cantidad = fnIsNull(record.getFieldValue('custbody_multicurrency_pay_amount'));
		    }
	    }
    }
    else{
    	var currency = fnIsNull(record.getFieldValue('currency'));
	    //GLUNA 25-10-2012. var currRecord = nlapiLoadRecord('currency', currency);
	
	    objGnrl.Fecha = fnIsNull(fecha);
	    objGnrl.Nombre = fnIsNull(payee);
	    objGnrl.Cantidad = fnIsNull(record.getFieldValue('usertotal'));
	    objGnrl.CantidadLetra = fnIsNull(record.getFieldValue('custbody_amount_words'));
	    objGnrl.ConceptoPago = fnIsNull(record.getFieldValue('memo'));
	
	    if (type == 'vendorpayment') {
	        objGnrl.Cantidad = fnIsNull(record.getFieldValue('total'));
	    }
    	
    }

    objGnrl.Currency = currency;
    objGnrl.Symbol =""; //GLUNA 25-10-2012. currRecord.getFieldValue('displaysymbol')

    return objGnrl;
}

function nlGetPayApply(id) {
    var itmconcep = new Array();

    var Applied = fnGetTranRecord(id);

    var Items = "";

    for (var i = 1; Applied != null && i <= Applied.getLineItemCount("apply"); i++) {
        var date = fnIsNull(Applied.getLineItemValue('apply', 'applydate', i));
        var refnum = fnIsNull(Applied.getLineItemValue('apply', 'refnum', i));
        var amount = fnIsNull(Applied.getLineItemValue('apply', 'amount', i));
        var total = fnIsNull(Applied.getLineItemValue('apply', 'total', i));
        var apply = fnIsNull(Applied.getLineItemValue('apply', 'apply', i));

        if (apply == 'T') {
            itmconcep[i - 1] = new TIAObjPaymentApply(date, refnum, amount, total);
        }
    }

    return itmconcep;
}

function nlGetImpacto(type, id) {
	
	//var recordpay = nlapiLoadRecord(type,id);
	//var applymulticurrency = recordpay.getFieldValue('custbody_apply_multicurrency');
	//if (applymulticurrency == 'F'){
	    var totalCargo = 0, totalAbono = 0;
	    var items = new Array();
	
	    var searchresults = nlGetImpactGLedger(type, id);
	
	    for (var x = 0; searchresults != null && x < searchresults.length; x++) {
	        var cuenta = nlGetNumberAccount(searchresults[x].getValue('account'));
	        var nombre = nlGetNameAccount(searchresults[x].getValue('account'));
	        var cargo = fnParseFloatOrZero(searchresults[x].getValue('creditamount'));
	        var abono = fnParseFloatOrZero(searchresults[x].getValue('debitamount'));
	        var fxamount = Math.abs(fnParseFloatOrZero(searchresults[x].getValue('fxamount')));
	
	
	        cargo = (cargo > 0 ? fxamount : 0);
	        abono = (abono > 0 ? fxamount : 0);
	
	        totalCargo = totalCargo + cargo;
	        totalAbono = totalAbono + abono;
	
	        items[x] = new TIAObjImpact(cuenta, nombre, cargo, abono);
	    }
	//}
	/*    
	else{
		
		//var totalCargo = 0, totalAbono = 0;
	    var items = new Array();
	    //var searchresults = nlGetImpactGLedger(type, id);
	    var filters = new Array();
	    filters[0] = new nlobjSearchFilter('custrecord_gl_multicurrency_id', null, 'anyOf', id);
	    
	    var columns = new Array();
	    columns[0] = new nlobjSearchColumn('custrecord_multicurrency_account');
	    columns[1] = new nlobjSearchColumn('custrecord_multicurrency_account_name');
	    columns[2] = new nlobjSearchColumn('custrecord_multicurrency_cargo');
	    columns[3] = new nlobjSearchColumn('custrecord_multicurrency_abono');


	    var searchresults = nlapiSearchRecord('customrecord_multicurrency_acct', null, filters, columns);
	
	    for (var x = 0; searchresults != null && x < searchresults.length; x++) {
	        //var cuenta = nlGetNumberAccount(searchresults[x].getValue('custrecord_multicurrency_account'));
	        //var nombre = nlGetNameAccount(searchresults[x].getValue('account'));
	    	var cuenta = searchresults[x].getValue('custrecord_multicurrency_account');
	        var nombre = searchresults[x].getValue('custrecord_multicurrency_account_name');
	        var cargo =  searchresults[x].getValue('custrecord_multicurrency_cargo');
	        var abono = searchresults[x].getValue('custrecord_multicurrency_abono');
	        //var fxamount = Math.abs(fnParseFloatOrZero(searchresults[x].getValue('fxamount')));
            
	        cargo = (cargo > 0 ? fxamount : 0);
	        abono = (abono > 0 ? fxamount : 0);
	
	        totalCargo = totalCargo + cargo;
	        totalAbono = totalAbono + abono;
	        
	        items[x] = new TIAObjImpact(cuenta, nombre, cargo, abono);
	    }
	}
	*/	

    return items;
}

function nlGetPoliza(type, id) {

    var objPoliza = new TIAObjPoliza();

    objPoliza.Numero = "Numero";
    objPoliza.Hecha = "Hecha";
    objPoliza.Remitida = "Remitida";
    objPoliza.Autorizada = "Autorizada";
    objPoliza.Diario = "Diario";
    objPoliza.Cargo = 0;
    objPoliza.Abono = 0;

    return objPoliza;
}

function nlGetImpactGLedger_Old(record_type, tranid) {
    if (tranid == null || tranid == '') return;
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('internalid', null, 'anyOf', tranid, null);
    var searchresults = nlapiSearchRecord(record_type, -36, filters, null);
    return searchresults;
}

function nlGetImpactGLedger(record_type, tranid) {
    if (tranid == null || tranid == '') return;

    var filters = new Array();
    filters[0] = new nlobjSearchFilter('internalid', null, 'anyOf', tranid);
    filters[1] = new nlobjSearchFilter('posting', null, 'is', 'T');

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('account');
    columns[1] = new nlobjSearchColumn('amount');
    columns[2] = new nlobjSearchColumn('creditamount');
    columns[3] = new nlobjSearchColumn('debitamount');
    columns[4] = new nlobjSearchColumn('posting');
    columns[5] = new nlobjSearchColumn('memo');
    columns[6] = new nlobjSearchColumn('name');
    columns[7] = new nlobjSearchColumn('subsidiary');
    columns[8] = new nlobjSearchColumn('department');
    columns[9] = new nlobjSearchColumn('class');
    columns[10] = new nlobjSearchColumn('location');
    columns[11] = new nlobjSearchColumn('fxamount');

    var results = nlapiSearchRecord('transaction', null, filters, columns);

    return results;
}

function nlGetNumberAccount(account_id) {
    var record = nlapiLoadRecord('account', account_id);
    var accountchain = '';
    accountchain = fnIsNull(record.getFieldValue('acctnumber'));
    return accountchain;
}

function nlGetNameAccount(account_id) {

    var record = nlapiLoadRecord('account', account_id);
    var name = '';
    name = fnIsNull(record.getFieldValue('acctname'));
    return name;
}

//------------------------------------------------------------------------------------------