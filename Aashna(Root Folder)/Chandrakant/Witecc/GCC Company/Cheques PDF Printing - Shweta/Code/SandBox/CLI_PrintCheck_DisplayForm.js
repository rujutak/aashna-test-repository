

/*
 * Script File  : CLI_PrintCheck_DisplayForm.js
 * Script Type  : Client Library
 * Description  : Functions to manage events from client side
 * Created      : 29 July 2013
 * Event        : -
 * Main Method  : -
 * ScriptId     : -
 * DeploymentId : -
 */
 
function set_numbers_of_checks()
{
	var first_check = nlapiGetFieldValue('custpage_firstchecknumber');
	
	if(first_check == null || first_check == "")
	{
		alert('Favor de proporcionar el primer # cheque');
		return;
	}
	
	var num_check = fnParseIntOrZero(first_check);
	
	for ( i = 1; i <= nlapiGetLineItemCount('custpage_sublistchecks'); i++)
	{
		var chkprint = nlapiGetLineItemValue('custpage_sublistchecks', 'print', i);

		if (chkprint == 'T') 
		{
		    nlapiSetLineItemValue('custpage_sublistchecks', 'tranid', i, num_check);
		    num_check++;
		}
		else 
		{
		    nlapiSetLineItemValue('custpage_sublistchecks', 'tranid', i, '');
		}
	}
}


