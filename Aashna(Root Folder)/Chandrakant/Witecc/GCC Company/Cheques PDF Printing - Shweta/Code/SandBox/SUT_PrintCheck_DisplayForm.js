/*
 * Script File  : SUT_PrintCheck_DisplayForm.js
 * Script Type  : Suitelet
 * Description  : Create form to print massively checks
 * Author       : Shweta
 * Created      : 29 July 2013
 * Event        : GET, POST
 * Main Method  : createform
 * ScriptId     : customscript_printcheck_displayform
 * DeploymentId : customdeploy_printcheck_displayform
 */

//---------------------
// M�todo principal ---
//---------------------
function displayform(request, response)
{
	var accountid = request.getParameter('custpage_account');
	var formatid = request.getParameter('custpage_format');
	var fchecknum = request.getParameter('custpage_firstchecknumber');
	
	nlapiLogExecution('DEBUG','displayform','  Account ID -->'+accountid);
	nlapiLogExecution('DEBUG','displayform',' Format ID -->'+formatid);
	nlapiLogExecution('DEBUG','displayform',' Check Num -->'+fchecknum);
		

	if(request.getMethod() == "GET")		 
	{	 
	   var acct = request.getParameter('pacct');
	   
	   createChecksForm(acct, fchecknum);
	   
	   nlapiLogExecution('DEBUG','displayform',' GET ...........');
	}
	
	if(request.getMethod() == "POST")
	{		
	  nlapiLogExecution('DEBUG','displayform',' POST  ...........');
	   
	    var objToPrint = getChecksToPrint();
		
		nlapiLogExecution('DEBUG','displayform',' objToPrint '+objToPrint)

	    if (objToPrint.checks == null || objToPrint.checks.length == 0) 
		{
			throw nlapiCreateError("ERROR", 'Debe seleccionar por lo menos un cheque.', false);
			 nlapiLogExecution('DEBUG','displayform',' ERROR  ...........');
		}
		else
		{
			nlapiLogExecution('DEBUG','displayform',' objToPrint.checks.toString() -->'+objToPrint.checks.toString());
			nlapiLogExecution('DEBUG','displayform',' objToPrint.numchecks.toString() -->'+objToPrint.numchecks.toString());
			nlapiLogExecution('DEBUG','displayform',' formatid -->'+formatid);
						
			// ******************* Call To Schedule SCript ********************
			
			var params = new Array();
			
			params['custscript_checksgcc'] = objToPrint.checks.toString() ;
			params['custscript_num_checks'] = objToPrint.numchecks.toString() ;
			params['custscript_format_id'] = formatid ;
						
			nlapiLogExecution('DEBUG','displayform',' Schedule Script Before Calling  .............. ');
			
			nlapiScheduleScript('customscript_sch_gcc_chequeprint_pdf','customdeploy1',params)
            nlapiLogExecution('DEBUG','displayform',' Schedule Script Called .............. ');
				
			response.write("<p><br/><br/><br/>The report has been submitted and it will be emailed to you as an attachment. You should receive that in few minutes.<br/>Please<a href=\'/app/site/hosting/scriptlet.nl?script=294&deploy=1 \'> Click Here<\a> to go back.<\/p>");	
				
				
						
			//nlapiSetRedirectURL('suitelet', 'customscript_tiamer_printcheck_displayf', 'customdeploy1', null, null);
			//nlapiLogExecution('DEBUG', 'displayform', ' suitelet Called : ' );
			
 		}
	}
}

function createChecksForm(accountid, fchecknum)
{
    var form = nlapiCreateForm("Impresi&oacute;n de Cheques");

	//--> Add script references 
    var file1 = fnLoadFile('CLI_PrintCheck_DisplayForm.js');
	var include = "\n<script type='text/javascript' src='" + file1.getURL() + "'></script>\n";
	var field = form.addField('custpage_html1', 'inlinehtml', '');
	field.setDefaultValue(include);	
	
	var file2 = fnLoadFile('CLI_PrintCheck_ObjectCheck.js');
	include = "\n<script type='text/javascript' src='" + file2.getURL() + "'></script>\n";
	field = form.addField('custpage_html2', 'inlinehtml', '');
	field.setDefaultValue(include);	
	
	var file3 = fnLoadFile('CLI_ValidateFunction.js');
	include = "\n<script type='text/javascript' src='" + file3.getURL() + "'></script>\n";
	field = form.addField('custpage_html3', 'inlinehtml', '');
	field.setDefaultValue(include);

	//--> Add tab
	var firstTab = form.addTab("custpage_tab1", "Facturas");
		
	//--> Create listbox field to accounts
	field = form.addField('custpage_account', 'select', 'Cuenta');
	form.getField('custpage_account').setMandatory(true); 
	field.addSelectOption('','');
	
	var vals = searchItemsAccount();
	
	for(var i = 0; vals != null && i < vals.length; i++)
	{
		var account = vals[i];
		var id		= account.getValue('internalid');
		var name 	= account.getValue('name');
		field.addSelectOption(id, name);
	}
	
	field.setDefaultValue(accountid);
	
	//--> Create listbox field to check format
	var formatid = getFormatCheckId(accountid);
	field = form.addField('custpage_format', 'select', 'Formato Cheques', 'customrecord_checkformat');
	field.setDisplayType('hidden');
	field.setDefaultValue(formatid);

	//--> Create field to manage event on client side 
	field = form.addField('custpage_refreshwindow','inlinehtml', 'refresh_HTML');
	var wscript = changeScriptSelect();
	field.setDefaultValue(wscript);
	
	//--> Add buttons to form
	form.addSubmitButton('Imprimir');
		 
    //--> Add sublist
	var sublist  = form.addSubList("custpage_sublistchecks", "list", "Cheques", firstTab);
	sublist.addMarkAllButtons();


	//--> Create textbox field to number of check
	form.addField("custpage_firstchecknumber", "integer", "No. Primer Cheque", null, firstTab).setLayoutType('startrow');
	form.getField("custpage_firstchecknumber").setMandatory(true);
	//form.getField.setDefaultValue(fchecknum);          
                      
	//--> Add Fileds To Sublist
	sublist.addField("print", "checkbox", "Imprimir");
	sublist.addField("date", "date", "Fecha");
	sublist.addField("beneficiary", "text", "Beneficiario");
	sublist.addField("beneficiaryaltern", "text", "Beneficiario Alterno");
    sublist.addField("transaction", "textarea", "Aplicado a");
	sublist.addField("type", "text", "Tipo");
	sublist.addField("recordtype", "text", "RecordType").setDisplayType('hidden');
	sublist.addField("moneda", "text", "Currency");
	sublist.addField("amount", "float", "Cantidad");
	sublist.addField("internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("tranid", "text", "No. Cheque").setDisplayType('entry');
	sublist.addButton("btn_updatechecks", 'Asignar n' + String.fromCharCode(250) + 'meros de cheque',  "set_numbers_of_checks();")
	
	//--> Fill sublist to transaction to print
	fillSublistChecks(accountid, sublist);

	//--> Response to client
	response.writePage(form);
}

function searchItemsAccount()
{
	var filters = new Array();
	filters[0]  = new nlobjSearchFilter('type', null, 'anyof', 'Bank');
	filters[1]  = new nlobjSearchFilter('isinactive', null, 'anyof', 'F');
	filters[2]  = new nlobjSearchFilter('custrecord_account_printchk_massive', null, 'is', 'T'); //Disponible impresi�n masiva chk
	
	var columns = new Array();
	columns[0]  = new nlobjSearchColumn('internalid');
	columns[1]  = new nlobjSearchColumn('name');
	
	var results = nlapiSearchRecord('account', null, filters, columns);
	
	return results;
}

function getFormatCheckId(acctid)
{
	var retval = null;
	
	if(acctid != null)
	{
		var filters = new Array();
		filters[0]  = new nlobjSearchFilter('custrecord_chkfrmt_account', null, 'anyof', acctid); 
		
		var columns = new Array();
		columns[0]  = new nlobjSearchColumn('internalid');
		columns[1]  = new nlobjSearchColumn('name');
		
		var results = nlapiSearchRecord('customrecord_checkformat', null, filters, columns);
		
		if(results != null && results.length > 0)
		{
			retval = results[0].getValue("internalid");
		}
	}
	
	return retval;
}

function fillSublistChecks(idcheckaccount, obj) {

    var arrtype = new Array();
    arrtype['vendorpayment'] = 'Pago Proveedor';
    arrtype['check'] = 'Cheque';
    arrtype['employee'] = 'Pago Empleado';

    var arrrecords = searchRecordsToPrint(idcheckaccount);

    var arrtrans = new Array();

    for (var i = 0; arrrecords != null && i < arrrecords.length; i++) {
        arrtrans[arrtrans.length] = arrrecords[i].getValue('internalid'); ;
    }

    var arrappliedtrans = searchApplyTrans(arrtrans);
		
    for (var i = 0; arrrecords != null && i < arrrecords.length; i++)
    {  
		var date = arrrecords[i].getValue('trandate');
		var beneficiary = arrrecords[i].getText('entity');
		var beneficiaryaltern = arrrecords[i].getValue('custbody_beneficiary');
		var type = arrrecords[i].getRecordType();
		var moneda = arrrecords[i].getText('currency');
		var total = arrrecords[i].getValue('fxamount');
		var id = arrrecords[i].getValue('internalid');
		var appliedtrans = getAppliedTrans(arrappliedtrans, id);
        var numline = parseInt(i + 1);
        var isprint = 'F';
		
		obj.setLineItemValue('print', numline, isprint);
		obj.setLineItemValue('date', numline, date);
		obj.setLineItemValue('beneficiary', numline, beneficiary);
		obj.setLineItemValue('beneficiaryaltern', numline, beneficiaryaltern);
		obj.setLineItemValue('type', numline, arrtype[type]);
		obj.setLineItemValue('recordtype', numline, type);
		obj.setLineItemValue('moneda', numline, moneda);
		obj.setLineItemValue('amount', numline, Math.abs(total));
		obj.setLineItemValue('internalid', numline, id);
		obj.setLineItemValue('transaction', numline, appliedtrans)	
    }
}

function getChecksToPrint()
{
    //-->
	var checks = new Array();
	var numchecks = new Array();
	var index = -1;
	var count = request.getLineItemCount('custpage_sublistchecks');
	
	for ( var i = 1; i <= count; i++ )
	{
		//-->
		var toprint = request.getLineItemValue('custpage_sublistchecks', 'print', i);
		
		if( toprint == 'T' )
		{
		    var internalid = request.getLineItemValue('custpage_sublistchecks', 'internalid', i);
		    var tranid = request.getLineItemValue('custpage_sublistchecks', 'tranid', i);		
			index++;
			checks[index] = internalid;
			numchecks[index] = tranid;			
		}	
	}

    var retObj = new Object();
    retObj.checks = checks;
    retObj.numchecks = numchecks;
    return retObj;
}

function searchRecordsToPrint(idaccount)
{
	if(idaccount == null || idaccount == "") return;
	
    var arrtypes = new Array();
    arrtypes[0] 	= "VendPymt";
    arrtypes[1] 	= "Check";
	arrtypes[2] 	= "employee";

	/*
    var filters   	= new Array();
    filters[0]    	= new nlobjSearchFilter('tobeprinted', null, 'is', 'T');
    filters[1]    	= new nlobjSearchFilter('account', null, 'anyof', idaccount);
    filters[2]    	= new nlobjSearchFilter('type', null, 'anyof', arrtypes);
    */
	
	var filterExpression =	[ [[ 'account', 'anyof', idaccount ],
		      					'or',
		    					[ 'custbody_foreign_account', 'anyof', idaccount ]],
		    					'and',
		      				    ['tobeprinted', 'is',  'T' ], 
		      				    'and',
		                        ['type', 'anyof',  arrtypes ] ] ;
		 
  
    var columns   	= new Array(); 
    columns[0]    	= new nlobjSearchColumn('trandate'); 
    columns[1]    	= new nlobjSearchColumn('entity');
	columns[2]    	= new nlobjSearchColumn('total');
    columns[3]    	= new nlobjSearchColumn('type'); 
    columns[4]    	= new nlobjSearchColumn('currency'); 
    columns[5]    	= new nlobjSearchColumn('total');
	columns[6]    	= new nlobjSearchColumn('internalid');
	columns[7]      = new nlobjSearchColumn('fxamount');
	columns[8]      = new nlobjSearchColumn('custbody_beneficiary');

    //var results = nlapiSearchRecord('transaction', null, filters, columns); 
	var results = nlapiSearchRecord('transaction', null, filterExpression, columns);
	
    return results;
}

function searchApplyTrans(trans) {

    if (trans == null || trans == "") return;

    var filters = new Array();
    filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', trans);
    filters[1] = new nlobjSearchFilter('mainline', 'appliedtotransaction', 'is', 'T');

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid', null, null);
    columns[1] = new nlobjSearchColumn('tranid', 'appliedtotransaction', null, null);
    columns[2] = new nlobjSearchColumn('type', 'appliedtotransaction', null, null);

    var results = nlapiSearchRecord('transaction', null, filters, columns);

    return results;
}

function getAppliedTrans(searchresults, id) {

    var retval = '';

    for (var i = 0; searchresults != null && i < searchresults.length; i++ ) {

        var internalid = searchresults[i].getValue('internalid');

        if (internalid == id) {
            retval += searchresults[i].getText('type', 'appliedtotransaction') + ' #' + searchresults[i].getValue('tranid', 'appliedtotransaction') + ' ';
        }
    }

    if (retval.length > 0) {
        retval = retval.substring(0, retval.length - 1);
    }

    return retval;

}


function changeScriptSelect()
{
    var url = nlapiResolveURL('SUITELET', 'customscript_tiamer_printcheck_displayf', 'customdeploy1');
	var wscript = "<script>";
	wscript += " var acc = document.forms['main_form'].elements['custpage_account'];";
	wscript += " acc.onchange = function() ";
	wscript += " { ";
	wscript += " var acct = nlapiGetFieldValue('custpage_account');";
	wscript += " window.location='" + url + "&pacct='  + acct;";
	wscript += " } ";
	wscript += " </script> ";	   	
	return wscript;
}