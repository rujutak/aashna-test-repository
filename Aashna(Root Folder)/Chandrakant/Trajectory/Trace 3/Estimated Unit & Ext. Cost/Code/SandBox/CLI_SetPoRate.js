// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_SetPoRate.js
	Author: Ashoksai
	Company:
	Date: 30/04/2013
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

    
     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY
	
	if(type == 'item') {
		var f_porate = nlapiGetCurrentLineItemValue('item','porate');
		//alert('f_porate :'+f_porate);
		
		var costEstimateType = nlapiGetCurrentLineItemValue('item','costestimatetype');
		//alert('costEstimateType :'+costEstimateType);
		
		if(costEstimateType != 'CUSTOM') {
			if(_logValidation(f_porate)) {
			var i_quantity = nlapiGetCurrentLineItemValue('item','quantity');
			//alert('i_quantity :'+i_quantity);
			
			var f_extCost = parseFloat(f_porate) * parseFloat(i_quantity);
			
			nlapiSetCurrentLineItemValue('item','costestimaterate',f_porate,false,true);
			nlapiSetCurrentLineItemValue('item','costestimate',f_extCost.toFixed(2),false,true);
			}
			
			else
			{
				nlapiSetCurrentLineItemValue('item','costestimaterate',0.0,false,true);
				nlapiSetCurrentLineItemValue('item','costestimate',0.0,false,true);
			}
		}
		
	}


	return true;

}

// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY
	
	if(type == 'item' && name == 'porate') {
		var f_porate = nlapiGetCurrentLineItemValue('item','porate');
		//alert('f_porate :'+f_porate);
		
		var costEstimateType = nlapiGetCurrentLineItemValue('item','costestimatetype');
		//alert('costEstimateType :'+costEstimateType);
		
		if(costEstimateType != 'CUSTOM') {
			if(_logValidation(f_porate)) {
			var i_quantity = nlapiGetCurrentLineItemValue('item','quantity');
			//alert('i_quantity :'+i_quantity);
			
			var f_extCost = parseFloat(f_porate) * parseFloat(i_quantity);
			
			nlapiSetCurrentLineItemValue('item','costestimaterate',f_porate,false,true);
			nlapiSetCurrentLineItemValue('item','costestimate',f_extCost.toFixed(2),false,true);
			}
			
			else
			{
				nlapiSetCurrentLineItemValue('item','costestimaterate',0.0,false,true);
				nlapiSetCurrentLineItemValue('item','costestimate',0.0,false,true);
			}
		}
	
	}
	
	if(type == 'item' && name == 'quantity') {
		var f_porate = nlapiGetCurrentLineItemValue('item','porate');
		//alert('f_porate :'+f_porate);
		
		var costEstimateType = nlapiGetCurrentLineItemValue('item','costestimatetype');  //CUSTOM
		//alert('costEstimateType :'+costEstimateType);
		
		
		if(costEstimateType != 'CUSTOM') {
			if(_logValidation(f_porate)) {
			var i_quantity = nlapiGetCurrentLineItemValue('item','quantity');
			//alert('i_quantity :'+i_quantity);
			
			var f_extCost = parseFloat(f_porate) * parseFloat(i_quantity);
			
			nlapiSetCurrentLineItemValue('item','costestimaterate',f_porate,false,true);
			nlapiSetCurrentLineItemValue('item','costestimate',f_extCost.toFixed(2),false,true);
			}
		}
			
	}

}

function _logValidation(value) {
	if(value!=null && value!='' && value!=undefined) {
		return true;
	} else {
		return false;
	}
}

// END FIELD CHANGED ================================================