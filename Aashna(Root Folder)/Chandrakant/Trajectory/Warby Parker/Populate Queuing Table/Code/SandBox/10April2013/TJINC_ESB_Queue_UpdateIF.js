/**
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: WAPn
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate: 20130328
* @DocumentationUrl:https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wapncr_esb_queue_update-js
* @NamingStandard: TJINC_NSJ-1-2
*/
var ESBUpdateQueue;
(function (ESBUpdateQueue) {
	//Docs URL: https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wapncr_esb_queue_update-js#TOC-Function:-TJINC_WAPNCR_addToQueue
    function TJINC_WAPNCR_addToQueue(recType, recId, isDelete) {
		var s_searchResult = TJINC_WAPNCR_searchRecord(recType)
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', 's_searchResult= ' +s_searchResult);
		if (s_searchResult == 1) {			
			if(recType == 'itemfulfillment' || recType == 'customrecord_prescription' || recType == 'returnauthorization') {			
				TJINC_WAPNCR_createCustRec(recType, recId, isDelete)								
			}
		    else if (recType == 'salesorder') {
				var i_userId = nlapiGetUser();
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_userId ' +i_userId);
				if (i_userId != 796565){
					TJINC_WAPNCR_createCustRec(recType, recId, isDelete)
				}
			}			
		}					       
    }
	//Docs URL: https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wapncr_esb_queue_update-js#TOC-Function:-TJINC_WAPNCR_createCustRec
	function TJINC_WAPNCR_createCustRec(recType, recId, isDelete) {
		var exists = nlapiSearchRecord('customrecord_esb_update_queue', null, [
        new nlobjSearchFilter('custrecord_eq_rec_type', null, 'is', recType), 
        new nlobjSearchFilter('custrecord_eq_rec_id', null, 'is', recId)
        ], new nlobjSearchColumn('custrecord_eq_deleted'));
        if (!exists) {
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'In create record');
            var rec = nlapiCreateRecord('customrecord_esb_update_queue');
            rec.setFieldValue('custrecord_eq_rec_type', recType);
            rec.setFieldValue('custrecord_eq_rec_id', recId);
            rec.setFieldValue('custrecord_eq_deleted', isDelete ? 'T' : 'F');
            var i_customRecId = nlapiSubmitRecord(rec);
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'New Custom Record ID= ' +i_customRecId);
        } else {
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= '+exists[0].getId());
			var o_custrecObj = nlapiLoadRecord('customrecord_esb_update_queue', exists[0].getId());
			nlapiSubmitRecord(o_custrecObj);			
            if(isDelete && 'T' != exists[0].getValue('custrecord_eq_deleted')) {
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= ' +exists[0].getId());
                nlapiSubmitField('customrecord_esb_update_queue', exists[0].getId(), 'custrecord_eq_deleted', 'T');
            }
        }	
	}
	//Docs URL: https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wapncr_esb_queue_update-js#TOC-Function:-TJINC_WAPNCR_searchRecord
	function TJINC_WAPNCR_searchRecord(recType) {
		var a_columns = new Array();
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('custrecord_inactiveesbupdatequeue', null, 'is', 'F');
		a_filters[1] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recType);
							
		a_columns[0] = new nlobjSearchColumn('internalid');
		a_columns[1] = new nlobjSearchColumn('custrecord_recordtype');
		a_columns[2] = new nlobjSearchColumn('custrecord_inactiveesbupdatequeue');		
		var o_Results = nlapiSearchRecord('customrecord_tocontrolupdatingesbque', null, a_filters, a_columns);
		if (o_Results != null) {
			return 1;
		}
		else {
			return 0;
		}
	}
	//Docs URL : https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wapncr_esb_queue_update-js#TOC-Function:-TJINC_WAPNCR_afterSubmit
    function TJINC_WAPNCR_afterSubmit(type) {
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'type= ' +type);
        if (type) {
			var s_Recordtype = nlapiGetRecordType();
			if (s_Recordtype == 'returnauthorization' && type == 'create') {
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 's_Recordtype= ' +s_Recordtype+' type= '+type);
			}
			else
			{
				TJINC_WAPNCR_addToQueue(nlapiGetRecordType(), nlapiGetRecordId(), type == 'delete'); 
			}				         
        }
    }
    ESBUpdateQueue.afterSubmit = TJINC_WAPNCR_afterSubmit;
})( ESBUpdateQueue || (ESBUpdateQueue = {}));
