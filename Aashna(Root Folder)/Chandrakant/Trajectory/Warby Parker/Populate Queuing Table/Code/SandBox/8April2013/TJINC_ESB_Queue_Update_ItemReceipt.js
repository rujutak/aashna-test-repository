/**
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: WAPn
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate: 20130403
* @DocumentationUrl: https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_esb_queue_update_itemreceipt-js
* @NamingStandard: TJINC_NSJ-1-2
*/


// BEGIN AFTER SUBMIT ==================================================
//https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_esb_queue_update_itemreceipt-js#TOC-Function:-TJINC_WAPNCR_afterSubmit
function TJINC_WAPNCR_afterSubmitRecord(type)
{
    if (type == 'create')
	{
        var i_ItemReceipt = nlapiGetRecordId();
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'Item Receipt ID=> ' + i_ItemReceipt);
		if(i_ItemReceipt!=null && i_ItemReceipt!='')
		{
			var o_IRObj = nlapiLoadRecord('itemreceipt',i_ItemReceipt);
		    var i_CreatedForm = o_IRObj.getFieldValue('createdfrom');
			try
			{
				nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'i_CreatedForm ID=> ' + i_CreatedForm);
				var o_RaObj = nlapiLoadRecord('returnauthorization',i_CreatedForm);
				if(o_RaObj!=null && o_RaObj!='')
				{
					var i_recType = o_RaObj.getFieldValue('custbody_sale_type');
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_recType= ' +i_recType);
					var s_recStatus = o_RaObj.getFieldValue('status');
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', 's_recStatus= ' +s_recStatus);
					if (i_recType == 1 && s_recStatus == 'Pending Refund') 
					{
						var exists = nlapiSearchRecord('customrecord_esb_update_queue', null, [
				        new nlobjSearchFilter('custrecord_eq_rec_type', null, 'is', 'returnauthorization'), 
				        new nlobjSearchFilter('custrecord_eq_rec_id', null, 'is', i_CreatedForm)
				        ], new nlobjSearchColumn('custrecord_eq_deleted'));
				        if (!exists) 
						{
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'In create record');
				            var rec = nlapiCreateRecord('customrecord_esb_update_queue');
				            rec.setFieldValue('custrecord_eq_rec_type', 'returnauthorization');
				            rec.setFieldValue('custrecord_eq_rec_id', i_CreatedForm);				           
				            var i_customRecId = nlapiSubmitRecord(rec);
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'New Custom Record ID= ' +i_customRecId);
				        } 
						else 
						{
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= '+exists[0].getId());
							var o_custrecObj = nlapiLoadRecord('customrecord_esb_update_queue', exists[0].getId());
							nlapiSubmitRecord(o_custrecObj);
				        }								
					}
				}
			}
			catch(ex)
			{
				nlapiLogExecution('DEBUG', 'Try - Catch', 'Error= ' +ex);
			}
		}		
    }	
    return true;
}