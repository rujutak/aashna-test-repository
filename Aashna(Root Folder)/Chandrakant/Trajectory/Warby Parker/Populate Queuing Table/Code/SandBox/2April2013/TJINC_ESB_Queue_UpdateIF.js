///<reference path='c:\development\Netsuite\SuiteScriptAPITS.d.ts'/>
var ESBUpdateQueue;
(function (ESBUpdateQueue) {
    function addToQueue(recType, recId, isDelete) {
		var s_searchResult = s_searchRecord(recType)
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', 's_searchResult= ' +s_searchResult);
		if(s_searchResult == 1)
		{
			var o_recObj = nlapiLoadRecord(recType, recId);		
			if(o_recObj!=null)
			{
				if(recType == 'itemfulfillment' || recType == 'customrecord_prescription')
				{			
					createCustRec(recType, recId, isDelete)								
				}
				else if(recType == 'returnauthorization')
				{				
					var i_recType = o_recObj.getFieldValue('custbody_sale_type');
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_recType= ' +i_recType);
					var s_recStatus = o_recObj.getFieldValue('status');
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', 's_recStatus= ' +s_recStatus);
					if(i_recType == 1 && s_recStatus == 'Pending Refund') {
						createCustRec(recType, recId, isDelete)			
					}				
				}
			    else if(recType == 'salesorder')
				{
					var i_userId = nlapiGetUser();
					nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_userId ' +i_userId);
					if(i_userId != 796565){
						createCustRec(recType, recId, isDelete)
					}
				}			
			}
		}					       
    }
	function createCustRec(recType, recId, isDelete)
	{
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'In createCustRec Function');
		var exists = nlapiSearchRecord('customrecord_esb_update_queue', null, [
        new nlobjSearchFilter('custrecord_eq_rec_type', null, 'is', recType), 
        new nlobjSearchFilter('custrecord_eq_rec_id', null, 'is', recId)
        ], new nlobjSearchColumn('custrecord_eq_deleted'));
        if(!exists) {
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'In create record');
            var rec = nlapiCreateRecord('customrecord_esb_update_queue');
            rec.setFieldValue('custrecord_eq_rec_type', recType);
            rec.setFieldValue('custrecord_eq_rec_id', recId);
            rec.setFieldValue('custrecord_eq_deleted', isDelete ? 'T' : 'F');
            var i_customRecId = nlapiSubmitRecord(rec);
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= ' +i_customRecId);
        } else {
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= '+exists[0].getId());
			var o_custrecObj = nlapiLoadRecord('customrecord_esb_update_queue', exists[0].getId());
			nlapiSubmitRecord(o_custrecObj);			
            if(isDelete && 'T' != exists[0].getValue('custrecord_eq_deleted')) {
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'Custom Record ID= ' +exists[0].getId());
                nlapiSubmitField('customrecord_esb_update_queue', exists[0].getId(), 'custrecord_eq_deleted', 'T');
            }
        }	
	}
	function s_searchRecord(recType)
	{
		var a_columns = new Array();
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('custrecord_inactiveesbupdatequeue', null, 'is', 'F');
		a_filters[1] = new nlobjSearchFilter('custrecord_recordtype', null, 'is', recType);
							
		a_columns[0] = new nlobjSearchColumn('internalid');
		a_columns[1] = new nlobjSearchColumn('custrecord_recordtype');
		a_columns[2] = new nlobjSearchColumn('custrecord_inactiveesbupdatequeue');		
		var o_Results = nlapiSearchRecord('customrecord_tocontrolupdatingesbque', null, a_filters, a_columns);
		if(o_Results != null)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
    function afterSubmit(type) {
        if(type) {
            addToQueue(nlapiGetRecordType(), nlapiGetRecordId(), type == 'delete');
        }
    }
    ESBUpdateQueue.afterSubmit = afterSubmit;
})(ESBUpdateQueue || (ESBUpdateQueue = {}));
