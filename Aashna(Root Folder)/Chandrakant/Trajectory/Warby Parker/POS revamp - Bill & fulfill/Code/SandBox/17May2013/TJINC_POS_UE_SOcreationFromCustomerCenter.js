/**
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: WAPn
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate: 20130410
* @DocumentationUrl:https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_pos_ue_socreationfromcustomercenter-js
* @NamingStandard: TJINC_NSJ-1-2
*/

var POS_GIFT_CARD_PARENT_ID;

//URL : https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_pos_ue_socreationfromcustomercenter-js#TOC-Function:-TJINC_POS_afterSubmitRecord
function TJINC_POS_afterSubmitRecord(type,form)
{
	var context = nlapiGetContext();
    var usageBegin = context.getRemainingUsage();
    nlapiLogExecution('DEBUG', 'schedulerFunction','usageBegin ==' + usageBegin);
	nlapiLogExecution('DEBUG', 'afterSubmit', 'type= ' + type);
	if(type != 'delete')
	{	
		if (nlapiGetContext().getEnvironment() == 'PRODUCTION') {
			POS_GIFT_CARD_PARENT_ID = 4275; //production internal ID
		} else {
			POS_GIFT_CARD_PARENT_ID = 4250; //SB1 internal ID
		}
		var i_SOid = nlapiGetRecordId();
		var o_SOobj = nlapiLoadRecord('salesorder', i_SOid);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Sales Order ID= ' + i_SOid);						
		var i_CC_AUTH_VALID = o_SOobj.getFieldValue('custbody_tjinc_cc_auth_valid');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'i_CC_AUTH_VALID= ' + i_CC_AUTH_VALID);		
		if ((type  == 'create' || (type=='edit' && i_CC_AUTH_VALID == 'T'))) 
		{
		if (i_SOid != null && i_SOid != '') {
			try 
			{
				var i_LoginCustomer = o_SOobj.getFieldValue('entity');
				nlapiLogExecution('DEBUG', 'afterSubmit', 'i_LoginCustomer= ' + i_LoginCustomer);
				var s_Source = o_SOobj.getFieldValue('source');
				nlapiLogExecution('DEBUG', 'afterSubmit', 'Source= ' + s_Source);
				var s_CC_Failed = o_SOobj.getFieldValue('custbody_tjinc_cc_no_auth');
				nlapiLogExecution('DEBUG', 'afterSubmit', 'CC_Failed= ' + s_CC_Failed);
				var s_PaymentEventResult = o_SOobj.getFieldValue('paymenteventresult');				
				nlapiLogExecution('DEBUG', 'afterSubmit', 'PaymentEventResult= ' + s_PaymentEventResult);
				
				if(s_CC_Failed != 'T' && s_PaymentEventResult != 'HOLD')
				{
					o_SOobj.setFieldValue('custbody_tjinc_cc_auth_valid', 'F');
					if (s_Source == 'Customer Center') {
						var s_CustomerName = o_SOobj.getFieldValue('custbody_tjinc_customer_name');
						nlapiLogExecution('DEBUG', 'afterSubmit', 'Customer Name= ' + s_CustomerName);						
						var s_CustomerEmail = o_SOobj.getFieldValue('custbody_tjinc_email');
						nlapiLogExecution('DEBUG', 'afterSubmit', 'Customer Email= ' + s_CustomerEmail);
						
						var b_Email_Opt_In = o_SOobj.getFieldValue('custbody_tjinc_email_opt_in');
						nlapiLogExecution('DEBUG', 'afterSubmit', 'Email_Opt_In= ' + b_Email_Opt_In);
						
						if (s_CustomerName != null && s_CustomerEmail != null) {
							var a_Name = new Array();
							a_Name = s_CustomerName.split(' ');
							nlapiLogExecution('DEBUG', 'afterSubmit', 'First Name= ' + a_Name[1] + ' Last Name= ' + a_Name[0]);
							
							if (a_Name[0] != null && a_Name[1] != null) {
								var a_filters = new Array();
								var a_columns = new Array();
								a_filters[0] = new nlobjSearchFilter('firstname', null, 'is', a_Name[1]);
								a_filters[1] = new nlobjSearchFilter('lastname', null, 'is', a_Name[0]);
								a_filters[2] = new nlobjSearchFilter('email', null, 'is', s_CustomerEmail);
								
								a_columns[0] = new nlobjSearchColumn('internalid');
								var results = nlapiSearchRecord('customer', null, a_filters, a_columns);
								
								if (results != null) {
									nlapiLogExecution('DEBUG', 'afterSubmit if loop', 'if loop');								
									var i_CustomerID = results[0].getValue('internalid');
									var o_CustomerRec = nlapiLoadRecord('customer', i_CustomerID);
									if(b_Email_Opt_In == 'T') {
										o_CustomerRec.setFieldValue('globalsubscriptionstatus', 1);										
									} 
									else {
										o_CustomerRec.setFieldValue('globalsubscriptionstatus', 2);
									}
									
									var i_RxName = o_SOobj.getFieldValue('custbody_tjinc_prescription_name');
									var i_OD_Sph = o_SOobj.getFieldValue('custbody_tjinc_right_rx');
									var i_OS_Sph = o_SOobj.getFieldValue('custbody_tjinc_left_rx');
									var i_OD_Cyl = o_SOobj.getFieldValue('custbody_tjinc_right_cyl');
									var i_OS_Cyl = o_SOobj.getFieldValue('custbody_tjinc_left_cyl');
									var i_OD_Axis = o_SOobj.getFieldValue('custbody_tjinc_right_axis');
									var i_OS_Axis = o_SOobj.getFieldValue('custbody_tjinc_left_axis');
									var i_Right_PD = o_SOobj.getFieldValue('custbody_tjinc_right_pd');
									var i_Left_PD = o_SOobj.getFieldValue('custbody_tjinc_left_pd');
									var i_Exp_Date = o_SOobj.getFieldValue('custbody_tjinc_expiration_date');
									var i_Dr_Name = o_SOobj.getFieldValue('custbody_tjinc_dr_name');
									var i_Dr_Number = o_SOobj.getFieldValue('custbody_tjinc_dr_number');
									var i_Dr_WillfaxOrEmail = o_SOobj.getFieldValue('custbody_posfaxemail');									
									var b_Needs_HI = o_SOobj.getFieldValue('custbody_tjinc_needs_hi');
									var b_Date_of_Birth = o_SOobj.getFieldValue('custbody_tjinc_dob');
									
									var o_CustomerPrescription = nlapiCreateRecord('customrecord_prescription');
									var i_flag = 0;
									if (i_RxName != null && i_RxName != '') {										
										o_CustomerPrescription.setFieldValue('name', i_RxName);
									}
									if (i_OD_Sph != null && i_OD_Sph != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_rx', i_OD_Sph);
									}
									if (i_OS_Sph != null && i_OS_Sph != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_rx', i_OS_Sph);
									}
									if (i_OD_Cyl != null && i_OD_Cyl != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_cyl', i_OD_Cyl);
									}
									if (i_OS_Cyl != null && i_OS_Cyl != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_cyl', i_OS_Cyl);
									}
									if (i_OD_Axis != null && i_OD_Axis != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_axis', i_OD_Axis);
									}
									if (i_OS_Axis != null && i_OS_Axis != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_axis', i_OS_Axis);
									}
									if (i_Right_PD != null && i_Right_PD != undefined) {
										i_flag = 1;									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_pd_od', i_Right_PD);
									}
									if (i_Left_PD != null && i_Left_PD != undefined) {
										i_flag = 1;									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_pd_os', i_Left_PD);
									}
									if (i_Exp_Date != null && i_Exp_Date != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_expires', i_Exp_Date);
									}
									if (i_Dr_Name != null && i_Dr_Name != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_verify_name', i_Dr_Name);
									}
									if (i_Dr_Number != null && i_Dr_Number != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_clinic_phone', i_Dr_Number);
									}
									if (i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '') {
										i_flag = 1;
										//o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_clinic_phone', i_Dr_Number);
									}
									if (b_Needs_HI != null && b_Needs_HI != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prefer_hi_index', b_Needs_HI);
									}
									if (b_Date_of_Birth != null && b_Date_of_Birth != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_verify_dob', b_Date_of_Birth);
									}
									var i_SubmitCustomerPrescription = '';
									if (i_flag == 1) {
										if (i_CustomerID != null && i_CustomerID != undefined) {
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_customer', i_CustomerID);
										}
										if(((i_OD_Sph != null && i_OD_Sph != '') || (i_OS_Sph != null && i_OS_Sph != '')) && i_Right_PD != null && i_Right_PD != '' && i_Left_PD != null && i_Left_PD != '')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 2);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 9);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'T');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 9);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit if', '1.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
										else if(i_Dr_Name != null && i_Dr_Name != '' && i_Dr_Number != null && i_Dr_Number != '')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 1);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 2);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'F');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 3);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit if', '2.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
										else if(i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '' && i_Dr_WillfaxOrEmail != 'F')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 1);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 5);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'F');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 5);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit if', '3.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
										else if((i_OD_Sph != null && i_OD_Sph != '') || (i_OS_Sph != null && i_OS_Sph != '') || (i_Right_PD != null && i_Right_PD != '') || (i_Left_PD != null && i_Left_PD != '') || (i_Dr_Name != null && i_Dr_Name != '') || (i_Dr_Number != null && i_Dr_Number != '') || (i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '' && i_Dr_WillfaxOrEmail != 'F'))
										{
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit if', '4.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
									}
									var s_ShipAddressee = o_SOobj.getFieldValue('custbody_tjinc_ship_addressee');								
									var s_ShipPhone = o_SOobj.getFieldValue('custbody_tjinc_ship_phone');								
									var s_ShipAddress1 = o_SOobj.getFieldValue('custbody_tjinc_ship_address1');								
									var s_ShipAddress2 = o_SOobj.getFieldValue('custbody_tjinc_ship_address2');								
									var s_ShipCity = o_SOobj.getFieldValue('custbody_tjinc_ship_city');								
									var s_ShipState = o_SOobj.getFieldValue('custbody_tjinc_ship_state');								
									var s_ShipZip = o_SOobj.getFieldValue('custbody_tjinc_ship_zip');
									
									if ((s_ShipAddress1 != null && s_ShipAddress1 != '') || (s_ShipCity != null && s_ShipCity != '')) {
										o_CustomerRec.selectNewLineItem('addressbook');
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addressee', s_ShipAddressee);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'phone', s_ShipPhone);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addr1', s_ShipAddress1);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addr2', s_ShipAddress2);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'city', s_ShipCity);
										//o_CustomerRec.setCurrentLineItemValue('addressbook', 'state', s_ShipState);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'zip', s_ShipZip);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
										o_CustomerRec.commitLineItem('addressbook');
									}
									var s_ShipToStore= o_SOobj.getFieldValue('custbody_posshiptostore');								
									if(s_ShipToStore == 'T')
									{
										var i_Location = o_SOobj.getFieldValue('location');
										if(i_Location != null && i_Location != '')
										{
											var o_LocationObj = nlapiLoadRecord('location', i_Location);
											if(o_LocationObj != null) {
												var s_LocationAddressee = s_ShipAddressee;
												var s_LocationPhone = o_LocationObj.getFieldValue('addrphone');
												var s_LocationAddress1 = o_LocationObj.getFieldValue('addr1');
												var s_LocationAddress2 = o_LocationObj.getFieldValue('addr2');
												var s_LocationCity = o_LocationObj.getFieldValue('city');
												var s_LocationState = o_LocationObj.getFieldValue('state');
												var s_LocationZip = o_LocationObj.getFieldValue('zip');
												if ((s_LocationAddress1 != '' && s_LocationAddress1 != '') || (s_LocationCity != '' && s_LocationCity != ''))
												{	
													var ShipAddressArray = [s_LocationAddressee, 'Warby Parker', s_LocationAddress1, s_LocationAddress2, s_LocationCity, s_LocationState, s_LocationZip];													
													var s_shipaddress='';
													for(var i=0;i<ShipAddressArray.length;i++)
													{
														if(i==0 && ShipAddressArray[i] != null)
														{
															s_shipaddress=ShipAddressArray[i];
														}
														else
														{
															if(ShipAddressArray[i] != null)
															{s_shipaddress=s_shipaddress+'\n'+ShipAddressArray[i];}															
														}
													}
													o_SOobj.setFieldValue('shipaddressee', s_LocationAddressee);
													o_SOobj.setFieldValue('shipattention', 'Warby Parker');
													o_SOobj.setFieldValue('shipaddr1', s_LocationAddress1);
													o_SOobj.setFieldValue('shipaddr2', s_LocationAddress2);
													o_SOobj.setFieldValue('shipcity', s_LocationCity);
													o_SOobj.setFieldValue('shipzip', s_LocationZip);
													o_SOobj.setFieldValue('shipaddress', s_shipaddress);																									
												}											
											}											
										}
									}																									
									var i_CC_Number = o_SOobj.getFieldValue('ccnumber');
									//nlapiLogExecution('DEBUG', 'afterSubmit', 'CC_Number= ' + i_CC_Number);
									var i_ExpDate = o_SOobj.getFieldValue('ccexpiredate');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'ExpDate= ' + i_ExpDate);
									var i_CardholderName = o_SOobj.getFieldValue('ccname');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'CardholderName= ' + i_CardholderName);
									var i_Promotion = o_SOobj.getFieldValue('promocode');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'Promotion= ' + i_Promotion);
									var i_CardType = o_SOobj.getFieldValue('paymentmethod');								
									nlapiLogExecution('DEBUG', 'afterSubmit', 'CardType= ' + i_CardType);
																
									if (i_CC_Number != null) {
										if(i_CardType != 12 && i_CardType != 1)
										{
											o_CustomerRec.selectNewLineItem('creditcards');
											o_CustomerRec.setCurrentLineItemValue('creditcards', 'ccnumber', i_CC_Number);
											o_CustomerRec.setCurrentLineItemValue('creditcards', 'ccexpiredate', i_ExpDate);
											o_CustomerRec.setCurrentLineItemValue('creditcards', 'ccname', i_CardholderName);
											o_CustomerRec.setCurrentLineItemValue('creditcards', 'paymentmethod', i_CardType);
											o_CustomerRec.commitLineItem('creditcards');
											
											o_SOobj.setFieldValue('ccnumber', i_CC_Number);
											o_SOobj.setFieldValue('ccexpiredate', i_ExpDate);
											o_SOobj.setFieldValue('ccname', i_CardholderName);
											o_SOobj.setFieldValue('paymentmethod', i_CardType);
										}										
									}
									var i_submitedCustomer = nlapiSubmitRecord(o_CustomerRec, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'Updated Customer= ' + i_submitedCustomer);
									o_SOobj.setFieldValue('entity', i_submitedCustomer);
									var i_PrefCC = nlapiLookupField('customer',i_LoginCustomer, 'prefccprocessor');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'PrefCC= ' + i_PrefCC);
									if(i_PrefCC != '' && i_PrefCC != null) {
										o_SOobj.setFieldValue('creditcardprocessor', i_PrefCC);
									}
									if(i_Promotion != '' && i_Promotion != null) {
										o_SOobj.setFieldValue('promocode', i_Promotion);
									}
									if(i_SubmitCustomerPrescription != '' && i_SubmitCustomerPrescription != null) {
										o_SOobj.setFieldValue('custbody_prescription', i_SubmitCustomerPrescription);
									}
									var i_billfulfill = o_SOobj.getFieldValue('custbody_tjinc_billfulfill');
									nlapiLogExecution('DEBUG', 'AfterSubmit', 'Bill & Fulfill= ' + i_billfulfill);
									if(i_billfulfill == 'T')
									{
										o_SOobj.setFieldValue('custbody_order_on_hold', 'F');
										o_SOobj.setFieldValue('orderstatus', 'B');
									}									
									var i_SalesOrderID = nlapiSubmitRecord(o_SOobj, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'i_SalesOrderID= ' + i_SalesOrderID);
									
									var o_SOobj = nlapiLoadRecord('salesorder', i_SalesOrderID);
									//function call for setting the Transaction Type and bill & fullfillment
									var o_OrderObj = f_setTransTypeandBillandFulfill(o_SOobj, POS_GIFT_CARD_PARENT_ID, i_SOid);
									/*
									o_OrderObj.setFieldValue('custbodytjinc_pos_order_complete', 'T');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'OPS Order Complete set as true');
									var i_SalesOrderID = nlapiSubmitRecord(o_OrderObj, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'i_SalesOrderID= ' + i_SalesOrderID);
									*/
								}
								else {
									nlapiLogExecution('DEBUG', 'afterSubmit else loop', 'else loop');
									var o_CustomerRec = nlapiCreateRecord('customer');
									o_CustomerRec.setFieldValue('firstname', a_Name[1]);
									o_CustomerRec.setFieldValue('lastname', a_Name[0]);
									o_CustomerRec.setFieldValue('email', s_CustomerEmail);
									if(b_Email_Opt_In == 'T') {
										o_CustomerRec.setFieldValue('globalsubscriptionstatus', 1);										
									} 
									else {
										o_CustomerRec.setFieldValue('globalsubscriptionstatus', 2);
									}									
									var i_RxName = o_SOobj.getFieldValue('custbody_tjinc_prescription_name');
									var i_OD_Sph = o_SOobj.getFieldValue('custbody_tjinc_right_rx');
									var i_OS_Sph = o_SOobj.getFieldValue('custbody_tjinc_left_rx');
									var i_OD_Cyl = o_SOobj.getFieldValue('custbody_tjinc_right_cyl');
									var i_OS_Cyl = o_SOobj.getFieldValue('custbody_tjinc_left_cyl');
									var i_OD_Axis = o_SOobj.getFieldValue('custbody_tjinc_right_axis');
									var i_OS_Axis = o_SOobj.getFieldValue('custbody_tjinc_left_axis');
									var i_Right_PD = o_SOobj.getFieldValue('custbody_tjinc_right_pd');
									var i_Left_PD = o_SOobj.getFieldValue('custbody_tjinc_left_pd');
									var i_Exp_Date = o_SOobj.getFieldValue('custbody_tjinc_expiration_date');
									var i_Dr_Name = o_SOobj.getFieldValue('custbody_tjinc_dr_name');									
									var i_Dr_Number = o_SOobj.getFieldValue('custbody_tjinc_dr_number');
									var i_Dr_WillfaxOrEmail = o_SOobj.getFieldValue('custbody_posfaxemail');
									nlapiLogExecution('DEBUG', 'afterSubmit if', 'i_Dr_WillfaxOrEmail ' + i_Dr_WillfaxOrEmail);
									var b_Needs_HI = o_SOobj.getFieldValue('custbody_tjinc_needs_hi');
									var b_Date_of_Birth = o_SOobj.getFieldValue('custbody_tjinc_dob');
									
									var o_CustomerPrescription = nlapiCreateRecord('customrecord_prescription');
									var i_flag = 0;
									if (i_RxName != null && i_RxName != '') {										
										o_CustomerPrescription.setFieldValue('name', i_RxName);
									}
									if (i_OD_Sph != null && i_OD_Sph != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_rx', i_OD_Sph);
									}
									if (i_OS_Sph != null && i_OS_Sph != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_rx', i_OS_Sph);
									}
									if (i_OD_Cyl != null && i_OD_Cyl != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_cyl', i_OD_Cyl);
									}
									if (i_OS_Cyl != null && i_OS_Cyl != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_cyl', i_OS_Cyl);
									}
									if (i_OD_Axis != null && i_OD_Axis != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_right_axis', i_OD_Axis);
									}
									if (i_OS_Axis != null && i_OS_Axis != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_left_axis', i_OS_Axis);
									}
									if (i_Right_PD != null && i_Right_PD != '') {
										i_flag = 1;									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_pd_od', i_Right_PD);
									}
									if (i_Left_PD != null && i_Left_PD != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_pd_os', i_Left_PD);
									}
									if (i_Exp_Date != null && i_Exp_Date != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_expires', i_Exp_Date);
									}
									if (i_Dr_Name != null && i_Dr_Name != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_verify_name', i_Dr_Name);
									}
									if (i_Dr_Number != null && i_Dr_Number != '') {
										i_flag = 1;
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_clinic_phone', i_Dr_Number);
									}
									if (i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '') {
										i_flag = 1;
										//o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_clinic_phone', i_Dr_Number);
									}
									if (b_Needs_HI != null && b_Needs_HI != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prefer_hi_index', b_Needs_HI);
									}
									if (b_Date_of_Birth != null && b_Date_of_Birth != undefined) {									
										o_CustomerPrescription.setFieldValue('custrecord_prescrip_dr_verify_dob', b_Date_of_Birth);
									}									
									var s_ShipAddressee = o_SOobj.getFieldValue('custbody_tjinc_ship_addressee');								
									var s_ShipPhone = o_SOobj.getFieldValue('custbody_tjinc_ship_phone');								
									var s_ShipAddress1 = o_SOobj.getFieldValue('custbody_tjinc_ship_address1');								
									var s_ShipAddress2 = o_SOobj.getFieldValue('custbody_tjinc_ship_address2');								
									var s_ShipCity = o_SOobj.getFieldValue('custbody_tjinc_ship_city');								
									var s_ShipState = o_SOobj.getFieldValue('custbody_tjinc_ship_state');								
									var s_ShipZip = o_SOobj.getFieldValue('custbody_tjinc_ship_zip');																
									if ((s_ShipAddress1 != null && s_ShipAddress1 != '') || (s_ShipCity != null && s_ShipCity != '')) {
										o_CustomerRec.selectNewLineItem('addressbook');
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addressee', s_ShipAddressee);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'phone', s_ShipPhone);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addr1', s_ShipAddress1);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'addr2', s_ShipAddress2);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'city', s_ShipCity);
										//o_CustomerRec.setCurrentLineItemValue('addressbook', 'state', s_ShipState);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'zip', s_ShipZip);
										o_CustomerRec.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
										o_CustomerRec.commitLineItem('addressbook');
									}								
									var s_ShipToStore= o_SOobj.getFieldValue('custbody_posshiptostore');								
									if(s_ShipToStore == 'T')
									{
										var i_Location = o_SOobj.getFieldValue('location');
										if(i_Location != null && i_Location != '')
										{
											var o_LocationObj = nlapiLoadRecord('location', i_Location);
											if(o_LocationObj != null) {
												var s_ShipAddresseeLoc = s_ShipAddressee;
												var s_ShipPhoneLoc = o_LocationObj.getFieldValue('addrphone');
												var s_ShipAddress1Loc = o_LocationObj.getFieldValue('addr1');
												var s_ShipAddress2Loc = o_LocationObj.getFieldValue('addr2');
												var s_ShipCityLoc = o_LocationObj.getFieldValue('city');
												var s_ShipStateLoc = o_LocationObj.getFieldValue('state');
												nlapiLogExecution('DEBUG', 'afterSubmit', 's_ShipStateLoc= ' + s_ShipStateLoc);
												var s_ShipZipLoc = o_LocationObj.getFieldValue('zip');
												if ((s_ShipAddress1Loc != null && s_ShipAddress1Loc != '') || (s_ShipCityLoc != null && s_ShipCityLoc != ''))
												{
													var ShipAddressArray = [s_ShipAddresseeLoc, 'Warby Parker' , s_ShipAddress1Loc, s_ShipAddress2Loc, s_ShipCityLoc, s_ShipStateLoc, s_ShipZipLoc];													
													var s_shipaddress='';
													for(var i=0;i<ShipAddressArray.length;i++)
													{
														if(i==0 && ShipAddressArray[i] != null)
														{
															s_shipaddress=ShipAddressArray[i];
														}
														else
														{
															if(ShipAddressArray[i] != null)
															{s_shipaddress=s_shipaddress+'\n'+ShipAddressArray[i];}															
														}
													}
													o_SOobj.setFieldValue('shipaddressee', s_ShipAddresseeLoc);
													o_SOobj.setFieldValue('shipattention', 'Warby Parker');
													o_SOobj.setFieldValue('shipaddr1', s_ShipAddress1Loc);
													o_SOobj.setFieldValue('shipaddr2', s_ShipAddress2Loc);
													o_SOobj.setFieldValue('shipcity', s_ShipCityLoc);
													o_SOobj.setFieldValue('shipzip', s_ShipZipLoc);
													o_SOobj.setFieldValue('shipaddress', s_shipaddress);													
												}											
											}											
										}
									}								
									o_CustomerRec.setFieldValue('phone', '(345) 455-5445');
									var i_CC_Number = o_SOobj.getFieldValue('ccnumber');
									var i_ExpDate = o_SOobj.getFieldValue('ccexpiredate');
									var i_CardholderName = o_SOobj.getFieldValue('ccname');
									var i_Promotion = o_SOobj.getFieldValue('promocode');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'Promotion= ' + i_Promotion);
									var i_CardType = o_SOobj.getFieldValue('paymentmethod');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'i_CardType= ' + i_CardType);
																	
									var i_customerID = nlapiSubmitRecord(o_CustomerRec, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'New customer ID= ' + i_customerID);
									o_SOobj.setFieldValue('entity', i_customerID);
									var o_NewCustomerObj = nlapiLoadRecord('customer', i_customerID);
									var i_SubmitCustomerPrescription = '';
									if (i_flag == 1) {
										if (i_customerID != null && i_customerID != undefined) {										
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_customer', i_customerID);
										}
										if(((i_OD_Sph != null && i_OD_Sph != '') || (i_OS_Sph != null && i_OS_Sph != '')) && i_Right_PD != null && i_Right_PD != '' && i_Left_PD != null && i_Left_PD != '')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 2);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 9);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'T');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 9);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit else loop', '1.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);											
										}
										else if(i_Dr_Name != null && i_Dr_Name != '' && i_Dr_Number != null && i_Dr_Number != '')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 1);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 2);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'F');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 3);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit else loop', '2.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
										else if(i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '' && i_Dr_WillfaxOrEmail != 'F')
										{
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_status', 1);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_entry_type', 5);
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_is_verified', 'F');
											o_CustomerPrescription.setFieldValue('custrecord_prescrip_verify_method', 5);
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit else loop', '3.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}
										else if((i_OD_Sph != null && i_OD_Sph != '') || (i_OS_Sph != null && i_OS_Sph != '') || (i_Right_PD != null && i_Right_PD != '') || (i_Left_PD != null && i_Left_PD != '') || (i_Dr_Name != null && i_Dr_Name != '') || (i_Dr_Number != null && i_Dr_Number != '') || (i_Dr_WillfaxOrEmail != null && i_Dr_WillfaxOrEmail != '' && i_Dr_WillfaxOrEmail != 'F'))
										{
											i_SubmitCustomerPrescription = nlapiSubmitRecord(o_CustomerPrescription, true, true);
											nlapiLogExecution('DEBUG', 'afterSubmit if', '4.Created CustomerPrescription= ' + i_SubmitCustomerPrescription);
										}										
									}
									
									if (i_CC_Number != null) {
										if(i_CardType != 12 && i_CardType != 1)
										{
											o_NewCustomerObj.selectNewLineItem('creditcards');
											o_NewCustomerObj.setCurrentLineItemValue('creditcards', 'ccnumber', i_CC_Number);
											o_NewCustomerObj.setCurrentLineItemValue('creditcards', 'ccexpiredate', i_ExpDate);
											o_NewCustomerObj.setCurrentLineItemValue('creditcards', 'ccname', i_CardholderName);
											o_NewCustomerObj.setCurrentLineItemValue('creditcards', 'paymentmethod', i_CardType);
											o_NewCustomerObj.commitLineItem('creditcards');
											
											o_SOobj.setFieldValue('ccnumber', i_CC_Number);
											o_SOobj.setFieldValue('ccexpiredate', i_ExpDate);
											o_SOobj.setFieldValue('ccname', i_CardholderName);
											o_SOobj.setFieldValue('paymentmethod', i_CardType);
										}										
									}
									if(i_Promotion != '' && i_Promotion != null) {
										o_SOobj.setFieldValue('promocode', i_Promotion);
									}
									if(i_SubmitCustomerPrescription != '' && i_SubmitCustomerPrescription != null) {
										o_SOobj.setFieldValue('custbody_prescription', i_SubmitCustomerPrescription);
									}
									nlapiSubmitRecord(o_NewCustomerObj, true, true);
									var i_PrefCC = nlapiLookupField('customer',i_LoginCustomer, 'prefccprocessor');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'PrefCC= ' + i_PrefCC);
									if(i_PrefCC != '' && i_PrefCC != null) {
										o_SOobj.setFieldValue('creditcardprocessor', i_PrefCC);
									}
									
									var i_billfulfill = o_SOobj.getFieldValue('custbody_tjinc_billfulfill');
									nlapiLogExecution('DEBUG', 'AfterSubmit', 'Bill & Fulfill= ' + i_billfulfill);
									if(i_billfulfill == 'T')
									{
										o_SOobj.setFieldValue('custbody_order_on_hold', 'F');
										o_SOobj.setFieldValue('orderstatus', 'B');
									}									
									var i_SalesOrderID = nlapiSubmitRecord(o_SOobj, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'i_SalesOrderID= ' + i_SalesOrderID);
									
									var o_SOobj = nlapiLoadRecord('salesorder', i_SalesOrderID);
									//function call for setting the Transaction Type and bill & fullfillment
									var o_OrderObj = f_setTransTypeandBillandFulfill(o_SOobj, POS_GIFT_CARD_PARENT_ID, i_SOid);
									
									/*
									o_OrderObj.setFieldValue('custbodytjinc_pos_order_complete', 'T');
									nlapiLogExecution('DEBUG', 'afterSubmit', 'OPS Order Complete set as true');
									var i_SalesOrderID = nlapiSubmitRecord(o_OrderObj, true, true);
									nlapiLogExecution('DEBUG', 'afterSubmit', 'i_SalesOrderID= ' + i_SalesOrderID);
									*/
								}
							}
						}
						var context = nlapiGetContext();
					    var usageEnd = context.getRemainingUsage();
					    nlapiLogExecution('DEBUG', 'schedulerFunction','usageEnd ==' + usageEnd);
						nlapiSetRedirectURL('TASKLINK', 'CARD_-47');
					}		
					
				}					
			} 
			catch (ex) {
				nlapiLogExecution('DEBUG', 'afterSubmit', 'Try-Catch Error= ' + ex);
			}
		}
	}
	}		
}

//
function f_setTransTypeandBillandFulfill(o_SOobj,POS_GIFT_CARD_PARENT_ID, i_SOid)
{
	//Setting the Transaction Type value
	var i_billfulfill = o_SOobj.getFieldValue('custbody_tjinc_billfulfill');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'Bill & Fulfill= ' + i_billfulfill);
	var s_Source = o_SOobj.getFieldValue('source');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'Source= ' + s_Source);
	var s_TransactionType = o_SOobj.getFieldValue('custbody_sale_type');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'TransactionType= ' + s_TransactionType);
	var i_LineItemCount = o_SOobj.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'LineItemCount= ' + i_LineItemCount);
	var b_NotServiceitemFlag = false;
	var b_IsServiceitemFlag = false;
	var b_SubItemOfFlag = false;
	var b_SubItemOfFlagBooks = false;
	var b_ItemIsSub101 = false;
	for(var i=1;i_LineItemCount != null && i<=i_LineItemCount;i++)
	{
		var lineItem = o_SOobj.selectLineItem('item',i);								
		var s_ItemType = o_SOobj.getCurrentLineItemValue('item','itemtype');
		//var s_SubItemOf = o_SOobj.getCurrentLineItemValue('item','parent');				
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'ItemType= ' + s_ItemType);
		
		var i_ItemId = o_SOobj.getLineItemValue('item', 'item', i);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_ItemId= ' + i_ItemId);
		var s_SubItemOf = nlapiLookupField('item', i_ItemId, 'parent');
		
		nlapiLogExecution('DEBUG', 'AfterSubmit', 's_SubItemOf= ' + s_SubItemOf);
		
		if(s_ItemType != 'Service')
		{
			b_NotServiceitemFlag = true;
		}
		else if(s_ItemType == 'Service')
		{
			b_IsServiceitemFlag = true;
		}
		//var s_SubItemOf = o_itemObj.getFieldValue('parent');
		
		if(s_SubItemOf == POS_GIFT_CARD_PARENT_ID) // 4275 value is for POS Gift Card Item (PROD value)
		{
			b_SubItemOfFlag = true;
		}
	
		if(s_SubItemOf == 3882) // 3882 value is for POS Gift Card Item
		{
			b_SubItemOfFlagBooks = true;
		}
		if(s_SubItemOf == 15) // 15 value is for Kit Package 101
		{
			b_ItemIsSub101 = true;
		}
	}
	if(i_billfulfill == 'F' && s_Source == 'Customer Center')
	{
		o_SOobj.setFieldValue('custbody_sale_type', '');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as null');
	}
	else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && (b_NotServiceitemFlag == true && b_SubItemOfFlag == false && b_SubItemOfFlagBooks == false && b_ItemIsSub101 == true))
	{
		o_SOobj.setFieldValue('custbody_sale_type', 2);//7 value is for Standard Sale
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Set Standard Sale (because bill and fulfill on a 101)');
	}
	else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && (b_NotServiceitemFlag == true && b_SubItemOfFlag == false && b_SubItemOfFlagBooks == false && b_ItemIsSub101 == false))
	{
		o_SOobj.setFieldValue('custbody_sale_type', 7);//7 value is for Sunglasses No Rx
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as Sunglasses No Rx');
	}
	else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && b_SubItemOfFlag == true)
	{
		o_SOobj.setFieldValue('custbody_sale_type', 6);//6 value is for Gift Card
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as SGift Card');
	}
	else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && b_IsServiceitemFlag == true)
	{
		o_SOobj.setFieldValue('custbody_sale_type', 16);//16 value is for Service
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as Service');
	}
	else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && b_SubItemOfFlagBooks == true)
	{
		o_SOobj.setFieldValue('custbody_sale_type', 12);//12 value is for Other Charge
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as Other');
	}
	
	//Code for Bill & Fulfillment
	var i_paymentmethod = o_SOobj.getFieldValue('paymentmethod');
	nlapiLogExecution('DEBUG', 'afterSubmit', 'paymentmethod= ' + i_paymentmethod);
	
	if (i_billfulfill == 'T' && i_paymentmethod != 12)
	{		
		//o_SOobj.setFieldValue('custbody_order_on_hold', 'F');
		//o_SOobj.setFieldValue('status', 'Pending Fulfillment');
		
		var d_trandate = o_SOobj.getFieldValue('trandate');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'trandate= ' + d_trandate);
		//var i_SOLineCount = o_SOobj.getLineItemCount('item');
		o_SOobj.setFieldValue('custbodytjinc_pos_order_complete', 'T');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'POS Order Complete set as true');
		var i_SalesOrderID = nlapiSubmitRecord(o_SOobj, true, true);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'i_SalesOrderID= ' + i_SalesOrderID);
		
		//Transfor Sales Order to ItemFulfillment
		var o_ItemfulfillObj = nlapiTransformRecord('salesorder', i_SOid, 'itemfulfillment')
		o_ItemfulfillObj.setFieldValue('shipstatus', 'C');
		o_ItemfulfillObj.setFieldValue('trandate', d_trandate);
		o_ItemfulfillObj.setFieldValue('customform', 106);
		var i_IFLineCount = o_ItemfulfillObj.getLineItemCount('item');
		for(var t = 1; t<= i_IFLineCount; t++)
		{
			o_ItemfulfillObj.setLineItemValue('item','itemreceive',t, 'T');
		}
		
		var i_ItemfulfillID = nlapiSubmitRecord(o_ItemfulfillObj, true);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'New Item Fulfillment  ID = ' + i_ItemfulfillID);
		
		//Transfor Sales Order to Invoice
		var o_IFObj = nlapiLoadRecord('itemfulfillment', i_ItemfulfillID);
		if(o_IFObj != null)
		{
			var o_CashSaleObj = nlapiTransformRecord('salesorder', i_SOid, 'cashsale');
			o_CashSaleObj.setFieldValue('customform', 107);
			o_CashSaleObj.setFieldValue('trandate', d_trandate);
			
			//o_CashSaleObj.setFieldValue('customform', 106);
			var i_InvLineItemCnt = o_CashSaleObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'Script Executed', 'i_InvLineItemCnt = ' + i_InvLineItemCnt);				
			var i_IFLineItemCount = o_IFObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'Script Executed', 'i_IFLineItemCount = ' + i_IFLineItemCount);
			var a_ItemArray = Array();
			var i_count = 0;
			for(var i=1;i<=i_IFLineItemCount;i++)
			{
				var i_IFItem = o_IFObj.getLineItemValue('item', 'item', i);
				var i_IFItemQty = o_IFObj.getLineItemValue('item', 'quantity', i);
				a_ItemArray[i_count++] = i_IFItem+'#$'+i_IFItemQty;
			}
			for(var k=1;k<=i_InvLineItemCnt;k++)
			{
				var i_InvoiceItem = o_IFObj.getLineItemValue('item', 'item', k);
				var i_InvoiceItemQty = o_IFObj.getLineItemValue('item', 'quantity', k);
				for(var l=0;a_ItemArray != null && l<a_ItemArray.length;l++)
				{
					var a_SplitItemArray=new Array();
					a_SplitItemArray=a_ItemArray[l].split('#$');
					if(i_InvoiceItem == a_SplitItemArray[0] && i_InvoiceItemQty == a_SplitItemArray[1])
					{
						o_CashSaleObj.setLineItemValue('item','custcol_ifitem', k, 'T');
					}
				}
			}
			for(var j=i_InvLineItemCnt;j>0;j--)
			{
				var b_IFItem = o_CashSaleObj.getLineItemValue('item', 'custcol_ifitem', j);
				if(b_IFItem != 'T')
				{
					o_CashSaleObj.removeLineItem('item',j);
				}				
			}			
			var i_CashSaleID = nlapiSubmitRecord(o_CashSaleObj);
			nlapiLogExecution('DEBUG', 'Script Executed', 'New Cash Sale  ID = ' + i_CashSaleID);				
			/*
			for(i=1;i<=i_IFLineItemCount;i++)
			{
				var i_IFItem = o_IFObj.getLineItemValue('item', 'item', i);
				var i_IFItemQty = o_IFObj.getLineItemValue('item', 'quantity', i);
				var i_NewItem = o_CashSaleObj.selectNewLineItem('item');
				o_CashSaleObj.setCurrentLineItemValue('item', 'item', i_IFItem);
				o_CashSaleObj.setCurrentLineItemValue('item', 'quantity', i_IFItemQty);
				o_CashSaleObj.commitLineItem('item');
			}
			var i_CashSaleID = nlapiSubmitRecord(o_CashSaleObj);
			nlapiLogExecution('DEBUG', 'Script Executed', 'New Cash Sale  ID = ' + i_CashSaleID);
			var i_InvLineItemCnt1 = o_CashSaleObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'Script Executed', ' after adding items on cashsalei_InvLineItemCnt = ' + i_InvLineItemCnt1);
			//removing Invoice line Item
			for(var j=i_InvLineItemCnt;j>0;j--)
			{
				nlapiLogExecution('DEBUG', 'Script Executed', ' j= ' + j);
				o_CashSaleObj.removeLineItem('item',j);
			}
			o_CashSaleObj.setFieldValue('createdfrom', i_SOid);	
			*/					
		}			
	}		
	//return o_SOobj;
}
