/**
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: WAPn
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate: 20130523
* @DocumentationUrl:
* @NamingStandard: TJINC_NSJ-1-2
*/

var a_JV_Array = new Array();
function TJINC_POS_afterSubmitRecord(type)
{
	nlapiLogExecution('DEBUG', 'afterSubmit', 'type= ' + type);
	if(type != 'delete')
	{
		var i_IFid = nlapiGetRecordId();
		var o_IFobj = nlapiLoadRecord('itemfulfillment', i_IFid);
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Item Fulfillment ID= ' + i_IFid);						
		var s_Status = o_IFobj.getFieldValue('status');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Status= ' + s_Status);
		
		//if (type  == 'create' && s_Status == 'Shipped')
		//if (type  == 'create' && s_Status == 'Shipped')
		{
			var i_LineItemCount = o_IFobj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'LineItemCount= ' + i_LineItemCount);
			
			for(var i=1;i<=i_LineItemCount;i++)
			{
				var s_itemtype = o_IFobj.getLineItemValue('item', 'itemtype', i);
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemtype= ' + s_itemtype);
				if(s_itemtype == 'Assembly')
				{
					var s_itemID = o_IFobj.getLineItemValue('item', 'item', i);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
					
					var i_COGS_AccountCredit='';
					if(s_itemID != null)
					{
						var o_itemObj = nlapiLoadRecord('assemblyitem', s_itemID);
						if(o_itemObj != null)
						{
							i_COGS_AccountCredit = o_itemObj.getFieldValue('cogsaccount');
							
							TJINC_POS_AssemblyItem(i_COGS_AccountCredit, s_itemID);
						}
					}
				}
			}
			if(a_JV_Array != null)
			{
				var o_JVObj = nlapiCreateRecord('journalentry');
				var f_creditTotal = 0;
				for(var f = 0;a_JV_Array != null && f<a_JV_Array.length; f++ )
				{
					var a_SplitJV_Array=new Array();
	    			a_SplitJV_Array=a_JV_Array[f].split('##');
					var o_LineItem = o_JVObj.selectNewLineItem('line');					
					o_JVObj.setCurrentLineItemValue('line', 'account', a_SplitJV_Array[1]);
					o_JVObj.setCurrentLineItemValue('line', 'debit', a_SplitJV_Array[2]);
					f_creditTotal =parseFloat(f_creditTotal) + parseFloat(a_SplitJV_Array[2]);					
					o_JVObj.commitLineItem('line');
					var credit = a_JV_Array.length - 1;
					if(f == credit)
					{
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_creditTotal= ' + f_creditTotal);
						var o_LineItem = o_JVObj.selectNewLineItem('line');					
						o_JVObj.setCurrentLineItemValue('line', 'account', a_SplitJV_Array[0]);
						o_JVObj.setCurrentLineItemValue('line', 'credit', f_creditTotal);					
						o_JVObj.commitLineItem('line');
					}					
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'a_JV_Array['+f+']= ' + a_JV_Array[f]);
				}
				var i_JVID = nlapiSubmitRecord(o_JVObj);
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'Created Journal= ' + i_JVID);
			}
		}
	}		
}

function TJINC_POS_AssemblyItem(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemOne(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
{
	if(s_Memitemtype == 'InvtPart')
	{
		var o_NonAssemblyItemObj = nlapiLoadRecord('inventoryitem', s_itemID);
		var f_averagecost = o_NonAssemblyItemObj.getFieldValue('averagecost');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'f_averagecost= ' + f_averagecost);
		var s_cogsaccount = o_NonAssemblyItemObj.getFieldValue('cogsaccount');
	    nlapiLogExecution('DEBUG', 'afterSubmit', 's_cogsaccount= ' + s_cogsaccount);
		/*
		var a_columns = nlapiLookupField('item', s_itemID, ['averagecost','cogsaccount']);
		var f_averagecost = a_columns.averagecost
		var s_cogsaccount = a_columns.cogsaccount
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_averagecost= ' + f_averagecost);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 's_cogsaccount= ' + s_cogsaccount);
		*/
	}
	else if(s_Memitemtype == 'Service')
	{
		var o_ServiceItemObj = nlapiLoadRecord('serviceitem', s_itemID);
		var f_averagecost = o_ServiceItemObj.getFieldValue('cost');
		nlapiLogExecution('DEBUG', 'afterSubmit', 'Purchase cost= ' + f_averagecost);
		var s_cogsaccount = o_ServiceItemObj.getFieldValue('expenseaccount');
	    nlapiLogExecution('DEBUG', 'afterSubmit', 'expenseaccount= ' + s_cogsaccount);
		/*
		a_columns = nlapiLookupField('item', s_itemID, ['cost','cogsaccount']);
		f_averagecost = a_columns.cost
		s_cogsaccount = a_columns.cogsaccount
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_averagecost= ' + f_averagecost);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 's_cogsaccount= ' + s_cogsaccount);
		*/
	}
	
	var f_DebitCost=0;
	if(f_averagecost != null && f_averagecost != '')
	{
		f_DebitCost = (parseFloat(f_averagecost) * parseFloat(s_itemQuantity));
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'DebitCost= ' + f_DebitCost);
	}
	if(i_COGS_AccountCredit != null && f_DebitCost != null && s_cogsaccount != null)
	{
		a_JV_Array.push(i_COGS_AccountCredit + '##' + s_cogsaccount + '##' + f_DebitCost);
	}
}

function TJINC_POS_AssemblyItemOne(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemTwo(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_AssemblyItemTwo(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemThree(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_AssemblyItemThree(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemFour(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_AssemblyItemFour(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemFive(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_AssemblyItemFive(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		if(s_Memitemtype == 'Assembly')
		{
			if(s_itemID != null)
			{
				//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
				TJINC_POS_AssemblyItemSix(i_COGS_AccountCredit, s_itemID);
			}
		}
		else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}

function TJINC_POS_AssemblyItemSix(i_COGS_AccountCredit, s_itemID)
{
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', s_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_MemberLineCount= ' + i_MemberLineCount);
	for(var j=1;j<=i_MemberLineCount;j++)
	{
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Member itemtype= ' + s_Memitemtype);
		var s_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + s_itemID);
		var s_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemQuantity= ' + s_itemQuantity);
		//var s_sitemtype = o_MemberItemObj.getLineItemValue('item', 'sitemtype', j);
		//nlapiLogExecution('DEBUG', 'AfterSubmit', 'sub itemtype= ' + s_sitemtype);
		
		//if(s_Memitemtype == 'Assembly')
		//{
		//	if(s_itemID != null)
		//	{
		//		//i_COGS_AccountCredit = o_MemberItemObj.getFieldValue('cogsaccount');				
		//		TJINC_POS_AssemblyItem(i_COGS_AccountCredit, s_itemID);
		//	}
		//}
		//else
		{
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, s_itemID, s_itemQuantity, s_Memitemtype)
		}
	}
}
