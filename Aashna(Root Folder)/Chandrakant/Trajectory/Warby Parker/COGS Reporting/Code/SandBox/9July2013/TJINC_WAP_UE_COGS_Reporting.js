/**
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: WAPn
* @Company: Trajectory Inc. / Kuspide Canada Inc.
* @CreationDate: 20130523
* @DocumentationUrl: https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wap_ue_cogs_reporting-js
* @NamingStandard: TJINC_NSJ-1-2
*/

//URL : https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wap_ue_cogs_reporting-js#TOC-Function:-TJINC_POS_afterSubmitRecord
var a_JV_Array = new Array();
function TJINC_POS_afterSubmitRecord(type){
	nlapiLogExecution('DEBUG', 'afterSubmit', 'type= ' + type);
	if (type != 'delete') {
		try {
			var i_IFid = nlapiGetRecordId();
			var o_IFobj = nlapiLoadRecord('itemfulfillment', i_IFid);
			nlapiLogExecution('DEBUG', 'afterSubmit', 'Item Fulfillment ID= ' + i_IFid);
			var s_Status = o_IFobj.getFieldValue('status');
			nlapiLogExecution('DEBUG', 'afterSubmit', 'Status= ' + s_Status);
			var i_journal = o_IFobj.getFieldValue('custbody_journal');
			nlapiLogExecution('DEBUG', 'afterSubmit', 'journal= ' + i_journal);
			var b_journalCreated = o_IFobj.getFieldValue('custbody_journalcreated');
			nlapiLogExecution('DEBUG', 'afterSubmit', 'Journal check box= ' + b_journalCreated);
			
			if (s_Status == 'Shipped' && (i_journal == null || i_journal == '') && b_journalCreated == 'F') {
				var i_LineItemCount = o_IFobj.getLineItemCount('item');
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'LineItemCount= ' + i_LineItemCount);
				
				for (var i = 1; i <= i_LineItemCount; i++) {
					var s_itemtype = o_IFobj.getLineItemValue('item', 'itemtype', i);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemtype= ' + s_itemtype);
					if (s_itemtype == 'Assembly') {
						var i_itemID = o_IFobj.getLineItemValue('item', 'item', i);
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'itemID= ' + i_itemID);
						var i_itemLocation = o_IFobj.getLineItemValue('item', 'location', i);
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'Item Location= ' + i_itemLocation);
						//nlapiLogExecution('DEBUG', '########AfterSubmit#######', 'i= ' + i);
						var i_COGS_AccountCredit = '';
						if (i_itemID != null) {
							var i_itemQty = o_IFobj.getLineItemValue('item', 'quantity', i);
							nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_itemQty= ' + i_itemQty);
							var o_itemObj = nlapiLoadRecord('assemblyitem', i_itemID);
							if (o_itemObj != null) {
								i_COGS_AccountCredit = o_itemObj.getFieldValue('cogsaccount');
								
								TJINC_POS_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQty, i_itemLocation);
							}
						}
					}
				}
				if (a_JV_Array != null) {
					var o_JVObj = nlapiCreateRecord('journalentry');
					var f_creditTotal = 0;
					for (var f = 0; a_JV_Array != null && f < a_JV_Array.length; f++) {
						var a_SplitJV_Array = new Array();
						a_SplitJV_Array = a_JV_Array[f].split('##');
						var o_LineItem = o_JVObj.selectNewLineItem('line');
						o_JVObj.setCurrentLineItemValue('line', 'account', a_SplitJV_Array[1]);
						var f_debitAmount = parseFloat(a_SplitJV_Array[2])						
						o_JVObj.setCurrentLineItemValue('line', 'debit', f_debitAmount.toFixed(2));
						f_debitAmount = f_debitAmount.toFixed(2);
						f_creditTotal = parseFloat(f_creditTotal) + parseFloat(f_debitAmount);						
						o_JVObj.commitLineItem('line');
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'a_JV_Array[' + f + ']= ' + a_JV_Array[f]);
						var credit = a_JV_Array.length - 1;
						if (f == credit) {
							nlapiLogExecution('DEBUG', 'AfterSubmit', 'f_creditTotal= ' + f_creditTotal);
							var o_LineItem = o_JVObj.selectNewLineItem('line');
							o_JVObj.setCurrentLineItemValue('line', 'account', a_SplitJV_Array[0]);
							o_JVObj.setCurrentLineItemValue('line', 'credit', f_creditTotal.toFixed(2));
							o_JVObj.commitLineItem('line');
						}
						
					}
					o_JVObj.setFieldValue('custbody_itemfulfillment', i_IFid);
					var i_JVID = nlapiSubmitRecord(o_JVObj);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'Created Journal= ' + i_JVID);
					if(i_JVID != null)
					{
						nlapiSubmitField('itemfulfillment', i_IFid, ['custbody_journal','custbody_journalcreated'], [i_JVID,'T']);
						//nlapiSubmitField('itemfulfillment', i_IFid, 'custbody_journalcreated', 'T');						
					}
					
				}
			}
		} 
		catch (exception) {
			nlapiLogExecution('DEBUG', 'Try Catch error', 'Try Catch Exception= ' + exception);
		}
	}
}

//URL : https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wap_ue_cogs_reporting-js#TOC-Function:-TJINC_POS_AssemblyItem
function TJINC_POS_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQty, i_itemLocation) {
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItem', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		nlapiLogExecution('DEBUG', '%%%%%%AssemblyItem%%%%%%', 'j= ' + j);
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItem', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItem', 'itemID= ' + i_itemID);
		var i_itemQuantity = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItem', 'itemQuantity= ' + i_itemQuantity);
		i_itemQuantity = parseFloat(i_itemQuantity) * parseFloat(i_itemQty);
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemOne(i_COGS_AccountCredit, i_itemID, i_itemQuantity, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantity, s_Memitemtype, i_itemLocation)
		}
	}
}

//URL : https://sites.google.com/a/trajectoryinc.com/wap/classes/tjinc_wap_ue_cogs_reporting-js#TOC-Function:-TJINC_POS_Non_AssemblyItem-
function TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantity, s_Memitemtype, i_itemLocation) {
	nlapiLogExecution('DEBUG', '$$$$$$$$$$Non Assembly$$$$$$$$$$$', '^^^^^^^^^^^^^^^^^^^^^^^');
	var f_averagecost = 0;
	if (s_Memitemtype == 'InvtPart') {
		var o_NonAssemblyItemObj = nlapiLoadRecord('inventoryitem', i_itemID);
		
		for(var m=1;m<=o_NonAssemblyItemObj.getLineItemCount('locations');m++)
		{
			var i_lineLocation = o_NonAssemblyItemObj.getLineItemValue('locations', 'locationid', m);
			//nlapiLogExecution('DEBUG', 'Non Assembly', 'lineLocation= ' + i_lineLocation);
			if(i_itemLocation == i_lineLocation)
			{
				f_averagecost = o_NonAssemblyItemObj.getLineItemValue('locations', 'averagecostmli', m);
			}
		}		
		//var f_averagecost = o_NonAssemblyItemObj.getFieldValue('averagecost');
		nlapiLogExecution('DEBUG', 'Non Assembly', 'f_averagecost= ' + f_averagecost);
		var s_cogsaccount = o_NonAssemblyItemObj.getFieldValue('cogsaccount');
		nlapiLogExecution('DEBUG', 'Non Assembly', 's_cogsaccount= ' + s_cogsaccount);
	}
	else 
		if (s_Memitemtype == 'Service') {
			var o_ServiceItemObj = nlapiLoadRecord('serviceitem', i_itemID);
			var f_averagecost = o_ServiceItemObj.getFieldValue('cost');
			nlapiLogExecution('DEBUG', 'Non Assembly', 'Purchase cost= ' + f_averagecost);
			var s_cogsaccount = o_ServiceItemObj.getFieldValue('expenseaccount');
			nlapiLogExecution('DEBUG', 'Non Assembly', 'expenseaccount= ' + s_cogsaccount);
		}
	
	var f_DebitCost = 0;
	if (f_averagecost != null && f_averagecost != '' && i_itemQuantity != null) {
		f_DebitCost = (parseFloat(f_averagecost) * parseFloat(i_itemQuantity));
		nlapiLogExecution('DEBUG', 'Non Assembly', 'DebitCost= ' + f_DebitCost);
	}
	if (i_COGS_AccountCredit != null && f_DebitCost != null && s_cogsaccount != null) {
		a_JV_Array.push(i_COGS_AccountCredit + '##' + s_cogsaccount + '##' + f_DebitCost);
	}
}

function TJINC_POS_AssemblyItemOne(i_COGS_AccountCredit, i_itemID, i_itemQuantity, i_itemLocation) {
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItemOne', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemOne', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemOne', 'itemID= ' + i_itemID);
		var i_itemQuantityOne = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemOne', 'itemQuantity= ' + i_itemQuantityOne);
		i_itemQuantityOne = parseFloat(i_itemQuantityOne) * parseFloat(i_itemQuantity);
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemTwo(i_COGS_AccountCredit, i_itemID, i_itemQuantityOne, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantityOne, s_Memitemtype, i_itemLocation)
		}
	}
}

function TJINC_POS_AssemblyItemTwo(i_COGS_AccountCredit, i_itemID, i_itemQuantityOne, i_itemLocation){
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItemTwo', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemTwo', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemTwo', 'itemID= ' + i_itemID);
		var i_itemQuantityTwo = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemTwo', 'itemQuantity= ' + i_itemQuantityTwo);
		i_itemQuantityTwo = parseFloat(i_itemQuantityTwo) * parseFloat(i_itemQuantityOne)
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemThree(i_COGS_AccountCredit, i_itemID, i_itemQuantityTwo, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantityTwo, s_Memitemtype, i_itemLocation)
		}
	}
}

function TJINC_POS_AssemblyItemThree(i_COGS_AccountCredit, i_itemID, i_itemQuantityTwo, i_itemLocation) {
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItemThree', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemThree', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemThree', 'itemID= ' + i_itemID);
		var i_itemQuantityThree = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemThree', 'itemQuantity= ' + i_itemQuantityThree);
		i_itemQuantityThree = parseFloat(i_itemQuantityThree) * parseFloat(i_itemQuantityTwo);
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemFour(i_COGS_AccountCredit, i_itemID, i_itemQuantityThree, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantityThree, s_Memitemtype, i_itemLocation)
		}
	}
}

function TJINC_POS_AssemblyItemFour(i_COGS_AccountCredit, i_itemID, i_itemQuantityThree, i_itemLocation) {
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItemFour', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFour', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFour', 'itemID= ' + i_itemID);
		var i_itemQuantityFour = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFour', 'itemQuantity= ' + i_itemQuantityFour);
		i_itemQuantityFour = parseFloat(i_itemQuantityFour) * parseFloat(i_itemQuantityThree);
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemFive(i_COGS_AccountCredit, i_itemID, i_itemQuantityFour, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantityFour, s_Memitemtype, i_itemLocation)
		}
	}
}

function TJINC_POS_AssemblyItemFive(i_COGS_AccountCredit, i_itemID, i_itemQuantityFour,i_itemLocation) {
	var o_MemberItemObj = nlapiLoadRecord('assemblyitem', i_itemID)
	var i_MemberLineCount = o_MemberItemObj.getLineItemCount('member');
	nlapiLogExecution('DEBUG', 'AssemblyItemFive', 'i_MemberLineCount= ' + i_MemberLineCount);
	for (var j = 1; j <= i_MemberLineCount; j++) {
		var s_Memitemtype = o_MemberItemObj.getLineItemValue('member', 'sitemtype', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFive', 'Member itemtype= ' + s_Memitemtype);
		var i_itemID = o_MemberItemObj.getLineItemValue('member', 'item', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFive', 'itemID= ' + i_itemID);
		var i_itemQuantityFive = o_MemberItemObj.getLineItemValue('member', 'quantity', j);
		nlapiLogExecution('DEBUG', 'AssemblyItemFive', 'itemQuantity= ' + i_itemQuantityFive);
		i_itemQuantityFive = parseFloat(i_itemQuantityFive) * parseFloat(i_itemQuantityFour);
		if (s_Memitemtype == 'Assembly') {
			if (i_itemID != null) {
				TJINC_POS_AssemblyItemFive(i_COGS_AccountCredit, i_itemID, i_itemQuantityFive, i_itemLocation);
			}
		}
		else {
			TJINC_POS_Non_AssemblyItem(i_COGS_AccountCredit, i_itemID, i_itemQuantityFive, s_Memitemtype, i_itemLocation)
		}
	}
}
