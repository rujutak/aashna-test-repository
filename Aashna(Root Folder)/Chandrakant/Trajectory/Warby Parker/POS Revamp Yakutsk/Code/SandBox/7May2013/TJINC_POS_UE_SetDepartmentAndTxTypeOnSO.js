/*Date Modified 29122012 02012012 04 11012013 21012013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20121912
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

function TJINC_NSJ_AfterSubmitPR(type)
{
	var i_RecordId = nlapiGetRecordId();
	if (i_RecordId != null && i_RecordId != '') 
	{		
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'RecordId= ' + i_RecordId);		
		var o_RecordObj = nlapiLoadRecord('salesorder', i_RecordId);
		if(o_RecordObj != null)
		{
			//setting the Depertment Value on Sales Order
			var i_Customer = o_RecordObj.getFieldValue('entity');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'Customer ID= ' + i_Customer);
			if(i_Customer != null && i_Customer != '')
			{
				var o_customerObj = nlapiLoadRecord('customer', i_Customer);
				if(o_customerObj != null)
				{
					var i_Location = o_customerObj.getFieldValue('custentity_tjinc_location');
					if(i_Location == 55)
					{
						o_RecordObj.setFieldValue('department', 16);
					}
					else if(i_Location == 1)
					{
						o_RecordObj.setFieldValue('department', 18);
					}
				}
				
			}
			var i_billfulfill = o_RecordObj.getFieldValue('custbody_tjinc_billfulfill');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'Bill & Fulfill= ' + i_billfulfill);
			var s_Source = o_RecordObj.getFieldValue('source');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'Source= ' + s_Source);
			var s_TransactionType = o_RecordObj.getFieldValue('custbody_sale_type');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'TransactionType= ' + s_TransactionType);
			var i_LineItemCount = o_RecordObj.getLineItemCount('item');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'LineItemCount= ' + i_LineItemCount);
			var i_NotServiceitemFlag = 0;
			var i_IsServiceitemFlag = 0;
			var i_SubItemOfFlag = 0;
			for(var i=1;i_LineItemCount != null && i<=i_LineItemCount;i++)
			{
				var lineItem = o_RecordObj.selectLineItem('item',i)
				//nlapiSelectLineItem
				var ItemType = o_RecordObj.getCurrentLineItemValue('item','itemtype');
				var s_SubItemOf = o_RecordObj.getCurrentLineItemValue('item','parent');
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'ItemType= ' + ItemType);
				nlapiLogExecution('DEBUG', 'AfterSubmit', 's_SubItemOf= ' + s_SubItemOf);
				//var i_Item = o_RecordObj.getLineItemValue('item','item',i);
				if(i_Item != null)
				{
					var fields = ['itemtype', 'parent']
					var columns = nlapiLookupField('item', i_Item, fields);
					var s_ItemType = columns['itemtype']
					var s_SubItemOf = columns['parent']
					//var name = columns.entityid
					nlapiLogExecution('DEBUG', 'AfterSubmit', 's_ItemType= ' + s_ItemType);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 's_SubItemOf= ' + s_SubItemOf);
					
					//var o_itemObj = nlapiLoadRecord('item', i_Item);
					//if(o_itemObj != null)
					{
						//var s_ItemType = o_itemObj.getFieldValue('itemtype');
						if(s_ItemType != 'Service')
						{
							i_NotServiceitemFlag = 1;
						}
						else if(s_ItemType == 'Service')
						{
							i_IsServiceitemFlag = 1;
						}
						//var s_SubItemOf = o_itemObj.getFieldValue('parent');
						if(s_SubItemOf == 4250) // 4250 value is for POS Gift Card Item
						{
							i_SubItemOfFlag = 1;
						}
					}
				}
			}
			if(i_billfulfill == 'F' && s_Source == 'Customer Center')
			{
				o_RecordObj.setFieldValue('custbody_sale_type', '');
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as null');
			}
			else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && (i_NotServiceitemFlag == 1 || i_SubItemOfFlag == 1))
			{
				o_RecordObj.setFieldValue('custbody_sale_type', 7);//7 value is for Sunglasses No Rx
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as Sunglasses No Rx');
			}
			else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && i_SubItemOfFlag == 1)
			{
				o_RecordObj.setFieldValue('custbody_sale_type', 6);//6 value is for Gift Card
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as SGift Card');
			}
			else if(i_billfulfill == 'T' && s_TransactionType != null && s_Source == 'Customer Center' && i_SubItemOfFlag == 1)
			{
				o_RecordObj.setFieldValue('custbody_sale_type', 16);//16 value is for Service
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'Transaction Type Seted as SGift Card');
			}
			var i_SOID = nlapiSubmitRecord(o_RecordObj);
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'Submited SalesOrder ID= ' + i_SOID);
		}						
	}
	return true;
}
