/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/


// Main function
function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
   nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	//var filters = new Array();
    //var a_columns = new Array();
 	

   // a_filters[0] = new nlobjSearchFilter('entityid', null, 'is', item);
   // a_columns[0] = new nlobjSearchColumn('entityid');
 	var a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
  	if(a_searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_searchResult.length=' + a_searchResult.length);
		//for(var k=0; a_searchResult != null && k <a_searchResult.length; k++)
			
		
		//for loop for Customer
		for(var k=0; a_searchResult != null && k <6; k++)
		{			
			var s_result = a_searchResult[k];
			// return all columns associated with this search
			var a_columns = s_result.getAllColumns();
			var columnLen = a_columns.length;
			//nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var s_column0 = a_columns[0];
			var s_customerName = s_result.getValue(s_column0);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerName['+k+']=' + s_customerName);
			
			var s_column1 = a_columns[1];
			var i_customerID = s_result.getValue(s_column1);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 'customerID['+k+']=' + i_customerID);
			
			var o_LoadCustomerRec=nlapiLoadRecord('customer', i_customerID);
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec['+k+']=' + o_LoadCustomerRec);
			
			/*
            if (o_LoadCustomerRec != null && o_LoadCustomerRec != '') 
			{
				var Bill_To_Addr = o_LoadCustomerRec.getFieldValue('comments');
				nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec[' + k + ']=' + o_LoadCustomerRec);
				
				var LineCountAddr = o_LoadCustomerRec.getLineItemCount('addressbook');
				nlapiLogExecution('DEBUG', 'CustomerSearch', ' LineCountAddr==' + LineCountAddr);
				
				if (LineCountAddr > 0 && LineCountAddr != null && LineCountAddr != '') 
				{					
					for (var x = 1; x <= LineCountAddr; x++) 
					{
						var DefaultBillingAddrVal = o_LoadCustomerRec.getLineItemValue('addressbook', 'defaultbilling', x);
						nlapiLogExecution('DEBUG', 'CustomerSearch', ' Default Billing Address[' + x + ']==' + DefaultBillingAddrVal);
						if (DefaultBillingAddrVal == 'T') 
						{
							var Address = o_LoadCustomerRec.getLineItemValue('addressbook', 'addressee', x);
							nlapiLogExecution('DEBUG', 'CustomerSearch', ' Billing Address[' + x + ']==' + Address);
						}
					}
				}
			}		
            */	
			var s_column2 = a_columns[2];
			var s_customerEmail = s_result.getValue(s_column2);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerEmail['+k+']=' + s_customerEmail);
			
			var s_column3 = a_columns[3];
			var s_BPInvoiceType = s_result.getValue(s_column3);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 's_BPInvoiceType['+k+']=' + s_BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var a_filtersSearch = new Array();
			a_filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', s_customerName);
            // var columnsSearch = new Array();
			
			var a_InvoicesearchResults= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', a_filtersSearch, null);
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'a_InvoicesearchResults['+k+'] len=' + a_InvoicesearchResults.length);
			
			
			var a_BillToAddress=new Array();
			var a_StoredArray=new Array();
			
			nlapiLogExecution('DEBUG', '***a_BillToAddress length == ' + a_BillToAddress.length);
			nlapiLogExecution('DEBUG', '***a_StoredArray length == ' + a_StoredArray.length);
			//nlapiLogExecution('DEBUG', '***a_SplitArray length == ' + a_SplitArray.length);
			//nlapiLogExecution('DEBUG', '***a_SplitArrayPayeeNull length == ' + a_SplitArrayPayeeNull.length);
			
			
				for (var i = 0; a_InvoicesearchResults != null && i < a_InvoicesearchResults.length; i++) 
				{
					var s_resultInvoice = a_InvoicesearchResults[i];
					// return all a_columns associated with this search
					var a_columnsInvoice = s_resultInvoice.getAllColumns();
					var i_columnLen2 = a_columnsInvoice.length;
					//nlapiLogExecution('DEBUG', '***i_columnLen2 == ' + i_columnLen2);
					
					var s_Columns0 = a_columnsInvoice[0];
					var s_Customer_Created_Date = s_resultInvoice.getValue(s_Columns0);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + s_Customer_Created_Date);		
					
					//var Columns= a_columnsInvoice[1];
					//var Columns1 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
					
					//var Columns= a_columnsInvoice[2];
					//var Columns2 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
					
					var s_Columns3 = a_columnsInvoice[3];
					var i_Invoice_ID = s_resultInvoice.getValue(s_Columns3);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + i_Invoice_ID);
					
					var s_Columns10 = a_columnsInvoice[10];
					var i_Location_ID = s_resultInvoice.getValue(s_Columns10);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'i_Location_ID=' + i_Location_ID);
					
					var o_LoadInvoiceRec = nlapiLoadRecord('invoice', i_Invoice_ID);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'o_LoadInvoiceRec['+i+']=' + o_LoadInvoiceRec);
					
					//var a_StoredArray=new Array();
					if (o_LoadInvoiceRec != null && o_LoadInvoiceRec != '') 
					{
						var s_PayerNew = o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_PayerNew['+i+']=' + s_PayerNew);
						var s_InvoiceBillToAddress = o_LoadInvoiceRec.getFieldValue('billaddress');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_InvoiceBillToAddress= ' + s_InvoiceBillToAddress);			    	
						
						if (s_PayerNew != null && s_PayerNew != '') 
						{
							//*************************************************************************************
							var i_arrayLength = a_StoredArray.length;
							if (i_arrayLength == 0) 
							{
								a_StoredArray[0] = s_PayerNew+'#$'+i_Location_ID+'#$'+ i_Invoice_ID;
							}
							else if (i_arrayLength != 0) 
							{
								var i_flag = 0;
								//var a_SplitArray=new Array();
								for (var kk = 0; kk < a_StoredArray.length; kk++) 
								{
									var a_SplitArray=new Array();
									//nlapiLogExecution('DEBUG', '***a_SplitArray length before == ' + a_SplitArray.length);
									a_SplitArray = a_StoredArray[kk].split('#$');
									//nlapiLogExecution('DEBUG', '***a_SplitArray length after == ' + a_SplitArray.length);
									if ((a_SplitArray[0] == s_PayerNew) && (a_SplitArray[1]==i_Location_ID)) 
									{
										a_StoredArray[kk] = a_StoredArray[kk] + '#$' + i_Invoice_ID;
										i_flag = 1;
										break;
									}
								}
								if (i_flag != 1) 
								{
									a_StoredArray[a_StoredArray.length] = s_PayerNew+'#$'+i_Location_ID + '#$' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)							
						}//if(s_PayerNew!=null && s_PayerNew!='')						
						else if (s_PayerNew == null && s_InvoiceBillToAddress != null) 
						{
							var i_BillToArray = a_BillToAddress.length;
							if (i_BillToArray == 0) 
							{
								a_BillToAddress[0] = s_InvoiceBillToAddress+'#$'+i_Location_ID +'#$'+ i_Invoice_ID;
							}
							else if (i_BillToArray != 0) 
							{
								var i_flag1 = 0;
								for (var l = 0; l < a_BillToAddress.length; l++) 
								{
									var a_SplitArrayPayeeNull=new Array();
									//nlapiLogExecution('DEBUG', '***a_SplitArrayPayeeNull length before == ' + a_SplitArrayPayeeNull.length);
									a_SplitArrayPayeeNull = a_BillToAddress[l].split('#$');
									//nlapiLogExecution('DEBUG', '***a_SplitArrayPayeeNull length after == ' + a_SplitArrayPayeeNull.length);
									if ((a_SplitArrayPayeeNull[0] == s_InvoiceBillToAddress) && (a_SplitArrayPayeeNull[1]==i_Location_ID)) 
									{
										a_BillToAddress[l] = a_BillToAddress[l] + '#$' + i_Invoice_ID;
										i_flag1 = 1;
										break;
									}
								}
								if (i_flag1 != 1) 
								{
									a_BillToAddress[a_BillToAddress.length] = s_InvoiceBillToAddress+'#$'+i_Location_ID + '#$' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)
						}
						
					}
					
					var s_Columns4 = a_columnsInvoice[4];
					var s_Invoice_Date = s_resultInvoice.getValue(s_Columns4);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + s_Invoice_Date);
					
					var s_Columns5 = a_columnsInvoice[5];
					var i_BP_Invoice_Type = s_resultInvoice.getValue(s_Columns5);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + i_BP_Invoice_Type);
					
			}//for i - invoice
			
			//Code for Summarize
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			if(s_BPInvoiceType==3) //3 means summary s_BPInvoiceType
			{
				//FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
			}
			
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************			
			//Code for Consolidated
			
            if(s_BPInvoiceType==2)
			{
				//functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
			}
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			//if(s_BPInvoiceType==1)
			//{
			for(s=0;s<a_StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray['+s+']=' + a_StoredArray[s]);
			}
			for(a=0;a<a_BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
			}
			//}

			
		}//for
  		
  	}//if
 
 	
}//function close







//**********************************************************************************************************
                    
					//   FUNCTION FOR CONSOLIDATED

//**********************************************************************************************************




function functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
{
	nlapiLogExecution('DEBUG', 'functionConsolidated', 'IN CONSOLIDATED FUNCTION' );
	
	nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();		
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();		
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		var s_AsOfDate='';
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();
		
		
	    i_cnt=0;
		var ItemQtyArray=new Array();
		var flagItem=0;
		var i_cntth=0;
		
		var s_PayeeAddress='';	
		var a_SplitStoredArray=new Array();				
		a_SplitStoredArray=a_StoredArray[s].split('#$');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+s+']=' + a_SplitStoredArray[s]);
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=2;n<=a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var r_SplitQty=0;
						if(s_InvoiceItemQuantity!=null && s_InvoiceItemQuantity!='')
						{						   
						    r_SplitQty=s_InvoiceItemQuantity;
						}	
												
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						var r_ItemAmt=0;
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='')
						{
							r_ItemAmt=s_InvoiceItemAmount;
						}						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						var r_SplitTax=new Array();
						if(s_InvoiceITax!=null && s_InvoiceITax!='')
						{						   
						    r_SplitTax=s_InvoiceITax.split('%');
						}
						else
						{
							r_SplitTax[0]=0;
						}
						
						
				//=========================================================================================================								
						if(ItemQtyArray.length==0)
						{
							ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemAmt;
						}
						else if(ItemQtyArray.length!=0)
						{
							for(var d=0;d<ItemQtyArray.length;d++)
							{
								var i_flagcheck=0;
								var a_SplitItemArray=new Array();
								a_SplitItemArray=ItemQtyArray[d].split('#$');
								if(a_SplitItemArray[0]==s_InvoiceItemValue)
								{
									var r_CalQty=a_SplitItemArray[2]+parseFloat(r_SplitQty);
									var r_CalTax=a_SplitItemArray[3]+parseFloat(r_SplitTax[0]);
									var r_CalAmt=a_SplitItemArray[4]+parseFloat(r_ItemAmt);
									
									ItemQtyArray[d]=a_SplitItemArray[1]+'#$'+r_CalQty+'#$'+r_CalTax+'#$'+r_CalAmt;
								    i_flagcheck=1;									
								}//if
								if(i_flagcheck!=1)
								{
									ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemAmt;
								}//if
							}// for							
						} //else if
	           //=========================================================================================================								
				
								
						
					}//for
	            if(a_AllPrimaryTherapist.length==0)
				{
					a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
					
				}
				else if(a_AllPrimaryTherapist.length!=0)
				{
					for(var g=0;g<a_AllPrimaryTherapist.length;g++)
					{								
						if(a_AllPrimaryTherapist[g]!=s_TherapistID)
						{													
							a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
						}//if								
							
					}// for							
				} //else if		
				}
					
				}						
			}	//for n	
			
		}//if
		
		//Extracting tha Item Array for Calculate the Item Quantity
				
		//Generate PDF
		for(var w=0;w<ItemQtyArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', '***ItemQtyArray ['+w+']==' + ItemQtyArray[w]);
		}	
		for(var k=0;k<a_AllPrimaryTherapist.length;k++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', '***a_AllPrimaryTherapist ['+k+']==' + a_AllPrimaryTherapist[k]);
		}				
		ConsolidatePrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,ItemQtyArray,r_TotalAmount,r_AllAmountDue)
		
	}//for
	
	
	
	
	//Second Array a_BillToAddress
	
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();		
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();	
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();	
		
		
	    i_cnt=0;
		var ItemQtyArray=new Array();
		var flagItem=0;
		
			
		var s_PayeeAddress='';	
		var a_SplitBillToAddress=new Array();				
		a_SplitBillToAddress=a_BillToAddress[a].split('#$');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
		
		if(a_SplitBillToAddress[0]!=null)
		{
			if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
			{
				s_PayeeAddress=a_SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);			    	
			}
			
			//Get The Data from Invoice
			for(var n=2;n<=a_SplitBillToAddress.length;n++)
			{	
			    var i_cntth=0;						
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'Invoice ID====' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{								
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
												
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var r_SplitQty=0;
						if(s_InvoiceItemQuantity!=null && s_InvoiceItemQuantity!='')
						{						   
						    r_SplitQty=s_InvoiceItemQuantity;
						}						
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						var r_ItemAmt=0;
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='')
						{
							r_ItemAmt=s_InvoiceItemAmount;
						}						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						var r_SplitTax=new Array();
						if(s_InvoiceITax!=null && s_InvoiceITax!='')
						{						   
						    r_SplitTax=s_InvoiceITax.split('%');
						}
						else
						{
							r_SplitTax[0]=0;
						}
						
						
				//=========================================================================================================								
						if(ItemQtyArray.length==0)
						{
							ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemAmt;
						}
						else if(ItemQtyArray.length!=0)
						{
							for(var d=0;d<ItemQtyArray.length;d++)
							{
								var i_flagcheck=0;
								var a_SplitItemArray=new Array();
								a_SplitItemArray=ItemQtyArray[d].split('#$');
								if(a_SplitItemArray[0]==s_InvoiceItemValue)
								{
									var r_CalQty=a_SplitItemArray[2]+parseFloat(r_SplitQty);
									var r_CalTax=a_SplitItemArray[3]+parseFloat(r_SplitTax[0]);
									var r_CalAmt=a_SplitItemArray[4]+parseFloat(r_ItemAmt);
									
									ItemQtyArray[d]=a_SplitItemArray[1]+'#$'+r_CalQty+'#$'+r_CalTax+'#$'+r_CalAmt;
								    i_flagcheck=1;									
								}//if
								if(i_flagcheck!=1)
								{
									ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemAmt;
								}//if
							}// for							
						} //else if
	           //=========================================================================================================								
													
						
						
					}//for
					if(a_AllPrimaryTherapist.length==0)
						{
							a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;	
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', '***s_TherapistID 1 ==' + s_TherapistID);						
						}
						else if(a_AllPrimaryTherapist.length!=0)
						{
							for(var g=0;g<a_AllPrimaryTherapist.length;g++)
							{								
								if(a_AllPrimaryTherapist[g]!=s_TherapistID)
								{													
									a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
								}//if								
									
							}// for							
						} //else if
	            
				}//if
					
				}//if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')						
			}//for(var n=1;n<a_SplitBillToAddress.length;n++)		
			
			
		}//if(a_SplitBillToAddress[0]!=null)
		
	    // loop for count each Item quantity
		
				
		for(var w=0;w<ItemQtyArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', '***ItemQtyArray ['+w+']==' + ItemQtyArray[w]);
		}
		
		for(var k=0;k<a_AllPrimaryTherapist.length;k++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', '***a_AllPrimaryTherapist ['+k+']==' + a_AllPrimaryTherapist[k]);
		}
		//Generate PDF		
		ConsolidatePrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,ItemQtyArray,r_TotalAmount,r_AllAmountDue)	
	}
}//END OF FOR
// END OF CONSOLIDATED

// function for seperate array elements



//**********************************************************************************************************
                    //   FUNCTION FOR SUMMARIZE

//**********************************************************************************************************




function FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
{
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'IN FunctionSummarize' );
	
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var i_cntth=0;		
		var s_PayeeAddress='';		
		var a_SplitStoredArray=new Array();			
		a_SplitStoredArray=a_StoredArray[s].split('#$');
		
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=2;n<=a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
												
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						
						
						
										
						r_AllAmountDue=r_AllAmountDue + parseFloat(r_AmountRemaining);
						a_InvoiceID[i_ArrCount]=a_SplitStoredArray[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						nlapiLogExecution('DEBUG', 'FunctionSummarize', '*****s_InvoiceItemQuantity==*****==' + s_InvoiceItemQuantity);
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//r_TotalAmount=r_TotalAmount+parseFloat(s_InvoiceItemAmount);
					}
                 
				 if(a_AllPrimaryTherapist.length==0)
						{
							a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
						}
						else if(a_AllPrimaryTherapist.length!=0)
						{
							for(var g=0;g<a_AllPrimaryTherapist.length;g++)
							{								
								if(a_AllPrimaryTherapist[g]!=s_TherapistID)
								{													
									a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
								}//if								
									
							}// for							
						} //else if		
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
		nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);		
		var i_array=1;
		summaryPrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue)
		
		
	}//for
	
	

	
//Second Array a_BillToAddress
    
	var a_ItemArray=new Array();
	var a_SplitItemArray=new Array();
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var i_cntth=0;	
			
		var s_PayeeAddress='';	
		var a_SplitBillToAddress=new Array();				
		a_SplitBillToAddress=a_BillToAddress[a].split('#$');
		
		if(a_SplitBillToAddress[0]!=null)
		{
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
			{
				s_PayeeAddress=a_SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'getitemtype', '******Final a_SplitBillToAddress******' + a_SplitBillToAddress[0]);
			//Get The Data from Invoice
			for(var n=2;n<=a_SplitBillToAddress.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitBillToAddress['+n+']=' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
								
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;												
						
						
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_AllPrimaryTherapist ==' + a_AllPrimaryTherapist);
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);																		
						a_InvoiceID[i_ArrCount]=a_SplitBillToAddress[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						nlapiLogExecution('DEBUG', 'FunctionSummarize', '*****s_InvoiceItemQuantity==*****' + s_InvoiceItemQuantity);
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'TotalAmountInSecondArray==' + r_TotalAmount);
					}
					if(a_AllPrimaryTherapist.length==0)
					{
						a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
					}
					else if(a_AllPrimaryTherapist.length!=0)
					{
						for(var g=0;g<a_AllPrimaryTherapist.length;g++)
						{								
							if(a_AllPrimaryTherapist[g]!=s_TherapistID)
							{													
								a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
							}//if								
								
						}// for							
					} //else if		
                 
				}
					
				}						
			}		
			
		}
		//Generate PDF
		
		
			
		
		nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);			
		var i_array=2;
		summaryPrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue)
		
	}
	//for(a=0;a<a_ItemArray.length;a++)
	//{
	//	nlapiLogExecution('DEBUG', 'getitemtype', 'a_ItemArray['+a+']=' + a_ItemArray[a]);
	//}
}  //End FOR

//    END OF FUNCTION SUMMARIZE


///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue)
{
	nlapiLogExecution('DEBUG', 'PDF', 'i m in summaryPrintPdf');
	//var s_PayeeAddress=s_customerName+"<br\/>"+s_PayeeAddress;
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='SI_'+i_customerID+'_'+timeStamp;
	}
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var s_TherapistList='';
	nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist length= ' + a_AllPrimaryTherapist.length);
	
    for(var f=0;f<a_AllPrimaryTherapist.length;f++)
	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist['+f+']=' + a_AllPrimaryTherapist[f]);
	}

	/*
for(var c=0;c<a_AllPrimaryTherapist.length;c++)
	{
		if(c==0)
		{
			s_TherapistList=a_AllPrimaryTherapist[c];
		}
		else
		{
			s_TherapistList=s_TherapistList+', '+a_AllPrimaryTherapist[c];
		}
		
	}
	
*/
	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\"><h2>Summary Invoice<\/h2><\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">";
strVar += "		<p>";
strVar += "		52 Victoria Street<br\/>";
strVar += "		Aurora ON L4G 1R2<br\/>";
strVar += "		Canada<br\/>";
strVar += "		Ph: 905-751-0970<br\/>";
strVar += "		<a href=\"http:\/\/www.blue-balloon.com\">www.blue-balloon.com<\/a>";
strVar += "		<\/p>		";
//strVar += "		"+CompanyAddrass+"<\/td>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"200\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td  align=\"left\" width=\"150\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_customerName+"<br\/>"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"100%\">";
strVar += "	      <tr>";
strVar += "	        <td >&nbsp;<\/td>";
strVar += "			";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td >&nbsp;<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+s_TherapistList+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
strVar += "		<tr background-color=\"#000000\">";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";
var pageBreak=7;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;
 for (var k = 1; a_InvoiceDate != null && k < a_InvoiceDate.length; k++) 
 {
 	 
	if(flagCheck2==1)
	{
	 strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
	    strVar += "		<tr background-color=\"#000000\">";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";	
	 flagCheck2=0;	
	}
	if(flagCheck3==1)
	{
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "		<tr background-color=\"#000000\">";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";
		flagCheck3=0;	
	}
 	strVar += "		<tr >";
 	if (a_InvoiceDate[k] != null && a_InvoiceDate[k] !='NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">" + a_InvoiceDate[k] + "<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_InvoiceID[k] != null && a_InvoiceID[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_InvoiceID[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_ServiceDate[k] != null && a_ServiceDate[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_ServiceDate[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_InvoiceItems[k] != null && a_InvoiceItems[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_InvoiceItems[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
 	if (a_InvItemQuantity[k] != null && a_InvItemQuantity[k] != 'NaN' && a_InvItemQuantity[k] !='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_InvItemQuantity[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (a_ItemDescription[k] != null && a_ItemDescription[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_Comments[k] != null && a_Comments[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_Comments[k]+"<\/td>";
		//strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
 	
	if (a_ItemTax[k] != null && a_ItemTax[k] != 'NaN') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(a_ItemTax[k])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (a_Amount[k] != null && a_Amount[k] != 'NaN' && a_Amount[k] != '') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_Amount[k]+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	} 	
 	strVar += "			<\/tr>";
	
   cnt++;
	
	if(k >pageBreak && flag==0)
	{
		strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		
		//if(a_InvoiceDate.length>k)
	
		//strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";	
		// strVar += "	<tr>";
		// strVar += "	<td>";
		pageBreak=k;
		cnt=0;
		flag=1;
		flagCheck2=1;
	}
	else if(flag==1)
	{
		if(cnt==10 && a_InvoiceDate.length-1!=k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			strVar += "<p style=\"page-break-before: always\">";
			//strVar += "		<table>";
			var flagCheck3=1;
			cnt=0;
		}
		/*else if(a_InvoiceDate.length-1==k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			
		}
		flagCheck=1;
		*/
	}//if
	
  
	
	
 }//for flagCheck
 
        /*strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">Total<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/p>";
		*/
 
if(flag==0)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', '****flag==0*****'+flag);
	//strVar += "		<\/td>";
	//strVar += "		<\/tr>";
	/*strVar += "  <tr >";
	strVar += "   <td colspan=\"7\"><\/td>  ";
	strVar += "   <td >Amount<\/td>   ";
	strVar += "   <td >11222<\/td>  ";
	strVar += "   <\/tr>";
	
	strVar += "		<\/table>";
	strVar += "<p style=\"page-break-before: always\">";
	strVar += "		<table>"; //colspan
	*/
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";
	//strVar += "<\/p>";
	strVar += "	<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
}//if
else if(flag==1)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' ####### flag==1 @@@@@@@@@='+flag);
	
	
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "	  <td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";	
	
	strVar += "<\/p>";
	//strVar += "		<\/table>";
	
}//else if


/*
strVar += "		<table class=\"textreg\" border=\"0.1pt\" border-color=\"silver\" width=\"20%\" align =\"right\" >";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Total<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Amount Due<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<\/table>";
*/
//strVar += "	<\/td>";
//strVar += "	<\/tr>";
//strVar += "<\/table>";


			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//--------------------Attached pdf file logic--------------
		
			var tempArray=new Array();
			var cnt1=0;
			for(var m=1;m<a_InvoiceID.length;m++)
			{
				if(tempArray.length==0)
				{
					tempArray[cnt1++]=a_InvoiceID[m];
				}
				else if(tempArray.length!=0)
				{
					for(var m2=0 ;m2<tempArray.length ;m2++)
					{
						if(tempArray[m2]!=a_InvoiceID[m])
						{
						  tempArray[cnt1++]=a_InvoiceID[m];	
						}
					}//
					
				}//else if
				
			}//for m
			
			for(var s=0;s<tempArray.length;s++)
			{
				var invoiceObj=nlapiLoadRecord('invoice',tempArray[s]);
				var createdfrom=invoiceObj.getFieldValue('createdfrom');
				
				if(createdfrom!=null)
				{
					var type = 'file';       // the record type for the record being attached
					var id = fileId ;          // the internal ID of an existing jpeg in the file cabinet
					var type2 = 'salesorder'; // the record type for the record being attached to
					var id2 = createdfrom; // this is the internal ID for the customer
					var attributes = null;
					
					nlapiAttachRecord(type, id, type2, id2, attributes);
					
				}//if
				
				
			}//for s
			
		///--------------------- Sending email Logic ------------------
		var auther='19182';   /// carlos id
		var Subject="Monthly Invoice Report";
		var bcc='';
		//nlapiSendEmail(auther,s_customerEmail, Subject, 'Please see the attachment', null, null, null, file);	
			

}//function 









///////////////// ------------- Consolidate Print ----------------------//////////////////////////////////

function ConsolidatePrintPdf(a_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,ItemQtyArray,r_TotalAmount,r_AllAmountDue)         
{	
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='CI_'+i_customerID+'_'+timeStamp;
	}
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var s_TherapistList='';
	for(var c=0;c<a_AllPrimaryTherapist.length;c++)
	{
		if(c==0)
		{
			s_TherapistList=a_AllPrimaryTherapist[c];
		}
		else
		{
			s_TherapistList=s_TherapistList+', '+a_AllPrimaryTherapist[c];
		}
		
	}
	
	
	
	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\"><h2>Summary Invoice<\/h2><\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">";
strVar += "		<p>";
strVar += "		52 Victoria Street<br\/>";
strVar += "		Aurora ON L4G 1R2<br\/>";
strVar += "		Canada<br\/>";
strVar += "		Ph: 905-751-0970<br\/>";
strVar += "		<a href=\"http:\/\/www.blue-balloon.com\">www.blue-balloon.com<\/a>";
strVar += "		<\/p>		";
//strVar += "		"+CompanyAddrass+"<\/td>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"380\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" width=\"230\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td  align=\"left\" width=\"150\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td align=\"left\" width=\"250\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"100%\">";
strVar += "	      <tr>";
strVar += "	        <td >&nbsp;<\/td>";
strVar += "			";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td >&nbsp;<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+s_TherapistList+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
strVar += "		<tr background-color=\"#000000\">";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";
var pageBreak=7;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;
var SpliteArray1=new Array();
var totalAmount=0;
 for (var k = 0; ItemQtyArray != null && k < ItemQtyArray.length; k++) 
 {
 	SpliteArray1=ItemQtyArray[k].split('#$');
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' item id[0]='+SpliteArray1[0]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' item name[0]='+SpliteArray1[1]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Qty[0]='+SpliteArray1[2]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Tax[0]='+SpliteArray1[3]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Amt[0]='+SpliteArray1[4]);
 	 
	if(flagCheck2==1)
	{
	 strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
	    strVar += "		<tr background-color=\"#$000000\">";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";	
	 flagCheck2=0;	
	}
	if(flagCheck3==1)
	{
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "		<tr background-color=\"#000000\">";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";
		flagCheck3=0;	
	}
 	strVar += "		<tr >";
 	/*
if (a_InvoiceDate[k] != null && a_InvoiceDate[k] !='NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">" + a_InvoiceDate[k] + "<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_InvoiceID[k] != null && a_InvoiceID[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_InvoiceID[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_ServiceDate[k] != null && a_ServiceDate[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_ServiceDate[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
*/
 	if (SpliteArray1[1]!= null && SpliteArray1[1]!= 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+SpliteArray1[1]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
 	if (SpliteArray1[2] != null && SpliteArray1[2] != 'NaN' && SpliteArray1[2] !='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+SpliteArray1[2]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	/*
if (a_ItemDescription[k] != null && a_ItemDescription[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_Comments[k] != null && a_Comments[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_Comments[k]+"<\/td>";
		//strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
*/
 	
	if (SpliteArray1[3] != null && SpliteArray1[3] != 'NaN') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+SpliteArray1[3]+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (SpliteArray1[4]  != null && SpliteArray1[4]  != 'NaN' && SpliteArray1[4] != '') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+SpliteArray1[4] +"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	} 	
 	strVar += "			<\/tr>";
	totalAmount=parseFloat(totalAmount)+parseFloat(SpliteArray1[4]);
   cnt++;
	
	if(k >pageBreak && flag==0)
	{
		strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		
		//if(a_InvoiceDate.length>k)
	
		//strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";	
		// strVar += "	<tr>";
		// strVar += "	<td>";
		pageBreak=k;
		cnt=0;
		flag=1;
		flagCheck2=1;
	}
	else if(flag==1)
	{
		if(cnt==10 && a_InvoiceDate.length-1!=k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			strVar += "<p style=\"page-break-before: always\">";
			//strVar += "		<table>";
			var flagCheck3=1;
			cnt=0;
		}
		/*else if(a_InvoiceDate.length-1==k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			
		}
		flagCheck=1;
		*/
	}//if
	
  
	
	
 }//for flagCheck
 
        /*strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">Total<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/p>";
		*/
 
if(flag==0)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', '****flag==0*****'+flag);
	//strVar += "		<\/td>";
	//strVar += "		<\/tr>";
	/*strVar += "  <tr >";
	strVar += "   <td colspan=\"7\"><\/td>  ";
	strVar += "   <td >Amount<\/td>   ";
	strVar += "   <td >11222<\/td>  ";
	strVar += "   <\/tr>";
	
	strVar += "		<\/table>";
	strVar += "<p style=\"page-break-before: always\">";
	strVar += "		<table>"; //colspan
	*/
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+totalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";
	//strVar += "<\/p>";
	strVar += "	<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
}//if
else if(flag==1)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' ####### flag==1 @@@@@@@@@='+flag);
	
	
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	//strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	//strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	//strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	//strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	//strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "	  <td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+totalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";	
	
	strVar += "<\/p>";
	//strVar += "		<\/table>";
	
}//else if


/*
strVar += "		<table class=\"textreg\" border=\"0.1pt\" border-color=\"silver\" width=\"20%\" align =\"right\" >";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Total<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Amount Due<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<\/table>";
*/
//strVar += "	<\/td>";
//strVar += "	<\/tr>";
//strVar += "<\/table>";


			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			//nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
			
			
			
			
	      
			//----------------------------------
			
			

}//function 


