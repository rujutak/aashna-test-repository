/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/



function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
   nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	//var filters = new Array();
    //var columns = new Array();
 	

   // filters[0] = new nlobjSearchFilter('entityid', null, 'is', item);
   // columns[0] = new nlobjSearchColumn('entityid');
 	var searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
  	if(searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'searchResult.length=' + searchResult.length);
		//for(var k=0; searchResult != null && k <searchResult.length; k++)
			
		
		
		for(var k=6; searchResult != null && k <searchResult.length; k++)
		{			
			var result = searchResult[k];
			// return all columns associated with this search
			var columns = result.getAllColumns();
			var columnLen = columns.length;
			//nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var column = columns[0];
			var customerName = result.getValue(column);			
			nlapiLogExecution('DEBUG', 'getitemtype', 'customerName['+k+']=' + customerName);
			
			var column = columns[1];
			var customerID = result.getValue(column);			
			nlapiLogExecution('DEBUG', 'getitemtype', 'customerID['+k+']=' + customerID);
			
			var LoadCustomerRec=nlapiLoadRecord('customer', customerID);
			//nlapiLogExecution('DEBUG', 'getitemtype', 'LoadCustomerRec['+k+']=' + LoadCustomerRec);
			
			/*
            if (LoadCustomerRec != null && LoadCustomerRec != '') 
			{
				var Bill_To_Addr = LoadCustomerRec.getFieldValue('comments');
				nlapiLogExecution('DEBUG', 'getitemtype', 'LoadCustomerRec[' + k + ']=' + LoadCustomerRec);
				
				var LineCountAddr = LoadCustomerRec.getLineItemCount('addressbook');
				nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' LineCountAddr==' + LineCountAddr);
				
				if (LineCountAddr > 0 && LineCountAddr != null && LineCountAddr != '') 
				{					
					for (var x = 1; x <= LineCountAddr; x++) 
					{
						var DefaultBillingAddrVal = LoadCustomerRec.getLineItemValue('addressbook', 'defaultbilling', x);
						nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Default Billing Address[' + x + ']==' + DefaultBillingAddrVal);
						if (DefaultBillingAddrVal == 'T') 
						{
							var Address = LoadCustomerRec.getLineItemValue('addressbook', 'addressee', x);
							nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Billing Address[' + x + ']==' + Address);
						}
					}
				}
			}		
            */	
			var column = columns[2];
			var customerEmail = result.getValue(column);			
			//nlapiLogExecution('DEBUG', 'getitemtype', 'customerEmail['+k+']=' + customerEmail);
			
			var column = columns[3];
			var BPInvoiceType = result.getValue(column);			
			nlapiLogExecution('DEBUG', 'CUSTOMERsEARCH', 'BPInvoiceType['+k+']=' + BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var filtersSearch = new Array();
			filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', customerName);
            // var columnsSearch = new Array();
			
			var searchRes= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', filtersSearch, null);
			nlapiLogExecution('DEBUG', 'getitemtype', 'searchRes['+k+'] len=' + searchRes.length);
			var BillToAddress=new Array();
			var StoredArray=new Array();
			var SplitArray=new Array();
			var SplitArrayPayeeNull=new Array();
			
				for (var i = 0; searchRes != null && i < searchRes.length; i++) 
				{
					var resultInvoice = searchRes[i];
					// return all columns associated with this search
					var columnsInvoice = resultInvoice.getAllColumns();
					var columnLen2 = columnsInvoice.length;
					nlapiLogExecution('DEBUG', '***columnLen2 == ' + columnLen2);
					
					var Columns = columnsInvoice[0];
					var Customer_Created_Date = resultInvoice.getValue(Columns);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + Customer_Created_Date);		
					
					//var Columns= columnsInvoice[1];
					//var Columns1 = resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
					
					//var Columns= columnsInvoice[2];
					//var Columns2 = resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
					
					var Columns = columnsInvoice[3];
					var Invoice_ID = resultInvoice.getValue(Columns);
					nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + Invoice_ID);
					
					var LoadInvoiceRec = nlapiLoadRecord('invoice', Invoice_ID);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'LoadInvoiceRec['+i+']=' + LoadInvoiceRec);
					
					//var StoredArray=new Array();
					if (LoadInvoiceRec != null && LoadInvoiceRec != '') {
						var PayerNew = LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
						//nlapiLogExecution('DEBUG', 'getitemtype', 'PayerNew['+i+']=' + PayerNew);
						
						if (PayerNew != null && PayerNew != '') {
							//*************************************************************************************
							var arrayLength = StoredArray.length;
							if (arrayLength == 0) {
								StoredArray[0] = PayerNew + '#' + Invoice_ID;
							}
							if (arrayLength != 0) {
								var flag = 0;
								//var SplitArray=new Array();
								for (var k = 0; k < StoredArray.length; k++) {
									SplitArray = StoredArray[k].split('#');
									if (SplitArray[0] == PayerNew) {
										StoredArray[k] = StoredArray[k] + '#' + Invoice_ID;
										flag = 1;
										break;
									}
								}
								if (flag != 1) {
									StoredArray[StoredArray.length] = PayerNew + '#' + Invoice_ID;
								}
							}//if(arrayLength!=0)							
						}//if(PayerNew!=null && PayerNew!='')
						var InvoiceBillToAddress = LoadInvoiceRec.getFieldValue('billaddress');
						//nlapiLogExecution('DEBUG', 'getitemtype', 'InvoiceBillToAddress= ' + InvoiceBillToAddress);			    	
						
						if (PayerNew == null && InvoiceBillToAddress != null) {
							var BillToArray = BillToAddress.length;
							if (BillToArray == 0) {
								BillToAddress[0] = InvoiceBillToAddress + '#' + Invoice_ID;
							}
							if (BillToArray != 0) {
								var flag = 0;
								for (var l = 0; l < BillToAddress.length; l++) {
									SplitArrayPayeeNull = BillToAddress[l].split('#');
									if (SplitArrayPayeeNull[0] == InvoiceBillToAddress) {
										BillToAddress[l] = BillToAddress[l] + '#' + Invoice_ID;
										flag = 1;
										break;
									}
								}
								if (flag != 1) {
									BillToAddress[BillToAddress.length] = BillToAddress + '#' + Invoice_ID;
								}
							}//if(arrayLength!=0)
						}
						
					}
					
					var Columns = columnsInvoice[4];
					var Invoice_Date = resultInvoice.getValue(Columns);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + Invoice_Date);
					
					var Columns = columnsInvoice[5];
					var BP_Invoice_Type = resultInvoice.getValue(Columns);
					nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + BP_Invoice_Type);
					
			}//for i - invoice
			
			//Code for Summarize
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			if(BPInvoiceType==3) //3 means summary BPInvoiceType
			{
				//FunctionSummarize(StoredArray,BillToAddress,customerName)
			}
			
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************			
			//Code for Consolidated
			
            if(BPInvoiceType==2)
			{
				functionConsolidated(StoredArray,BillToAddress,customerName)
			}
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			/*
			if(BPInvoiceType==1)
			{
				for(s=0;s<StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'StoredArray['+s+']=' + StoredArray[s]);
			}
			for(a=0;a<BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'BillToAddress['+a+']=' + BillToAddress[a]);
			}
			}
*/
			
		}//for
  		
  	}//if
 
 	
}//function close







//**********************************************************************************************************
                    
					//   FUNCTION FOR CONSOLIDATED

//**********************************************************************************************************




function functionConsolidated(StoredArray,BillToAddress,customerName)
{
	nlapiLogExecution('DEBUG', 'functionConsolidated', 'IN CONSOLIDATED FUNCTION' );
	var SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'getitemtype', 'StoredArray Length=' + StoredArray.length);
				
	for(s=0;s<StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var ArrCount=0;
		var TotalAmount=0;	
		var AsOfDate='';
		var AllPrimaryTherapist='';
		var AllAmountDue=0;
		var ItemArray=new Array();
		var SplitItemArray=new Array();
				
		var PayeeAddress='';					
		SplitStoredArray=StoredArray[s].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'SplitStoredArray['+s+']=' + SplitStoredArray[s]);
		if(SplitStoredArray[0]!=null)
		{
			var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitStoredArray[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'LoadPayerRec['+i+']=' + LoadPayerRec);
	        
			if(LoadPayerRec!=null && LoadPayerRec!='')
			{
				PayeeAddress=LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'PayeeAddress['+i+']=' + PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'SplitStoredArray Length=' + SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'SplitStoredArray['+n+']=' + SplitStoredArray[n]);
				
				var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitStoredArray[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
				
	            if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
				{
			    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceDate['+i+']=' + InvoiceDate);
		    			
				var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'ServiceDate['+i+']=' + ServiceDate);
		    	
				var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'LineItemCount ==' + LineItemCount);
				
				var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'LineItemCount ==' + LineItemCount);
				
				var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'AmountRemaining ==' + AmountRemaining);
				
				if (LineItemCount != null && LineItemCount != '') 
				{
					
					
					//Invoice Line Count
	                for (var j = 1; j <= LineItemCount; j++) 
					{
			
						var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
						
						var InvoiceItemValue = LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemValue ['+j+']==' + InvoiceItemValue);
						
						var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
						
	           	//=========================================================================================================								
						if(InvoiceItemValue!=null && InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var arrayLength=ItemArray.length;
							if(arrayLength==0)
							{
								ItemArray[0]=InvoiceItemValue+'#'+InvoiceItemQuantity;
							}
							if(arrayLength!=0)
							{
								var flag=0;
								//var SplitArray=new Array();
								for(var p=0;p<ItemArray.length;p++)
								{								
									SplitItemArray=ItemArray[p].split('#');
									if(SplitItemArray[0]==InvoiceItemValue)
									{
										ItemArray[p]=ItemArray[p]+'#'+InvoiceItemQuantity;
										flag=1;
										break;
									}
								}
								if(flag!=1)
								{
									ItemArray[ItemArray.length]=InvoiceItemValue+'#'+InvoiceItemQuantity;
								}
							}//if(arrayLength!=0)							
					       			
						
						}//if(InvoiceItemValue!=null && InvoiceItemValue!='')
								
						
						
	           //=========================================================================================================								
				
						var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
						
						var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
						
						var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceITax ['+j+']==' + InvoiceITax);
						
						ArrCount=ArrCount+1;
						a_InvoiceDate[ArrCount]=InvoiceDate;
						
						if(AsOfDate=='')
						{
							AsOfDate=''+InvoiceDate;
						}									
						else
						{
							AsOfDate=AsOfDate+', '+InvoiceDate;
						}	
						if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
						{
							if(AllPrimaryTherapist=='')
							{
								AllPrimaryTherapist=''+PrimaryTherapist
							}
							else
							{
								AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
							}										
						}
										
						AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);
						a_InvoiceID[ArrCount]=SplitStoredArray[n];
						a_ServiceDate[ArrCount]=ServiceDate;
						a_InvoiceItems[ArrCount]=InvoiceItemText;
						a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
						a_ItemDescription[ArrCount]=InvoiceItemDescription;
						//a_Comments[ArrCount]=
						a_ItemTax[ArrCount]=InvoiceITax
						a_Amount[ArrCount]=InvoiceItemAmount;
						if (InvoiceItemAmount != null && InvoiceItemAmount != '') 
						{
							TotalAmount = TotalAmount + parseFloat(InvoiceItemAmount);
						}
						//TotalAmount=TotalAmount+parseFloat(InvoiceItemAmount);
					}
	             
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
							
		var array=1;
		//summaryPrintPdf(AsOfDate,AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
		
		for(var w=0;w<ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', 'ItemArray ['+w+']==' + ItemArray[w]);
		}
	}//for
	
	var SplitBillToAddress=new Array();
	
	
	//Second Array BillToAddress
	
	for(a=0;a<BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var ArrCount=0;
		var TotalAmount=0;	
		var AsOfDate='';
		var AllPrimaryTherapist='';
		var AllAmountDue=0;
		var ItemArray=new Array();
		var SplitItemArray=new Array();
			
		var PayeeAddress='';					
		SplitBillToAddress=BillToAddress[a].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'BillToAddress['+a+']=' + BillToAddress[a]);
		
		if(SplitBillToAddress[0]!=null)
		{
			//var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitBillToAddress[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'LoadPayerRec['+i+']=' + LoadPayerRec);
	        
			//if(LoadPayerRec!=null && LoadPayerRec!='')
			//{
			//	PayeeAddress=LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		    //    nlapiLogExecution('DEBUG', 'functionConsolidated', 'PayeeAddress['+i+']=' + PayeeAddress);			    	
			//}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'SplitBillToAddress Length=' + SplitBillToAddress.length);
			//Get The Data from Invoice
			for(var n=1;n<SplitBillToAddress.length;n++)
			{							
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'Invoice ID====' + SplitBillToAddress[n]);
				
				var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitBillToAddress[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
				
	            if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
				{
			    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceDate['+i+']=' + InvoiceDate);
		    			
				var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'ServiceDate['+i+']=' + ServiceDate);
		    	
				var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'LineItemCount ==' + LineItemCount);
				
				var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'LineItemCount ==' + LineItemCount);
				
				var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'AmountRemaining ==' + AmountRemaining);
				
				if (LineItemCount != null && LineItemCount != '') 
				{								
					//Invoice Line Count
	                for (var j = 1; j <= LineItemCount; j++) 
					{
			
						var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
						
						var InvoiceItemValue = LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
												
						var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
						
										
						
				//=========================================================================================================								
						if(InvoiceItemValue!=null && InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var arrayLength=ItemArray.length;
							if(arrayLength==0)
							{
								ItemArray[0]=InvoiceItemValue+'#'+InvoiceItemQuantity;
							}
							if(arrayLength!=0)
							{
								var flag=0;
								//var SplitArray=new Array();
								for(var p=0;p<ItemArray.length;p++)
								{								
									SplitItemArray=ItemArray[p].split('#');
									if(SplitItemArray[0]==InvoiceItemValue)
									{
										ItemArray[p]=ItemArray[p]+'#'+InvoiceItemQuantity;
										flag=1;
										break;
									}
								}
								if(flag!=1)
								{
									ItemArray[ItemArray.length]=InvoiceItemValue+'#'+InvoiceItemQuantity;
								}
							}//if(arrayLength!=0)							
					       			
						
						}//if(InvoiceItemValue!=null && InvoiceItemValue!='')
											
						
	           //=========================================================================================================								
				
						var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
						
						var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
						
						var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'InvoiceITax ['+j+']==' + InvoiceITax);
						
						ArrCount=ArrCount+1;
						a_InvoiceDate[ArrCount]=InvoiceDate;
						if(AsOfDate=='')
						{
							AsOfDate=''+InvoiceDate;
						}									
						else
						{
							AsOfDate=AsOfDate+', '+InvoiceDate;
						}
						if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
						{
							if(AllPrimaryTherapist=='')
							{
								AllPrimaryTherapist=''+PrimaryTherapist
							}
							else
							{
								AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
							}										
						}
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'SplitBillToAddress[n] ==' + SplitBillToAddress[n];);
						AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);																		
						a_InvoiceID[ArrCount]=SplitBillToAddress[n];
						a_ServiceDate[ArrCount]=ServiceDate;
						a_InvoiceItems[ArrCount]=InvoiceItemText;
						a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
						a_ItemDescription[ArrCount]=InvoiceItemDescription;
						//a_Comments[ArrCount]=
						a_ItemTax[ArrCount]=InvoiceITax
						a_Amount[ArrCount]=InvoiceItemAmount;
						if (InvoiceItemAmount != null && InvoiceItemAmount != '') 
						{
							TotalAmount = TotalAmount + parseFloat(InvoiceItemAmount);
						}
						
					}//for
	             //nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + TotalAmount);
				}//if
					
				}//if(LoadInvoiceRec!=null && LoadInvoiceRec!='')						
			}//for(var n=1;n<SplitBillToAddress.length;n++)		
			
			
		}//if(SplitBillToAddress[0]!=null)
		
		for(var w=0;w<ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', 'ItemArray ['+w+']==' + ItemArray[w]);
		}
		//Generate PDF		
		
		//nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + TotalAmount);			
		//var array=2;
		//summaryPrintPdf(AsOfDate,AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
		
	}
}//END OF FOR
// END OF CONSOLIDATED





//**********************************************************************************************************
                    //   FUNCTION FOR SUMMARIZE

//**********************************************************************************************************

function FunctionSummarize(StoredArray,BillToAddress,customerName)
{
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'IN FunctionSummarize' );
	var SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'StoredArray Length=' + StoredArray.length);
				
	for(s=0;s<StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var ArrCount=0;
		var TotalAmount=0;	
		var AsOfDate='';
		var AllPrimaryTherapist='';
		var AllAmountDue=0;
				
		var PayeeAddress='';					
		SplitStoredArray=StoredArray[s].split('#');
		
		if(SplitStoredArray[0]!=null)
		{
			var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitStoredArray[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LoadPayerRec['+i+']=' + LoadPayerRec);
            
			if(LoadPayerRec!=null && LoadPayerRec!='')
			{
				PayeeAddress=LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'PayeeAddress['+i+']=' + PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'SplitStoredArray Length=' + SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'SplitStoredArray['+n+']=' + SplitStoredArray[n]);
				
				var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitStoredArray[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
				
                if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
				{
			    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceDate['+i+']=' + InvoiceDate);
		    			
				var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'ServiceDate['+i+']=' + ServiceDate);
		    	
				var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LineItemCount ==' + LineItemCount);
				
				var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LineItemCount ==' + LineItemCount);
				
				var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'AmountRemaining ==' + AmountRemaining);
				
				if (LineItemCount != null && LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= LineItemCount; j++) 
					{
			
						var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
						
						var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
						
						var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
						
						var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
						
						var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceITax ['+j+']==' + InvoiceITax);
						
						ArrCount=ArrCount+1;
						a_InvoiceDate[ArrCount]=InvoiceDate;
						
						if(AsOfDate=='')
						{
							AsOfDate=''+InvoiceDate;
						}									
						else
						{
							AsOfDate=AsOfDate+', '+InvoiceDate;
						}	
						if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
						{
							if(AllPrimaryTherapist=='')
							{
								AllPrimaryTherapist=''+PrimaryTherapist
							}
							else
							{
								AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
							}										
						}
										
						AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);
						a_InvoiceID[ArrCount]=SplitStoredArray[n];
						a_ServiceDate[ArrCount]=ServiceDate;
						a_InvoiceItems[ArrCount]=InvoiceItemText;
						a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
						a_ItemDescription[ArrCount]=InvoiceItemDescription;
						//a_Comments[ArrCount]=
						a_ItemTax[ArrCount]=InvoiceITax
						a_Amount[ArrCount]=InvoiceItemAmount;
						if (InvoiceItemAmount != null && InvoiceItemAmount != '') 
						{
							TotalAmount = TotalAmount + parseFloat(InvoiceItemAmount);
						}
						//TotalAmount=TotalAmount+parseFloat(InvoiceItemAmount);
					}
                 
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
				
		var array=1;
		summaryPrintPdf(AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
		
		
	}//for
	
	

	
//Second Array BillToAddress
    var SplitBillToAddress=new Array();
	var ItemArray=new Array();
	var SplitItemArray=new Array();
	for(a=0;a<BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var ArrCount=0;
		var TotalAmount=0;	
		var AsOfDate='';
		var AllPrimaryTherapist='';
		var AllAmountDue=0;
		
			
		var PayeeAddress='';					
		SplitBillToAddress=BillToAddress[a].split('#');
		
		if(SplitBillToAddress[0]!=null)
		{
			//var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitBillToAddress[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LoadPayerRec['+i+']=' + LoadPayerRec);
            
			if(SplitBillToAddress[0]!=null && SplitBillToAddress[0]!='')
			{
				PayeeAddress=SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 'PayeeAddress== ' + PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'getitemtype', '******Final SplitBillToAddress******' + SplitBillToAddress[0]);
			//Get The Data from Invoice
			for(var n=1;n<SplitBillToAddress.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'SplitBillToAddress['+n+']=' + SplitBillToAddress[n]);
				
				var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitBillToAddress[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
				
                if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
				{
			    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceDate['+i+']=' + InvoiceDate);
		    			
				var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'ServiceDate['+i+']=' + ServiceDate);
		    	
				var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LineItemCount ==' + LineItemCount);
				
				var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'LineItemCount ==' + LineItemCount);
				
				var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'AmountRemaining ==' + AmountRemaining);
				
				if (LineItemCount != null && LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= LineItemCount; j++) 
					{
			
						var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
						
						var InvoiceItemValue = LoadInvoiceRec.getLineItemValue('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemValue ['+j+']==' + InvoiceItemValue);
						
						var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
						
						var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
						
						var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
						
						var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'InvoiceITax ['+j+']==' + InvoiceITax);
						
						ArrCount=ArrCount+1;
						a_InvoiceDate[ArrCount]=InvoiceDate;
						if(AsOfDate=='')
						{
							AsOfDate=''+InvoiceDate;
						}									
						else
						{
							AsOfDate=AsOfDate+', '+InvoiceDate;
						}
						if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
						{
							if(AllPrimaryTherapist=='')
							{
								AllPrimaryTherapist=''+PrimaryTherapist
							}
							else
							{
								AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
							}										
						}
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 'AllPrimaryTherapist ==' + AllPrimaryTherapist);
						AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);																		
						a_InvoiceID[ArrCount]=SplitBillToAddress[n];
						a_ServiceDate[ArrCount]=ServiceDate;
						a_InvoiceItems[ArrCount]=InvoiceItemText;
						a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
						a_ItemDescription[ArrCount]=InvoiceItemDescription;
						//a_Comments[ArrCount]=
						a_ItemTax[ArrCount]=InvoiceITax
						a_Amount[ArrCount]=InvoiceItemAmount;
						if (InvoiceItemAmount != null && InvoiceItemAmount != '') 
						{
							TotalAmount = TotalAmount + parseFloat(InvoiceItemAmount);
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'TotalAmountInSecondArray==' + TotalAmount);
					}
                 
				}
					
				}						
			}		
			
		}
		//Generate PDF
		
		
			
		
		//nlapiLogExecution('DEBUG', 'getitemtype', 'TotalAmountInSecondArray==' + TotalAmount);			
		var array=2;
		summaryPrintPdf(AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
		
	}
	for(a=0;a<ItemArray.length;a++)
	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'ItemArray['+a+']=' + ItemArray[a]);
	}
}  //End FOR

//    END OF FUNCTION SUMMARIZE


///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
{
	
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1
	var day = currentTime.getDate()
	var year = currentTime.getFullYear()
	var CurrantDate=month + "/" + day + "/" + year;
	

			var strVar="";
			strVar += "<table border=\"0\" width=\"100%\" cellpadding=\"0\"  height=\"59\">";
			strVar += "	<tr>";
			strVar += "		<td align=\"right\" valign=\"top\" height=\"37\" border-color=\"silver\">";
			strVar += "		<table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "			<tr><td border=\"0.1pt\" border-color=\"silver\">3-Summary Invoice<\/td><\/tr>";
			strVar += "		<\/table>";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "		<td align =\"right\" valign =\"top\" border-color=\"silver\">";
			strVar += "		    <table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "				<tr><td width =\"46%\" border=\"0.1pt\" border-color=\"silver\">As Of Date :<\/td><td  width =\"51%\" border=\"0.1pt\" border-color=\"silver\">"+CurrantDate+"<\/td><\/tr>";
			strVar += "			<\/table>";
			strVar += "	";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "	<td>";
			//strVar += "	";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "		<tr>";
			strVar += "			<td>&#160;<\/td>";
			strVar += "		<\/tr>";
			strVar += "		<\/table>";
			strVar += "		<table width=\"100%\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			
			/*strVar += "				<td width=\"190\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Bill To :<\/td>";
			strVar += "				<td height=\"27\">&#160;<\/td>";
			strVar += "				<td height=\"27\" width=\"215\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<\/td>";
			strVar += "				<td width=\"218\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan)<\/td>";
			*/
			strVar += "    <td width=\"286\" border=\"0.1pt\" border-color=\"silver\">Bill To :<br/>"+PayeeAddress+"<\/td>";
		    strVar += "    <td width=\"185\" >&#160;<\/td>";
		    strVar += "    <td width=\"245\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<br/>"+customerName+"<\/td>";
		    strVar += "    <td width=\"296\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan): "+AllPrimaryTherapist+"<\/td>";
			strVar += "			<\/tr>";
			/*strVar += "			<tr>";
			strVar += "				<td width=\"190\">"+PayeeAddress+"<\/td>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "				<td width=\"215\">"+customerName+"<\/td>";
			strVar += "				<td width=\"218\">&#160;<\/td>";
			strVar += "			<\/tr>";*/
			strVar += "	    <\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "			<tr>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0.1pt\" border-color=\"silver\" align =\"left\">";
			strVar += "			<tr>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Date<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Invoice #<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Service Date <\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Item<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Quantity<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Descriptions<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Comments<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Tax<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Amount<\/td>";
			strVar += "			<\/tr>";
		      // iterate through the results
			  for ( var k = 1; a_InvoiceDate != null && k < a_InvoiceDate.length; k++ )
			  {
			  	strVar += "			<tr>";
			  	strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceID[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_ServiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceItems[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvItemQuantity[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";				
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">&#160;<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemTax[k])+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_Amount[k]+"<\/td>";
			  	strVar += "			<\/tr>";
			  }
			strVar += "		<\/table>";
			strVar += "";
			strVar += "	";
			strVar += "		<table border=\"0\" >";
			strVar += "			<tr>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "				";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "		";
			strVar += "		<table width=\"30%\" align=\"right\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Total<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+TotalAmount+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Amount Due<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+AllAmountDue+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";			
			strVar += "	<\/td>";
			strVar += "	<\/tr>";
			//strVar += "	";		
			strVar += "<\/table>";
			
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body font-size=\"10\">";
			xml += "<p></p>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile('FirstSummaryPDF'+array+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//----------------------------------
			
			

}//function 

//[5:55:15 PM] mangesh: strName += "<td class=\"textreg\" border=\"0.1pt\" border-color=\"silver\">";