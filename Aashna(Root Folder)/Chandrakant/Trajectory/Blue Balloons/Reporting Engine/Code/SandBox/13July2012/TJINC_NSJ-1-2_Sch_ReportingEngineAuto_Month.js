/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/


// Main function
function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
   nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	//var filters = new Array();
    //var a_columns = new Array();
 	

   // a_filters[0] = new nlobjSearchFilter('entityid', null, 'is', item);
   // a_columns[0] = new nlobjSearchColumn('entityid');
 	var a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
  	if(a_searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_searchResult.length=' + a_searchResult.length);
		//for(var k=0; a_searchResult != null && k <a_searchResult.length; k++)
			
		
		//for loop for Customer
		for(var k=6; a_searchResult != null && k <a_searchResult.length; k++)
		{			
			var s_result = a_searchResult[k];
			// return all columns associated with this search
			var a_columns = s_result.getAllColumns();
			var columnLen = a_columns.length;
			//nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var s_column0 = a_columns[0];
			var s_customerName = s_result.getValue(s_column0);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerName['+k+']=' + s_customerName);
			
			var s_column1 = a_columns[1];
			var i_customerID = s_result.getValue(s_column1);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'customerID['+k+']=' + i_customerID);
			
			var o_LoadCustomerRec=nlapiLoadRecord('customer', i_customerID);
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec['+k+']=' + o_LoadCustomerRec);
			
			/*
            if (o_LoadCustomerRec != null && o_LoadCustomerRec != '') 
			{
				var Bill_To_Addr = o_LoadCustomerRec.getFieldValue('comments');
				nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec[' + k + ']=' + o_LoadCustomerRec);
				
				var LineCountAddr = o_LoadCustomerRec.getLineItemCount('addressbook');
				nlapiLogExecution('DEBUG', 'CustomerSearch', ' LineCountAddr==' + LineCountAddr);
				
				if (LineCountAddr > 0 && LineCountAddr != null && LineCountAddr != '') 
				{					
					for (var x = 1; x <= LineCountAddr; x++) 
					{
						var DefaultBillingAddrVal = o_LoadCustomerRec.getLineItemValue('addressbook', 'defaultbilling', x);
						nlapiLogExecution('DEBUG', 'CustomerSearch', ' Default Billing Address[' + x + ']==' + DefaultBillingAddrVal);
						if (DefaultBillingAddrVal == 'T') 
						{
							var Address = o_LoadCustomerRec.getLineItemValue('addressbook', 'addressee', x);
							nlapiLogExecution('DEBUG', 'CustomerSearch', ' Billing Address[' + x + ']==' + Address);
						}
					}
				}
			}		
            */	
			var s_column2 = a_columns[2];
			var s_customerEmail = s_result.getValue(s_column2);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerEmail['+k+']=' + s_customerEmail);
			
			var s_column3 = a_columns[3];
			var s_BPInvoiceType = s_result.getValue(s_column3);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 's_BPInvoiceType['+k+']=' + s_BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var a_filtersSearch = new Array();
			a_filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', s_customerName);
            // var columnsSearch = new Array();
			
			var a_InvoicesearchResults= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', a_filtersSearch, null);
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'a_InvoicesearchResults['+k+'] len=' + a_InvoicesearchResults.length);
			var a_BillToAddress=new Array();
			var a_StoredArray=new Array();
			var a_SplitArray=new Array();
			var a_SplitArrayPayeeNull=new Array();
			
				for (var i = 0; a_InvoicesearchResults != null && i < a_InvoicesearchResults.length; i++) 
				{
					var s_resultInvoice = a_InvoicesearchResults[i];
					// return all a_columns associated with this search
					var a_columnsInvoice = s_resultInvoice.getAllColumns();
					var i_columnLen2 = a_columnsInvoice.length;
					nlapiLogExecution('DEBUG', '***i_columnLen2 == ' + i_columnLen2);
					
					var s_Columns0 = a_columnsInvoice[0];
					var s_Customer_Created_Date = s_resultInvoice.getValue(s_Columns0);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + s_Customer_Created_Date);		
					
					//var Columns= a_columnsInvoice[1];
					//var Columns1 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
					
					//var Columns= a_columnsInvoice[2];
					//var Columns2 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
					
					var s_Columns3 = a_columnsInvoice[3];
					var i_Invoice_ID = s_resultInvoice.getValue(s_Columns3);
					nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + i_Invoice_ID);
					
					var o_LoadInvoiceRec = nlapiLoadRecord('invoice', i_Invoice_ID);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'o_LoadInvoiceRec['+i+']=' + o_LoadInvoiceRec);
					
					//var a_StoredArray=new Array();
					if (o_LoadInvoiceRec != null && o_LoadInvoiceRec != '') 
					{
						var s_PayerNew = o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_PayerNew['+i+']=' + s_PayerNew);
						
						if (s_PayerNew != null && s_PayerNew != '') 
						{
							//*************************************************************************************
							var i_arrayLength = a_StoredArray.length;
							if (i_arrayLength == 0) 
							{
								a_StoredArray[0] = s_PayerNew + '#' + i_Invoice_ID;
							}
							if (i_arrayLength != 0) 
							{
								var i_flag = 0;
								//var a_SplitArray=new Array();
								for (var k = 0; k < a_StoredArray.length; k++) 
								{
									a_SplitArray = a_StoredArray[k].split('#');
									if (a_SplitArray[0] == s_PayerNew) 
									{
										a_StoredArray[k] = a_StoredArray[k] + '#' + i_Invoice_ID;
										i_flag = 1;
										break;
									}
								}
								if (i_flag != 1) 
								{
									a_StoredArray[a_StoredArray.length] = s_PayerNew + '#' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)							
						}//if(s_PayerNew!=null && s_PayerNew!='')
						var s_InvoiceBillToAddress = o_LoadInvoiceRec.getFieldValue('billaddress');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_InvoiceBillToAddress= ' + s_InvoiceBillToAddress);			    	
						
						if (s_PayerNew == null && s_InvoiceBillToAddress != null) 
						{
							var i_BillToArray = a_BillToAddress.length;
							if (i_BillToArray == 0) 
							{
								a_BillToAddress[0] = s_InvoiceBillToAddress + '#' + i_Invoice_ID;
							}
							if (i_BillToArray != 0) 
							{
								var i_flag = 0;
								for (var l = 0; l < a_BillToAddress.length; l++) 
								{
									a_SplitArrayPayeeNull = a_BillToAddress[l].split('#');
									if (a_SplitArrayPayeeNull[0] == s_InvoiceBillToAddress) 
									{
										a_BillToAddress[l] = a_BillToAddress[l] + '#' + i_Invoice_ID;
										i_flag = 1;
										break;
									}
								}
								if (i_flag != 1) 
								{
									a_BillToAddress[a_BillToAddress.length] = a_BillToAddress + '#' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)
						}
						
					}
					
					var s_Columns4 = a_columnsInvoice[4];
					var s_Invoice_Date = s_resultInvoice.getValue(s_Columns4);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + s_Invoice_Date);
					
					var s_Columns5 = a_columnsInvoice[5];
					var i_BP_Invoice_Type = s_resultInvoice.getValue(s_Columns5);
					nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + i_BP_Invoice_Type);
					
			}//for i - invoice
			
			//Code for Summarize
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			if(s_BPInvoiceType==3) //3 means summary s_BPInvoiceType
			{
				//FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName)
			}
			
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************			
			//Code for Consolidated
			
            if(s_BPInvoiceType==2)
			{
				functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName)
			}
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			/*
			if(s_BPInvoiceType==1)
			{
				for(s=0;s<a_StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray['+s+']=' + a_StoredArray[s]);
			}
			for(a=0;a<a_BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
			}
			}
*/
			
		}//for
  		
  	}//if
 
 	
}//function close







//**********************************************************************************************************
                    
					//   FUNCTION FOR CONSOLIDATED

//**********************************************************************************************************




function functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName)
{
	nlapiLogExecution('DEBUG', 'functionConsolidated', 'IN CONSOLIDATED FUNCTION' );
	var a_SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();
		var a_SplitItemArray=new Array();
				
		var s_PayeeAddress='';					
		a_SplitStoredArray=a_StoredArray[s].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+s+']=' + a_SplitStoredArray[s]);
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('r_AmountRemaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
	           	//=========================================================================================================								
						if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var i_arrayLength=a_ItemArray.length;
							if(i_arrayLength==0)
							{
								a_ItemArray[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
							}
							if(i_arrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemArray.length;p++)
								{								
									a_SplitItemArray=a_ItemArray[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemArray[p]=a_ItemArray[p]+'#'+s_InvoiceItemQuantity;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemArray[a_ItemArray.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
								}
							}//if(i_arrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
								
						
						
	           //=========================================================================================================								
				
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}	
						if(s_PrimaryTherapist!=null && s_PrimaryTherapist!='' && s_PrimaryTherapist!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_PrimaryTherapist
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+s_PrimaryTherapist;
							}										
						}
										
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);
						a_InvoiceID[i_ArrCount]=a_SplitStoredArray[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=s_InvoiceItemQuantity;
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						//a_Comments[i_ArrCount]=
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//r_TotalAmount=r_TotalAmount+parseFloat(s_InvoiceItemAmount);
					}
	             
				}
					
				}						
			}	//for n	
			
		}//if
		
		//Extracting tha Item Array for Calculate the Item Quantity
		
		
        for(var t=0;t<a_ItemArray.length;t++)
		{
			var a_ItemName=new Array();
			var a_ItemValue=new Array();
			var a_ItemQuantity=new Array();
			var a_ItemTotalQuantity=new Array();
			var a_SplitItemArrayQuantity=new Array();
			var i_count=0;
			a_SplitItemArrayQuantity=a_ItemArray[t].split('#');
			
			if (a_SplitItemArrayQuantity[0] != null) 
			{
				a_ItemName[i_count]=a_SplitItemArrayQuantity[0];
				//a_ItemValue[i_count]=a_SplitItemArrayQuantity[1];
				for (var u = 2; u < a_SplitItemArrayQuantity.length; u++) 
				{
					i_count=i_count+1;
					a_ItemQuantity=a_SplitItemArrayQuantity[u];
					a_ItemTotalQuantity=a_ItemTotalQuantity+parseInt(a_ItemQuantity);					
				}							
			}
			for(var q=0;q<a_ItemName.length;q++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemName', 'a_ItemName ['+q+']==' + a_ItemName[q]);
			}
			for(var e=0;e<a_ItemTotalQuantity.length;e++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemTotalQuantity', 'a_ItemTotalQuantity ['+e+']==' + a_ItemTotalQuantity[e]);
			}
			
		}

		
		//Generate PDF
							
		var i_array=1;
		//summaryPrintPdf(s_AsOfDate,s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
		for(var w=0;w<a_ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', 'a_ItemArray ['+w+']==' + a_ItemArray[w]);
		}
	}//for
	
	var a_SplitBillToAddress=new Array();
	
	
	//Second Array a_BillToAddress
	
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();
		var a_SplitItemArray=new Array();
			
		var s_PayeeAddress='';					
		a_SplitBillToAddress=a_BillToAddress[a].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
		
		if(a_SplitBillToAddress[0]!=null)
		{
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			//if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			//{
			//	s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		    //    nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			//}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitBillToAddress Length=' + a_SplitBillToAddress.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitBillToAddress.length;n++)
			{							
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'Invoice ID====' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('r_AmountRemaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{								
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
												
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
										
						
				//=========================================================================================================								
						if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var i_arrayLength=a_ItemArray.length;
							if(i_arrayLength==0)
							{
								a_ItemArray[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
							}
							if(i_arrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemArray.length;p++)
								{								
									a_SplitItemArray=a_ItemArray[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemArray[p]=a_ItemArray[p]+'#'+s_InvoiceItemQuantity;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemArray[a_ItemArray.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
								}
							}//if(i_arrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
											
						
	           //=========================================================================================================								
				
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}
						*/
						if(s_PrimaryTherapist!=null && s_PrimaryTherapist!='' && s_PrimaryTherapist!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_PrimaryTherapist
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+s_PrimaryTherapist;
							}										
						}
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitBillToAddress[n] ==' + a_SplitBillToAddress[n];);
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);																		
						a_InvoiceID[i_ArrCount]=a_SplitBillToAddress[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=s_InvoiceItemQuantity;
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						//a_Comments[i_ArrCount]=
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						
					}//for
	             //nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + r_TotalAmount);
				}//if
					
				}//if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')						
			}//for(var n=1;n<a_SplitBillToAddress.length;n++)		
			
			
		}//if(a_SplitBillToAddress[0]!=null)
		
		for(var w=0;w<a_ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', 'a_ItemArray ['+w+']==' + a_ItemArray[w]);
		}
		//Generate PDF		
		
		//nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + r_TotalAmount);			
		//var i_array=2;
		//summaryPrintPdf(s_AsOfDate,s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
	}
}//END OF FOR
// END OF CONSOLIDATED





//**********************************************************************************************************
                    //   FUNCTION FOR SUMMARIZE

//**********************************************************************************************************

function FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName)
{
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'IN FunctionSummarize' );
	var a_SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
				
		var s_PayeeAddress='';					
		a_SplitStoredArray=a_StoredArray[s].split('#');
		
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('r_AmountRemaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate+', '+s_InvoiceDate;
						}	
						*/
						if(s_PrimaryTherapist!=null && s_PrimaryTherapist!='' && s_PrimaryTherapist!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_PrimaryTherapist
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+s_PrimaryTherapist;
							}										
						}
										
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);
						a_InvoiceID[i_ArrCount]=a_SplitStoredArray[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=s_InvoiceItemQuantity;
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						//a_Comments[i_ArrCount]=
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//r_TotalAmount=r_TotalAmount+parseFloat(s_InvoiceItemAmount);
					}
                 
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
				
		var i_array=1;
		summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
		
	}//for
	
	

	
//Second Array a_BillToAddress
    var a_SplitBillToAddress=new Array();
	var a_ItemArray=new Array();
	var a_SplitItemArray=new Array();
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		
			
		var s_PayeeAddress='';					
		a_SplitBillToAddress=a_BillToAddress[a].split('#');
		
		if(a_SplitBillToAddress[0]!=null)
		{
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
			{
				s_PayeeAddress=a_SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'getitemtype', '******Final a_SplitBillToAddress******' + a_SplitBillToAddress[0]);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitBillToAddress.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitBillToAddress['+n+']=' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('r_AmountRemaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}
						*/
						if(s_PrimaryTherapist!=null && s_PrimaryTherapist!='' && s_PrimaryTherapist!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_PrimaryTherapist
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+s_PrimaryTherapist;
							}										
						}
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_AllPrimaryTherapist ==' + s_AllPrimaryTherapist);
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);																		
						a_InvoiceID[i_ArrCount]=a_SplitBillToAddress[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=s_InvoiceItemQuantity;
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						//a_Comments[i_ArrCount]=
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'TotalAmountInSecondArray==' + r_TotalAmount);
					}
                 
				}
					
				}						
			}		
			
		}
		//Generate PDF
		
		
			
		
		//nlapiLogExecution('DEBUG', 'getitemtype', 'TotalAmountInSecondArray==' + r_TotalAmount);			
		var i_array=2;
		summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
	}
	for(a=0;a<a_ItemArray.length;a++)
	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_ItemArray['+a+']=' + a_ItemArray[a]);
	}
}  //End FOR

//    END OF FUNCTION SUMMARIZE


///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
{
	
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1
	var day = currentTime.getDate()
	var year = currentTime.getFullYear()
	var CurrantDate=month + "/" + day + "/" + year;
	

			var strVar="";
			strVar += "<table border=\"0\" width=\"100%\" cellpadding=\"0\"  height=\"59\">";
			strVar += "	<tr>";
			strVar += "		<td align=\"right\" valign=\"top\" height=\"37\" border-color=\"silver\">";
			strVar += "		<table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "			<tr><td border=\"0.1pt\" border-color=\"silver\">3-Summary Invoice<\/td><\/tr>";
			strVar += "		<\/table>";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "		<td align =\"right\" valign =\"top\" border-color=\"silver\">";
			strVar += "		    <table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "				<tr><td width =\"46%\" border=\"0.1pt\" border-color=\"silver\">As Of Date :<\/td><td  width =\"51%\" border=\"0.1pt\" border-color=\"silver\">"+CurrantDate+"<\/td><\/tr>";
			strVar += "			<\/table>";
			strVar += "	";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "	<td>";
			//strVar += "	";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "		<tr>";
			strVar += "			<td>&#160;<\/td>";
			strVar += "		<\/tr>";
			strVar += "		<\/table>";
			strVar += "		<table width=\"100%\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			
			/*strVar += "				<td width=\"190\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Bill To :<\/td>";
			strVar += "				<td height=\"27\">&#160;<\/td>";
			strVar += "				<td height=\"27\" width=\"215\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<\/td>";
			strVar += "				<td width=\"218\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan)<\/td>";
			*/
			strVar += "    <td width=\"286\" border=\"0.1pt\" border-color=\"silver\">Bill To :<br/>"+s_PayeeAddress+"<\/td>";
		    strVar += "    <td width=\"185\" >&#160;<\/td>";
		    strVar += "    <td width=\"245\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<br/>"+s_customerName+"<\/td>";
		    strVar += "    <td width=\"296\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan): "+s_AllPrimaryTherapist+"<\/td>";
			strVar += "			<\/tr>";
			/*strVar += "			<tr>";
			strVar += "				<td width=\"190\">"+s_PayeeAddress+"<\/td>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "				<td width=\"215\">"+s_customerName+"<\/td>";
			strVar += "				<td width=\"218\">&#160;<\/td>";
			strVar += "			<\/tr>";*/
			strVar += "	    <\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "			<tr>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0.1pt\" border-color=\"silver\" align =\"left\">";
			strVar += "			<tr>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Date<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Invoice #<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Service Date <\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Item<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Quantity<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Descriptions<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Comments<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Tax<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Amount<\/td>";
			strVar += "			<\/tr>";
		      // iterate through the results
			  for ( var k = 1; a_InvoiceDate != null && k < a_InvoiceDate.length; k++ )
			  {
			  	strVar += "			<tr>";
			  	strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceID[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_ServiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceItems[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvItemQuantity[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";				
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">&#160;<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemTax[k])+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_Amount[k]+"<\/td>";
			  	strVar += "			<\/tr>";
			  }
			strVar += "		<\/table>";
			strVar += "";
			strVar += "	";
			strVar += "		<table border=\"0\" >";
			strVar += "			<tr>";
			strVar += "				<td>&#160;<\/td>";
			strVar += "				";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "		";
			strVar += "		<table width=\"30%\" align=\"right\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Total<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+r_TotalAmount+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Amount Due<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+r_AllAmountDue+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";			
			strVar += "	<\/td>";
			strVar += "	<\/tr>";
			//strVar += "	";		
			strVar += "<\/table>";
			
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body font-size=\"10\">";
			xml += "<p></p>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile('FirstSummaryPDF'+i_array+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//----------------------------------
			
			

}//function 

