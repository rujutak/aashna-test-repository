/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/


// Main function
function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
    //nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	
    var context=nlapiGetContext();
    
 	//var a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
	var paramValue=context.getSetting('SCRIPT','custscript_customercounter');
	//nlapiLogExecution('DEBUG', 'TJINC_NSJ_ReportEngAutoInvoiceMonthly', 'before paramValue='+paramValue);
	var filters=new Array();
	var a_searchResult;
	//paramValue='18305';
	if(paramValue!=null)
	{
		filters[0] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', paramValue);
		a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', filters, null);
	}	
    else
	{
		a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
	}
	//nlapiLogExecution('DEBUG', 'TJINC_NSJ_ReportEngAutoInvoiceMonthly', 'after paramValue='+paramValue);
  	if(a_searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'CustomerSearch', '************** New Customer Start ************');
		//for(var k=0; a_searchResult != null && k <a_searchResult.length; k++)
		nlapiLogExecution('DEBUG', 'TJINC_NSJ_ReportEngAutoInvoiceMonthly', 'a_searchResult length='+a_searchResult.length);	
		
		//for loop for Customer
		//for(var k=paramValue; a_searchResult != null && k <a_searchResult.length; k++)
		for(var k=0; a_searchResult != null && k <a_searchResult.length; k++)
		{	
		//nlapiLogExecution('DEBUG', 'columnLen == ');		
			var s_result = a_searchResult[k];
			// return all columns associated with this search
			var a_columns = s_result.getAllColumns();
			var columnLen = a_columns.length;
			////nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var s_column0 = a_columns[0];
			var s_customerName = s_result.getValue(s_column0);			
			////nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerName['+k+']=' + s_customerName);
			
			var s_column1 = a_columns[1];
			var i_customerID = s_result.getValue(s_column1);			
		    nlapiLogExecution('DEBUG', 'CustomerSearch', 'customerID['+k+']=' + i_customerID);
			
			var o_LoadCustomerRec=nlapiLoadRecord('customer', i_customerID);
			////nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec['+k+']=' + o_LoadCustomerRec);
						
			var s_column2 = a_columns[2];
			var s_customerEmail = s_result.getValue(s_column2);			
			////nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerEmail['+k+']=' + s_customerEmail);
			
			var s_column3 = a_columns[3];
			var s_BPInvoiceType = s_result.getValue(s_column3);			
			////nlapiLogExecution('DEBUG', 'CustomerSearch', 's_BPInvoiceType['+k+']=' + s_BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var a_filtersSearch = new Array();
			a_filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', s_customerName);
			//a_filtersSearch[1] = new nlobjSearchFilter('custbody_tjinc_bbonbf_invoicetype', null, 'anyof', s_BPInvoiceType);
            // var columnsSearch = new Array();
			
			var a_InvoicesearchResults= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', a_filtersSearch, null);
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'a_InvoicesearchResults['+k+'] len=' + a_InvoicesearchResults.length);
			
			
			var a_BillToAddress=new Array();
			var a_StoredArray=new Array();
			
			////nlapiLogExecution('DEBUG', '***a_BillToAddress length == ' + a_BillToAddress.length);
			////nlapiLogExecution('DEBUG', '***a_StoredArray length == ' + a_StoredArray.length);		
              var remainUsage=context.getRemainingUsage();
              ////nlapiLogExecution('DEBUG','ReadfileFrmCabinet','============Customer start Usage k['+k+']=='+remainUsage);
			  //nlapiLogExecution('DEBUG','ReadfileFrmCabinet','***customerName k['+k+']=='+s_customerName);
				for (var i = 0; a_InvoicesearchResults != null && i < a_InvoicesearchResults.length; i++) 
				{
					var s_resultInvoice = a_InvoicesearchResults[i];
					// return all a_columns associated with this search
					var a_columnsInvoice = s_resultInvoice.getAllColumns();
					var i_columnLen2 = a_columnsInvoice.length;
					////nlapiLogExecution('DEBUG', '***i_columnLen2 == ' + i_columnLen2);
									
					var s_Columns5 = a_columnsInvoice[5];
					var i_BP_Invoice_Type = s_resultInvoice.getValue(s_Columns5);
									
					nlapiLogExecution('DEBUG', 'getitemtype', '****** BP Invoice Type=' + i_BP_Invoice_Type);
					nlapiLogExecution('DEBUG', 'getitemtype', '****** s_BPInvoiceType=' + s_BPInvoiceType);
					if(s_BPInvoiceType==i_BP_Invoice_Type)
					{						
					var s_Columns8 = a_columnsInvoice[8];
					var i_BP_Invoice_Billing_Protocol = s_resultInvoice.getValue(s_Columns8);
					nlapiLogExecution('DEBUG', 'getitemtype', '****** i_BP_Invoice_Billing_Protocol=' + i_BP_Invoice_Billing_Protocol);
					
					var BillingProtocolOBJ=nlapiLoadRecord('customrecord_tjinc_bbonbf_billingprotoco',i_BP_Invoice_Billing_Protocol);
					
					var IsInactive=BillingProtocolOBJ.getFieldValue('isinactive');
					nlapiLogExecution('DEBUG', 'getitemtype', '****** IsInactive=' + IsInactive);
					
					var ToBeProcessed=BillingProtocolOBJ.getFieldValue('custrecord_tjinc_bbonbf_tobeprocessed');
					nlapiLogExecution('DEBUG', 'getitemtype', '****** ToBeProcessed=' + ToBeProcessed);
					
					if (IsInactive == 'F' && ToBeProcessed == 'T')
					{
						var s_Columns0 = a_columnsInvoice[0];
						var s_Customer_Created_Date = s_resultInvoice.getValue(s_Columns0);
						////nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + s_Customer_Created_Date);		
						
						//var Columns= a_columnsInvoice[1];
						//var Columns1 = s_resultInvoice.getValue(Columns);			
						//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
						
						//var Columns= a_columnsInvoice[2];
						//var Columns2 = s_resultInvoice.getValue(Columns);			
						//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
						
						var s_Columns3 = a_columnsInvoice[3];
						var i_Invoice_ID = s_resultInvoice.getValue(s_Columns3);
						nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + i_Invoice_ID);
						
						var s_Columns10 = a_columnsInvoice[10];
						var i_Location_ID = s_resultInvoice.getValue(s_Columns10);
						//nlapiLogExecution('DEBUG', 'getitemtype', 'i_Location_ID=' + i_Location_ID);
						
						var o_LoadInvoiceRec = nlapiLoadRecord('invoice', i_Invoice_ID);
						nlapiLogExecution('DEBUG', 'getitemtype', 'o_LoadInvoiceRec['+i+']=' + o_LoadInvoiceRec);
						
						//var a_StoredArray=new Array();
						if (o_LoadInvoiceRec != null && o_LoadInvoiceRec != '') 
						{
							var s_PayerNew = o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
							nlapiLogExecution('DEBUG', 'getitemtype', 's_PayerNew['+i+']=' + s_PayerNew);
							var s_InvoiceBillToAddress = o_LoadInvoiceRec.getFieldValue('billaddress');
							nlapiLogExecution('DEBUG', 'getitemtype', 's_InvoiceBillToAddress= ' + s_InvoiceBillToAddress);			    	
							
							
							if (s_PayerNew != null && s_PayerNew != '') 
							{
								//*************************************************************************************
								var i_arrayLength = a_StoredArray.length;
								if (i_arrayLength == 0) 
								{
									a_StoredArray[0] = s_PayerNew + '#$' + i_Location_ID + '#$' + i_Invoice_ID;
								}
								else 
									if (i_arrayLength != 0) 
									{
										var i_flag = 0;
										//var a_SplitArray=new Array();
										for (var kk = 0;a_StoredArray!=null && kk < a_StoredArray.length; kk++) 
										{
											var a_SplitArray = new Array();
											////nlapiLogExecution('DEBUG', '***a_SplitArray length before == ' + a_SplitArray.length);
											a_SplitArray = a_StoredArray[kk].split('#$');
											////nlapiLogExecution('DEBUG', '***a_SplitArray length after == ' + a_SplitArray.length);
											if ((a_SplitArray[0] == s_PayerNew) && (a_SplitArray[1] == i_Location_ID)) {
												a_StoredArray[kk] = a_StoredArray[kk] + '#$' + i_Invoice_ID;
												i_flag = 1;
												break;
											}
										}//for
										if (i_flag != 1) 
										{
											a_StoredArray[a_StoredArray.length] = s_PayerNew + '#$' + i_Location_ID + '#$' + i_Invoice_ID;
										}//if
									}//if(i_arrayLength!=0)							
							}//if(s_PayerNew!=null && s_PayerNew!='')						
							else 
								if (s_PayerNew == null && s_InvoiceBillToAddress != null) 
								{
									nlapiLogExecution('DEBUG', '*** In Payer==null ');
									var i_BillToArray = a_BillToAddress.length;
									if (i_BillToArray == 0) 
									{
										nlapiLogExecution('DEBUG', '***In Bill to address==0 ');
										a_BillToAddress[0] = s_InvoiceBillToAddress + '#$' + i_Location_ID + '#$' + i_Invoice_ID;
									}//if
									else 
										if (i_BillToArray != 0) 
										{
											var i_flag1 = 0;
											for (var l = 0;a_BillToAddress!=null && l < a_BillToAddress.length; l++) 
											{
												var a_SplitArrayPayeeNull = new Array();
												nlapiLogExecution('DEBUG', '***a_SplitArrayPayeeNull length before == ' + a_SplitArrayPayeeNull.length);
												a_SplitArrayPayeeNull = a_BillToAddress[l].split('#$');
												////nlapiLogExecution('DEBUG', '***a_SplitArrayPayeeNull length after == ' + a_SplitArrayPayeeNull.length);
												if ((a_SplitArrayPayeeNull[0] == s_InvoiceBillToAddress) && (a_SplitArrayPayeeNull[1] == i_Location_ID)) 
												{
													a_BillToAddress[l] = a_BillToAddress[l] + '#$' + i_Invoice_ID;
													i_flag1 = 1;
													break;
												}
											}//for Bill To Address
											if (i_flag1 != 1) 
											{
												a_BillToAddress[a_BillToAddress.length] = s_InvoiceBillToAddress + '#$' + i_Location_ID + '#$' + i_Invoice_ID;
											}//if
										}//if(i_arrayLength!=0)
								}// else if PayerNew == null
						}//if (o_LoadInvoiceRec != null && o_LoadInvoiceRec != '') 
						var s_Columns4 = a_columnsInvoice[4];
						var s_Invoice_Date = s_resultInvoice.getValue(s_Columns4);
					////nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + s_Invoice_Date);
					/*
	 var s_Columns5 = a_columnsInvoice[5];
	 var i_BP_Invoice_Type = s_resultInvoice.getValue(s_Columns5);
	 ////nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + i_BP_Invoice_Type);
	 */
					}	
					}//if s_BPInvoiceType
					
			}//for i - invoice
			
			//Code for Summarize
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			if(s_BPInvoiceType==3) //3 means summary s_BPInvoiceType
			{
				nlapiLogExecution('DEBUG','ReadfileFrmCabinet','****** Call fun Summary Invoice type='+s_BPInvoiceType);
				FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
			}
			
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************			
			//Code for Consolidated
			
            if(s_BPInvoiceType==2)
			{
				nlapiLogExecution('DEBUG','ReadfileFrmCabinet','***%%%%%% Call fun Consolidated Invoice type='+s_BPInvoiceType);
				functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
			}
			 
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			var remainUsage=context.getRemainingUsage();
            //nlapiLogExecution('DEBUG','ReadfileFrmCabinet','####### Customer end Usage =='+remainUsage);
			
		  // if(a_searchResult.length!=k+1)
		   {
		   	if(context.getRemainingUsage() <2000 )
			{
			 //nlapiLogExecution('DEBUG','Schedulevouchergeneration','URescheduling the script as limit exceded='+context.getExecutionContext());
			 if(context.getExecutionContext() == "scheduled")
			 {
			   Schedulescriptafterusageexceeded(i_customerID);
			   break;
			 }//if
			}//if
		   	
		   }//if a_searchResult
						
					
		}//for k
  		
  	}//if Customer SearchResult not null
 
 	
}//function close







//**********************************************************************************************************
                    
					//   FUNCTION FOR CONSOLIDATED

//**********************************************************************************************************




function functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
{
	//nlapiLogExecution('DEBUG', 'functionConsolidated', 'IN CONSOLIDATED FUNCTION' );
	
	//nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray Length=' + a_StoredArray.length);
    
	for(s=0;a_StoredArray!=null && s<a_StoredArray.length;s++)
	{
		var a_DataArray=new Array();		
		var i_ArrCount=0;
		var r_TotalAmount=0;			
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;			
	    i_cnt=0;
		var ItemQtyArray=new Array();
		var flagItem=0;
		var i_cntth=0;
		var a_supplementaryInvArray=new Array();
		var i_supplementaryCnt=0;
		var s_PayeeAddress='';
		var S_PayeeAddress_Email='';	
		var a_SplitStoredArray=new Array();	
		var a_TotalTax=new Array();
		var i_taxcount=0;			
		a_SplitStoredArray=a_StoredArray[s].split('#$');
		////nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+s+']=' + a_SplitStoredArray[s]);
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
	        ////nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        ////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			    var s_PayeeEmailID=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeremail');
		        ////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
		        S_PayeeAddress_Email=s_PayeeAddress+'##$$'+s_PayeeEmailID;
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'S_PayeeAddress_Email=' + S_PayeeAddress_Email);
			}
			////nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=2;a_SplitStoredArray!=null && n<a_SplitStoredArray.length;n++)
			{
				nlapiLogExecution('DEBUG', 'functionConsolidated', '*****INV ID== ['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
	            ////nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
				a_supplementaryInvArray[i_supplementaryCnt++]=a_SplitStoredArray[n];
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    	if(s_InvoiceDate==null || s_InvoiceDate=='NaN')
				{
					s_InvoiceDate='';
				}			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_servicedate');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	if(s_ServiceDate==null || s_ServiceDate=='NaN')
				{
					s_ServiceDate='';
				}	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_taxtotal = o_LoadInvoiceRec.getFieldValue('taxtotal');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_taxtotal ==' + s_taxtotal);
				
				if(s_taxtotal==null || s_taxtotal=='NaN')
				{
					s_taxtotal=0;
				}
				a_TotalTax[i_taxcount++]=parseFloat(s_taxtotal);
				
				/*
                var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
                */
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						//var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesContribution = o_LoadInvoiceRec.getLineItemValue('salesteam', 'contribution', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesContribution==' + s_InvoiceSalesContribution);
						
						var a_Contribution=new Array();
						if(s_InvoiceSalesContribution!=null)
						{
							a_Contribution=s_InvoiceSalesContribution.split('%');
						}
						
						nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_Contribution[0]== ' + a_Contribution[0]);
												
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceSalesRoleValue==-2 && a_Contribution[0]>0 && s_InvoiceSalesRoleValue!=null) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								//nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									var flagTh=0;
									if(a_AllPrimaryTherapist.length==0 && s_TherapistID!='')
									{
										a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
										
									}
									else if(a_AllPrimaryTherapist.length!=0 && s_TherapistID!='')
									{
										for(var g=0;a_AllPrimaryTherapist!=null && g<a_AllPrimaryTherapist.length;g++)
										{								
											if(a_AllPrimaryTherapist[g]==s_TherapistID)
											{													
												//a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
												flagTh=1;
												break;
											}//if								
												
										}// for
										if (flagTh != 1) 
										{
											a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
										}							
									} //else if		
								}
							}
						}
						
					}					
				//}
				
				
				//var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				if (r_AmountRemaining != null && r_AmountRemaining != '') 
				{
					r_AllAmountDue = parseFloat(r_AllAmountDue) + parseFloat(r_AmountRemaining);
				}
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{					
					
					//Invoice Line Count
	                for (var j = 1;i_LineItemCount!=null && j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						nlapiLogExecution('DEBUG', 'functionConsolidated', '%%%%%%%%%%%%%s_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						if(s_InvoiceItemText==null || s_InvoiceItemText=='NaN' || s_InvoiceItemText=='undefined')
						{
							s_InvoiceItemText='';
						}
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						if(s_InvoiceItemValue==null || s_InvoiceItemValue=='NaN' || s_InvoiceItemValue=='undefined')
						{
							s_InvoiceItemValue='';
						}
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);						
						var r_SplitQty=0;
						if(s_InvoiceItemQuantity!=null && s_InvoiceItemQuantity!='' && s_InvoiceItemQuantity!='undefined')
						{						   
						    r_SplitQty=s_InvoiceItemQuantity;
						}
						else
						{
							r_SplitQty=0;
						}	
						
						// Unit Price of Item
						var s_InvoiceItemUnitPrice = o_LoadInvoiceRec.getLineItemValue('item', 'rate', j);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemUnitPrice ['+j+']==' + s_InvoiceItemUnitPrice);
						var r_ItemUP=0;
						if(s_InvoiceItemUnitPrice!=null && s_InvoiceItemUnitPrice!='' && s_InvoiceItemUnitPrice!='undefined')
						{
							r_ItemUP=s_InvoiceItemUnitPrice;
						}
						else
						{
							r_ItemUP=0;
						}		
						
												
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						var r_ItemAmt=0;
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='' && s_InvoiceItemAmount!='undefined')
						{
							r_ItemAmt=s_InvoiceItemAmount;
						}
						else
						{
							r_ItemAmt=0;
						}						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						var r_SplitTax=new Array();
						if(s_InvoiceITax!=null && s_InvoiceITax!='' && s_InvoiceITax!='undefined')
						{						   
						    r_SplitTax=s_InvoiceITax.split('%');
							//r_SplitTax=s_InvoiceITax
						}
						else
						{
							r_SplitTax[0]=0;
						}
						
						var s_ItemTaxAmount=parseFloat((parseFloat(s_InvoiceItemAmount)/100)*parseFloat(s_InvoiceITax));
						nlapiLogExecution('DEBUG', 'inFunctionDetailed', 's_ItemTaxAmount ['+j+']==' + s_ItemTaxAmount);
						
						if(s_ItemTaxAmount==null || s_ItemTaxAmount=='NaN')	
						{
							s_ItemTaxAmount=0;
						}
				//=========================================================================================================								
						if(ItemQtyArray.length==0)
						{
							ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+r_ItemAmt+'#$'+a_SplitStoredArray[n];
						}
						else if(ItemQtyArray.length!=0)
						{
							for(var d=0;ItemQtyArray!=null && d<ItemQtyArray.length;d++)
							{
								var i_flagcheck=0;
								var a_SplitItemArray=new Array();
								a_SplitItemArray=ItemQtyArray[d].split('#$');
								if(a_SplitItemArray[0]==s_InvoiceItemValue)
								{
									var r_CalQty=0;
									var r_CalTax=0;
									var r_CalAmt=0;
									r_CalQty=parseFloat(a_SplitItemArray[2])+parseFloat(r_SplitQty);
									r_CalTax=parseFloat(a_SplitItemArray[5])+parseFloat(s_ItemTaxAmount);
									r_CalAmt=parseFloat(a_SplitItemArray[6])+parseFloat(r_ItemAmt);
									
									ItemQtyArray[d]=a_SplitItemArray[0]+'#$'+s_InvoiceItemText+'#$'+r_CalQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+r_CalTax+'#$'+r_CalAmt+'#$'+a_SplitStoredArray[n];
								    i_flagcheck=1;	
									break;								
								}//if
								
							}// for	ItemQtyArray
							if(i_flagcheck!=1)
							{
								ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+r_ItemAmt+'#$'+a_SplitStoredArray[n];
							}//if						
						} //else if(ItemQtyArray.length!=0)
	           //=========================================================================================================								
				
								
						
					}//for LineItemCount
	            
				}// if LineItemCount
				
				
				
				}	//if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')					
			}//for a_SplitStoredArray	
			
		}//if(a_SplitStoredArray[0]!=null)
		
		//Extracting tha Item Array for Calculate the Item Quantity
				
		//Generate PDF
			
        var flagPayeeAddr=1;	
		ConsolidatePrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,ItemQtyArray,r_TotalAmount,r_AllAmountDue,a_SplitStoredArray[1],s_customerEmail,a_supplementaryInvArray,flagPayeeAddr,a_TotalTax)
		
	}//for StoredArray
	
	
	
	
	//Second Array a_BillToAddress
	
	for(a=0;a_BillToAddress!=null && a<a_BillToAddress.length;a++)
	{
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;		
		var i_cntth=0;
		
		var a_supplementaryInvArray1=new Array();
		var i_supplementaryCnt1=0;
		
	    var i_cnt=0;
		var ItemQtyArray=new Array();
		var flagItem=0;
		var a_TotalTax=new Array();
		var i_taxcount=0;	
		var s_PayeeAddress='';	
		var S_PayeeAddress_Email='';
		var a_SplitBillToAddress=new Array();				
		a_SplitBillToAddress=a_BillToAddress[a].split('#$');
		////nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
		
		if(a_SplitBillToAddress[0]!=null)
		{
			
			
			//Get The Data from Invoice
			for(var n=2;a_SplitBillToAddress!=null && n<a_SplitBillToAddress.length;n++)
			{				   						
				nlapiLogExecution('DEBUG', 'functionConsolidated', '****INV ID====' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
	            ////nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
				a_supplementaryInvArray1[i_supplementaryCnt1++]=a_SplitBillToAddress[n];
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
					
				if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='' && a_SplitBillToAddress[0]!='undefined')
				{
					s_PayeeAddress=a_SplitBillToAddress[0];
			        ////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);	
					var s_EmailAddress=o_LoadInvoiceRec.getFieldValue('custbody_client_email');
			        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_EmailAddress== ' + s_EmailAddress);	
					S_PayeeAddress_Email=s_PayeeAddress+'##$$'+s_EmailAddress;		    	
				}
				
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    	if(s_InvoiceDate==null || s_InvoiceDate=='NaN' || s_InvoiceDate=='undefined')
				{
					s_InvoiceDate='';
				}			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_servicedate');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	if(s_ServiceDate==null || s_ServiceDate=='NaN' || s_ServiceDate=='undefined')
				{
					s_ServiceDate='';
				}	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_taxtotal = o_LoadInvoiceRec.getFieldValue('taxtotal');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_taxtotal ==' + s_taxtotal);
				
				if(s_taxtotal==null || s_taxtotal=='NaN' || s_taxtotal=='undefined')
				{
					s_taxtotal=0;
				}
				a_TotalTax[i_taxcount++]=parseFloat(s_taxtotal);
				
				/*
                var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
                */
				var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
				
				for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
				{
					//var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
					
					var s_InvoiceSalesContribution = o_LoadInvoiceRec.getLineItemValue('salesteam', 'contribution', f);
					nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesContribution==' + s_InvoiceSalesContribution);
						
					var a_Contribution=new Array();
					if(s_InvoiceSalesContribution!=null)
					{
						a_Contribution=s_InvoiceSalesContribution.split('%');
					}
					
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_Contribution[0]== ' + a_Contribution[0]);
											
					var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
											
					if (s_InvoiceSalesRoleValue==-2 && a_Contribution[0]>0 && s_InvoiceSalesRoleValue!=null) 
					{
						var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
						
						if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
						{
							var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
							//nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
							
							if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
							{
								s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
								//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
								var flagTh=0;
								if(a_AllPrimaryTherapist.length==0 && s_TherapistID!='')
								{
									a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									
								}
								else if(a_AllPrimaryTherapist.length!=0 && s_TherapistID!='')
								{
									for(var g=0;a_AllPrimaryTherapist!=null && g<a_AllPrimaryTherapist.length;g++)
									{								
										if(a_AllPrimaryTherapist[g]==s_TherapistID)
										{													
											//a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
											flagTh=1;
											break;
										}//if								
											
									}// for
									if (flagTh != 1) 
									{
										a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									}							
								} //else if		
							}
						}
					}
					
				}					
				//}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				////nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				if (r_AmountRemaining != null && r_AmountRemaining != '' && r_AmountRemaining != 'undefined') 
				{
					r_AllAmountDue = parseFloat(r_AllAmountDue) + parseFloat(r_AmountRemaining);
				}
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{								
					//Invoice Line Count
	                for (var j = 1;i_LineItemCount!=null && j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						nlapiLogExecution('DEBUG', 'functionConsolidated', '%%%%%%%%%%%%%s_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						if(s_InvoiceItemText==null || s_InvoiceItemText=='NaN' || s_InvoiceItemText=='undefined')
						{
							s_InvoiceItemText='';
						}
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						if(s_InvoiceItemValue==null || s_InvoiceItemValue=='NaN' || s_InvoiceItemValue=='undefined')
						{
							s_InvoiceItemValue='';
						}
												
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);						
						var r_SplitQty=0;
						if(s_InvoiceItemQuantity!=null && s_InvoiceItemQuantity!='' && s_InvoiceItemQuantity!='undefined')
						{						   
						    r_SplitQty=s_InvoiceItemQuantity;
						}						
						
						// Unit Price of Item
						var s_InvoiceItemUnitPrice = o_LoadInvoiceRec.getLineItemValue('item', 'rate', j);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemUnitPrice ['+j+']==' + s_InvoiceItemUnitPrice);
						var r_ItemUP=0;
						if(s_InvoiceItemUnitPrice!=null && s_InvoiceItemUnitPrice!='' && s_InvoiceItemUnitPrice!='undefined')
						{
							r_ItemUP=s_InvoiceItemUnitPrice;
						}
						else
						{
							r_ItemUP=0;
						}		
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						var r_ItemAmt=0;
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='' && s_InvoiceItemAmount!='undefined')
						{
							r_ItemAmt=s_InvoiceItemAmount;
						}	
											
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						var r_SplitTax=new Array();
						if(s_InvoiceITax!=null && s_InvoiceITax!='' && s_InvoiceITax!='undefined')
						{						   
						    r_SplitTax=s_InvoiceITax.split('%');
							//r_SplitTax=s_InvoiceITax;
						}
						else
						{
							r_SplitTax[0]=0;
						}
						
						var s_ItemTaxAmount=parseFloat((parseFloat(s_InvoiceItemAmount)/100)*parseFloat(s_InvoiceITax));
						nlapiLogExecution('DEBUG', 'inFunctionDetailed', 's_ItemTaxAmount ['+j+']==' + s_ItemTaxAmount);
						
						if(s_ItemTaxAmount==null || s_ItemTaxAmount=='NaN')	
						{
							s_ItemTaxAmount=0;
						}
						
				//=========================================================================================================								
						if(ItemQtyArray.length==0)
						{
							ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+r_ItemAmt+'#$'+a_SplitBillToAddress[n];
						}
						else if(ItemQtyArray.length!=0)
						{
							for(var d=0;ItemQtyArray!=null && d<ItemQtyArray.length;d++)
							{
								var i_flagcheck=0;
								var a_SplitItemArray=new Array();
								a_SplitItemArray=ItemQtyArray[d].split('#$');
								if(a_SplitItemArray[0]==s_InvoiceItemValue)
								{
									var r_CalQty=0;
									var r_CalTax=0;
									var r_CalAmt=0;
									
									r_CalQty=parseFloat(a_SplitItemArray[2])+parseFloat(r_SplitQty);
									r_CalTax=parseFloat(a_SplitItemArray[5])+parseFloat(s_ItemTaxAmount);
									r_CalAmt=parseFloat(a_SplitItemArray[6])+parseFloat(r_ItemAmt);
									
									ItemQtyArray[d]=a_SplitItemArray[0]+'#$'+s_InvoiceItemText+'#$'+r_CalQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+r_CalTax+'#$'+r_CalAmt+'#$'+a_SplitBillToAddress[n];
								    i_flagcheck=1;
									break;									
								}//if
								
							}// for	
							if(i_flagcheck!=1)
							{
								ItemQtyArray[i_cnt++]=s_InvoiceItemValue+'#$'+s_InvoiceItemText+'#$'+r_SplitQty+'#$'+r_SplitTax[0]+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+r_ItemAmt+'#$'+a_SplitBillToAddress[n];
							}//if						
						} //else if
	           //=========================================================================================================								
													
						
						
					}//for
					
	            
				}//LineItemCount
					
				}//if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')						
			}//for(var n=1;n<a_SplitBillToAddress.length;n++)		
			
			
		}//if(a_SplitBillToAddress[0]!=null)
		
	    // loop for count each Item quantity
		var flagBTAddr=2;
		//Generate PDF		
		ConsolidatePrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,ItemQtyArray,r_TotalAmount,r_AllAmountDue,a_SplitBillToAddress[1],s_customerEmail,a_supplementaryInvArray1,flagBTAddr,a_TotalTax);	
	}// End of for BillToAddress
}//END OF FUNCTION FOR CONSOLIDATED
// END OF CONSOLIDATED

// function for seperate array elements





//**********************************************************************************************************
                    //   FUNCTION FOR SUMMARIZE

//**********************************************************************************************************




function FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID,s_customerEmail)
{
	//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'IN FunctionSummarize' );
	
	//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_StoredArray Length=' + a_StoredArray.length);
	
	//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_BillToAddress Length=' + a_BillToAddress.length);			
	for(s=0;s<a_StoredArray.length;s++)
	{
		var DataArray=new Array();			
		var i_ArrCount=0;
		var r_TotalAmount=0;			
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var i_cntth=0;		
		var s_PayeeAddress='';		
		var S_PayeeAddress_Email='';
		var a_SplitStoredArray=new Array();		
		var a_TotalTax=new Array();	
		var i_taxcount=0;
		a_SplitStoredArray=a_StoredArray[s].split('#$');
		
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+s+']=' + o_LoadPayerRec);
            
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        ////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress=' + s_PayeeAddress);
				var s_PayeeEmailID=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeremail');
		        ////nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
		        S_PayeeAddress_Email=s_PayeeAddress+'##$$'+s_PayeeEmailID;
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'S_PayeeAddress_Email=' + S_PayeeAddress_Email);			    	
			}
			////nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=2;a_SplitStoredArray!=null && n<a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'Invoice ID['+n+']=' + a_SplitStoredArray[n]);
								
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
                ////nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    	
				if(s_InvoiceDate==null || s_InvoiceDate=='NaN')
				{
					s_InvoiceDate='';
				}		
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_servicedate');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	if(s_ServiceDate==null || s_ServiceDate=='NaN')
				{
					s_ServiceDate='';
				}	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				//var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_InvoiceTranid = o_LoadInvoiceRec.getFieldValue('tranid');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceTranid ==' + s_InvoiceTranid);
				
				var s_taxtotal = o_LoadInvoiceRec.getFieldValue('taxtotal');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_taxtotal ==' + s_taxtotal);
				
				if(s_taxtotal==null || s_taxtotal=='NaN')
				{
					s_taxtotal=0;
				}
				a_TotalTax[i_taxcount++]=parseFloat(s_taxtotal);
				//var s_TherapistID='';
				//if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				//{
				var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
				
				for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
				{
					//var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
					
					var s_InvoiceSalesContribution = o_LoadInvoiceRec.getLineItemValue('salesteam', 'contribution', f);
					nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesContribution==' + s_InvoiceSalesContribution);
					
					var a_Contribution=new Array();
					if(s_InvoiceSalesContribution!=null)
					{
						a_Contribution=s_InvoiceSalesContribution.split('%');
					}
					
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_Contribution[0]== ' + a_Contribution[0]);
											
					var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
											
					if (s_InvoiceSalesRoleValue==-2 && a_Contribution[0]>0 && s_InvoiceSalesRoleValue!=null) 
					{
						var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
						
						if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
						{
							var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
							//nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
							
							if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
							{
								s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
								//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
								var flagTh=0;
								if(a_AllPrimaryTherapist.length==0 && s_TherapistID!='')
								{
									a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									
								}
								else if(a_AllPrimaryTherapist.length!=0 && s_TherapistID!='')
								{
									for(var g=0;a_AllPrimaryTherapist!=null && g<a_AllPrimaryTherapist.length;g++)
									{								
										if(a_AllPrimaryTherapist[g]==s_TherapistID)
										{													
											//a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
											flagTh=1;
											break;
										}//if								
											
									}// for
									if (flagTh != 1) 
									{
										a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									}							
								} //else if		
							}
						}
					}
					
				}					
				//}
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				////nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				if(r_AmountRemaining!=null && r_AmountRemaining!='')
				{
					r_AllAmountDue=parseFloat(r_AllAmountDue) + parseFloat(r_AmountRemaining);
				}
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1;i_LineItemCount!=null && j <= i_LineItemCount; j++) 
					{
			            
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						if(s_InvoiceItemText==null || s_InvoiceItemText=='NaN' || s_InvoiceItemText=='undefined')
						{
							s_InvoiceItemText='';
						}
						
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						if(s_InvoiceItemQuantity==null || s_InvoiceItemQuantity =='' || s_InvoiceItemQuantity =='NaN' || s_InvoiceItemQuantity=='undefined')
						{
							s_InvoiceItemQuantity=0;
						}
						
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						if(s_InvoiceItemDescription==null || s_InvoiceItemDescription=='NaN')
						{
							s_InvoiceItemDescription='';
						}
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
						if(s_InvoiceItemComments==null || s_InvoiceItemComments=='NaN')
						{
							s_InvoiceItemComments='';
						}	
						
						
						// Unit Price of Item
						var s_InvoiceItemUnitPrice = o_LoadInvoiceRec.getLineItemValue('item', 'rate', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemUnitPrice ['+j+']==' + s_InvoiceItemUnitPrice);
						var r_ItemUP=0;
						if(s_InvoiceItemUnitPrice!=null && s_InvoiceItemUnitPrice!='')
						{
							r_ItemUP=s_InvoiceItemUnitPrice;
						}
						else
						{
							r_ItemUP=0;
						}		
											
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						if(s_InvoiceItemAmount==null || s_InvoiceItemAmount=='NaN')
						{
							s_InvoiceItemAmount=0;
						}
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						////nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						if(s_InvoiceITax==null || s_InvoiceITax=='NaN')	
						{
							s_InvoiceITax=0;
						}
						
						var s_ItemTaxAmount=parseFloat((parseFloat(s_InvoiceItemAmount)/100)*parseFloat(s_InvoiceITax));
						nlapiLogExecution('DEBUG', 'inFunctionDetailed', 's_ItemTaxAmount ['+j+']==' + s_ItemTaxAmount);
						
						if(s_ItemTaxAmount==null || s_ItemTaxAmount=='NaN')	
						{
							s_ItemTaxAmount=0;
						}
					//*********************Data Array****************************													
						DataArray[i_ArrCount++]=s_InvoiceDate+'#$'+s_InvoiceTranid+'#$'+s_ServiceDate+'#$'+s_InvoiceItemText+'#$'+parseFloat(s_InvoiceItemQuantity)+'#$'+s_InvoiceItemDescription+'#$'+s_InvoiceItemComments+'#$'+s_InvoiceITax+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+s_InvoiceItemAmount+'#$'+a_SplitStoredArray[n];
						
														
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						
					}				 
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
		////nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);	
		var flagPayee=1;	
		nlapiLogExecution('DEBUG', 'getitemtype', '=====Stored Array call fun summaryPrintPdf called times='+s);
		summaryPrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,DataArray,r_TotalAmount,r_AllAmountDue,a_SplitStoredArray[1],s_customerEmail,flagPayee,a_TotalTax)
		//for(var tt=0;tt<a_AllPrimaryTherapist.length;tt++)
		//{
		//	nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist['+tt+']==' + a_AllPrimaryTherapist[tt]);
		//}
		
	}//for s
	
	

	
//Second Array a_BillToAddress
    
	
	for(var a=0;a_BillToAddress!=null && a<a_BillToAddress.length;a++)
	{
		var InvoiceDataArray=new Array();		
		
		var i_ArrCount=0;
		var r_TotalAmount=0;			
		var a_AllPrimaryTherapist=new Array();
		var r_AllAmountDue=0;
		var i_cntth=0;				
		var s_PayeeAddress='';
		var S_PayeeAddress_Email='';	
		var a_SplitBillToAddress=new Array();	
		var a_TotalTax=new Array();
		var i_taxcount=0;			
		a_SplitBillToAddress=a_BillToAddress[a].split('#$');
		
		if(a_SplitBillToAddress[0]!=null)
		{
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			
			//nlapiLogExecution('DEBUG', 'getitemtype', '******Final a_SplitBillToAddress******' + a_SplitBillToAddress[0]);
			//Get The Data from Invoice
			for(var n=2;a_SplitBillToAddress!=null && n<a_SplitBillToAddress.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitBillToAddress['+n+']=' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
					
				if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
				{
					s_PayeeAddress=a_SplitBillToAddress[0];
			        //nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);
					
					var s_EmailAddress=o_LoadInvoiceRec.getFieldValue('custbody_client_email');
			        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_EmailAddress== ' + s_EmailAddress);	
					S_PayeeAddress_Email=s_PayeeAddress+'##$$'+s_EmailAddress;
				}
				
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    	if(s_InvoiceDate==null || s_InvoiceDate=='NaN')
				{
					s_InvoiceDate='';
				}			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_servicedate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	if(s_ServiceDate==null || s_ServiceDate=='NaN')
				{
					s_ServiceDate='';
				}	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				//var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_InvoiceTranid = o_LoadInvoiceRec.getFieldValue('tranid');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceTranid ==' + s_InvoiceTranid);
				
				var s_taxtotal = o_LoadInvoiceRec.getFieldValue('taxtotal');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_taxtotal ==' + s_taxtotal);
				
				if(s_taxtotal==null || s_taxtotal=='NaN')
				{
					s_taxtotal=0;
				}
				a_TotalTax[i_taxcount++]=parseFloat(s_taxtotal);
				
				//var s_TherapistID='';
				//if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				//{
				var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
				
				for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
				{
					//var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
					
					var s_InvoiceSalesContribution = o_LoadInvoiceRec.getLineItemValue('salesteam', 'contribution', f);
					nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesContribution==' + s_InvoiceSalesContribution);
					
					var a_Contribution=new Array();
					if(s_InvoiceSalesContribution!=null)
					{
						a_Contribution=s_InvoiceSalesContribution.split('%');
					}
					
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_Contribution[0]== ' + a_Contribution[0]);
					
					var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
					//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
					
					if (s_InvoiceSalesRoleValue==-2 && a_Contribution[0]>0 && s_InvoiceSalesRoleValue!=null) 
					{
						var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
					    
						if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
						{
							var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
							//nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
							
							if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
							{
								s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
								//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
								var flagTh=0;
								if(a_AllPrimaryTherapist.length==0 && s_TherapistID!='')
								{
									a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									
								}
								else if(a_AllPrimaryTherapist.length!=0 && s_TherapistID!='')
								{
									for(var g=0;a_AllPrimaryTherapist!=null && g<a_AllPrimaryTherapist.length;g++)
									{								
										if(a_AllPrimaryTherapist[g]==s_TherapistID)
										{													
											//a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
											flagTh=1;
											break;
										}//if								
											
									}// for
									if (flagTh != 1) 
									{
										a_AllPrimaryTherapist[i_cntth++]=s_TherapistID;
									}							
								} //else if		
							}
						}
					}
					
				}					
				//}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				if(r_AmountRemaining!=null && r_AmountRemaining!='')
				{
					r_AllAmountDue=parseFloat(r_AllAmountDue) + parseFloat(r_AmountRemaining);
				}
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1;i_LineItemCount!=null && j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						if(s_InvoiceItemText==null || s_InvoiceItemText=='NaN' || s_InvoiceItemText=='undefined')
						{
							s_InvoiceItemText='';
						}
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						if(s_InvoiceItemQuantity==null || s_InvoiceItemQuantity =='' || s_InvoiceItemQuantity =='NaN')
						{
							s_InvoiceItemQuantity=0;
						}
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
						if(s_InvoiceItemComments==null || s_InvoiceItemComments=='NaN')
						{
							s_InvoiceItemComments='';
						}		
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						if(s_InvoiceItemDescription==null || s_InvoiceItemDescription=='NaN')
						{
							s_InvoiceItemDescription='';
						}
						
						// Unit Price of Item
						var s_InvoiceItemUnitPrice = o_LoadInvoiceRec.getLineItemValue('item', 'rate', j);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemUnitPrice ['+j+']==' + s_InvoiceItemUnitPrice);
						var r_ItemUP=0;
						if(s_InvoiceItemUnitPrice!=null && s_InvoiceItemUnitPrice!='')
						{
							r_ItemUP=s_InvoiceItemUnitPrice;
						}
						else
						{
							r_ItemUP=0;
						}		
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						if(s_InvoiceItemAmount==null || s_InvoiceItemAmount=='NaN')
						{
							s_InvoiceItemAmount=0;
						}
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						if(s_InvoiceITax==null || s_InvoiceITax=='NaN')
						{
							s_InvoiceITax=0;
						}
						
						var s_ItemTaxAmount=parseFloat((parseFloat(s_InvoiceItemAmount)/100)*parseFloat(s_InvoiceITax));
						nlapiLogExecution('DEBUG', 'inFunctionDetailed', 's_ItemTaxAmount ['+j+']==' + s_ItemTaxAmount);
						
						if(s_ItemTaxAmount==null || s_ItemTaxAmount=='NaN')	
						{
							s_ItemTaxAmount=0;
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_AllPrimaryTherapist ==' + a_AllPrimaryTherapist);
							
		//**********************************************************************************
						InvoiceDataArray[i_ArrCount++]=s_InvoiceDate+'#$'+s_InvoiceTranid+'#$'+s_ServiceDate+'#$'+s_InvoiceItemText+'#$'+parseFloat(s_InvoiceItemQuantity)+'#$'+s_InvoiceItemDescription+'#$'+s_InvoiceItemComments+'#$'+s_InvoiceITax+'#$'+r_ItemUP+'#$'+s_ItemTaxAmount+'#$'+s_InvoiceItemAmount+'#$'+a_SplitBillToAddress[n];
												
		//**********************************************************************************				
						
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'TotalAmountInSecondArray==' + r_TotalAmount);
					}               
				}
					
				}// if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')						
			}// for SplitBillToAddress		
			
		} //if(a_SplitBillToAddress[0]!=null)
		//Generate PDF		
		
		//nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);	
		var flagBTAddr=2;		
		nlapiLogExecution('DEBUG', 'getitemtype', '=====Bill to Address Array call fun summaryPrintPdf called times='+a);
		summaryPrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,InvoiceDataArray,r_TotalAmount,r_AllAmountDue,a_SplitBillToAddress[1],s_customerEmail,flagBTAddr,a_TotalTax)
	    //for(var t1=0;t1<a_AllPrimaryTherapist.length;t1++)
		//{
		//	nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist['+t1+']==' + a_AllPrimaryTherapist[t1]);
		//}
	}//for BillToAddress
	
}  //End FOR

//    END OF FUNCTION SUMMARIZE





///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,InvoiceDataArray,r_TotalAmount,r_AllAmountDue,LocationId,s_customerEmail,flagPayeeBTAddr,a_TotalTax)
{
	 var context=nlapiGetContext();
	 var remainUsage=context.getRemainingUsage();
	 nlapiLogExecution('DEBUG','ReadfileFrmCabinet','************ summaryPrintPdf start Usage k=='+remainUsage);
	

	nlapiLogExecution('DEBUG', 'PDF', 'i m in summaryPrintPdf');
	//var s_PayeeAddress=s_customerName+"<br\/>"+s_PayeeAddress;
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	var locationAddress='';
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='SI_'+i_customerID+'_'+timeStamp;
	}
	
	nlapiLogExecution('DEBUG', 'PDF', '##### LocationId='+LocationId);
	if(LocationId!=null && LocationId!='')
	{
		var locationObj=nlapiLoadRecord('location',LocationId);
		locationAddress=locationObj.getFieldValue('addrtext');
		//***********************************************************				
		 locationAddress = locationAddress.toString();
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  locationAddress.toString()= " + locationAddress);
		 
		 locationAddress = locationAddress.replace(/\n/g,"<br\/>");/// /g
		 //locationAddress = locationAddress.replace(/\r/g,"<br\/>");/// /g
		
		 
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  locationAddress After Replcing spaces with % =====" + locationAddress);
		
		//*****
	}//if
	
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var s_TherapistList='';
	nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist length= ' + a_AllPrimaryTherapist.length);
	
    for(var f=0;a_AllPrimaryTherapist!=null && f<a_AllPrimaryTherapist.length;f++)
	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_AllPrimaryTherapist['+f+']=' + a_AllPrimaryTherapist[f]);
	}

	
    for(var c=0;a_AllPrimaryTherapist!=null && c<a_AllPrimaryTherapist.length;c++)
	{
		if(c==0)
		{
			s_TherapistList=a_AllPrimaryTherapist[c];
		}
		else
		{
			s_TherapistList=s_TherapistList+', '+a_AllPrimaryTherapist[c];
		}
		
	}
	

	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\" align =\"right\"><h2>Summary Invoice<\/h2><table width=\"140\" align =\"right\"><tr><td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">As Of Date<\/td><\/tr><tr><td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td><\/tr><\/table><\/td>";
strVar += "			<\/tr>";
strVar += "			<tr>";
//strVar += "   <td width=\"248\">&nbsp;<\/td>"
strVar += "			<td>&nbsp;<\/td>";
//strVar += "				<td width=\"206\">As Of Date<\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td align =\"left\">";
//strVar += "		<td width=\"1327\">";
//strVar += "		<td width=\"1327\" ><table width=\"100\" align =\"left\"><tr><td>"+locationAddress+"<\/td><\/tr><table\/><\/td>";
//strVar += "		<td width=\"1327\" >"+locationAddress+"<\/td>";
strVar += "		<table width=\"200\" align =\"left\">";
strVar += "			<tr>";
strVar += "   <td align =\"left\" font-size=\"9\">"+locationAddress+"<\/td>"
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"250\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td  align=\"left\" width=\"150\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
var SplitS_PayeeAddress_Email=new Array();
SplitS_PayeeAddress_Email=S_PayeeAddress_Email.split('##$$');
var s_PayeeAddress=SplitS_PayeeAddress_Email[0];
var s_EmailAddr=SplitS_PayeeAddress_Email[1];

       //***********************************************************				
		 s_PayeeAddress = s_PayeeAddress.toString();
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  s_PayeeAddress.toString()= " + s_PayeeAddress);
		 
		 //s_PayeeAddress = s_PayeeAddress.replace(/\n/g,"<br\/>");/// /g
		  s_PayeeAddress = s_PayeeAddress.replace(/\r/g,"<br\/>");/// /g
		 
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  s_PayeeAddress After Replcing spaces with % =====" + s_PayeeAddress);
		
		//*************************************************************				
		
	if(flagPayeeBTAddr==1)
	{
	 strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_customerName+"<br\/>"+s_PayeeAddress+"<\/td>";	
	}
	else if(flagPayeeBTAddr==2)
	{
			
		strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_PayeeAddress+"<\/td>";
	}//flagPayeeBTAddr
//strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_customerName+"<br\/>"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"300\" align=\"right\">";
strVar += "	      <tr>";
//strVar += "	        <td >&nbsp;<\/td>";
//strVar += "			";
//strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
//strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
//strVar += "			<td >&nbsp;<\/td>";
//strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+nlapiEscapeXML(s_TherapistList)+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	<\/td>";
strVar += "	<\/tr>";
strVar += "<\/table>";

///-------------------------------------------------------------------------------------------------

strVar += "	    <table border=\"0\" width=\"100%\" border-color=\"#C0C0C0\" cellpadding=\"3\">";
strVar += "		<tr background-color=\"#000000\">";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice&nbsp;#<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service&nbsp;Date<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Unit&nbsp;Price<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax&nbsp;%<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax&nbsp;Amt<\/td>";
strVar += "			<td font-size=\"10\" border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";
var pageBreak=6;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;

var remainUsage=context.getRemainingUsage();
nlapiLogExecution('DEBUG','summaryPrintPdf','$$$$$$***** Before for loop Start Usage=='+remainUsage);
	

var SpliteArray1=new Array();
var totalAmount=0;
var a_InvoiceID=new Array();
 	
 for (var k = 0; InvoiceDataArray != null && k < InvoiceDataArray.length; k++) 
 {
 	SpliteArray1=InvoiceDataArray[k].split('#$');
	//nlapiLogExecution('DEBUG','summaryPrintPdf','$$$$$$***** pageBreak=='+pageBreak);
	//nlapiLogExecution('DEBUG','summaryPrintPdf','$$$$$$***** k=='+k);
	/*
    nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' INV DATE[0]='+SpliteArray1[0]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' INV NO[1]='+SpliteArray1[1]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' SERVICE DATE[2]='+SpliteArray1[2]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' ITEM[3]='+SpliteArray1[3]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' QTY[4]='+SpliteArray1[4]); 
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' DIS[5]='+SpliteArray1[5]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' COMMENTS[6]='+SpliteArray1[6]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' TAX[7]='+SpliteArray1[7]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' AMT[8]='+SpliteArray1[8]);
*/
	a_InvoiceID[k]=SpliteArray1[11];

 	strVar += "		<tr>";
 	if (SpliteArray1[0] != null && SpliteArray1[0] !='NaN' && SpliteArray1[0]!='&nbsp;' && SpliteArray1[0]!='') 
	{
		strVar += "<td  border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\">"+nlapiEscapeXML(SpliteArray1[0])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\"><\/td>";
	}
	
	if (SpliteArray1[1] != null && SpliteArray1[1] != 'NaN' && SpliteArray1[1]!='&nbsp;' && SpliteArray1[1]!='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\">"+nlapiEscapeXML(SpliteArray1[1])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\"><\/td>";
	}
	
	if (SpliteArray1[2] != null && SpliteArray1[2] != 'NaN' && SpliteArray1[2]!='&nbsp;' && SpliteArray1[2]!='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\">"+nlapiEscapeXML(SpliteArray1[2])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\"><\/td>";
	}
	
 	if (SpliteArray1[3] != null && SpliteArray1[3] != 'NaN' && SpliteArray1[3]!='&nbsp;' && SpliteArray1[3]!='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[3])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	}
 	if (SpliteArray1[4] != null && SpliteArray1[4] != 'NaN' && SpliteArray1[4] !='' && SpliteArray1[4]!='&nbsp;' && SpliteArray1[4]!='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[4])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	}
	if (SpliteArray1[8] != null && SpliteArray1[8] != 'NaN' && SpliteArray1[8] != '' && SpliteArray1[8]!='&nbsp;' && SpliteArray1[8]!='') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[8])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	} 	
	
 	if (SpliteArray1[5] != null && SpliteArray1[5] != 'NaN' && SpliteArray1[5]!='&nbsp;' && SpliteArray1[5]!='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\">"+nlapiEscapeXML(SpliteArray1[5])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\"><\/td>";
	}
	
 	if (SpliteArray1[6] != null && SpliteArray1[6]!='&nbsp;' && SpliteArray1[6]!='' && SpliteArray1[6]!='NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\">"+nlapiEscapeXML(SpliteArray1[6])+"<\/td>";
		//strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#C0C0C0\" align=\"left\" font-size=\"9\"><\/td>";
	}
 	
	if (SpliteArray1[7] != null && SpliteArray1[7] != 'NaN' && SpliteArray1[7]!='&nbsp;' && SpliteArray1[7]!='') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[7])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	}
			
 	if (SpliteArray1[9] != null && SpliteArray1[9] != 'NaN' && SpliteArray1[9]!='&nbsp;' && SpliteArray1[9]!='') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[9])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	} 
	
	if (SpliteArray1[10] != null && SpliteArray1[10] != 'NaN' && SpliteArray1[10]!='&nbsp;' && SpliteArray1[10]!='') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[10])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\"><\/td>";
	}	
 	strVar += "			<\/tr>";
	
 }//for flagCheck
 
      
      var r_TotalInvoicesTax=0;
	  for(var y=0;a_TotalTax!=null && y<a_TotalTax.length;y++)
	  {
	  	 r_TotalInvoicesTax=parseFloat(r_TotalInvoicesTax)+parseFloat(a_TotalTax[y]);
	  } 
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"10\"  >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(roundNumber(r_TotalAmount))+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"10\" >GST/HST<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(roundNumber(r_TotalInvoicesTax))+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"10\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#C0C0C0\" font-size=\"9\" align =\"right\">"+"$"+nlapiEscapeXML(roundNumber(r_AllAmountDue))+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";


//------------------------------------------------------------------------------------------------



			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			var remainUsage=context.getRemainingUsage();
			nlapiLogExecution('DEBUG','ReadfileFrmCabinet','%%%%%% PDF Generated after start Usage =='+remainUsage);		
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			nlapiLogExecution('DEBUG', 'PDF', '*****===PDF fileId='+ fileId);
	      
		  var remainUsage=context.getRemainingUsage();
	      nlapiLogExecution('DEBUG','ReadfileFrmCabinet','%%%%%% PDF Generated after end Usage =='+remainUsage);
	
			//--------------------Attached pdf file logic--------------
		nlapiLogExecution('DEBUG', 'PDF', '**a_InvoiceID length='+ a_InvoiceID.length);
			var tempArray=new Array();
			var cnt1=0;
			var cFlag=0;
			var cFlag2=0;
			for(var m=0;a_InvoiceID!=null && m<a_InvoiceID.length;m++)
			{
				if(tempArray.length==0 && cFlag==0)
				{
					tempArray[cnt1++]=a_InvoiceID[m];
					cFlag=1;
				}
				else if(tempArray.length!=0 && cFlag==1)
				{
					cFlag2=0;
					for(var m2=0 ;tempArray!=null && m2<tempArray.length ;m2++)
					{
						if(tempArray[m2]==a_InvoiceID[m])
						{
						  //tempArray[cnt1++]=a_InvoiceID[m];
						  cFlag2=1;	
						}//if
					}//
					if(cFlag2!=1)
					{
						tempArray[cnt1++]=a_InvoiceID[m];
					}
					
				}//else if
				
			}//for m
			var recordMessagesSoId=new Array();
			nlapiLogExecution('DEBUG', 'PDF', '**tempArray length='+ tempArray.length);
			for(var s=0;tempArray!=null && s<tempArray.length;s++)
			{
				var invoiceObj=nlapiLoadRecord('invoice',tempArray[s]);
				var createdfrom=invoiceObj.getFieldValue('createdfrom');
				
				if(createdfrom!=null)
				{
					recordMessagesSoId[s]=createdfrom; ///  in this SO Id stored
					var type = 'file';       // the record type for the record being attached
					var id = fileId ;          // the internal ID of an existing jpeg in the file cabinet
					var type2 = 'salesorder'; // the record type for the record being attached to
					var id2 = createdfrom; // this is the internal ID for the customer
					var attributes = null;
					
					nlapiAttachRecord(type, id, type2, id2, attributes);
					
				}//if
				
				
			}//for s
		///--------------------------- find unique sales order id ---------------
		nlapiLogExecution('DEBUG', 'PDF', '**before recordMessagesSoId length='+ recordMessagesSoId.length);
			var tempArrayMsg=new Array();
			var cnt12=0;
			var cFlag11=0;
			var cFlagMsg=0;
			for(var mm=0;recordMessagesSoId!=null && mm<recordMessagesSoId.length;mm++)
			{
				if(tempArrayMsg.length==0 && cFlag11==0)
				{
					tempArrayMsg[cnt12++]=recordMessagesSoId[mm];
					cFlag11=1;
				}
				else if(tempArrayMsg.length!=0 && cFlag11==1)
				{
					cFlagMsg=0;
					for(var mm2=0 ;tempArrayMsg!=null && mm2<tempArrayMsg.length ;mm2++)
					{
						if(tempArrayMsg[mm2]==recordMessagesSoId[mm])
						{
						  //tempArray[cnt1++]=a_InvoiceID[m];
						  cFlagMsg=1;	
						  break;
						}//if
					}//
					if(cFlagMsg!=1)
					{
						tempArrayMsg[cnt12++]=recordMessagesSoId[mm];
					}
					
				}//else if
				
			}//for mm
		
				nlapiLogExecution('DEBUG', 'PDF', '**after tempArrayMsg length='+ tempArrayMsg.length);
		///--------------------- Sending email Logic ------------------
		if(i_customerID!=null && i_customerID!='')
		{
			var custRecObj=nlapiLoadRecord('customer',i_customerID);
			var clientEmailFlag=custRecObj.getFieldValue('custentity_tjinc_bbonbf_clientemailflag');
			nlapiLogExecution('DEBUG', 'PDF', 'clientEmailFlag%%%%%%%%%%='+ clientEmailFlag);
			for(var w1=0;tempArray!=null && w1<tempArray.length;w1++)
			{
				nlapiLogExecution('DEBUG', 'PDF', 'Invoice id['+w1+']='+ tempArray[w1]);
				nlapiSubmitField('invoice',tempArray[w1],'custbody_tjinc_bbonbf_repengflag','T');
			}//for
			if(clientEmailFlag=='T')
			{
				if(s_EmailAddr!=null && s_EmailAddr!='')
				{
					nlapiLogExecution('DEBUG', 'PDF', 'in if clientEmailFlag='+ clientEmailFlag);
					//var auther='19182';   /// carlos id
					var auther='19183';
					var Subject="blueballoon Health Services: Monthly Summary Invoice";
					//var cc='blueballoon.invoices@blue-balloon.co';
					//var bcc='invoices@bue-balloon.com';
					var bcc='blueballoon.invoices@blue-balloon.com';
					var records=new Array();
					
					var body="";				
					
					body+= "Thank you for your recent visit to blueballoon Health Services. Attached is a copy of the ";
					body+="invoice for your records.<br\/><br\/>";
					body+="If you have any questions about your invoice please contact your local Client Support Team.<br\/><br\/>";;
					//body+="<p>Please do not reply to this email message.<\/p><br\/><br\/>";
					body+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
					body+="Regards,<br\/><br\/>";
					body+="Billing Team<br\/>";
					//body+="Billing Team"+"<br\/>";
					//records['entity'] = custrecinternalid;  //on customer
					//records['transaction'] = recordMessagesSoId; ///on sales order
					records['transaction'] = tempArrayMsg; ///on sales order
					
					nlapiSendEmail(auther,s_EmailAddr, Subject, body, null, bcc, records, file);	
					
					for(var w=0;w<tempArray.length;w++)
					{
						nlapiLogExecution('DEBUG', 'PDF', 'Invoice id['+w+']='+ tempArray[w]);
						nlapiSubmitField('invoice',tempArray[w],'custbody_emailed','T');
					}//for
					
				}//if s_EmailAddr			
				
						
			}//if clientEmailFlag
			
			
		}//if s_EmailAddr
		
		var remainUsage=context.getRemainingUsage();
	    nlapiLogExecution('DEBUG','ReadfileFrmCabinet','************ summaryPrintPdf end Usage =='+remainUsage);
	

}//function Summary









///////////////// ------------- Consolidated Print ----------------------//////////////////////////////////

function ConsolidatePrintPdf(a_AllPrimaryTherapist,S_PayeeAddress_Email,s_customerName,i_customerID,ItemQtyArray,r_TotalAmount,r_AllAmountDue,LocationId,s_customerEmail,a_InvoiceID,flagPayeeBTAddr,a_TotalTax)         
{	
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='CI_'+i_customerID+'_'+timeStamp;
	}
	
	//nlapiLogExecution('DEBUG', 'PDF', '##### LocationId='+LocationId);
	if(LocationId!=null && LocationId!='')
	{
		var locationObj=nlapiLoadRecord('location',LocationId);
		locationAddress=locationObj.getFieldValue('addrtext');
		//***********************************************************				
		 locationAddress = locationAddress.toString();
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  locationAddress.toString()= " + locationAddress);
		 
		 locationAddress = locationAddress.replace(/\n/g,"<br\/>");/// /g
		 //locationAddress = locationAddress.replace(/\r/g,"<br\/>");/// /g
		
		 
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  locationAddress After Replcing spaces with % =====" + locationAddress);
		
		//*****
	}//if
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var s_TherapistList='';
	for(var c=0;a_AllPrimaryTherapist!=null && c<a_AllPrimaryTherapist.length;c++)
	{
		if(c==0)
		{
			s_TherapistList=a_AllPrimaryTherapist[c];
		}
		else
		{
			s_TherapistList=s_TherapistList+', '+a_AllPrimaryTherapist[c];
		}
		
	}
	
	
	
	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\" align =\"right\"><h2>Consolidated Invoice<\/h2><table width=\"140\" align =\"right\"><tr><td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Invoice Creation Date<\/td><\/tr><tr><td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td><\/tr><tr><td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Supplementary Invoice #<\/td><\/tr><tr><td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+a_InvoiceID[0]+"<\/td><\/tr><\/table><\/td>";
//strVar += "				<td width=\"206\" align =\"right\"><h2>Consolidated Invoice<\/h2><table width=\"140\" align =\"right\"><tr><td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Invoice Creation Date<\/td><td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td><\/tr><tr><td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Supplementary Invoice #<\/td><td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td><\/tr><\/table><\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">";
strVar += "		<table width=\"200\" align =\"left\">";
strVar += "			<tr>";
strVar += "   <td align =\"left\" font-size=\"9\">"+locationAddress+"<\/td>"
strVar += "			<\/tr>";
strVar += "		<\/table>			";
//strVar += "		"+CompanyAddrass+"<\/td>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"250\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td  align=\"left\" width=\"150\" font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";

var SplitS_PayeeAddress_Email=new Array();
SplitS_PayeeAddress_Email=S_PayeeAddress_Email.split('##$$');
var s_PayeeAddress=SplitS_PayeeAddress_Email[0];
var s_EmailAddr=SplitS_PayeeAddress_Email[1];

        //***********************************************************				
		 s_PayeeAddress = s_PayeeAddress.toString();
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  s_PayeeAddress.toString()= " + s_PayeeAddress);
		 
		 //s_PayeeAddress = s_PayeeAddress.replace(/\n/g,"<br\/>");/// /g
		 s_PayeeAddress = s_PayeeAddress.replace(/\r/g,"<br\/>");/// /g
		
		 
		 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  s_PayeeAddress After Replcing spaces with % =====" + s_PayeeAddress);
		
		//*************************************************************				
		
	if(flagPayeeBTAddr==1)
	{
	 strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_customerName+"<br\/>"+s_PayeeAddress+"<\/td>";	
	}
	else if(flagPayeeBTAddr==2)
	{
		
		strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_PayeeAddress+"<\/td>";
	}//flagPayeeBTAddr
//strVar += "			<td align=\"left\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+s_customerName+"<br\/>"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
//strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"300\" align=\"right\" >";
strVar += "	      <tr>";
//strVar += "	        <td >&nbsp;<\/td>";
//strVar += "			";
//strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
//strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"10\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
//strVar += "			<td >&nbsp;<\/td>";
//strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+nlapiEscapeXML(s_TherapistList)+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	<\/td>";
strVar += "	<\/tr>";
strVar += "<\/table>";


//------------------------------------------------------------------------------------------------------



strVar += "	    <table border=\"0\" width=\"100%\" border-color=\"#C0C0C0\" cellpadding=\"3\">";
strVar += "		<tr background-color=\"#000000\">";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Unit&nbsp;Price<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Tax&nbsp;%<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Tax&nbsp;Amt<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"10\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";

var pageBreak=6;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;
//var a_InvoiceID=new Array();
var SpliteArray1=new Array();
var totalAmount=0;
 for (var k = 0; ItemQtyArray != null && k < ItemQtyArray.length; k++) 
 {
 	SpliteArray1=ItemQtyArray[k].split('#$');
	//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' item id[0]='+SpliteArray1[0]);
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' item name['+k+']='+SpliteArray1[1]);
	//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Qty[0]='+SpliteArray1[2]);
	//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Tax[0]='+SpliteArray1[3]);
	//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' Amt[0]='+SpliteArray1[4]);
 	//a_InvoiceID[k]=SpliteArray1[5]; 
	strVar += "		<tr >";
 	if (SpliteArray1[1]!= null && SpliteArray1[1]!= 'NaN' && SpliteArray1[1]!= 'undefined' && SpliteArray1[1]!= '' && SpliteArray1[1]!= '&nbsp;') 
	{
		strVar += "<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"left\">"+nlapiEscapeXML(SpliteArray1[1])+"<\/td>";
	}
	else
	{
		strVar += "<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"left\"><\/td>";
	}
 	if (SpliteArray1[2] != null && SpliteArray1[2] != 'NaN' && SpliteArray1[2] !='' && SpliteArray1[2]!='undefined' && SpliteArray1[2]!= '&nbsp;') 
	{
		strVar += "<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\">"+SpliteArray1[2]+"<\/td>";
	}
	else
	{
		strVar += "<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\"><\/td>";
	}
	 	 	
	if (SpliteArray1[4] != null && SpliteArray1[4] != 'NaN' && SpliteArray1[4] !='' && SpliteArray1[4]!='undefined' && SpliteArray1[4]!= '&nbsp;') 
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[4])+"<\/td>";
	}
	else
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\"><\/td>";
	}
	
 	if (SpliteArray1[3]  != null && SpliteArray1[3]  != 'NaN' && SpliteArray1[3] != '' && SpliteArray1[3]!='undefined' && SpliteArray1[4]!= '&nbsp;') 
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\">"+nlapiEscapeXML(SpliteArray1[3])+"%"+"<\/td>";
	}
	else
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\"><\/td>";
	} 
	if (SpliteArray1[5]  != null && SpliteArray1[5]  != 'NaN' && SpliteArray1[5] != '' && SpliteArray1[5]!='undefined' && SpliteArray1[5]!= '&nbsp;') 
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\">"+SpliteArray1[5] +"<\/td>";
	}
	else
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\"><\/td>";
	}
	
	if (SpliteArray1[6]  != null && SpliteArray1[6]  != 'NaN' && SpliteArray1[6] != '' && SpliteArray1[6]!='undefined' && SpliteArray1[6]!= '&nbsp;') 
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\">"+SpliteArray1[6] +"<\/td>";
	}
	else
	{
		strVar += "			<td font-size=\"9\" border=\"0.1pt\" border-color=\"#C0C0C0\" align =\"right\"><\/td>";
	}
 	strVar += "			<\/tr>";
	totalAmount=parseFloat(totalAmount)+parseFloat(SpliteArray1[6]);
  	
	
 }//for flagCheck
 
        
      var r_TotalInvoicesTax=0;
	  for(var y=0;y<a_TotalTax.length;y++)
	  {
	  	 r_TotalInvoicesTax=parseFloat(r_TotalInvoicesTax)+parseFloat(a_TotalTax[y]);
	  }

	
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"3\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" font-size=\"10\" border-color=\"#C0C0C0\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" font-size=\"9\" border-color=\"#C0C0C0\" align =\"right\">"+nlapiEscapeXML(roundNumber(totalAmount))+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" font-size=\"10\" border-color=\"#C0C0C0\" >GST/HST<\/td>";
	strVar += "		<td border=\"0.1pt\" font-size=\"9\" border-color=\"#C0C0C0\" align =\"right\">"+nlapiEscapeXML(roundNumber(r_TotalInvoicesTax))+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" font-size=\"10\" border-color=\"#C0C0C0\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" font-size=\"9\" border-color=\"#C0C0C0\" align =\"right\">"+"$"+nlapiEscapeXML(roundNumber(r_AllAmountDue))+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";



//------------------------------------------------------------------------------------------------------

			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			//nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			//nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
			
			      
			//----------------------------------			
			
			//--------------------Attached pdf file logic--------------
		    nlapiLogExecution('DEBUG', 'PDF', '**a_InvoiceID length='+ a_InvoiceID.length);
			var tempArray=new Array();
			var cnt1=0;
			var cFlag=0;
			var cFlag2=0;
			for(var m=0;a_InvoiceID!=null && m<a_InvoiceID.length;m++)
			{
				if(tempArray.length==0 && cFlag==0)
				{
					tempArray[cnt1++]=a_InvoiceID[m];
					cFlag=1;
				}
				else if(tempArray.length!=0 && cFlag==1)
				{
					cFlag2=0;
					for(var m2=0 ;tempArray!=null && m2<tempArray.length ;m2++)
					{
						if(tempArray[m2]==a_InvoiceID[m])
						{
						  //tempArray[cnt1++]=a_InvoiceID[m];
						  cFlag2=1;	
						}//if
					}//
					if(cFlag2!=1)
					{
						tempArray[cnt1++]=a_InvoiceID[m];
					}
					
				}//else if
				
			}//for m
			
			
			var recordMessagesSoId=new Array();
			for(var s=0;tempArray!=null && s<tempArray.length;s++)
			{
				var invoiceObj=nlapiLoadRecord('invoice',tempArray[s]);
				var createdfrom=invoiceObj.getFieldValue('createdfrom');
				
				if(createdfrom!=null)
				{
					
					recordMessagesSoId[s]=createdfrom; ///  in this SO Id stored
					nlapiLogExecution('DEBUG', 'PDF', '%%%%%%%%recordMessagesSoId[s]='+recordMessagesSoId[s]);
					var type = 'file';       // the record type for the record being attached
					var id = fileId ;          // the internal ID of an existing jpeg in the file cabinet
					var type2 = 'salesorder'; // the record type for the record being attached to
					var id2 = createdfrom; // this is the internal ID for the customer
					var attributes = null;
					
					nlapiAttachRecord(type, id, type2, id2, attributes);
					
					nlapiLogExecution('DEBUG', 'PDF', '%%%%%%%%Attach Record='+ type+' '+id);
					
				}//if
				
				
			}//for s
			
	
		
		///--------------------------- find unique sales order id ---------------
		nlapiLogExecution('DEBUG', 'PDF', '**before recordMessagesSoId length='+ recordMessagesSoId.length);
			var tempArrayMsg=new Array();
			var cnt12=0;
			var cFlag11=0;
			var cFlagMsg=0;
			for(var mm=0;recordMessagesSoId!=null && mm<recordMessagesSoId.length;mm++)
			{
				if(tempArrayMsg.length==0 && cFlag11==0)
				{
					tempArrayMsg[cnt12++]=recordMessagesSoId[mm];
					cFlag11=1;
				}
				else if(tempArrayMsg.length!=0 && cFlag11==1)
				{
					cFlagMsg=0;
					for(var mm2=0 ;tempArrayMsg!=null && mm2<tempArrayMsg.length ;mm2++)
					{
						if(tempArrayMsg[mm2]==recordMessagesSoId[mm])
						{
						  //tempArray[cnt1++]=a_InvoiceID[m];
						  cFlagMsg=1;	
						  break;
						}//if
					}//
					if(cFlagMsg!=1)
					{
						tempArrayMsg[cnt12++]=recordMessagesSoId[mm];
					}
					
				}//else if
				
			}//for mm
		
				nlapiLogExecution('DEBUG', 'PDF', '**after tempArrayMsg length='+ tempArrayMsg.length);
		
				//////--------------------- Supplementory Invoice custom record creation logic ------------------
		nlapiLogExecution('DEBUG', 'PDF', '**i_customerID='+ i_customerID);
		
		if(tempArray!=null)
		{
			var SupplementoryInvoiceObj=nlapiCreateRecord('customrecord_supplementaryinvoice');
			SupplementoryInvoiceObj.setFieldValue('custrecord_supplementoryinvoice',timeStamp);
			SupplementoryInvoiceObj.setFieldValues('custrecord_invoice',tempArray);
			SupplementoryInvoiceObj.setFieldValue('custrecord_tjinc_bbonbf_customersupinv',i_customerID)
			SupplementoryInvoiceObj.setFieldValue('custrecord_tjinc_bbonbf_servplsupinv',tempArrayMsg[0]);
			var SubmitRec=nlapiSubmitRecord(SupplementoryInvoiceObj,true,true);
			nlapiLogExecution('DEBUG', 'PDF', '**SupplementoryInvoice Custom Record Submit='+ SubmitRec);
			
		}//if
	
		///--------------------- Sending email Logic ------------------
		
        if(i_customerID!=null && i_customerID!='')
		{
			var custRecObj=nlapiLoadRecord('customer',i_customerID);
			var clientEmailFlag=custRecObj.getFieldValue('custentity_tjinc_bbonbf_clientemailflag');
			nlapiLogExecution('DEBUG', 'PDF', 'clientEmailFlag='+ clientEmailFlag);
			for(var w1=0;tempArray!=null && w1<tempArray.length;w1++)
			{
				nlapiLogExecution('DEBUG', 'PDF', 'Invoice id['+w1+']='+ tempArray[w1]);
				nlapiSubmitField('invoice',tempArray[w1],'custbody_tjinc_bbonbf_repengflag','T');
			}//for
			if(clientEmailFlag=='T' && s_EmailAddr!=null && s_EmailAddr!='')
			{
				//var auther='19182';   /// carlos id
				var auther='19183';
				var Subject="blueballoon Health Services: Monthly Consolidated Invoice";
				//var cc='blueballoon.invoices@blue-balloon.co';
				//var bcc='invoices@bue-balloon.com';
				var bcc='blueballoon.invoices@blue-balloon.com';
				var records=new Array();				
				var body="";				
					
				body+= "Thank you for your recent visit to blueballoon Health Services. Attached is a copy of the ";
				body+="invoice for your records.<br\/><br\/>";
				body+="If you have any questions about your invoice please contact your local Client Support Team.<br\/><br\/>";;
				//body+="<p>Please do not reply to this email message.<\/p><br\/><br\/>";
				body+= "<b>Please do not reply to this email message.<\/b><br\/><br\/>";
				body+="Regards,<br\/><br\/>";
				body+="Billing Team<br\/>";
				
				
				//records['entity'] = custrecinternalid;  //on customer
				records['transaction'] = tempArrayMsg; ///on sales order
				//nlapiSendEmail(FromAuthorID, toList, subjectTemplate, msg, null, null, records, null);
				nlapiSendEmail(auther,s_EmailAddr, Subject, body, null, bcc, records, file);	
				
				for(var w=0;tempArray!=null && w<tempArray.length;w++)
				{
					nlapiSubmitField('invoice',tempArray[w],'custbody_emailed','T');
				}//for
						
			}//if clientEmailFlag
			
			
		}//if s_EmailAddr

		

}//function consolidated



///=======================Reschedule Code Logic =========================

function Schedulescriptafterusageexceeded(k)
{
     ////Define all parameters to schedule the script for voucher generation.
     var params=new Array();
     params['status']='scheduled';
     params['runasadmin']='T';
     var startDate = new Date();
     params['startdate']=startDate.toUTCString();//
     params['custscript_customercounter']=k;
    var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
    ///var status = nlapiScheduleScript(154, null,params);
    //nlapiLogExecution('DEBUG','After Scheduling','Script scheduled status='+ status);
    
    ////If script is scheduled then successfuly then check for if status=queued
    if (status == 'QUEUED') 
     {
    //nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
     }
}//fun close




//FUNCTION FOR ROUND UP
function roundNumber(num)
{

    var result = String(Math.round(num * Math.pow(10, 2)) / Math.pow(10, 2));
    if (result.indexOf('.') < 0) 
	{
        result += '.';
    }
    while (result.length - result.indexOf('.') <= 2)
	{
        result += '0';
    }
    return result;
}// END OF FUNCTION FOR ROUND UP



///---------------------------------------------------------------------------------------









