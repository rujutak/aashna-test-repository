/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/



function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
   nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	//var filters = new Array();
    //var columns = new Array();
 	

   // filters[0] = new nlobjSearchFilter('entityid', null, 'is', item);
   // columns[0] = new nlobjSearchColumn('entityid');
 	var searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
  	if(searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'searchResult.length=' + searchResult.length);
		//for(var k=0; searchResult != null && k <searchResult.length; k++)
			
		var BillToAddress=new Array();
		var StoredArray=new Array();
		var SplitArray=new Array();
		var SplitArrayPayeeNull=new Array();
		
		for(var k=0; searchResult != null && k <1; k++)
		{			
			var result = searchResult[k];
			// return all columns associated with this search
			var columns = result.getAllColumns();
			var columnLen = columns.length;
			//nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var column = columns[0];
			var customerName = result.getValue(column);			
			//nlapiLogExecution('DEBUG', 'getitemtype', 'customerName['+k+']=' + customerName);
			
			var column = columns[1];
			var customerID = result.getValue(column);			
			//nlapiLogExecution('DEBUG', 'getitemtype', 'customerID['+k+']=' + customerID);
			
			var LoadCustomerRec=nlapiLoadRecord('customer', customerID);
			//nlapiLogExecution('DEBUG', 'getitemtype', 'LoadCustomerRec['+k+']=' + LoadCustomerRec);
			
			/*
            if (LoadCustomerRec != null && LoadCustomerRec != '') 
			{
				var Bill_To_Addr = LoadCustomerRec.getFieldValue('comments');
				nlapiLogExecution('DEBUG', 'getitemtype', 'LoadCustomerRec[' + k + ']=' + LoadCustomerRec);
				
				var LineCountAddr = LoadCustomerRec.getLineItemCount('addressbook');
				nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' LineCountAddr==' + LineCountAddr);
				
				if (LineCountAddr > 0 && LineCountAddr != null && LineCountAddr != '') 
				{					
					for (var x = 1; x <= LineCountAddr; x++) 
					{
						var DefaultBillingAddrVal = LoadCustomerRec.getLineItemValue('addressbook', 'defaultbilling', x);
						nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Default Billing Address[' + x + ']==' + DefaultBillingAddrVal);
						if (DefaultBillingAddrVal == 'T') 
						{
							var Address = LoadCustomerRec.getLineItemValue('addressbook', 'addressee', x);
							nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Billing Address[' + x + ']==' + Address);
						}
					}
				}
			}		
            */	
			var column = columns[2];
			var customerEmail = result.getValue(column);			
			//nlapiLogExecution('DEBUG', 'getitemtype', 'customerEmail['+k+']=' + customerEmail);
			
			var column = columns[3];
			var BPInvoiceType = result.getValue(column);			
			//nlapiLogExecution('DEBUG', 'getitemtype', 'BPInvoiceType['+k+']=' + BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var filtersSearch = new Array();
			filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', customerName);
            // var columnsSearch = new Array();
			
			var searchRes= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', filtersSearch, null);
			//nlapiLogExecution('DEBUG', 'getitemtype', 'searchRes['+k+'] len=' + searchRes.length);
			for(var i=0; searchRes != null && i <searchRes.length; i++)
			{
				var resultInvoice = searchRes[i];
				// return all columns associated with this search
				var columnsInvoice = resultInvoice.getAllColumns();
				var columnLen2 = columnsInvoice.length;
				//nlapiLogExecution('DEBUG', '***columnLen2 == '+ columnLen2);
				
				var Columns= columnsInvoice[0];
				var Customer_Created_Date = resultInvoice.getValue(Columns);	
				//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + Customer_Created_Date);		
							
				//var Columns= columnsInvoice[1];
				//var Columns1 = resultInvoice.getValue(Columns);			
				//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
				
				//var Columns= columnsInvoice[2];
				//var Columns2 = resultInvoice.getValue(Columns);			
				//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
				
				var Columns= columnsInvoice[3];
				var Invoice_ID = resultInvoice.getValue(Columns);			
				//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + Invoice_ID);
					
			    var LoadInvoiceRec=nlapiLoadRecord('invoice', Invoice_ID);
			    //nlapiLogExecution('DEBUG', 'getitemtype', 'LoadInvoiceRec['+i+']=' + LoadInvoiceRec);
										
				//var StoredArray=new Array();
				if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
				{
					var PayerNew=LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
					nlapiLogExecution('DEBUG', 'getitemtype', 'PayerNew['+i+']=' + PayerNew);
			    	
					if(PayerNew!=null && PayerNew!='')
					{
						//*************************************************************************************
				        var arrayLength=StoredArray.length;
						if(arrayLength==0)
						{
							StoredArray[0]=PayerNew+'#'+Invoice_ID;
						}
						if(arrayLength!=0)
						{
							var flag=0;
							//var SplitArray=new Array();
							for(var k=0;k<StoredArray.length;k++)
							{								
								SplitArray=StoredArray[k].split('#');
								if(SplitArray[0]==PayerNew)
								{
									StoredArray[k]=StoredArray[k]+'#'+Invoice_ID;
									flag=1;
									break;
								}
							}
							if(flag!=1)
							{
								StoredArray[StoredArray.length]=PayerNew+'#'+Invoice_ID;
							}
						}//if(arrayLength!=0)							
				       			
					
					}//if(PayerNew!=null && PayerNew!='')
					
							
					var InvoiceBillToAddress=LoadInvoiceRec.getFieldValue('billaddress');
			        //nlapiLogExecution('DEBUG', 'getitemtype', 'InvoiceBillToAddress= ' + InvoiceBillToAddress);			    	
														
                    if(PayerNew==null && InvoiceBillToAddress!=null)
					{
						var BillToArray=BillToAddress.length;
						if(BillToArray==0)
						{
							BillToAddress[0]=InvoiceBillToAddress+'#'+Invoice_ID;
						}
						if(BillToArray!=0)
						{
							var flag=0;
							for(var l=0;l<BillToAddress.length;l++)
							{								
								SplitArrayPayeeNull=BillToAddress[l].split('#');
								if(SplitArrayPayeeNull[0]==InvoiceBillToAddress)
								{
									BillToAddress[l]=BillToAddress[l]+'#'+Invoice_ID;
									flag=1;
									break;
								}
							}
							if(flag!=1)
							{
								BillToAddress[BillToAddress.length]=BillToAddress+'#'+Invoice_ID;
							}
						}//if(arrayLength!=0)
					}										
					
				}
												
				var Columns= columnsInvoice[4];
				var Invoice_Date = resultInvoice.getValue(Columns);			
				nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + Invoice_Date);
				
				var Columns= columnsInvoice[5];
				var BP_Invoice_Type = resultInvoice.getValue(Columns);			
				nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + BP_Invoice_Type);
					
			}//for i - invoice
			
			//Code for Summarize
			
			if(BPInvoiceType==3) //3 means summary BPInvoiceType
			{
				var SplitStoredArray=new Array();
				nlapiLogExecution('DEBUG', 'getitemtype', 'StoredArray Length=' + StoredArray.length);
							
				for(s=0;s<StoredArray.length;s++)
				{
					var a_InvoiceDate=new Array();
					var a_InvoiceID=new Array();
					var a_ServiceDate=new Array();
					var a_InvoiceItems=new Array();
					var a_InvItemQuantity=new Array();
					var a_ItemDescription=new Array();
					var a_Comments=new Array();
					var a_ItemTax=new Array();
					var a_Amount=new Array();
					var ArrCount=0;
					var TotalAmount=0;	
					var AsOfDate='';
					var AllPrimaryTherapist='';
					var AllAmountDue=0;
							
					var PayeeAddress='';					
					SplitStoredArray=StoredArray[s].split('#');
					
					if(SplitStoredArray[0]!=null)
					{
						var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitStoredArray[0]);
			            //nlapiLogExecution('DEBUG', 'getitemtype', 'LoadPayerRec['+i+']=' + LoadPayerRec);
			            
						if(LoadPayerRec!=null && LoadPayerRec!='')
						{
							PayeeAddress=LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
					        nlapiLogExecution('DEBUG', 'getitemtype', 'PayeeAddress['+i+']=' + PayeeAddress);			    	
						}
						//nlapiLogExecution('DEBUG', 'getitemtype', 'SplitStoredArray Length=' + SplitStoredArray.length);
						//Get The Data from Invoice
						for(var n=1;n<SplitStoredArray.length;n++)
						{							
							//nlapiLogExecution('DEBUG', 'getitemtype', 'SplitStoredArray['+n+']=' + SplitStoredArray[n]);
							
							var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitStoredArray[n]);
			                nlapiLogExecution('DEBUG', 'getitemtype', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
							
                            if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
							{
						    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
							//nlapiLogExecution('DEBUG', 'getitemtype', 'InvoiceDate['+i+']=' + InvoiceDate);
					    			
							var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
							//nlapiLogExecution('DEBUG', 'getitemtype', 'ServiceDate['+i+']=' + ServiceDate);
					    	
							var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'LineItemCount ==' + LineItemCount);
							
							var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'LineItemCount ==' + LineItemCount);
							
							var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'AmountRemaining ==' + AmountRemaining);
							
							if (LineItemCount != null && LineItemCount != '') 
							{
								
								
								//Invoice Line Count
		                        for (var j = 1; j <= LineItemCount; j++) 
								{
						
									var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
									
									var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
									
									var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
									
									var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
									
									var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceITax ['+j+']==' + InvoiceITax);
									
									ArrCount=ArrCount+1;
									a_InvoiceDate[ArrCount]=InvoiceDate;
									
									if(AsOfDate=='')
									{
										AsOfDate=''+InvoiceDate;
									}									
									else
									{
										AsOfDate=AsOfDate+', '+InvoiceDate;
									}	
									if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
									{
										if(AllPrimaryTherapist=='')
										{
											AllPrimaryTherapist=''+PrimaryTherapist
										}
										else
										{
											AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
										}										
									}
													
									AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);
									a_InvoiceID[ArrCount]=SplitStoredArray[n];
									a_ServiceDate[ArrCount]=ServiceDate;
									a_InvoiceItems[ArrCount]=InvoiceItemText;
									a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
									a_ItemDescription[ArrCount]=InvoiceItemDescription;
									//a_Comments[ArrCount]=
									a_ItemTax[ArrCount]=InvoiceITax
									a_Amount[ArrCount]=InvoiceItemAmount;
									TotalAmount=TotalAmount+parseFloat(InvoiceItemAmount);
								}
		                     
							}
								
							}						
						}	//for n	
						
					}//if
					//Generate PDF
					/*for(a=1;a<a_InvoiceDate.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceDate['+a+']=' + a_InvoiceDate[a]);
					}
					for(a=1;a<a_InvoiceID.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceID['+a+']=' + a_InvoiceID[a]);
					}	
					for(a=1;a<a_ServiceDate.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_ServiceDate['+a+']=' + a_ServiceDate[a]);
					}	
					for(a=1;a<a_InvoiceItems.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceItems['+a+']=' + a_InvoiceItems[a]);
					}	
					for(a=1;a<a_InvItemQuantity.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvItemQuantity['+a+']=' + a_InvItemQuantity[a]);
					}	
					for(a=1;a<a_ItemDescription.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_ItemDescription['+a+']=' + a_ItemDescription[a]);
					}	
					for(a=1;a<a_Amount.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_Amount['+a+']=' + a_Amount[a]);
					}*/					
					var array=1;
					//summaryPrintPdf(AsOfDate,AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
					
					
				}//for
				
				var SplitBillToAddress=new Array();
			
				
			//Second Array BillToAddress
				
				for(a=0;a<BillToAddress.length;a++)
				{
					var a_InvoiceDate=new Array();
					var a_InvoiceID=new Array();
					var a_ServiceDate=new Array();
					var a_InvoiceItems=new Array();
					var a_InvItemQuantity=new Array();
					var a_ItemDescription=new Array();
					var a_Comments=new Array();
					var a_ItemTax=new Array();
					var a_Amount=new Array();
					var ArrCount=0;
					var TotalAmount=0;	
					var AsOfDate='';
					var AllPrimaryTherapist='';
					var AllAmountDue=0;
						
					var PayeeAddress='';					
					SplitBillToAddress=BillToAddress[a].split('#');
					
					if(SplitBillToAddress[0]!=null)
					{
						//var LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', SplitBillToAddress[0]);
			            //nlapiLogExecution('DEBUG', 'getitemtype', 'LoadPayerRec['+i+']=' + LoadPayerRec);
			            
						//if(LoadPayerRec!=null && LoadPayerRec!='')
						//{
						//	PayeeAddress=LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
					    //    nlapiLogExecution('DEBUG', 'getitemtype', 'PayeeAddress['+i+']=' + PayeeAddress);			    	
						//}
						//nlapiLogExecution('DEBUG', 'getitemtype', 'SplitBillToAddress Length=' + SplitBillToAddress.length);
						//Get The Data from Invoice
						for(var n=1;n<SplitBillToAddress.length;n++)
						{							
							//nlapiLogExecution('DEBUG', 'getitemtype', 'SplitBillToAddress['+n+']=' + SplitBillToAddress[n]);
							
							var LoadInvoiceRec=nlapiLoadRecord('invoice', SplitBillToAddress[n]);
			                //nlapiLogExecution('DEBUG', 'getitemtype', 'LoadInvoiceRec['+n+']=' + LoadInvoiceRec);
							
                            if(LoadInvoiceRec!=null && LoadInvoiceRec!='')
							{
						    var InvoiceDate=LoadInvoiceRec.getFieldValue('trandate');
							//nlapiLogExecution('DEBUG', 'getitemtype', 'InvoiceDate['+i+']=' + InvoiceDate);
					    			
							var ServiceDate=LoadInvoiceRec.getFieldValue('custbody2');
							//nlapiLogExecution('DEBUG', 'getitemtype', 'ServiceDate['+i+']=' + ServiceDate);
					    	
							var LineItemCount = LoadInvoiceRec.getLineItemCount('item');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'LineItemCount ==' + LineItemCount);
							
							var PrimaryTherapist = LoadInvoiceRec.getFieldValue('salesrep');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'LineItemCount ==' + LineItemCount);
							
							var AmountRemaining = LoadInvoiceRec.getFieldValue('amountremaining');
							//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'AmountRemaining ==' + AmountRemaining);
							
							if (LineItemCount != null && LineItemCount != '') 
							{
								
								
								//Invoice Line Count
		                        for (var j = 1; j <= LineItemCount; j++) 
								{
						
									var InvoiceItemText = LoadInvoiceRec.getLineItemText('item', 'item', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemText ['+j+']==' + InvoiceItemText);
									
									var InvoiceItemQuantity = LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemQuantity ['+j+']==' + InvoiceItemQuantity);
									
									var InvoiceItemDescription = LoadInvoiceRec.getLineItemValue('item', 'description', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemDescription ['+j+']==' + InvoiceItemDescription);
									
									var InvoiceItemAmount = LoadInvoiceRec.getLineItemValue('item', 'amount', j);
									nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceItemAmount ['+j+']==' + InvoiceItemAmount);
									
									var InvoiceITax = LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'InvoiceITax ['+j+']==' + InvoiceITax);
									
									ArrCount=ArrCount+1;
									a_InvoiceDate[ArrCount]=InvoiceDate;
									if(AsOfDate=='')
									{
										AsOfDate=''+InvoiceDate;
									}									
									else
									{
										AsOfDate=AsOfDate+', '+InvoiceDate;
									}
									if(PrimaryTherapist!=null && PrimaryTherapist!='' && PrimaryTherapist!='undefined')
									{
										if(AllPrimaryTherapist=='')
										{
											AllPrimaryTherapist=''+PrimaryTherapist
										}
										else
										{
											AllPrimaryTherapist=AllPrimaryTherapist+', '+PrimaryTherapist;
										}										
									}
									//nlapiLogExecution('DEBUG', 'onBeforeLoad', 'AllPrimaryTherapist ==' + AllPrimaryTherapist);
									AllAmountDue=AllAmountDue+parseFloat(AmountRemaining);																		
									a_InvoiceID[ArrCount]=SplitBillToAddress[n];
									a_ServiceDate[ArrCount]=ServiceDate;
									a_InvoiceItems[ArrCount]=InvoiceItemText;
									a_InvItemQuantity[ArrCount]=InvoiceItemQuantity;
									a_ItemDescription[ArrCount]=InvoiceItemDescription;
									//a_Comments[ArrCount]=
									a_ItemTax[ArrCount]=InvoiceITax
									a_Amount[ArrCount]=InvoiceItemAmount;
									if(InvoiceItemAmount!=null && InvoiceItemAmount!='')
									TotalAmount=TotalAmount+parseFloat(InvoiceItemAmount);
									nlapiLogExecution('DEBUG', 'getitemtype', 'TotalAmountInSecondArray==' + TotalAmount);
								}
		                     
							}
								
							}						
						}		
						
					}
					//Generate PDF
					/*for(a=1;a<a_InvoiceDate.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceDate['+a+']=' + a_InvoiceDate[a]);
					}
					for(a=1;a<a_InvoiceID.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceID['+a+']=' + a_InvoiceID[a]);
					}	
					for(a=1;a<a_ServiceDate.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_ServiceDate['+a+']=' + a_ServiceDate[a]);
					}	
					for(a=1;a<a_InvoiceItems.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvoiceItems['+a+']=' + a_InvoiceItems[a]);
					}	
					for(a=1;a<a_InvItemQuantity.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_InvItemQuantity['+a+']=' + a_InvItemQuantity[a]);
					}	
					for(a=1;a<a_ItemDescription.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_ItemDescription['+a+']=' + a_ItemDescription[a]);
					}	
					for(a=1;a<a_Amount.length;a++)
					{
						nlapiLogExecution('DEBUG', 'getitemtype', 'a_Amount['+a+']=' + a_Amount[a]);
					}	*/	
					
					nlapiLogExecution('DEBUG', 'getitemtype', 'TotalAmountInSecondArray==' + TotalAmount);			
					var array=2;
					summaryPrintPdf(AsOfDate,AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
					
				}
			}
			/*
if(BP_Invoice_Type==2)
			{
				for(s=0;s<StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'StoredArray['+s+']=' + StoredArray[s]);
			}
			for(a=0;a<BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'BillToAddress['+a+']=' + BillToAddress[a]);
			}
			}
			if(BP_Invoice_Type==1)
			{
				for(s=0;s<StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'StoredArray['+s+']=' + StoredArray[s]);
			}
			for(a=0;a<BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'BillToAddress['+a+']=' + BillToAddress[a]);
			}
			}
*/
			
		}//for
  		
  	}//if
 
 	
}//function close




///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(AsOfDate,AllPrimaryTherapist,PayeeAddress,customerName,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_ItemTax,a_Amount,TotalAmount,AllAmountDue,array)
{
	
	

			var strVar="";
			strVar += "<table border=\"0\" width=\"100%\" cellpadding=\"0\"  height=\"59\">";
			strVar += "	<tr>";
			strVar += "		<td align=\"right\" valign=\"top\" height=\"37\" border-color=\"silver\">";
			strVar += "		<table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "			<tr><td border=\"0.1pt\" border-color=\"silver\">3-Summary Invoice<\/td><\/tr>";
			strVar += "		<\/table>";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "		<td align =\"right\" valign =\"top\" border-color=\"silver\">";
			strVar += "		    <table border=\"0\" width=\"20%\" cellpadding=\"0\" >";
			strVar += "				<tr><td width =\"46%\" border=\"0.1pt\" border-color=\"silver\">As Of Date :<\/td><td  width =\"51%\" border=\"0.1pt\" border-color=\"silver\">"+AsOfDate+"<\/td><\/tr>";
			strVar += "			<\/table>";
			strVar += "	";
			strVar += "		<\/td>";
			strVar += "	<\/tr>";
			strVar += "	<tr>";
			strVar += "	<td>";
			//strVar += "	";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "		<tr>";
			strVar += "			<td>&nbsp;<\/td>";
			strVar += "		<\/tr>";
			strVar += "		<\/table>";
			strVar += "		<table width=\"100%\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			
			/*strVar += "				<td width=\"190\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Bill To :<\/td>";
			strVar += "				<td height=\"27\">&nbsp;<\/td>";
			strVar += "				<td height=\"27\" width=\"215\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<\/td>";
			strVar += "				<td width=\"218\" height=\"27\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan)<\/td>";
			*/
			strVar += "    <td width=\"286\" border=\"0.1pt\" border-color=\"silver\">Bill To :<br/>"+PayeeAddress+"<\/td>";
		    strVar += "    <td width=\"185\" >&nbsp;<\/td>";
		    strVar += "    <td width=\"245\" border=\"0.1pt\" border-color=\"silver\">Patient Name :<br/>"+customerName+"<\/td>";
		    strVar += "    <td width=\"296\" border=\"0.1pt\" border-color=\"silver\">Therapist ID (Primary From Service Plan): "+AllPrimaryTherapist+"<\/td>";
			strVar += "			<\/tr>";
			/*strVar += "			<tr>";
			strVar += "				<td width=\"190\">"+PayeeAddress+"<\/td>";
			strVar += "				<td>&nbsp;<\/td>";
			strVar += "				<td width=\"215\">"+customerName+"<\/td>";
			strVar += "				<td width=\"218\">&nbsp;<\/td>";
			strVar += "			<\/tr>";*/
			strVar += "	    <\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0\" width=\"100%\">";
			strVar += "			<tr>";
			strVar += "				<td>&nbsp;<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "";
			strVar += "		";
			strVar += "		<table border=\"0.1pt\" border-color=\"silver\" align =\"left\">";
			strVar += "			<tr>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Date<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Invoice #<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Service Date <\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Item<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Quantity<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Descriptions<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Comments<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Tax<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">Amount<\/td>";
			strVar += "			<\/tr>";
		      // iterate through the results
			  for ( var k = 1; a_InvoiceDate != null && k < a_InvoiceDate.length; k++ )
			  {
			  	strVar += "			<tr>";
			  	strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceID[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_ServiceDate[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvoiceItems[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_InvItemQuantity[k]+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";				
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">&nbsp;<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+nlapiEscapeXML(a_ItemTax[k])+"<\/td>";
				strVar += "				<td border=\"0.1pt\" border-color=\"silver\" align =\"left\">"+a_Amount[k]+"<\/td>";
			  	strVar += "			<\/tr>";
			  }
			strVar += "		<\/table>";
			strVar += "";
			strVar += "	";
			strVar += "		<table border=\"0\" >";
			strVar += "			<tr>";
			strVar += "				<td>&nbsp;<\/td>";
			strVar += "				";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";
			strVar += "		";
			strVar += "		<table width=\"30%\" align=\"right\" border=\"0.1pt\" border-color=\"silver\">";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Total<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+TotalAmount+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "			<tr>";
			strVar += "				<td width=\"91\" border=\"0.1pt\" border-color=\"silver\">Amount Due<\/td>";
			strVar += "				<td border=\"0.1pt\" border-color=\"silver\">"+AllAmountDue+"<\/td>";
			strVar += "			<\/tr>";
			strVar += "		<\/table>";			
			strVar += "	<\/td>";
			strVar += "	<\/tr>";
			//strVar += "	";		
			strVar += "<\/table>";
			
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body font-size=\"10\">";
			xml += "<p></p>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile('FirstSummaryPDF'+array+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//----------------------------------
			
			

}//function 

