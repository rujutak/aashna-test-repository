/*
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20120709
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/


// Main function
function TJINC_NSJ_ReportEngAutoInvoiceMonthly()
{
	
   nlapiLogExecution('DEBUG', 'getitemtype', 'In Scheduled script');
 	//var filters = new Array();
    //var a_columns = new Array();
 	

   // a_filters[0] = new nlobjSearchFilter('entityid', null, 'is', item);
   // a_columns[0] = new nlobjSearchColumn('entityid');
 	var a_searchResult= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_grp', null, null);
  	if(a_searchResult != null)
  	{
		nlapiLogExecution('DEBUG', 'getitemtype', 'a_searchResult.length=' + a_searchResult.length);
		//for(var k=0; a_searchResult != null && k <a_searchResult.length; k++)
			
		
		//for loop for Customer
		for(var k=7; a_searchResult != null && k <a_searchResult.length; k++)
		{			
			var s_result = a_searchResult[k];
			// return all columns associated with this search
			var a_columns = s_result.getAllColumns();
			var columnLen = a_columns.length;
			//nlapiLogExecution('DEBUG', 'columnLen == '+ columnLen);
			
			var s_column0 = a_columns[0];
			var s_customerName = s_result.getValue(s_column0);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerName['+k+']=' + s_customerName);
			
			var s_column1 = a_columns[1];
			var i_customerID = s_result.getValue(s_column1);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'customerID['+k+']=' + i_customerID);
			
			var o_LoadCustomerRec=nlapiLoadRecord('customer', i_customerID);
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec['+k+']=' + o_LoadCustomerRec);
			
			/*
            if (o_LoadCustomerRec != null && o_LoadCustomerRec != '') 
			{
				var Bill_To_Addr = o_LoadCustomerRec.getFieldValue('comments');
				nlapiLogExecution('DEBUG', 'CustomerSearch', 'o_LoadCustomerRec[' + k + ']=' + o_LoadCustomerRec);
				
				var LineCountAddr = o_LoadCustomerRec.getLineItemCount('addressbook');
				nlapiLogExecution('DEBUG', 'CustomerSearch', ' LineCountAddr==' + LineCountAddr);
				
				if (LineCountAddr > 0 && LineCountAddr != null && LineCountAddr != '') 
				{					
					for (var x = 1; x <= LineCountAddr; x++) 
					{
						var DefaultBillingAddrVal = o_LoadCustomerRec.getLineItemValue('addressbook', 'defaultbilling', x);
						nlapiLogExecution('DEBUG', 'CustomerSearch', ' Default Billing Address[' + x + ']==' + DefaultBillingAddrVal);
						if (DefaultBillingAddrVal == 'T') 
						{
							var Address = o_LoadCustomerRec.getLineItemValue('addressbook', 'addressee', x);
							nlapiLogExecution('DEBUG', 'CustomerSearch', ' Billing Address[' + x + ']==' + Address);
						}
					}
				}
			}		
            */	
			var s_column2 = a_columns[2];
			var s_customerEmail = s_result.getValue(s_column2);			
			//nlapiLogExecution('DEBUG', 'CustomerSearch', 's_customerEmail['+k+']=' + s_customerEmail);
			
			var s_column3 = a_columns[3];
			var s_BPInvoiceType = s_result.getValue(s_column3);			
			nlapiLogExecution('DEBUG', 'CustomerSearch', 's_BPInvoiceType['+k+']=' + s_BPInvoiceType);
			
			
			////------------------ use second search to map the customer with the open invoices.-------
			var a_filtersSearch = new Array();
			a_filtersSearch[0] = new nlobjSearchFilter('entityid', null, 'is', s_customerName);
            // var columnsSearch = new Array();
			
			var a_InvoicesearchResults= nlapiSearchRecord('customer', 'customsearch_reporting_engine_search_2', a_filtersSearch, null);
			nlapiLogExecution('DEBUG', 'CustomerSearch', 'a_InvoicesearchResults['+k+'] len=' + a_InvoicesearchResults.length);
			var a_BillToAddress=new Array();
			var a_StoredArray=new Array();
			var a_SplitArray=new Array();
			var a_SplitArrayPayeeNull=new Array();
			
				for (var i = 0; a_InvoicesearchResults != null && i < a_InvoicesearchResults.length; i++) 
				{
					var s_resultInvoice = a_InvoicesearchResults[i];
					// return all a_columns associated with this search
					var a_columnsInvoice = s_resultInvoice.getAllColumns();
					var i_columnLen2 = a_columnsInvoice.length;
					nlapiLogExecution('DEBUG', '***i_columnLen2 == ' + i_columnLen2);
					
					var s_Columns0 = a_columnsInvoice[0];
					var s_Customer_Created_Date = s_resultInvoice.getValue(s_Columns0);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Created Date=' + s_Customer_Created_Date);		
					
					//var Columns= a_columnsInvoice[1];
					//var Columns1 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Name=' + Columns1);
					
					//var Columns= a_columnsInvoice[2];
					//var Columns2 = s_resultInvoice.getValue(Columns);			
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Customer Email=' + Columns2);
					
					var s_Columns3 = a_columnsInvoice[3];
					var i_Invoice_ID = s_resultInvoice.getValue(s_Columns3);
					nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice ID=' + i_Invoice_ID);
					
					var o_LoadInvoiceRec = nlapiLoadRecord('invoice', i_Invoice_ID);
					nlapiLogExecution('DEBUG', 'getitemtype', 'o_LoadInvoiceRec['+i+']=' + o_LoadInvoiceRec);
					
					//var a_StoredArray=new Array();
					if (o_LoadInvoiceRec != null && o_LoadInvoiceRec != '') 
					{
						var s_PayerNew = o_LoadInvoiceRec.getFieldValue('custbody_tjinc_bbonbf_payernew');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_PayerNew['+i+']=' + s_PayerNew);
						
						if (s_PayerNew != null && s_PayerNew != '') 
						{
							//*************************************************************************************
							var i_arrayLength = a_StoredArray.length;
							if (i_arrayLength == 0) 
							{
								a_StoredArray[0] = s_PayerNew + '#' + i_Invoice_ID;
							}
							if (i_arrayLength != 0) 
							{
								var i_flag = 0;
								//var a_SplitArray=new Array();
								for (var k = 0; k < a_StoredArray.length; k++) 
								{
									a_SplitArray = a_StoredArray[k].split('#');
									if (a_SplitArray[0] == s_PayerNew) 
									{
										a_StoredArray[k] = a_StoredArray[k] + '#' + i_Invoice_ID;
										i_flag = 1;
										break;
									}
								}
								if (i_flag != 1) 
								{
									a_StoredArray[a_StoredArray.length] = s_PayerNew + '#' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)							
						}//if(s_PayerNew!=null && s_PayerNew!='')
						var s_InvoiceBillToAddress = o_LoadInvoiceRec.getFieldValue('billaddress');
						//nlapiLogExecution('DEBUG', 'getitemtype', 's_InvoiceBillToAddress= ' + s_InvoiceBillToAddress);			    	
						
						if (s_PayerNew == null && s_InvoiceBillToAddress != null) 
						{
							var i_BillToArray = a_BillToAddress.length;
							if (i_BillToArray == 0) 
							{
								a_BillToAddress[0] = s_InvoiceBillToAddress + '#' + i_Invoice_ID;
							}
							if (i_BillToArray != 0) 
							{
								var i_flag = 0;
								for (var l = 0; l < a_BillToAddress.length; l++) 
								{
									a_SplitArrayPayeeNull = a_BillToAddress[l].split('#');
									if (a_SplitArrayPayeeNull[0] == s_InvoiceBillToAddress) 
									{
										a_BillToAddress[l] = a_BillToAddress[l] + '#' + i_Invoice_ID;
										i_flag = 1;
										break;
									}
								}
								if (i_flag != 1) 
								{
									a_BillToAddress[a_BillToAddress.length] = a_BillToAddress + '#' + i_Invoice_ID;
								}
							}//if(i_arrayLength!=0)
						}
						
					}
					
					var s_Columns4 = a_columnsInvoice[4];
					var s_Invoice_Date = s_resultInvoice.getValue(s_Columns4);
					//nlapiLogExecution('DEBUG', 'getitemtype', 'Invoice Date=' + s_Invoice_Date);
					
					var s_Columns5 = a_columnsInvoice[5];
					var i_BP_Invoice_Type = s_resultInvoice.getValue(s_Columns5);
					nlapiLogExecution('DEBUG', 'getitemtype', 'BP Invoice Type=' + i_BP_Invoice_Type);
					
			}//for i - invoice
			
			//Code for Summarize
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			
			if(s_BPInvoiceType==3) //3 means summary s_BPInvoiceType
			{
				//FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
			}
			
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************			
			//Code for Consolidated
			
            if(s_BPInvoiceType==2)
			{
				functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
			}
//*******************************************************************************************************************************************************************************************************************************************************************************************************************************************
			/*
			if(s_BPInvoiceType==1)
			{
				for(s=0;s<a_StoredArray.length;s++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray['+s+']=' + a_StoredArray[s]);
			}
			for(a=0;a<a_BillToAddress.length;a++)
			{
				nlapiLogExecution('DEBUG', 'getitemtype', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
			}
			}
*/
			
		}//for
  		
  	}//if
 
 	
}//function close







//**********************************************************************************************************
                    
					//   FUNCTION FOR CONSOLIDATED

//**********************************************************************************************************




function functionConsolidated(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
{
	nlapiLogExecution('DEBUG', 'functionConsolidated', 'IN CONSOLIDATED FUNCTION' );
	var a_SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'getitemtype', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();
		var a_SplitItemArray=new Array();
		
		//var a_ItemArray=new Array();
		var a_ItemAmount=new Array();
		var a_ItemTaxAmt=new Array();
		//var a_SplitItemArray=new Array();
				
		var s_PayeeAddress='';					
		a_SplitStoredArray=a_StoredArray[s].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+s+']=' + a_SplitStoredArray[s]);
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        //nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				
				
				var s_PrimaryTherapist = o_LoadInvoiceRec.getFieldValue('salesrep');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
								
	           	//=========================================================================================================								
						if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var i_arrayLength=a_ItemArray.length;
							if(i_arrayLength==0)
							{
								a_ItemArray[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
							}
							if(i_arrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemArray.length;p++)
								{								
									a_SplitItemArray=a_ItemArray[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemArray[p]=a_ItemArray[p]+'#'+s_InvoiceItemQuantity;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemArray[a_ItemArray.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
								}
							}//if(i_arrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
								
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='')
						{
							//*************************************************************************************
					        var i_QtyarrayLength=a_ItemArray.length;
							if(i_QtyarrayLength==0)
							{
								a_ItemAmount[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemAmount;
							}
							if(i_QtyarrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemAmount.length;p++)
								{								
									a_SplitItemArray=a_ItemAmount[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemAmount[p]=a_ItemAmount[p]+'#'+s_InvoiceItemAmount;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemAmount[a_ItemAmount.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemAmount;
								}
							}//if(i_QtyarrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
						
	           //=========================================================================================================								
				
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}	
						if(s_PrimaryTherapist!=null && s_PrimaryTherapist!='' && s_PrimaryTherapist!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_PrimaryTherapist
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+'<br/>'+s_TherapistID;
							}										
						}
										
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);
						a_InvoiceID[i_ArrCount]=a_SplitStoredArray[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//r_TotalAmount=r_TotalAmount+parseFloat(s_InvoiceItemAmount);
					}
	             
				}
					
				}						
			}	//for n	
			
		}//if
		
		//Extracting tha Item Array for Calculate the Item Quantity
		
		
        for(var t=0;t<a_ItemArray.length;t++)
		{
			var a_ItemName=new Array();
			var a_ItemValue=new Array();
			var a_ItemQuantity=new Array();
			var a_ItemTotalQuantity=new Array();
			var a_SplitItemArrayQuantity=new Array();			
			
			var a_ArrayItemAmount=new Array();
			var a_TotalItemAmount=new Array();
			var a_SpliteArrayAmount=new Array();
			var i_count=0;
			a_SplitItemArrayQuantity=a_ItemArray[t].split('#');
			a_SpliteArrayAmount=a_ItemAmount[t].split('#');
			if (a_SplitItemArrayQuantity[0] != null) 
			{
				a_ItemName[i_count]=a_SplitItemArrayQuantity[0];
				//a_ItemValue[i_count]=a_SplitItemArrayQuantity[1];
				for (var u = 2; u < a_SplitItemArrayQuantity.length; u++) 
				{
					i_count=i_count+1;
					a_ItemQuantity=a_SplitItemArrayQuantity[u];
					a_ItemTotalQuantity=a_ItemTotalQuantity+parseFloat(a_ItemQuantity);						
									
				}							
			}
			if (a_SpliteArrayAmount[0] != null) 
			{
				//a_ItemName[i_count]=a_SpliteArrayAmount[0];
				//a_ItemValue[i_count]=a_SplitItemArrayQuantity[1];
				for (var u = 2; u < a_SpliteArrayAmount.length; u++) 
				{
					i_count=i_count+1;
					a_ArrayItemAmount=a_SpliteArrayAmount[u];
					a_TotalItemAmount=a_TotalItemAmount+parseFloat(a_ArrayItemAmount);						
									
				}							
			}
			for(var q=0;q<a_ItemName.length;q++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemName', 'a_ItemName ['+q+']==' + a_ItemName[q]);
			}
			for(var e=0;e<a_ItemTotalQuantity.length;e++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemTotalQuantity', 'a_ItemTotalQuantity ['+e+']==' + a_ItemTotalQuantity[e]);
			}
			for(var h=0;h<a_TotalItemAmount.length;h++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemTotalQuantity', '****a_TotalItemAmount*** ['+h+']==' + a_TotalItemAmount[h]);
			}
			
			//function call for Consolidated for array BilltoAddress
			ConsolidatePrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_ItemName,a_ItemTotalQuantity,a_ItemTax,a_TotalItemAmount,r_TotalAmount,r_AllAmountDue)
		}

		
		//Generate PDF
							
		var i_array=1;
		//summaryPrintPdf(s_AsOfDate,s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
		for(var w=0;w<a_ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', 'a_ItemArray ['+w+']==' + a_ItemArray[w]);
		}
	}//for
	
	var a_SplitBillToAddress=new Array();
	
	
	//Second Array a_BillToAddress
	
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		var a_ItemArray=new Array();
		var a_ItemAmount=new Array();
		var a_ItemTaxAmt=new Array();
		var a_SplitItemArray=new Array();
			
		var s_PayeeAddress='';					
		a_SplitBillToAddress=a_BillToAddress[a].split('#');
		nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_BillToAddress['+a+']=' + a_BillToAddress[a]);
		
		if(a_SplitBillToAddress[0]!=null)
		{
			if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
			{
				s_PayeeAddress=a_SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);			    	
			}
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
	        //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
	        
			//if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			//{
			//	s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		    //    nlapiLogExecution('DEBUG', 'functionConsolidated', 's_PayeeAddress['+i+']=' + s_PayeeAddress);			    	
			//}
			//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitBillToAddress Length=' + a_SplitBillToAddress.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitBillToAddress.length;n++)
			{							
				nlapiLogExecution('DEBUG', 'functionConsolidated', 'Invoice ID====' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
	            //nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
	            if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'functionConsolidated', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{								
					//Invoice Line Count
	                for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
												
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
												
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
												
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
				//=========================================================================================================								
						if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
						{
							//*************************************************************************************
					        var i_arrayLength=a_ItemArray.length;
							if(i_arrayLength==0)
							{
								a_ItemArray[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
							}
							if(i_arrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemArray.length;p++)
								{								
									a_SplitItemArray=a_ItemArray[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemArray[p]=a_ItemArray[p]+'#'+s_InvoiceItemQuantity;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemArray[a_ItemArray.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
								}
							}//if(i_arrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
											
						if(s_InvoiceItemAmount!=null && s_InvoiceItemAmount!='')
						{
							//*************************************************************************************
					        var i_QtyarrayLength=a_ItemArray.length;
							if(i_QtyarrayLength==0)
							{
								a_ItemAmount[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemAmount;
							}
							if(i_QtyarrayLength!=0)
							{
								var i_flag=0;
								//var a_SplitArray=new Array();
								for(var p=0;p<a_ItemAmount.length;p++)
								{								
									a_SplitItemArray=a_ItemAmount[p].split('#');
									if(a_SplitItemArray[0]==s_InvoiceItemValue)
									{
										a_ItemAmount[p]=a_ItemAmount[p]+'#'+s_InvoiceItemAmount;
										i_flag=1;
										break;
									}
								}
								if(i_flag!=1)
								{
									a_ItemAmount[a_ItemAmount.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemAmount;
								}
							}//if(i_QtyarrayLength!=0)							
					       			
						
						}//if(s_InvoiceItemValue!=null && s_InvoiceItemValue!='')
	           //=========================================================================================================								
				
					
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}
						*/
						if(s_TherapistID!=null && s_TherapistID!='' && s_TherapistID!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_TherapistID
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+'<br/>'+s_TherapistID;
							}										
						}
						//nlapiLogExecution('DEBUG', 'functionConsolidated', 'a_SplitBillToAddress[n] ==' + a_SplitBillToAddress[n];);
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);																		
						a_InvoiceID[i_ArrCount]=a_SplitBillToAddress[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						
					}//for
	             //nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + r_TotalAmount);
				}//if
					
				}//if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')						
			}//for(var n=1;n<a_SplitBillToAddress.length;n++)		
			
			
		}//if(a_SplitBillToAddress[0]!=null)
		
	    // loop for count each Item quantity
		
		for(var t=0;t<a_ItemArray.length;t++)
		{
			var a_ItemName=new Array();
			var a_ItemValue=new Array();
			var a_ItemQuantity=new Array();
			var a_ItemTotalQuantity=new Array();
			var a_SplitItemArrayQuantity=new Array();			
			
			var a_ArrayItemAmount=new Array();
			var a_TotalItemAmount=new Array();
			var a_SpliteArrayAmount=new Array();
			var i_count=0;
			a_SplitItemArrayQuantity=a_ItemArray[t].split('#');
			a_SpliteArrayAmount=a_ItemAmount[t].split('#');
			if (a_SplitItemArrayQuantity[0] != null) 
			{
				a_ItemName[i_count]=a_SplitItemArrayQuantity[0];
				//a_ItemValue[i_count]=a_SplitItemArrayQuantity[1];
				for (var u = 2; u < a_SplitItemArrayQuantity.length; u++) 
				{
					i_count=i_count+1;
					a_ItemQuantity=a_SplitItemArrayQuantity[u];
					a_ItemTotalQuantity=a_ItemTotalQuantity+parseFloat(a_ItemQuantity);						
									
				}							
			}
			if (a_SpliteArrayAmount[0] != null) 
			{
				//a_ItemName[i_count]=a_SpliteArrayAmount[0];
				//a_ItemValue[i_count]=a_SplitItemArrayQuantity[1];
				for (var u = 2; u < a_SpliteArrayAmount.length; u++) 
				{
					//i_count=i_count+1;
					a_ArrayItemAmount=a_SpliteArrayAmount[u];
					a_TotalItemAmount=a_TotalItemAmount+parseFloat(a_ArrayItemAmount);						
									
				}							
			}
			
            /*
for(var q=0;q<a_ItemName.length;q++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemName', '***a_ItemName ['+q+']==' + a_ItemName[q]);
			}
			for(var e=0;e<a_ItemTotalQuantity.length;e++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemTotalQuantity', '***a_ItemTotalQuantity ['+e+']==' + a_ItemTotalQuantity[e]);
			}
			for(var h=0;h<a_TotalItemAmount.length;h++)
			{
				nlapiLogExecution('DEBUG', 'a_TotalItemAmount', '****a_TotalItemAmount ['+h+']==' + a_TotalItemAmount[h]);
			}

*/
			/*
for(var b=0;b<a_ItemAmount.length;b++)
			{
				nlapiLogExecution('DEBUG', 'a_ItemName', '***a_ItemAmount ['+b+']==' + a_ItemAmount[b])
			}
*/
			//function call for Consolidated for array BilltoAddress
			ConsolidatePrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_ItemName,a_ItemTotalQuantity,a_ItemTax,a_TotalItemAmount,r_TotalAmount,r_AllAmountDue)
		}
		
		
		for(var w=0;w<a_ItemArray.length;w++)
		{
			nlapiLogExecution('DEBUG', 'onBeforeLoad', '***a_ItemArray ['+w+']==' + a_ItemArray[w]);
		}
		//Generate PDF		
		
		//nlapiLogExecution('DEBUG', 'functionConsolidated', 'TotalAmountInSecondArray==' + r_TotalAmount);			
		//var i_array=2;
		//summaryPrintPdf(s_AsOfDate,s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
	}
}//END OF FOR
// END OF CONSOLIDATED

// function for seperate array elements



/*function SortArrayElements()
{
	//*************************************************************************************
    var i_arrayLength=a_ItemArray.length;
	if(i_arrayLength==0)
	{
		a_ItemArray[0]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
	}
	if(i_arrayLength!=0)
	{
		var i_flag=0;
		//var a_SplitArray=new Array();
		for(var p=0;p<a_ItemArray.length;p++)
		{								
			a_SplitItemArray=a_ItemArray[p].split('#');
			if(a_SplitItemArray[0]==s_InvoiceItemValue)
			{
				a_ItemArray[p]=a_ItemArray[p]+'#'+s_InvoiceItemQuantity;
				i_flag=1;
				break;
			}
		}
		if(i_flag!=1)
		{
			a_ItemArray[a_ItemArray.length]=s_InvoiceItemText+'#'+s_InvoiceItemValue+'#'+s_InvoiceItemQuantity;
		}
	}//if(i_arrayLength!=0)							
					       			
}
*/


//**********************************************************************************************************
                    //   FUNCTION FOR SUMMARIZE

//**********************************************************************************************************




function FunctionSummarize(a_StoredArray,a_BillToAddress,s_customerName,i_customerID)
{
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'IN FunctionSummarize' );
	var a_SplitStoredArray=new Array();
	nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_StoredArray Length=' + a_StoredArray.length);
				
	for(s=0;s<a_StoredArray.length;s++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
				
		var s_PayeeAddress='';					
		a_SplitStoredArray=a_StoredArray[s].split('#');
		
		if(a_SplitStoredArray[0]!=null)
		{
			var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitStoredArray[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(o_LoadPayerRec!=null && o_LoadPayerRec!='')
			{
				s_PayeeAddress=o_LoadPayerRec.getFieldValue('custrecord_tjinc_bbonbf_payeraddress');
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress=' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray Length=' + a_SplitStoredArray.length);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitStoredArray.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitStoredArray['+n+']=' + a_SplitStoredArray[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitStoredArray[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
												
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate+', '+s_InvoiceDate;
						}	
						*/
						if(s_TherapistID!=null && s_TherapistID!='' && s_TherapistID!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_TherapistID
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+'<br/>'+s_TherapistID;
							}										
						}
										
						r_AllAmountDue=r_AllAmountDue + parseFloat(r_AmountRemaining);
						a_InvoiceID[i_ArrCount]=a_SplitStoredArray[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						nlapiLogExecution('DEBUG', 'FunctionSummarize', '*****s_InvoiceItemQuantity==*****==' + s_InvoiceItemQuantity);
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//r_TotalAmount=r_TotalAmount+parseFloat(s_InvoiceItemAmount);
					}
                 
				}
					
				}						
			}	//for n	
			
		}//if
		//Generate PDF
		nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);		
		var i_array=1;
		summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
		
	}//for
	
	

	
//Second Array a_BillToAddress
    var a_SplitBillToAddress=new Array();
	var a_ItemArray=new Array();
	var a_SplitItemArray=new Array();
	for(a=0;a<a_BillToAddress.length;a++)
	{
		var a_InvoiceDate=new Array();
		var a_InvoiceID=new Array();
		var a_ServiceDate=new Array();
		var a_InvoiceItems=new Array();
		var a_InvItemQuantity=new Array();
		var a_ItemDescription=new Array();
		var a_Comments=new Array();
		var a_ItemTax=new Array();
		var a_Amount=new Array();
		var i_ArrCount=0;
		var r_TotalAmount=0;	
		//var s_AsOfDate='';
		var s_AllPrimaryTherapist='';
		var r_AllAmountDue=0;
		
			
		var s_PayeeAddress='';					
		a_SplitBillToAddress=a_BillToAddress[a].split('#');
		
		if(a_SplitBillToAddress[0]!=null)
		{
			//var o_LoadPayerRec=nlapiLoadRecord('customrecord_tjinc_bbonbf_payercustom', a_SplitBillToAddress[0]);
            //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadPayerRec['+i+']=' + o_LoadPayerRec);
            
			if(a_SplitBillToAddress[0]!=null && a_SplitBillToAddress[0]!='')
			{
				s_PayeeAddress=a_SplitBillToAddress[0];
		        nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_PayeeAddress== ' + s_PayeeAddress);			    	
			}
			//nlapiLogExecution('DEBUG', 'getitemtype', '******Final a_SplitBillToAddress******' + a_SplitBillToAddress[0]);
			//Get The Data from Invoice
			for(var n=1;n<a_SplitBillToAddress.length;n++)
			{							
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'a_SplitBillToAddress['+n+']=' + a_SplitBillToAddress[n]);
				
				var o_LoadInvoiceRec=nlapiLoadRecord('invoice', a_SplitBillToAddress[n]);
                //nlapiLogExecution('DEBUG', 'FunctionSummarize', 'o_LoadInvoiceRec['+n+']=' + o_LoadInvoiceRec);
				
                if(o_LoadInvoiceRec!=null && o_LoadInvoiceRec!='')
				{
			    var s_InvoiceDate=o_LoadInvoiceRec.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceDate['+i+']=' + s_InvoiceDate);
		    			
				var s_ServiceDate=o_LoadInvoiceRec.getFieldValue('custbody2');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_ServiceDate['+i+']=' + s_ServiceDate);
		    	
				var i_LineItemCount = o_LoadInvoiceRec.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'i_LineItemCount ==' + i_LineItemCount);
				
				var s_InvoiceSalesRep = o_LoadInvoiceRec.getFieldValue('salesrep');
				nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceSalesRep ==' + s_InvoiceSalesRep);
				
				var s_TherapistID='';
				if (s_InvoiceSalesRep != null && s_InvoiceSalesRep != '') 
				{
					var i_PTherapistLineItemCount = o_LoadInvoiceRec.getLineItemCount('salesteam');
					nlapiLogExecution('DEBUG', 'functionConsolidated', 'i_PTherapistLineItemCount ==' + i_PTherapistLineItemCount);
					
					for (var f = 1; i_PTherapistLineItemCount != null && f <= i_PTherapistLineItemCount; f++) 
					{
						var s_InvoiceIsPrimaryValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'isprimary', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceIsPrimaryValue ==' + s_InvoiceIsPrimaryValue);
						
						var s_InvoiceSalesRoleValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'salesrole', f);
						nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceSalesRoleValue==' + s_InvoiceSalesRoleValue);
												
						if (s_InvoiceIsPrimaryValue == 'T' && s_InvoiceSalesRoleValue==-2) 
						{
							var s_InvoiceEmployeeValue = o_LoadInvoiceRec.getLineItemValue('salesteam', 'employee', f);
							nlapiLogExecution('DEBUG', 'functionConsolidated', 's_InvoiceEmployeeValue ==' + s_InvoiceEmployeeValue);
							
							if (s_InvoiceEmployeeValue != null && s_InvoiceEmployeeValue != '') 
							{
								var o_LoadSalesRepRecord = nlapiLoadRecord('employee', s_InvoiceEmployeeValue);
								nlapiLogExecution('DEBUG', 'functionConsolidated', 'o_LoadSalesRepRecord ==' + o_LoadSalesRepRecord);
								
								if (o_LoadSalesRepRecord != null && o_LoadSalesRepRecord != '') 
								{
									s_TherapistID = o_LoadSalesRepRecord.getFieldValue('custentity_therapist_id');
									nlapiLogExecution('DEBUG', 'functionConsolidated', 's_TherapistID ==' + s_TherapistID);
									
								}
							}
						}
						
					}
				}
				var r_AmountRemaining = o_LoadInvoiceRec.getFieldValue('amountremaining');
				//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'r_AmountRemaining ==' + r_AmountRemaining);
				
				if (i_LineItemCount != null && i_LineItemCount != '') 
				{
					
					
					//Invoice Line Count
                    for (var j = 1; j <= i_LineItemCount; j++) 
					{
			
						var s_InvoiceItemText = o_LoadInvoiceRec.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemText ['+j+']==' + s_InvoiceItemText);
						
						var s_InvoiceItemValue = o_LoadInvoiceRec.getLineItemValue('item', 'item', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemValue ['+j+']==' + s_InvoiceItemValue);
						
						var s_InvoiceItemQuantity = o_LoadInvoiceRec.getLineItemValue('item', 'quantity', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemQuantity ['+j+']==' + s_InvoiceItemQuantity);
						
						//Comments are the Item Fullfillment Notes
						var s_InvoiceItemComments = o_LoadInvoiceRec.getLineItemValue('item', 'custcol_tjinc_bbonbf_textcolumn', j);
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemComments ['+j+']==' + s_InvoiceItemComments);
								
						var s_InvoiceItemDescription = o_LoadInvoiceRec.getLineItemValue('item', 'description', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemDescription ['+j+']==' + s_InvoiceItemDescription);
						
						var s_InvoiceItemAmount = o_LoadInvoiceRec.getLineItemValue('item', 'amount', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceItemAmount ['+j+']==' + s_InvoiceItemAmount);
						
						var s_InvoiceITax = o_LoadInvoiceRec.getLineItemValue('item', 'taxrate1', j);
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_InvoiceITax ['+j+']==' + s_InvoiceITax);
						
						i_ArrCount=i_ArrCount+1;
						a_InvoiceDate[i_ArrCount]=s_InvoiceDate;
						/*
						if(s_AsOfDate=='')
						{
							s_AsOfDate=''+s_InvoiceDate;
						}									
						else
						{
							s_AsOfDate=s_AsOfDate+', '+s_InvoiceDate;
						}
						*/
						if(s_TherapistID!=null && s_TherapistID!='' && s_TherapistID!='undefined')
						{
							if(s_AllPrimaryTherapist=='')
							{
								s_AllPrimaryTherapist=''+s_TherapistID
							}
							else
							{
								s_AllPrimaryTherapist=s_AllPrimaryTherapist+', '+'<br/>'+s_TherapistID;
							}										
						}
						nlapiLogExecution('DEBUG', 'FunctionSummarize', 's_AllPrimaryTherapist ==' + s_AllPrimaryTherapist);
						r_AllAmountDue=r_AllAmountDue+parseFloat(r_AmountRemaining);																		
						a_InvoiceID[i_ArrCount]=a_SplitBillToAddress[n];
						a_ServiceDate[i_ArrCount]=s_ServiceDate;
						a_InvoiceItems[i_ArrCount]=s_InvoiceItemText;
						nlapiLogExecution('DEBUG', 'FunctionSummarize', '*****s_InvoiceItemQuantity==*****' + s_InvoiceItemQuantity);
						a_InvItemQuantity[i_ArrCount]=parseFloat(s_InvoiceItemQuantity);
						a_ItemDescription[i_ArrCount]=s_InvoiceItemDescription;
						a_Comments[i_ArrCount]=s_InvoiceItemComments;
						a_ItemTax[i_ArrCount]=s_InvoiceITax
						a_Amount[i_ArrCount]=s_InvoiceItemAmount;
						if (s_InvoiceItemAmount != null && s_InvoiceItemAmount != '') 
						{
							r_TotalAmount = r_TotalAmount + parseFloat(s_InvoiceItemAmount);
						}
						//nlapiLogExecution('DEBUG', 'FunctionSummarize', 'TotalAmountInSecondArray==' + r_TotalAmount);
					}
                 
				}
					
				}						
			}		
			
		}
		//Generate PDF
		
		
			
		
		nlapiLogExecution('DEBUG', 'getitemtype', 'r_AllAmountDue==' + r_AllAmountDue);			
		var i_array=2;
		summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
		
	}
	//for(a=0;a<a_ItemArray.length;a++)
	//{
	//	nlapiLogExecution('DEBUG', 'getitemtype', 'a_ItemArray['+a+']=' + a_ItemArray[a]);
	//}
}  //End FOR

//    END OF FUNCTION SUMMARIZE


///////////////// ------------- Summary Print ----------------------

function summaryPrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_InvoiceID,a_ServiceDate,a_InvoiceItems,a_InvItemQuantity,a_ItemDescription,a_Comments,a_ItemTax,a_Amount,r_TotalAmount,r_AllAmountDue,i_array)
{
	
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='SI_'+i_customerID+'_'+timeStamp;
	}
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\"><h2>Summary Invoice<\/h2><\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">";
strVar += "		<p>";
strVar += "		52 Victoria Street<br\/>";
strVar += "		Aurora ON L4G 1R2<br\/>";
strVar += "		Canada<br\/>";
strVar += "		Ph: 905-751-0970<br\/>";
strVar += "		<a href=\"http:\/\/www.blue-balloon.com\">www.blue-balloon.com<\/a>";
strVar += "		<\/p>		";
//strVar += "		"+CompanyAddrass+"<\/td>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"380\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" width=\"230\" font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td  align=\"left\" width=\"150\" font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td align=\"left\" width=\"250\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"100%\">";
strVar += "	      <tr>";
strVar += "	        <td >&nbsp;<\/td>";
strVar += "			";
strVar += "			<td font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td >&nbsp;<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+s_AllPrimaryTherapist+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
strVar += "		<tr background-color=\"#000000\">";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";
var pageBreak=7;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;
 for (var k = 1; a_InvoiceDate != null && k < a_InvoiceDate.length; k++) 
 {
 	 
	if(flagCheck2==1)
	{
	 strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
	    strVar += "		<tr background-color=\"#000000\">";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";	
	 flagCheck2=0;	
	}
	if(flagCheck3==1)
	{
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "		<tr background-color=\"#000000\">";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";
		flagCheck3=0;	
	}
 	strVar += "		<tr >";
 	if (a_InvoiceDate[k] != null && a_InvoiceDate[k] !='NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">" + a_InvoiceDate[k] + "<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_InvoiceID[k] != null && a_InvoiceID[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_InvoiceID[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_ServiceDate[k] != null && a_ServiceDate[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_ServiceDate[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_InvoiceItems[k] != null && a_InvoiceItems[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_InvoiceItems[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
 	if (a_InvItemQuantity[k] != null && a_InvItemQuantity[k] != 'NaN' && a_InvItemQuantity[k] !='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_InvItemQuantity[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (a_ItemDescription[k] != null && a_ItemDescription[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_Comments[k] != null && a_Comments[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_Comments[k]+"<\/td>";
		//strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
 	
	if (a_ItemTax[k] != null && a_ItemTax[k] != 'NaN') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+nlapiEscapeXML(a_ItemTax[k])+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (a_Amount[k] != null && a_Amount[k] != 'NaN' && a_Amount[k] != '') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_Amount[k]+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	} 	
 	strVar += "			<\/tr>";
	
   cnt++;
	
	if(k >pageBreak && flag==0)
	{
		strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		
		//if(a_InvoiceDate.length>k)
	
		//strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";	
		// strVar += "	<tr>";
		// strVar += "	<td>";
		pageBreak=k;
		cnt=0;
		flag=1;
		flagCheck2=1;
	}
	else if(flag==1)
	{
		if(cnt==10 && a_InvoiceDate.length-1!=k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			strVar += "<p style=\"page-break-before: always\">";
			//strVar += "		<table>";
			var flagCheck3=1;
			cnt=0;
		}
		/*else if(a_InvoiceDate.length-1==k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			
		}
		flagCheck=1;
		*/
	}//if
	
  
	
	
 }//for flagCheck
 
        /*strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">Total<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/p>";
		*/
 
if(flag==0)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', '****flag==0*****'+flag);
	//strVar += "		<\/td>";
	//strVar += "		<\/tr>";
	/*strVar += "  <tr >";
	strVar += "   <td colspan=\"7\"><\/td>  ";
	strVar += "   <td >Amount<\/td>   ";
	strVar += "   <td >11222<\/td>  ";
	strVar += "   <\/tr>";
	
	strVar += "		<\/table>";
	strVar += "<p style=\"page-break-before: always\">";
	strVar += "		<table>"; //colspan
	*/
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";
	//strVar += "<\/p>";
	strVar += "	<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
}//if
else if(flag==1)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' ####### flag==1 @@@@@@@@@='+flag);
	
	
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "	  <td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";	
	
	strVar += "<\/p>";
	//strVar += "		<\/table>";
	
}//else if


/*
strVar += "		<table class=\"textreg\" border=\"0.1pt\" border-color=\"silver\" width=\"20%\" align =\"right\" >";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Total<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Amount Due<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<\/table>";
*/
//strVar += "	<\/td>";
//strVar += "	<\/tr>";
//strVar += "<\/table>";


			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			//nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//----------------------------------
			
			

}//function 



///////////////// ------------- Consolidate Print ----------------------

function ConsolidatePrintPdf(s_AllPrimaryTherapist,s_PayeeAddress,s_customerName,i_customerID,a_InvoiceDate,a_ItemName,a_ItemTotalQuantity,a_ItemTax,a_TotalItemAmount,r_TotalAmount,r_AllAmountDue)         
{	
	var d_currentTime = new Date();
	var month = d_currentTime.getMonth() + 1
	var day = d_currentTime.getDate()
	var year = d_currentTime.getFullYear()
	var d_CurrantDate=month + "/" + day + "/" + year;
	var currentdateObj=new Date();
	var timeStamp=currentdateObj.getTime();
	if(i_customerID!=null && i_customerID!='')
	{
		//var s_PDFName='SummaryInvoice_'+i_customerID+'_'+d_CurrantDate+'('+i_array+')';
		var s_PDFName='CI_'+i_customerID+'_'+timeStamp;
	}
	
	var context = nlapiGetContext();
	var CompanyAddrass=context.getCompany();
	
	var strVar="";
strVar += "";
strVar += "<table>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">		";
strVar += "		<table width=\"100%\" align =\"right\">";
strVar += "			<tr>";
strVar += "   <td width=\"248\"> <img align=\"left\" height=\"60\" width=\"60\" src=\"" +nlapiEscapeXML("https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f")+"\"><\/img><\/td>";
//strVar += "			<td width=\"248\"><img  align =\"left\"  src =\"https:\/\/system.sandbox.netsuite.com\/core\/media\/media.nl?id=342&c=396105&h=15c034fd8528af5b219f\"><\/td>";
strVar += "			<td>&nbsp;<\/td>";
strVar += "				<td width=\"206\"><h2>Summary Invoice<\/h2><\/td>";
strVar += "			<\/tr>";
strVar += "		<\/table>			";
strVar += "		";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	";
strVar += "	<tr>";
strVar += "		<td >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\">";
strVar += "		<p>";
strVar += "		52 Victoria Street<br\/>";
strVar += "		Aurora ON L4G 1R2<br\/>";
strVar += "		Canada<br\/>";
strVar += "		Ph: 905-751-0970<br\/>";
strVar += "		<a href=\"http:\/\/www.blue-balloon.com\">www.blue-balloon.com<\/a>";
strVar += "		<\/p>		";
//strVar += "		"+CompanyAddrass+"<\/td>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >&nbsp;<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td width=\"1327\" >";
strVar += "		<table border=\"1\" width=\"380\" >";
strVar += "	      <tr >";
strVar += "			<td  align=\"left\" width=\"230\" font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Bill To<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td  align=\"left\" width=\"150\" font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">Patient Name<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td align=\"left\" width=\"250\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" >"+nlapiEscapeXML(s_PayeeAddress)+"<\/td>";
//strVar += "			<td>&nbsp;<\/td>";
strVar += "			<td align=\"left\" width=\"150\" border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+s_customerName+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "	     ";
strVar += "	     <table width=\"100%\">";
strVar += "	      <tr>";
strVar += "	        <td >&nbsp;<\/td>";
strVar += "			";
strVar += "			<td font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			As Of Date<\/td>";
strVar += "			<td font-size=\"13\" font-weight=\"bold\" color=\"#FFFFFF\" background-color=\"#000000\">";
strVar += "			Therapist ID<\/td>";
strVar += "	      <\/tr>";
strVar += "	       <tr >";
strVar += "			<td >&nbsp;<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+d_CurrantDate+"<\/td>";
strVar += "			<td border=\"0.1pt\" font-size=\"9\" border-color=\"#000000\">"+s_AllPrimaryTherapist+"<\/td>";
strVar += "	      <\/tr>";
strVar += "	     <\/table>";
strVar += "		<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "	<td>";
strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
strVar += "		<tr background-color=\"#000000\">";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
strVar += "		<\/tr>";
var pageBreak=7;
var flag=0;
var cnt=0;
var flagCheck=0;
var flagCheck2=0;
var flagCheck3=0;
 for (var k = 0; a_ItemName != null && k < a_ItemName.length; k++) 
 {
 	 
	if(flagCheck2==1)
	{
	 strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
	    strVar += "		<tr background-color=\"#000000\">";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";	
	 flagCheck2=0;	
	}
	if(flagCheck3==1)
	{
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "		<tr background-color=\"#000000\">";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Date<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Invoice #<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Service Date<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Item<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Quantity<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Description<\/td>";
		//strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Comments<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Tax<\/td>";
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" color=\"#FFFFFF\">Amount<\/td>";
		strVar += "		<\/tr>";
		flagCheck3=0;	
	}
 	strVar += "		<tr >";
 	/*
if (a_InvoiceDate[k] != null && a_InvoiceDate[k] !='NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">" + a_InvoiceDate[k] + "<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_InvoiceID[k] != null && a_InvoiceID[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_InvoiceID[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
	if (a_ServiceDate[k] != null && a_ServiceDate[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_ServiceDate[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
*/
 	if (a_ItemName[k] != null && a_ItemName[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_ItemName[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
 	if (a_ItemTotalQuantity[k] != null && a_ItemTotalQuantity[k] != 'NaN' && a_ItemTotalQuantity[k] !='') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_ItemTotalQuantity[k]+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	/*
if (a_ItemDescription[k] != null && a_ItemDescription[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+nlapiEscapeXML(a_ItemDescription[k])+"<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\"><\/td>";
	}
	
 	if (a_Comments[k] != null && a_Comments[k] != 'NaN') 
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\">"+a_Comments[k]+"<\/td>";
		//strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
	else
	{
		strVar += "<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"left\">Test comments<\/td>";
	}
*/
 	
	if (a_ItemTax[k] != null && a_ItemTax[k] != 'NaN') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	}
	
 	if (a_TotalItemAmount[k] != null && a_TotalItemAmount[k] != 'NaN' && a_TotalItemAmount[k] != '') 
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\">"+a_TotalItemAmount[k]+"<\/td>";
	}
	else
	{
		strVar += "			<td border=\"0.1pt\" border-color=\"#000000\" font-size=\"9\" align =\"right\"><\/td>";
	} 	
 	strVar += "			<\/tr>";
	
   cnt++;
	
	if(k >pageBreak && flag==0)
	{
		strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		
		//if(a_InvoiceDate.length>k)
	
		//strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";	
		// strVar += "	<tr>";
		// strVar += "	<td>";
		pageBreak=k;
		cnt=0;
		flag=1;
		flagCheck2=1;
	}
	else if(flag==1)
	{
		if(cnt==10 && a_InvoiceDate.length-1!=k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			strVar += "<p style=\"page-break-before: always\">";
			//strVar += "		<table>";
			var flagCheck3=1;
			cnt=0;
		}
		/*else if(a_InvoiceDate.length-1==k)
		{
			strVar += "		<\/table>";
			strVar += "<\/p>";
			
		}
		flagCheck=1;
		*/
	}//if
	
  
	
	
 }//for flagCheck
 
        /*strVar += "		<\/table>";
		strVar += "	<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<p style=\"page-break-before: always\">";
		strVar += "	    <table border=\"0.1pt\" width=\"100%\" border-color=\"#000000\">";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">Total<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "	<tr>";
		strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>  ";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
		strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
		strVar += "	<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/p>";
		*/
 
if(flag==0)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', '****flag==0*****'+flag);
	//strVar += "		<\/td>";
	//strVar += "		<\/tr>";
	/*strVar += "  <tr >";
	strVar += "   <td colspan=\"7\"><\/td>  ";
	strVar += "   <td >Amount<\/td>   ";
	strVar += "   <td >11222<\/td>  ";
	strVar += "   <\/tr>";
	
	strVar += "		<\/table>";
	strVar += "<p style=\"page-break-before: always\">";
	strVar += "		<table>"; //colspan
	*/
	strVar += "	<tr>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr >";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";
	//strVar += "<\/p>";
	strVar += "	<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	
	
}//if
else if(flag==1)
{
	nlapiLogExecution('DEBUG', 'PrintXmlToPDF', ' ####### flag==1 @@@@@@@@@='+flag);
	
	
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "   <td rowspan=\"2\">&nbsp;<\/td>";
	strVar += "	  <td border=\"0.1pt\" border-color=\"#000000\" >Total<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "   <td border=\"0.1pt\" border-color=\"#000000\" colspan=\"7\"><\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\" >Amount Due<\/td>";
	strVar += "		<td border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "		<\/table>";	
	
	strVar += "<\/p>";
	//strVar += "		<\/table>";
	
}//else if


/*
strVar += "		<table class=\"textreg\" border=\"0.1pt\" border-color=\"silver\" width=\"20%\" align =\"right\" >";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Total<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_TotalAmount+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<tr>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\" width=\"65\">Amount Due<\/td>";
strVar += "		<td class=\"textreg\" border=\"0.1pt\" border-color=\"#000000\">"+r_AllAmountDue+"<\/td>";
strVar += "	<\/tr>";
strVar += "	<\/table>";
*/
//strVar += "	<\/td>";
//strVar += "	<\/tr>";
//strVar += "<\/table>";


			nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'strVar='+strVar);
			// build up BFO-compliant XML using well-formed HTML
			
					
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>\n<body>";
			xml += strVar;
			xml += "</body>\n</pdf>";
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
			// run the BFO library to convert the xml document to a PDF 
			var file = nlapiXMLToPDF( xml );
			//nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'file='+file);
			
			var fileObj=nlapiCreateFile(s_PDFName+'.pdf', 'PDF', file.getValue());
			nlapiLogExecution('DEBUG', 'PDF', ' PDF fileObj='+ fileObj);
			fileObj.setFolder(217830);
			var fileId=nlapiSubmitFile(fileObj);
			//nlapiLogExecution('DEBUG', 'PDF', 'PDF fileId='+ fileId);
	      
			//----------------------------------
			
			

}//function 


