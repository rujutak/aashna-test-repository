/**
* Copyright (c) 2012 Trajectory Group Inc. / Kuspide Canada Inc.
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: BlueBallon
* @Company: Trajectory Inc.  
* @CreationDate: 20120601
* @DocumentationUrl:  https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js
* @NamingStandard: TJINC_NSJ-1-2
*/

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_BBONBF_FulfillToInvoice
function TJINC_BBONBF_FulfillToInvoice(){
	
	nlapiLogExecution('AUDIT','A_FTI_1 Running TJINC_BBONBF_FulfillToInvoice.','Units: '+nlapiGetContext().getRemainingUsage());
	
	var s_EmailAuthor 	= '-5';
	var s_EmailRecipi 	= 'csanchez@trajectoryinc.com';
	var s_EmailSubjct 	= 'Invoice Engine Automation';
	var i_MinUnits 		= 200;
	var i_lastFulfillId = 0;
	var b_executionPrc	= true;
	var s_invoiceIds	= '';
	var s_errorMsg		= '';
	
	//Comment 1 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-1
	if(nlapiGetContext().getDeploymentId() != 'customdeploy1'){
		
		var i_temLastFulfillId  = nlapiGetContext().getSetting('SCRIPT','custscript_tjinc_bbonbf_lastitemfulfill');
		if(isNotNull(i_temLastFulfillId)) i_lastFulfillId = i_temLastFulfillId;
		
		s_invoiceIds = nlapiGetContext().getSetting('SCRIPT','custscript_tjinc_bbonbf_invoices');
		s_errorMsg = nlapiGetContext().getSetting('SCRIPT','custscript_tjinc_bbonbf_errormsg');
		
		nlapiLogExecution('DEBUG','A_FTI_2 Script :'+nlapiGetContext().getDeploymentId(),'i_temLastFulfillId: '+i_temLastFulfillId+' s_invoiceIds: '+s_invoiceIds.length+' s_errorMsg: '+s_errorMsg.length );
	}
	
	//Comment 2 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-2
	while(b_executionPrc && (nlapiGetContext().getRemainingUsage() > i_MinUnits)){
		
		var o_searchInformation = TJINC_BBONBF_GetItemFulfillment(i_lastFulfillId, true);
		var o_fulfillments = o_searchInformation.itemfulfillments;
		var o_clinlTeanInf = o_searchInformation.clinicalteam;
		
		if(isNotJsonNull(o_fulfillments) && isNotJsonNull(o_clinlTeanInf)){
			
			for (var i_index in o_fulfillments) {
				
				if(nlapiGetContext().getRemainingUsage() > i_MinUnits && (isNotNull(o_clinlTeanInf[i_index]))){
					
					var i_fulfillId = o_fulfillments[i_index].id;
					i_lastFulfillId = i_fulfillId;
					var i_soid 		= o_fulfillments[i_index].soid;
					var s_tranDate 	= o_fulfillments[i_index].trandate;
					var s_postPerd 	= o_fulfillments[i_index].postperiod;
					var i_locatnid 	= o_fulfillments[i_index].locationid;
					var s_memo 		= o_fulfillments[i_index].memo;
					var s_serType 	= o_fulfillments[i_index].servicetype;
					var s_serDtType = o_fulfillments[i_index].servTypedate;
					
					nlapiLogExecution('DEBUG','A_FTI_3.'+i_index+' Before Create Invoice.','Fulfill Id: '+i_fulfillId+' SO Id: '+i_soid+' s_tranDate: '+s_tranDate+' s_postPerd: '+s_postPerd+' i_locatnid: '+i_locatnid +' s_serType: '+s_serType);
					
					var o_invResult = TJINC_CreateaInvoiceBySalesOrder(i_fulfillId, i_soid, o_fulfillments[i_index].items, s_tranDate, s_postPerd, i_locatnid, s_memo, s_serType, s_serDtType, o_clinlTeanInf[i_fulfillId].members);
					
					if(o_invResult.status == '1'){
						
						nlapiLogExecution('DEBUG','A_FTI_4.'+i_index+' After Create Invoice SUCCESS.','Item Fulfillment Id: '+o_fulfillments[i_index].id+' INVOICE Id : '+o_invResult.invid);
						if(isNull(s_invoiceIds)) s_invoiceIds = 'Item Fulfillment : '+i_fulfillId+' Invoice Id: '+o_invResult.invid;
						else s_invoiceIds = s_invoiceIds+' - '+' Item Fulfillment : '+i_fulfillId+' Invoice Id: '+o_invResult.invid;
					}
					else{
						
						nlapiLogExecution('DEBUG','A_FTI_5.'+i_index+' After Create Invoice ERROR.','Item Fulfillment Id: '+o_fulfillments[i_index].id);
						
						if(isNull(s_invoiceIds))s_errorMsg = 'Item Fulfillment : '+i_fulfillId+' Error Msg: '+o_invResult.msg;
						else s_errorMsg = s_errorMsg+' - '+' Item Fulfillment : '+i_fulfillId+' Error Msg: '+o_invResult.msg;
					}
				}
			}
		}
		else b_executionPrc = false;
	}
	
	//Comment 3 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-3
	if(nlapiGetContext().getRemainingUsage() < i_MinUnits){
		
		nlapiLogExecution('DEBUG','A_FTI_6 Before Schedule Script.','Units: '+nlapiGetContext().getRemainingUsage());
		
		TJINC_DeployScriptFulfillToInvoice(i_lastFulfillId, s_invoiceIds, s_errorMsg);
		
		nlapiLogExecution('DEBUG','A_FTI_7 After Schedule Script.','Units: '+nlapiGetContext().getRemainingUsage());
	}
	else{
		//Comment 4 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-4
		nlapiLogExecution('DEBUG','A_FTI_8 Before Send Email.','Units: '+nlapiGetContext().getRemainingUsage());
		
		var s_invMsg = '';
		var s_errMsg = '';
		
		if(isNotNull(s_invoiceIds)) s_invMsg = 'Invoice Created by the Process:<br>'+s_invoiceIds;
			
		if(isNotNull(s_errorMsg	)) s_errorMsg = 'Error(s) on the Process:<br>'+s_errorMsg;
		
		nlapiSendEmail(s_EmailAuthor, s_EmailRecipi, s_EmailSubjct, 'Dear User,<br>The process was completed. <br>'+s_invMsg+'<br> '+s_errorMsg, null, null, null, null);
	
		nlapiLogExecution('DEBUG','A_FTI_9 After Send Email.','Units: '+nlapiGetContext().getRemainingUsage());
	}
	
	nlapiLogExecution('AUDIT','A_FTI_10 Completed TJINC_BBONBF_FulfillToInvoice.','Units: '+nlapiGetContext().getRemainingUsage());
}

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_BBONBF_GetItemFulfillment
function TJINC_BBONBF_GetItemFulfillment(i_lastFulfillId, b_normalExecution){
	
	nlapiLogExecution('AUDIT','B_GIF_1 Running TJINC_BBONBF_GetItemFulfillment.','Units: '+nlapiGetContext().getRemainingUsage());
	
	var o_itemFulfills 	= {};
	var o_clinicalTeam	= {};
	var b_executeSearch = true;
	var b_searchType	= false;
	var s_itemFulfillments 	= '';
	var a_itemfulfillIds 	= new Array();
	
	while(b_executeSearch){
		
		nlapiLogExecution('DEBUG','B_GIF_2 Before Search','b_searchType: '+b_searchType+' i_lastFulfillId: '+i_lastFulfillId);
		//Comment 1 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-11
		var o_itemFulFillments = TJINC_ItemfulfillmentSearch(b_searchType, i_lastFulfillId, b_normalExecution);
		
		if(isNotNull(o_itemFulFillments)){
			
			nlapiLogExecution('DEBUG','B_GIF_3 Search Response','Size: '+o_itemFulFillments.length);
			
			for (var i = 0; i < o_itemFulFillments.length; i++) {
				
				var i_fulfillmentId = o_itemFulFillments[i].getValue('internalid');
				a_itemfulfillIds.push(i_fulfillmentId);
				i_lastFulfillId 	= i_fulfillmentId;
				var i_salesOrderId 	= o_itemFulFillments[i].getValue('createdfrom');
				var i_entityId 		= o_itemFulFillments[i].getValue('entity');
				var s_trandDate 	= o_itemFulFillments[i].getValue('trandate');
				var s_postingPeriod = o_itemFulFillments[i].getValue('postingperiod');
				var i_locationId 	= o_itemFulFillments[i].getValue('custbody_tjinc_bbonbf_location');
				var s_memo 			= o_itemFulFillments[i].getValue('memo');
				var s_serviceType 	= o_itemFulFillments[i].getValue('custbody5');//Service Type
				var s_servTyDate 	= o_itemFulFillments[i].getValue('custbody_tjinc_bbonbf_servicedate');
				var i_lineId 		= o_itemFulFillments[i].getValue('custcol_tjinc_bbonbf_lineid');
				var s_itemId 		= o_itemFulFillments[i].getValue('item');
				var s_therapist 	= o_itemFulFillments[i].getValue('custcol_tjinc_bbonbf_therapistid');
				var i_quantity 		= o_itemFulFillments[i].getValue('quantity');
				var i_therapist 	= o_itemFulFillments[i].getValue('custcol_tjinc_bbonbf_therapistid');
				var i_department	= o_itemFulFillments[i].getValue('department');
				var s_itemNote		= o_itemFulFillments[i].getValue('custcol_tjinc_bbonbf_textcolumn');
				var i_employeeId    = o_itemFulFillments[i].getValue('custcol_tjinc_bbonbf_employee');
				
				if(isNotNull(i_lineId)){
					
					if(isNull(o_itemFulfills[i_fulfillmentId])){
						
						s_itemFulfillments = s_itemFulfillments + '-'+i_fulfillmentId;
						var o_items = {};
						o_items[i_lineId] = {'lineid':i_lineId,'itemid':s_itemId,'destherpst':s_therapist,'qty':i_quantity,'idtherpst':i_therapist,'departmentid':i_department,'rate':0, 'fillnote':s_itemNote, 'employeeid':i_employeeId};
						o_itemFulfills[i_fulfillmentId] = {'id': i_fulfillmentId, 'soid':i_salesOrderId,'entityid':i_entityId,'trandate':s_trandDate,'postperiod':s_postingPeriod,'locationid':i_locationId,'memo':s_memo,'servicetype':s_serviceType,'items':o_items, 'servTypedate':s_servTyDate};
						nlapiLogExecution('DEBUG','B_GIF_4.'+i+' New Fulfillment', 'i_fulfillmentId: '+i_fulfillmentId+' Line Id :'+i_lineId);
					}
					else{
						
						var o_temItems = o_itemFulfills[i_fulfillmentId].items;
						
						if(isNull(o_temItems[i_lineId])){
							
							o_temItems[i_lineId] = {'lineid':i_lineId,'itemid':s_itemId,'destherpst':s_therapist,'qty':i_quantity,'idtherpst':i_therapist, 'departmentid':i_department,'rate':0, 'fillnote':s_itemNote, 'employeeid':i_employeeId};
							o_itemFulfills[i_fulfillmentId].items = o_temItems;
							nlapiLogExecution('DEBUG','B_GIF_5.'+i+' New Line Item Fulfillment', 'i_fulfillmentId: '+i_fulfillmentId+' Line Id :'+i_lineId);
						}
					}
				}
			}
			nlapiLogExecution('DEBUG','B_GIF_6 After search.', 'b_searchType: '+b_searchType+' b_executeSearch :'+b_executeSearch);
			
			if(b_normalExecution){
				
				if(!b_searchType){
					
					b_searchType = true;
				}
				else {
					b_executeSearch = false;
				}
			}
			else{
				b_executeSearch = false;
			}
		}
		else{
			b_executeSearch = false;
		}
	}
	
	//Comment 2 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-21
	if(a_itemfulfillIds.length > 0){
		o_clinicalTeam = TJINC_GetClinicalTeam(a_itemfulfillIds);
	}
	nlapiLogExecution('AUDIT','B_GIF_7 Completed TJINC_BBONBF_GetItemFulfillment.','Units: '+nlapiGetContext().getRemainingUsage()+' itemfulfillments: '+s_itemFulfillments);
	
	return {'itemfulfillments':o_itemFulfills, 'clinicalteam':o_clinicalTeam};
}

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_ItemfulfillmentSearch
function TJINC_ItemfulfillmentSearch(b_sinbleRecord, i_itemfulfillmentId, b_normalExecution){
	
	nlapiLogExecution('AUDIT','C_IFS_1 Running TJINC_ItemfulfillmentSearch.','b_sinbleRecord: '+b_sinbleRecord+' i_itemfulfillmentId :'+i_itemfulfillmentId);
	
	var a_columns = new Array();
	var a_filters = new Array();
	
	if(b_normalExecution){
		
		if(b_sinbleRecord){
			a_filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', i_itemfulfillmentId));
		}
		else{
			a_filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', i_itemfulfillmentId));
		}
	}
	else{
		a_filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', i_itemfulfillmentId));
	}
	
	a_filters.push(new nlobjSearchFilter('custrecord_tjinc_bbonbf_tobeprocessed', 'custbody_tjinc_bbonbf_billingprotoco', 'is', 'T'));
	a_filters.push(new nlobjSearchFilter('custbody_tjinc_bbonbf_processed', null, 'is', 'F'));
	a_filters.push(new nlobjSearchFilter('custbody_tjinc_bbonbf_onhold', null, 'is', 'F'));
	a_filters.push(new nlobjSearchFilter('custbody_tjinc_bbonbf_billingprotoco', null, 'noneof', '@NONE@'));
	
	a_columns.push(new nlobjSearchColumn('internalid').setSort());
	a_columns.push(new nlobjSearchColumn('createdfrom'));
	a_columns.push(new nlobjSearchColumn('entity'));
	a_columns.push(new nlobjSearchColumn('trandate'));
	a_columns.push(new nlobjSearchColumn('postingperiod'));
	a_columns.push(new nlobjSearchColumn('custbody_tjinc_bbonbf_location'));
	a_columns.push(new nlobjSearchColumn('memo'));
	a_columns.push(new nlobjSearchColumn('custbody5'));//Service Type
	a_columns.push(new nlobjSearchColumn('item'));
	a_columns.push(new nlobjSearchColumn('custcol_tjinc_bbonbf_employee'));
	a_columns.push(new nlobjSearchColumn('quantity'));
	a_columns.push(new nlobjSearchColumn('custcol_tjinc_bbonbf_lineid'));
	a_columns.push(new nlobjSearchColumn('custcol_tjinc_bbonbf_therapistid'));
	a_columns.push(new nlobjSearchColumn('custbody_tjinc_bbonbf_servicedate'));
	a_columns.push(new nlobjSearchColumn('custcol_tjinc_bbonbf_textcolumn'));
	a_columns.push(new nlobjSearchColumn('department'));
	
	var o_searchIF = nlapiSearchRecord('itemfulfillment', null, a_filters, a_columns);
	nlapiLogExecution('AUDIT','C_IFS_2 Completed TJINC_ItemfulfillmentSearch');
	
	return o_searchIF;
}

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_CreateaInvoiceBySalesOrder
function TJINC_CreateaInvoiceBySalesOrder(i_fulfillmentId, i_salesOrderId, o_itemsFulfill, s_tranDate, s_postPerd, i_locatnid, s_memo, s_serType, s_serDtType, o_clinicalTeam){
	
	nlapiLogExecution('AUDIT','D_CISO_1 Running TJINC_CreateaInvoiceBySalesOrder');
	
	try {
		//IF FOR SOME REASON THERE IS AN ISSUE WITH THE INVOICE PLEASE TEST TO CREATE IT AND SAVE IT AGAIN
		var o_newInvoice = nlapiTransformRecord('salesorder', i_salesOrderId, 'invoice',{recordmode: 'dynamic'});//,{recordmode: 'dynamic'}
		var db_partialAmount = 0;
		var s_discount = o_newInvoice.getFieldValue('discountrate');
		var i_discount = 0;
		var i_gstTax = 0;
		var i_pstTax = 0;
		var a_taxInvoice = new Array();
		var i_customForm = '117';
		var s_therapisthIdDescription = '';
		var o_employeeId = {};
		
		nlapiLogExecution('DEBUG','D_CISO_2 Loaded New Invoice.', 's_discount: '+s_discount);
		
		if(isNotNull(s_discount)){
			s_discount = s_discount+"";
			s_discount = s_discount.replace(/%/g,"");
			i_discount = parseFloat(s_discount);
		}
		
		o_newInvoice.setFieldValue('customform', i_customForm);
		if(isNotNull(s_memo))o_newInvoice.setFieldValue('memo', s_memo); 
		o_newInvoice.setFieldValue('trandate', s_tranDate);
		o_newInvoice.setFieldValue('postingperiod',  s_postPerd);
		o_newInvoice.setFieldValue('location',  i_locatnid);
		o_newInvoice.setFieldValue('custbody_tjinc_bbonbf_servicedate',  s_serDtType);
		o_newInvoice.setFieldValue('custbody5',  s_serType);
		o_newInvoice.setFieldValue('custbody_tjinc_bbonbf_itemfulfilmentid',  i_fulfillmentId);
		  
		var s_temItemTypeInvToDelOld = '';
		
		//Comment 1 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-12
		for(var i = 1 ; i <= o_newInvoice.getLineItemCount('item')  ; i++) {	
			
			var s_temItemTypeInv = o_newInvoice.getLineItemValue('item','itemtype',i);
			
			if (s_temItemTypeInv != 'EndGroup') {
				
				if(isNotNull(o_itemsFulfill[o_newInvoice.getLineItemValue('item','custcol_tjinc_bbonbf_lineid',i)])) o_itemsFulfill[o_newInvoice.getLineItemValue('item','custcol_tjinc_bbonbf_lineid',i)].rate = o_newInvoice.getLineItemValue('item', 'rate', i);
				
				if (s_temItemTypeInv != 'Description'){
					
					o_newInvoice.setLineItemValue('item', 'quantity', i, 0);
					o_newInvoice.setLineItemValue('item', 'rate', i, 0);
					o_newInvoice.setLineItemValue('item', 'amount', i, 0);
					o_newInvoice.setLineItemValue('item', 'fulfillable', i, 'F');
					
				}
			}
			
			if(i != 1 && s_temItemTypeInvToDelOld == 'Group'){
				o_newInvoice.setLineItemValue('item', 'fulfillable', (i-1), 'F');
				o_newInvoice.setLineItemValue('item','quantity', (i-1), 0);
				o_newInvoice.setLineItemValue('item','department',(i-1),o_newInvoice.getLineItemValue('item','department',(i)));
			}
			
			s_temItemTypeInvToDelOld = o_newInvoice.getLineItemValue('item','itemtype',i);
		}
		
		nlapiLogExecution('DEBUG','D_CISO_2 New Invoice Cleaned.');
		
		//Comment 2 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-22
		for (var i_index in o_itemsFulfill) {	
			
			nlapiLogExecution('DEBUG','D_CISO_3_'+i_index+' itemList');
			
			var s_itemTypeInv = '';
			var b_groupItem = false;
			
			
			for(var i = 1 ; i <= o_newInvoice.getLineItemCount('item') ; i++) {		
				
				//nlapiLogExecution('DEBUG','D_CISO_4.'+i+' Lines', 'Fulfill line Id: '+o_itemsFulfill[i_index].lineid+' - Invoice line id: '+ o_newInvoice.getLineItemValue('item','custcol_tjinc_bbonbf_lineid',i));
				
				if(o_itemsFulfill[i_index].lineid == o_newInvoice.getLineItemValue('item','custcol_tjinc_bbonbf_lineid',i)) {
					
					//nlapiLogExecution('DEBUG','D_CISO_5.'+i+' Lines', 'qty: '+o_itemsFulfill[i_index].qty+' Rate : '+ o_itemsFulfill[i_index].rate+' Department : '+o_itemsFulfill[i_index].departmentid);
					
					o_newInvoice.setLineItemValue('item', 'fulfillable', i, 'T');
					o_newInvoice.setLineItemValue('item','quantity', i, o_itemsFulfill[i_index].qty);
					o_newInvoice.setLineItemValue('item','rate',i,o_itemsFulfill[i_index].rate);
					db_partialAmount = parseFloat(db_partialAmount) + parseFloat(parseFloat(parseFloat(o_itemsFulfill[i_index].qty)*parseFloat(o_itemsFulfill[i_index].rate)).toFixed(2));
					o_newInvoice.setLineItemValue('item','amount',i,parseFloat(parseFloat(o_itemsFulfill[i_index].qty)*parseFloat(o_itemsFulfill[i_index].rate)).toFixed(2));
					o_newInvoice.setLineItemValue('item','department',i,o_itemsFulfill[i_index].departmentid);
					o_newInvoice.setLineItemValue('item','custcol_tjinc_bbonbf_therapistid',i,o_itemsFulfill[i_index].destherpst);
					o_newInvoice.setLineItemValue('item','custcol_tjinc_bbonbf_textcolumn',i,o_itemsFulfill[i_index].fillnote);
					var i_temEmployeeId = o_itemsFulfill[i_index].employeeid;
					
					if(isNull(o_employeeId[i_temEmployeeId])){
						s_therapisthIdDescription = s_therapisthIdDescription +' '+ o_itemsFulfill[i_index].destherpst;
						o_employeeId[i_temEmployeeId] = 'T';
					}
					
					var s_taxrate1 = (o_newInvoice.getLineItemValue('item','taxrate1',i))+"";
					var s_taxrate2 = (o_newInvoice.getLineItemValue('item','taxrate2',i))+"";
					
					//nlapiLogExecution('DEBUG','D_CISO_5.'+i+' Lines', 'qty: '+o_itemsFulfill[i_index].qty+' Rate : '+ o_itemsFulfill[i_index].rate+' Department : '+o_itemsFulfill[i_index].departmentid+' s_taxrate1: '+s_taxrate1 +' s_taxrate2: '+s_taxrate2);
					
					if(isNotNull(s_taxrate1) && isNotNull(s_taxrate2)){
						
						s_taxrate1 = s_taxrate1.replace(/%/g,"");
						s_taxrate2 = s_taxrate2.replace(/%/g,"");
						var i_taxrate1 = parseFloat(s_taxrate1);
						var i_taxrate2 = parseFloat(s_taxrate2);
						
						if(i_taxrate1 != 0 || i_taxrate2 != 0){
							
							a_taxInvoice.push({'gst':i_taxrate1, 'pst':i_taxrate2, 'lineamt':parseFloat(o_itemsFulfill[i_index].qty)*parseFloat(o_itemsFulfill[i_index].rate)});
						}
					}
					if(s_itemTypeInv == 'Group'){
						o_newInvoice.setLineItemValue('item', 'fulfillable', (i-1), 'T');
						o_newInvoice.setLineItemValue('item','quantity', (i-1), o_itemsFulfill[i_index].qty);
						o_newInvoice.setLineItemValue('item','department',(i-1),o_itemsFulfill[i_index].departmentid);
						s_itemTypeInv = '';
					} 
					break;
				}
				
				s_itemTypeInv = o_newInvoice.getLineItemValue('item','itemtype',i);
			}
		}
		
		nlapiLogExecution('DEBUG','D_CISO_4 Lines Set On The New Invoice.', 'db_partialAmount: '+db_partialAmount+' i_discount : '+i_discount+' s_therapisthIdDescription : '+s_therapisthIdDescription);
		
		if(isNotNull(s_therapisthIdDescription)) o_newInvoice.setFieldValue('custbody6',  s_therapisthIdDescription);
		
		//Comment 3 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-31
		o_newInvoice.setFieldValue('subtotal', db_partialAmount);
		
		if(i_discount != 0 ){
			
			var db_discount = (i_discount*db_partialAmount)/100;
			o_newInvoice.setFieldValue('discounttotal', parseFloat(db_discount).toFixed(2)); 
			db_partialAmount = db_partialAmount + db_discount;
			nlapiLogExecution('DEBUG','D_CISO_4.1 Partial Amount.', 'db_discount: '+db_discount+' New db_partialAmount'+db_partialAmount);
		}
		
		for(var i = 0; i < a_taxInvoice.length; i++){
			
			nlapiLogExecution('DEBUG','D_CISO_4.2.'+i+' Taxes.', 'a_taxInvoice[i].gst: '+a_taxInvoice[i].gst+' a_taxInvoice[i].pst: '+a_taxInvoice[i].pst);
			
			if(a_taxInvoice[i].gst != 0 ){
				if(i_discount != 0 ){
					
					var i_temLineVal = parseFloat(a_taxInvoice[i].lineamt*i_discount/100);
					i_temLineVal = parseFloat(a_taxInvoice[i].lineamt+i_temLineVal);
					i_gstTax = i_gstTax + (i_temLineVal*a_taxInvoice[i].gst/100);
				}
				else{
					i_gstTax = i_gstTax + (a_taxInvoice[i].lineamt*a_taxInvoice[i].gst/100);
				}
			}
			
			if(a_taxInvoice[i].pst != 0){
				if(i_discount != 0 ){
					var i_temLineVal = parseFloat(a_taxInvoice[i].lineamt*i_discount/100);
					i_temLineVal = parseFloat(a_taxInvoice[i].lineamt+i_temLineVal);
					i_pstTax = i_pstTax + (i_temLineVal*a_taxInvoice[i].pst/100);
				}
				else{
					i_pstTax = i_pstTax + (a_taxInvoice[i].lineamt*a_taxInvoice[i].pst/100);
				}
			}
			
		}
		
		nlapiLogExecution('DEBUG','D_CISO_4.3 Total Taxes.', 'i_gstTax: '+i_gstTax+' i_pstTax: '+i_pstTax);
		
		if(i_gstTax != 0 ){
			o_newInvoice.setFieldValue('taxtotal', parseFloat(i_gstTax).toFixed(2));
			db_partialAmount = db_partialAmount + i_gstTax;
		}
		else{
			o_newInvoice.setFieldValue('taxtotal', 0);
		}
		if(i_pstTax != 0 ){
			o_newInvoice.setFieldValue('tax2total', parseFloat(i_pstTax).toFixed(2));
			db_partialAmount = db_partialAmount + i_pstTax;
		}
		else{
			o_newInvoice.setFieldValue('tax2total', 0);
		}
		
		o_newInvoice.setFieldValue('total', parseFloat(db_partialAmount).toFixed(2));
		
	/*	for(var i = 1 ; i <= o_newInvoice.getLineItemCount('salesteam') ; i++) {	
			nlapiLogExecution('DEBUG','D_CISO_4.3.1.'+i+' salesteam list', 'Sales Team List: '+i);
			o_newInvoice.removeLineItem('salesteam',i);
		}
		*/
		
		nlapiLogExecution('DEBUG','D_CISO_4.3.2 salesteam size', 'Sales Team List: '+o_newInvoice.getLineItemCount('salesteam'));
		//Comment 4 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-41
		var b_isSameTeam = true;
		//This is a workaround because if the sales order clinical team is equal to the item fulfillment clinical team and if you want to update 
		//the clinical team when is the same netsuite returns an UNEXPECTED ERROR.
		for(var i = 1 ; i <= o_newInvoice.getLineItemCount('salesteam') ; i++) {		
					
			var i_employeeInv = o_newInvoice.getLineItemValue('salesteam','employee',i);
			if(isNull(o_clinicalTeam[i_employeeInv])){
				b_isSameTeam = false;
				break;
			}
			else{
				
				if(o_clinicalTeam[i_employeeInv].contribution != o_newInvoice.getLineItemValue('salesteam','contribution',i)){
					b_isSameTeam = false;
					break;
				}
			}	
		}
		
		nlapiLogExecution('DEBUG','D_CISO_4.3.3 CLINICAL TEMA', 'b_isSameTeam: '+b_isSameTeam);
		if(!b_isSameTeam){
			
			for(var i = 1 ; i <= o_newInvoice.getLineItemCount('salesteam') ; i++) {		
				
				var i_employeeInv = o_newInvoice.getLineItemValue('salesteam','employee',i);
				
				if(isNotNull(o_clinicalTeam[i_employeeInv])){
					o_newInvoice.setLineItemValue('salesteam', 'contribution', i, o_clinicalTeam[i_employeeInv].contribution);
					o_newInvoice.setLineItemValue('salesteam', 'salesrole', i,o_clinicalTeam[i_employeeInv].role);
					o_newInvoice.setLineItemValue('salesteam', 'isprimary', i, o_clinicalTeam[i_employeeInv].isprimary);
					o_clinicalTeam[i_employeeInv].isready = 'T';
				}
				else{
					
					o_newInvoice.setLineItemValue('salesteam', 'contribution', i, 0);
					o_newInvoice.setLineItemValue('salesteam', 'isprimary', i, 'F');
				}
			}
			
			for(var i = 1 ; i <= o_newInvoice.getLineItemCount('salesteam') ; i++) {
				
				if(o_newInvoice.getLineItemValue('salesteam', 'contribution', i) == 0){
					o_newInvoice.removeLineItem('salesteam',i);
					i = i-1;
				}
				
			}
			
			for (var i_indexCT in o_clinicalTeam) {
				
				if(o_clinicalTeam[i_indexCT].isready == 'F'){
					
					o_newInvoice.selectNewLineItem('salesteam');
					o_newInvoice.setCurrentLineItemValue('salesteam', 'employee', i_indexCT);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'contribution', o_clinicalTeam[i_indexCT].contribution);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'salesrole', o_clinicalTeam[i_indexCT].role);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'isprimary',  o_clinicalTeam[i_indexCT].isprimary);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'transaction',  i_salesOrderId);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'id',  i_salesOrderId+'_'+i_indexCT);
					
					o_newInvoice.commitLineItem('salesteam');
					
					/*
					var intCount = o_newInvoice.getLineItemCount('salesteam');

					o_newInvoice.insertLineItem('salesteam', intCount + 1);

					o_newInvoice.setCurrentLineItemValue('salesteam', 'employee', intCount + 1, i_indexCT);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'contribution', intCount + 1, o_clinicalTeam[i_indexCT].contribution);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'salesrole', intCount + 1, o_clinicalTeam[i_indexCT].role);
					o_newInvoice.setCurrentLineItemValue('salesteam', 'isprimary', intCount + 1, o_clinicalTeam[i_indexCT].isprimary);

					o_newInvoice.commitLineItem('salesteam');
*/
				}
			}
		}
		//Comment 5 https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Comment-5
		nlapiLogExecution('DEBUG','D_CISO_6 Before Save Invoice.');
		
		var i_newInvoiceId = nlapiSubmitRecord(o_newInvoice,true,false);
		
		nlapiLogExecution('AUDIT','D_CISO_7 THE Invoice was Created.', 'i_newInvoiceId : '+i_newInvoiceId);
		
		nlapiSubmitField('itemfulfillment', i_fulfillmentId, ['custbody_tjinc_bbonbf_processed'], ['T']);
		nlapiSubmitField('invoice', i_newInvoiceId, ['custbody6'], [s_therapisthIdDescription]);
		
		nlapiLogExecution('AUDIT','D_CISO_8 Completed TJINC_CreateaInvoiceBySalesOrder', 'Updated Item Fulfillment : '+i_fulfillmentId);
		
		return	{'status':1,'invid':i_newInvoiceId};
		
	}
	catch(e) {
		
		nlapiLogExecution('ERROR','D_CISO_9 Error On TJINC_CreateaInvoiceBySalesOrder','Item Fulfillment Id: '+i_fulfillmentId+' '+getErrorMsg(e));
		
		//2012-08-31 HECTOR - THIS IS A NEW USE CASE ON PRODUCTION TO THIS KIND OF ERROR NETSUITE CREATE THE INVOICE BUT WITH AN ERROR FOR THIS IF THE ERROR IS THIS THE SCRIPT ISGOING TO UPDATE THE FULFILLMENT TO AVOID MORE THAN ONE INVOICE BY FULFILLMENT.
		var s_message = getErrorMsg(e)+'';
		nlapiLogExecution('ERROR','D_CISO_10 Message Error','Item s_message: '+s_message);
		
		if(s_message.indexOf('THE_INVOICE_HAS_BEEN_SAVED_BUT_AUTOMATIC_APPLICATION_OF_THE_DEPOSIT_HAS_FAILED_PLEASE_APPLY_THE_DEPOSIT_MANUALLY') != '-1'){
			
			nlapiLogExecution('ERROR','D_CISO_11 Updating Item fulfillment','Item Fulfillment Id: '+i_fulfillmentId);
			
			var a_columns 	= new Array();
			var a_filters 	= new Array();
			a_filters.push(new nlobjSearchFilter('custbody_tjinc_bbonbf_itemfulfilmentid', null, 'anyof', [i_fulfillmentId]));
			a_columns.push(new nlobjSearchColumn('internalid').setSort());
			
			var o_srchInvoiceByItemFulfill = nlapiSearchRecord('invoice', null, a_filters, a_columns);
			
			if(isNotNull(o_srchInvoiceByItemFulfill)){
				
				if(o_srchInvoiceByItemFulfill.length > 0){
					
					nlapiSubmitField('itemfulfillment', i_fulfillmentId, ['custbody_tjinc_bbonbf_processed'], ['T']);
					var s_itemFulfillIdSrch = o_srchInvoiceByItemFulfill[0].getValue('internalid');
					return	{'status':1,'invid':s_itemFulfillIdSrch};
				}
			}
		}
		
		return {'status':0,'msg':getErrorMsg(e)};
	} 
}

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_GetClinicalTeam
function TJINC_GetClinicalTeam(a_itemFulfillments){
	
	nlapiLogExecution('AUDIT','E_GCT_1 Running TJINC_GetClinicalTeam', 'a_itemFulfillments : '+a_itemFulfillments.length);
	
	var a_columns 	= new Array();
	var a_filters 	= new Array();
	var i_lastClinicalTeamId = '';
	var b_keepRunning 	= true;
	var o_itemFulCT 	= {};
	var b_aNewRecord 	= false;
	
	while(b_keepRunning){
		
		a_filters.push(new nlobjSearchFilter('custrecord_tjinc_bbonbf_itemfulfillment', null, 'anyof', a_itemFulfillments));
		a_filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		
		if(isNotNull(i_lastClinicalTeamId)){
			a_filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', i_lastClinicalTeamId));
		}
		
		a_columns.push(new nlobjSearchColumn('internalid').setSort());
		a_columns.push(new nlobjSearchColumn('custrecord_tjinc_bbonbf_contribution'));
		a_columns.push(new nlobjSearchColumn('custrecord_tjinc_bbonbf_employee'));
		a_columns.push(new nlobjSearchColumn('custrecord_tjinc_bbonbf_salesrole'));
		a_columns.push(new nlobjSearchColumn('custrecord_tjinc_bbonbf_primary'));
		a_columns.push(new nlobjSearchColumn('custrecord_tjinc_bbonbf_itemfulfillment'));
		
		var o_srchCliniTeam = nlapiSearchRecord('customrecord_tjinc_bbonbf_clinicalteam', null, a_filters, a_columns);
		

		if(isNotNull(o_srchCliniTeam)){
			
			for (var j = 0; j < o_srchCliniTeam.length; j++) {
				
				i_lastClinicalTeamId 	= o_srchCliniTeam[j].getId();
				var i_itemFulFillId 	= o_srchCliniTeam[j].getValue('custrecord_tjinc_bbonbf_itemfulfillment');
				var i_clinicalId		= o_srchCliniTeam[j].getId();
				var i_clinicalEmployee 	= o_srchCliniTeam[j].getValue('custrecord_tjinc_bbonbf_employee');
				var s_contribution 		= o_srchCliniTeam[j].getValue('custrecord_tjinc_bbonbf_contribution');
				var s_role 				= o_srchCliniTeam[j].getValue('custrecord_tjinc_bbonbf_salesrole');
				var s_isPrimary 		= o_srchCliniTeam[j].getValue('custrecord_tjinc_bbonbf_primary');
			 
				if(isNull(o_itemFulCT[i_itemFulFillId])){
					
					var o_clinicaTeam = {};
					b_aNewRecord = true;
					o_clinicaTeam[i_clinicalEmployee] = {'clid':i_clinicalId, 'empid':i_clinicalEmployee,'contribution':s_contribution,'role':s_role,'isprimary':s_isPrimary,'isready':'F'};
					o_itemFulCT[i_itemFulFillId] = {'fulfillid': i_itemFulFillId, 'members':o_clinicaTeam};
					
				}
				else{
					
					var o_temEmpCT = o_itemFulCT[i_itemFulFillId].members;
					
					if(isNull(o_temEmpCT[i_clinicalEmployee])){
						
						b_aNewRecord = true;
						o_temEmpCT[i_clinicalEmployee] = {'clid':i_clinicalId,'empid':i_clinicalEmployee,'contribution':s_contribution,'role':s_role,'isprimary':s_isPrimary,'isready':'F'};
						o_itemFulCT[i_itemFulFillId].members = o_temEmpCT;
						
					}
				}
			}
			
			if(b_aNewRecord){
				b_aNewRecord = false;
			}
			else{
				b_keepRunning = false;
			}
		}
		else{
			b_keepRunning = false;
		}
	}
	nlapiLogExecution('AUDIT','E_GCT_2 Completed TJINC_GetClinicalTeam', 'a_itemFulfillments : '+a_itemFulfillments.length);
	
	return o_itemFulCT;
}

// https://sites.google.com/a/trajectoryinc.com/bbo/classes/tjinc_bbonbf_createinvoicebyfulfill-js#TOC-Function:-TJINC_DeployScriptFulfillToInvoice
function TJINC_DeployScriptFulfillToInvoice(i_lastFulfillId, s_invoiceIds, s_errorMsg){
	
	nlapiLogExecution('AUDIT','F_DSF_1 Running TJINC_DeployScriptFulfillToInvoice', 'i_lastFulfillId : '+i_lastFulfillId+' s_invoiceIds: '+s_invoiceIds.length+' s_errorMsg: '+s_errorMsg.length);
	
	if(parseInt(nlapiGetContext().getDeploymentId().replace('customdeploy','')) == 1) {
		
		nlapiLogExecution('DEBUG','F_DSF_2 Invoice Engine deployScript 1 Units: ('+nlapiGetContext().getRemainingUsage()+')','No script units remaining, rescheduling customdeploy2');
		nlapiScheduleScript(nlapiGetContext().getScriptId(),'customdeploy2', {'custscript_tjinc_bbonbf_lastitemfulfill':i_lastFulfillId, 'custscript_tjinc_bbonbf_invoices':s_invoiceIds, 'custscript_tjinc_bbonbf_errormsg':s_errorMsg});
	}
	else if(parseInt(nlapiGetContext().getDeploymentId().replace('customdeploy','')) == 2) {
		
		nlapiLogExecution('DEBUG','F_DSF_3 Invoice Engine deployScript 2  Units: ('+nlapiGetContext().getRemainingUsage()+')','No script units remaining, rescheduling customdeploy3');
		nlapiScheduleScript(nlapiGetContext().getScriptId(),'customdeploy3', {'custscript_tjinc_bbonbf_lastitemfulfill':i_lastFulfillId, 'custscript_tjinc_bbonbf_invoices':s_invoiceIds, 'custscript_tjinc_bbonbf_errormsg':s_errorMsg});
	}
	else if(parseInt(nlapiGetContext().getDeploymentId().replace('customdeploy','')) == 3) {
		
		nlapiLogExecution('DEBUG','F_DSF_4 Invoice Engine deployScript 3  Units: ('+nlapiGetContext().getRemainingUsage()+')','No script units remaining, rescheduling customdeploy2');
		nlapiScheduleScript(nlapiGetContext().getScriptId(),'customdeploy2', {'custscript_tjinc_bbonbf_lastitemfulfill':i_lastFulfillId, 'custscript_tjinc_bbonbf_invoices':s_invoiceIds, 'custscript_tjinc_bbonbf_errormsg':s_errorMsg});
	}
	
	nlapiLogExecution('AUDIT','F_DSF_5 Completed TJINC_DeployScriptFulfillToInvoice', 'i_lastFulfillId : '+i_lastFulfillId+' s_invoiceIds: '+s_invoiceIds.length+' s_errorMsg: '+s_errorMsg.length);
	
}

function TJINC_BBONBF_FulfillByID(i_fulfillmentId){
	
	nlapiLogExecution('AUDIT','G_FID_1 Running TJINC_BBONBF_FulfillByID.','Units: '+nlapiGetContext().getRemainingUsage()+' Item Fulfillment ID:'+i_fulfillmentId);
	
	try{
		var o_searchInformation = TJINC_BBONBF_GetItemFulfillment(i_fulfillmentId, false);
		var o_fulfillments = o_searchInformation.itemfulfillments;
		var o_clinlTeanInf = o_searchInformation.clinicalteam;
		
		if(isNotJsonNull(o_fulfillments) && isNotJsonNull(o_clinlTeanInf)){
			
			for (var i_index in o_fulfillments) {
				
				if(isNotNull(o_clinlTeanInf[i_index])){
					
					var i_fulfillId = o_fulfillments[i_index].id;
					var i_soid 		= o_fulfillments[i_index].soid;
					var s_tranDate 	= o_fulfillments[i_index].trandate;
					var s_postPerd 	= o_fulfillments[i_index].postperiod;
					var i_locatnid 	= o_fulfillments[i_index].locationid;
					var s_memo 		= o_fulfillments[i_index].memo;
					var s_serType 	= o_fulfillments[i_index].servicetype;
					var s_serDtType = o_fulfillments[i_index].servTypedate;
					
					nlapiLogExecution('DEBUG','G_FID_2.'+i_index+' Before Create Invoice.','Fulfill Id: '+i_fulfillId+' SO Id: '+i_soid+' s_tranDate: '+s_tranDate+' s_postPerd: '+s_postPerd+' i_locatnid: '+i_locatnid +' s_serType: '+s_serType);
					
					var o_invResult = TJINC_CreateaInvoiceBySalesOrder(i_fulfillId, i_soid, o_fulfillments[i_index].items, s_tranDate, s_postPerd, i_locatnid, s_memo, s_serType, s_serDtType, o_clinlTeanInf[i_index].members);
					
					if(o_invResult.status == '1'){
						
						nlapiLogExecution('DEBUG','G_FID_3.'+i_index+' After Create Invoice SUCCESS.','Item Fulfillment Id: '+o_fulfillments[i_index].id+' INVOICE Id : '+o_invResult.invid);
						return o_invResult.invid;
					}
					else{
						
						nlapiLogExecution('DEBUG','G_FID_4.'+i_index+' After Create Invoice ERROR.','Item Fulfillment Id: '+o_fulfillments[i_index].id);
						return o_invResult.msg;
					}
				}
			}
		}
	}
	catch(e){
		nlapiLogExecution('ERROR','G_FID_5 Error on TJINC_BBONBF_FulfillByID ','Item Fulfillment Id: '+i_fulfillmentId+' - '+getErrorMsg(e) );
		return -1;
	}
	
	nlapiLogExecution('AUDIT','G_FID_6 Completed TJINC_BBONBF_FulfillByID.','Units: '+nlapiGetContext().getRemainingUsage());
}

//SB