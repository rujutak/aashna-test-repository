/*Date Modified 29122012 02012012 04 11012013 21012013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20132106
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

//--------- Global Variables ----------
var i_CurrentUser=nlapiGetUser(); /// Get current user id
var o_LevelsObj = new Object();
var o_Level_Status_Obj = new Object();

function TJINC_TIFF_PR_Pageinit()
{
	o_LevelsObj.i_level_1 = nlapiGetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
	o_LevelsObj.i_level_2 = nlapiGetFieldValue('custbody_tjinc_tifnfi_directorapprover');
	o_LevelsObj.i_level_3 = nlapiGetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
	o_LevelsObj.i_level_CFO = nlapiGetFieldValue('custbody_tjinc_tifnfi_exec1approver');
	o_LevelsObj.i_level_COO = nlapiGetFieldValue('custbody_tjinc_tifnfi_exec2approver');
	
	o_Level_Status_Obj.i_level_1 = nlapiGetFieldValue('custbody_tjinc_tifnfi_l1status');
	o_Level_Status_Obj.i_level_2 = nlapiGetFieldValue('custbody2');
	o_Level_Status_Obj.i_level_3 = nlapiGetFieldValue('custbody_tjinc_tifnfi_l3status');
	o_Level_Status_Obj.i_level_CFO = nlapiGetFieldValue('custbody_tjinc_tifnfi_cfostatus');
	o_Level_Status_Obj.i_level_COO = nlapiGetFieldValue('custbodycustbody_tjinc_tifnfi_coo');
}


function TJINC_TIFF_PR_FieldChange(type, name)
{
	var i_PR_Administrator = nlapiGetFieldValue('employee');
	switch (name)
	{
		case 'custbody_tjinc_tifnfi_srmanagerapprove':
		{
			//alert('i_PR_Administrator= '+i_PR_Administrator +' i_CurrentUser= '+ i_CurrentUser + ' o_Level_Status_Obj.i_level_1= 1 '+ o_Level_Status_Obj.i_level_1  + ' && o_Level_Status_Obj.i_level_2 != 3 '+ o_Level_Status_Obj.i_level_2 + ' && o_Level_Status_Obj.i_level_3 != 3 '+ o_Level_Status_Obj.i_level_3 + ' && o_Level_Status_Obj.i_level_CFO!=3 '+ o_Level_Status_Obj.i_level_CFO + ' && o_Level_Status_Obj.i_level_COO!=3 '+ o_Level_Status_Obj.i_level_COO)
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 == 1 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', o_LevelsObj.i_level_1, false, false);
			}
			if(o_Level_Status_Obj.i_level_2 == 1 || o_Level_Status_Obj.i_level_3 == 1 || o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', o_LevelsObj.i_level_1, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_directorapprover':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 == 1 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', o_LevelsObj.i_level_2, false, false);
			}
			if(o_Level_Status_Obj.i_level_3 == 1 || o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', o_LevelsObj.i_level_2, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_vicepresidentapp':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 == 1 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp', o_LevelsObj.i_level_3, false, false);
			}
			if(o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp', o_LevelsObj.i_level_3, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_exec1approver':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO == 1 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec1approver', o_LevelsObj.i_level_CFO, false, false);
			}
			if(o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec1approver', o_LevelsObj.i_level_CFO, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_exec2approver':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', o_LevelsObj.i_level_COO, false, false);
			}
		}
		
		//default:
		break;
	}
}

