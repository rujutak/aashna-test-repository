/*Date Modified 29122012 02012012 04 11012013 21012013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20132106
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

//--------- Global Variables ----------
var i_CurrentUser=nlapiGetUser(); /// Get current user id
var o_LevelsObj = new Object();
var o_Level_Status_Obj = new Object();
var a_ItemArray = new Array();

function TJINC_TIFF_PR_Pageinit()
{
	if(type != 'create' && type != 'delete')
	{
		o_LevelsObj.i_level_1 = nlapiGetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
		o_LevelsObj.i_level_2 = nlapiGetFieldValue('custbody_tjinc_tifnfi_directorapprover');
		o_LevelsObj.i_level_3 = nlapiGetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
		o_LevelsObj.i_level_CFO = nlapiGetFieldValue('custbody_tjinc_tifnfi_exec1approver');
		o_LevelsObj.i_level_COO = nlapiGetFieldValue('custbody_tjinc_tifnfi_exec2approver');
		
		o_Level_Status_Obj.i_level_1 = nlapiGetFieldValue('custbody_tjinc_tifnfi_l1status');
		o_Level_Status_Obj.i_level_2 = nlapiGetFieldValue('custbody2');
		o_Level_Status_Obj.i_level_3 = nlapiGetFieldValue('custbody_tjinc_tifnfi_l3status');
		o_Level_Status_Obj.i_level_CFO = nlapiGetFieldValue('custbody_tjinc_tifnfi_cfostatus');
		o_Level_Status_Obj.i_level_COO = nlapiGetFieldValue('custbodycustbody_tjinc_tifnfi_coo');
		
		TJINC_TIFNIM_GetItemInfoOP();
	}
}


function TJINC_TIFF_PR_FieldChange(type, name)
{
	var i_PR_Administrator = nlapiGetFieldValue('employee');
	switch (name)
	{
		case 'custbody_tjinc_tifnfi_srmanagerapprove':
		{
			//alert('i_PR_Administrator= '+i_PR_Administrator +' i_CurrentUser= '+ i_CurrentUser + ' o_Level_Status_Obj.i_level_1= 1 '+ o_Level_Status_Obj.i_level_1  + ' && o_Level_Status_Obj.i_level_2 != 3 '+ o_Level_Status_Obj.i_level_2 + ' && o_Level_Status_Obj.i_level_3 != 3 '+ o_Level_Status_Obj.i_level_3 + ' && o_Level_Status_Obj.i_level_CFO!=3 '+ o_Level_Status_Obj.i_level_CFO + ' && o_Level_Status_Obj.i_level_COO!=3 '+ o_Level_Status_Obj.i_level_COO)
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 == 1 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', o_LevelsObj.i_level_1, false, false);
			}
			if(o_Level_Status_Obj.i_level_2 == 1 || o_Level_Status_Obj.i_level_3 == 1 || o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', o_LevelsObj.i_level_1, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_directorapprover':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 == 1 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', o_LevelsObj.i_level_2, false, false);
			}
			if(o_Level_Status_Obj.i_level_3 == 1 || o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', o_LevelsObj.i_level_2, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_vicepresidentapp':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 == 1 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp', o_LevelsObj.i_level_3, false, false);
			}
			if(o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp', o_LevelsObj.i_level_3, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_exec1approver':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO == 1 && o_Level_Status_Obj.i_level_COO != 3)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec1approver', o_LevelsObj.i_level_CFO, false, false);
			}
			if(o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for next level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec1approver', o_LevelsObj.i_level_CFO, false, false);
				break;
			}
		}
		case 'custbody_tjinc_tifnfi_exec2approver':
		{
			if(i_PR_Administrator == i_CurrentUser && o_Level_Status_Obj.i_level_1 != 3 && o_Level_Status_Obj.i_level_2 != 3 && o_Level_Status_Obj.i_level_3 != 3 && o_Level_Status_Obj.i_level_CFO != 3 && o_Level_Status_Obj.i_level_COO == 1)
			{
				alert('The approval name cannot be changed because an approval has already been recorded for this level.');
				nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', o_LevelsObj.i_level_COO, false, false);
			}
		}
		
		//default:
		break;
	}
}

function TJINC_TIFNIM_GetItemInfoOP()
{	
	a_ItemArray = {};
	if(nlapiGetLineItemCount('item') > 0)
	{		
		for(var i = 1; i <= nlapiGetLineItemCount('item'); i++)
		{
			var i_item = nlapiGetLineItemValue('item', 'item', i);
			var i_qty = nlapiGetLineItemValue('item', 'quantity', i);
			var f_rate = nlapiGetLineItemValue('item', 'rate', i);
						
			a_ItemArray[i] = i_item+'##'+f_rate;
		}
	}
}

/*
function TJINC_TIFNIM_ValidateLine()
{
	var i_index = nlapiGetCurrentLineItemIndex('item');
	var i_itemid =  nlapiGetCurrentLineItemValue('item','item');	
	var f_itemrate = nlapiGetCurrentLineItemValue('item', 'rate');
	var i_itemarrayindex = i_index + 1;
	if(i_index == a_ItemArray[i_itemarrayindex])
	{
		
	}
}
*/

function TJINC_TIFNIM_SaveRecord()
{
	if(o_Level_Status_Obj.i_level_1 == 1 || o_Level_Status_Obj.i_level_2 == 1 || o_Level_Status_Obj.i_level_3 == 1 || o_Level_Status_Obj.i_level_CFO == 1 || o_Level_Status_Obj.i_level_COO == 1)
	{
		(nlapiGetLineItemCount('item') > 0)
		{
			for(var i = 1; i <= nlapiGetLineItemCount('item'); i++)
			{
				var i_item = nlapiGetLineItemValue('item', 'item', i);			
				var f_rate = nlapiGetLineItemValue('item', 'rate', i);
				var a_SplitArray = new Array();			
				a_SplitArray = a_ItemArray[i].split('##');
				if(i_item != a_SplitArray[0] ||  f_rate != a_SplitArray[1])
				{
					alert('A line item change has occurred after an approval has been processed. The PR will be marked as Rejected and will need to be resubmitted for approval.');
					RejectCall();
				}
			}
		}
	}		
	return true;
}

//=============================== Function for Rejection of PR ================================================
function RejectCall()
{
    var reasonForRejection="Amount or Item was changed after submission.";            
    nlapiSetFieldValue('custbody_tjinc_tifnfi__rejectionreason',reasonForRejection);
    nlapiSetFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
    nlapiSetFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
    nlapiSetFieldValue('approvalstatus','3'); //approvalstatus is the Approval Status and 3 is for Rejected
    
    //Get custom email template of rejection    
    var rejectflag=2;           
    var rejectEmailTemplate =getCustomEmailtemplatefun(rejectflag);
    var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');  
	 
    //---- Reject Email send to the Administrator and Requesting Employee By Default.   
    var PR_Adminstrator=nlapiGetFieldValue('employee');
    var PR_RequestingEmployee=nlapiGetFieldValue('custbody_tjinc_tifnfi_employee');
    if(PR_Adminstrator==PR_RequestingEmployee)
    {
           var to_Email=nlapiGetFieldValue('custbody_emailsourceofadministrator');
           rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_administratorfirstname'));
                               
           if(_LogValidation(to_Email))
             nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator   
    }
    else
    {                       
            //=====Email Requesting Employee =====
           var to_Email_ReqEmp=nlapiGetFieldValue('custbody_requestingemployeeemail');
           rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_requestingemployeefirstname'));                
           
           if(_LogValidation(to_Email_ReqEmp))
                nlapiSendEmail('645',to_Email_ReqEmp,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
           //=====Email Administartor =====
           var to_Email_Admin=nlapiGetFieldValue('custbody_emailsourceofadministrator');
           rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_administratorfirstname'));                 
          
           if(_LogValidation(to_Email_Admin))
           nlapiSendEmail('645',to_Email_Admin,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator               
    }       
        
	if(o_Level_Status_Obj.i_level_COO == 1)
	{
		nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo', 2)
		Level_COO_Email(rejectEmailTemplate, RejectedEmailSubject)
		if(o_Level_Status_Obj.i_level_CFO == 1)
		{
			//nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus', 2)
			Level_CFO_Email(rejectEmailTemplate, RejectedEmailSubject)
			if(o_Level_Status_Obj.i_level_3 == 1)
			{
				//nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status', 2)
				Level_3_Email(rejectEmailTemplate, RejectedEmailSubject)
				if(o_Level_Status_Obj.i_level_2 == 1)
				{
					//nlapiSetFieldValue('custbody2', 2)
					Level_2_Email(rejectEmailTemplate, RejectedEmailSubject)
					if(o_Level_Status_Obj.i_level_1 == 1)
					{
						//nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status', 2)
						Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
					}
				}
			}
		}
	}
	else if(o_Level_Status_Obj.i_level_CFO == 1)
	{
		nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus', 2)
		Level_CFO_Email(rejectEmailTemplate, RejectedEmailSubject)
		nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo', 4)
		if(o_Level_Status_Obj.i_level_3 == 1)
		{
			//nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status', 2)
			Level_3_Email(rejectEmailTemplate, RejectedEmailSubject)
			if(o_Level_Status_Obj.i_level_2 == 1)
			{
				//nlapiSetFieldValue('custbody2', 2)
				Level_2_Email(rejectEmailTemplate, RejectedEmailSubject)
				if(o_Level_Status_Obj.i_level_1 == 1)
				{
					//nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status', 2)
					Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
				}
			}
		}
	}
	else if(o_Level_Status_Obj.i_level_3 == 1)
	{
		nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status', 2)
		Level_3_Email(rejectEmailTemplate, RejectedEmailSubject)
		nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo', 4)
		nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus', 4)
		//alert('Email has been sent');
		if(o_Level_Status_Obj.i_level_2 == 1)
		{
			//nlapiSetFieldValue('custbody2', 2)
			Level_2_Email(rejectEmailTemplate, RejectedEmailSubject)
			//alert('Email has been sent level 2');
			if(o_Level_Status_Obj.i_level_1 == 1)
			{
				//nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status', 2)
				Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
				//alert('Email has been sent level 3');
			}
		}
	}
	else if(o_Level_Status_Obj.i_level_2 == 1)
	{
		nlapiSetFieldValue('custbody2', 2)
		Level_2_Email(rejectEmailTemplate, RejectedEmailSubject)
		nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo', 4)
		nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus', 4)
		nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status', 4)
		if(o_Level_Status_Obj.i_level_1 == 1)
		{
			//nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status', 2)
			Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
		}
	}
	else if(o_Level_Status_Obj.i_level_1 == 1)
	{
		nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status', 2)
		Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
		nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo', 4)
		nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus', 4)
		nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status', 4)
		nlapiSetFieldValue('custbody2', 4)
	}
    //location.reload(true);
}//fun RejectCall

function Level_1_Email(rejectEmailTemplate, RejectedEmailSubject)
{
    var level1=nlapiGetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
    if(_LogValidation(level1))
    {
        var to_Email=nlapiGetFieldValue('custbody_level1email');
        rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_level1firstname'));                     
        
        if(_LogValidation(to_Email))
           nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);//RejectedEmailSubject
    }
    
}//function i =1

function Level_2_Email(rejectEmailTemplate, RejectedEmailSubject)
{
    var level2=nlapiGetFieldValue('custbody_tjinc_tifnfi_directorapprover');
    if(_LogValidation(level2))
    {   
        var to_Email=nlapiGetFieldValue('custbody_level2email');
        rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_level2firstname'));                 
        
        if(_LogValidation(to_Email))
          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
    }
    
}//function i =2
function Level_3_Email(rejectEmailTemplate, RejectedEmailSubject)
{
    var level3=nlapiGetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
    if(_LogValidation(level3))
    {   
        var to_Email=nlapiGetFieldValue('custbody_level3email');
        rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_level3firstname'));                 
        
        if(_LogValidation(to_Email))
            nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
    }
    
}//function i =3
function Level_CFO_Email(rejectEmailTemplate, RejectedEmailSubject)
{
    var level4=nlapiGetFieldValue('custbody_tjinc_tifnfi_exec1approver');
    if(_LogValidation(level4))
    {   
        var to_Email=nlapiGetFieldValue('custbody_cfoemail');
        rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_cfofirstname'));                        
        
        if(_LogValidation(to_Email))
          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
    }
    
}//function i =4
function Level_COO_Email(rejectEmailTemplate, RejectedEmailSubject)
{
    var level5=nlapiGetFieldValue('custbody_tjinc_tifnfi_exec2approver');
    if(_LogValidation(level5))
    {   
        var to_Email=nlapiGetFieldValue('custbody_emailsourceofcoo');
        rejectEmailTemplate = rejectEmailTemplate.replace('USER',nlapiGetFieldValue('custbody_coofirstname'));              
        
        if(_LogValidation(to_Email))
        nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
    }
    
}//function i =5 


function getCustomEmailtemplatefun(flag)
{
    //------- Load Custom record for Email template
    var emailtemplateObj=nlapiLoadRecord('customrecord_tiffprocurementapprovaem',2);
    var EmailTemplate='';   
    if(flag==3)
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveremailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);      
        EmailTemplate = EmailTemplate.replace('DATEVALUE',nlapiGetFieldValue('trandate'));
        //EmailTemplate = EmailTemplate.replace('PERIODVALUE','');
        EmailTemplate = EmailTemplate.replace('NUMBER',nlapiGetFieldValue('tranid'));
        EmailTemplate = EmailTemplate.replace('VENDORNAME',nlapiGetFieldText('entity'));
        EmailTemplate = EmailTemplate.replace('MEMO',nlapiGetFieldValue('memo'));
        EmailTemplate = EmailTemplate.replace('AMOUNT',nlapiGetFieldValue('custbody_tjinc_tifnfi_prnetnetamount'));//custbody_tjinc_tifnfi_prnetnetamount
        EmailTemplate = EmailTemplate.replace('STATUS',nlapiGetFieldValue('status'));
        EmailTemplate = EmailTemplate.replace('RECEIVED','Received');       
        
        var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
        var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
        
        EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
        return EmailTemplate;
        
    }//flag!=3
    
    if(flag==1)/// For Approval template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveemailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);
    }
    else if(flag==2) /// For Reject template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_rejectemailtemplate');
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','rejectEmailTemplate='+EmailTemplate);
        //alert("EmailTemplate="+EmailTemplate);
        var rejReason=nlapiGetFieldValue('custbody_tjinc_tifnfi__rejectionreason');
        if(rejReason==null || rejReason=='' || rejReason==undefined){rejReason='';}
        EmailTemplate = EmailTemplate.replace('REJECTREASON',rejReason);
    }   
    //20062013
	var memo=nlapiGetFieldValue('memo')
	if(memo==null){memo=''}
	EmailTemplate = EmailTemplate.replace('MEMO',memo);
    EmailTemplate = EmailTemplate.replace('PONUMBER',nlapiGetFieldValue('tranid'));    
    EmailTemplate = EmailTemplate.replace('VENDORNAME',nlapiGetFieldValue('custbody_vendorname'));     
    EmailTemplate = EmailTemplate.replace('STATUS',nlapiGetFieldValue('status'));
    
    var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
    var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
    
    EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
    //alert("EmailTemplate="+EmailTemplate);
    return EmailTemplate;
}//function getCustomEmailtemplate