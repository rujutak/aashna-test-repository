/**
* Copyright (c) 2012 Trajectory Group Inc. / Kuspide Canada Inc.
* 76 Richmond St. East, Suite 400, Toronto, ON, Canada, M5C 1P1 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: TIFF
* @Company: Trajectory Inc.  
* @CreationDate: 20121211
* @DocumentationUrl:  https://sites.google.com/a/trajectoryinc.com/knb/classes/tjinc_knbnctr_ss_ctrtososync-js
* @NamingStandard: TJINC_NSJ-1-2
*/


function TJINC_TIFNIM_OnSavePO(){
	
	
		//alert('TJINC_TIFNIM_OnFieldChange type: '+type+' name : '+name);
	var b_resultStatus = true;
	var s_level1 = nlapiGetFieldText('custbody_tjinc_tifnfi_srmanagerapprove');
	var s_level2 = nlapiGetFieldText('custbody_tjinc_tifnfi_directorapprover');
	var s_level3 = nlapiGetFieldText('custbody_tjinc_tifnfi_vicepresidentapp');
	var s_cfo =  nlapiGetFieldText('custbody_tjinc_tifnfi_exec1approver');
	var s_coo = nlapiGetFieldText('custbody_tjinc_tifnfi_exec2approver');
	
	//alert('subtotal: '+nlapiGetFieldValue('subtotal'));
    var i_subTotal = nlapiGetFieldValue('subtotal');
    if(isNull(i_subTotal))i_subTotal = 0;
    
    if(i_subTotal < 5000.00){
        //var s_level1 = nlapiGetFieldText('custbody_tjinc_tifnfi_srmanagerapprove');
        //var s_level2 = nlapiGetFieldText('custbody_tjinc_tifnfi_directorapprover');
        if((isNotNull(s_level2) && isNotNull(s_level3))){
            alert('Unable to save Purchase Request where there is a value in Level 2 or Level 3.');
            b_resultStatus = false;
        }
		else if(isNotNull(s_level1) && (isNotNull(s_level2) || isNotNull(s_level3))){
            alert('Unable to save Purchase Request where there is a value in Level 2 or Level 3.');
            b_resultStatus = false;
        }
    }
	
	if((i_subTotal >= 5000.00) && (i_subTotal < 10000.00)) {
		if(isNotNull(s_level1)) {
            alert('Unable to save Purchase Request where there is a value in Level 1.');
            b_resultStatus = false;
        }
		else if(isNotNull(s_level2) && isNotNull(s_level3)){
            alert('Unable to save Purchase Request where there is a value in Level 2 and Level 3.');
            b_resultStatus = false;
        }
	}
	
	if((i_subTotal >= 10000.00) && (i_subTotal < 25000.00)) {
		if(isNotNull(s_level1) || isNotNull(s_level2)){
            alert('Unable to save Purchase Request where there is a value in Level 1 or Level 2.');
            b_resultStatus = false;
        }
	}
    
    if(b_resultStatus){
    	var b_resultVal = TJINC_FieldsValidation(s_level1, s_level2, s_level3, s_cfo, s_coo);
    		
    	if(!b_resultVal){
    		alert('The system detected a duplicate approver. Remove the duplicate approver to proceed.');
    		b_resultStatus = false;
    	}
    }
	
	
	return b_resultStatus;
}

function TJINC_TIFNIM_OnFieldChangePO(type, name) {
	
	try {
		//alert('TJINC_TIFNIM_OnFieldChangePO type: '+type+' name : '+name);
		
		switch (name) {
			case 'custbody_tjinc_tifnfi_srmanagerapprove':
			case 'custbody_tjinc_tifnfi_directorapprover':
			case 'custbody_tjinc_tifnfi_vicepresidentapp':
			case 'custbody_tjinc_tifnfi_exec1approver':
			case 'custbody_tjinc_tifnfi_exec2approver':
				
				//alert('Into Function '+' name : '+name);
				var s_level1 = nlapiGetFieldText('custbody_tjinc_tifnfi_srmanagerapprove');
				var s_level2 = nlapiGetFieldText('custbody_tjinc_tifnfi_directorapprover');
				var s_level3 = nlapiGetFieldText('custbody_tjinc_tifnfi_vicepresidentapp');
				var s_cfo =  nlapiGetFieldText('custbody_tjinc_tifnfi_exec1approver');
				var s_coo = nlapiGetFieldText('custbody_tjinc_tifnfi_exec2approver');
				
				if(isNotNull(s_level1) || isNotNull(s_level2) || isNotNull(s_level3) || isNotNull(s_cfo) || isNotNull(s_coo) ){
					var b_resultVal = TJINC_FieldsValidation(s_level1, s_level2, s_level3, s_cfo, s_coo);
					//alert('Into Function Part 2 '+' b_resultVal : '+b_resultVal);
					if(!b_resultVal){
						alert('The system detected a duplicate approver. Remove the duplicate approver to proceed.');
					}
				}
				break;
				
		}
	}
	catch (err) {
		alert(err);
	}
}

function TJINC_FieldsValidation(s_level1, s_level2, s_level3, s_cfo, s_coo){
	
	var b_resultValidation = true;
	var o_uniqueVal = {};
	var a_valuesToRev = new Array();
	if(isNotNull(s_level1)) a_valuesToRev.push(cleanStringForReport(s_level1));
	if(isNotNull(s_level2)) a_valuesToRev.push(cleanStringForReport(s_level2));
	if(isNotNull(s_level3)) a_valuesToRev.push(cleanStringForReport(s_level3));
	if(isNotNull(s_cfo)) a_valuesToRev.push(cleanStringForReport(s_cfo));
	if(isNotNull(s_coo)) a_valuesToRev.push(cleanStringForReport(s_coo));
	
	//alert('a_valuesToRev : '+a_valuesToRev.length);
	
	if(a_valuesToRev.length > 1 ){
		
		for(var i = 0 ; i < a_valuesToRev.length ; i++){
			
			//alert('Value i : '+i+' This is the array Value :'+a_valuesToRev[i]+' Object Value: '+ o_uniqueVal[a_valuesToRev[i]]);
			if(isNull(o_uniqueVal[a_valuesToRev[i]])){
				o_uniqueVal[a_valuesToRev[i]] = a_valuesToRev[i];
			}
			else{
				b_resultValidation = false;
				break;
			}
		}	
	}
	
	return b_resultValidation;
}


function cleanStringForReport(s_stringCSV){
	
	if(isNotNull(s_stringCSV)){
		
		s_stringCSV = s_stringCSV.replace(/'/g,"");
		s_stringCSV = s_stringCSV.replace(/"/g,"");
		s_stringCSV = s_stringCSV.replace(/&/g,"");  
		s_stringCSV = s_stringCSV.replace(/\r/g,"");
		s_stringCSV = s_stringCSV.replace(/\n/g,"");
		s_stringCSV = s_stringCSV.replace(/\s/g,"");
		s_stringCSV = s_stringCSV.replace(/[^a-zA-Z 0-9,.]+/g, "");
	}
	return s_stringCSV;
}


/*
function isNotNull(checkThis) {
	if (checkThis == null) return false;
	else return (trim(checkThis != null) && (trim(checkThis) != ''));
}

function trim(stringToTrim) {
	if(stringToTrim == null) return stringToTrim;
	return stringToTrim.toString().replace(new RegExp("/^\s+|\s+$/g"),"");
}

function isNull(checkThis) {
	return !(isNotNull(checkThis));
}

function getErrorMsg(errorObj)
{
	var errMessage = errorObj;
	if (errMessage == '[object nlobjError]') return trim(errMessage.getDetails() + errMessage.getStackTrace());
	else return errorObj;
}

function isNotJsonNull(checkThis) {
	var isN = true;
	try { for (i in checkThis) {
			isN = false;
			break;}} 
	catch (e) {}
	return !isN;
}
*/