/*Date Modified 
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:23072013
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/


function getCustomEmailtemplate(flag,recordObj)
{
    //------- Load Custom record for Email template
    var emailtemplateObj=nlapiLoadRecord('customrecord_tiffprocurementapprovaem',2);
    var EmailTemplate='';   
    if(flag==3)/// For Approver Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveremailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);      
        EmailTemplate = EmailTemplate.replace('DATEVALUE',recordObj.getFieldValue('trandate'));
        //EmailTemplate = EmailTemplate.replace('PERIODVALUE','');
        EmailTemplate = EmailTemplate.replace('NUMBER',recordObj.getFieldValue('tranid'));
        EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldText('entity'));
        //EmailTemplate = EmailTemplate.replace('MEMO',recordObj.getFieldValue('memo'));
		var memo1=recordObj.getFieldValue('memo')
		if(memo1==null){memo1=''}
		EmailTemplate = EmailTemplate.replace('MEMO',memo1);
        EmailTemplate = EmailTemplate.replace('AMOUNT',recordObj.getFieldValue('custbody_tjinc_tifnfi_prnetnetamount'));//custbody_tjinc_tifnfi_prnetnetamount
        EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
        EmailTemplate = EmailTemplate.replace('RECEIVED','Received');       
        
        var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
        var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
        
        EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
        return EmailTemplate;
        
    }//flag!=3
    
    if(flag==1)/// For Approved Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveemailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);
    }
    else if(flag==2) /// For Reject template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_rejectemailtemplate');
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','rejectEmailTemplate='+EmailTemplate);
        //alert("EmailTemplate="+EmailTemplate);
        var rejReason=recordObj.getFieldValue('custbody_tjinc_tifnfi__rejectionreason');
        if(rejReason==null || rejReason=='' || rejReason==undefined){rejReason='';}
        EmailTemplate = EmailTemplate.replace('REJECTREASON',rejReason);
    }   
    //20062013
	var memo=recordObj.getFieldValue('memo')
	if(memo==null){memo=''}
	EmailTemplate = EmailTemplate.replace('MEMO',memo);
    EmailTemplate = EmailTemplate.replace('PONUMBER',recordObj.getFieldValue('tranid'));    
    EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldValue('custbody_vendorname'));     
    EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
    
    var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
    var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
    
    EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
    //alert("EmailTemplate="+EmailTemplate);
    return EmailTemplate;
}//function getCustomEmailtemplate
