/*Date Modified 15072013 23072013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20121912
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

//pending approval 3
//Approved 1
//NA 4
//Rejected 2

function TJINC_TIFF_BackDoorEmailNotification(type)
{
	nlapiLogExecution('DEBUG', 'AfterSubmit', ' type= ' + type);
	if(type!='delete' && type=='edit')
	{
	var role=nlapiGetRole();
	if(role=='3')// 3 means it's work for the NS Administartor only
	{	
	
	var o_OldObj = nlapiGetOldRecord();
	var i_NewRecordId = nlapiGetRecordId();
	var s_RecordType=nlapiGetRecordType();
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'New RecordId= ' + i_NewRecordId);
	nlapiLogExecution('DEBUG', 'AfterSubmit', 's_RecordType= ' + s_RecordType);
	
	  // This object will get all the old status of the Levels
      var old_Level_Status_Obj=new Object();
	  //getting fields data through old object
	  old_Level_Status_Obj.Level1 = o_OldObj.getFieldValue('custbody_tjinc_tifnfi_l1status');
      old_Level_Status_Obj.Level2 = o_OldObj.getFieldValue('custbody2');
      old_Level_Status_Obj.Level3 = o_OldObj.getFieldValue('custbody_tjinc_tifnfi_l3status');
      old_Level_Status_Obj.CFO = o_OldObj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
      old_Level_Status_Obj.COO = o_OldObj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
	
	
	
	if(_Validation(i_NewRecordId))
	{
			var recordObj=nlapiLoadRecord(s_RecordType,i_NewRecordId);
			var i_NextApprover=recordObj.getFieldValue('nextapprover');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_NextApprover= ' + i_NextApprover); //
			var i_approvalstatus=recordObj.getFieldValue('approvalstatus');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_approvalstatus= ' + i_approvalstatus);			
			var i_CustomForm=recordObj.getFieldValue('customform');
			nlapiLogExecution('DEBUG', 'AfterSubmit', 'i_CustomForm= ' + i_CustomForm);
			if(i_CustomForm=='122')//Custom Form: TIFF Purchase Order-BACKDOOR
			{
			  
			  		var s_l1status = recordObj.getFieldValue('custbody_tjinc_tifnfi_l1status');
				    var s_custbody2 = recordObj.getFieldValue('custbody2');
				    var s_l3status = recordObj.getFieldValue('custbody_tjinc_tifnfi_l3status');
				    var s_cfostatus = recordObj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
				    var s_coo = recordObj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
								
					 // This object will get all the status of the Levels
				    var Level_Status_Obj=new Object();
				    Level_Status_Obj.Level1 = s_l1status;
				    Level_Status_Obj.Level2 = s_custbody2;
				    Level_Status_Obj.Level3 = s_l3status;
				    Level_Status_Obj.CFO    = s_cfostatus;
				    Level_Status_Obj.COO    = s_coo;
					
					// This object is used to get all the Levels data (i.e. Approver)
				    LevelsObj=new Object();
				    LevelsObj.Level1=recordObj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
				    LevelsObj.Level2=recordObj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
				    LevelsObj.Level3=recordObj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
				    LevelsObj.CFO=recordObj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
				    LevelsObj.COO=recordObj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
					
					//--------------------------------------------------------------------------------------------------------------------------------
					///rejected PR Administrator email notification send to PR Administrator and PR Requester & Approver 
					//else if(i_approvalstatus=='3' &&((old_Level_Status_Obj.Level1 == '3' && Level_Status_Obj.Level1 == '2') || (old_Level_Status_Obj.Level2 == '3' && Level_Status_Obj.Level2 == '2') || (old_Level_Status_Obj.Level3 == '3' && Level_Status_Obj.Level3 == '2') || (old_Level_Status_Obj.CFO == '3' && Level_Status_Obj.CFO == '2') || (old_Level_Status_Obj.COO == '3' && Level_Status_Obj.COO == '2')))
					if(i_approvalstatus=='3' &&((old_Level_Status_Obj.Level1 == '3' && Level_Status_Obj.Level1 == '2') || (old_Level_Status_Obj.Level2 == '3' && Level_Status_Obj.Level2 == '2') || (old_Level_Status_Obj.Level3 == '3' && Level_Status_Obj.Level3 == '2') || (old_Level_Status_Obj.CFO == '3' && Level_Status_Obj.CFO == '2') || (old_Level_Status_Obj.COO == '3' && Level_Status_Obj.COO == '2')))
					{
						if(recordObj.getFieldValue('custbody_tjinc_tfinfi_submitwsaved')=='T' )
						{
							 nlapiLogExecution('DEBUG', 'AfterSubmit', 'I am in Rejection condition');			
						   
						     RejectEmailsNotification(recordObj,old_Level_Status_Obj,Level_Status_Obj);
							
						}//if						
						
					}//if				 
		             
					 //send Approved Email Notification to PR Administrator and PR Requester Logic Note: In this case the NS Administrator manually set the standard Approval status field to Approved.
					 if(i_approvalstatus=='2') ///2 means Approved
						SendEmailToPRAdministrator(recordObj);
			  	
			}//if i_CustomForm
	
	}//if i_NewRecordId
	}//if role
}//if type 

}//fun

//Updated : 23072013 for this below function used Libray file.
/*
function getCustomEmailtemplate(flag,recordObj)
{
    //------- Load Custom record for Email template
    var emailtemplateObj=nlapiLoadRecord('customrecord_tiffprocurementapprovaem',2);
    var EmailTemplate='';   
    if(flag==3)//flag 3 is approver flag
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveremailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);      
        EmailTemplate = EmailTemplate.replace('DATEVALUE',recordObj.getFieldValue('trandate'));
        //EmailTemplate = EmailTemplate.replace('PERIODVALUE','');
        EmailTemplate = EmailTemplate.replace('NUMBER',recordObj.getFieldValue('tranid'));
        EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldText('entity'));
        //EmailTemplate = EmailTemplate.replace('MEMO',recordObj.getFieldValue('memo'));
		var memo1=recordObj.getFieldValue('memo')
		if(memo1==null){memo1=''}
		EmailTemplate = EmailTemplate.replace('MEMO',memo1);
        EmailTemplate = EmailTemplate.replace('AMOUNT',recordObj.getFieldValue('custbody_tjinc_tifnfi_prnetnetamount'));//custbody_tjinc_tifnfi_prnetnetamount
        EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
        EmailTemplate = EmailTemplate.replace('RECEIVED','Received');       
        
        var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
        var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
        
        EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
        return EmailTemplate;
        
    }//flag!=3
    
    if(flag==1)/// For Approval template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveemailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);
    }
    else if(flag==2) /// For Reject template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_rejectemailtemplate');
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','rejectEmailTemplate='+EmailTemplate);
        //alert("EmailTemplate="+EmailTemplate);
        var rejReason=recordObj.getFieldValue('custbody_tjinc_tifnfi__rejectionreason');
        if(rejReason==null || rejReason=='' || rejReason==undefined){rejReason='';}
        EmailTemplate = EmailTemplate.replace('REJECTREASON',rejReason);
    }   
    
	var memo=recordObj.getFieldValue('memo')
	if(memo==null){memo=''}
	EmailTemplate = EmailTemplate.replace('MEMO',memo);
    EmailTemplate = EmailTemplate.replace('PONUMBER',recordObj.getFieldValue('tranid'));    
    EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldValue('custbody_vendorname'));     
    EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
    
    var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
    var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
    
    EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
    //alert("EmailTemplate="+EmailTemplate);
    return EmailTemplate;
}//function getCustomEmailtemplate
*/

function SendEmailToPRAdministrator(PR_Obj)
{
	         
                     /*
                      var approvedflag=1;
                      //var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
                      var PR_Adminstrator=PR_Obj.getFieldValue('employee'); 
                      var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');                             
                      var approvedEmailTemplate=getCustomEmailtemplate(approvedflag,PR_Obj);                     
                      var firstName=PR_Obj.getFieldValue('custbody_administratorfirstname');                      
                      var to_email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');                 
                      approvedEmailTemplate = approvedEmailTemplate.replace('USER',firstName);
                      var ApprovedSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject');
                       if(_Validation(to_email))
                            nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);  
                       */							
					
					 if(PR_Obj!=null)
                     { 
                      var approvedflag=1;
                      //var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
                      var PR_Adminstrator=PR_Obj.getFieldValue('employee'); 
                      var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');                             
                      var approvedEmailTemplate=getCustomEmailtemplate(approvedflag,PR_Obj);
					  var approvedEmailTemplate_Requester=approvedEmailTemplate;                    
                      var firstName=PR_Obj.getFieldValue('custbody_administratorfirstname');  
					  //alert("Approved Email sent PR Administrator="+firstName);                    
                      var to_email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');                 
                      approvedEmailTemplate = approvedEmailTemplate.replace('USER',firstName);
                      var ApprovedSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject');
                      if (PR_Adminstrator == PR_RequestingEmployee) 
                      {                        
                        if(_Validation(to_email))
                            nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);							
                       }//if
                       else
                       {
                         //----------- Send Approved Email to PR- Administrator
                         if(_Validation(to_email))
                           nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                         //=====Email Requesting Employee =====
                         var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                         //----------- Send Approved Email to Requesting Employee						 
                         approvedEmailTemplate_Requester = approvedEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));						 
                         if(_Validation(to_Email_ReqEmp))                         
                             nlapiSendEmail('645',to_Email_ReqEmp,ApprovedSubject, approvedEmailTemplate_Requester);
                       }
                   }//if		           
	
}//fun

function RejectEmailsNotification(PR_Obj,old_Level_Status_Obj,Level_Status_Obj)
{	
            //Get custom email template of rejection    
            var rejectflag=2;           
            var rejectEmailTemplate =getCustomEmailtemplate(rejectflag,PR_Obj);
			var rejectEmailTemplate_Bckp=rejectEmailTemplate;
			var rejectEmailTemplate_Requester=rejectEmailTemplate;
            var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');   
            //---- Reject Email send to the Administrator and Requesting Employee By Default.   
            var PR_Adminstrator=PR_Obj.getFieldValue('employee');
            var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');
            if(PR_Adminstrator==PR_RequestingEmployee)
            {              
                  var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                  rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));                  
                  if(_Validation(to_Email))
                     nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator   
                     
					 nlapiLogExecution('DEBUG','*************Back Door Email Notification Reject Email','One email sent');
            }
            else
            {                       
                   //=====Rejected Email Requesting Employee =====
                   var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                   rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));                      
                   if(_Validation(to_Email_ReqEmp))
                        nlapiSendEmail('645',to_Email_ReqEmp,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                         nlapiLogExecution('DEBUG','*************Back Door Email Notification Reject Email','email sent to PR Requester');
                   //=====Rejected Email Administartor =====
                   var to_Email_Admin=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                   rejectEmailTemplate_Requester = rejectEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));               
                 
                  if(_Validation(to_Email_Admin))
                   nlapiSendEmail('645',to_Email_Admin,RejectedEmailSubject,rejectEmailTemplate_Requester); /// Adminstator 
                   nlapiLogExecution('DEBUG','*************Back Door Email Notification Reject Email','email sent to PR Administrator');              
            }//else
			
			var levelsFlag=0;
			if(old_Level_Status_Obj.Level1 == '3' && Level_Status_Obj.Level1 == '2'){levelsFlag=1;}			
			else if(old_Level_Status_Obj.Level2 == '3' && Level_Status_Obj.Level2 == '2'){levelsFlag=2;}
			else if(old_Level_Status_Obj.Level3 == '3' && Level_Status_Obj.Level3 == '2'){levelsFlag=3;}
			else if(old_Level_Status_Obj.CFO == '3' && Level_Status_Obj.CFO == '2'){levelsFlag=4;}
			else if(old_Level_Status_Obj.COO == '3' && Level_Status_Obj.COO == '2'){levelsFlag=5;}
		nlapiLogExecution('DEBUG','*************Back Door Email Notification Reject Email','levelsFlag='+levelsFlag);	
		for(var i=1;i<levelsFlag;i++)
            {
                
                if(i==1)
                {
                    var level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
                    if(_Validation(level1))
                    {
                        var to_Email=PR_Obj.getFieldValue('custbody_level1email');
						var rejectEmailTemplate_level1=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level1 = rejectEmailTemplate_level1.replace('USER',PR_Obj.getFieldValue('custbody_level1firstname'));     
                        if(_Validation(to_Email))
                           nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level1);//RejectedEmailSubject
                    }
                    
                }//if i =1
                else if(i==2)
                {
                    var level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
                    if(_Validation(level2))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level2email');
						var rejectEmailTemplate_level2=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level2 = rejectEmailTemplate_level2.replace('USER',PR_Obj.getFieldValue('custbody_level2firstname'));
						if(_Validation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level2);
                    }
                    
                }//if i =2
                else if(i==3)
                {
                    var level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
                    if(_Validation(level3))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level3email');
						var rejectEmailTemplate_level3=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level3 = rejectEmailTemplate_level3.replace('USER',PR_Obj.getFieldValue('custbody_level3firstname'));                 
                        if(_Validation(to_Email))
                            nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level3);
                    }
                    
                }//if i =3
                else if(i==4)
                {
                    var level4=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
                    if(_Validation(level4))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_cfoemail');
						var rejectEmailTemplate_level4=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level4 = rejectEmailTemplate_level4.replace('USER',PR_Obj.getFieldValue('custbody_cfofirstname'));                        
                        if(_Validation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level4);
                    }
                    
                }//if i =4
                else if(i==5)
                {
                    var level5=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
                    if(_Validation(level5))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofcoo');
						var rejectEmailTemplate_level5=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level5 = rejectEmailTemplate_level5.replace('USER',PR_Obj.getFieldValue('custbody_coofirstname'));                        
                        if(_Validation(to_Email))
                        nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level5);
                    }
                    
                }//else if i =5         
                
            }//for
			
}//fun RejectEmailsNotification



function _Validation(value)
{
	if(value!=null && value!='' && value!=' ' && value!=undefined){return 1;}
	return 0;
}//fun
