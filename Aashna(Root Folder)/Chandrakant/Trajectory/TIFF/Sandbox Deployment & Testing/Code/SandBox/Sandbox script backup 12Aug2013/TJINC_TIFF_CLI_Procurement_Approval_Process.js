/*Date Modified 23072013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/
//--------- Global Variables ----------
     var NA ='4'
     var PendingApproval ='3';
     var Rejected ='2';
     var Approved ='1';


///--------- This is the Client Script function without deployment
function ApproveBtnCall(recordId,levelsFlag)
{
    ////alert("recordId="+recordId);
    ////alert("levelsFlag="+levelsFlag);
    var checkSupervisorApprovalFlag=0;
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
        
        if(_LogValidation(PR_Obj))
        {
             
            // This object will get all the status of the Levels
            Level_Status_Obj=new Object();
            Level_Status_Obj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l1status');
            Level_Status_Obj.Level2=PR_Obj.getFieldValue('custbody2');
            Level_Status_Obj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l3status');
            Level_Status_Obj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
            Level_Status_Obj.COO=PR_Obj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
            var level_status_id='';
            if(levelsFlag==1)
            {
                level_status_id='custbody_tjinc_tifnfi_l1status'
            }
            else if(levelsFlag==2)
            {
                level_status_id='custbody2'
            }
             else if(levelsFlag==3)
            {
                level_status_id='custbody_tjinc_tifnfi_l3status'
            }
            else if(levelsFlag==4)
            {
                level_status_id='custbody_tjinc_tifnfi_cfostatus'
            }
            else if(levelsFlag==5)
            {
                level_status_id='custbodycustbody_tjinc_tifnfi_coo'
            }
            if(level_status_id!='')
               PR_Obj.setFieldValue(level_status_id,Approved);      
                    
             
             ///------------- This logic is used when we have to set standard 
            LevelsObj=new Object();
            LevelsObj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
            LevelsObj.Level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
            LevelsObj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
            LevelsObj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
            LevelsObj.COO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
            
            if(_LogValidation(LevelsObj.Level1))
            {
                checkSupervisorApprovalFlag=1;
            }
            if(_LogValidation(LevelsObj.Level2))
            {
                checkSupervisorApprovalFlag=2;
            }
            if(_LogValidation(LevelsObj.Level3))
            {
                checkSupervisorApprovalFlag=3;
            }
            if(_LogValidation(LevelsObj.CFO))
            {
                checkSupervisorApprovalFlag=4;
            }
            if(_LogValidation(LevelsObj.COO))
            {
                checkSupervisorApprovalFlag=5;
            }
            
             if(levelsFlag==checkSupervisorApprovalFlag)
             {          
                PR_Obj.setFieldValue('approvalstatus','2'); /// 2 means Approved
                //PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);                
                   if(submitPR_ID!=null)
                   { 
                      var approvedflag=1;
                      var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
                      var PR_Adminstrator=PR_Obj.getFieldValue('employee'); 
                      var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');                             
                      var approvedEmailTemplate=getCustomEmailtemplate(approvedflag,PR_Obj);
					  var approvedEmailTemplate_Requester=approvedEmailTemplate;                    
                      var firstName=PR_Obj.getFieldValue('custbody_administratorfirstname');  
					  //alert("Approved Email sent PR Administrator="+firstName);                    
                      var to_email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');                 
                      approvedEmailTemplate = approvedEmailTemplate.replace('USER',firstName);
                      var ApprovedSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject');
                      if (PR_Adminstrator == PR_RequestingEmployee) 
                      {                   
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approved Email',approvedEmailTemplate); ///Approved Email
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com', nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject'), approvedEmailTemplate);
                        //---------uPDATE 21012013
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                        if(_LogValidation(to_email))
                            nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
							//alert("PR_Adminstrator == PR_RequestingEmployee equal sent email");
                       }//if
                       else
                       {
                         //----------- Send Approved Email to PR- Administrator
                         //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
						 //alert("PR_Adminstrator template before sending an email"+approvedEmailTemplate);
                         if(_LogValidation(to_email))
                           nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                         //=====Email Requesting Employee =====
                         var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                         //----------- Send Approved Email to Requesting Employee
						 //alert("PR_Requester="+PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));
                         approvedEmailTemplate_Requester = approvedEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));
						 //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                         if(_LogValidation(to_Email_ReqEmp))                         
                             nlapiSendEmail('645',to_Email_ReqEmp,ApprovedSubject, approvedEmailTemplate_Requester);
                       }
                   }//if
                    location.reload(true);
                
             }//if  
             else
             {
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);
                location.reload(true);
             }  
        }//if
        
    }//if recordId
    
}//fun ApproveBtnCall

///=================================== Reject Btn Function ==============================
function RejectBtnCall(recordId,levelsFlag)
{   
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);       
        if(_LogValidation(PR_Obj))
        {
            var reasonForRejection=prompt("Please enter reason for rejection.");            
            PR_Obj.setFieldValue('custbody_tjinc_tifnfi__rejectionreason',reasonForRejection);
            PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
            PR_Obj.setFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
            PR_Obj.setFieldValue('approvalstatus','3'); //approvalstatus is the Approval Status and 3 is for Rejected
                                    
            if(levelsFlag==1){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l1status',2);}            
            else if(levelsFlag==2){PR_Obj.setFieldValue('custbody2',2);}            
            else if(levelsFlag==3){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l3status',2);}           
            else if(levelsFlag==4){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_cfostatus',2);}          
            else if(levelsFlag==5){PR_Obj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo',2);}    
            
            var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);            
            //location.reload(true);
            
            var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
            //Get custom email template of rejection    
            var rejectflag=2;           
            var rejectEmailTemplate =getCustomEmailtemplate(rejectflag,PR_Obj);
			var rejectEmailTemplate_Bckp=rejectEmailTemplate;
			var rejectEmailTemplate_Requester=rejectEmailTemplate;
            var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');   
            //---- Reject Email send to the Administrator and Requesting Employee By Default.   
                var PR_Adminstrator=PR_Obj.getFieldValue('employee');
                var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');
                if(PR_Adminstrator==PR_RequestingEmployee)
                {              
                       var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Email',rejectEmailTemplate); /// Adminstator   
                      // var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                      
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email))
                         nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator   
                }
                else
                {
                        //var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                        //=====Email Requesting Employee =====
                       var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));                
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Requesting Employee Email',rejectEmailTemplate); /// Adminstator
                       
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       if(_LogValidation(to_Email_ReqEmp))
                            nlapiSendEmail('645',to_Email_ReqEmp,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       //=====Email Administartor =====
                       var to_Email_Admin=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate_Requester = rejectEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));                 
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Administartor Email',rejectEmailTemplate); /// Adminstator 
                      
                      //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email_Admin))
                       nlapiSendEmail('645',to_Email_Admin,RejectedEmailSubject,rejectEmailTemplate_Requester); /// Adminstator               
                }       
    
            for(var i=1;i<levelsFlag;i++)
            {
                
                if(i==1)
                {
                    var level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
                    if(_LogValidation(level1))
                    {
                        var to_Email=PR_Obj.getFieldValue('custbody_level1email');
						var rejectEmailTemplate_level1=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level1 = rejectEmailTemplate_level1.replace('USER',PR_Obj.getFieldValue('custbody_level1firstname'));                     
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level1 Email',rejectEmailTemplate);//
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);//RejectedEmailSubject
                        if(_LogValidation(to_Email))
                           nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level1);//RejectedEmailSubject
                    }
                    
                }//if i =1
                else if(i==2)
                {
                    var level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
                    if(_LogValidation(level2))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level2email');
						var rejectEmailTemplate_level2=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level2 = rejectEmailTemplate_level2.replace('USER',PR_Obj.getFieldValue('custbody_level2firstname'));
						                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level2 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level2);
                    }
                    
                }//if i =2
                else if(i==3)
                {
                    var level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
                    if(_LogValidation(level3))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level3email');
						var rejectEmailTemplate_level3=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level3 = rejectEmailTemplate_level3.replace('USER',PR_Obj.getFieldValue('custbody_level3firstname'));                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level3 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                            nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level3);
                    }
                    
                }//if i =3
                else if(i==4)
                {
                    var level4=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
                    if(_LogValidation(level4))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_cfoemail');
						var rejectEmailTemplate_level4=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level4 = rejectEmailTemplate_level4.replace('USER',PR_Obj.getFieldValue('custbody_cfofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level4 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level4);
                    }
                    
                }//if i =4
                else if(i==5)
                {
                    var level5=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
                    if(_LogValidation(level5))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofcoo');
						var rejectEmailTemplate_level5=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level5 = rejectEmailTemplate_level5.replace('USER',PR_Obj.getFieldValue('custbody_coofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level5 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                        nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level5);
                    }
                    
                }//else if i =5         
                
            }//for
        }//if       
    }//if   
    location.reload(true);
}//fun RejectBtnCall


function getCustomEmailtemplate(flag,recordObj)
{
    //------- Load Custom record for Email template
    var emailtemplateObj=nlapiLoadRecord('customrecord_tiffprocurementapprovaem',2);
    var EmailTemplate='';   
    if(flag==3)/// For Approver Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveremailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);      
        EmailTemplate = EmailTemplate.replace('DATEVALUE',recordObj.getFieldValue('trandate'));
        //EmailTemplate = EmailTemplate.replace('PERIODVALUE','');
        EmailTemplate = EmailTemplate.replace('NUMBER',recordObj.getFieldValue('tranid'));
        EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldText('entity'));
        //EmailTemplate = EmailTemplate.replace('MEMO',recordObj.getFieldValue('memo'));
		var memo1=recordObj.getFieldValue('memo')
		if(memo1==null){memo1=''}
		EmailTemplate = EmailTemplate.replace('MEMO',memo1);
        EmailTemplate = EmailTemplate.replace('AMOUNT',recordObj.getFieldValue('custbody_tjinc_tifnfi_prnetnetamount'));//custbody_tjinc_tifnfi_prnetnetamount
        EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
        EmailTemplate = EmailTemplate.replace('RECEIVED','Received');       
        
        var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
        var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
        
        EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
        return EmailTemplate;
        
    }//flag!=3
    
    if(flag==1)/// For Approved Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveemailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);
    }
    else if(flag==2) /// For Reject template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_rejectemailtemplate');
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','rejectEmailTemplate='+EmailTemplate);
        //alert("EmailTemplate="+EmailTemplate);
        var rejReason=recordObj.getFieldValue('custbody_tjinc_tifnfi__rejectionreason');
        if(rejReason==null || rejReason=='' || rejReason==undefined){rejReason='';}
        EmailTemplate = EmailTemplate.replace('REJECTREASON',rejReason);
    }   
    //20062013
	var memo=recordObj.getFieldValue('memo')
	if(memo==null){memo=''}
	EmailTemplate = EmailTemplate.replace('MEMO',memo);
    EmailTemplate = EmailTemplate.replace('PONUMBER',recordObj.getFieldValue('tranid'));    
    EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldValue('custbody_vendorname'));     
    EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
    
    var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
    var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
    
    EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
    //alert("EmailTemplate="+EmailTemplate);
    return EmailTemplate;
}//function getCustomEmailtemplate


//----- Log validation function
function _LogValidation(value)
{
    if(value!=null && value!='' && value!=' ' && value!=undefined)
    {
       return 1;    
    }     
    else
    {
        return 0;   
    }
     
}//fun

