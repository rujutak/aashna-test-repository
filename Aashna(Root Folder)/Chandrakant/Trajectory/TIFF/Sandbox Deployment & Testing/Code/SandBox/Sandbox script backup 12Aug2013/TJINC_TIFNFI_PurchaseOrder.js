/**
* Copyright (c) 2012 Trajectory Group Inc. / Kuspide Canada Inc.
* 76 Richmond St. East, Suite 400, Toronto, ON, Canada, M5C 1P1 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: TIFF
* @Company: Trajectory Inc.  
* @CreationDate: 20121221
* @DocumentationUrl: 
* @NamingStandard: TJINC_NSJ-1-2
*/

function TJINC_Recalc_PurchaseOrder (type, name) {
	var b_LogTJDev = false;
	//if (navigator.userAgent == "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:17.0) Gecko/20100101 Firefox/17.0") b_LogTJDev = true;	
	
	if (navigator.userAgent == "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17") b_LogTJDev = true;
	if (b_LogTJDev) console.log("In Recalc type = " + type + ", name = " + name);
	
	if ((type == 'item') && (name == "commit")) {
		nlapiSetFieldValue('custbody_tjinc_validateapprovers', 'T');
		if (b_LogTJDev) console.log("custbody_tjinc_validateapprovers set to true.");
	}
	
	/*if((type == 'item') && (name == "remove")){
		//nlapiSetFieldValue('memo', name,true, false);
		nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove','');
		nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover','');
		nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp','');
		nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '');
		nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '');
		nlapiCommitLineItem(type);
		if (b_LogTJDev) console.log("5 fields set to blank.");
		
	}
	
	nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', '',false,false);
	nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', '',false,false);
	nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp', '',false,false);
	nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '',false,false);
	nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '',false,false);*/
}