/*Date Modified 29122012 02012012 04 11012013 21012013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20121912
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

function TJINC_NSJ_AfterSubmitPR(type){
	var o_OldObj = nlapiGetOldRecord();
	var i_NewRecordId = nlapiGetRecordId();
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'New RecordId= ' + i_NewRecordId);
	if (o_OldObj != null && o_OldObj != '') {
		var s_OldRecordStatus = o_OldObj.getFieldValue('status');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Old Record Status= ' + s_OldRecordStatus);
		var s_OldRecordOrderstatus = o_OldObj.getFieldValue('orderstatus');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Old Record s_OldRecordOrderstatus= ' + s_OldRecordOrderstatus);
		if (s_OldRecordStatus == 'Closed') {
			//var i_NewRecordId = nlapiGetRecordId();
			//nlapiLogExecution('DEBUG', 'AfterSubmit', 'New RecordId= ' + i_NewRecordId);
			if (i_NewRecordId != null) {
				var o_NewRecordObj = nlapiLoadRecord('purchaseorder', i_NewRecordId);
				var o_NewRecordStatus = o_NewRecordObj.getFieldValue('status');
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'New Record Status= ' + o_NewRecordStatus);
				if (s_OldRecordStatus != o_NewRecordStatus) {
					o_NewRecordObj.setFieldValue('approvalstatus', 1);
					try {
						nlapiSubmitRecord(o_NewRecordObj, false, true);
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'Status seted as Pending Approval');
					} 
					catch (ex) {
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'Try Catch Exception= ' + ex);
					}
				}
			}
		}//if
		else if(s_OldRecordOrderstatus=='C')//C means rejected
		{
			var i_flag=0;
			var o_NewRecordObj = nlapiLoadRecord('purchaseorder', i_NewRecordId);
			var o_NewRecordOrderStatus = o_NewRecordObj.getFieldValue('orderstatus');
			if(o_NewRecordOrderStatus=='C'){
				var i_LineCount=o_NewRecordObj.getLineItemCount('item');
				for(var i=1;i_LineCount!=null && i<=i_LineCount;i++){
					var b_IsClosed=nlapiGetLineItemValue('item','isclosed',i);
					if(b_IsClosed=='T'){
						i_flag=1;
						break;
					}
				}//for i
				if(i_flag!=1){
					o_NewRecordObj.setFieldValue('approvalstatus', 1);
					nlapiSubmitRecord(o_NewRecordObj, false, true);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'Status seted as Pending Approval');
				}
			}//if
			
		}//else if
		
	}
	return true;
}
