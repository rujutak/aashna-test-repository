/*Date Modified 
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:17072013
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/

var b_PageInitSaveASDraft;
function SaveAsDraftPageInit(type)
{
	b_PageInitSaveASDraft=nlapiGetFieldValue('custbody_tjinc_tfinfi_submitwsaved');
}


function SaveAsDraftSaveRecord()
{
	//alert('mySaveRecord b_PageInitSaveASDraft='+b_PageInitSaveASDraft);
	var i_customform=nlapiGetFieldValue('customform');//122 is a cusom form'TIFF Purchase Order - BACKDOOR' in sandbox
	var i_approvalstatus=nlapiGetFieldValue('approvalstatus');
	var i_role=nlapiGetRole();
	var b_OnSave_SaveASDraft=nlapiGetFieldValue('custbody_tjinc_tfinfi_submitwsaved');
	if(i_customform=='122' && i_approvalstatus=='3' && i_role=='3' && b_PageInitSaveASDraft=='F'){nlapiSetFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');}
	return true;
}
