/*Date Modified 29122012 02012012 04 11012013 21012013 07052013 30052013 20062013 10072013 1107 23072013
* Copyright (c) [year] Trajectory Inc. 
* 165 John St. 3rd Floor, Toronto, ON, Canada, M5T 1X3 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/* 
* @System: [Name of the system which is part this class, and the url for the documentation]
* @Company: Trajectory Inc. / Kuspide Canada Inc. 
* @CreationDate:20121912
* @DocumentationUrl: [Url of the page that has the general description of the functionality] 
* @NamingStandard: TJINC_NSJ-1-2
*/
//--------- Global Variables ----------
     var NA ='4'
     var PendingApproval ='3';
     var Rejected ='2';
     var Approved ='1';
///------------ Before load function used to disable the standard edit button and create the customize Approve and Reject Button
function TJINC_NSJ_BeforeLoadProcurementApprovalP(type,form)
{
	/***THIS Part is inactive by Carlos Request.
    if(type == 'view' || type == 'create' || type == 'edit'){
        var o_filedEnt = form.getField('entity');
        o_filedEnt.setDisplayType('hidden');
        
    }
    ***/
    nlapiLogExecution('DEBUG','*************Before Load','type='+type);
    var ApprovalProcessFlag=nlapiGetFieldValue('custbody_tjinc_tifnfi_approvalprocessf');
    var PR_Administrator=nlapiGetFieldValue('employee');
    var Current_Role=nlapiGetRole();
    var ApprovalStatus=nlapiGetFieldValue('approvalstatus');
    if(ApprovalProcessFlag=='T')
    {
          var recordID=nlapiGetRecordId();
          var recordType=nlapiGetRecordType();
          var currentUser=nlapiGetUser(); /// Get current user id
          
          //Set Client script
          form.setScript('customscript_tjinctiffcliprocurement');
          
          var buttonOBJ=form.getButton('edit');
         // nlapiLogExecution('DEBUG', 'beforeLoadRecord', ' Button Object ==' + buttonOBJ);
         // Admin Internal ID = 3 (can edit the PO currently)
         // AP Clerk Internal ID = 1005
          if(Current_Role!='3' && Current_Role !='1005')
          {
             if(PR_Administrator!=nlapiGetUser() || ApprovalStatus=='2')
              {
                if(buttonOBJ!=null)
                  {
                    buttonOBJ.setDisabled(true);
                  }
                
              }//if
              
             /*
              if(approvalstatus=='2') ///approvalstatus=='2' means Approved
              {
                if(buttonOBJ!=null)
                  {
                    buttonOBJ.setDisabled(true);
                  }
              }
             */         
          }//if
         
          
           // This object is used to get all the Levels data (Here you will get the employee id)
            LevelsObj=new Object();
            LevelsObj.Level1=nlapiGetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
            LevelsObj.Level1=ExtraDepartmentFun(LevelsObj.Level1);
            LevelsObj.Level2=nlapiGetFieldValue('custbody_tjinc_tifnfi_directorapprover');
            LevelsObj.Level2=ExtraDepartmentFun(LevelsObj.Level2);
            LevelsObj.Level3=nlapiGetFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
            LevelsObj.Level3=ExtraDepartmentFun(LevelsObj.Level3);
            LevelsObj.CFO=nlapiGetFieldValue('custbody_tjinc_tifnfi_exec1approver');
            LevelsObj.COO=nlapiGetFieldValue('custbody_tjinc_tifnfi_exec2approver');
            
            // This object will get all the status of the Levels
            Level_Status_Obj=new Object();
            Level_Status_Obj.Level1=nlapiGetFieldValue('custbody_tjinc_tifnfi_l1status');
            Level_Status_Obj.Level2=nlapiGetFieldValue('custbody2');
            Level_Status_Obj.Level3=nlapiGetFieldValue('custbody_tjinc_tifnfi_l3status');
            Level_Status_Obj.CFO=nlapiGetFieldValue('custbody_tjinc_tifnfi_cfostatus');
            Level_Status_Obj.COO=nlapiGetFieldValue('custbodycustbody_tjinc_tifnfi_coo');
           
            if((Level_Status_Obj.Level1==PendingApproval) && (LevelsObj.Level1==currentUser))
            {
                if (type == 'view')
                {
                    var Level1_flag=1;
                    form.addButton('custpage_approve', 'Approve', 'ApproveBtnCall(\'' + recordID + '\',\'' + Level1_flag + '\');');
                    form.addButton('custpage_reject', 'Reject', 'RejectBtnCall(\'' + recordID + '\',\'' + Level1_flag + '\');');
                }
                
            }
            else  if((Level_Status_Obj.Level2==PendingApproval) && (LevelsObj.Level2==currentUser))
            {
                if (type == 'view')
                {
                    var Level2_flag=2;
                    form.addButton('custpage_approve', 'Approve', 'ApproveBtnCall(\'' + recordID + '\',\'' + Level2_flag + '\');');
                    form.addButton('custpage_reject', 'Reject', 'RejectBtnCall(\'' + recordID + '\',\'' + Level2_flag + '\');');
                }
                
            }
            else  if((Level_Status_Obj.Level3==PendingApproval) && (LevelsObj.Level3==currentUser))
            {
                if (type == 'view')
                {
                    var Level3_flag=3;
                    form.addButton('custpage_approve', 'Approve', 'ApproveBtnCall(\'' + recordID + '\',\'' + Level3_flag + '\');');
                    form.addButton('custpage_reject', 'Reject', 'RejectBtnCall(\'' + recordID + '\',\'' + Level3_flag + '\');');
                }
                
            }
            else  if((Level_Status_Obj.CFO==PendingApproval) && (LevelsObj.CFO==currentUser))
            {
                if (type == 'view')
                {
                    var LevelCOF_flag=4;
                    form.addButton('custpage_approve', 'Approve', 'ApproveBtnCall(\'' + recordID + '\',\'' + LevelCOF_flag + '\');');
                    form.addButton('custpage_reject', 'Reject', 'RejectBtnCall(\'' + recordID + '\',\'' + LevelCOF_flag + '\');');
                }
                
            }
            else  if((Level_Status_Obj.COO==PendingApproval) && (LevelsObj.COO==currentUser))
            {
                if (type == 'view')
                {
                    var LevelCOO_flag=5;
                    form.addButton('custpage_approve', 'Approve', 'ApproveBtnCall(\'' + recordID + '\',\'' + LevelCOO_flag + '\');');
                    form.addButton('custpage_reject', 'Reject', 'RejectBtnCall(\'' + recordID + '\',\'' + LevelCOO_flag + '\');');
                }
                
            }
           
        //  }//if buttonOBJ
     
	 
	 var ButtonStatus=nlapiGetFieldValue('custbody_buttonstatus'); /// 1 means user click on Approved button , 2 means user click on reject button
	 if(ButtonStatus==1 || ButtonStatus==2)
	 {
	 	try{
			var O_ApproveButton = form.getButton('custpage_approve');		
			O_ApproveButton.setDisabled(true);
			
			var O_RejectButton = form.getButton('custpage_reject');		
			O_RejectButton.setDisabled(true);
			
		}catch(e){}
	 	
	 }
	 
	    
    }//if ApprovalProcessFlag
    
}//fun


///--------- This is the Client Script function without deployment
function ApproveBtnCall(recordId,levelsFlag)
{
    ////alert("recordId="+recordId);
    ////alert("levelsFlag="+levelsFlag);
    var checkSupervisorApprovalFlag=0;
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
        
       /*
 if(_LogValidation(PR_Obj))
        {
             
            // This object will get all the status of the Levels
            Level_Status_Obj=new Object();
            Level_Status_Obj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l1status');
            Level_Status_Obj.Level2=PR_Obj.getFieldValue('custbody2');
            Level_Status_Obj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l3status');
            Level_Status_Obj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
            Level_Status_Obj.COO=PR_Obj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
            var level_status_id='';
            if(levelsFlag==1)
            {
                level_status_id='custbody_tjinc_tifnfi_l1status'
            }
            else if(levelsFlag==2)
            {
                level_status_id='custbody2'
            }
             else if(levelsFlag==3)
            {
                level_status_id='custbody_tjinc_tifnfi_l3status'
            }
            else if(levelsFlag==4)
            {
                level_status_id='custbody_tjinc_tifnfi_cfostatus'
            }
            else if(levelsFlag==5)
            {
                level_status_id='custbodycustbody_tjinc_tifnfi_coo'
            }
            if(level_status_id!='')
               PR_Obj.setFieldValue(level_status_id,Approved);      
                    
             
             ///------------- This logic is used when we have to set standard 
            LevelsObj=new Object();
            LevelsObj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
            LevelsObj.Level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
            LevelsObj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
            LevelsObj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
            LevelsObj.COO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
            
            if(_LogValidation(LevelsObj.Level1))
            {
                checkSupervisorApprovalFlag=1;
            }
            if(_LogValidation(LevelsObj.Level2))
            {
                checkSupervisorApprovalFlag=2;
            }
            if(_LogValidation(LevelsObj.Level3))
            {
                checkSupervisorApprovalFlag=3;
            }
            if(_LogValidation(LevelsObj.CFO))
            {
                checkSupervisorApprovalFlag=4;
            }
            if(_LogValidation(LevelsObj.COO))
            {
                checkSupervisorApprovalFlag=5;
            }
            
             if(levelsFlag==checkSupervisorApprovalFlag)
             {          
                PR_Obj.setFieldValue('approvalstatus','2'); /// 2 means Approved
                //PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);                
                   if(submitPR_ID!=null)
                   { 
                      var approvedflag=1;
                      var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
                      var PR_Adminstrator=PR_Obj.getFieldValue('employee'); 
                      var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');                             
                      var approvedEmailTemplate=getCustomEmailtemplate(approvedflag,PR_Obj);                     
                      var firstName=PR_Obj.getFieldValue('custbody_administratorfirstname');                      
                      var to_email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');                 
                      approvedEmailTemplate = approvedEmailTemplate.replace('USER',firstName);
                      var ApprovedSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject');
                      if (PR_Adminstrator == PR_RequestingEmployee) 
                      {                   
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approved Email',approvedEmailTemplate); ///Approved Email
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com', nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject'), approvedEmailTemplate);
                        //---------uPDATE 21012013
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                        if(_LogValidation(to_email))
                            nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                       }//if
                       else
                       {
                         //----------- Send Approved Email to PR- Administrator
                         //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                         if(_LogValidation(to_email))
                           nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                         //=====Email Requesting Employee =====
                         var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                         //----------- Send Approved Email to Requesting Employee
                         approvedEmailTemplate = approvedEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));
                         //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                         if(_LogValidation(to_Email_ReqEmp))                         
                             nlapiSendEmail('645',to_Email_ReqEmp,ApprovedSubject, approvedEmailTemplate);
                       }
                   }//if
                    location.reload(true);
                
             }//if  
             else
             {
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);
                location.reload(true);
             }  
        }//if
*/
		
		///---new code for increase performance of the script
		
		PR_Obj.setFieldValue('custbody_levelsflag',levelsFlag);
		PR_Obj.setFieldValue('custbody_buttonstatus',1); ///1 means click on Approved button
		var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);
        location.reload(true);
        
    }//if recordId
    
}//fun ApproveBtnCall

///=================================== Reject Btn Function ==============================
function RejectBtnCall(recordId,levelsFlag)
{   
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);  
		var reasonForRejection=prompt("Please enter reason for rejection.");            
        PR_Obj.setFieldValue('custbody_tjinc_tifnfi__rejectionreason',reasonForRejection);     
       /*
 if(_LogValidation(PR_Obj))
        {
            var reasonForRejection=prompt("Please enter reason for rejection.");            
            PR_Obj.setFieldValue('custbody_tjinc_tifnfi__rejectionreason',reasonForRejection);
            PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
            PR_Obj.setFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
            PR_Obj.setFieldValue('approvalstatus','3'); //approvalstatus is the Approval Status and 3 is for Rejected
                                    
            if(levelsFlag==1){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l1status',2);}            
            else if(levelsFlag==2){PR_Obj.setFieldValue('custbody2',2);}            
            else if(levelsFlag==3){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l3status',2);}           
            else if(levelsFlag==4){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_cfostatus',2);}          
            else if(levelsFlag==5){PR_Obj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo',2);}    
            
            var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);            
            //location.reload(true);
            
            var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
            //Get custom email template of rejection    
            var rejectflag=2;           
            var rejectEmailTemplate =getCustomEmailtemplate(rejectflag,PR_Obj);
            var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');   
            //---- Reject Email send to the Administrator and Requesting Employee By Default.   
                var PR_Adminstrator=PR_Obj.getFieldValue('employee');
                var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');
                if(PR_Adminstrator==PR_RequestingEmployee)
                {              
                       var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Email',rejectEmailTemplate); /// Adminstator   
                      // var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                      
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email))
                         nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator   
                }
                else
                {
                        //var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                        //=====Email Requesting Employee =====
                       var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));                
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Requesting Employee Email',rejectEmailTemplate); /// Adminstator
                       
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       if(_LogValidation(to_Email_ReqEmp))
                            nlapiSendEmail('645',to_Email_ReqEmp,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       //=====Email Administartor =====
                       var to_Email_Admin=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));                 
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Administartor Email',rejectEmailTemplate); /// Adminstator 
                      
                      //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email_Admin))
                       nlapiSendEmail('645',to_Email_Admin,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator               
                }       
    
            for(var i=1;i<levelsFlag;i++)
            {
                
                if(i==1)
                {
                    var level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
                    if(_LogValidation(level1))
                    {
                        var to_Email=PR_Obj.getFieldValue('custbody_level1email');
                        rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_level1firstname'));                     
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level1 Email',rejectEmailTemplate);//
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);//RejectedEmailSubject
                        if(_LogValidation(to_Email))
                           nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);//RejectedEmailSubject
                    }
                    
                }//if i =1
                else if(i==2)
                {
                    var level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
                    if(_LogValidation(level2))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level2email');
                        rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_level2firstname'));                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level2 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
                    }
                    
                }//if i =2
                else if(i==3)
                {
                    var level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
                    if(_LogValidation(level3))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level3email');
                        rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_level3firstname'));                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level3 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                            nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
                    }
                    
                }//if i =3
                else if(i==4)
                {
                    var level4=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
                    if(_LogValidation(level4))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_cfoemail');
                        rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_cfofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level4 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
                    }
                    
                }//if i =4
                else if(i==5)
                {
                    var level5=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
                    if(_LogValidation(level5))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofcoo');
                        rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_coofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level5 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                        nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate);
                    }
                    
                }//else if i =5         
                
            }//for
        }//if
*/  
		PR_Obj.setFieldValue('custbody_levelsflag',levelsFlag);
		PR_Obj.setFieldValue('custbody_buttonstatus',2); ///2 means click on Reject button
		var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);		     
    }//if   
    location.reload(true);
}//fun RejectBtnCall

//Updated : 23072013 for this below function used Libray file.
/*
function getCustomEmailtemplate(flag,recordObj)
{
    //------- Load Custom record for Email template
    var emailtemplateObj=nlapiLoadRecord('customrecord_tiffprocurementapprovaem',2);
    var EmailTemplate='';   
    if(flag==3)/// For Approver Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveremailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);      
        EmailTemplate = EmailTemplate.replace('DATEVALUE',recordObj.getFieldValue('trandate'));
        //EmailTemplate = EmailTemplate.replace('PERIODVALUE','');
        EmailTemplate = EmailTemplate.replace('NUMBER',recordObj.getFieldValue('tranid'));
        EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldText('entity'));
        var memo1=recordObj.getFieldValue('memo')
		if(memo1==null){memo1=''}
		EmailTemplate = EmailTemplate.replace('MEMO',memo1);
		//EmailTemplate = EmailTemplate.replace('MEMO',recordObj.getFieldValue('memo'));
        EmailTemplate = EmailTemplate.replace('AMOUNT',recordObj.getFieldValue('custbody_tjinc_tifnfi_prnetnetamount'));//custbody_tjinc_tifnfi_prnetnetamount
        EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
        EmailTemplate = EmailTemplate.replace('RECEIVED','Received');       
        
        var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
        var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
        
        EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
        return EmailTemplate;
        
    }//flag!=3
    
    if(flag==1)/// For Approved Email template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_approveemailtemplate');
        //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','approveEmailTemplate='+EmailTemplate);
    }
    else if(flag==2) /// For Reject template
    {
        EmailTemplate=emailtemplateObj.getFieldValue('custrecord_rejectemailtemplate');
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','rejectEmailTemplate='+EmailTemplate);
        //alert("EmailTemplate="+EmailTemplate);
        var rejReason=recordObj.getFieldValue('custbody_tjinc_tifnfi__rejectionreason');
        if(rejReason==null || rejReason=='' || rejReason==undefined){rejReason='';}
        EmailTemplate = EmailTemplate.replace('REJECTREASON',rejReason);
    }   
    //20062013
	var memo=recordObj.getFieldValue('memo')
	if(memo==null){memo=''}
	EmailTemplate = EmailTemplate.replace('MEMO',memo);	
    EmailTemplate = EmailTemplate.replace('PONUMBER',recordObj.getFieldValue('tranid'));    
    EmailTemplate = EmailTemplate.replace('VENDORNAME',recordObj.getFieldValue('custbody_vendorname'));     
    EmailTemplate = EmailTemplate.replace('STATUS',recordObj.getFieldValue('status'));
    
    var url = 'https://system.netsuite.com/app/accounting/transactions/purchord.nl?id=' + nlapiGetRecordId();
    var LinkValue = '<a href=' + url + ' target=\'_blank\'>View Record</a>';
    
    EmailTemplate = EmailTemplate.replace('View Record',LinkValue); 
    //alert("EmailTemplate="+EmailTemplate);
    return EmailTemplate;
}//function getCustomEmailtemplate
*/

///After Submit function On Purchase Order
function TJINC_NSJ_ProcurementApprovalProcess(type)
{
    nlapiLogExecution('DEBUG','PAP_01 TJINC_NSJ_ProcurementApprovalProcess',' type='+type);
    if(type!='delete')
    { 
	 
	///------------------------------------------------------
	var b_CheckOrderStatus=0;
	var o_OldObj = nlapiGetOldRecord();
	var i_NewRecordId = nlapiGetRecordId();
	nlapiLogExecution('DEBUG', 'AfterSubmit', 'New RecordId= ' + i_NewRecordId);
	if (o_OldObj != null && o_OldObj != '') {
		var s_OldRecordStatus = o_OldObj.getFieldValue('status');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Old Record Status= ' + s_OldRecordStatus);
		var s_OldRecordOrderstatus = o_OldObj.getFieldValue('orderstatus');
		nlapiLogExecution('DEBUG', 'AfterSubmit', 'Old Record s_OldRecordOrderstatus= ' + s_OldRecordOrderstatus);
		if (s_OldRecordStatus == 'Closed') {
			//var i_NewRecordId = nlapiGetRecordId();
			//nlapiLogExecution('DEBUG', 'AfterSubmit', 'New RecordId= ' + i_NewRecordId);
			if (i_NewRecordId != null) {
				var o_NewRecordObj = nlapiLoadRecord('purchaseorder', i_NewRecordId);
				var o_NewRecordStatus = o_NewRecordObj.getFieldValue('status');
				nlapiLogExecution('DEBUG', 'AfterSubmit', 'New Record Status= ' + o_NewRecordStatus);
				if (s_OldRecordStatus != o_NewRecordStatus) {
					o_NewRecordObj.setFieldValue('approvalstatus', 1);
					try {
						nlapiSubmitRecord(o_NewRecordObj, false, true);
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'Status seted as Pending Approval');
						b_CheckOrderStatus=1;
					} 
					catch (ex) {
						nlapiLogExecution('DEBUG', 'AfterSubmit', 'Try Catch Exception= ' + ex);
					}
				}
			}
		}//if
		else if(s_OldRecordOrderstatus=='C')//C means rejected
		{
			var i_flag=0;
			var o_NewRecordObj = nlapiLoadRecord('purchaseorder', i_NewRecordId);
			var o_NewRecordOrderStatus = o_NewRecordObj.getFieldValue('orderstatus');
			if(o_NewRecordOrderStatus=='C'){
				var i_LineCount=o_NewRecordObj.getLineItemCount('item');
				for(var i_i=1;i_LineCount!=null && i_i<=i_LineCount;i_i++){
					var b_IsClosed=nlapiGetLineItemValue('item','isclosed',i_i);
					if(b_IsClosed=='T'){
						i_flag=1;
						break;
					}
				}//for i
				if(i_flag!=1){
					o_NewRecordObj.setFieldValue('approvalstatus', 1);
					nlapiSubmitRecord(o_NewRecordObj, false, true);
					nlapiLogExecution('DEBUG', 'AfterSubmit', 'Status seted as Pending Approval');
					b_CheckOrderStatus=1;
				}
			}//if
			
		}//else if
		
	}
	
nlapiLogExecution('DEBUG', 'AfterSubmit', 'b_CheckOrderStatus= ' + b_CheckOrderStatus);
    //------------------------------------------------------
        var old_SaveAsDraftValue='';
        var new_SaveAsDraftValue='';
        var new_OrderStatus='';
        var old_OrderStatus='';
        
        //var oldPRObj=nlapiGetOldRecord();
        //nlapiLogExecution('DEBUG','PAP_02 OLD Record Obj','oldPRObj='+oldPRObj);
        
        var recordID=nlapiGetRecordId();
        var recordType=nlapiGetRecordType();
        
        var recordObj=nlapiLoadRecord(recordType,recordID); 
        //nlapiLogExecution('DEBUG','orderstatus','$$$$$ orderstatus='+recordObj.getFieldValue('orderstatus'));
        //if(_LogValidation(oldPRObj)){ old_SaveAsDraftValue=oldPRObj.getFieldValue('custbody_tjinc_tfinfi_submitwsaved');}
        if(_LogValidation(recordObj)){ new_SaveAsDraftValue=recordObj.getFieldValue('custbody_tjinc_tfinfi_submitwsaved');}
        if(_LogValidation(recordObj)){ new_OrderStatus=recordObj.getFieldValue('orderstatus');}
        //ERROR if(_LogValidation(oldPRObj)){ old_OrderStatus=recordObj.getFieldValue('orderstatus');}
        //nlapiLogExecution('DEBUG','PAP_03 New Record Obj','new_OrderStatus='+new_OrderStatus);
        //nlapiLogExecution('DEBUG','PAP_04 OLD Record Obj','old_OrderStatus='+old_OrderStatus);
        //----Logic to blank the field if the administarator uncheck the save as draft field and try to save again. 
        ///------ Once PR is rejected again come for the approval then only below logic is going to run. 
        //if((new_SaveAsDraftValue=='F') && (old_OrderStatus=='C' && new_OrderStatus!='C')) /// OrderStatus=C means Rejected
        nlapiLogExecution('DEBUG','PAP_02 OLD Record Obj','new_SaveAsDraftValue='+new_SaveAsDraftValue+' new_OrderStatus: '+new_OrderStatus);
        
        if(((new_SaveAsDraftValue=='F') && (new_OrderStatus=='C')) || b_CheckOrderStatus==1) /// OrderStatus=C means Rejected
        {
            nlapiLogExecution('DEBUG','PAP_2.1 Re-Set Information','new_SaveAsDraftValue='+new_SaveAsDraftValue+' new_OrderStatus: '+new_OrderStatus);
            
            ///-------- Set custom field on NA
            recordObj.setFieldValue('custbody_tjinc_tifnfi_l1status','4');
            recordObj.setFieldValue('custbody2','4');
            recordObj.setFieldValue('custbody_tjinc_tifnfi_l3status','4');
            recordObj.setFieldValue('custbody_tjinc_tifnfi_cfostatus','4');
            recordObj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo','4'); 
            //recordObj.setFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
            recordObj.setFieldValue('custbody_tjinc_tifnfi__rejectionreason','');
            recordObj.setFieldValue('nextapprover',''); //
           /*  comment both below line as requested by carlos dtae 07/May/2013*/
		   // recordObj.setFieldValue('approvalstatus','1'); // 1 means Pending Approval
           // recordObj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');  //
            
            nlapiSubmitRecord(recordObj,false,true);    
        }//if
        var recordObj=nlapiLoadRecord(recordType,recordID);
        if(_LogValidation(recordObj))
        {
            var SaveasDraft=recordObj.getFieldValue('custbody_tjinc_tfinfi_submitwsaved');
            nlapiLogExecution('DEBUG','PAP_05 replaceCustRecordCarLink','SaveasDraft='+SaveasDraft);
            var orderstatus=recordObj.getFieldValue('orderstatus');
            nlapiLogExecution('DEBUG','PAP_06 replaceCustRecordCarLink','orderstatus='+orderstatus);
            //***** HECTOR CHANGES February 6 - the SaveasDraft is not used
            if(SaveasDraft=='F' && orderstatus=='A') //// This is the main check to start Approval Process   ///orderstatus=A means Pending Approval
            {
                var s_l1status = recordObj.getFieldValue('custbody_tjinc_tifnfi_l1status');
                var s_custbody2 = recordObj.getFieldValue('custbody2');
                var s_l3status = recordObj.getFieldValue('custbody_tjinc_tifnfi_l3status');
                var s_cfostatus = recordObj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
                var s_coo = recordObj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
            
                
                recordObj.setFieldValue('custbody_tjinc_tifnfi_l1status', ((s_l1status != '1')?'4':'1'));
                recordObj.setFieldValue('custbody2',((s_custbody2 != '1')?'4':'1'));
                recordObj.setFieldValue('custbody_tjinc_tifnfi_l3status',((s_l3status != '1')?'4':'1'));
                recordObj.setFieldValue('custbody_tjinc_tifnfi_cfostatus',((s_cfostatus != '1')?'4':'1'));
                recordObj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo',((s_coo != '1')?'4':'1'));
            
                //nlapiLogExecution('DEBUG','PAP_07 Sub-Level Status','s_l1status= '+s_l1status+' s_custbody2 : '+s_custbody2+' s_l3status: '+s_l3status+' s_cfostatus: '+s_cfostatus+' s_coo: '+s_coo);
                
                // This object is used to get all the Levels data (i.e. Approver)
                LevelsObj=new Object();
                LevelsObj.Level1=recordObj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
                LevelsObj.Level2=recordObj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
                LevelsObj.Level3=recordObj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
                LevelsObj.CFO=recordObj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
                LevelsObj.COO=recordObj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
                
                // This object will get all the status of the Levels
                Level_Status_Obj=new Object();
                Level_Status_Obj.Level1 = s_l1status;
                Level_Status_Obj.Level2 = s_custbody2;
                Level_Status_Obj.Level3 = s_l3status;
                Level_Status_Obj.CFO    = s_cfostatus;
                Level_Status_Obj.COO    = s_coo;
                
                //function to check is employee is present or not
                var checkLevelsValue=IsLevelHasEmployee(LevelsObj);
                var oldSavedASDraft='';
                //if(_LogValidation(oldPRObj))
                  //oldSavedASDraft=oldPRObj.getFieldValue('custbody_tjinc_tfinfi_submitwsaved');
                //nlapiLogExecution('DEBUG','PAP_07 **************After Submit','oldSavedASDraft='+oldSavedASDraft);    
                //SaveasDraft       
                if(checkLevelsValue==1 && RestrictOnEdit(type,recordObj))
                {
                    nlapiLogExecution('DEBUG','PAP_08 **************After Submit','in if of RestrictOnEdit');
                    ApprovalLevelsLogicFunction(recordObj,LevelsObj,Level_Status_Obj);
                    
                }//if checkLevelsValue
                else if(type=='edit' /*&& (oldSavedASDraft=='T' && SaveasDraft=='F') */)//***** HECTOR CHANGES February 6 this logic is on comment && (oldSavedASDraft=='T' && SaveasDraft=='F')
                {
                    nlapiLogExecution('DEBUG','PAP_09 **************After Submit','in if of oldSavedASDraft==T && SaveasDraft==F');
                    ApprovalLevelsLogicFunction(recordObj,LevelsObj,Level_Status_Obj);
                }   
                nlapiLogExecution('DEBUG','PAP_10 **************After Submit','value RestrictOnEdit(type,recordObj)='+RestrictOnEdit(type,recordObj));              
                
            }//if SaveasDraft=='F'      
        }//if recordObj 
    }//if type!=delete
}//fun ProcurementApprovalProcess

function RestrictOnEdit(type,recordObj)
{
    nlapiLogExecution('DEBUG','**************After Submit','type='+type);
    var Current_User=nlapiGetUser();
    var Current_Role=nlapiGetRole();
    nlapiLogExecution('DEBUG','**************After Submit','Current_User='+Current_User);
    nlapiLogExecution('DEBUG','**************After Submit','Current_Role='+Current_Role);
    if(type=='edit'){       
        var PR_Administrator=recordObj.getFieldValue('employee');
        if(_LogValidation(PR_Administrator)){
            if(Current_User==PR_Administrator){return 0}
            else if(Current_Role=='3'){return 0} /// 3 means Administrator role
        }
    }
    return 1;
    
}//function

//----- Log validation function
function _LogValidation(value)
{
    if(value!=null && value!='' && value!=' ' && value!=undefined)
    {
       return 1;    
    }     
    else
    {
        return 0;   
    }
     
}//fun


function IsLevelHasEmployee(LevelsObj)
{
          var flag=0;
          if(_LogValidation(LevelsObj.Level1)){flag=1;}
          if(_LogValidation(LevelsObj.Level2)){flag=1;}
          if(_LogValidation(LevelsObj.Level3)){flag=1;}
          if(_LogValidation(LevelsObj.CFO)){flag=1;}
          if(_LogValidation(LevelsObj.COO)){flag=1;}
                  
          return flag;        
    
}//fun 


//------Approval Levels Logic
function ApprovalLevelsLogicFunction(recordObj,LevelsObj,Level_Status_Obj)
{

    var approverflag=3;
    var approveEmailTemplate =getCustomEmailtemplate(approverflag,recordObj);
    var ApproverEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_approveremailsubject');
    //Hector Change the If logic
    if(_LogValidation(LevelsObj.Level1) && (Level_Status_Obj.Level1 != '1'))//if(_LogValidation(LevelsObj.Level1) && (Level_Status_Obj.Level1==NA))
    {
        // Sending Notification to the related employee
        //Set value as pending Approval to the level status
        recordObj.setFieldValue('custbody_tjinc_tifnfi_l1status',PendingApproval);
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','LevelsObj.Level1='+LevelsObj.Level1);
        if(_LogValidation(LevelsObj.Level1))
        {
            //var ExtraDepartment_Obj=nlapiLoadRecord('customrecord_tjinc_tifnfi_departments',LevelsObj.Level1);
            //var level1EmpId=ExtraDepartment_Obj.getFieldValue('custrecord_tjinc_tifnfi_employed');
            var level1EmpId=ExtraDepartmentFun(LevelsObj.Level1)
            nlapiLogExecution('DEBUG','replaceCustRecordCarLink','level1 EmpId='+level1EmpId);
            if(_LogValidation(level1EmpId))
            {
                 recordObj.setFieldValue('nextapprover',level1EmpId);
                 var to_email=recordObj.getFieldValue('custbody_level1email');
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver Level1 Email',approveEmailTemplate);
                 
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com',ApproverEmailSubject,approveEmailTemplate);
                 if(_LogValidation(to_email))
                 nlapiSendEmail('645',to_email,ApproverEmailSubject,approveEmailTemplate);
            }//if          
        }//if       
    }
    else if(_LogValidation(LevelsObj.Level2) && (Level_Status_Obj.Level2 != '1'))//else if(_LogValidation(LevelsObj.Level2) && (Level_Status_Obj.Level2==NA))
    {
        // Sending Notification to the related employee
        //Set value as pending Approval to the level status
        recordObj.setFieldValue('custbody2',PendingApproval);
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','LevelsObj.Level2='+LevelsObj.Level2);
        if(_LogValidation(LevelsObj.Level2))
        {
            var ExtraDepartment_Obj=nlapiLoadRecord('customrecord_tjinc_tifnfi_departments',LevelsObj.Level2);
            var level2EmpId=ExtraDepartment_Obj.getFieldValue('custrecord_tjinc_tifnfi_employed');
            nlapiLogExecution('DEBUG','replaceCustRecordCarLink','level2 EmpId='+level2EmpId);
            if(_LogValidation(level2EmpId))
            {
                 recordObj.setFieldValue('nextapprover',level2EmpId);
                 var to_email=recordObj.getFieldValue('custbody_level2email');
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver Level2 Email',approveEmailTemplate);
                 
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com',ApproverEmailSubject,approveEmailTemplate);
                  if(_LogValidation(to_email))
                 nlapiSendEmail('645',to_email,ApproverEmailSubject,approveEmailTemplate);
            }//if          
        }//if       
        //recordObj.setFieldValue('nextapprover',LevelsObj.Level2);
        //var to_email=recordObj.getFieldValue('custbody_level2email');
        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver Email',approveEmailTemplate);
    }
    else if(_LogValidation(LevelsObj.Level3) && (Level_Status_Obj.Level3 != '1'))//else if(_LogValidation(LevelsObj.Level3) && (Level_Status_Obj.Level3==NA))
    {
         // Sending Notification to the related employee
        //Set value as pending Approval to the level status
        recordObj.setFieldValue('custbody_tjinc_tifnfi_l3status',PendingApproval);
        nlapiLogExecution('DEBUG','replaceCustRecordCarLink','LevelsObj.Level3='+LevelsObj.Level3);
        if(_LogValidation(LevelsObj.Level3))
        {
            var ExtraDepartment_Obj=nlapiLoadRecord('customrecord_tjinc_tifnfi_departments',LevelsObj.Level3);
            var level3EmpId=ExtraDepartment_Obj.getFieldValue('custrecord_tjinc_tifnfi_employed');
            nlapiLogExecution('DEBUG','replaceCustRecordCarLink','level3 EmpId='+level3EmpId);
            if(_LogValidation(level3EmpId))
            {
                 recordObj.setFieldValue('nextapprover',level3EmpId);
                 var to_email=recordObj.getFieldValue('custbody_level3email');
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver Level3 Email',approveEmailTemplate);
                 
                 //nlapiSendEmail('645','csanchez@trajectoryinc.com',ApproverEmailSubject,approveEmailTemplate);
                 if(_LogValidation(to_email))
                 nlapiSendEmail('645',to_email,ApproverEmailSubject,approveEmailTemplate);
            }//if          
        }//if
        //recordObj.setFieldValue('nextapprover',LevelsObj.Level3);
        //var to_email=recordObj.getFieldValue('custbody_level3email');
        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver Email',approveEmailTemplate);
    }
    else if(_LogValidation(LevelsObj.CFO) && (Level_Status_Obj.CFO != '1'))//else if(_LogValidation(LevelsObj.CFO) && (Level_Status_Obj.CFO==NA))
    {
         // Sending Notification to the related employee
        //Set value as pending Approval to the level status
        recordObj.setFieldValue('custbody_tjinc_tifnfi_cfostatus',PendingApproval);
        recordObj.setFieldValue('nextapprover',LevelsObj.CFO);
        var to_email=recordObj.getFieldValue('custbody_cfoemail');
        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver CFO Email',approveEmailTemplate);
        
        //nlapiSendEmail('645','csanchez@trajectoryinc.com',ApproverEmailSubject,approveEmailTemplate);
         if(_LogValidation(to_email))
        nlapiSendEmail('645',to_email,ApproverEmailSubject,approveEmailTemplate);
    }
    else if(_LogValidation(LevelsObj.COO) && (Level_Status_Obj.COO != '1'))//else if(_LogValidation(LevelsObj.COO) && (Level_Status_Obj.COO==NA))
    {
         // Sending Notification to the related employee
        //Set value as pending Approval to the level status
        recordObj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo',PendingApproval);
        recordObj.setFieldValue('nextapprover',LevelsObj.COO);
        var to_email=recordObj.getFieldValue('custbody_emailsourceofcoo');
        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approver COO Email',approveEmailTemplate);
        
        //nlapiSendEmail('645','csanchez@trajectoryinc.com',ApproverEmailSubject,approveEmailTemplate);
         if(_LogValidation(to_email))
        nlapiSendEmail('645',to_email,ApproverEmailSubject,approveEmailTemplate);
    }
    
    recordObj.setFieldValue('custbody_tjinc_tifnfi_approvalprocessf','T'); /// Set the flag
    
    var submitPR=nlapiSubmitRecord(recordObj,false,true);
    nlapiLogExecution('DEBUG','TJINC_NSJ_ProcurementApprovalProcess','****submitPR='+submitPR);
    
}//fun ApprovalLevelsLogicFunction

function getEmailIdOfEmployee(empid)
{
    var empObj=nlapiLoadRecord('employee',empid);
    return empObj.getFieldValue('email');
    
}//

function TJINC_NSJ_BeforeSubmitFor(type)
{
    nlapiLogExecution('DEBUG','#### BeforeSubmit','##### type='+type);
    nlapiLogExecution('DEBUG','#### BeforeSubmit','##### Role='+nlapiGetRole());
   if(type!='delete')
   {
   	 nlapiSetFieldValue('custbody_vendorname',nlapiGetFieldText('entity'));
    if(type=='create')
        {
            ///-------- Set custom field on NA
            nlapiSetFieldValue('custbody_tjinc_tifnfi_l1status','4');
            nlapiSetFieldValue('custbody2','4');
            nlapiSetFieldValue('custbody_tjinc_tifnfi_l3status','4');
            nlapiSetFieldValue('custbody_tjinc_tifnfi_cfostatus','4');
            nlapiSetFieldValue('custbodycustbody_tjinc_tifnfi_coo','4'); 
            nlapiSetFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
            nlapiSetFieldValue('custbody_tjinc_tifnfi__rejectionreason','');  
			
			//nlapiSetFieldValue('custbody_levelsflag',0);
			//nlapiSetFieldValue('custbody_buttonstatus',0);       
        }
    
	////-------------------- New logic for script performance update date 30 May 2013
		 /*
         if(type!='create')
		 {
		 	var levelsFlag2=nlapiGetFieldValue('custbody_levelsflag'); /// five levels 1,2,3,4,5
			var ButtonStatus=nlapiGetFieldValue('custbody_buttonstatus'); /// if get 1 means Approved, 2 means reject
		 	var recordId2=nlapiGetRecordId();
			try{
				  if(ButtonStatus==1)// 1 means Click on Approved button
					{
					  ApproveBtnCall_2(recordId2,levelsFlag2);	
					}
					else if(ButtonStatus==2)//2 means  Click on reject button
					{
						RejectBtnCall_2(recordId2,levelsFlag2);
					}				
			}
			finally
			{
				nlapiSetFieldValue('custbody_levelsflag',0);
			    nlapiSetFieldValue('custbody_buttonstatus',0); 	
			}//finally
			
			
		 	
		 }//type ==edit
         */
   	
   }//type!='delete'
   
    
    
}//fun

function getEmailAddress(empid)
{
    var valueArray=new Array();
    var empObj=nlapiLoadRecord('employee',empid);   
    valueArray['email']=empObj.getFieldValue('email');
    valueArray['firstname']=empObj.getFieldValue('firstname');
    return valueArray;
}//fun


function ExtraDepartmentFun(LevelValue)
{
    var levelEmpId='';
    if(_LogValidation(LevelValue))
        {
            var ExtraDepartment_Obj=nlapiLoadRecord('customrecord_tjinc_tifnfi_departments',LevelValue);
            levelEmpId=ExtraDepartment_Obj.getFieldValue('custrecord_tjinc_tifnfi_employed');
            //nlapiLogExecution('DEBUG','replaceCustRecordCarLink','level1 EmpId='+levelEmpId);
                       
        }//if
        return levelEmpId;      
}


///------------------------------------ To validate the Administarator is not equal to Approvers.

function TJINC_TIFF_OnSavePR()
{
    
    var returnValue=1;
    var LevelsObj=new Object();
    LevelsObj.i_Adminstarator=nlapiGetFieldValue('employee');
    LevelsObj.i_Level1=nlapiGetFieldValue('custbody_tjinc_tifnfi_leveloneemp');
    LevelsObj.i_Level2=nlapiGetFieldValue('custbody_tjinc_tifnfi_leveltwoemp');
    LevelsObj.i_Level3=nlapiGetFieldValue('custbody_tjinc_tifnfi_levelthreeemp');
    LevelsObj.i_CFO=nlapiGetFieldValue('custbody_tjinc_tifnfi_exec1approver');
    LevelsObj.i_COO=nlapiGetFieldValue(' custbody_tjinc_tifnfi_exec2approver');
    if(_LogValidation(LevelsObj.i_Adminstarator))
    returnValue=CheckAdminAndApprover(LevelsObj);
    if(returnValue){return true}else{alert("Duplicate administrator and approvers are not allowed.Make the necessary changes."); return false}  
    
}//fun TJINC_TIFF_OnSavePR

function CheckAdminAndApprover(LevelsObj)
{
    if(LevelsObj.i_Adminstarator==LevelsObj.i_Level1){return 0;}
    if(LevelsObj.i_Adminstarator==LevelsObj.i_Level2){return 0;}
    if(LevelsObj.i_Adminstarator==LevelsObj.i_Level3){return 0;}
    if(LevelsObj.i_Adminstarator==LevelsObj.i_CFO){return 0;}
    if(LevelsObj.i_Adminstarator==LevelsObj.i_COO){return 0;}
        
    return 1;
}//fun
var typeOnSave='';
function TJINC_TIFF_OnLineInit(type)
{
    typeOnSave=type;
    
    var ApprovalProcessFlag=nlapiGetFieldValue('custbody_tjinc_tifnfi_approvalprocessf');
    var nextapprover=nlapiGetFieldValue('nextapprover');
    var approvalstatus=nlapiGetFieldValue('approvalstatus');
    var PR_Administrator=nlapiGetFieldValue('employee'); //
    var SaveAsDraft=nlapiGetFieldValue('custbody_tjinc_tfinfi_submitwsaved');
    var Current_User=nlapiGetUser();
    var Current_Role=nlapiGetRole();
    if(_LogValidation(PR_Administrator) && type=='item' && SaveAsDraft=='F' && Current_Role!='3')
    {
        if(ApprovalProcessFlag=='T' && _LogValidation(nextapprover) && PR_Administrator==Current_User && approvalstatus!='3' && CheckApproverStatus())
        {
            nlapiDisableLineItemField('item','item','true');
            nlapiDisableLineItemField('item','description','true');
            nlapiDisableLineItemField('item','amount','true');
            nlapiDisableLineItemField('item','quantity',true);
            nlapiDisableLineItemField('item','rate','true');
            nlapiDisableLineItemField('item','taxcode','true'); 
            nlapiDisableLineItemField('item','custcol_tjinc_tiffnfi_prdescription','true'); 
            nlapiDisableLineItemField('item','isopen','true');
            nlapiDisableLineItemField('item','matchbilltoreceipt','true');
            nlapiDisableLineItemField('item','origrate','true');
            nlapiDisableLineItemField('item','isclosed','true');
            
        }//if
                
    }//if
    
       
    return true;
}
function myLineInit(type)
{
    alert('myLineInit');
    alert('type=' + type);
}

//------------------------- 
function CheckApproverStatus()
{
            var Level_Status_Obj=new Object();
            Level_Status_Obj.Level1=nlapiGetFieldValue('custbody_tjinc_tifnfi_l1status');
            Level_Status_Obj.Level2=nlapiGetFieldValue('custbody2');
            Level_Status_Obj.Level3=nlapiGetFieldValue('custbody_tjinc_tifnfi_l3status');
            Level_Status_Obj.CFO=nlapiGetFieldValue('custbody_tjinc_tifnfi_cfostatus');
            Level_Status_Obj.COO=nlapiGetFieldValue('custbodycustbody_tjinc_tifnfi_coo');
           
           if(Level_Status_Obj.Level1=='3'){return 0}  ////3 is Pending Approval
           else if(Level_Status_Obj.Level1=='1'){return 1}  /// 1 is Approved
           else if(Level_Status_Obj.Level2=='3'){return 0}
           else if(Level_Status_Obj.Level2=='1'){return 1}
           else if(Level_Status_Obj.Level3=='3'){return 0}
           else if(Level_Status_Obj.Level3=='1'){return 1}
           else if(Level_Status_Obj.CFO=='3'){return 0}
           else if(Level_Status_Obj.CFO=='1'){return 1}
           else if(Level_Status_Obj.COO=='3'){return 0}
           else if(Level_Status_Obj.COO=='1'){return 1}
           
           return 1;
    
}



///---------------------------

function myValidateLine(type)
{
    //alert('myValidateLine');
    //alert('type=' + type);
    var ApprovalProcessFlag=nlapiGetFieldValue('custbody_tjinc_tifnfi_approvalprocessf');
    var nextapprover=nlapiGetFieldValue('nextapprover');
    var approvalstatus=nlapiGetFieldValue('approvalstatus');
    var PR_Administrator=nlapiGetFieldValue('employee');
    var Current_User=nlapiGetUser();
    var Current_Role=nlapiGetRole();
    var SaveAsDraft=nlapiGetFieldValue('custbody_tjinc_tfinfi_submitwsaved');
    if(_LogValidation(PR_Administrator) && type=='item' && SaveAsDraft=='F' && Current_Role!='3')
    {
        if(ApprovalProcessFlag=='T' && _LogValidation(nextapprover) && PR_Administrator==Current_User && approvalstatus!='3' && CheckApproverStatus())
        {
            return false;
        }//if       
    }//if
    return true;
}



///------------------------------
function myValidateInsert(type)
{
    //alert('myValidateInsert');
    //alert('type=' + type);
}
function myValidateDelete(type)
{
    //alert('myValidateDelete');
    //alert('type=' + type);
    var ApprovalProcessFlag=nlapiGetFieldValue('custbody_tjinc_tifnfi_approvalprocessf');
    var nextapprover=nlapiGetFieldValue('nextapprover');
    var approvalstatus=nlapiGetFieldValue('approvalstatus');
    var PR_Administrator=nlapiGetFieldValue('employee');
    var Current_User=nlapiGetUser();
    var Current_Role=nlapiGetRole();
    var SaveAsDraft=nlapiGetFieldValue('custbody_tjinc_tfinfi_submitwsaved');
    if(_LogValidation(PR_Administrator) && type=='item' && SaveAsDraft=='F' && Current_Role!='3')
    {
        if(ApprovalProcessFlag=='T' && _LogValidation(nextapprover) && PR_Administrator==Current_User && approvalstatus!='3' && CheckApproverStatus())
        {
            return false;
        }//if       
    }//if   
    return true;
}



//////-----------------------*********** New Logic for script performanance *********************--------------------------

///--------- This is the Client Script function without deployment
function ApproveBtnCall_2(recordId,levelsFlag)
{
    ////alert("recordId="+recordId);
    ////alert("levelsFlag="+levelsFlag);
    var checkSupervisorApprovalFlag=0;
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
        
        if(_LogValidation(PR_Obj))
        {
             
            // This object will get all the status of the Levels
            Level_Status_Obj=new Object();
            Level_Status_Obj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l1status');
            Level_Status_Obj.Level2=PR_Obj.getFieldValue('custbody2');
            Level_Status_Obj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_l3status');
            Level_Status_Obj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_cfostatus');
            Level_Status_Obj.COO=PR_Obj.getFieldValue('custbodycustbody_tjinc_tifnfi_coo');
            var level_status_id='';
            if(levelsFlag==1)
            {
                level_status_id='custbody_tjinc_tifnfi_l1status'
            }
            else if(levelsFlag==2)
            {
                level_status_id='custbody2'
            }
             else if(levelsFlag==3)
            {
                level_status_id='custbody_tjinc_tifnfi_l3status'
            }
            else if(levelsFlag==4)
            {
                level_status_id='custbody_tjinc_tifnfi_cfostatus'
            }
            else if(levelsFlag==5)
            {
                level_status_id='custbodycustbody_tjinc_tifnfi_coo'
            }
            if(level_status_id!='')
               PR_Obj.setFieldValue(level_status_id,Approved);      
                    
             
             ///------------- This logic is used when we have to set standard 
            LevelsObj=new Object();
            LevelsObj.Level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
            LevelsObj.Level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
            LevelsObj.Level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
            LevelsObj.CFO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
            LevelsObj.COO=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
            
            if(_LogValidation(LevelsObj.Level1))
            {
                checkSupervisorApprovalFlag=1;
            }
            if(_LogValidation(LevelsObj.Level2))
            {
                checkSupervisorApprovalFlag=2;
            }
            if(_LogValidation(LevelsObj.Level3))
            {
                checkSupervisorApprovalFlag=3;
            }
            if(_LogValidation(LevelsObj.CFO))
            {
                checkSupervisorApprovalFlag=4;
            }
            if(_LogValidation(LevelsObj.COO))
            {
                checkSupervisorApprovalFlag=5;
            }
            
             if(levelsFlag==checkSupervisorApprovalFlag)
             {          
                PR_Obj.setFieldValue('approvalstatus','2'); /// 2 means Approved
                //PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);                
                   if(submitPR_ID!=null)
                   { 
                      var approvedflag=1;
                      var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
                      var PR_Adminstrator=PR_Obj.getFieldValue('employee'); 
                      var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');                             
                      var approvedEmailTemplate=getCustomEmailtemplate(approvedflag,PR_Obj); 
					  var approvedEmailTemplate_Requester=approvedEmailTemplate;                    
                      var firstName=PR_Obj.getFieldValue('custbody_administratorfirstname');                      
                      var to_email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');                 
                      approvedEmailTemplate = approvedEmailTemplate.replace('USER',firstName);
                      var ApprovedSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject');
                      if (PR_Adminstrator == PR_RequestingEmployee) 
                      {                   
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Approved Email',approvedEmailTemplate); ///Approved Email
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com', nlapiLookupField('customrecord_tiffprocurementapprovaem', '2', 'custrecord_approvedemailsubject'), approvedEmailTemplate);
                        //---------uPDATE 21012013
                        //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                        if(_LogValidation(to_email))
                            nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                       }//if
                       else
                       {
                         //----------- Send Approved Email to PR- Administrator
                         //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                         if(_LogValidation(to_email))
                           nlapiSendEmail('645',to_email,ApprovedSubject, approvedEmailTemplate);
                         //=====Email Requesting Employee =====
                         var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                         //----------- Send Approved Email to Requesting Employee
                         approvedEmailTemplate_Requester = approvedEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));
                         //nlapiSendEmail('645', 'csanchez@trajectoryinc.com',ApprovedSubject, approvedEmailTemplate);
                         if(_LogValidation(to_Email_ReqEmp))                         
                             nlapiSendEmail('645',to_Email_ReqEmp,ApprovedSubject, approvedEmailTemplate_Requester);
                       }
                   }//if
                    //location.reload(true);
                
             }//if  
             else
             {
                var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);
                //location.reload(true);
             }  
        }//if
        
    }//if recordId
    
}//fun ApproveBtnCall

///=================================== Reject Btn Function ==============================
function RejectBtnCall_2(recordId,levelsFlag)
{   
    if(_LogValidation(recordId))
    {
        var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);       
        if(_LogValidation(PR_Obj))
        {
            var reasonForRejection= PR_Obj.getFieldValue('custbody_tjinc_tifnfi__rejectionreason');           
            //PR_Obj.setFieldValue('custbody_tjinc_tifnfi__rejectionreason',reasonForRejection);
            PR_Obj.setFieldValue('custbody_tjinc_tfinfi_submitwsaved','T');
            PR_Obj.setFieldValue('custbody_tjinc_tifnfi_approvalprocessf','F');
            PR_Obj.setFieldValue('approvalstatus','3'); //approvalstatus is the Approval Status and 3 is for Rejected
                                    
            if(levelsFlag==1){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l1status',2);}            
            else if(levelsFlag==2){PR_Obj.setFieldValue('custbody2',2);}            
            else if(levelsFlag==3){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_l3status',2);}           
            else if(levelsFlag==4){PR_Obj.setFieldValue('custbody_tjinc_tifnfi_cfostatus',2);}          
            else if(levelsFlag==5){PR_Obj.setFieldValue('custbodycustbody_tjinc_tifnfi_coo',2);}    
            
            var submitPR_ID=nlapiSubmitRecord(PR_Obj,true,true);            
            //location.reload(true);
            
            var PR_Obj=nlapiLoadRecord('purchaseorder',recordId);
            //Get custom email template of rejection    
            var rejectflag=2;           
            var rejectEmailTemplate =getCustomEmailtemplate(rejectflag,PR_Obj);
			var rejectEmailTemplate_Bckp=rejectEmailTemplate;
			var rejectEmailTemplate_Requester=rejectEmailTemplate;
            var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');   
            //---- Reject Email send to the Administrator and Requesting Employee By Default.   
                var PR_Adminstrator=PR_Obj.getFieldValue('employee');
                var PR_RequestingEmployee=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_employee');
                if(PR_Adminstrator==PR_RequestingEmployee)
                {              
                       var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Email',rejectEmailTemplate); /// Adminstator   
                      // var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                      
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email))
                         nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator   
                }
                else
                {
                        //var RejectedEmailSubject=nlapiLookupField('customrecord_tiffprocurementapprovaem','2','custrecord_rejectedemailsubject');
                        //=====Email Requesting Employee =====
                       var to_Email_ReqEmp=PR_Obj.getFieldValue('custbody_requestingemployeeemail');
                       rejectEmailTemplate = rejectEmailTemplate.replace('USER',PR_Obj.getFieldValue('custbody_requestingemployeefirstname'));                
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Requesting Employee Email',rejectEmailTemplate); /// Adminstator
                       
                       //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       if(_LogValidation(to_Email_ReqEmp))
                            nlapiSendEmail('645',to_Email_ReqEmp,RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                       //=====Email Administartor =====
                       var to_Email_Admin=PR_Obj.getFieldValue('custbody_emailsourceofadministrator');
                       rejectEmailTemplate_Requester = rejectEmailTemplate_Requester.replace('USER',PR_Obj.getFieldValue('custbody_administratorfirstname'));                 
                      // nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected Administartor Email',rejectEmailTemplate); /// Adminstator 
                      
                      //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate); /// Adminstator
                      if(_LogValidation(to_Email_Admin))
                       nlapiSendEmail('645',to_Email_Admin,RejectedEmailSubject,rejectEmailTemplate_Requester); /// Adminstator               
                }       
    
            for(var i=1;i<levelsFlag;i++)
            {
                
                if(i==1)
                {
                    var level1=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_srmanagerapprove');
                    if(_LogValidation(level1))
                    {
                        var to_Email=PR_Obj.getFieldValue('custbody_level1email');
						var rejectEmailTemplate_level1=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level1 = rejectEmailTemplate_level1.replace('USER',PR_Obj.getFieldValue('custbody_level1firstname'));                     
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level1 Email',rejectEmailTemplate);//
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);//RejectedEmailSubject
                        if(_LogValidation(to_Email))
                           nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level1);//RejectedEmailSubject
                    }
                    
                }//if i =1
                else if(i==2)
                {
                    var level2=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_directorapprover');
                    if(_LogValidation(level2))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level2email');
						var rejectEmailTemplate_level2=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level2 = rejectEmailTemplate_level2.replace('USER',PR_Obj.getFieldValue('custbody_level2firstname'));                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level2 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level2);
                    }
                    
                }//if i =2
                else if(i==3)
                {
                    var level3=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_vicepresidentapp');
                    if(_LogValidation(level3))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_level3email');
						var rejectEmailTemplate_level3=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level3 = rejectEmailTemplate_level3.replace('USER',PR_Obj.getFieldValue('custbody_level3firstname'));                 
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level3 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                            nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level3);
                    }
                    
                }//if i =3
                else if(i==4)
                {
                    var level4=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec1approver');
                    if(_LogValidation(level4))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_cfoemail');
						var rejectEmailTemplate_level4=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level4 = rejectEmailTemplate_level4.replace('USER',PR_Obj.getFieldValue('custbody_cfofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level4 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                          nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level4);
                    }
                    
                }//if i =4
                else if(i==5)
                {
                    var level5=PR_Obj.getFieldValue('custbody_tjinc_tifnfi_exec2approver');
                    if(_LogValidation(level5))
                    {   
                        var to_Email=PR_Obj.getFieldValue('custbody_emailsourceofcoo');
						var rejectEmailTemplate_level5=rejectEmailTemplate_Bckp;
                        rejectEmailTemplate_level5 = rejectEmailTemplate_level5.replace('USER',PR_Obj.getFieldValue('custbody_coofirstname'));                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com','Rejected level5 Email',rejectEmailTemplate);
                        
                        //nlapiSendEmail('645','csanchez@trajectoryinc.com',RejectedEmailSubject,rejectEmailTemplate);
                        if(_LogValidation(to_Email))
                        nlapiSendEmail('645',to_Email,RejectedEmailSubject,rejectEmailTemplate_level5);
                    }
                    
                }//else if i =5         
                
            }//for
        }//if       
    }//if   
    //location.reload(true);
}//fun RejectBtnCall