function descriptionMandatory (type){
	try{
		if(type=='item'){	
			if(isNull(nlapiGetCurrentLineItemValue('item','description'))){
				alert('Please populate the Description on the line');
				return false;
			}
			else return true;
		}
		else if(type=='expense'){
			if(isNull(nlapiGetCurrentLineItemValue('expense','memo'))){
				alert('Please populate the Description on the line');
				return false;
			}
			else return true;
		}
	}
	catch(e){
		alert(e);
		nlapiLogExecution('ERROR', 'Client Script descriptionMandatory',"User:"+nlapiGetUser()+"; "+e)
	}
}

function isNotNull(checkThis) {
	if (checkThis == null) return false;
	else return (trim(checkThis != null) && (trim(checkThis) != ''));
}
function isNull(checkThis) {
	return !(isNotNull(checkThis));
}
function isNotJsonNull(checkThis) {
	var isN = true;
	try { for (i in checkThis) {
			isN = false;
			break;}} 
	catch (e) {}
	return !isN;
}
function trim(stringToTrim) {
	if(stringToTrim == null) return null;
	return stringToTrim.toString().replace(new RegExp("/^\s+|\s+$/g"),"");
}
function ltrim(stringToTrim) {
	if(stringToTrim == null) return null;
	return stringToTrim.toString().replace(new RegExp("/^\s+/"),"");
}
function rtrim(stringToTrim) {
	if(stringToTrim == null) return null;
	return stringToTrim.toString().replace(new RegExp("/\s+$/"),"");
}