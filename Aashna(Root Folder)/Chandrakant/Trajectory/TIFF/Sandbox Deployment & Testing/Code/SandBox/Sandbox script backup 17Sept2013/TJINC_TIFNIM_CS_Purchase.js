/**
* Copyright (c) 2012 Trajectory Group Inc. / Kuspide Canada Inc.
* 76 Richmond St. East, Suite 400, Toronto, ON, Canada, M5C 1P1 
* www.trajectoryinc.com 
* All Rights Reserved. 
*/

/** 
* @System: TIFF
* @Company: Trajectory Inc.  
* @CreationDate: 20121211
* @DocumentationUrl:  https://sites.google.com/a/trajectoryinc.com/tif/classes/tjinc_tifnim_ue_invreqts-js
* @NamingStandard: TJINC_NSJ-1-2
*/

var O_TJINC_ITEM_INFO = {};

function TJINC_TIFNIM_OnFieldChangePOFunction(type, name) {
	
	try {
	    console.info('type : '+type+' name: '+name);
		
		if(name == 'custbody_tjinc_tifnim_vendorpayee'){
			TJINC_TIFNIM_GetItemInfoOPfun();
			var s_entity  = nlapiGetFieldValue('entity');
			var s_temEntity  = nlapiGetFieldValue('custbody_tjinc_tifnim_vendorpayee');
			if(isNotNull(s_temEntity) && isNull(s_entity) && (nlapiGetLineItemCount('item') == 0)){
				nlapiSetFieldValue('entity', s_temEntity, true, true);
			}
			/*
			TJINC_TIFNIM_GetItemInfoOPfun();
			
			nlapiSetFieldValue('custbody_tjinc_tifnim_isanewvendor', 'F', false, false);
			nlapiDisableField('custbody_tjinc_tifnim_isanewvendor', false);
			//var s_entity = nlapiGetFieldValue('entity');
			//nlapiSetFieldValue('custbody_tjinc_tifnim_isanewvendor', 'T', false, false);

			//TJINC_TIFNIM_SetItemInfoOP();*/
		}else if(name == 'department'){
			
			nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', '');
			nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', '');
			nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp', '');
			nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '');
			nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '');
			
		}else if(name == 'memo'){
			if(nlapiGetFieldValue('memo') == 'remove'){
			nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', '');
			nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', '');
			nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp', '');
			nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '');
			nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '');
			nlapiSelectLineItem('item',nlapiGetCurrentLineItemIndex('item'));
			nlapiRemoveLineItem('item');
			nlapiSetFieldValue('memo', 'done');
			}
		}
		
		else if(name=='custbody_tjinc_tifnfi_currencyoverride'){
			if(nlapiGetRole()==1005||nlapiGetRole()==3){	
				var i_currencyCustom = nlapiGetFieldValue('custbody_tjinc_tifnfi_currencyoverride');
				var b_confirmCurrency= confirm('Note you are changing the currency of this purchase order. \n\nProceed? Please validate currency and exchange rate upon completion.')
				if(b_confirmCurrency){
					nlapiSetFieldValue('currency',i_currencyCustom,false);		
					nlapiSetFieldValue('custbody_tjinc_tifnfi_currencyoverride','',false);
				}
				else nlapiSetFieldValue('custbody_tjinc_tifnfi_currencyoverride','',false);	
			}	
			else nlapiSetFieldValue('custbody_tjinc_tifnfi_currencyoverride','',false);	
		}
		
		
		if (type == 'item') {
			var i_item = nlapiGetCurrentLineItemValue('item', 'item')
			if(i_item != null && i_item != '')
			{				
				switch (name) {
				case 'quantity':
				case 'rate':
				case 'amount':
					
					var i_amount = 0;
					var i_rate = 0;
					var i_quantity = 0;
					var i_amountLine = 0;
					var i_line = nlapiGetCurrentLineItemIndex('item');

					//alert('Field ID : '+name+' Line Index : '+nlapiGetCurrentLineItemIndex('item'));
					i_quantity = (isNotNull(nlapiGetCurrentLineItemValue('item', 'quantity')))?parseFloat(nlapiGetCurrentLineItemValue('item', 'quantity')):(0);
					
					i_rate = (isNotNull(nlapiGetCurrentLineItemValue('item', 'rate')))?parseFloat(nlapiGetCurrentLineItemValue('item', 'rate')):(0);
					 
					var i_oldAmount = nlapiGetCurrentLineItemValue('item', 'custcol_tjinc_tifnim_amounthistory');
					i_amount = i_quantity * i_rate;
					
					if(i_oldAmount != i_amount){
						nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', '', false);
						nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', '', false);
						nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp', '', false);
						nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '', false);
						nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '', false);
					}
					
					nlapiSetCurrentLineItemValue('item', 'custcol_tjinc_tifnim_amounthistory',i_amount, false);
					nlapiSetCurrentLineItemValue('item', 'amount', i_amount,false);
					
					break;
				
				}
			}
				
		}
		
	}
	catch (err) {
		alert(err);
	}
}


function TJINC_TIFNIM_SetItemInfoOPByLine(i_line){
    
   for (var i_index in O_TJINC_ITEM_INFO) {
        
        if(i_index == i_line){
            var i_qty = O_TJINC_ITEM_INFO[i_index].qty;
            var i_rate = O_TJINC_ITEM_INFO[i_index].rate;
            nlapiSetCurrentLineItemValue('item', 'quantity', i_qty, false,false);
            nlapiSetCurrentLineItemValue('item', 'rate', i_rate, false,false);
            nlapiSetCurrentLineItemValue('item', 'amount', parseFloat(i_qty)*parseFloat(i_rate), false,false);
        }
    }
}

function TJINC_TIFNIM_pageinit(type)
{
	if(type != 'create' && type != 'delete')
	{
		TJINC_TIFNIM_GetItemInfoOPfun();		
		/*
		var i_lineCount = nlapiGetLineItemCount('item');
		alert('lineCount= '+i_lineCount);
		for(var i=1;i<=i_lineCount;i++)
		{
			var i_item = nlapiGetLineItemValue('item', 'item', i);
			
		}
		*/
	}
}

function TJINC_TIFNIM_OnSaveOP(){
	var s_entity  = nlapiGetFieldValue('entity');
	var s_temEntity  = nlapiGetFieldValue('custbody_tjinc_tifnim_vendorpayee');
	TJINC_TIFNIM_SetItemInfoOP();
	if(s_entity != s_temEntity)
	{
		//TJINC_TIFNIM_GetItemInfoOPfun();
		nlapiSetFieldValue('entity', s_temEntity, true, true);
		
	}
	
	return true;
}

function TJINC_TIFNIM_GetItemInfoOPfun(){
	
	O_TJINC_ITEM_INFO = {};
	//taxrate1,taxrate2
	if(nlapiGetLineItemCount('item') > 0){
		
		for(var i = 1; i <= nlapiGetLineItemCount('item'); i++){
			O_TJINC_ITEM_INFO[i] = {'item':nlapiGetLineItemValue('item', 'item', i), 'qty':nlapiGetLineItemValue('item', 'quantity', i), 'rate':nlapiGetLineItemValue('item', 'rate', i)};
		}
	}
}

function TJINC_TIFNIM_SetItemInfoOP(){
	//alert('Step 1 TJINC_TIFNIM_SetItemInfoOP');
	for (var i_index in O_TJINC_ITEM_INFO) {
		
		var i_qty = O_TJINC_ITEM_INFO[i_index].qty;
		var i_rate = O_TJINC_ITEM_INFO[i_index].rate;
		console.info('Qty : '+i_qty);
		console.info('Rte : '+i_rate);
		console.info('Indx : '+i_index);
		
		nlapiSelectLineItem('item', parseInt(i_index));
		if ((nlapiGetCurrentLineItemValue('item', 'rate')) == '0.00') {
			nlapiSetCurrentLineItemValue('item', 'quantity', i_qty, false, false);
			nlapiSetCurrentLineItemValue('item', 'rate', i_rate, false, false);
			nlapiSetCurrentLineItemValue('item', 'amount', parseFloat(i_qty) * parseFloat(i_rate), false, false);
		}
		nlapiSetFieldValue('custbody_tjinc_tiff_commitline', 'T');
		nlapiCommitLineItem('item');
		console.info('After commit : '+i_index);
	}
	//alert('Step 2 TJINC_TIFNIM_SetItemInfoOP');
}

function TJINC_TIFNIM_DeleteLineOP(type){
	
	if(type == 'item'){
		
		nlapiSetFieldValue('custbody_tjinc_tifnfi_srmanagerapprove', '',false,false);
		nlapiSetFieldValue('custbody_tjinc_tifnfi_directorapprover', '',false,false);
		nlapiSetFieldValue( 'custbody_tjinc_tifnfi_vicepresidentapp', '',false,false);
		nlapiSetFieldValue( 'custbody_tjinc_tifnfi_exec1approver', '',false,false);
		nlapiSetFieldValue('custbody_tjinc_tifnfi_exec2approver', '',false,false);
		nlapiSetCurrentLineItemValue('item', 'item', '',false);
		nlapiSetCurrentLineItemValue('item', 'custcol_tjinc_tiffnfi_prdescription', '',false);
		nlapiSetCurrentLineItemValue('item', 'quantity', '',false);
		nlapiSetCurrentLineItemValue('item', 'rate', '0',false);
		nlapiSetCurrentLineItemValue('item', 'amount', '0',false);
		nlapiSetCurrentLineItemValue('item', 'project', '',false);
	}
	return true;
}