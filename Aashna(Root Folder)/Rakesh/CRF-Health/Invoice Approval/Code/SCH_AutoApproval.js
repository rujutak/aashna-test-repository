// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_AutoApproval.js
     Author: Rakesh malvade
     Company: Aashna Cloud tech
     Date: 23-may-2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - CreateWeeklyStockReport(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function InvoiceAutoApproval(type){

    var fileId_Param = nlapiGetContext().getSetting('SCRIPT', 'custscript_ndaysapproval');
    nlapiLogExecution('DEBUG', 'Parameter value', 'fileId_Param :' + fileId_Param);
    //var daysvalue = 'daysAgo' + fileId_Param;
   var daysvalue = 'daysago' + fileId_Param;
    nlapiLogExecution('DEBUG', 'Parameter value', 'daysvalue :' + daysvalue);
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custbody_approval_status', null, 'is', 3);
    filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
    //filters[2] = new nlobjSearchFilter('trandate', null, 'onOrAfter', daysvalue);
   filters[2] = new nlobjSearchFilter('trandate', null, 'onorbefore', daysvalue);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    var results = nlapiSearchRecord('invoice', null, filters, columns);
    
    if (results != null) {
        nlapiLogExecution('DEBUG', 'invoice', 'Results Length :' + results.length);
        for (var i = 0; i < results.length; i++) {
            var Internalid = results[i].getValue('internalid');
            nlapiLogExecution('DEBUG', 'invoice', 'Internalid :' + Internalid);
            var status=updateinvoice(Internalid);
            Internalid='';
        }
        
        
    }
}


function updateinvoice(invoiceid)
{
	 if (invoiceid != null && invoiceid != 'undefined' && invoiceid != '') {
                var ObjInvoice = nlapiLoadRecord('invoice', invoiceid);
                ObjInvoice.setFieldValue('custbody_approval_status',1);
                ObjInvoice.setFieldValue('custbody_autoapprove', 'T');
				ObjInvoice.setFieldValue('custbody_approved_by', '');
                nlapiSubmitRecord(ObjInvoice);
            }
}