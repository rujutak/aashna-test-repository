// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_PaymentCheck.js
     Author:Rakesh Malvade
     Company:Aashna Cloud Tech
     Date:27-jun-2013
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     22-06-2013				Rakesh.m						Mangesh						check invoice is approved or not
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================


function onsave_payment(){
    var returnflag = true;
    
    var lineitemcount = nlapiGetLineItemCount('apply');
    //alert('line item count=' + lineitemcount);
    var count = 0;
    for (var i =1 ; i <= lineitemcount; i++) {
        var id = nlapiGetLineItemValue('apply', 'internalid', i);
        //alert('line item invoice id=' + id);
        var Applyvalue = nlapiGetLineItemValue('apply', 'apply', i);
        
        if (Applyvalue == 'T') {
            var ObjInvoice = nlapiLoadRecord('invoice', id);
            var Inovicestatus = ObjInvoice.getFieldValue('custbody_approval_status');
           // alert('Inovicestatus==' + Inovicestatus+'=i='+i);
            if (Inovicestatus != 1 || Inovicestatus == null || Inovicestatus == '' || Inovicestatus == 'undefined') {
                count++;
                //alert('count=='+count);
                nlapiSetLineItemValue('apply', 'apply', i, 'F');
				nlapiSetLineItemValue('apply', 'amount', i, '');
				nlapiSetCurrentLineItemValue('apply','apply','F',true,false);
            }
        }
    }
    //alert('count2222==' + count);
    
    if (count == lineitemcount) {
        alert('The payment you are created for invoice is not approved yet please approved the invoice first and then get create the payment for it');
        returnflag = false;
        
    }
    else 
        if (count != lineitemcount) {
            //alert('in not equal')
            returnflag = true;
        }
    //
    
    /*var record = nlapiLoadRecord('inventoryitem', id);
     record.setFieldValue('isinactive', 'T');
     nlapiSubmitRecord(record);
     return true;*/
	//alert('returnflag==='+returnflag)
    return true
    
}


