// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_EditField.js
     Author:Rakesh Malvade
     Company:Aashna cloud tech
     Date:10-jun-2013
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     26-06-2013          Rakesh .m							Mangesh							Disable the All the custom field 
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================
function PageInit_invoice(type){
    //alert('i am in clinet script');
	 var userid = nlapiGetRole();
    if (type == 'view' || type == 'edit') {
       
        //alert(userid);
        var InvoiceID = nlapiGetRecordId();
        //alert('Invoice ID-->' + InvoiceID);
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        var SubmitApproval = ObjInvoice.getFieldValue('custbody_submitforapproval');
        var status = ObjInvoice.getFieldValue('custbody_approval_status');
        if (SubmitApproval == 'F' && userid != 3 && (status == 1 || status == 3 || status == '' || status == null))//&& status!=2
		{
		
			var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
			var approvedby = ObjInvoice.getFieldValue('custbody_approved_by');
			var rejectionnote = ObjInvoice.getFieldValue('custbody_approvalrejectionnote');
			
			if (approvalstatus == null) {
				nlapiDisableField('custbody_approval_status', true);
			}
			if (approvedby == null) {
				nlapiDisableField('custbody_approved_by', true);
			}
			if (rejectionnote == null) {
				nlapiDisableField('custbody_approvalrejectionnote', true);
			}
			nlapiDisableField('custbody_submitforapproval', true);
		}
		else 
			if (SubmitApproval == 'F' && userid != 3 && status == 2) {
			
				var approvedby = ObjInvoice.getFieldValue('custbody_approved_by');
				var rejectionnote = ObjInvoice.getFieldValue('custbody_approvalrejectionnote');
				var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
				if (approvedby != null && approvedby != 'undefined' && approvedby != '') {
					nlapiDisableField('custbody_approved_by', true);
				}
				if (rejectionnote != null && rejectionnote != 'undefined' && rejectionnote != '') {
					nlapiDisableField('custbody_approvalrejectionnote', false);
				}
				if (approvalstatus != null && approvalstatus != 'undefined' && approvalstatus != '') {
					nlapiDisableField('custbody_approval_status', false);
				}
				nlapiDisableField('custbody_submitforapproval', true);
				
			}
			else 
				if (SubmitApproval == 'T' && userid != 3) {
				
					var approvedby = ObjInvoice.getFieldValue('custbody_approved_by');
					var rejectionnote = ObjInvoice.getFieldValue('custbody_approvalrejectionnote');
					var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
					if (approvedby != null && approvedby != 'undefined' && approvedby != '') {
						nlapiDisableField('custbody_approved_by', true);
					}
					if (rejectionnote != null && rejectionnote != 'undefined' && rejectionnote != '') {
						nlapiDisableField('custbody_approvalrejectionnote', true);
					}
					if (approvalstatus != null && approvalstatus != 'undefined' && approvalstatus != '') {
						nlapiDisableField('custbody_approval_status', true);
					}
					nlapiDisableField('custbody_submitforapproval', true);
				}
    }
    else 
        if (type == 'create' && userid != 3) {
            nlapiDisableField('custbody_approval_status', true);
            nlapiDisableField('custbody_approved_by', true);
            nlapiDisableField('custbody_approvalrejectionnote', true);
            nlapiDisableField('custbody_submitforapproval', true);
        }
		 else 
        if (type == 'copy' && userid != 3) {
            nlapiDisableField('custbody_approval_status', true);
            nlapiDisableField('custbody_approved_by', true);
            nlapiDisableField('custbody_approvalrejectionnote', true);
            nlapiDisableField('custbody_submitforapproval', true);
        }
}

