// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_Sales_Order_Item_validation.js
     Author:Rakesh Malvade
     Company:Proquest solutions
     Date:05-march-2013
     Description:On Item Coloumn field changePick the program Item Type Field Value If it is Of type TV Allow to set the item
     else  Show one alert message �Please Enter TV Program Items � & clear the line.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     05-march-2013				Rakesh.M						Supriya						On Item Coloumn field changePick the program Item Type Field Value If it is Of type TV Allow to set the item
     else  Show one alert message �Please Enter TV Program Items � & clear the line.
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================



// ============================ Disaply Submit for approval button for accounant user  =======================================

function forappproval(InvoiceID){
    try {
        //alert('i am in client script' + InvoiceID);
        var userid = nlapiGetUser()
        //alert('userid=' + userid);
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        var jobid = ObjInvoice.getFieldValue('job');
        var tranid = ObjInvoice.getFieldValue('tranid');
        //alert('jobid' + jobid);
        var ObjProject = nlapiLoadRecord('job', jobid);
        var ProjectManagerid = ObjProject.getFieldValue('custentity_project_manager');
        //alert('ProjectManager=' + ProjectManagerid);
        if (ProjectManagerid != null && ProjectManagerid != 'undefined' && ProjectManagerid != '') {
            var superwiserid = SearchsuperwiserId(ProjectManagerid);
            //var superwiserid=ObjEmployee.getFieldValue('supervisor');
            //alert('superwiserid=' + superwiserid);
            if (superwiserid != null && superwiserid != 'undefined' && superwiserid != '') {
                var emailid = SearchEmailId(superwiserid);
                //alert('emailid=' + emailid);
                if (emailid != null && emailid != 'undefined' && emailid != '') {
                    var click_here = 'Please Click Here.';
                    var url = 'https://system.sandbox.netsuite.com/app/accounting/transactions/custinvc.nl?id=' + InvoiceID;
                    var LinkValue = '<a href=' + url + ' target=\'_blank\'>' + click_here + '</a>';
                    //alert("LinkValue"+LinkValue);
                    nlapiSendEmail(userid, emailid, 'Invoice for your approval invoice# ' + tranid, '<br><br> The invoice number ' + '  ' + tranid + ' ' + 'is pending for your approval, ' + ' ' + LinkValue + ' ' + '<br><br><br>' + 'Thank you', null, null, null)
                    alert("The email has been send to project mangaer sucessfully.");
                    ObjInvoice.setFieldValue('custbody_submitforapproval', 'T');
                    //alert('custbody_submitforapproval');
                    ObjInvoice.setFieldValue('custbody_pm', ProjectManagerid);
                    //alert('custbody_pm=');
                    ObjInvoice.setFieldValue('custbody_approval_status', 3);
                    //alert('custbody_approval_status');
                    nlapiSubmitRecord(ObjInvoice, false, false);
                    //alert('submit record=');
                    location.reload();
                }
                else {
                    alert("superwiser email id is not present or Superwiser subsidiary is different. ");
                }
            }
            else {
                alert("superwiser field is blank on project manager record . ");
            }
        }
        else {
            alert("project manager field is blank on project record. ");
        }
        
        
        
        
    } 
    catch (e) {
        alert('error====' + e.getDetails())
    }
    
    //window.open("https://system.sandbox.netsuite.com/app/common/custom/custrecordentry.nl?rectype=82&custrecord_mp1_soref="+SOId)

}

// ============================ End Submit for approval button for accounant user  =======================================


// ============================ Maneger Click on Approve buuton  =======================================
function appproved(InvoiceID, UserId){
    try {
    
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        ObjInvoice.setFieldValue('custbody_approved_by', UserId);
        ObjInvoice.setFieldValue('custbody_approval_status', 1);
        ObjInvoice.setFieldValue('orderstatus', 'B');
        nlapiSubmitRecord(ObjInvoice);
        location.reload();
    } 
    catch (e) {
        alert('error====' + e.getDetails())
    }
    //alert('i am in client script approved function' + InvoiceID+'==='+ApprovedButtonvalue);
}

// ============================ End Function  =======================================


// ============================ Maneger Click on reject buuton  =======================================
function reject(InvoiceID, UserId){

    try {
        var retVal = prompt("Enter the rejection reason : ");
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        //var custmername = ObjInvoice.getFieldText('entity');
        /* var custmerid = ObjInvoice.getFieldValue('entity');
         var emailid = SearchEmailId(custmerid);
         if (emailid != null && emailid != 'undefined' && emailid != '') {
         nlapiSendEmail(UserId, emailid, 'your invoice is rejected', 'Dear <br><br><br> Your application for invoice is Rejected. ' + ' ' + '<br> If you have any question about your invoice,please contact your support team.' + '<br><br><br>' + 'Thank you', null, null, null)
         alert("The email has been send to employee sucessfully.");
         ObjInvoice.setFieldValue('custbody_approved_by', UserId);
         ObjInvoice.setFieldValue('custbody_approval_status', 2);
         ObjInvoice.setFieldValue('custbody_approvalrejectionnote', retVal);
         nlapiSubmitRecord(ObjInvoice);
         location.reload();
         }
         else {
         alert("employee email id is not present or employee subsidiary is different. ");
         }
         */
        ObjInvoice.setFieldValue('custbody_approved_by', UserId);
        ObjInvoice.setFieldValue('custbody_approval_status', 2);
        ObjInvoice.setFieldValue('custbody_approvalrejectionnote', retVal);
        nlapiSubmitRecord(ObjInvoice);
        location.reload();
    } 
    catch (e) {
        alert('error====' + e.getDetails())
    }
    //alert('i am in client script Reject Function' + InvoiceID+'==='+UserId);
}

// ============================ End Function  =======================================

// ============================ Search Email Id  =======================================

function SearchEmailId(id){
    //alert('i am in search' + id);
    var columns = new Array();
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is', id);
    columns[0] = new nlobjSearchColumn('email');
    
    
    // Create the saved search
    var searchresults = nlapiSearchRecord('employee', null, filter, columns);
    //alert(searchresults);
    if (searchresults != null) {
        //alert(searchresults[0].getValue('email'));
        return searchresults[0].getValue('email');
    }
    else {
        return "";
    }
}

// ============================ End Function  =======================================

// ============================ Search Email Id  =======================================

function SearchsuperwiserId(employeeid){
    var columns = new Array();
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is', employeeid);
    columns[0] = new nlobjSearchColumn('supervisor');
    
    
    // Create the saved search
    var searchresults = nlapiSearchRecord('employee', null, filter, columns);
    if (searchresults != null) {
    
        return searchresults[0].getValue('supervisor');
    }
    else {
        return "";
    }
}

// ============================ End Function  =======================================
