// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:USE_InoviceApproval.js
     Author:Rakesh Malvade
     Company:Aashna solutions
     Date:10-jun-2013
     Description:before Load On media plan Dispaly add button.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     26-06-2013 			Rakesh							Mangesh							Rejection Mail goes to created by and after save that invoice once agin all process to follow
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     BEFORE LOAD
     - beforeLoadRecord(type)
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     AFTER SUBMIT
     - afterSubmitRecord(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ===============================================



function beforeLoadRecord_submitapprovalbutton(type){
    var userid = nlapiGetUser()
    
    // if (userid == '41745' || userid == '1917') {
    
    
    
    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    if (type == 'create') {
        var i_projectid = nlapiGetFieldValue('job');
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'Project ID-->' + i_projectid);
        var ObjProject = nlapiLoadRecord('job', i_projectid);
        var ProjectManagerid = ObjProject.getFieldValue('custentity_project_manager');
         nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'ProjectManager=' + ProjectManagerid);
        if (ProjectManagerid != null && ProjectManagerid != 'undefined' && ProjectManagerid != '') {
        }
        else {
            
            throw ("Project manager field is blank in project. ");
			return false;
        }
        
        /*  if (custmerid != null && custmerid != 'undefined' && custmerid != '') {
        
         }
        
         else {
        
         nlapiSetFieldValue('custbody_created_by', userid, false, false);
        
         }*/
        
        //return true;
    }
    
    if (type == 'view') {
        form.setScript('customscript178');
        var InvoiceID = nlapiGetRecordId();
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'Invoice ID-->' + InvoiceID);
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        var emailsend = ObjInvoice.getFieldValue('custbody_submitforapproval');
        var roleid = nlapiGetRole();
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'role ID-->' + roleid);
        var SubmitApproval = nlapiGetFieldValue('custbody_submitforapproval');
        var Inovicestatus1 = ObjInvoice.getFieldValue('custbody_approval_status');
        if (SubmitApproval == 'F' && (roleid == 1021 || roleid == 1009 || roleid == 1008) && (Inovicestatus1 == null || Inovicestatus1 == 'undefined' || Inovicestatus1 == '')) {
        
            form.addButton('custpage_Button_submitapprovalbutton', 'Submit For Approval', 'forappproval(' + InvoiceID + ')');
            var button = form.getButton('edit');
            var button1 = form.getButton('credit');
            var button2 = form.getButton('acceptpayment');
            if (button != null && button != 'undefined' && button != '') {
                button.setDisabled(true);
            }
            if (button1 != null && button1 != 'undefined' && button1 != '') {
                button1.setDisabled(true);
            }
            if (button2 != null && button2 != 'undefined' && button2 != '') {
                button2.setDisabled(true);
            }
            
            
        }
        else 
            if (SubmitApproval == 'F' && (roleid == 1021 || roleid == 1009 || roleid == 1008) && (Inovicestatus1 == 1)) {
                var button = form.getButton('edit');
                if (button != null && button != 'undefined' && button != '') {
                    button.setDisabled(false);
                }
            }
            else 
                if (SubmitApproval == 'T' && (roleid == 1021 || roleid == 1009 || roleid == 1008)) {
                
                    if (Inovicestatus1 == 1) {
                        var button = form.getButton('edit');
                        //   var button1 = form.getButton('credit');
                        //  var button2 = form.getButton('acceptpayment');
                        //renewal
                        //Disable the button in the UI
                        if (button != null && button != 'undefined' && button != '') {
                            button.setDisabled(true);
                        }
                    /*if (button1 != null && button1 != 'undefined' && button1 != '') {
                 button1.setDisabled(true);
                 }
                 if (button2 != null && button2 != 'undefined' && button2 != '') {
                 button2.setDisabled(true);
                 }*/
                    }//if status is approved
                    else {
                        var button = form.getButton('edit');
                        var button1 = form.getButton('credit');
                        var button2 = form.getButton('acceptpayment');
                        //renewal
                        //Disable the button in the UI
                        if (button != null && button != 'undefined' && button != '') {
                            button.setDisabled(true);
                        }
                        if (button1 != null && button1 != 'undefined' && button1 != '') {
                            button1.setDisabled(true);
                        }
                        if (button2 != null && button2 != 'undefined' && button2 != '') {
                            button2.setDisabled(true);
                        }
                    }
                    
                }//end else role id==1
        var Inovicestatus = ObjInvoice.getFieldValue('custbody_approval_status');
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'userId=' + userId);
        if (Inovicestatus == 1) {
            var button = form.getButton('edit');
            //Disable the button in the UI
            if (button != null && button != 'undefined' && button != '') {
                button.setDisabled(true);
            }
        }
        else {
            if (emailsend == 'T') {
            
                var userId = nlapiGetUser();
                nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'Inovicestatus=' + Inovicestatus);
                var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
                var ProjectManagerid = ObjInvoice.getFieldValue('custbody_pm');
                var jobid = ObjInvoice.getFieldValue('job');
                var ObjProject = nlapiLoadRecord('job', jobid);
                var ProjectManagerid = ObjProject.getFieldValue('custentity_project_manager');
                nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'userId=' + userId);
                if (userId == ProjectManagerid) {
                    form.addButton('custpage_Button_Approvebutton', 'Approve', 'appproved(' + InvoiceID + ',' + userId + ')');
                    form.addButton('custpage_Button_Rejectbutton', 'Reject', 'reject(' + InvoiceID + ',' + userId + ')');
                    var Inovicestatus = ObjInvoice.getFieldValue('custbody_approval_status');
                    if (Inovicestatus == 1 || Inovicestatus == 2) {
                        var button = form.getButton('custpage_Button_Approvebutton');
                        var button1 = form.getButton('custpage_Button_Rejectbutton');
                        //Disable the button in the UI
                        if (button != null && button != 'undefined' && button != '') {
                            button.setDisabled(true);
                        }
                        if (button1 != null && button1 != 'undefined' && button1 != '') {
                            button1.setDisabled(true);
                        }
                        
                    }//end Disabled button 
                }//end user id and manager id same display button approve and reject
            }//end if submit approval check box check
        }//ens else inovice status in approved or rejected
    }//end if type=view
    return true;
    // }


}

// END BEFORE LOAD ====================================================

// BEGIN After LOAD ==================================================
function afterSubmitRecord(type){
    var userid = nlapiGetUser()
    nlapiLogExecution('DEBUG', 'afterLoadRecord', 'userid=' + userid);
    var newType = nlapiGetRecordType();
    nlapiLogExecution('DEBUG', 'afterLoadRecord', 'newType=' + newType);
    var contextObj = nlapiGetContext();
    
    var newType = contextObj.getExecutionContext();
    nlapiLogExecution('DEBUG', 'afterLoadRecord', 'newType=' + newType);
    //if (userid == '41745' || userid == '1917') {
    
    if (type == 'create') {
        var InvoiceID = nlapiGetRecordId();
        nlapiLogExecution('DEBUG', 'beforeLoadRecord', 'Invoice ID-->' + InvoiceID);
        forappproval(InvoiceID)
    }
    /*var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
     if (ObjInvoice != null && ObjInvoice != undefined && ObjInvoice != '') {
     ObjInvoice.setFieldValue('custbody_approval_status', 3);
     //ObjInvoice.setFieldValue('orderstatus', 'B');
     nlapiSubmitRecord(ObjInvoice);
     }
     }*/
    if (type == 'edit') {
        if (contextObj.getExecutionContext() != 'scheduled') {
            var roleid = nlapiGetRole();
            if (roleid == '3') {
            
            
                var Oldinvoice = nlapiGetOldRecord();
                nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Oldinovice=' + Oldinvoice);
                var Approvalstatus = Oldinvoice.getFieldValue('custbody_approval_status');
                nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Approvalstatus=' + Approvalstatus);
                var InvoiceID = nlapiGetRecordId();
                nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Invoice ID-->' + InvoiceID);
                var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
                var newApprovalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
                nlapiLogExecution('DEBUG', 'afterLoadRecord', 'newApprovalstatus=' + newApprovalstatus);
                if (Approvalstatus == 3 && newApprovalstatus == 1) {
                    ObjInvoice.setFieldValue('custbody_approved_by', userid);
                    ObjInvoice.setFieldValue('custbody_approval_status', 1);
                    ObjInvoice.setFieldValue('orderstatus', 'B');
                    nlapiSubmitRecord(ObjInvoice);
                    
                    
                }//end administarter is approved 
                else 
                    if (Approvalstatus == 3 && newApprovalstatus == 2) {
                    
                        ObjInvoice.setFieldValue('custbody_approved_by', userid);
                        ObjInvoice.setFieldValue('custbody_approval_status', 2);
                        nlapiSubmitRecord(ObjInvoice);
                        
                    }//end administarter is rejected
            }
            else 
                if (roleid == 1021 || roleid == 1009 || roleid == 1008) {
                    if (contextObj.getExecutionContext() != 'userevent') {
                        var InvoiceID = nlapiGetRecordId();
                        nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Invoice ID-->' + InvoiceID);
                        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
                        var newApprovalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
                        var rejectionnote = ObjInvoice.getFieldValue('custbody_rejectcheckbox');
                        if (newApprovalstatus == 2 && rejectionnote == 'T') {
                        
                            ObjInvoice.setFieldValue('custbody_approval_status', '');
                            ObjInvoice.setFieldValue('custbody_approved_by', '');
                            ObjInvoice.setFieldValue('custbody_rejectcheckbox', 'F');
                            ObjInvoice.setFieldValue('custbody_approvalrejectionnote', '');
                            nlapiSubmitRecord(ObjInvoice);
                            
                        }
                    }
                }
            
        }//check sheduled or not   
    }
    //}
}

// END BEFORE LOAD ====================================================


// ============================ Disaply Submit for approval button for accounant user  =======================================

function forappproval(InvoiceID){
    try {
        //alert('i am in client script' + InvoiceID);
        var userid = nlapiGetUser()
        //alert('userid=' + userid);
        var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
        var jobid = ObjInvoice.getFieldValue('job');
        if (jobid != null && jobid != 'undefined' && jobid != '') {
            var tranid = ObjInvoice.getFieldValue('tranid');
            //alert('jobid' + jobid);
            var ObjProject = nlapiLoadRecord('job', jobid);
            var ProjectManagerid = ObjProject.getFieldValue('custentity_project_manager');
            //alert('ProjectManager=' + ProjectManagerid);
            if (ProjectManagerid != null && ProjectManagerid != 'undefined' && ProjectManagerid != '') {
                // var ObjEmployee = nlapiLoadRecord('employee',ProjectManagerid);
                var emailid = SearchEmailId(ProjectManagerid);
                if (emailid != null && emailid != 'undefined' && emailid != '') {
                    //alert('emailid=' + emailid);
                    var click_here = 'Please Click Here.';
                    var url = 'https://system.sandbox.netsuite.com/app/accounting/transactions/custinvc.nl?id=' + InvoiceID;
                    var LinkValue = '<a href=' + url + ' target=\'_blank\'>' + click_here + '</a>';
                    //alert("LinkValue"+LinkValue);
                    nlapiSendEmail(userid, emailid, 'Invoice for your approval', '<br><br> The invoice number ' + '  ' + tranid + ' ' + 'is pending for your approval, ' + ' ' + LinkValue + ' ' + '<br><br><br>' + 'Thank you', null, null, null)
                    //alert("The email has been send to project mangaer sucessfully.");
                    ObjInvoice.setFieldValue('custbody_submitforapproval', 'T');
                    //alert('custbody_submitforapproval');
                    ObjInvoice.setFieldValue('custbody_pm', ProjectManagerid);
                    //alert('custbody_pm=');
                    ObjInvoice.setFieldValue('custbody_approval_status', 3);
                    //alert('custbody_approval_status');
                    nlapiSubmitRecord(ObjInvoice, false, false);
                    //alert('submit record=');
                    //location.reload();
                }
                else {
                    throw ("employee email id is not present or employee subsidiary is different. ");
                }
            }
            else {
                throw ("Project manager field is blank in project. ");
            }
        }
        else {
            throw ("Project Field is blank in invoice. ");
        }
        
        
    } 
    catch (e) {
        throw ('error====' + e.getDetails())
    }
    
    //window.open("https://system.sandbox.netsuite.com/app/common/custom/custrecordentry.nl?rectype=82&custrecord_mp1_soref="+SOId)

}

// ============================ End Submit for approval button for accounant user  =======================================


// ============================ Search Email Id  =======================================

function SearchEmailId(employeeid){
    var columns = new Array();
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is', employeeid);
    columns[0] = new nlobjSearchColumn('email');
    
    
    // Create the saved search
    var searchresults = nlapiSearchRecord('employee', null, filter, columns);
    if (searchresults != null) {
    
        return searchresults[0].getValue('email');
    }
    else {
        return "";
    }
}

// ============================ End Function  =======================================
