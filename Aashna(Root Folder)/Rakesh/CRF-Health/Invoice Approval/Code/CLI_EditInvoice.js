// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_EditInvoice.js
     Author:Rakesh Malvade
     Company:Aashna cloud tech
     Date:10-jun-2013
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     26-06-2013          Rakesh .m							Mangesh							
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================

function onsave_editinvoice(){
    //if (type == 'edit') {
    var roleid = nlapiGetRole();
    //alert(roleid);
    if ((roleid == 1021 || roleid == 1009 || roleid == 1008)) {
        var InvoiceID = nlapiGetRecordId();
        //alert('invoiceid==' + InvoiceID);
        if (InvoiceID != null && InvoiceID != 'undefined' && InvoiceID != '') {
            var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
            var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
            //alert('approvalstatus==' + approvalstatus);
            if (approvalstatus == 3 || approvalstatus == '' || approvalstatus == null || approvalstatus == undefined) {
                alert("The invoice cannot be edited while pending approval.  Please contact an Administrator to override.");
                return false;
            }
			else if(approvalstatus==1)
			{
				alert("You can't edit and submit the invoice beacuse this invoice allready approved");
                    return false;
			}
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    else 
        if (roleid == 3) {
            var InvoiceID = nlapiGetRecordId();
            //alert('invoiceid==' + InvoiceID);
            if (InvoiceID != null && InvoiceID != 'undefined' && InvoiceID != '') {
                var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
                var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
                //alert('approvalstatus==' + approvalstatus);
                if (approvalstatus == 1) {
                    alert("You can't edit and submit the invoice beacuse this invoice allready approved");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
            
        }
        else 
            if (roleid != 3 && (roleid == 1021 || roleid == 1009 || roleid == 1008)) {
            
            
            }
            else {
                var InvoiceID = nlapiGetRecordId();
                //alert('invoiceid==' + InvoiceID);
                if (InvoiceID != null && InvoiceID != 'undefined' && InvoiceID != '') {
                    var ObjInvoice = nlapiLoadRecord('invoice', InvoiceID);
                    var approvalstatus = ObjInvoice.getFieldValue('custbody_approval_status');
                    //alert('approvalstatus==' + approvalstatus);
                    if (approvalstatus == 1) {
                        alert("You can't edit and submit the invoice beacuse this invoice allready approved");
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true
                }
            }
    //}
}
