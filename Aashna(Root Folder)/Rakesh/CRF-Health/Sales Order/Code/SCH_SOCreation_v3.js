// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_SOCreation.js
     Author: Rakesh malvade
     Company: Aashna Cloud tech
     Date: 16-Apr-2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     02-09-2013				Rakesh						  Sachin.K						On project Sales order link add.
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - CreateWeeklyStockReport(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function CreateSalesorder(type){
    var CustRecID = '';
    
    try {
    
        var contextObj = nlapiGetContext();
        
        //total usage
        var beginUsage = contextObj.getRemainingUsage();
        nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
        
        //getting the Parameter value
        var fileId_Param = contextObj.getSetting('SCRIPT', 'custscript_sofileid_v3');
        nlapiLogExecution('DEBUG', 'Search Results', 'fileId_Param :' + fileId_Param);
        
        
        
        //get the results from save search 
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('Internalid');
        columns[1] = new nlobjSearchColumn('name');
        
        var filter = new Array();
        
        if (_logValidation(fileId_Param)) {
            filter.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', fileId_Param));
        }
        
        var searchResult = nlapiSearchRecord(null, '152', filter, columns);
        
        if (searchResult != null) {
            for (var i_i = 0; i_i < searchResult.length; i_i++) {
                try {
                    //get file id from save search
                    var Fileid = searchResult[i_i].getValue('internalid');
                    nlapiLogExecution('DEBUG', 'Test Schedule', 'Internalid ' + i_i + '=' + Fileid);
                    
                    var s_FileName = searchResult[i_i].getValue('name');
                    nlapiLogExecution('DEBUG', 'Test Schedule', 'File Name ' + i_i + '=' + s_FileName);
                    
                    
                    //Get the Name of the File
                    
                    
                    var isSO = s_FileName.toString().match(/SO/);
                    nlapiLogExecution('DEBUG', 'Search Results', 'is file name  :' + isSO);
                    
                    var isCIS = s_FileName.toString().match(/CIS/);
                    nlapiLogExecution('DEBUG', 'Search Results', 'is file name :' + isCIS);
                    
                    if (!_logValidation(isSO)) {
                        isSO = s_FileName.toString().match(/so/);
                        nlapiLogExecution('DEBUG', 'Search Results', 'isSO :' + isSO);
                        
                    }
                    
                    if (!_logValidation(isCIS)) {
                        isCIS = s_FileName.toString().match(/cis/);
                        nlapiLogExecution('DEBUG', 'Search Results', 'is file name :' + isCIS);
                    }
                    nlapiLogExecution('DEBUG', 'Search Results', 'is file name :' + isCIS);
                    //if it is SO File then Move that File into Process Folder
                    if ((isSO == 'SO' || isSO == 'so') && !_logValidation(isCIS)) {
                    
                        nlapiLogExecution('DEBUG', 'New sales order', 'in csv file cis name is not present :');
                        var fileobj = nlapiLoadFile(Fileid);
                        
                        //Get the File Contents and Create/Update the Estimate
                        var records = GetFileContents(fileobj);
                        nlapiLogExecution('DEBUG', 'Search Results', 'records :' + records);
                        
                        var split = records.toString().split('#$');
                        nlapiLogExecution('DEBUG', 'Search Results', 'split Length*************** :' + split.length);
                        
                        //Usage to execute the Current file with respect to the line items
                        var cal_Usage = parseInt(parseInt([parseInt(split.length) * parseInt(5)]) + parseInt(92));
                        nlapiLogExecution('DEBUG', 'Search Results', 'cal_Usage-' + i_i + ' :' + cal_Usage);
                        
                        //current remaining usage
                        var remainingUsage = contextObj.getRemainingUsage();
                        nlapiLogExecution('DEBUG', 'Search Results', 'remainingUsage-' + i_i + ' :' + remainingUsage);
                        
                        //if (remainingUsage > cal_Usage) {
                        if (remainingUsage > 9100) {
                            //move in folder 	
                            fileobj.setFolder(1315);
                            nlapiSubmitFile(fileobj);
                            
                            
                            //Create the Custom Record
                            
                            var CustRec = CreateCustomRecord(Fileid, s_FileName);
                            var CustRecID = CustRec[0];
                            nlapiLogExecution('DEBUG', ' Custom Record', ' Custom Record ID :' + CustRecID);
                            //Get the File Contents and Create/Update the sales order
                            var records = GetFileContents(fileobj);
                            
                            
                            //create or update sales order 
                            var status = CreateUpdateSalesorder(records);
                            var splitArray = '';
                            nlapiLogExecution('DEBUG', 'main function', 'Sales order status :' + status);
                            //update the custom record 			
                            if (_logValidation(status) && _logValidation(CustRecID)) {
                                //Update the Custom Record
                                UpdateCustomRecord(status, CustRecID);
                            }
                            
                        }//end of if(remainingUsage > cal_Usage)
                        else {
                            Schedulescriptafterusageexceeded(Fileid);
                            break;
                        }
                        
                    }
                    //----------------------------if file  name cis is present -----------------------	
                    //if civ file is there 
                    else 
                        if ((isSO == 'SO' || isSO == 'so') && (isCIS == 'CIS' || isSO == 'cis')) {
                        
                            nlapiLogExecution('DEBUG', 'updation sales order', 'in csv file cis name is present :');
                            var fileobj = nlapiLoadFile(Fileid);
                            
                            //Get the File Contents and Create/Update the Estimate
                            var records = GetFileContents(fileobj);
                            nlapiLogExecution('DEBUG', 'Search Results', 'records :' + records);
                            
                            var split = records.toString().split('#$');
                            nlapiLogExecution('DEBUG', 'Search Results', 'split Length*************** :' + split.length);
                            
                            //Usage to execute the Current file with respect to the line items
                            var cal_Usage = parseInt(parseInt([parseInt(split.length) * parseInt(5)]) + parseInt(92));
                            nlapiLogExecution('DEBUG', 'Search Results', 'cal_Usage-' + i_i + ' :' + cal_Usage);
                            
                            //current remaining usage
                            var remainingUsage = contextObj.getRemainingUsage();
                            nlapiLogExecution('DEBUG', 'Search Results', 'remainingUsage-' + i_i + ' :' + remainingUsage);
                            
                            //if (remainingUsage > cal_Usage) {
                            if (remainingUsage > 9900) {
                                //move in folder 	
                                fileobj.setFolder(1315);
                                nlapiSubmitFile(fileobj);
                                
                                
                                //Create the Custom Record
                                
                                var CustRec = CreateCustomRecord(Fileid, s_FileName);
                                var CustRecID = CustRec[0];
                                nlapiLogExecution('DEBUG', ' Custom Record', ' Custom Record ID :' + CustRecID);
                                //Get the File Contents and Create/Update the sales order
                                var records = GetFileContents(fileobj);
                                
                                
                                // update sales order 
                                var status = UpdateSalesorder(records);
                                var splitArray = '';
                                
                                //update the custom record 			
                                if (_logValidation(status) && _logValidation(CustRecID)) {
                                    //Update the Custom Record
                                    UpdateCustomRecord(status, CustRecID);
                                }
                                
                            }//end of if(remainingUsage > cal_Usage)
                            else {
                                Schedulescriptafterusageexceeded(Fileid);
                                break;
                            }
                        }
                } 
                catch (ex) {
                    nlapiLogExecution('DEBUG', 'Search Results', 'ex :' + ex);
                }
                
            }//for end
        }
        var endUsage = contextObj.getRemainingUsage();
        nlapiLogExecution('DEBUG', 'Search Results', 'endUsage :' + endUsage);
    } 
    
    catch (e) {
        //nlapiLogExecution('ERROR',e.getDetails());
        var status1 = "" + "#" + e.getDetails();
        if (_logValidation(status1) && _logValidation(CustRecID)) {
            //Update the Custom Record
            UpdateCustomRecord(status, CustRecID);
        }
    }
    
}



// END FUNCTION =====================================================

function _logValidation(value){
    if (value != null && value != '' && value != undefined) {
        return true;
    }
    else {
        return false;
    }
}

function _logValidation1(value){
    if (value != null && value != '' && value != undefined) {
        return value;
    }
    else {
        return '';
    }
}

//==================================function to Create Custom record===============================================//

//function to Create Custom record
function CreateCustomRecord(fileId, fileName){
    if (_logValidation(fileId)) {
        var id = '';
        if (_logValidation(fileName)) {
            id = searchCustomRecord(fileName, fileId)
            
        }
        
        var fileObj = nlapiLoadFile(fileId);
        
        if (!_logValidation(id)) {
            fileObj.setFolder(1315);
            var newID = nlapiSubmitFile(fileObj);
            
            var custRecordObj = nlapiCreateRecord('customrecord_csvuploadreferencerecord');
            
            custRecordObj.setFieldValue('custrecord_file', fileId);
            custRecordObj.setFieldValue('custrecord_status', 5);
            custRecordObj.setFieldValue('custrecord_recordtype', 'Sales order');
            
            if (_logValidation(fileName)) {
                custRecordObj.setFieldValue('custrecord_filename', fileName);
            }
            
            var id = nlapiSubmitRecord(custRecordObj, false, true);
            nlapiLogExecution('DEBUG', 'New Custom Record', 'New Record ID :' + id);
            if (_logValidation(id)) {
                return [id, false];
            }
            else {
                return [];
            }
        }
        else { //return results[0].getValue('internalid');
            var CustRecObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', id);
            var PrevFileId = CustRecObj.getFieldValue('custrecord_file');
            nlapiLogExecution('DEBUG', 'searchCustomRecord', 'Previous File ID :' + PrevFileId);
            nlapiLogExecution('DEBUG', 'searchCustomRecord', 'New File ID :' + fileId);
            if (_logValidation(PrevFileId)) {
                nlapiDeleteFile(PrevFileId);
            }
            
            CustRecObj.setFieldValue('custrecord_status', 3);
            
            fileObj.setFolder(1315);
            var newID = nlapiSubmitFile(fileObj);
            
            CustRecObj.setFieldValue('custrecord_file', newID);
            var UpdatedRec = nlapiSubmitRecord(CustRecObj);
            nlapiLogExecution('DEBUG', 'Update Custom Record', 'Updated Custom Record ID :' + UpdatedRec);
            return [id, true];
        }
        
    }
    else {
        return [];
    }
}

// END FUNCTION =====================================================



//===========================Function to get the file contents =====================================//

function GetFileContents(fileobj){
    if (_logValidation(fileobj)) {
        var filedecodedrows = '';
        
        var fileContents = fileobj.getValue();
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'fileContents=' + fileContents + '# Type =' + fileobj.getType());
        
        // Begin Code : To parse The data.
        
        if ((fileobj.getType() == 'EXCEL') || (fileobj.getType() == 'MISCTEXT')) {
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ excel file');
            filedecodedrows = decode_base64(fileContents);
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After filedecodedrows=' + filedecodedrows);
        }
        else {
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ other than excel file');
            filedecodedrows = fileContents;
            nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After fileContents=' + fileContents);
        }
        
        // End Code : To parse The data.
        
        var arr = filedecodedrows.split(/\n/g);
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'base64Ecodedstring=' + arr.length);
        
        var Length = parseInt(arr.length) - parseInt(1);
        
        var FullContents = '';
        
        for (var i = 1; i < Length; i++) {
            //Begin Code : to get the data from the CSV Row wise
            
            arr[i] = parseCSV(arr[i]);
            if (i != (Length - 1)) {
                FullContents += arr[i] + '#$';
            }
            else {
                FullContents += arr[i];
            }
        }
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'FullContents =' + FullContents);
        return FullContents;
    }
}

function decode_base64(s){
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
    
    for (z in n) {
        for (i = n[z][0]; i < n[z][1]; i++) {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++) {
        e[v[i]] = i;
    }
    
    for (i = 0; i < s.length; i += 72) {
        var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
    
}

function parseCSV(csvString){
    var fieldEndMarker = /([,\015\012] *)/g; /* Comma is assumed as field separator */
    var qFieldEndMarker = /("")*"([,\015\012] *)/g; /* Double quotes are assumed as the quote character */
    var startIndex = 0;
    var records = [], currentRecord = [];
    do {
        // If the to-be-matched substring starts with a double-quote, use the qFieldMarker regex, otherwise use fieldMarker.
        var endMarkerRE = (csvString.charAt(startIndex) == '"') ? qFieldEndMarker : fieldEndMarker;
        endMarkerRE.lastIndex = startIndex;
        var matchArray = endMarkerRE.exec(csvString);
        if (!matchArray || !matchArray.length) {
            break;
        }
        var endIndex = endMarkerRE.lastIndex - matchArray[matchArray.length - 1].length;
        var match = csvString.substring(startIndex, endIndex);
        if (match.charAt(0) == '"') // The matching field starts with a quoting character, so remove the quotes
        {
            match = match.substring(1, match.length - 1).replace(/""/g, '"');
        }
        currentRecord.push(match);
        var marker = matchArray[0];
        if (marker.indexOf(',') < 0) // Field ends with newline, not comma
        {
            records.push(currentRecord);
            currentRecord = [];
        }
        startIndex = endMarkerRE.lastIndex;
    }
    while (true);
    if (startIndex < csvString.length) { // Maybe something left over?
        var remaining = csvString.substring(startIndex).trim();
        if (remaining) 
            currentRecord.push(remaining);
    }
    if (currentRecord.length > 0) { // Account for the last record
        records.push(currentRecord);
    }
    //nlapiLogExecution('DEBUG','parseCSV','records :'+records);
    return records;
}

// END FUNCTION =====================================================



//===========================================Function to create & Update the Sales order Record===========================================//
function CreateUpdateSalesorder(records){
    if (_logValidation(records)) {
        var tranID = '';
        //nlapiLogExecution('DEBUG', 'New Sales order ', 'Records  :' + records);
        
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Search Results', 'split Length :' + split.length);
        
        if (_logValidation(split)) {
            //for(var j=0;j<split.length;j++)
            {
                var splitArray = split[0].toString().split('|');
                
                tranID = splitArray[0];
            }
            
            //Search Sales Order Record Using the TranID
            //search Sales Order 
            if (_logValidation(tranID)) {
                var SOID = SearchSalesOrder(tranID);
                nlapiLogExecution('DEBUG', 'Sales order ', 'Sales order id :' + SOID);
                
                
                //update so 
                if (_logValidation(SOID)) {
                    /*  var soobj = nlapiLoadRecord('salesorder', SOID);
                     var linecount = soobj.getLineItemCount('item');
                     
                     for (var i = linecount; i > 0; i--) {
                     soobj.removeLineItem('item', i);
                     }
                     for (var j = 0; j < split.length; j++) {
                     var splitArray = split[j].toString().split(',');
                     
                     //for(var k=0;k<splitArray.length;k++)
                     //	{
                     
                     
                     if (_logValidation(splitArray[1])) {
                     soobj.setFieldText('custbody_crf_project', splitArray[1]);
                     nlapiLogExecution('DEBUG', 'updated sales order ', 'custbody_crf_project :' + splitArray[1]);
                     
                     
                     if (j == 0) {
                     /*
                     if(_logValidation(splitArray[0]))
                     {
                     soobj.setFieldValue('tranid',splitArray[0]);
                     nlapiLogExecution('DEBUG','updated sales order ','tranid :'+splitArray[0]);
                     }
                     if(_logValidation(splitArray[2]))
                     {
                     soobj.setFieldText('orderstatus',splitArray[2]);
                     nlapiLogExecution('DEBUG','updated sales order ','orderstatus :'+splitArray[2]);
                     }
                     
                     if(_logValidation(splitArray[7]))
                     {
                     soobj.setFieldText('currency',splitArray[7]);
                     nlapiLogExecution('DEBUG','updated sales order ','currency :'+splitArray[7]);
                     }
                     
                     if (_logValidation(splitArray[8])) {
                     soobj.setFieldText('entity', splitArray[8]);
                     nlapiLogExecution('DEBUG', 'updated sales order ', 'entity :' + splitArray[8].trim());
                     }
                     if (_logValidation(splitArray[13])) {
                     var Opportunity = getOpportunity(splitArray[13])
                     nlapiLogExecution('DEBUG', 'Set Opportunity  ', 'Set Opportunity===  :' + Opportunity);
                     if (_logValidation(Opportunity)) {
                     soobj.setFieldValue('opportunity', Opportunity)
                     }
                     else {
                     var status = "" + "#" + "Given Extimate opportunity is not Available but present in CSv File";
                     return status.toString();
                     }
                     
                     }
                     else {
                     var status = "" + "#" + "Given Extimate opportunity is not Available but present in CSv File";
                     return status.toString();
                     
                     }
                     if (_logValidation(splitArray[9])) {
                     soobj.setFieldValue('trandate', splitArray[9]);
                     nlapiLogExecution('DEBUG', 'updated sales order ', 'trandate :' + splitArray[9]);
                     }
                     
                     if (_logValidation(splitArray[10])) {
                     var deptId = getDeptID(splitArray[10]);
                     soobj.setFieldValue('department', deptId);
                     nlapiLogExecution('DEBUG', 'updated sales order ', 'department :' + splitArray[10]);
                     }
                     if (_logValidation(splitArray[11])) {
                     soobj.setFieldText('location', splitArray[11]);
                     nlapiLogExecution('DEBUG', 'updated sales order ', 'location :' + splitArray[10]);
                     }
                     
                     
                     }
                     
                     if (_logValidation(splitArray[3])) {
                     var itemId = getItemID(splitArray[3]);
                     
                     if (_logValidation(itemId)) {
                     soobj.selectNewLineItem('item');
                     soobj.setCurrentLineItemValue('item', 'item', itemId);
                     
                     if (_logValidation(splitArray[4])) {
                     soobj.setCurrentLineItemValue('item', 'quantity', splitArray[4]);
                     }
                     
                     if (_logValidation(splitArray[5])) {
                     soobj.setCurrentLineItemValue('item', 'rate', splitArray[5]);
                     }
                     if (_logValidation(splitArray[6])) {
                     soobj.setCurrentLineItemValue('item', 'revrecstartdate', splitArray[6]);
                     }
                     if (_logValidation(splitArray[14])) {
                     soobj.setCurrentLineItemValue('item', 'revrecschedule', splitArray[14]);
                     }
                     if (_logValidation(splitArray[15])) {
                     soobj.setCurrentLineItemValue('item', 'revrecenddate', splitArray[15]);
                     }
                     
                     if (_logValidation(splitArray[17])) {
                     
                     var TaxcodeId = getTaxcodeID(splitArray[17]);
                     
                     if (_logValidation(TaxcodeId)) {
                     soobj.setCurrentLineItemValue('item', 'taxcode', TaxcodeId);
                     }
                     }
                     
                     soobj.commitLineItem('item');
                     }
                     
                     }
                     
                     }
                     else {
                     
                     var status = "" + "#" + "Project Is not Available In CSV File ,Please Enter Project Detalis";
                     return status.toString();
                     }
                     }
                     
                     var id = '';
                     
                     var status = "";
                     
                     try {
                     id = nlapiSubmitRecord(soobj);
                     nlapiLogExecution('DEBUG', 'updated sales order', 'updated sales order ID  :' + id);
                     
                     //update opportunity
                     
                     if (_logValidation(soobj.getFieldValue('opportunity'))) {
                     updateopportunity(soobj.getFieldValue('opportunity'), soobj.getFieldValue('createdfrom'));
                     }
                     
                     status = id + "#" + "" + "#" + "Update SO";
                     
                     return status.toString();
                     }
                     catch (exception) {
                     nlapiLogExecution('DEBUG', 'updated sales order Catch Block :' + exception);
                     
                     status = SOID + "#" + exception.getDetails();
                     nlapiLogExecution('DEBUG','status', 'New Sales order Catch Block :' + status);
                     return status.toString();
                     }
                     
                     */
                    var status = "" + "#" + "The sales order number mentioned in column[0] is allready present in netsuite.";
                    return status.toString();
                }//end of if(_logValidation(SOID)) update so
                //create new sales order 
                
                else {
                
                
                    nlapiLogExecution('DEBUG', 'new sales order ', 'Estimate ID  :' + splitArray[12]);
                    var estid = '';
                    var soobj = '';
                    var OopTranID = '';
                    var proj_obj = '';
                    if (_logValidation(splitArray[12])) {
                        estid = getEstimate(splitArray[12])
                        if (!_logValidation(estid)) {
                            var status = "" + "#" + "The estimate number mentioned in column[12] is not present in netsuite.";
                            return status.toString();
                        }
                        nlapiLogExecution('DEBUG', 'get Estimate  ', 'get Estimate===  :' + estid);
                        
                        //check opprchunites
                        
                        var Objestimate = nlapiLoadRecord('estimate', estid);
                        var oppid = Objestimate.getFieldValue('opportunity');
                        var customer_name = Objestimate.getFieldValue('entity');
                        var customer_name1 = Objestimate.getFieldText('entity');
                        var estcurrency = Objestimate.getFieldText('currency');
                        //serach Tran id 
                        OopTranID = getOopTranid(oppid)
                        if (_logValidation(OopTranID)) {
                            if (OopTranID != splitArray[13]) {
                                var status = "" + "#" + "The opportunity number mentioned in column[13] is not match with on estimate opportunity number.";
                                return status.toString();
                            }
                        }
                        /*     var OopTranID = getOopTranid(oppid)
                         nlapiLogExecution('DEBUG', 'Opp tarn ID', 'OopTranID :' + OopTranID);
                         for (var m = 0; m < split.length; m++) {
                         var splitArray = split[m].toString().split(',');
                         if (_logValidation(OopTranID)) {
                         if (OopTranID.trim() != splitArray[13].trim()) {
                         var status = "" + "#" + "The opportunity number mentioned in column[13] is not associated with customer or the opportunity status is closed.";
                         return status.toString();
                         }
                         }
                         }*/
                        //search oppchunities status
                        var OopStatus = getOopStatus(oppid)
                        nlapiLogExecution('DEBUG', 'Opp Status', 'OopStatus :' + OopStatus);
                        if (_logValidation(OopStatus)) {
                            if (OopStatus.trim() == 'closedWon') {
                                var status = "" + "#" + "The opportunity number mentioned in column[13] is not associated with customer or the opportunity status is closed.";
                                return status.toString();
                            }
                        }
                        
                        
                        
                        
                        
                        if (_logValidation(estid)) {
                            soobj = nlapiTransformRecord('estimate', estid, 'salesorder');
                        }
                        else {
                            var status = "" + "#" + "The Estimate number mentioned in column[12] is not present in Netsuite";
                            return status.toString();
                        }
                        
                    }
                    else {
                        var status = "" + "#" + " The Estimate number mentioned in column[12] is not present in CSV File";
                        return status.toString();
                        
                    }
                    
                    
                    var linecount = soobj.getLineItemCount('item');
                    
                    for (var i = linecount; i > 0; i--) {
                        soobj.removeLineItem('item', i);
                    }
                    
                    
                    for (var j = 0; j < split.length; j++) {
                        //nlapiLogExecution('DEBUG', 'new sales order ', 'i am in for loop j value is=  :' + j);
                        var splitArray = split[j].toString().split('|');
                        
                        //	for(var k=0;k<splitArray.length;k++)
                        //	{
                        if (_logValidation(splitArray[1])) {
                            // soobj.setFieldText('custbody_crf_project', splitArray[1]);
                            // soobj.setFieldText('custbody_crf_project', splitArray[1]);
                            
                            
                            //search project
                            var projectid = getProject(splitArray[1].trim());
                            nlapiLogExecution('DEBUG', 'new sales order ', 'project id :' + projectid);
                            
                            if (_logValidation(projectid)) {
                                //search customer name on project
                                nlapiLogExecution('DEBUG', 'new sales order ', 'customer name' + customer_name);
                                proj_obj = nlapiLoadRecord('job', projectid);
                                var proj_cust_name = proj_obj.getFieldValue('parent');
                                nlapiLogExecution('DEBUG', 'new sales order ', 'project customer name :' + proj_cust_name);
                                if (proj_cust_name == customer_name) {
                                    soobj.setFieldText('job', splitArray[1]);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'custbody_crf_project :' + splitArray[1]);
                                }
                                else {
                                    var status = "" + "#" + "The project number mentioned in column[1] is not associated with customer.";
                                    return status.toString();
                                }
                                
                            }
                            else {
                                var status = "" + "#" + " The project number mentioned in column[1] is not present in Netsuite";
                                return status.toString();
                            }
                            
                            if (_logValidation(splitArray[8])) {
                                nlapiLogExecution('DEBUG', 'customer name in File', 'customer name in file :' + splitArray[8].trim());
                                nlapiLogExecution('DEBUG', 'customer name on estimare', 'estimate customer name1 :' + customer_name1);
                                if (splitArray[8].trim() != customer_name1) {
                                    var status = "" + "#" + " The customer name mentioned in column[8] is not match with on estimate customer name";
                                    return status.toString();
                                }
                                
                            }
                            
                            if (_logValidation(splitArray[7])) {
                                nlapiLogExecution('DEBUG', 'currency name in File', 'currency name in file :' + splitArray[7].trim());
                                nlapiLogExecution('DEBUG', 'currency name on estimare', 'estimate currency name1 :' + estcurrency);
                                if (splitArray[7].trim() != estcurrency) {
                                    var status = "" + "#" + " The currency name mentioned in column[7] is not match with on estimate currency name";
                                    return status.toString();
                                }
                            }
                            
                            
                            if (j == 0) {
                            
                            
                                if (_logValidation(tranID)) {
                                    soobj.setFieldValue('tranid', tranID);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'tranid :' + tranID);
                                }
                                
                                if (_logValidation(splitArray[2])) {
                                    soobj.setFieldText('orderstatus', splitArray[2]);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'orderstatus :' + splitArray[2]);
                                }
                                
                                if (_logValidation(splitArray[7])) {
                                    soobj.setFieldText('currency', splitArray[7]);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'currency :' + splitArray[7]);
                                }
                                
                                if (_logValidation(splitArray[8])) {
                                    soobj.setFieldText('entity', splitArray[8]);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'entity :' + splitArray[8].trim());
                                }
                                if (_logValidation(splitArray[13])) {
                                    var Opportunity = getOpportunity(splitArray[13].trim())
                                    nlapiLogExecution('DEBUG', 'Set Opportunity  ', ' Opportunity===  :' + Opportunity);
                                    if (_logValidation(Opportunity)) {
                                        soobj.setFieldValue('opportunity', Opportunity)
                                    }
                                    else {
                                        var status = "" + "#" + "The Opportunity number mentioned in column[13] is not present in Netsuite";
                                        return status.toString();
                                    }
                                    
                                }
                                else {
                                    var status = "" + "#" + " The Opportunity number mentioned in column[13] is not present in CSV File";
                                    return status.toString();
                                    
                                }
                                if (_logValidation(splitArray[9])) {
                                    soobj.setFieldValue('trandate', splitArray[9]);
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'trandate :' + splitArray[9]);
                                }
                                
                                if (_logValidation(splitArray[10])) {
                                    var deptId = getDeptID(splitArray[10]);
                                    if (_logValidation(deptId)) {
                                        soobj.setFieldValue('department', deptId);
                                        nlapiLogExecution('DEBUG', 'new sales order ', 'department :' + splitArray[10]);
                                    }
                                    else {
                                        var status = "" + "#" + "The department name mentioned in column[10] is not present in Netsuite";
                                        return status.toString();
                                    }
                                }
                                else {
                                    var status = "" + "#" + " The department name mentioned in column[10] is not present in CSV File,it is mandetory filed in sales order";
                                    return status.toString();
                                }
                                
                                if (_logValidation(splitArray[11])) {
                                    soobj.setFieldText('location', splitArray[11]);
                                }
                                else {
                                    var status = "" + "#" + " The location name mentioned in column[11] is not present in CSV File, it is mandetory filed in sales order";
                                    return status.toString();
                                }
                                
                                if (_logValidation(splitArray[12])) {
                                    soobj.setFieldValue('createdfrom', estid);
                                }
                                if (_logValidation(splitArray[16])) {
                                    soobj.setFieldValue('exchangerate', splitArray[16]);
                                }
                                
                            }
                            
                            if (_logValidation(splitArray[3])) {
                                var itemId = getItemID(splitArray[3]);
                                
                                if (_logValidation(itemId)) {
                                    soobj.selectNewLineItem('item');
                                    soobj.setCurrentLineItemValue('item', 'item', itemId);
                                    
                                    if (_logValidation(splitArray[4])) {
                                        soobj.setCurrentLineItemValue('item', 'quantity', splitArray[4]);
                                    }
                                    
                                    if (_logValidation(splitArray[5])) {
                                        soobj.setCurrentLineItemValue('item', 'rate', splitArray[5]);
                                    }
                                    if (_logValidation(splitArray[6])) {
                                        soobj.setCurrentLineItemValue('item', 'revrecstartdate', splitArray[6]);
                                    }
                                    if (_logValidation(splitArray[14])) {
                                        soobj.setCurrentLineItemValue('item', 'revrecschedule', splitArray[14]);
                                    }
                                    if (_logValidation(splitArray[15])) {
                                        soobj.setCurrentLineItemValue('item', 'revrecenddate', splitArray[15]);
                                    }
                                    
                                    if (_logValidation(splitArray[17])) {
                                        nlapiLogExecution('DEBUG', 'new sales order ', 'Tax Code Name to pass   :' + splitArray[17]);
                                        var TaxcodeId = getTaxcodeID(splitArray[17]);
                                        nlapiLogExecution('DEBUG', 'new sales order ', 'Tax Code ID  :' + TaxcodeId);
                                        if (_logValidation(TaxcodeId)) {
                                        
                                            soobj.setCurrentLineItemValue('item', 'taxcode', TaxcodeId);
                                            
                                        }
                                    }
                                    nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule name  :' + splitArray[21]);
                                    if (_logValidation(splitArray[21])) {
                                    
                                        var myFld1 = soobj.getLineItemField('item', 'billingschedule', 1);
                                        var options1 = myFld1.getSelectOptions();
                                        var flag = 0;
                                        for (var m = 0; m < options1.length; m++) {
                                        
                                            nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule name 2 :' + splitArray[21]);
                                            if (options1[m].getText() == splitArray[21]) {
                                                flag = 1;
                                                nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule name  :' + options1[m].getText() + '==' + splitArray[21]);
                                                var billscheduleid = options1[m].getId();
                                                nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule ID  :' + billscheduleid);
                                                soobj.setCurrentLineItemValue('item', 'billingschedule', billscheduleid);
                                            }
                                            
                                        }
                                        if (flag == 0) {
                                            var status = "" + "#" + " The Bill Schedule name mentioned in column[21] is not present in Net Suite";
                                            return status.toString();
                                        }
                                        
                                    }
                                    
                                    
                                    soobj.commitLineItem('item');
                                }
                                else {
                                    var status = "" + "#" + "The Item mentioned in column[3] is not present in Netsuite";
                                    return status.toString();
                                }
                                
                            }
                            
                        }
                        else {
                            var status = "" + "#" + "Project Is not Available in CSV File ,Please Enter Project Detalis";
                            return status.toString();
                        }
                    }
                    
                    var id = '';
                    
                    var status = "";
                    
                    try {
                    
                        // nlapiLogExecution('DEBUG', 'New Sales Order   :', 'New Sales Order opportunity Value' + soobj.getFieldValue('opportunity'));
                        
                        
                        id = nlapiSubmitRecord(soobj, true, true);
                        
                        
                        nlapiLogExecution('DEBUG', 'New Sales Order   :', 'New Sales Order ID =' + id);
                        
                        //update project ON PROJECT SO  LINK //02/09/2013
                        
                        proj_obj.setFieldValue('custentity_sow_reference', id);
                        var i_projid = nlapiSubmitRecord(proj_obj, true, true);
                        nlapiLogExecution('DEBUG', 'Update project   :', 'update project ID =' + i_projid);
                        
                        //update opportunity
                        
                        
                        if (_logValidation(soobj.getFieldValue('opportunity'))) {
                            updateopportunity(soobj.getFieldValue('opportunity'), estid);
                        }
                        
                        
                        
                        status = OopTranID + "#" + "" + "#" + "NewSO";
                        nlapiLogExecution('DEBUG', 'New Sales Order   :', 'New Sales Order status=' + status);
                        
                        return status.toString();
                    } 
                    catch (exception) {
                        nlapiLogExecution('DEBUG', 'New Sales order Catch Block :' + exception);
                        
                        status = '' + "#" + 'Error code:- ' + exception.getCode() + ', ' + 'Error Details:- ' + exception.getDetails();
                        nlapiLogExecution('DEBUG', 'status', 'New Sales order Catch Block :' + status);
                        
                        
                        return status.toString();
                    }
                    
                    
                }
                
            }//end of if(_logValidation(tranID))
            else {
            
                var status = "" + "#" + "Tran ID Is not Available in File,Please Enter Tran Id in File";
                return status.toString();
            }
        }//end of if(_logValidation(split))
    }//end of if(_logValidation(records))
}//end of function
//-------------------------------------------------------Search the sales order using the Tran Id--------------------------------------------//
//search the sales order using the Tran Id
function SearchSalesOrder(TranId){
    if (_logValidation(TranId)) {
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var filters = new Array();
        filters.push(new nlobjSearchFilter('tranid', null, 'is', TranId));
        filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
        
        var results = nlapiSearchRecord('salesorder', null, filters, columns);
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'search sales order ', 'Results Length :' + results.length);
            
            return results[0].getValue('internalid');
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

//-------------------------------------------------------Function to get the Item ID---------------------------------------
//
function getItemID(itemName){
    if (_logValidation(itemName)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('name', null, 'is', itemName);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('item', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'getItemID', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'getItemID', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

//-------------------------------------------------------Function to get the deptName---------------------------------------

//Function to get the deptName
function getDeptID(deptName){
    if (_logValidation(deptName)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('name', null, 'is', deptName);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('department', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'deptName', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'deptName', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
    
}

//-------------------------------------------------------searchCustomRecord---------------------------------------

function searchCustomRecord(fileName, fileId){
    if (_logValidation(fileName)) {
        var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_filename', null, 'is', fileName));
        
        var columns = new Array();
        columns[columns.length] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('customrecord_csvuploadreferencerecord', null, filters, columns);
        
        if (_logValidation(results)) {
            return results[0].getValue('internalid');
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

//-------------------------------------------------------//function to Update the Custom Record---------------------------------------

function UpdateCustomRecord(status, CustRecId){
    if (_logValidation(status) && _logValidation(CustRecId)) {
        var statusSplit = status.toString().split('#');
        var custObj = nlapiLoadRecord('customrecord_csvuploadreferencerecord', CustRecId);
        if (_logValidation(statusSplit[0])) {
        
        }
        
        
        if (_logValidation(statusSplit[2])) {
            var NewSO = statusSplit.toString().match(/NewSO/);
            if (NewSO == 'NewSO') {
                custObj.setFieldValue('custrecord_createdrecords', ' New Sales Order ID: ' + statusSplit[0]);
            }
            else {
                custObj.setFieldValue('custrecord_updatedrecords', 'Updated Sales order ID: ' + statusSplit[0]);
            }
            
            custObj.setFieldValue('custrecord_status', 1);
            
        }
        if (_logValidation(statusSplit[1])) {
            custObj.setFieldValue('custrecord_status', 2);
            
            custObj.setFieldValue('custrecord_errorlog', statusSplit[1]);
        }
        else {
            custObj.setFieldValue('custrecord_errorlog', '');
        }
        
        
        var id = nlapiSubmitRecord(custObj, false, true);
        nlapiLogExecution('DEBUG', 'Custom Record ', 'Custom Record ID :' + id);
        
    }
}


//-------------------------------------------------------//Function to get the Tax code  ID---------------------------------------
//Function to get the Tax code  ID
function getTaxcodeID(taxcodeName){
    if (_logValidation(taxcodeName)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('name', null, 'is', taxcodeName);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('salestaxitem', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'Tax Code', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'TaxCode', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}


//-------------------------------------------------------//Function to updateopportunity---------------------------------------
function updateopportunity(oppid, estimateid){


    // Define search filters
    var a_Number = new Array();
    var filters = new Array();
    //filters[0] = new nlobjSearchFilter('type', null, 'is', 'estimate' );
    filters[0] = new nlobjSearchFilter('opportunity', null, 'is', oppid);
    filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
    
    // Define search columns
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    
    
    // Execute the search. You must specify the internal ID of the record type.
    var searchresults = nlapiSearchRecord('estimate', null, filters, columns);
    
    for (var i = 0; searchresults != null && i < searchresults.length; i++) {
        a_Number = searchresults[i].getValue('internalid');
        
        if (a_Number != estimateid) {
            var Estobj = nlapiLoadRecord('estimate', a_Number);
            Estobj.setFieldValue('entitystatus', 23);
            var id = nlapiSubmitRecord(Estobj, false, true);
            nlapiLogExecution('DEBUG', 'Update Estimated', 'estimate close Id  :' + id);
        }
    }
    
    
    
}

//-------------------------------------------------------//Function to getOpportunity---------------------------------------
function getOpportunity(TranId){
    if (_logValidation(TranId)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('tranid', null, 'is', TranId);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('opportunity', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'opportunity', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'opportunity', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

//-------------------------------------------------------//Function to Schedulescriptafterusageexceeded---------------------------------------
function Schedulescriptafterusageexceeded(Fileid){
    ////Define all parameters to schedule the script for voucher generation.
    var params = new Array();
    params['status'] = 'scheduled';
    params['runasadmin'] = 'T';
    params['custscript_sofileid_v2'] = Fileid;
    var startDate = new Date();
    params['startdate'] = startDate.toUTCString();
    
    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
    nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    
    ////If script is scheduled then successfuly then check for if status=queued
    if (status == 'QUEUED') {
        nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
    }
}//fun close
//-------------------------------------------------------//Function to getEstimate---------------------------------------
function getEstimate(TranId){
    if (_logValidation(TranId)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('tranid', null, 'is', TranId);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('estimate', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'estimate', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'estimate', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}




//-------------------------------------------------------//Function to UpdateSalesorder---------------------------------------

function UpdateSalesorder(records){
    if (_logValidation(records)) {
        var tranID = '';
        var itemId = '';
        var split = records.toString().split('#$');
        nlapiLogExecution('DEBUG', 'Search Results', 'split Length :' + split.length);
        
        //get tran id form csv file
        if (_logValidation(split)) {
        
            var splitArray = split[0].toString().split('|');
            
            tranID = splitArray[0];
        }
        
        //Search Sales Order Record Using the TranID
        //search Sales Order 
        if (_logValidation(tranID)) {
            var SOID = SearchSalesOrder(tranID);
            nlapiLogExecution('DEBUG', 'Sales order ', 'Sales order id :' + SOID);
            //update so 
            if (_logValidation(SOID)) {
            
                //load sales order
                var soobj = nlapiLoadRecord('salesorder', SOID);
                nlapiLogExecution('DEBUG', 'Sales order ', 'Sales order obj :' + soobj);
                for (var j = 0; j < split.length; j++) {
                    var splitArray = split[j].toString().split('|');
                    
                    if (j == 0) {
                    
                        if (_logValidation(splitArray[9])) {
                            soobj.setFieldValue('trandate', splitArray[9]);
                            nlapiLogExecution('DEBUG', 'updated sales order ', 'trandate :' + splitArray[9]);
                        }
                        
                        if (_logValidation(splitArray[10])) {
                            var deptId = getDeptID(splitArray[10]);
                            soobj.setFieldValue('department', deptId);
                            nlapiLogExecution('DEBUG', 'updated sales order ', 'department :' + splitArray[10]);
                        }
                        if (_logValidation(splitArray[11])) {
                            soobj.setFieldText('location', splitArray[11]);
                            nlapiLogExecution('DEBUG', 'updated sales order ', 'location :' + splitArray[10]);
                        }
                        
                        
                    }//J=0 end
                    if (_logValidation(splitArray[3])) {
                        itemId = getItemID(splitArray[3]);
                    }
                    //==========================================if action type is add======================================
                    //if action type is add 
                    
                    if (_logValidation1(splitArray[20]) == 'Add' || _logValidation1(splitArray[20]) == 'add' || _logValidation1(splitArray[20]) == 'ADD') {
                        nlapiLogExecution('DEBUG', 'add line item ', 'action in if information :' + splitArray[20]);
                        
                        
                        if (_logValidation(itemId)) {
                            soobj.selectNewLineItem('item');
                            soobj.setCurrentLineItemValue('item', 'item', itemId);
                            
                            if (_logValidation(splitArray[4])) {
                                soobj.setCurrentLineItemValue('item', 'quantity', splitArray[4]);
                            }
                            
                            if (_logValidation(splitArray[5])) {
                                soobj.setCurrentLineItemValue('item', 'rate', splitArray[5]);
                            }
                            if (_logValidation(splitArray[6])) {
                                soobj.setCurrentLineItemValue('item', 'revrecstartdate', splitArray[6]);
                            }
                            if (_logValidation(splitArray[14])) {
                                soobj.setCurrentLineItemValue('item', 'revrecschedule', splitArray[14]);
                            }
                            if (_logValidation(splitArray[15])) {
                                soobj.setCurrentLineItemValue('item', 'revrecenddate', splitArray[15]);
                            }
                            
                            if (_logValidation(splitArray[17])) {
                            
                                var TaxcodeId = getTaxcodeID(splitArray[17]);
                                
                                if (_logValidation(TaxcodeId)) {
                                    soobj.setCurrentLineItemValue('item', 'taxcode', TaxcodeId);
                                }
                            }
                            nlapiLogExecution('DEBUG', 'add line item ', 'class id :' + splitArray[18]);
                            if (_logValidation(splitArray[18])) {
                            
                                var ClasscodeId = getClasscodeID(splitArray[18]);
                                nlapiLogExecution('DEBUG', 'add line item ', 'ClasscodeId :' + ClasscodeId);
                                if (_logValidation(ClasscodeId)) {
                                    soobj.setCurrentLineItemValue('item', 'class', ClasscodeId);
                                }
                            }
                            
                            nlapiLogExecution('DEBUG', 'add line item ', 'cis information :' + splitArray[19]);
                            if (_logValidation(splitArray[19])) {
                                soobj.setCurrentLineItemValue('item', 'custcol_cis_information', splitArray[19]);
                            }
                            if (_logValidation(splitArray[21])) {
                                var myFld1 = soobj.getLineItemField('item', 'billingschedule', 1);
                                var options1 = myFld1.getSelectOptions();
                                var flag = 0;
                                for (var m = 0; m < options1.length; m++) {
                                
                                
                                    if (options1[m].getText() == splitArray[21]) {
                                        flag = 1;
                                        nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule name  :' + options1[m].getText() + '==' + splitArray[21]);
                                        var billscheduleid = options1[m].getId();
                                        nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule ID  :' + billscheduleid);
                                        soobj.setCurrentLineItemValue('item', 'billingschedule', billscheduleid);
                                    }
                                    
                                }
                                if (flag == 0) {
                                    var status = "" + "#" + " The Bill Schedule name mentioned in column[21] is not present in Net Suite";
                                    return status.toString();
                                }
                            }
                            
                            soobj.commitLineItem('item');
                        }
                    }//end if check action
                    //================================================//if action type is modify ===================================================
                    try {
                    
                    
                        if (_logValidation1(splitArray[20]) == 'Modify' || _logValidation1(splitArray[20]) == 'modify' || _logValidation1(splitArray[20]) == 'MODIFY') {
                        
                            nlapiLogExecution('DEBUG', 'modify line item ', 'action in if information :' + splitArray[20]);
                            var linecount = soobj.getLineItemCount('item');
                            nlapiLogExecution('DEBUG', 'Modify line item ', 'linecount :' + linecount);
                            for (var i = 1; i <= linecount; i++) {
                            
                                var lineitemid = soobj.getLineItemValue('item', 'item', i);
                                nlapiLogExecution('DEBUG', 'modify line item ', 'lineitemid i:' + i + "=" + lineitemid);
                                
                                if (lineitemid == itemId) {
                                    nlapiLogExecution('DEBUG', 'modify line item ', 'lineitemid == itemid');
                                    nlapiLogExecution('DEBUG', 'modify line item ', 'itemId i:' + i + "=" + itemId);
                                    if (_logValidation(itemId)) {
                                        soobj.setLineItemValue('item', 'item', i, itemId)
                                        
                                        if (_logValidation(splitArray[4])) {
                                            soobj.setLineItemValue('item', 'quantity', i, splitArray[4]);
                                        }
                                        
                                        if (_logValidation(splitArray[5])) {
                                            soobj.setLineItemValue('item', 'rate', i, splitArray[5]);
                                        }
                                        if (_logValidation(splitArray[6])) {
                                            soobj.setLineItemValue('item', 'revrecstartdate', i, splitArray[6]);
                                        }
                                        if (_logValidation(splitArray[14])) {
                                            soobj.setLineItemValue('item', 'revrecschedule', i, splitArray[14]);
                                        }
                                        if (_logValidation(splitArray[15])) {
                                            soobj.setLineItemValue('item', 'revrecenddate', i, splitArray[15]);
                                        }
                                        
                                        if (_logValidation(splitArray[17])) {
                                        
                                            var TaxcodeId = getTaxcodeID(splitArray[17]);
                                            
                                            if (_logValidation(TaxcodeId)) {
                                                soobj.setLineItemValue('item', 'taxcode', i, TaxcodeId);
                                            }
                                        }
                                        
                                        nlapiLogExecution('DEBUG', 'modify line item ', 'class id :' + splitArray[18]);
                                        if (_logValidation(splitArray[18])) {
                                        
                                            var ClasscodeId = getClasscodeID(splitArray[18]);
                                            nlapiLogExecution('DEBUG', 'modify line item', 'ClasscodeId :' + ClasscodeId);
                                            if (_logValidation(ClasscodeId)) {
                                                soobj.setLineItemValue('item', 'class', i, ClasscodeId);
                                            }
                                        }
                                        
                                        nlapiLogExecution('DEBUG', 'modify line item ', 'cis information :' + splitArray[19]);
                                        if (_logValidation(splitArray[19])) {
                                        
                                            var cisinfo = soobj.getLineItemValue('item', 'custcol_cis_information', i);
                                            nlapiLogExecution('DEBUG', 'modify line item ', 'cisinfo :' + cisinfo);
                                            if (cisinfo != null && cisinfo != '' && cisinfo != undefined) {
                                                cisinfo = cisinfo + "," + splitArray[19];
                                                soobj.setLineItemValue('item', 'custcol_cis_information', i, cisinfo);
                                                
                                                
                                            }
                                            else {
                                            
                                                soobj.setLineItemValue('item', 'custcol_cis_information', i, splitArray[19]);
                                            }
                                            
                                        }
                                        if (_logValidation(splitArray[21])) {
                                            var myFld1 = soobj.getLineItemField('item', 'billingschedule', 1);
                                            var options1 = myFld1.getSelectOptions();
                                            var flag = 0;
                                            for (var m = 0; m < options1.length; m++) {
                                            
                                            
                                                if (options1[m].getText() == splitArray[21]) {
                                                    flag = 1;
                                                    nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule name  :' + options1[m].getText() + '==' + splitArray[21]);
                                                    var billscheduleid = options1[m].getId();
                                                    nlapiLogExecution('DEBUG', 'new sales order ', 'Bill Schedule ID  :' + billscheduleid);
                                                    soobj.setLineItemValue('item', 'billingschedule', i, billscheduleid);
                                                }
                                                
                                            }
                                            if (flag == 0) {
                                                var status = "" + "#" + " The Bill Schedule name mentioned in column[21] is not present in Net Suite";
                                                return status.toString();
                                            }
                                        }
                                    }//end item if  
                                    //soobj.commitLineItem('item');
                                    nlapiLogExecution('DEBUG', 'modify line item ', 'commit line item');
                                }//end if 
                            }//end for
                        }//end if check action modify
                    } 
                    catch (e) {
                        nlapiLogExecution('DEBUG', 'modify line item ', 'error:' + e.getDetalis());
                    }
                    //================================================= //if action type is Delete ======================================================
                    //nlapiLogExecution('DEBUG', 'delete line item ', 'action in if information :' + splitArray[20]);
                    if (_logValidation1(splitArray[20]) == 'Delete' || _logValidation1(splitArray[20]) == 'delete' || _logValidation1(splitArray[20]) == 'DELETE') {
                        nlapiLogExecution('DEBUG', 'delete line item ', 'action in if information :' + splitArray[20]);
                        var linecount = soobj.getLineItemCount('item');
                        nlapiLogExecution('DEBUG', 'delete line item ', 'linecount :' + linecount);
                        for (var i = linecount; i > 0; i--) {
                        
                            var lineitemid = soobj.getLineItemValue('item', 'item', i);
                            nlapiLogExecution('DEBUG', 'delete line item ', 'lineitemid i:' + i + "=" + lineitemid);
                            if (lineitemid == itemId) {
                                nlapiLogExecution('DEBUG', 'modify line item ', 'lineitemid == itemid');
                                soobj.removeLineItem('item', i);
                            }//end if 
                        }//end for            
                    }//end if check action is delete
                    //============================== end function ========================================
                }//end for j
                var id = '';
                var status = "";
                
                try {
                    id = nlapiSubmitRecord(soobj);
                    nlapiLogExecution('DEBUG', 'updated sales order', 'updated sales order ID  :' + id);
                    
                    //update opportunity
                    
                    if (_logValidation(soobj.getFieldValue('opportunity'))) {
                        updateopportunity(soobj.getFieldValue('opportunity'), soobj.getFieldValue('createdfrom'));
                    }
                    
                    status = tranID + "#" + "" + "#" + "Update SO";
                    
                    return status.toString();
                } 
                catch (exception) {
                    nlapiLogExecution('DEBUG', 'updated sales order Catch Block :' + exception);
                    
                    status = SOID + "#" + exception.getDetails();
                    
                    return status.toString();
                }
                
                
            }//end Sales order
            else {
            
                var status = "" + "#" + "The Sales Order number mentioned in column[0] is not present in Netsuite";
                return status.toString();
            }
            
            
        }//train id blank 
        else {
            var status = "" + "#" + " The Sales Order number mentioned in column[0] is not present in CSV File";
            return status.toString();
            
        }
        
        
    }//End Record 
}//End Function
//-------------------------------------------------------//Function to get the Class code  ID---------------------------------------
//Function to get the Class code  ID
function getClasscodeID(classcodeName){
    if (_logValidation(classcodeName)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('name', null, 'is', classcodeName);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('classification', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'class Code', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'class code', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

//-------------------------------------------------------//Function to get the opprchunities status---------------------------------------
//Function to get the opprchunities status
function getOopStatus(OppID){
    if (_logValidation(OppID)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('internalid', null, 'is', OppID);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('status');
        
        var results = nlapiSearchRecord('opportunity', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'Opp Status', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'Opp Status', 'Results[0] :' + results[0].getValue('status'));
            return results[0].getValue('status');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

//-------------------------------------------------------//Function to get the opprchunities Train Id---------------------------------------
//Function to get the opprchunities Id
function getOopTranid(OppID){
    if (_logValidation(OppID)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('internalid', null, 'is', OppID);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('tranid');
        
        var results = nlapiSearchRecord('opportunity', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'Opp Tran id', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'Opp Tran id', 'Results[0] :' + results[0].getValue('tranid'));
            return results[0].getValue('tranid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

//-------------------------------------------------------//Function to project---------------------------------------
function getProject(TranId){
    if (_logValidation(TranId)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('entityid', null, 'is', TranId);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var results = nlapiSearchRecord('job', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'project', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'project', 'Results[0] :' + results[0].getValue('internalid'));
            return results[0].getValue('internalid');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

//-------------------------------------------------------//Function to get the project customer name---------------------------------------
//Function to get the  project customer name
function getProjectCustomer(projID){
    if (_logValidation(projID)) {
        nlapiLogExecution('DEBUG', 'customer name', 'i am in search customer');
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('internalid', null, 'is', projID);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('parent');
        
        var results = nlapiSearchRecord('job', null, filter, columns);
        
        if (_logValidation(results)) {
            nlapiLogExecution('DEBUG', 'customer name', 'Results Length :' + results.length);
            nlapiLogExecution('DEBUG', 'customer name', 'Results[0] :' + results[0].getValue('parent'));
            return results[0].getValue('parent');
        }
        else {
            return "";
        }
        
    }
    else {
        return "";
    }
}

