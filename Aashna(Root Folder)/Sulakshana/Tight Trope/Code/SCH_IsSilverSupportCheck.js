// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
		Script Name:	SCH_IsSilverSupportCheck.js
		Author: 		Sulakshana Ghadage
		Company:		
		Created Date:	22-May-2013
		Description:	To make a silver support check
		Script Modification Log:
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
		
		 // BEGIN SCHEDULED FUNCTION =============================================
	
	
	   SCHEDULED FUNCTION
		- function SCHPOcreation(type)
		 */
	
}

function isSilverSupportChk()
{
	var filters = new Array();
	var columns = new Array();
	columns[0]= new nlobjSearchColumn('internalid');
	var searchresults = nlapiSearchRecord('customrecord_warrantycard',167, filters, columns);
	nlapiLogExecution('DEBUG', 'Invoice Fields', 'searchresults=' + searchresults);
	if (searchresults != null) 
	{
		for (var i = 0; i < searchresults.length; i++) 
		{
			var WRCardId = searchresults[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WRCardId=' + WRCardId);
			
			var WRRec = nlapiLoadRecord('customrecord_warrantycard', WRCardId);
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WRRec=' + WRRec);
			
			var WRSerialNo = WRRec.getFieldValue('custrecord_serialnumber');
			nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WRSerialNo=' + WRSerialNo);
			
			var seachSerialNo = SearchCustomRecord(WRSerialNo);
			nlapiLogExecution('DEBUG', 'Invoice Line Item', 'seachSerialNo=' + seachSerialNo);
			if (seachSerialNo != null && seachSerialNo != '') 
			{
			
			var WCSupportType = WRRec.getFieldValue('custrecord_supporttype');
			nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WCSupportType=' +WCSupportType);
			
			var WRStartDate = WRRec.getFieldValue('custrecord_warrantystartdate');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WRStartDate=' + WRStartDate);
			
			var WREndDate = WRRec.getFieldValue('custrecord_warrantyenddate');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WREndDate=' + WREndDate);
			if(WCSupportType==2)
			{
				WRRec.setFieldValue('custrecord_supporttype','2');
			}
			else If(WCSupportType==3)
			{
				WRRec.setFieldValue('custrecord_supporttype','3');
			}
			WRRec.setFieldValue('custrecord_issliversupport','F');
			var submitSilver = nlapiSubmitRecord(WRRec,false,false);
			nlapiLogExecution('DEBUG', 'Invoice Line Item', 'submitSilver=' +submitSilver);	
					
				}
				
			}
		}
	}


function SearchCustomRecord(WRSerialNo)
{
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Search serialNo=' + WRSerialNo);
	var Filters = new Array();
	Filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is',WRSerialNo );
	var Column= new Array();
	Column[0]= new nlobjSearchColumn('internalid');
	
	var searchResult =nlapiSearchRecord('customrecord_warrantycard',null,Filters,Column);
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult);
	if(searchResult!=null)
	{
		var Serial=searchResult[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'In SearchUnit', 'Serial : ' +Serial);
		
		return Serial;
		
	}
	return null;
	
}
