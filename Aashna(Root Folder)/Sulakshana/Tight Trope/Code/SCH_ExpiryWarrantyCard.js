// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
		Script Name:	SCH_ExpiryWarrantyCard.js
		Author: 		Sulakshana Ghadage
		Company:		
		Created Date:	21-May-2013
		Description:	Create Warranty record for item from invoice line item
		Script Modification Log:
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
		
		 // BEGIN SCHEDULED FUNCTION =============================================
	
	
	   SCHEDULED FUNCTION
		- function SCHPOcreation(type)
		 */
	
}

function ExpiryWarrantyRecord()
{
	var filters = new Array();
	var columns = new Array();
	columns[0]= new nlobjSearchColumn('internalid');
	var searchresults = nlapiSearchRecord('customrecord_warrantycard',164, filters, columns);
	nlapiLogExecution('DEBUG', 'Invoice Fields', 'searchresults=' +searchresults);
	if(searchresults != null)
	{
		for (var i = 0; i < searchresults.length; i++) 
		{
			var WRCardId = searchresults[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WRCardId=' + WRCardId);
			
			var WRRec = nlapiLoadRecord('customrecord_warrantycard', WRCardId);
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'WRRec=' + WRRec);
			
			var OutOfWarranty =WRRec.getFieldValue('custrecord_outofwarranty');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'OutOfWarranty=' + OutOfWarranty);
			
			WRRec.setFieldValue('custrecord_outofwarranty','T');
			
			var IsSilverCheck =WRRec.getFieldValue('custrecord_issliversupport');
			nlapiLogExecution('DEBUG', 'Invoice Fields', 'OutOfWarranty=' + OutOfWarranty);
			
			WRRec.setFieldValue('custrecord_issliversupport','F');
			
			var submitWCExpRecord = nlapiSubmitRecord(WRRec,false,false)
			nlapiLogExecution('DEBUG', 'Invoice Line Item', 'submitWCExpRecord=' + submitWCExpRecord);
		}
		
	}
	
	
	
}
/*
function SearchCustomRecord(WRSerialNo)
{
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Search serialNo=' + WRSerialNo);
	var Filters = new Array();
	Filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is',WRSerialNo );
	var Column= new Array();
	Column[0]= new nlobjSearchColumn('internalid');
	
	var searchResult =nlapiSearchRecord('customrecord_warrantycard',null,Filters,Column);
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult);
	if(searchResult!=null)
	{
		var Serial=searchResult[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'In SearchUnit', 'Serial : ' +Serial);
		
		return Serial;
		
	}
	return null;
	
}

*/