// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
		Script Name:	SCH_WarrantyCardCreation.js
		Author: 		Sulakshana Ghadage
		Company:		
		Created Date:	14-May-2013
		Description:	Create Warranty record for item from invoice line item
		Script Modification Log:
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
		
		 // BEGIN SCHEDULED FUNCTION =============================================
	
	
	   SCHEDULED FUNCTION
		- function SCHPOcreation(type)
		 */
	
}

function CreateWarrantyCard()
{
	//get Values from Save search
	var filters = new Array();
	var columns = new Array();
   var searchresults = nlapiSearchRecord('invoice', 159, filters, columns);
	nlapiLogExecution('DEBUG', 'Invoice Fields', 'searchresults=' +searchresults);
	
	if (searchresults != null) //customsearch_warrantycardforinvoices  
	{
		for (var i = 0; i < searchresults.length; i++) 
		{
			var invoiceId = searchresults[i].getValue('internalid');
			//nlapiLogExecution('DEBUG', 'Invoice Fields', 'invoiceId=' + invoiceId);
			
			var InvoiceObj = nlapiLoadRecord('invoice', invoiceId);
			//nlapiLogExecution('DEBUG', 'Invoice Fields', 'InvoiceObj=' + InvoiceObj);
			
			var warrantycardnumberCk = InvoiceObj.getFieldValue('custbody_warrantycardnumber');
			//nlapiLogExecution('DEBUG', 'Invoice Fields', 'warrantycardnumberCk=' + warrantycardnumberCk);
			
			//if (warrantycardnumberCk == null || warrantycardnumberCk == '' || warrantycardnumberCk == 'undefined'||warrantycardnumberCk=='F') 
			{
				var InvLineItemCount = InvoiceObj.getLineItemCount('item');
				//nlapiLogExecution('DEBUG', 'Invoice Fields', 'InvLineItemCount=' + InvLineItemCount);
				
				var invoiceDate = InvoiceObj.getFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'Invoice Fields', 'invoiceDate=' + invoiceDate);
				
				var date = nlapiStringToDate(invoiceDate);
				//nlapiLogExecution('DEBUG', 'Schedule Script', 'trandate==' + invoiceDate);
				
				var Invday = date.getDate();//Day
				//nlapiLogExecution('DEBUG', 'Schedule Script', 'tranday==' + Invday);
				var Invmonth = date.getMonth() + 1;//Month
				//nlapiLogExecution('DEBUG', 'Schedule Script', 'tranmonth==' + Invmonth);
				var Invyear = date.getFullYear(); //Year
				//nlapiLogExecution('DEBUG', 'Schedule Script', 'tranyear==' + Invyear);
				
				var customer = InvoiceObj.getFieldValue('entity');
				//nlapiLogExecution('DEBUG', 'Invoice Fields', 'customer=' + customer);
				
				var endUser = InvoiceObj.getFieldValue('custbody9');
				//nlapiLogExecution('DEBUG', 'Invoice Fields', 'endUser=' + endUser);
				for (var j = 1; j <= InvLineItemCount; j++) 
				{ 
				try 
				{
					var item = InvoiceObj.getLineItemValue('item', 'item', j);
					//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'item=' + item);
					
					var itemType = InvoiceObj.getLineItemValue('item', 'itemtype', j);
					//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'itemType=' + itemType);
					
					if (itemType == 'Assembly') 
					{
						nlapiLogExecution('DEBUG', 'Invoice Line Item', 'I m in Assembly itemType=' + itemType);
						var itemText = InvoiceObj.getLineItemText('item', 'item', j);
						//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'itemText=' + itemText);
						
						var ItemQty = InvoiceObj.getLineItemValue('item', 'quantity', j);
						//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'ItemQty=' + ItemQty);
						
						InvoiceObj.selectLineItem('item', j);
						var invNumberSubRec = InvoiceObj.viewLineItemSubrecord('item', 'inventorydetail', j)
						//18062013
						if (invNumberSubRec != null && invNumberSubRec != '' && invNumberSubRec != 'undefined') {
							var linecount = invNumberSubRec.getLineItemCount('inventoryassignment');
							//nlapiLogExecution('DEBUG', 'ON Invetory Detail', 'Inventory Detail linecount :' + linecount);
							for (var k = 1; k <= linecount; k++) {
								invNumberSubRec.selectLineItem('inventoryassignment', k);
								var serialId = invNumberSubRec.getCurrentLineItemValue('inventoryassignment', 'issueinventorynumber');
								//nlapiLogExecution('DEBUG', 'ON Invetory Detail', 'serialId :' + serialId);
								
								var SerialNumber = nlapiLookupField('inventorynumber', serialId, 'inventorynumber')
								//nlapiLogExecution('DEBUG', 'ON Invetory Detail', 'SerialNumber :' + SerialNumber);
								//var supportAppiedTo = InvoiceObj.getLineItemValue('item', 'custcol_supportappliedto', j);
								//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'supportAppiedTo=' + supportAppiedTo);
								
								//var supportAppiedTotx = InvoiceObj.getLineItemText('item', 'custcol_supportappliedto', j);
								//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'supportAppiedTotx=' + supportAppiedTotx);
								
								
								//Create Warranty Record
								
								var CreateWarrantyObj = nlapiCreateRecord('customrecord_warrantycard');
								nlapiLogExecution('DEBUG', 'Invoice Line Item', 'CreateWarrantyObj=' + CreateWarrantyObj);
								endUser = valueCheck(endUser)
								CreateWarrantyObj.setFieldValue('custrecord_enduser', endUser);
								customer = valueCheck(customer)
								CreateWarrantyObj.setFieldValue('custrecord_distributor', customer);
								item = valueCheck(item)
								CreateWarrantyObj.setFieldValue('custrecord_supportappliedto', item);
								SerialNumber = valueCheck(SerialNumber)
								CreateWarrantyObj.setFieldValue('custrecord_serialnumber', SerialNumber);
								invoiceDate = valueCheck(invoiceDate)
								CreateWarrantyObj.setFieldValue('custrecord_warrantystartdate', invoiceDate);
								var silverSupport = /Silver/g;
								var goldSuppot = /Gold/g;
								if (goldSuppot.test(itemText)) 
								{
									//WarrantyYr = parseInt(Invyear) + parseInt(ItemQty) + 1;
									WarrantyYr = parseInt(Invyear) + 1;
									nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr=' + WarrantyYr);
									var warrantyEndDate = Invmonth + '/' + Invday + '/' + WarrantyYr;
									//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr in gold=' + warrantyEndDate);
									CreateWarrantyObj.setFieldValue('custrecord_supporttype', '1');
									CreateWarrantyObj.setFieldValue('custrecord_warrantyenddate', warrantyEndDate.toString());
								}
								else 
									if (silverSupport.test(itemText)) 
									{
										//WarrantyYr = parseInt(Invyear) + parseInt(ItemQty) + 1;
										WarrantyYr = parseInt(Invyear) + 1;
										var warrantyEndDate = Invmonth + '/' + Invday + '/' + WarrantyYr;
										//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr in Silve=' + WarrantyYr);
										CreateWarrantyObj.setFieldValue('custrecord_supporttype', '2');
										CreateWarrantyObj.setFieldValue('custrecord_warrantyenddate', warrantyEndDate.toString());
									}
									else 
										if (!(goldSuppot.test(itemText)) && !(silverSupport.test(itemText))) 
										{
											WarrantyYr = parseInt(Invyear) + 1;
											var warrantyEndDate = Invmonth + '/' + Invday + '/' + WarrantyYr;
											CreateWarrantyObj.setFieldValue('custrecord_supporttype', '3');
											CreateWarrantyObj.setFieldValue('custrecord_warrantyenddate', warrantyEndDate.toString());
										}
								var invchkbx = InvoiceObj.getFieldValue('custbody_warrantycardnumber');
								//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'invchkbx=' + invchkbx);
								//17062013
								CreateWarrantyObj.setFieldValue('custrecord1', invoiceId);
								
								var warrentySubmit = nlapiSubmitRecord(CreateWarrantyObj, false, false);//submit warranty record
								//nlapiLogExecution('DEBUG', 'Invoice Line Item', 'warrentySubmit=' + warrentySubmit);
							}//End of For
						}
						
					}
					else 
						if (itemType != 'Assembly') 
						{
							//var serialNo = new Array();
							nlapiLogExecution('DEBUG', 'Invoice Line Item', 'I m in Non Assembly itemType=' + itemType);
							var serialNo = InvoiceObj.getLineItemValue('item', 'custcol1', j);
							nlapiLogExecution('DEBUG', 'Invoice Line Item', 'serialNo=' + serialNo);
							var splitSRNo = new Array();
							
							if(serialNo !=null && serialNo !='')
								{
								splitSRNo = serialNo.split(',');
								nlapiLogExecution('DEBUG', 'Invoice Line Item', 'splitSRNo=' + splitSRNo)
								}
							
							for (n = 0; n < splitSRNo.length; n++) 
							{
								nlapiLogExecution('DEBUG', 'Invoice Line Item', 'serialNo.length=' + splitSRNo.length);
								if (splitSRNo != null && splitSRNo != '') 
								{
									var seachSerialNo = SearchCustomRecord(splitSRNo[n]);
									nlapiLogExecution('DEBUG', 'Invoice Line Item', 'seachSerialNo[n]=' + seachSerialNo);
									if (seachSerialNo != null && seachSerialNo != '' && seachSerialNo != 'undefined') 
									{
										var serialNumber = seachSerialNo.toString().split('#')
										
										var SNumber = serialNumber[0]
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'SNumber[0]=====' + SNumber);
										
										var SInternalId = serialNumber[1]
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'SInternalId[1]====' + SInternalId);
										var itemText = InvoiceObj.getLineItemText('item', 'item', j);
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'itemText=' + itemText);
										
										var ItemQty = InvoiceObj.getLineItemValue('item', 'quantity', j);
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'ItemQty=' + ItemQty);
										
										var units = InvoiceObj.getLineItemValue('item', 'units', j);
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'units=' + units);
									}
									//}
									
									if (SInternalId != null && SInternalId != '') 
									{
										//load existing record with there serial number
										var existingWCObj = nlapiLoadRecord('customrecord_warrantycard', SInternalId);
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'existingWCObj Load By sr no=' + existingWCObj);
										
										var WCExpStartdate = existingWCObj.getFieldValue('custrecord_warrantystartdate');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Old WCExpStartdate=' + WCExpStartdate);
										
										var WCExpEndDate = existingWCObj.getFieldValue('custrecord_warrantyenddate');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Old WCExpEndDate=' + WCExpEndDate);
										
										var OutofWarrenty = existingWCObj.getFieldValue('custrecord_outofwarranty');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'OutofWarrenty=' + OutofWarrenty);
										
										var isSilverG = existingWCObj.getFieldValue('custrecord_issliversupport');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Old isSilverG=' + isSilverG);
										
										var PreEnddate = nlapiStringToDate(WCExpEndDate);
										nlapiLogExecution('DEBUG', 'Schedule Script', 'trandate==' + invoiceDate);
										
										var PreInvday = PreEnddate.getDate();//Day
										nlapiLogExecution('DEBUG', 'Schedule Script', 'PreInvday==' + PreInvday);
										var PreInvmonth = PreEnddate.getMonth() + 1;//Month
										nlapiLogExecution('DEBUG', 'Schedule Script', 'PreInvmonth==' + PreInvmonth);
										var PreInvyear = PreEnddate.getFullYear(); //Year
										nlapiLogExecution('DEBUG', 'Schedule Script', 'PreInvyear==' + PreInvyear);
										
										var SupportAppliesTo = existingWCObj.getFieldValue('custrecord_supportappliedto');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Old SupportAppliesTo=' + SupportAppliesTo);
										
										var SupportType = existingWCObj.getFieldValue('custrecord_supporttype');
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Old SupportType=' + SupportType);
										
										nlapiLogExecution('DEBUG', 'Invoice Line Item', 'goldSuppot.test(itemText)=='+goldSuppot.test(itemText));
										var silverSupport = /Silver/g;
										var goldSuppot = /Gold/g;
										if (goldSuppot.test(itemText)) 
										{
											nlapiLogExecution('DEBUG', 'Invoice Line Item', 'in gold Warranty=');
											WarrantyYr = parseInt(PreInvyear) + parseInt(units);
											nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr=' + WarrantyYr);
											var warrantyEndDate = Invmonth + '/' + Invday + '/' + WarrantyYr;
											nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr in gold=' + warrantyEndDate);
											existingWCObj.setFieldValue('custrecord_supporttype', '1');
											var OutOfwarrantyforgold = existingWCObj.getFieldValue('custrecord_outofwarranty');
											nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Out Of warranty for gold=' + OutOfwarrantyforgold);
											if (isSilverG == 'T') 
											{
												existingWCObj.setFieldValue('custrecord_issliversupport', 'F');
											}
											
											if (OutOfwarrantyforgold != null && OutOfwarrantyforgold != '' && OutOfwarrantyforgold != 'undefind') {
												existingWCObj.setFieldValue('custrecord_supporttype', '1');
											}
											existingWCObj.setFieldValue('custrecord_warrantyenddate', warrantyEndDate.toString());
										}
										else 
											if (silverSupport.test(itemText)) 
											{
												WarrantyYr = parseInt(PreInvyear) + parseInt(units);
												var warrantyEndDate = Invmonth + '/' + Invday + '/' + WarrantyYr;
												nlapiLogExecution('DEBUG', 'Invoice Line Item', 'WarrantyYr in Silve=' + WarrantyYr);
												//existingWCObj.setFieldValue('custrecord_supporttype', '2');
												existingWCObj.setFieldValue('custrecord_warrantyenddate', warrantyEndDate.toString());
												var IsSilver = existingWCObj.setFieldValue('custrecord_issliversupport', 'T');
												nlapiLogExecution('DEBUG', 'Invoice Line Item', 'IsSilver=' + IsSilver);
												if (SupportType == '3') 
												{
													nlapiLogExecution('DEBUG', 'Invoice Line Item', 'It is silver support but item is in manufacture Support');
												}
											}
											
										
									}// End of IF
								}//if for serial no.
								SupportAppliesTo = valueCheck(SupportAppliesTo)
							var supportAppToSet = existingWCObj.setFieldValue('custrecord_supportappliedto', SupportAppliesTo);
							nlapiLogExecution('DEBUG', 'Invoice Line Item', 'supportAppToSet=' + supportAppToSet);
							var existingWCObjSubmit = nlapiSubmitRecord(existingWCObj, false, false);//submit warranty record
							nlapiLogExecution('DEBUG', 'Invoice Line Item', 'existingWCObjSubmit=' + existingWCObjSubmit);
							}// End of For
							
						}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Cought=' + e.getDetails());
				}
				
							
				}//end of Main For
				InvoiceObj.setFieldValue('custbody_warrantycardnumber', 'T');
				nlapiSubmitRecord(InvoiceObj, false, false);
				
			}
		}
	}
}

function SearchCustomRecord(splitSRNo)
{
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'Search serialNo=' + splitSRNo);
	var Filters = new Array();
	Filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is',splitSRNo );
	var Column= new Array();
	Column[0]= new nlobjSearchColumn('custrecord_serialnumber');
	Column[1]= new nlobjSearchColumn('internalid');
	
	var searchResult =nlapiSearchRecord('customrecord_warrantycard',null,Filters,Column);
	nlapiLogExecution('DEBUG', 'Invoice Line Item', 'searchResult=' + searchResult);
	if(searchResult!=null)
	{
		var Serial=searchResult[0].getValue('custrecord_serialnumber')
		nlapiLogExecution('DEBUG', 'In SearchUnit', 'Serial : ' +Serial);
		var SerialInternalid=searchResult[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'In SearchUnit', 'Serial : ' +Serial);
		
		return Serial+"#"+SerialInternalid;
		
	}
	return null;
	
}

function valueCheck(value)
{
	//////nlapiLogExecution('DEBUG', 'value data', 'value= '+value);
	if(isNaN(value) || value=='' || value=='undefined' || value==null || value.toString()=='NaN')
	//if(value==Infinity||value==-Infinity||isNaN(value))
    {
        //////nlapiLogExecution('DEBUG', 'value data', 'return= '+0);
		return '';
    }
	else
	{
		//////nlapiLogExecution('DEBUG', 'value data', 'return value= '+value);
		return value
	}
}
