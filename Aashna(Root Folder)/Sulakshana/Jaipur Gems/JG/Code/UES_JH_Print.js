// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Button on Invoice record
	Author:Sulakshana Ghadage	
	Company:Jaipur Gems
	Date:10-10-2012
	Description:JH_Print added on Job History Form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function JHPrintAfterSubmitRecord(type,form)
{
    if (type == 'view') 
	{
		
		
			var JHid = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'JHid', 'JH id' + JHid);
			
			
			
			
			/*
var custid = nlapiGetFieldValue('customer');
			nlapiLogExecution('DEBUG', 'custid', 'customer id' + custid);
			
	           
            var depository = nlapiGetFieldValue('customerdeposit');
			nlapiLogExecution('DEBUG', 'depository', 'depository' +depository);
*/
			form.setScript('customscript_clijhprint');
			
			form.addButton('custpage_jhprint', 'JH Print','callPrintJHSuitelets(\'' + JHid + '#' + 'custpage_jhprint'+ '\');');
		
		return true;
	}

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function InvoiceBeforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function InvoiceAfterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
