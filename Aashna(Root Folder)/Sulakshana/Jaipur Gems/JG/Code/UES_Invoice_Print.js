// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Button on Invoice record
	Author:Sulakshana Ghadage	
	Company:JG
	Date:24-9-2012
	Description:Invoice Print 1 and Quotation Print 2 added on Quotation Record


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function InvoicePrint(type,form)
{
	if (type == 'view') 
	{
		var SOid = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'SOid', 'SO id' + SOid);
		
			/*
var invoiceid = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'invoiceid', 'invoiceid' + invoiceid);
*/
			
			
					/*
var custid = nlapiGetFieldValue('customer');
					nlapiLogExecution('DEBUG', 'custid', 'customer id' + custid);
*/
					
					/*
var soid = nlapiGetFieldValue('salesorder');
					nlapiLogExecution('DEBUG', 'soid', 'soid' + soid);
*/
					if (SOid != null) 
					{
						var soObj = nlapiLoadRecord('salesorder', SOid);
						var cutomForm = soObj.getFieldValue('customform')
						nlapiLogExecution('DEBUG', 'cutom Form', 'cutomForm == ' + cutomForm);
						
						if (cutomForm == '138') 
						{
							/*
var custid = nlapiGetFieldValue('customer');
							nlapiLogExecution('DEBUG', 'custid', 'customer id' + custid);
*/
							
							//nlapiLogExecution('DEBUG', 'SalesorderInternalId', 'Salesorder InternalId' +SalesorderInternalId);
							
							//Sandbox Setup
							//form.setScript('customscript_cliinvoiceprint');
							//Live Setup
							form.setScript('customscript_clipurchasedc');
								  
							
							form.addButton('custpage_invoceprint', 'Purchase DC', 'callPrintInvoiceSuitelets1(\'' + SOid + '\');'); //'#' + 'custpage_invoceprint'+ 
							return true;
						}
					}
			
	}

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function InvoiceBeforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function InvoiceAfterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
