// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Suitlet for Quotation print
	Author:Sulakshana Ghadage	
	Company:Jaipur Gems
	Date:10-10-2012
	Description:Design of HTML is Added in Suitlet and All entries of form getting  
	from So,which is get into the Suitlet.



	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	19-11-2012			Sulakshana Ghadage				Jitesh/Ganesh						Added Teams and conditions line on print
		


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- printSO(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

function printSO(request,response)
{
		var internalId;
		//var totalGrossAmt= 0.0;
		
		//Sandbox//var SOInternalId = request.getParameter('custscript2');
		//Live Setup//
		var SOInternalId = request.getParameter('custscript_soidvalue');
		nlapiLogExecution('DEBUG', 'printSO', " Print Record ID :" + SOInternalId);
		
		var recObj = nlapiLoadRecord('salesorder', SOInternalId);
		nlapiLogExecution('DEBUG', 'printInvoice', " recObj :" + recObj);
      
       
	//--------------------------------------Order No-------------------------------------------//
		 var OrderNo = recObj.getFieldValue('tranid');
		 if(OrderNo!=null && OrderNo!=''&& OrderNo!='undefined')
		 {
		 OrderNo = OrderNo;
		 }
		 else
		 {
		 OrderNo = '';
		 }
		 nlapiLogExecution('DEBUG', 'printSO', "Sales Order No:" +OrderNo);
	//--------------------------------------Order No-------------------------------------------//
	
	//-------------------------------------SO date----------------------------------------------//
		 var SODate = recObj.getFieldValue('trandate');
		 if(SODate!=null && SODate!=''&& SODate!='undefined')
		 {
		 SODate = SODate;
		 }
		 else
		 {
		 SODate = '';
		 }
		 nlapiLogExecution('DEBUG', 'printSO', "Sales Order Date:" +SODate);
	//-------------------------------------SO date----------------------------------------------//
		var paymentRec=recObj.getFieldValue('custbody_totalpaymentso');
		nlapiLogExecution('DEBUG', 'printSO', "First cust name :" +paymentRec);
		if (paymentRec != null && paymentRec != '' && paymentRec != 'undefined') 
		{
			paymentRec =paymentRec ;
		}
		else 
		{
			paymentRec = '';
		}
		nlapiLogExecution('DEBUG', 'printSO', " payment Rec :" +paymentRec);
	//--------------------------------------------------------------------------------------------//
		var paymentDue=recObj.getFieldValue('custbody_totalpaymentdueso');
		nlapiLogExecution('DEBUG', 'printSO', "paymentDue:" +paymentDue);
		if (paymentDue != null && paymentDue != '' && paymentDue != 'undefined') 
		{
			paymentDue =paymentDue ;
		}
		else 
		{
			paymentDue = '';
		}
		nlapiLogExecution('DEBUG', 'printSO', " paymentDue :" +paymentDue);
	//----------------------------------Customer Details----------------------------------------//
		var custId=recObj.getFieldValue('entity');
		nlapiLogExecution('DEBUG', 'SOprint', " Customer ID :" +custId);
		if(custId!='' && custId!=null)
		{
		var custDetail=nlapiLoadRecord('customer',custId);
		nlapiLogExecution('DEBUG', 'printSO', " custDetail :" +custDetail);
		
		var custname=custDetail.getFieldValue('entityid');//Customer Name
		nlapiLogExecution('DEBUG', 'printSO', "First cust name :" +custname);
		if (custname != null && custname != '' && custname != 'undefined') 
		{
			custname =formatAndReplaceMessageForAnd(nlapiEscapeXML(custname)) ;
		}
		else 
		{
			custname = '';
		}
		nlapiLogExecution('DEBUG', 'printSO', " After cust name :" +custname);
		
		var custBal=custDetail.getFieldValue('depositbalance');//Customer Balance
		if (custBal != null && custBal != '' && custBal != 'undefined') 
		{
			custBal = custBal;
		}
		else 
		{
			custBal = 0;
		}
		nlapiLogExecution('DEBUG', 'printSO', " Cust Balance:" +custBal);
		
		var landlinePhn=custDetail.getFieldValue('phone');//customer Phone No
		if (landlinePhn != null && landlinePhn != '' && landlinePhn != 'undefined') 
		{
			landlinePhn = landlinePhn;
		}
		else 
		{
			landlinePhn = '';
		}
		nlapiLogExecution('DEBUG', 'printSO', " landline Phn :" +landlinePhn);
		
		var address=custDetail.getFieldValue('defaultaddress');//Customer Address
		nlapiLogExecution('DEBUG', 'printSO', " Address:" +address);
		if (address != null && address != '' && address != 'undefined') 
		{
			address = nlapiEscapeXML(address);
		}
		else 
		{
			address = '';
		}
		nlapiLogExecution('DEBUG', 'printSO', " Address:" +address);
			
		}
		
		
	//----------------------------------Customer Details----------------------------------------//
	 
address=address+"<br\/> "+' '
    address = address.toString();
	nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address.toString()= " + address);
	address = address.replace(/\n/g,"<br\/>");/// /g
	nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  customer_address After Replcing spaces with % =====" + address);

		
	//----------------------------------Customer Depository------------------------------------//
	var message="";
	message += "<table border=\"0.1\" width=\"100%\" style=\"border-collapse: collapse\">";
	message += "<tr>";
	message += "<td width=\"100%\" colspan=\"2\" align=\"right\" style=\"font-size: 22pt;\" ><b>Estimate<\/b>";
	message += "<\/td>";
	message += "<\/tr>";
	message += "	<tr>";
	message += "		<td width=\"50%\" border=\"0.1\" style=\"font-size: 11pt;border-bottom-style:none;border-left-style:none;\"  ><b>Customer Address<\/b><\/td>";
	message += "		<td border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\"><b>Order<\/b> #&nbsp; "+OrderNo+"<\/td>";
	message += "	<\/tr>";
	message += "	<tr>";
	message += "		<td width=\"50%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;border-left-style:none;font-size: 9pt;\" ><\/td>";
	message += "		<td border=\"0.1\" style=\"border-left-style:none;font-size: 9pt; \" ><b>Date<\/b>&nbsp;"+SODate+"<\/td>";
	message += "	<\/tr>";

	
	message += "	<tr>";
	message += "		<td width=\"50%\" align=\"left\" border=\"0.1\" style=\"font-size: 9pt;border-left-style:none;border-right-style:none;border-top-style:none;border-bottom-style:none;\" ><p> "+address+"</p> <b>Tel No/Mobile No <\/b>&nbsp;&nbsp;"+landlinePhn+" <\/td>";
	message += "	</tr>";

	/*
message += "	<tr>";
	message += "		<td width=\"50%\" align=\"left\" border=\"0.1\" style=\"font-size: 9pt;border-left-style:none;border-right-style:none;border-top-style:none;border-bottom-style:none;\" >"+address+"<\/td>";
	message += "	<\/tr>";
*/
	/*
message += "	<tr>";
	message += "		<td width=\"50%\" align=\"left\" border=\"0.1\" style=\"font-size: 9pt;border-left-style:none;border-right-style:none;border-top-style:none;border-bottom-style:none;\" ><b>Tel No/Mobile No <\/b>&nbsp;&nbsp;"+landlinePhn+"<\/td>";
	message += "	<\/tr>";
*/
/*	message += "<td rowspan=\"2\" border=\"0.1\"><b>Customer Deposit Details<\/b><table width=\"100%\" >";
	message += "			<tr>";
	message += "				<td width=\"30%\" border=\"0.1\" ><b>No.<\/b><\/td>";
	message += "				<td width=\"40%\" border=\"0.1\" ><b>Amount<\/b><\/td>";
	message += "				<td width=\"30%\" border=\"0.1\" style=\"border-left-style:none;\" ><b>Date<\/b><\/td>";
	message += "			<\/tr>";
	  var SOid = recObj.getFieldValue('tranid');
	  nlapiLogExecution('DEBUG', 'printSO', " custdepository:" +SOid);
	  
	  if(SOid!=null)
	  {
  		var Filters = new Array();
		var Column = new Array();
		
		Filters[0] = new nlobjSearchFilter('salesorder', null, 'is', SOInternalId)
		
		Column[0] = new nlobjSearchColumn('tranid');
		
		Column[1] = new nlobjSearchColumn('trandate');
		Column[2] = new nlobjSearchColumn('internalid');
		Column[3] = new nlobjSearchColumn('amount');
		var searchResult=nlapiSearchRecord('customerdeposit',null,Filters,Column);
		
		nlapiLogExecution('DEBUG', 'printSO', " searchResult:" +searchResult);
		var totalpayment=0;
		if(searchResult!=null)
		{
			//var totalpayment=0;
			nlapiLogExecution('DEBUG', 'printSO', " searchResult length : " +searchResult.length);
			
			for(var i=0;i<searchResult.length;i++)
			
			{
				nlapiLogExecution('DEBUG', 'printSO', " i counter : " +i);
	            var internalId=searchResult[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'printSO', " internalId : " +internalId);
				
				var depObj=nlapiLoadRecord('customerdeposit',internalId);
				nlapiLogExecution('DEBUG', 'printSO', " depObj : " +depObj);//formatAndReplaceSpacesofMessage(nlapiEscapeXML
				
				var payment=depObj.getFieldValue('payment');
				if (payment != null && payment != '' &&  payment != 'undefined') 
				{
					payment = parseFloat(payment);
				}
				else 
				{
					payment = 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " payment : " +payment);
				
				totalpayment=parseFloat(totalpayment)+parseFloat(payment);
				nlapiLogExecution('DEBUG', 'printSO', " Before Total Payment : " +totalpayment);
				if (totalpayment!= null && totalpayment!= '' && totalpayment!= 'undefined')
	            {
					totalpayment= totalpayment;
				}
				else 
				{
					totalpayment= 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " Total payment : " +totalpayment);
				
				var depositNo=depObj.getFieldValue('tranid');
				if (depositNo != null && depositNo != '' && depositNo != 'undefined') 
				{
					depositNo = depositNo;
				}
				else 
				{
					depositNo = 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " Deposit No : " +depositNo);
				
				var depositDate=depObj.getFieldValue('trandate');
				if (depositDate != null && depositDate != '' && depositDate != 'undefined') 
				{
					depositDate = depositDate;
				}
				else 
				{
					depositDate = '';
				}
				nlapiLogExecution('DEBUG', 'printSO', " Deposit Date : " +depositDate);
				
				message += "			<tr>";
				message += "				<td width=\"30%\" border=\"0.1\" style=\"border-top-style:none;\" >"+depositNo+"<\/td>";
				message += "				<td width=\"40%\" border=\"0.1\" style=\"border-top-style:none;\">"+payment+"<\/td>";
				message += "				<td border=\"0.1\" width=\"30%\" style=\"border-top-style:none;border-left-style:none;\">"+depositDate+"<\/td>";
				message += "			<\/tr>";
			}
		}
	  }
	message += "			<tr>";
	message += "				<td width=\"50%\" border=\"0.1\" style=\"border-top-style:none;\"><b>Total Deposit <\/b><\/td>";
	message += "				<td colspan=\"2\" border=\"0.1\" style=\"border-top-style:none;\"><b>"+totalpayment+"<\/b><\/td>";
	message += "				<\/tr>";
	message += "			<tr>";
	message += "				<td colspan=\"3\" border=\"0.1\"><b>Customer Balance <\/b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>"+custBal+"<\/b><\/td>";
	message += "				<\/tr>";
	message += "		<\/table>";
	message += "		<\/td>";
	message += "	<\/tr>";
*/
	//message += "	<tr>";
	//message += "		<td width=\"50%\"><b>Complete Address<\/b> <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+address+"<\/p><\/td>";
	//message += "	<\/tr>";
	message += "<\/table>";
	//----------------------------------Customer Depository------------------------------------//
	

	
	

	//-------------------------------------Line Item-------------------------------------------//
	
	message += "<table border=\"0.1\"  width=\"100%\" >";
	message += "	<tr>";
	message += "		<td align=\"center\" border=\"0.1\"  width=\"0.5%\" height=\"15\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Sr.No<\/b><\/td>";
	message += "		<td align=\"left\" border=\"0.1\"  width=\"35%\" height=\"15\" style=\"border-top-style:none;border-bottom-style:none;border-right-style:none;font-size: 8pt;\"><b>Item Description<\/b><\/td>";
	message += "		<td align=\"left\" border=\"0.1\"  width=\"25%\" height=\"15\" style=\"border-top-style:none;border-bottom-style:none;border-left-style:none;font-size: 8pt;\"><b><\/b><\/td>";
	//message += "		<td align=\"center\" border=\"0.1\"  width=\"10%\" height=\"15\" style=\"border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Delivered<\/b><\/td>";
	//message += "		<td align=\"center\" border=\"0.1\"  width=\"10%\" height=\"15\" style=\"border-left-style:none;border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Adjusted<\/b><\/td>";
	message += "		<td align=\"right\" border=\"0.1\"  width=\"12%\" height=\"15\" style=\"border-left-style:none;border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Act. Gross Wt.<\/b><\/td>";
	message += "		<td align=\"right\" border=\"0.1\"  width=\"12%\" height=\"15\" style=\"border-left-style:none;border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Actual Net Wt.<\/b><\/td>";
	message += "		<td align=\"center\" border=\"0.1\"  width=\"0.5%\" height=\"15\" style=\"border-left-style:none;border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Qty<\/b><\/td>";
	//message += "		<td align=\"right\" border=\"0.1\"  width=\"5%\" height=\"15\" style=\"border-left-style:none;border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Rate<\/b><\/td>";
	message += "		<td align=\"right\" border=\"0.1\"  width=\"6%\" height=\"15\" style=\"border-top-style:none;border-bottom-style:none;font-size: 8pt;\"><b>Amount<\/b><\/td>";
	message += "	<\/tr>";

	
	 var SOLineItemCount=recObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG','In printInvoice', " SO Line Item Count = " + SOLineItemCount);
		
		if (SOLineItemCount != null && SOLineItemCount != '' && SOLineItemCount != 'undefined') 
		{
			//var TotalGrossAmtount= 0;
			var msg = '';
			for (i = 1; i <= SOLineItemCount; i++) 
			{
			
				var result;
				var item = recObj.getLineItemValue('item', 'item', i);
				nlapiLogExecution('DEBUG', 'printSO', " item : " + item);
			//----------------------------------Sr No.-----------------------------------------------//
				var sNo = recObj.getLineItemValue('item', 'custcol_srno', i);
				nlapiLogExecution('DEBUG', 'printInvoice()', " sNo :" + sNo);
				if (sNo != null && sNo != '' && sNo != 'undefined') 
				{
					sNo = sNo;
				}
				else 
				{
					sNo = '';
				}
				
				nlapiLogExecution('DEBUG', 'printInvoice()', " Sr No :" + sNo);
			//----------------------------------Sr No.-----------------------------------------------//
				var ItemArry=getItemName(item)	
			//---------------------------------Item name---------------------------------------------//
				var itemName=ItemArry['name'];
				nlapiLogExecution('DEBUG', 'printSO', " Item Name: " +itemName);
				if (itemName != null && itemName != '' && itemName != 'undefined') 
				{
					itemName = itemName;
				}
				/*
else 
				{
					itemName = '';
				}
				
				nlapiLogExecution('DEBUG', 'printSO', " Item Name: " +itemName);
*/
			//---------------------------------Item name---------------------------------------------//
			
			//---------------------------------Item Image---------------------------------------------//
				
				var itemimage = ItemArry['Url']
				if (itemimage != null && itemimage != '' && itemimage != 'undefined') 
				{
					itemimage = nlapiEscapeXML(itemimage);
					var flag= checkURL(itemimage) ;
					nlapiLogExecution('DEBUG', 'printSO', " Flag: " +flag);
					if(flag==false)
					{
						itemimage='';
					}
					
				}
				/*
else 
				{
					itemimage = '';
				}
				
				nlapiLogExecution('DEBUG', 'printSO', " Item image : " +itemimage);

*/
			//---------------------------------Item Image---------------------------------------------//	
			//---------------------------------Item Description---------------------------------------//			
				var itemDesc = ItemArry['description']
				nlapiLogExecution('DEBUG', 'printSO', " Item Description: " +itemDesc);
				if (itemDesc != null && itemDesc != '' && itemDesc != 'undefined') 
				{
					itemDesc = itemDesc;
				}
				/*
else 
				{
					itemDesc = '';
				}
				
				nlapiLogExecution('DEBUG', 'printSO', " Item Description: " +itemDesc);
*/
			//---------------------------------Item Description---------------------------------------//
			//----------------------------------Item category---------------------------------------//	
				var itemCategory = ItemArry['category'];
				nlapiLogExecution('DEBUG', 'printSO', " Item Category : " +itemCategory);
				if (itemCategory != null && itemCategory != '' && itemCategory != 'undefined') 
				{
					itemCategory = itemCategory;
				}
				/*
else 
				{
					itemCategory = '';
				}
				nlapiLogExecution('DEBUG', 'printSO', " Item Category : " +itemCategory);
*/
			//----------------------------------Item category---------------------------------------//
			
			//----------------------------------Delivered-------------------------------------------//
				var delivered = recObj.getLineItemValue('item', 'custcol2', i);
				nlapiLogExecution('DEBUG', 'printSO()', " Delivered : " +delivered);
				
				if (delivered != null && delivered != '' && delivered != 'undefined') 
				{
					delivered = delivered;
				}
				if(delivered=="T")
				{
					delivered="Yes"
				}
				else
				{
					delivered="No"
				}
				
				nlapiLogExecution('DEBUG', 'printSO', " Delivered : " +delivered);
			
			//----------------------------------Delivered-------------------------------------------//
			
			//----------------------------------Adjusted--------------------------------------------//
				var adjusted = recObj.getLineItemValue('item', 'custcol3', i);
				nlapiLogExecution('DEBUG', 'printSO()', " Adjusted : " +adjusted);
				
				if (adjusted != null && adjusted != '' && adjusted != 'undefined') 
				{
					adjusted = adjusted;
				}
				if(adjusted=="T") 
				{
					adjusted="Yes";
				}
				else 
				{
					adjusted = "No";
				}
				nlapiLogExecution('DEBUG', 'printSO', " Adjusted : " +adjusted);
			
			//----------------------------------Adjusted--------------------------------------------//
			
			//-----------------------------------Act Gross Wt----------------------------------------//
				var actGWeight = recObj.getLineItemValue('item', 'custcol_act_gw', i);//custcol_act_gw
				nlapiLogExecution('DEBUG', 'printInvoice()', " Actual gross Weight : " + actGWeight);
				
				if (actGWeight != null && actGWeight != '' && actGWeight != 'undefined') 
				{
					actGWeight = actGWeight;
				}
				else 
				{
					actGWeight = 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " Actual gross Weight : " +actGWeight);
				
			//-----------------------------------Act Gross Wt----------------------------------------//	
				var actNetWeight = recObj.getLineItemValue('item', 'custcol_act_new', i);//custcol_net_weight
				nlapiLogExecution('DEBUG', 'printInvoice()', " Net Weight : " + actNetWeight);
				
				if (actNetWeight != null && actNetWeight != '' && actNetWeight != 'undefined') 
				{
					actNetWeight = actNetWeight;
				}
				else 
				{
					actNetWeight = 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " Actual Net Weight : " + actNetWeight);
			//-----------------------------------Act Gross Wt----------------------------------------//	
			//------------------------------------Quantity--------------------------------------------//
				var quantity = recObj.getLineItemValue('item', 'quantity', i);
				if (quantity != null && quantity != '' && quantity != 'undefined') 
				{
					quantity = quantity;
				}
				else 
				{
					quantity = 0;
				}
				nlapiLogExecution('DEBUG', 'printSO', " Quantity: " + quantity);
			//------------------------------------Quantity--------------------------------------------//
			//------------------------------------Rate------------------------------------------------//
			
				var Rate = recObj.getLineItemValue('item', 'rate', i);
				if (Rate != null && Rate != '' && Rate != 'undefined' && !isNaN(Rate)) 
				{
					Rate = Rate;
				}
				else 
				{
					Rate = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Rate: " + Rate);
			//------------------------------------Rate------------------------------------------------//
			//------------------------------------Amount----------------------------------------------//
				var grossAmount = recObj.getLineItemValue('item', 'amount', i);	
				nlapiLogExecution('DEBUG', 'printInvoice()', " BeforeAmount : " + grossAmount);			
				if (grossAmount != null && grossAmount != '' && grossAmount != 'undefined' && !isNaN(grossAmount)) 
				{
					grossAmount = grossAmount;
				}
				else 
				{
					grossAmount = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " AfterAmount : " + grossAmount);
			//------------------------------------Amount----------------------------------------------//
			
			//--------------------------------------Total--------------------------------------------//
				var TotalGrossAmtount = recObj.getFieldValue('total');
				nlapiLogExecution('DEBUG', 'printInvoice()', " BeforeAmount : " + TotalGrossAmtount);			
				if (TotalGrossAmtount != null && TotalGrossAmtount != '' && TotalGrossAmtount != 'undefined' && !isNaN(TotalGrossAmtount)) 
				{
					TotalGrossAmtount = TotalGrossAmtount;
				}
				else 
				{
					TotalGrossAmtount = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " AfterAmount : " + TotalGrossAmtount);
			//--------------------------------------Total--------------------------------------------//
			
				/*
totalGrossAmt= parseFloat(totalGrossAmt)+ parseFloat(grossAmount);
				nlapiLogExecution('DEBUG', 'printInvoice()', " first Total Gross Amount : " +totalGrossAmt);
				
				if (totalGrossAmt != null && totalGrossAmt != '' && totalGrossAmt != 'undefined' && !isNaN(totalGrossAmt)) 
				{
					totalGrossAmt = grossAmount;
				}
				else 
				{
					totalGrossAmt = '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Total Gross Amount : " +totalGrossAmt);
		
		
*/
	
		
		// -----------------------------------------Total Amount------------------------------------------------------//
		
		
    		
				if (TotalGrossAmtount != null && TotalGrossAmtount != '' && TotalGrossAmtount != 'undefined' && !isNaN(TotalGrossAmtount))
				 {
					nlapiLogExecution('DEBUG', 'after Submit', 'FirsttotalGrossAmount=' + TotalGrossAmtount);
					
					if (TotalGrossAmtount == '0') 
					{
						var ZeroWord = 'Rs. Zero Only';
					}
				 else
				 {
				 	var amtstr = toWordsFunc(TotalGrossAmtount);
					var v = 0;
					v = amtstr.search('And');
					if (v != -1)
				    {
						amtstr = amtstr + 'Paisaa ';
					}
					amtstr = amtstr + 'Only';
					
					var Vatstring = amtstr;    //To Display the Amount in words
				 }
				}
	//-------------------------Amount setting------------------------------------------------------------//	
			
			
		message += "	<tr>";
		message += "		<td align=\"center\" width=\"0.5%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+sNo+"<\/td>";
		message += "<td align=\"left\" width=\"35%\" height=\"15\" border=\"0.1\" style=\"border-right-style:none;font-size: 9pt;\">"+itemName+" <p>"+itemCategory+"<\/p> <p>"+itemDesc+"<\/p><\/td> ";
		if (itemimage != '') 
		{
			message += "<td align=\"left\" width=\"25%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\" ><img width=\"58px\" height=\"44px\" align=\"right\" src=\"" +itemimage+ " \" \/><\/td> ";
		}
		else  
		{
			message += "<td align=\"left\" width=\"25%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\"><\/td> ";
		}

		//message += "		<td align=\"center\" valign=\"middle\" width=\"10%\" height=\"15\" border=\"0.1\" style=\"font-size: 9pt;\">"+delivered+"<\/td>";
		//message += "		<td align=\"center\" valign=\"middle\" width=\"10%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+adjusted+"<\/td>";
		message += "		<td align=\"center\" valign=\"middle\" width=\"12%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+actGWeight+"<\/td>";
		message += "		<td align=\"center\" valign=\"middle\" width=\"12%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+actNetWeight+"<\/td>";
		message += "		<td align=\"center\" valign=\"middle\" width=\"0.5%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+quantity+"<\/td>";
		//message += "		<td align=\"right\" valign=\"middle\" width=\"5%\" height=\"15\" border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\">"+Rate+"<\/td>";
		message += "		<td align=\"right\" valign=\"middle\" width=\"6%\" height=\"17\" border=\"0.1\" style=\"font-size: 9pt;\">"+grossAmount+"<\/td>";
		message += "	<\/tr>";
		
			}
		}
		
		message += "<tr>";
		message += "		<td border=\"0.1\" colspan=\"5\" style=\"border-left-style:none;font-size: 9pt;\"><b>Amount in words for total amount :&nbsp; ";
		message += "		"+Vatstring+"<\/b><\/td>";
		message += "		<td border=\"0.1\" style=\"border-top-style:none;font-size: 9pt;\" ><b>Total<\/b><\/td>";
		message += "		<td border=\"0.1\" style=\"border-left-style:none;font-size: 9pt;\" ><b>"+TotalGrossAmtount+"<\/b><\/td>";
		message += "	<\/tr>";
		message += "<tr>";
		message += "		<td border=\"0.1\" colspan=\"6\" style=\"border-bottom-style:none;border-right-style:none;border-top-style:none;border-left-style:none;font-size: 9pt;\" align=\"right\" ><b>Payment Received<\/b><\/td>";
		message += "		<td style=\"font-size: 9pt;\" align=\"right\"><b>"+paymentRec+"<\/b><\/td>";
		message += "	<\/tr>";
	
		message += "<tr>";
		message += "		<td border=\"0.1\" colspan=\"6\" style=\"border-bottom-style:none;border-top-style:none;border-right-style:none;border-left-style:none;font-size: 9pt;\" align=\"right\"><b>Payment Due<\/b><\/td>";
		message += "		<td style=\"font-size: 9pt;\" align=\"right\"><b>"+paymentDue+"<\/b><\/td>";
		message += "	<\/tr>";
		message += "<\/table>";
		
		
		/*
message += "<table border=\"0.1\" width=\"100%\">";
		message += "	<tr>";
		message += "		<td style=\"font-size: 9pt;\" align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
		message += "		<b>Payment Received</b><\/td><td style=\"font-size: 9pt;\" align=\"right\"><b>"+paymentRec+"</b><\/td>";
		message += "	<\/tr>";
		message += "	<tr>";
		message += "		<td style=\"font-size: 9pt;\" align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
		message += "		<b>Payment Due</b><\/td><td style=\"font-size: 9pt;\" align=\"right\"><b>"+paymentDue+"</b>";
		message += "		<\/td>";
		message += "	<\/tr>";
		message += "<\/table>";
*/
		

		
	
	//-------------------------------------Line Item-------------------------------------------//
	//-------------------------------------Last table------------------------------------------//
	
	
	message += "<table border=\"0.1\" width=\"100%\">";
	message += "	<tr>";
	message += "		<td style=\"font-size: 9pt;\" ><b>**Terms And Conditions: VAT Extra to be Printed on Estimate Print <\/b>&nbsp;<p>&nbsp;<\/p>";
	message += "		<b>Verified By&nbsp;:<\/b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
	message += "		<b>Verified Date :<\/b><\/td>";
	message += "	<\/tr>";
	message += "<\/table>";
	message += "";



  



//========================================================Line Item=======================================================//
		//nlapiLogExecution('DEBUG', 'Message', " message : " + message);
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">"+message+"</body>\n</pdf> ";
		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF','SO Print.pdf', 'inline');
		response.write( file.getValue() );

}

	function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
	{
	
	 messgaeToBeSendPara = messgaeToBeSendPara.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);
	
	 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
	
	 return messgaeToBeSendPara;
	}

	function formatAndReplaceSpacesOnlyfMessage(messgaeToBeSendPara)
	{
	
	 messgaeToBeSendPara = messgaeToBeSendPara.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);
	
	 messgaeToBeSendPara = messgaeToBeSendPara.replace(/ /g,"&nbsp");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
	
	 return messgaeToBeSendPara;
	}

	function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
	{
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);
	
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&amp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
	
	 return messgaeToBeSendParaAnd;
	}
	function formatAndReplaceMessageForhash(messgaeToBeSendParaAnd)
	{
	
	 
	 
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);
	
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/#/g,"&amp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
	
	 return messgaeToBeSendParaAnd;
	}

//---------------------------------item Description and URL--------------------------------------------------//
				function getItemName(item)
				{
					var name = '';
					var Url = '';
					if (item != '' && item != null) 
					{
						var Filters = new Array();
						var Column = new Array();
						
						Filters[0] = new nlobjSearchFilter('internalid', null, 'is', item)
						
						Column[0] = new nlobjSearchColumn('itemid');
						Column[1] = new nlobjSearchColumn('salesdescription');
						Column[2] = new nlobjSearchColumn('custitem_item_category');
						Column[3] = new nlobjSearchColumn('custitem_img_url');
						
						var result = nlapiSearchRecord('item', null, Filters, Column);
						nlapiLogExecution('DEBUG', 'In getItemName', 'search==' +result);
						if (result != null)
						{
							name = result[0].getValue('itemid');
							if (name != null && name != '' && name != 'undefined') 
							{
								name = name;
							}
							else 
							{
								name = '';
							}
							nlapiLogExecution('DEBUG', 'getItemName', 'name = ' +name);
							
							description = result[0].getValue('salesdescription');
							if (description != null && description != '' && description != 'undefined') 
							{
								description = formatAndReplaceSpacesofMessage(nlapiEscapeXML(description));//description;
							}
							else 
							{
								description = '';
							}
							nlapiLogExecution('DEBUG', 'getItemName', 'Description = ' +description);
							
							category = result[0].getText('custitem_item_category');
							if (category != null && category != '' && category != 'undefined') 
							{
								category = category;
							}
							else 
							{
								category = '';
							}
							nlapiLogExecution('DEBUG', 'getItemName', 'Category = ' +category);
							
							Url = result[0].getValue('custitem_img_url');
							if (Url != null && Url != '' && Url != 'undefined') 
							{
								Url = Url;
							}
							else 
							{
								Url = '';
							}
							nlapiLogExecution('DEBUG', 'getItemName', 'Url = ' +Url);
						}
						
					}
					var ItemArry=new Array();
					ItemArry['name']=name;
					ItemArry['description']=description;
					ItemArry['category']=category;
					ItemArry['Url']=Url;
					return ItemArry;
				}	
				
				 			
 //---------------------------------item Description and URL--------------------------------------------------//	
//--------------------------------Amount in words----------------------------------------------------------------//

function toWordsFunc(s)
{
   // alert('In conversion function')
    var str='Rs.'

    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}

//---------------------------------------------------------------------------------------Amount in Words----------------------------------//

function checkURL(value) 
{
    var Url = new RegExp("^(http:\/\/system.|https:\/\/system.|ftp:\/\/system.|system.){1}([0-9A-Za-z]+\.)");
	nlapiLogExecution('DEBUG', 'getItemName', 'value = ' +value);
	nlapiLogExecution('DEBUG', 'getItemName', 'After reg exp = ' +Url.test(value));
    if (Url.test(value))
	
	{
        return (true);
		
    }
    return (false);
}




