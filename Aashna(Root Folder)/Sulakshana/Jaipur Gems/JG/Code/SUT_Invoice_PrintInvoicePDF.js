// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Suitlet for Quotation print
	Author:Sulakshana Ghadage	
	Company:JG
	Date:24-9-2012
	Description:Design of HTML is Added in Suitlet and All entries of form getting  
	from Invoice and Item ,which is get into the Suitlet.



	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

function printInvoice(request,response)
{
		var internalId;
		
		var SOInternalId = request.getParameter('custscriptcustscript1');
		nlapiLogExecution('DEBUG', 'SOInternalId', " SO Internal ID :" +SOInternalId);
		
		//var buttonType = request.getParameter('custscript_buttonpara');
		//nlapiLogExecution('DEBUG', 'printInvoice', " buttonType :" + buttonType);
		
		var recObj;
		var recordType='';
		
		try 
		{
			var recObj = nlapiLoadRecord('salesorder', SOInternalId);
			nlapiLogExecution('DEBUG', 'printInvoice', " recObj :" + recObj);
			recordType = 'salesorder';
		} 
		catch (e) 
		{
			recObj = nlapiLoadRecord('salesorder', InvoiceInternalId);
			nlapiLogExecution('DEBUG', 'printInvoice', " recObj :" + recObj);
			recordType = 'salesorder';
		}

		var remark = recObj.getFieldValue('custbody_estimate_remarks');
      	nlapiLogExecution('DEBUG', 'printInvoice', " remark :" + remark);
		
		if(remark != null && remark!='' && remark!='undefined')
		{
			remark = remark;
		}
		else
		{
			remark = '';
		}
		
		var buyer = recObj.getFieldValue('custbody_buyer');
		nlapiLogExecution('DEBUG', 'printInvoice', " buyer :" + buyer);
		
		if(buyer != null && buyer!='' && buyer!='undefined')
		{
			buyer = buyer;
		}
		else
		{
			buyer = '';
		}
		
		
       // var soid = request.getParameter('custscript_soid');
		//nlapiLogExecution('DEBUG', 'printInvoice', " So ID :" + soid);
		//if(soid!=null && soid!=''&& soid!='undefined')
		//{
			
//------------------------------Buyer's Order No---------------------------------------------//			
			var SOId=recObj.getFieldValue('createdfrom');
			nlapiLogExecution('DEBUG', 'printInvoice', " SOId :" +SOId);
			if(SOId!=''&&SOId!=null)
			{
				var soDetails = nlapiLoadRecord('salesorder',SOId);
			    nlapiLogExecution('DEBUG', 'printInvoice', " soDetails :" +soDetails);
				var BuyerOrderNo=soDetails.getFieldValue('otherrefnum');  //tranid
				nlapiLogExecution('DEBUG', 'printInvoice()', " BuyerOrderNo :" +BuyerOrderNo);
				if(BuyerOrderNo!=null && BuyerOrderNo!=''&& BuyerOrderNo!=undefined)
				{
					BuyerOrderNo=BuyerOrderNo;
				}
				else
				{
					BuyerOrderNo='';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', "Sales order no:" +BuyerOrderNo);
				
				var OrderDate=soDetails.getFieldValue('custbody4');  //trandate
				if(OrderDate!=null && OrderDate!=''&& OrderDate!='undefined')
				{
					OrderDate=OrderDate;
				}
				else
				{
					OrderDate='';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', "Sales order date:" +OrderDate); 
			}
			
			else
			{
				var BuyerOrderNo=recObj.getFieldValue('otherrefnum');  //tranid
				nlapiLogExecution('DEBUG', 'printInvoice()', " BuyerOrderNo :" +BuyerOrderNo);
				
				if(BuyerOrderNo!=null && BuyerOrderNo!=''&& BuyerOrderNo!=undefined)
				{
					BuyerOrderNo=BuyerOrderNo;
				}
				else
				{
					BuyerOrderNo='';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', "Sales order no:" +BuyerOrderNo);
				
				var OrderDate=recObj.getFieldValue('custbody4');  //trandate
				if(OrderDate!=null && OrderDate!=''&& OrderDate!='undefined')
				{
					OrderDate=OrderDate;
				}
				else
				{
					OrderDate='';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', "Sales order date:" +OrderDate);
				
				//BuyerOrderNo='';
				//OrderDate='';
			}
			
	//------------------------------Buyer's Order No---------------------------------------------//
	//------------------------------Order Date---------------------------------------------//
		  /*
  var OrderDate=recObj.getFieldValue('trandate');
			if(OrderDate!=null && OrderDate!=''&& OrderDate!='undefined')
			{
				OrderDate=OrderDate;
			}
			else
			{
				OrderDate='';
			}
			nlapiLogExecution('DEBUG', 'printInvoice()', "Sales order date:" +OrderDate); 
*/
		//}
		
//------------------------------Order Date---------------------------------------------//		


	   
		
		 
//------------------------------Order Date---------------------------------------------//     
//----------------------------Final destination Country---------------------------------------//
 	     var finalDesit = recObj.getFieldText('custbody_country');
		 if(finalDesit!=null && finalDesit!=''&& finalDesit!='undefined')
		 {
		 finalDesit = finalDesit;
		 }
		 else
		 {
		 finalDesit = '';
		 }
		
         //name= formatAndReplaceSpacesofMessage(name);
		 nlapiLogExecution('DEBUG', 'printInvoice()', "Country Of Final Destination:" +finalDesit);
//----------------------------Final destination Country---------------------------------------//
//----------------------------PreCarriaged By-------------------------------------------------//
         var preCarriagedBy = recObj.getFieldValue('shipcarrier');
		 if(preCarriagedBy!=null && preCarriagedBy!=''&& preCarriagedBy!='undefined')
		 {
		 preCarriagedBy = preCarriagedBy;
		 }
		 else
		 {
		 preCarriagedBy = '';
		 }
		
         //name= formatAndReplaceSpacesofMessage(name);
		 nlapiLogExecution('DEBUG', 'printInvoice()', "Pre-Carriage By :" +preCarriagedBy);
//----------------------------PreCarriaged By-------------------------------------------------//
//-----------------------------------------Date-----------------------------------------------//			
		var Date = recObj.getFieldValue('trandate');
		if (Date != null && Date != '' && Date != 'undefined') 
		{
			Date = Date;
		}
		else 
		{
			Date = '';
		}
		nlapiLogExecution('DEBUG', 'printInvoice()', " Date :" + Date);
//-----------------------------------------Date-----------------------------------------------// 
//-----------------------------------------Customer name-----------------------------------------------//

		/*
		var CustomerName = recObj.getFieldValue('billaddress');
		if (CustomerName != null && CustomerName != '' && CustomerName != 'undefined')
	    {
			CustomerName = formatAndReplaceSpacesofMessage(nlapiEscapeXML(CustomerName));
		}
		else 
		{
			CustomerName = '';
		}
		
		*/
		
		var CustomerName = '<b>STARFIRE GEMS PVT LTD<\/b><br/>A/4, Todi Industrial Estate,<br/>Summill Compound, Lower Parel, Mumbai<br/>India';
		
		nlapiLogExecution('DEBUG', 'printInvoice()', " Customer Name:" + CustomerName);
//-----------------------------------------Customer name-----------------------------------------------//
//-----------------------------------------Consignee Name & Address-----------------------------------------------//

		/*		
		var ConsigneeNameAdd = recObj.getFieldValue('billaddress');
		if (ConsigneeNameAdd != null && ConsigneeNameAdd != '' && ConsigneeNameAdd != 'undefined')
	    {
			ConsigneeNameAdd = formatAndReplaceSpacesofMessage(nlapiEscapeXML(ConsigneeNameAdd));
		}
		else 
		{
			ConsigneeNameAdd = '';
		}
		
		*/
		
		var ConsigneeNameAdd = '<b>STARFIRE GEMS PVT LTD<\/b><br/>A-Wing, 2nd Floor, TODI INDUSTRIAL ESTATE<br/>SUMMILL COMPOUND, LOWER PAREL (WEST)<br/>MUMBAI';
		
		nlapiLogExecution('DEBUG', 'printInvoice()', " Consignee Name and Address:" + ConsigneeNameAdd);
//-----------------------------------------Consignee Name & Address-----------------------------------------------//
//-----------------------------------------Invoice No-----------------------------------------------//		
		var InvoiceNo = recObj.getFieldValue('tranid');
		if (InvoiceNo != null && InvoiceNo != '' && InvoiceNo != 'undefined')
	    {
			InvoiceNo = InvoiceNo;
		}
		else 
		{
			InvoiceNo = '';
		}
		nlapiLogExecution('DEBUG', 'printInvoice()', " Invoice No:" + InvoiceNo);
//-----------------------------------------Invoice No-----------------------------------------------//	
//-----------------------------------------Date-----------------------------------------------//			
		var Date = recObj.getFieldValue('trandate');
		if (Date != null && Date != '' && Date != 'undefined') 
		{
			Date = Date;
		}
		else 
		{
			Date = '';
		}
		nlapiLogExecution('DEBUG', 'printInvoice()', " Date :" + Date);
//-----------------------------------------Date-----------------------------------------------//
//-----------------------------------------Terms & condition-----------------------------------------------//
	    var Terms = recObj.getFieldText('terms');
		if (Terms != null && Terms != '' && Terms != 'undefined')
	    {
			Terms =nlapiEscapeXML(Terms);
		}
		else 
		{
			Terms = '';
		}
		nlapiLogExecution('DEBUG', 'printInvoice()', " Terms of Delivery and Payment :" + Terms);
//-----------------------------------------Terms & condition-----------------------------------------------//
//-----------------------------------------net Amount------------------------------------------------------//
	            var netAmount = recObj.getFieldValue('total');
				
				if (netAmount != null && netAmount != '' && netAmount != 'undefined') 
				{
					netAmount = netAmount;
				}
				else 
				{
					netAmount = '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Net Amount : " + netAmount);
//-----------------------------------------net Amount------------------------------------------------------//

//-----------------------------------------net Amount------------------------------------------------------//
	            var totalAmount;
				
				if(recordType=='invoice')
				{
					totalAmount= recObj.getFieldValue('amountremainingtotalbox');
				}
				else if(recordType=='salesorder')
				{
					totalAmount= recObj.getFieldValue('total');
				}
				
				
				if (totalAmount != null && totalAmount != '' && totalAmount != 'undefined') 
				{
					totalAmount = totalAmount;
				}
				else 
				{
					netAmount = '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Total Amount : " + totalAmount);
//-----------------------------------------net Amount------------------------------------------------------//

//-----------------------------------------Total Amount------------------------------------------------------//
			var totalInvoiceAmount=recObj.getFieldValue('total')
			nlapiLogExecution('DEBUG', 'after Submit', 'totalInvoiceAmount=' + totalInvoiceAmount);
			if (totalInvoiceAmount == '0') 
				{
					var ZeroWord = 'Rs. Zero Only';
				}
			else
			 {
			 	var amtstr = toWordsFunc(totalInvoiceAmount);
				var v = 0;
				v = amtstr.search('And');
				if (v != -1)
			    {
					amtstr = amtstr + 'Paisaa ';
				}
				amtstr = amtstr + 'Only';
				
				var Vatstring = amtstr;    //To Display the Amount in words
			}
			nlapiLogExecution('DEBUG', 'after Submit', 'Amt in words=' + Vatstring);
			//InvoiceRec.setFieldValue('custbody_amtinwords',Vatstring)
//-------------------------Amount setting------------------------------------------------------------//

//-------------------------Exporter's Reference------------------------------------------------------------//
               var exporttoref = '';
						
				nlapiLogExecution('DEBUG', 'printInvoice()', " Net Amount : " + netAmount);
//-------------------------Exporter's Reference------------------------------------------------------------//
		
//=========================================New Line Item============================================//	
  var breakLine='x';
//----------------------------------------------Invoice HTML-------------------------------------------------------//
var message=" ";
message += "<p align=\"center\"><b style=\"font-size:12\">Invoice Cum Delivery Challan<\/b><\/p> ";

message += "<table border=\"0.1\" width=\"100%\" height=\"10%\" style=\"border-collapse: collapse\"> ";
message += "	<tr> ";
message += "		<td align=\"left\" rowspan=\"2\"  border=\"0.1\" width=\"54%\" style=\"border-top-style:none;border-left-style:none;border-bottom-style:none; \" >"+CustomerName+"<\/td> ";     //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
message += "		<td width=\"30%\"><b>Invoice No<\/b><br/>"+InvoiceNo+"<\/td>";  //+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
message += "		<td width=\"30%\" border=\"0.1\" style=\"border-bottom-style:none;border-top-style:none;\"><b align=\"right\">Date<\/b><br/>"+Date+" <\/td> ";
message += "		<td border=\"0.1\" width=\"30%\" style=\"border-bottom-style:none;border-top-style:none;\"><b align=\"left\">Exporter's Reference<\/b><\/td> ";   // style=\"border-top-style:none; \" // "+exporttoref+" 
message += "	<\/tr> ";




message += "	<tr> ";
message += "		<td align=\"left\"  width=\"35%\" border=\"0.1\" style=\"border-left-style:none;\"><b>Buyer's&nbsp;Order&nbsp;No.<\/b><br/>"+BuyerOrderNo+"<\/td>";     //rowspan=\"2\"  //+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "
message += "		<td align=\"left\" width=\"25%\" border=\"0.1\" style=\"border-left-style:none;\"><b>Date<\/b><br/>"+OrderDate+"  <\/td> ";      //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
message += "		<td border=\"0.1\" width=\"40%\"><b>PAN<\/b> <\/td> ";   //rowspan=\"2\" //  style=\"border-top-style:none; \"
message += "	<\/tr> ";


message += "	<tr> ";
////message += " 		<td height=\"5%\" border=\"0.1\" width=\"50%\" style=\"border-top-style:none;border-left-style:none;\"><b>Terms of Delivery And Payment<\/b>&nbsp;&nbsp;"+Terms+"<\/td>";  // rowspan=\"3\" 
message += " 		<td border=\"0\" style=\"border-top-style:none;\" width=\"54%\"><\/td>";  // width=\"54%\"
message += "		<td border=\"0.1\" width=\"25%\" colspan=\"2\" align=\"left\" style=\"border-top-style:none;border-right-style:none;border-bottom-style:none;\"><b>Other Reference(s)<\/b><\/td> ";
//message += " 		<td><\/td>";
message += "		<td border=\"0.1\" width=\"40%\" style=\"border-top-style:none;border-bottom-style:none; \" ><b>G.R. No<\/b><\/td> ";
message += "	<\/tr> ";


message += "<\/table> ";

//----------------------------------------------Invoice HTML-------------------------------------------------------//
//-----------------------------------------------------2-----------------------------------------------------------//



message += "<table border=\"0.1\" width=\"100%\" style=\"border-collapse: collapse\"> ";  //height=\"10%\"
message += "	<tr> ";
message += "		<td rowspan=\"2\" width=\"52.2%\" border=\"0.1\" align=\"left\" style=\"border-right-style:none;border-bottom-style:none;border-top-style:none;border-left-style:none;\"><b>Consignee Name &amp; Address<\/b><br/>"+ConsigneeNameAdd+"<\/td> ";  //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
message += "		<td colspan=\"2\" border=\"0.1\" height=\"30\" width=\"48%\" style=\"border-top-style:none; \"><b>Buyer (if other then consignee)<\/b><br/><br/>"+buyer+"<\/td> ";

//message += " 		<td border=\"0.1\" width=\"40%\" style=\"border-top-style:none;\"><b>Terms of Delivery And Payment<\/b>&nbsp;&nbsp;"+Terms+"<\/td>";   //border-left-style:none;height=\"5%\" 


message += "	<\/tr> ";

message += "	<tr> ";
message += "		<td  border=\"0.1\" width=\"45%\" style=\"border-bottom-style:none;\"><b>Country of origin of Goods<\/b><\/td> ";
message += "		<td border=\"0.1\" width=\"45%\" style=\"border-bottom-style:none;\"><b>Country of Final destination<\/b> "+finalDesit+" <\/td> ";
//message += "		<td><\/td> ";
//message += "		<td><\/td> ";


message += "	<\/tr> ";



message += "<\/table> ";
message += " ";



//-----------------------------------------------------2-----------------------------------------------------------//
//-----------------------------------------------------3------------------------------------------------------------//
//=======================================================================================================================

message += "<table border=\"0.1\" width=\"100%\" height=\"5%\">";
message += "	<tr>";
message += "		<td width=\"15%\" height=\"1.6%\"><b>Pre Carriage by <\/b><\/td>";   //"+preCarriagedBy+"
message += "		<td width=\"25.6%\" height=\"1.6%\" border=\"0.1\" style=\"border-top-style:none;border-bottom-style:none; \" >Place of reciept by pre-carriage<br/><b>Lower Parel, Mumbai<\/b> <\/td>";
message += "		<td rowspan=\"3\" height=\"5%\" border=\"0.1\" width=\"64%\" style=\"border-top-style:none;border-left-style:none;  \"><b>Terms of Delivery And Payment<\/b>&nbsp;&nbsp;"+Terms+"<\/td>";
message += "	<\/tr>";
message += "	<tr>";
message += "		<td width=\"15%\" height=\"1.6%\" border=\"0.1\" style=\"border-left-style:none; \"><b>Vessel/Flight No. vfn<\/b><\/td>";
message += "		<td width=\"25.6%\" height=\"1.6%\" border=\"0.1\">Port of loading<br/><b>Lower Parel, Mumabi<\/b><\/td>";
message += "	<\/tr>";
message += "	<tr>";
message += "		<td width=\"15%\" height=\"1.6%\" border=\"0.1\" style=\"border-left-style:none; \">Port of discharge<br/><b>MUMBAI<\/b> <\/td>";
message += "		<td width=\"25.6%\" height=\"1.6%\" border=\"0.1\">Final destination<br/><b>INDIA<\/b><\/td>";  // "+finalDesit+"
message += "	<\/tr>"; 
message += "<\/table>";


//====================================================================================================================

//----------------------------------------------------4 line item-----------------------------------------------------//




message += "<table border=\"0.1\" width=\"100%\" height=\"70%\" style=\"border-collapse: collapse\"> ";
message += "	<tr> ";
message += "<td  align=\"center\" border=\"0.1\" style=\"border-left-style:none;\" width=\"5%\" height=\"15\"><b>Sr.NO<\/b><\/td> ";
message += "<td  align=\"center\" border=\"0.1\"  width=\"40%\" height=\"15\" ><b>PARTICULAR<\/b><\/td> ";
message += "<td  align=\"center\" border=\"0.1\"  width=\"5%\" height=\"15\"><b>PCS<\/b><\/td> ";
message += "<td  align=\"center\" border=\"0.1\"  width=\"20%\" height=\"15\" ><b>WEIGHT GMS<\/b><\/td> ";
message += "<td  align=\"right\" border=\"0.1\"  width=\"15%\" height=\"15\" ><b>PRICE\/PCS<\/b><\/td> ";
message += "<td  align=\"right\" border=\"0.1\"  width=\"20%\" height=\"15\"><b>AMOUNT<\/b><\/td> ";
message += "	<\/tr> ";

        var QLineItemCount=recObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG','In printInvoice', " invoice Line Item Count = " + QLineItemCount);
		
		if (QLineItemCount != null && QLineItemCount != '' && QLineItemCount != 'undefined')
		 {
		 	var totalQty=0;
			var totalWt=0;
			var totalRate=0;

		 	var msg='';
			for (i = 1; i <= QLineItemCount; i++)
			 {
			 	
				 var result;
                 var item = recObj.getLineItemValue('item', 'item', i);
				
				nlapiLogExecution('DEBUG', 'printInvoice()', " item : " + item);
				var ItemArry = getItemName(item);
				
				var itemdescription = ItemArry['description'];
				
				if (itemdescription != null && itemdescription != '' && itemdescription != 'undefined') 
				{
					itemdescription = itemdescription;
				}
				else 
				{
					itemdescription = '';
				}
			
				nlapiLogExecution('DEBUG', 'printInvoice()', " itemdescription: " +itemdescription);
				
				
			    var Particuar=ItemArry['name'];
				if (Particuar != null && Particuar != '' && Particuar != 'undefined') 
				{
					Particuar = formatAndReplaceMessageForAnd(nlapiEscapeXML(Particuar));
				}
				
				else
				{
					Particuar = '';
				}
			
				nlapiLogExecution('DEBUG', 'printInvoice()', " Particuar: " +Particuar);
				
			    var itemimage=ItemArry['Url'];
				if (itemimage != null && itemimage != '' && itemimage != 'undefined') 
				{
					itemimage = nlapiEscapeXML(itemimage);
				}
				else 
				{
					itemimage = '';
				}
				
				nlapiLogExecution('DEBUG', 'printInvoice()', " Item image : " +itemimage);
				
				var sNo = recObj.getLineItemValue('item','custcol_srno', i);
				nlapiLogExecution('DEBUG', 'printInvoice()', " sNo :" + sNo);
				if (sNo != null && sNo != '' && sNo != 'undefined') 
				{
					sNo = sNo;
				}
				else 
				{
					sNo = '';
				}
				
				nlapiLogExecution('DEBUG', 'printInvoice()', " Sr No :" + sNo);
				
		        var quantity = recObj.getLineItemValue('item', 'quantity', i);
				if (quantity != null && quantity != '' && quantity != 'undefined' && !isNaN(quantity)) 
				{
					quantity = quantity;
				}
				else 
				{
					quantity = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Quantity : " + quantity);
				
				totalQty=parseFloat(totalQty)+parseFloat(quantity);
                if (totalQty!= null && totalQty!= '' && totalQty!= 'undefined') 
				{
					totalQty= totalQty;
				}
				else 
				{
					totalQty= '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Total Amount : " + totalQty);
				
				var Weight = recObj.getLineItemValue('item', 'custcol_net_weight', i);
				nlapiLogExecution('DEBUG', 'printInvoice()', " Net Weight : " + Weight);
				
				if (Weight != null && Weight != '' && Weight != 'undefined'&& !isNaN(Weight)) 
				{
					Weight = parseFloat(Weight).toFixed(2);
				}
				else 
				{
					Weight = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Net Weight : " + Weight);
				
				totalWt=parseFloat(totalWt)+parseFloat(Weight);
                if (totalWt!= null && totalWt!= '' && totalWt!= 'undefined') 
				{
					totalWt= parseFloat(totalWt).toFixed(2);
				}
				else 
				{
					totalWt= '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Total weight: " + totalWt);
				
				var Rate = recObj.getLineItemValue('item', 'rate', i);
				if (Rate != null && Rate != '' && Rate!= 'undefined' && !isNaN(Rate)) 
				{
					Rate = Rate;
				}
				else
				{
					Rate=0;
				}
				
				
				nlapiLogExecution('DEBUG', 'printInvoice()', " Rate: " + Rate);
				
				totalRate=parseFloat(totalRate)+parseFloat(Rate);
				if (totalRate!= null && totalRate!= '' && totalRate!= 'undefined' ) 
				{
					totalRate= parseFloat(totalRate).toFixed(2);
				}
				else 
				{
					totalRate= '';
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Total weight: " + totalRate);
 
				var grossAmount = 0;
				
				//if(recordType=='invoice')
				{
				//	grossAmount= recObj.getLineItemValue('item', 'grossamt', i);
				}
				//else if(recordType=='salesorder')
				{
					grossAmount= recObj.getLineItemValue('item', 'amount', i);
				}
				
				if (grossAmount != null && grossAmount != '' && grossAmount != 'undefined' && !isNaN(grossAmount)) 
				{
					grossAmount = grossAmount;
				}
				else
				{
					grossAmount = 0;
				}
				nlapiLogExecution('DEBUG', 'printInvoice()', " Gross Amount : " + grossAmount);
				
	          
				msg += "<tr> ";
				msg += "<td align=\"center\" border=\"0\" border-color=\"#C0C0C0\" width=\"5%\" height=\"15\" style=\";border-right-style:none;border-bottom-style:none;\"   >"+i+"<\/td> ";
				
				var split = Particuar.toString().split('<br/>');
				
				if (itemimage != '') 
				{
					msg += "<td  border=\"0.1\"  width=\"5%\" height=\"15\"  style=\"border-right-style:none;border-top-style:none;border-bottom-style:none;font-size:11;\"  >" + Particuar + "<br/><p style=\"font-size:8\" align=\"left\">"+itemdescription+"<\/p> <img width=\"116px\" height=\"87px\" align=\"right\" src=\"" + itemimage + " \" \/><\/td> ";
				}
				else  
				{
					msg += "<td  align=\"left\" border=\"0.1\"  width=\"40%\" height=\"15\"  style=\"border-right-style:none; border-top-style:none;border-bottom-style:none;font-size:11;\"  >" + Particuar+ "<br/><p style=\"font-size:8\" align=\"left\">"+itemdescription+"<\/p><\/td> ";
				}
				msg += "<td  align=\"center\" border=\"0.1\"  width=\"5%\" height=\"15\" style=\"border-right-style:none;border-top-style:none;border-bottom-style:none;font-size:11; \" >"+quantity+"<\/td> ";
				msg += "<td  align=\"center\" border=\"0.1\"  width=\"20%\" height=\"15\" style=\"border-right-style:none; border-top-style:none;border-bottom-style:none;font-size:11;\">"+Weight+"<\/td> ";
				msg += "<td  align=\"right\" border=\"0.1\"  width=\"15%\" height=\"15\" style=\"border-right-style:none; border-top-style:none;border-bottom-style:none;font-size:11;\" >"+Rate+"<\/td> ";
				msg += "<td  align=\"right\" border=\"0.1\"  width=\"20%\" height=\"15\" style=\"border-top-style:none;border-bottom-style:none;font-size:11;\">"+grossAmount+"<\/td> ";
				msg += "<\/tr> ";
                nlapiLogExecution('DEBUG','In printInvoice', " valiue of i = " + i);
			}
			if(isNaN(totalWt))
			{
				totalWt='';
			}
			if(isNaN(totalRate))
			{
				totalRate='';
			}
			if(isNaN(totalAmount))
			{
				totalAmount='';
			}
			//totalQty=parseFloat(totalQty)+quantity;
			msg += "	<tr> ";
msg += "<td  align=\"center\" border=\"0.1\" style=\"border-left-style:none;\" width=\"5%\" height=\"15\"><b><\/b><\/td> ";
msg += "<td  align=\"right\" border=\"0.1\"  width=\"40%\" height=\"15\" style=\"font-size:11;\"><b>Total<\/b><\/td> ";
msg += "<td  align=\"center\" border=\"0.1\" style=\" border-left-style:none;font-size:11;\"  width=\"5%\" height=\"15\">"+totalQty+"<\/td> ";
msg += "<td  align=\"center\" border=\"0.1\"  width=\"20%\" height=\"15\" style=\"font-size:11;\">"+totalWt+"<\/td> ";
msg += "<td  align=\"right\" border=\"0.1\"  width=\"15%\" height=\"15\" style=\"font-size:11;\">"+totalRate+"<\/td> ";
msg += "<td  align=\"right\" border=\"0.1\" style=\" border-left-style:none;font-size:11\" width=\"20%\" height=\"15\">"+totalAmount+"<\/td> ";
msg += "	<\/tr> ";
                 //amtInWods=totalInvoiceAmount(totalAmount);
		}  
		message=message+msg;
message += "<\/table> ";



//----------------------------------------------------4 line item-----------------------------------------------------//
//------------------------------------------------------5-------------------------------------------------------------//





message += "<table border=\"0.1\" width=\"100%\" height=\"2.5%\" style=\"border-collapse: collapse\"> ";
message += "	<tr> ";
message += "		<td width=\"70%\"><b align=\"left\">Amount Chargable <\/b><\/td>";   //<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Net Amount<\/b>&nbsp; "+netAmount+" ";

message += "		<td width=\"30%\" align=\"right\"><b border=\"0.0\" style=\"font-size:11;\">Net Amount: "+netAmount+"<\/b><\/td>"; 

////message += "		<p align='left'><b>"+Vatstring+"<\/b><\/p>";
//message += "		<b>&nbsp;&nbsp;&nbsp; Net Amount<\/b>&nbsp; "+netAmount+" &nbsp;&nbsp;&nbsp;&nbsp; ";
////message += "		<b>Remark:<\/b><\/td> ";
message += "	<\/tr> ";

message += "	<tr> ";
message += " 		<td><p align='left'><b>"+Vatstring+"<\/b><\/p><\/td>";
message += "	<\/tr> ";

message += "	<tr> ";
message += " 		<td><p align='left'><b>Remark:<\/b>"+remark+"<\/p><\/td>";
message += "	<\/tr> ";

message += "<\/table> ";

message += "<table border=\"0.1\" width=\"100%\" height=\"100\" style=\"border-collapse: collapse\"> ";
message += "	<tr> ";
message += "		<td align=\"left\" width=\"50%\"><b>VAT Regn. No.<\/b> :<\/td>";  //<\/b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "
message += "		<td align=\"right\"><b>FOR STARFIRE GEMS PVT LTD <\/b><\/td>";   //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "
//message += "		<b>Buyer's VAT Regn.No. :<\/b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
//message += "		<b>Declaration :<\/b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
////message += "		<td align=\"right\"><b>Authorized Signatory<\/b><\/td> ";
message += "	<\/tr> ";

message += "	<tr> ";
message += "		<td align=\"left\" width=\"50%\"><b>Buyer's VAT Regn.No.<\/b> :<\/td>";
//message += "		<td width=\"100%\"><\/td> ";

message += "	<\/tr> ";

message += "	<tr> ";
message += "		<td align=\"left\" width=\"50%\"><b>Declaration<\/b> :<\/td>";

//message += "		<td width=\"100%\"><\/td> ";
//message += "		<td width=\"100%\"><\/td> ";


message += "		<td align=\"right\"><b>Authorized Signatory<\/b><\/td> ";

message += "	<\/tr> ";

message += "<\/table> ";





//------------------------------------------------------5-------------------------------------------------------------//


//========================================================Line Item=======================================================//
	//nlapiLogExecution('DEBUG', 'printInvoice()', " message : " + message);
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">"+message+"</body>\n</pdf> ";
		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF','Invoice Print.pdf', 'inline');
		response.write( file.getValue() );


}



function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g   //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);

 return messgaeToBeSendPara;
}

function formatAndReplaceSpacesOnlyfMessage(messgaeToBeSendPara)
{

 messgaeToBeSendPara = messgaeToBeSendPara.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);

 messgaeToBeSendPara = messgaeToBeSendPara.replace(/ /g,"&nbsp");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);

 return messgaeToBeSendPara;
}

function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
{
 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);

 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&amp;");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);

 return messgaeToBeSendParaAnd;
}
function formatAndReplaceMessageForhash(messgaeToBeSendParaAnd)
{

 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);

 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/#/g,"&amp;");/// /g
 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);

 return messgaeToBeSendParaAnd;
}

//---------------------------------item Description and URL--------------------------------------------------//
				function getItemName(item)
				{
					var name = '';
					var Url = '';
					var description = '';
					if (item != '' && item != null) 
					{
						var Filters = new Array();
						var Column = new Array();
						
						Filters[0] = new nlobjSearchFilter('internalid', null, 'is', item)
						
						Column[0] = new nlobjSearchColumn('custitem_item_category');
						Column[1] = new nlobjSearchColumn('custitem_img_url');
						Column[2] = new nlobjSearchColumn('salesdescription');
						
						var result = nlapiSearchRecord('item', null, Filters, Column);
						nlapiLogExecution('DEBUG', 'In getItemName', 'search==' +result);
						if (result != null)
						{
							name = result[0].getText('custitem_item_category');
							if (name != null && name != '' && name != 'undefined') 
							{
								name = name;
							}
							else 
							{
								name = '';
							}
							nlapiLogExecution('DEBUG', 'ItemName', 'name = ' +name);
							
							Url = result[0].getValue('custitem_img_url');
							if (Url != null && Url != '' && Url != 'undefined') 
							{
								Url = Url;
							}
							else 
							{
								Url = '';
							}
							nlapiLogExecution('DEBUG', 'getItemUrl', 'Url = ' +Url);
							
							description = result[0].getValue('salesdescription');
							if (description != null && description != '' && description != 'undefined') 
							{
								description = formatAndReplaceMessageForAnd(description);
							}
							else 
							{
								description = '';
							}
							nlapiLogExecution('DEBUG', 'getItemUrl', 'description = ' +description);
						}
						
					}
					var ItemArry=new Array();
					
					ItemArry['name'] = name;
					ItemArry['Url'] = Url;
					ItemArry['description'] = description;
					
					return ItemArry;
				}	
				
				 			
 //---------------------------------item Description and URL--------------------------------------------------//	
//--------------------------------Amount in words----------------------------------------------------------------//

function toWordsFunc(s)
{
   // alert('In conversion function')
    var str='Rs.'

    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');

// uncomment this line for English Number System

// var th = ['','thousand','million', 'milliard','billion'];
	var dg = new Array ('10000000','100000','1000','100');

	var dem=s.substr(s.lastIndexOf('.')+1)

		//lastIndexOf(".")
		//alert(dem)
            s=parseInt(s)
            //alert('passed value'+s)
            var d
            var n1,n2
            while(s>=100)
            {
                       for(var k=0;k<4;k++)
                        {
                                    //alert('s ki value'+s)
                                    d=parseInt(s/dg[k])
                                    //alert('d='+d)
                                    if(d>0)
                                    {
                                                if(d>=20)
                                                {
                   //alert ('in 2nd if ')
                                                            n1=parseInt(d/10)
                                                            //alert('n1'+n1)
                                                            n2=d%10
                                                            //alert('n2'+n2)
                                                            printnum2(n1)
                                                            printnum1(n2)
                                                }
                                     			 else
                                      				printnum1(d)
		                             			str=str+th[k]
                           			 }
                            		s=s%dg[k]
                        }
            }
			 if(s>=20)
			 {
			            n1=parseInt(s/10)
			            n2=s%10
			 }
			 else
			 {
			            n1=0
			            n2=s
			 }

			printnum2(n1)
			printnum1(n2)
			if(dem>0)
			{
				decprint(dem)
			}
			return str

			function decprint(nm)
			{
			       // alert('in dec print'+nm)
			             if(nm>=20)
					     {
			                n1=parseInt(nm/10)
			                n2=nm%10
					     }
						  else
						  {
						              n1=0
						              n2=parseInt(nm)
						  }
						     //alert('n2=='+n2)
						  str=str+'And '

						  printnum2(n1)

					 	  printnum1(n2)
			}

			function printnum1(num1)
			{
			            //alert('in print 1'+num1)
			            switch(num1)
			            {
							  case 1:str=str+'One '
							         break;
							  case 2:str=str+'Two '
							         break;
							  case 3:str=str+'Three '
							        break;
							  case 4:str=str+'Four '
							         break;
							  case 5:str=str+'Five '
							         break;
							  case 6:str=str+'Six '
							         break;
							  case 7:str=str+'Seven '
							         break;
							  case 8:str=str+'Eight '
							         break;
							  case 9:str=str+'Nine '
							         break;
							  case 10:str=str+'Ten '
							         break;
							  case 11:str=str+'Eleven '
							        break;
							  case 12:str=str+'Twelve '
							         break;
							  case 13:str=str+'Thirteen '
							         break;
							  case 14:str=str+'Fourteen '
							         break;
							  case 15:str=str+'Fifteen '
							         break;
							  case 16:str=str+'Sixteen '
							         break;
							  case 17:str=str+'Seventeen '
							         break;
							  case 18:str=str+'Eighteen '
							         break;
							  case 19:str=str+'Nineteen '
							         break;
						}
			}

			function printnum2(num2)
			{
			           // alert('in print 2'+num2)
				switch(num2)
				{
						  case 2:str=str+'Twenty '
						         break;
						  case 3:str=str+'Thirty '
						        break;
						  case 4:str=str+'Forty '
						         break;
						  case 5:str=str+'Fifty '
						         break;
						  case 6:str=str+'Sixty '
						         break;
						  case 7:str=str+'Seventy '
						         break;
						  case 8:str=str+'Eighty '
						         break;
						  case 9:str=str+'Ninety '
						         break;
	            }
				           // alert('str in loop2'+str)
			}
}

//---------------------------------------------------------------------------------------Amount in Words----------------------------------//
