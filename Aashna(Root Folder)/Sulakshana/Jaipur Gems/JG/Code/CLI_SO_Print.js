// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Client script for Quotation print 
	Author:Sulakshana Ghadage
	Company:JG
	Date:10-10-2012
    Description:Call suitlet from client script


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================

/*
function callPrintDeliveryOrderSuitelets()
{
	//alert('In Call Print Delivery Order Script');
	var currentIFID = nlapiGetRecordId();
	window.open('/app/site/hosting/scriptlet.nl?script=11&deploy=1&CurrentIFRecordID='+currentIFID);
	
}

function callPrintProFormaInvoiceSuitelets()
{
	//alert('In Call Print Pro-Forma Invoice Script');
	var currentIFID = nlapiGetRecordId();
	window.open('/app/site/hosting/scriptlet.nl?script=13&deploy=1&CurrentIFRecordID='+currentIFID);
	
}

function callPrintPackingSlipSuitelets()
{
	
	var currentIFID = nlapiGetRecordId();
	window.open('/app/site/hosting/scriptlet.nl?script=21&deploy=1&CurrentIFRecordID='+currentIFID);
	
}


*/
function callPrintSoSuitelets(SOid)
{
	//alert('I am in function first');
	//Sandbox Setup/var urlSut ='https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=94&deploy=1&custscript2='+SOid;
	//Live Setup
	
	
	
	var temp = SOid.split('#');
	SOid = temp[0];
	//	https://forms.netsuite.com/app/site/hosting/scriptlet.nl?script=98&deploy=1&compid=852584&h=79e76ea4d5a932d642a8
	var urlSut ='https://forms.netsuite.com/app/site/hosting/scriptlet.nl?script=98&deploy=1&compid=852584&h=79e76ea4d5a932d642a8&custscript_soidvalue='+SOid;   
	       
	popitup(urlSut);
	//newwindow=window.open(urlSut);
	//alert(" urlSut == "+urlSut);
	//newwindow=window.open(urlSut);
	//alert ("Finally I have done it !!!!!!!!!")
		
		
}


function popitup(urlSut) 
{
 newwindow=window.open(urlSut,'print','height=700,width=650');
}





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
