// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Suitlet for Quotation print
	Author:Sulakshana Ghadage	
	Company:Jaipur Gems
	Date:15-10-2012
	Description:Design of HTML is Added in Suitlet and All entries of form getting  
	from JH_Job History,which is get into the Suitlet.



	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- printJH(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



/*
// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...
	var totalLossWt=0;
	var totalLossPer=0;
	var totalAlnlpercent=0;
	var totalScrap=0;
	var totalUseWt=0;
	var totalUsePcs=0;


}
// END GLOBAL VARIABLE BLOCK  =======================================
*/

function printJH(request,response)
{
		var internalId;
		
		var JHInternalId = request.getParameter('custscript_jhrecordid');
		nlapiLogExecution('DEBUG', 'printJH', " JH InternalId :" +JHInternalId);
		
		var recObj = nlapiLoadRecord('customrecord_jobhistory', JHInternalId);
		nlapiLogExecution('DEBUG', 'printJH', " recObj :" + recObj);
      
	   //--------------------------------------Job No-------------------------------------------//
		 var jobNo = recObj.getFieldValue('name');
		 if(jobNo!=null && jobNo!=''&& jobNo!='undefined')
		 {
		 jobNo = jobNo;
		 }
		 else
		 {
		 jobNo = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Job No:" +jobNo);
	//--------------------------------------Job No-------------------------------------------//
	  
	  //--------------------------------------Order No-------------------------------------------//
		 var OrderNo = recObj.getFieldValue('custrecord_ordno');
		 if(OrderNo!=null && OrderNo!=''&& OrderNo!='undefined')
		 {
		 OrderNo = OrderNo;
		 }
		 else
		 {
		 OrderNo = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Sales Order No:" +OrderNo);
	//--------------------------------------Order No-------------------------------------------//
	
	//-------------------------------------SO date----------------------------------------------//
		 var SODate = recObj.getFieldValue('custrecord_orddt');
		 if(SODate!=null && SODate!=''&& SODate!='undefined')
		 {
		 SODate = SODate;
		 }
		 else
		 {
		 SODate = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Sales Order Date:" +SODate);
	//-------------------------------------SO date----------------------------------------------//
	
	//-------------------------------------PSC----------------------------------------------//
		 var psc = recObj.getFieldValue('custrecord_pcss');
		 if(psc!=null && psc!=''&& psc!='undefined')
		 {
		 psc = psc;
		 }
		 else
		 {
		 psc = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "psc:" +psc);
	//-------------------------------------PSC----------------------------------------------// 
	
	//-------------------------------------DEL date----------------------------------------------//
		 var delDate = recObj.getFieldValue('custrecord_deldt');
		 if(delDate!=null && delDate!=''&& delDate!='undefined')
		 {
		 delDate = delDate;
		 }
		 else
		 {
		 delDate = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Delivery Date:" +delDate);
	//-------------------------------------DEL date----------------------------------------------// 
	
	//-------------------------------------Ref.No----------------------------------------------//
		 var refNo= recObj.getFieldValue('custrecord_refno');
		 if(refNo!=null && refNo!=''&& refNo!='undefined')
		 {
		 refNo = refNo;
		 }
		 else
		 {
		 refNo = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Ref.No:" +refNo);
	//-------------------------------------Ref.No----------------------------------------------//
	
	//-------------------------------------Client----------------------------------------------//
		 var client= recObj.getFieldText('custrecord_client');
		 nlapiLogExecution('DEBUG', 'printJH', "client:" +client);
		 var strclient =client.split(':')
		 strclient[0];
		 strclient[1];
		 if(strclient[1]!=null && strclient[1]!=''&& strclient[1]!='undefined')
		 {
		 strclient[1] = strclient[1];
		 }
		 else
		 {
		 strclient[1] = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "strclient:" +strclient[1]);
	//-------------------------------------Client----------------------------------------------//
	
	//-------------------------------------Pro date----------------------------------------------//
		 var ProdDate = recObj.getFieldValue('custrecord_proddt');
		 if(ProdDate!=null && ProdDate!=''&& ProdDate!='undefined')
		 {
		 ProdDate = ProdDate;
		 }
		 else
		 {
		 ProdDate = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "Product Date:" +ProdDate);
	//-------------------------------------Pro date----------------------------------------------// 
	
	//-------------------------------------Remark Footer----------------------------------------------//
		 var remarkFooter = recObj.getFieldValue('custrecord_remarkfooter');
		 if(remarkFooter!=null && remarkFooter!=''&& remarkFooter!='undefined')
		 {
		 remarkFooter = remarkFooter;
		 }
		 else
		 {
		 remarkFooter = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "remarkFooter:" +remarkFooter);
	//-------------------------------------Remark footer----------------------------------------------// 
	
	//-------------------------------------Image----------------------------------------------//
		 var jImage = recObj.getFieldValue('custrecord_itemimage');
		 if(jImage!=null && jImage!=''&& jImage!='undefined')
		 {
		 jImage = nlapiEscapeXML(jImage);
		 }
		 else
		 {
		 jImage = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "JH Image:" +jImage);
	//-------------------------------------Image----------------------------------------------// 
	
	//-------------------------------------Image----------------------------------------------//
		 var urlImage = recObj.getFieldValue('custrecord_imageurl');
		 if(urlImage!=null && urlImage!=''&& urlImage!='undefined')
		 {
		 urlImage = nlapiEscapeXML(urlImage);
		 }
		 else
		 {
		 urlImage = '';
		 }
		 nlapiLogExecution('DEBUG', 'printJH', "JH Image:" +urlImage);
	//-------------------------------------Image----------------------------------------------// 
	
	//-------------------------------------Body Field Value--------------------------------------//
	
		var message="";
		message +="<html>";
		message += "<body font-size='7'>";
		message += "<table border='1' width='100%' style='border-collapse: collapse' cellpadding='0' cellspacing='0' >";
		message += "	<tr>";
		message += "	<td>";
		message += "<table border='0.1' width='100%'>";
		message += "	<tr>";
		message += "		<td colspan='5'  style='font-size: 14pt;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		message += "		<b>JOB HISTORY</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		message += "	</tr>";
		message += "	<br/>";
		message += "	<tr>";
		message += "		<td rowspan='2' width='19%' style='font-size: 10pt;'><b>JOB NO</b>&nbsp;&nbsp; "+jobNo+"</td>";
		message += "		<td width='16%' style='font-size: 12pt;'>&nbsp;</td>";
		message += "		<td width='17%' style='font-size: 10pt;'><b>ORD NO :</b> "+OrderNo+"</td>";
		message += "		<td width='20%' style='font-size: 10pt;'><b>ORD DT :</b> "+SODate+"</td>";
		message += "		<td width='26%' style='font-size: 10pt;'><b>DEL DT :</b> "+delDate+"</td>";
		message += "	</tr>";
		message += "	<tr>";
		message += "		<td width='16%' style='font-size: 10pt;'><b>PCS </b> "+psc+"</td>";
		message += "		<td width='17%' style='font-size: 10pt;'><b>REF NO :</b> "+refNo+"</td>";
		message += "		<td width='20%' style='font-size: 10pt;'><b>CLIENT :</b> "+strclient[1]+"</td>";
		message += "		<td width='26%' style='font-size: 10pt;'><b>PROD DT :</b> "+ProdDate+"</td>";
		message += "	</tr>";
		message += "</table>";
		//------------------------------------------------HTML Issue Details----------------------------------------------------//
		
		message += "<table border='1' width='100%' style='border-collapse: collapse'>";
		message += "	<tr>";
		message += "		<td width='3%'  border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='3%'  border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='6%'  border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td colspan='7' align='center' border='0.1' style='font-size: 11pt;'><b>ISSUE DETAILS</b></td>";
		message += "		<td width='45%' colspan='11' align='center' border='0.1' style='font-size: 11pt;'><b>RETURN DETAILS</b></td>";
		message += "	</tr>";
	
	    message += "	<tr>";
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>JOB</b></td>"; 
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>PRO</b></td>";
		message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>WORKER</b></td>";
		message += "		<td width='9%' border='0.1' style='border-right-style:none;border-top-style:none;font-size: 10pt;'><b>DATE</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;' ><b>PCS</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>G WT</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>N WT</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>ST WT</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>ACC WT</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>EXT WT</b></td>";
		message += "		<td width='9%' border='0.1' style='border-right-style:none;border-top-style:none;font-size: 10pt;'><b>DATE</b></td>";
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>PCS</b></td>";
		message += "		<td width'=4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>G WT</b></td>";
		message += "		<td width='4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>N WT</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>REJ WT</b></td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>REJ PCS</b></td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>LOSS WT</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>LOSS%</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>ALW L%</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>SCRAP</b></td>";
		message += "		<td width='4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>DUST</b></td>";
		message += "	</tr>";
	   
		
	
	//------------------------------------------------HTML Issue Details----------------------------------------------------//
	//------------------------------------------------Issue Details--------------------------------------------------------//
	
	var issueDetailsCount=recObj.getLineItemCount('recmachcustrecord_job');
	nlapiLogExecution('DEBUG','JH Print', " issue Details Count = " +issueDetailsCount);
	var totalLossWt=0;
	var totalLossPer=0;
	var totalAlnlpercent=0;
	var totalScrap=0;
	if (issueDetailsCount != null && issueDetailsCount != '' && issueDetailsCount != 'undefined') 
	{
		
		for (var i = 1; i <= issueDetailsCount; i++) 
		{
			
			//----------------------------------------job----------------------------------------------------------//
			 var jobNo = recObj.getFieldValue('name');
			 if(jobNo!=null && jobNo!=''&& jobNo!='undefined')
			 {
			 jobNo = jobNo;
			 }
			 else
			 {
			 jobNo = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "Job No:" +jobNo);
			//----------------------------------------job----------------------------------------------------------//
			
			//----------------------------------------PRO---------------------------------------------------------//
			var pro = recObj.getLineItemText('recmachcustrecord_job', 'custrecord_pro', i);
			nlapiLogExecution('DEBUG', 'printJH', "Pro :" +pro);
			if (pro != null && pro != '' && pro != 'undefined') 
			{
				pro = pro;
			}
			else 
			{
				pro = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "Pro :" +pro);
			//----------------------------------------PRO---------------------------------------------------------//
			
			//------------------------------------------worker-------------------------------------------------------------//
			var worker = recObj.getLineItemText('recmachcustrecord_job', 'custrecord_worker', i);
			nlapiLogExecution('DEBUG', 'printJH', "worker :" + worker);
			if (worker != null && worker != '' && worker != 'undefined') {
				worker = worker;
			}
			else 
			{
				worker = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " worker :" + worker);
			//------------------------------------------worker-------------------------------------------------------------//
			
			//---------------------------------------issue Date-------------------------------------------------------//
			var issueDate = recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_issuedate', i);
			nlapiLogExecution('DEBUG', 'printJH', "Issue Date :" +issueDate);
			if (issueDate != null && issueDate != '' && issueDate != 'undefined')  
			{
				issueDate = issueDate;
			}
			else 
			{
				issueDate = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " Issue Date :" +issueDate);
			//---------------------------------------issue Date-------------------------------------------------------//
			
			//---------------------------------------PCS Issue-------------------------------------------------------//
			var pcsissue= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_pcsissue', i);
			nlapiLogExecution('DEBUG', 'printJH', "pcs issue  :" +pcsissue);
			if (pcsissue != null && pcsissue != '' && pcsissue != 'undefined')  
			{
				pcsissue = pcsissue;
			}
			else 
			{
				pcsissue = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " pcsissue :" +pcsissue);
			//---------------------------------------PCS Issue-------------------------------------------------------//
			
			//---------------------------------------G WT------------------------------------------------------------//
			var GWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_gwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "G Wt  :" +GWt);
			if (GWt != null && GWt != '' && GWt != 'undefined')  
			{
				GWt = GWt;
			}
			else 
			{
				GWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " G Wt :" +GWt);
			//---------------------------------------G WT------------------------------------------------------------//
			
			//---------------------------------------N WT------------------------------------------------------------//
			var NWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_nwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "N Wt  :" +NWt);
			if (NWt != null && NWt != '' && NWt != 'undefined')  
			{
				NWt = NWt;
			}
			else 
			{
				NWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "N Wt :" +NWt);
			//---------------------------------------N WT------------------------------------------------------------//
			
			//---------------------------------------ST WT------------------------------------------------------------//
			var STWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_stwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "ST Wt  :" +STWt);
			if (STWt != null && STWt != '' && STWt != 'undefined')  
			{
				STWt = STWt;
			}
			else 
			{
				STWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "ST Wt :" +STWt);
			//---------------------------------------ST WT------------------------------------------------------------//
			
			//---------------------------------------Acc WT------------------------------------------------------------//
			var AccWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_accwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "Acc Wt  :" +AccWt);
			if (AccWt != null && AccWt != '' && AccWt != 'undefined')  
			{
				AccWt = AccWt;
			}
			else 
			{
				AccWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "AccWt :" +AccWt);
			//---------------------------------------ST WT------------------------------------------------------------//
			
			//---------------------------------------Ext Wt------------------------------------------------------------//
			var ExtWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_extwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "Ext Wt  :" +ExtWt);
			if (ExtWt != null && ExtWt != '' && ExtWt != 'undefined')  
			{
				ExtWt = ExtWt;
			}
			else 
			{
				ExtWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "Ext Wt :" +ExtWt);
			//---------------------------------------Ext Wt-----------------------------------------------------------//
	//===================================================return Details====================================================//
			//---------------------------------------Return Date------------------------------------------------------------//
			var returnDate= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_returndate', i);
			nlapiLogExecution('DEBUG', 'printJH', "Return Date  :" +returnDate);
			if (returnDate != null && returnDate != '' && returnDate != 'undefined')  
			{
				returnDate = returnDate;
			}
			else 
			{
				returnDate = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "returnDate :" +returnDate);
			//---------------------------------------Return Date-----------------------------------------------------------//
			
			//---------------------------------------PCS Return------------------------------------------------------//
			var pcsReturn= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_pcsreturn', i);
			nlapiLogExecution('DEBUG', 'printJH', "pcs Return  :" +pcsReturn);
			if (pcsReturn != null && pcsReturn != '' && pcsReturn != 'undefined')  
			{
				pcsReturn = pcsReturn;
			}
			else 
			{
				pcsReturn = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " pcsReturn :" +pcsReturn);
			//---------------------------------------PCS Return-------------------------------------------------------//
			
			//---------------------------------------G return WT------------------------------------------------------------//
			var GReturnWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_gwtreturn', i);
			nlapiLogExecution('DEBUG', 'printJH', "G Return Wt  :" +GReturnWt);
			if (GReturnWt != null && GReturnWt != '' && GReturnWt != 'undefined')  
			{
				GReturnWt = GReturnWt;
			}
			else 
			{
				GReturnWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " G Return Wt :" +GReturnWt);
			//---------------------------------------G return WT------------------------------------------------------------//
			
			//---------------------------------------N return WT------------------------------------------------------------//
			var NReturnWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_nwtreturn', i);
			nlapiLogExecution('DEBUG', 'printJH', "N Return Wt  :" +NReturnWt);
			if (NReturnWt != null && NReturnWt != '' && NReturnWt != 'undefined')  
			{
				NReturnWt = NReturnWt;
			}
			else 
			{
				NReturnWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "N Return Wt :" +NReturnWt);
			//---------------------------------------N return WT------------------------------------------------------------//
			
			//---------------------------------------Reject Wt-----------------------------------------------------//
			var rejwt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_rejwt', i);
			nlapiLogExecution('DEBUG', 'printJH', "Rej wt  :" +rejwt);
			if (rejwt != null && rejwt != '' && rejwt != 'undefined')  
			{
				rejwt = rejwt;
			}
			else 
			{
				rejwt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " rejwt :" +rejwt);
			//---------------------------------------Reject Wt-------------------------------------------------------//
			
			//---------------------------------------Reject PCS------------------------------------------------------//
			var rejpsc= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_rejpcs', i);
			nlapiLogExecution('DEBUG', 'printJH', "rej psc  :" +rejpsc);
			if (rejpsc != null && rejpsc != '' && rejpsc != 'undefined')  
			{
				rejpsc = rejpsc;
			}
			else 
			{
				rejpsc = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " rej psc :" +rejpsc);
			//----------------------------------------Reject PCS-------------------------------------------------------//
			
			//---------------------------------------Loss Wt------------------------------------------------------//
			var lossWt= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_losswt', i);
			nlapiLogExecution('DEBUG', 'printJH', "loss Wt  :" +lossWt);
			if (lossWt != null && lossWt != '' && lossWt != 'undefined')  
			{
				lossWt = lossWt;
			}
			else 
			{
				lossWt = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " lossWt :" +lossWt);
			//----------------------------------------Loss Wt-------------------------------------------------------//
			//----------------------------------------Total Loss Wt--------------------------------------------------//
				totalLossWt= parseFloat(totalLossWt)+ parseFloat(lossWt);
				nlapiLogExecution('DEBUG', 'printJH', " Total Loss Wt : " +totalLossWt);
				if (totalLossWt!= null && totalLossWt!= '' && totalLossWt!= 'undefined') 
				{
					totalLossWt= totalLossWt;
				}
				else 
				{
					totalLossWt= 0;
				}
				nlapiLogExecution('DEBUG', 'printJH', " total Loss Wt : " + totalLossWt);
			//----------------------------------------Total Loss Wt--------------------------------------------------//
			//---------------------------------------Loss %------------------------------------------------------//
			var losspercent= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_losspercent', i);
			nlapiLogExecution('DEBUG', 'printJH', "loss percent :" +losspercent);
			if (losspercent != null && losspercent != '' && losspercent != 'undefined')  
			{
				losspercent = losspercent;
			}
			else 
			{
				losspercent = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " loss percent :" +losspercent);
			//----------------------------------------Loss %-------------------------------------------------------//
			
			//---------------------------------------Total Loss %--------------------------------------------------//
			totalLossPer= parseFloat(totalLossPer)+ parseFloat(losspercent);
			nlapiLogExecution('DEBUG', 'printJH', " total Loss Per : " +totalLossPer);
			if (totalLossPer!= null && totalLossPer!= '' && totalLossPer!= 'undefined') 
			{
				totalLossPer= totalLossPer;
			}
			else 
			{
				totalLossPer= 0;
			}
			nlapiLogExecution('DEBUG', 'printJH', " Total Loss Per : " +totalLossPer);
			//---------------------------------------Total Loss %--------------------------------------------------//
			
			//---------------------------------------Alnlpercent------------------------------------------------------//
			var alnlpercent= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_alnlpercent', i);
			nlapiLogExecution('DEBUG', 'printJH', "alnl percent :" +alnlpercent);
			if (alnlpercent != null && alnlpercent != '' && alnlpercent != 'undefined')  
			{
				alnlpercent = alnlpercent;
			}
			else 
			{
				alnlpercent = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " alnl percent :" +alnlpercent);
			//----------------------------------------Alnlpercent-------------------------------------------------------//
			//----------------------------------------Total Alnlpercent-----------------------------------------------//
			totalAlnlpercent=parseFloat(totalAlnlpercent)+ parseFloat(alnlpercent);
			nlapiLogExecution('DEBUG', 'printJH', " total Loss Per : " +totalAlnlpercent);
			if (totalAlnlpercent!= null && totalAlnlpercent!= '' && totalAlnlpercent!= 'undefined') 
			{
				totalAlnlpercent= totalAlnlpercent;
			}
			else 
			{
				totalAlnlpercent= 0;
			}
			nlapiLogExecution('DEBUG', 'printJH', " Total Alnlpercent : " +totalAlnlpercent);
			//----------------------------------------Total Alnlpercent-----------------------------------------------//
			
			//---------------------------------------Scrap------------------------------------------------------//
			var scrap= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_scrap', i);
			nlapiLogExecution('DEBUG', 'printJH', "Scrap :" +scrap);
			if (scrap != null && scrap != '' && scrap != 'undefined')  
			{
				scrap = scrap;
			}
			else 
			{
				scrap = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "Scrap :" +scrap);
			//----------------------------------------Scrap-------------------------------------------------------//
			//----------------------------------------Total Scrap-------------------------------------------------//
			totalScrap=parseFloat(totalScrap)+ parseFloat(scrap);
			nlapiLogExecution('DEBUG', 'printJH', " total Loss Per : " +totalScrap);
			if (totalScrap!= null && totalScrap!= '' && totalScrap!= 'undefined') 
			{
				totalScrap= totalScrap;
			}
			else 
			{
				totalScrap= 0;
			}
			nlapiLogExecution('DEBUG', 'printJH', " Total Scrap : " +totalScrap);
			//----------------------------------------Total Scrap-------------------------------------------------//
			//---------------------------------------Scrap--------------------------------------------------------//
			var dust= recObj.getLineItemValue('recmachcustrecord_job', 'custrecord_dust', i);
			nlapiLogExecution('DEBUG', 'printJH', "Dust :" +dust);
			if (dust != null && dust != '' && dust != 'undefined')  
			{
				dust = dust;
			}
			else 
			{
				dust = '';
			}
			nlapiLogExecution('DEBUG', 'printJH', "Dust :" +dust);
			//----------------------------------------Scrap-------------------------------------------------------//
			
		message += "<tr>";
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+jobNo+"</td>";
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+pro+"</td>";
		message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+worker+"</td>";
		message += "		<td width='9%' border='0.1'  style='border-right-style:none;border-top-style:none;font-size: 7pt;'>"+issueDate+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+pcsissue+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+GWt+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+NWt+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+STWt+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+AccWt+"</td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+ExtWt+"</td>";
		message += "		<td width='9%' border='0.1'  style='border-right-style:none;font-size: 7pt;'>"+returnDate+"</td>";
		message += "		<td width='2%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+pcsReturn+"</td>";
		message += "		<td width='4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+GReturnWt+"</td>";
		message += "		<td width='4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+NReturnWt+"</td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+rejwt+"</td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+rejpsc+"</td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+lossWt+"</td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+losspercent+"</td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+alnlpercent+"</td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+scrap+"</td>";
		message += "		<td width='4%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+dust+"</td>";
		message += "	</tr>";
		
		
	
				
		//===================================================return Details====================================================//		
				
			}
		}
		message += "	<tr>";
		message += "		<td width='3%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='3%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='3%' border='0.1' style='border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='3%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='4%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='4%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='5%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='5%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "		<td width='25%' colspan='6' border='0.1' style='border-right-style:none;font-size: 10pt;' ><b>TOTAL</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;font-size: 10pt;'><b>"+totalLossWt+"</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;font-size: 10pt;'><b>"+totalLossPer+"</b></td>";
		message += "		<td width='5%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;font-size: 10pt;'><b>"+totalAlnlpercent+"</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;font-size: 10pt;'><b>"+totalScrap+"</b></td>";
		message += "		<td width='3%'border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
		message += "	</tr>";
		message += "</table>";		
		
	    //------------------------------------------------Issue Details--------------------------------------------------------//
		//----------------------------------------------HTML STONE DETAIL --------------------------------------------------//
		
		message += "<table border='1' width='100%' style='border-collapse: collapse' >";
		message += "	<tr>";
		message += "		<td colspan='13' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 11pt;'><b>STONE DETAILS</b></td>";
		message += "	</tr>";
		message += "	<tr>";
		message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>SSKU</b></td>";
		message += "		<td width='12%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>Description</b></td>";
		message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>SIZE</b></td>";
		message += "		<td width='8%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>WT RANGE</b></td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>WT/PCS</b></td>";
		message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>USE WT</b></td>";
		message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>USE PCS</b></td>";
		message += "		<td width='8%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>BREAK WT</b></td>";
		message += "		<td width='9%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>BREAK PCS</b></td>";
		message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>LOST WT</b></td>";
		message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>LOST PCS</b></td>";
		message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>S TYPE</b></td>";
		message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'><b>REMARK</b></td>";
		message += "	</tr>";
		
	
	//------------------------------------------------STONE Details--------------------------------------------------------//
	var stoneDetailsCount=recObj.getLineItemCount('recmachcustrecord_job1');
	nlapiLogExecution('DEBUG','JH Print', " stone Details Count = " +stoneDetailsCount);
	var totalUseWt=0;
	var totalUsePcs=0;
	if (stoneDetailsCount != null && stoneDetailsCount != '' && stoneDetailsCount != 'undefined') 
	{
		for (var i = 1; i <= stoneDetailsCount; i++) 
		{
		//----------------------------------------ssku details----------------------------------------------------------//
			 var sskudetails = recObj.getLineItemText('recmachcustrecord_job1','custrecord_sskudetails',i);
			 if(sskudetails!=null && sskudetails!=''&& sskudetails!='undefined')
			 {
			 sskudetails = sskudetails;
			 }
			 else
			 {
			 sskudetails = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "ssku Details:" +sskudetails);
			//----------------------------------------ssku details----------------------------------------------------------//
			
			//----------------------------------------Description----------------------------------------------------------//
			 var descrptn = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_descriptionstonedetails',i);
			 if(descrptn!=null && descrptn!=''&& descrptn!='undefined')
			 {
			 descrptn = descrptn;
			 }
			 else
			 {
			 descrptn = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "Description:" +descrptn);
			//----------------------------------------Description----------------------------------------------------------//
			
			//----------------------------------------Size----------------------------------------------------------//
			 var size = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_size',i);
			 if(size!=null && size!=''&& size!='undefined')
			 {
			 size = size;
			 }
			 else
			 {
			 size = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "Size:" +size);
			//----------------------------------------Size----------------------------------------------------------//
			
			//----------------------------------------WT RANGE----------------------------------------------------------//
			 var wtrange = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_wtrange',i);
			 if(wtrange!=null && wtrange!=''&& wtrange!='undefined')
			 {
			 wtrange = wtrange;
			 }
			 else
			 {
			 wtrange = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "WT RANGE :" +wtrange);
			//----------------------------------------WT RANGE----------------------------------------------------------//
			
			//----------------------------------------WT PSC----------------------------------------------------------//
			 var wtpsc = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_wtpcs',i);
			 if(wtpsc!=null && wtpsc!=''&& wtpsc!='undefined')
			 {
			 wtpsc = wtpsc;
			 }
			 else
			 {
			 wtpsc = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "WT PSC :" +wtpsc);
			//----------------------------------------WT PSC----------------------------------------------------------//
			
			//----------------------------------------USE WT----------------------------------------------------------//
			 var usewt = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_usewt',i);
			 if(usewt!=null && usewt!=''&& usewt!='undefined')
			 {
			 usewt = usewt;
			 }
			 else
			 {
			 usewt = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "USE WT :" +usewt);
			//----------------------------------------USE WT----------------------------------------------------------//
			//----------------------------------------Total Use WT---------------------------------------------------//
			totalUseWt=parseFloat(totalUseWt)+ parseFloat(usewt);
			nlapiLogExecution('DEBUG', 'printJH', " total Loss Per : " +totalUseWt);
			if (totalUseWt!= null && totalUseWt!= '' && totalUseWt!= 'undefined') 
			{
				totalUseWt= totalUseWt;
			}
			else 
			{
				totalUseWt= '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " Total UseWt : " +totalUseWt);
			//----------------------------------------Total Use WT---------------------------------------------------//
			
			//----------------------------------------USE PSC----------------------------------------------------------//
			 var usepcs = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_usepcs',i);
			 if(usepcs!=null && usepcs!=''&& usepcs!='undefined')
			 {
			 usepcs = usepcs;
			 }
			 else
			 {
			 usepcs = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "USE PSC :" +usepcs);
			//----------------------------------------USE PSC----------------------------------------------------------//
			//--------------------------------------Total Use PCS------------------------------------------------------//
			totalUsePcs=parseFloat(totalUsePcs)+ parseFloat(usepcs);
			nlapiLogExecution('DEBUG', 'printJH', " total Use Pcs : " +totalUsePcs);
			if (totalUsePcs!= null && totalUsePcs!= '' && totalUsePcs!= 'undefined') 
			{
				totalUsePcs= totalUsePcs;
			}
			else 
			{
				totalUsePcs= '';
			}
			nlapiLogExecution('DEBUG', 'printJH', " total Use Pcs: " +totalUsePcs);
			//--------------------------------------Total Use PCS------------------------------------------------------//
			//----------------------------------------BREAK WT----------------------------------------------------------//
			 var breakwt = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_breakwt',i);
			 if(breakwt!=null && breakwt!=''&& breakwt!='undefined')
			 {
			 breakwt = breakwt;
			 }
			 else
			 {
			 breakwt = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "BREAK WT :" +breakwt);
			//----------------------------------------BREAK WT----------------------------------------------------------//
			
			//----------------------------------------BREAK PCS----------------------------------------------------------//
			 var breakpcs = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_breakpcs',i);
			 if(breakpcs!=null && breakpcs!=''&& breakpcs!='undefined')
			 {
			 breakpcs = breakpcs;
			 }
			 else
			 {
			 breakpcs = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "BREAK PCS :" +breakpcs);
			//----------------------------------------BREAK PCS----------------------------------------------------------//
			
			//----------------------------------------LOSS WT----------------------------------------------------------//
			 var stonelosswt = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_lostwt',i);
			 if(stonelosswt!=null && stonelosswt!=''&& stonelosswt!='undefined')
			 {
			 stonelosswt = stonelosswt;
			 }
			 else
			 {
			 stonelosswt = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "Stone Loss wt :" +stonelosswt);
			//----------------------------------------LOSS WT----------------------------------------------------------//
			
			//----------------------------------------LOSS PCS----------------------------------------------------------//
			 var stoneLostPcs = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_lostpcs',i);
			 if(stoneLostPcs!=null && stoneLostPcs!=''&& stoneLostPcs!='undefined')
			 {
			 stoneLostPcs = stoneLostPcs;
			 }
			 else
			 {
			 stoneLostPcs = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "LOST PCS :" +stoneLostPcs);
			//----------------------------------------LOSS PCS----------------------------------------------------------//
			
			//----------------------------------------S TYPE----------------------------------------------------------//
			 var stype = recObj.getLineItemText('recmachcustrecord_job1','custrecord_stype',i);
			 if(stype!=null && stype!=''&& stype!='undefined')
			 {
			 stype = stype;
			 }
			 else
			 {
			 stype = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "S TYPE :" +stype);
			//----------------------------------------S TYPE----------------------------------------------------------//
			
			//----------------------------------------REMARK----------------------------------------------------------//
			 var remark = recObj.getLineItemValue('recmachcustrecord_job1','custrecord_remark',i);
			 if(remark!=null && remark!=''&& remark!='undefined')
			 {
			 remark = remark;
			 }
			 else
			 {
			 remark = '';
			 }
			 nlapiLogExecution('DEBUG', 'printJH', "REMARK :" +remark);
			//----------------------------------------REMARK---------------------------------------------------------//
			
			message += "<tr>";
			message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+sskudetails+"</td>";
			message += "		<td width='12%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+descrptn+"</td>";
			message += "		<td width='3%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+size+"</td>";
			message += "		<td width='8%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+wtrange+"</td>";
			message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+wtpsc+"</td>";
			message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+usewt+"</td>";
			message += "		<td width='6%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+usepcs+"</td>";
			message += "		<td width='8%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+breakwt+"</td>";
			message += "		<td width='9%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+breakpcs+"</td>";
			message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+stonelosswt+"</td>";
			message += "		<td width='7%' border='0.1' align='center' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+stoneLostPcs+"</td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+stype+"</td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;border-top-style:none;font-size: 10pt;'>"+remark+"</td>";
			message += "	</tr>";
			
	
			}
		}
		
			message += "<tr>";
			message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
			message += "		<td width='12%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
			message += "		<td width='3%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
			message += "		<td width='8%'border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'><b>TOTAL</b></td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;'>&nbsp;</td>";
			message += "		<td width='7%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;' align='center'><b>"+totalUseWt+"</b></td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;' align='center'><b>"+totalUsePcs+"</b></td>";
			message += "		<td width='8%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "		<td width='9%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "		<td width='7%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "		<td width='7%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "		<td width='6%' border='0.1' style='border-left-style:none;border-right-style:none;font-size: 10pt;'>&nbsp;</td>";
			message += "	</tr>";
			message += "</table>";
			message += "";
			message += "";
			message += "<p>&nbsp;</p>";
			message += "";
			message += "<table border='0.1' width='100%' style='border-left-style:none;border-right-style:none;border-top-style:none;border-bottom-style:none;'>";
			message += "	<tr>";
			message += "		<td style='font-size: 7pt;'><b>REMARK :</b> <b>"+remarkFooter+"</b></td>";
			message += "	</tr>";
			message += "</table>";
			message += "";
			message += "<p>&nbsp;</p>";
			message += "<table border='0.1'  width='100%' style='border-left-style:none;border-right-style:none;border-top-style:none;border-bottom-style:none;' >";
			message += "	<tr>";
			message += "		<td align='center'><img width='232px' height='174px' align='center' src=" +urlImage+ " /></td>";
			message += "	</tr>";
			message += "</table>";
			
			message += "	</td>";
			message += "	</tr>";
			message += "</table>";
			message += "</body>";
			message +="</html>";
			
	//------------------------------------------------STONE Details--------------------------------------------------------//
	//========================================================Line Item=======================================================//


  ///-----------------------------------------------------
  
 
	  var xml="";
	  xml += "<pdf>";
	  xml += "<head>";
	  xml += "  <style > @page {size: landscape;}</style>"; 
	  xml += "</head>";
	  //xml += "<body margin-top=\"0pt\" header=\"smallheader\" >"; 
	  xml += "<body margin-bottom=\"1pt\" header-height=\"60\">";
	   nlapiLogExecution('DEBUG', 'Message', " message : " + message);  
	    xml += "</body>\n</pdf>";
		response.write( message );
	  ////------------------------------------------------------
	 
	 // xml += message;
	 
	  //nlapiLogExecution('DEBUG', 'PrintXmlToPDF', 'xml='+xml); 
	  // run the BFO library to convert the xml document to a PDF 
	 // var file = nlapiXMLToPDF( xml );
	 // response.setContentType('PDF','JH Print.pdf', 'inline');
	  //response.write( file.getValue() );
	  
	  



	
}

	function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
	{
	
	 messgaeToBeSendPara = messgaeToBeSendPara.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);
	
	 messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
	
	 return messgaeToBeSendPara;
	}

	function formatAndReplaceSpacesOnlyfMessage(messgaeToBeSendPara)
	{
	
	 messgaeToBeSendPara = messgaeToBeSendPara.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);
	
	 messgaeToBeSendPara = messgaeToBeSendPara.replace(/ /g,"&nbsp");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);
	
	 return messgaeToBeSendPara;
	}

	function formatAndReplaceMessageForAnd(messgaeToBeSendParaAnd)
	{
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);
	
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/&/g,"&amp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
	
	 return messgaeToBeSendParaAnd;
	}
	function formatAndReplaceMessageForhash(messgaeToBeSendParaAnd)
	{
	
	 
	 
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.toString();
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  Meesage with and =====" + messgaeToBeSendParaAnd);
	
	 messgaeToBeSendParaAnd = messgaeToBeSendParaAnd.replace(/#/g,"&amp;");/// /g
	 nlapiLogExecution('DEBUG','In formatAndReplaceMessageForAnd', "  messgaeToBeSendParaAnd After Replcing and with &amp; =====" + messgaeToBeSendParaAnd);
	
	 return messgaeToBeSendParaAnd;
	}
 
	 //-----------------------------------Landscape--------------------------------------------------------------//

 


