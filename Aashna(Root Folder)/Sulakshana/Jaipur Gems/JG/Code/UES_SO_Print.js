// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Button on Invoice record
	Author:Sulakshana Ghadage	
	Company:Jaipur Gems
	Date:10-10-2012
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function SOPrintAfterSubmitRecord(type,form)
{
    if (type == 'view') 
	{
		
		
			var SOid = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'SOid', 'SO id' + SOid);
			
			if(SOid!=null)
			{
				var soObj = nlapiLoadRecord('salesorder',SOid);
				var cutomForm = soObj.getFieldValue('customform')
				nlapiLogExecution('DEBUG', 'cutom Form', 'cutomForm == ' +cutomForm);
				
				if(cutomForm=='136')
				{
					/*
var custid = nlapiGetFieldValue('entity');
					nlapiLogExecution('DEBUG', 'custid', 'customer id' +custid);
*/
					
			           
				     /*
		   			var depository = nlapiGetFieldValue('customerdeposit');
					nlapiLogExecution('DEBUG', 'depository', 'depository' +depository);//customform
					*/
		
		   			/*
						var soid = nlapiGetFieldValue('salesorder');
						nlapiLogExecution('DEBUG', 'soid', 'soid' +soid);
					*/
		
					//nlapiLogExecution('DEBUG', 'SalesorderInternalId', 'Salesorder InternalId' +SalesorderInternalId);
					form.setScript('customscript_clisoprint');
					
					form.addButton('custpage_soprint', 'Estimate Print','callPrintSoSuitelets(\'' + SOid + '#' + 'custpage_soprintEstimate'+ '\');');
					nlapiLogExecution('DEBUG', 'cutom Form', 'cutomForm' +cutomForm);
					return true;
						
				}				
			}
			
			
			
			
	}

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function SOBeforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function SOAfterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
