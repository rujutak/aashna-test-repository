// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:Sulakshana Ghadage
	Company:Aashna Cloudtech Pvt Ltd
	Date:16-04-2013
	Description:Revenue Recognition Journal Entry at Item-Item Fulfillment transaction is set to ï¿½Shippedï¿½. 
				Make a Journal entry record and add a Debit and a Credit for each line item on the Fulfillment 
				transaction. 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
   
	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
	var IFstatus1=nlapiGetFieldValue('status');
	  nlapiLogExecution('DEBUG', 'after Submit', 'IFstatus1=' + IFstatus1);
	nlapiLogExecution('DEBUG', 'after Submit', 'type in after submit=' + type);
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	if(type=='create' || type =='edit'|| type =='ship')
	{
		//var srNo=0;
		//load Item fullfillment record
	  var recordId = nlapiGetRecordId()
	  //nlapiLogExecution('DEBUG', 'after Submit', 'recordId=' + recordId);
	  var rectype = nlapiGetRecordType();
	  //nlapiLogExecution('DEBUG', 'after Submit', 'rectype=' + rectype);
	  var IFObj = nlapiLoadRecord(rectype, recordId);
	  //nlapiLogExecution('DEBUG', 'after Submit', 'IFObj=' + IFObj);	  
	  
	  //get sales order id
	  var createdfrom=IFObj.getFieldValue('createdfrom');
	  //nlapiLogExecution('DEBUG', 'after Submit', 'createdfrom=' + createdfrom);
	  //get status of IF after submit//status
	  var IFstatus=IFObj.getFieldValue('status');
	  //nlapiLogExecution('DEBUG', 'after Submit', 'IFstatus=' + IFstatus);
	  
	  var revRecJ=IFObj.getFieldValue('custbody_revrec_journal');
	  //nlapiLogExecution('DEBUG', 'after Submit', 'revRecJ=' + revRecJ);
	  
	  var IFItemLineCount = IFObj.getLineItemCount('item');
	  //nlapiLogExecution('DEBUG', 'After Submit', 'IFItemLineCount == ' + IFItemLineCount);
	  //if IF item count is more then 90 call schedule script
	  
	 //nlapiLogExecution('DEBUG', 'before submit', " TYPE ::" + type); 	  
	  
	  if(IFstatus =='Shipped' && revRecJ == null)
	  {
			  if(IFItemLineCount>=90)
			  {
			  	//nlapiLogExecution('DEBUG', 'before submit', " In schedule script ::" );
			  	var params = new Array();
				params['status'] = 'scheduled';
			    params['runasadmin'] = 'T';
				var startDate = new Date();
				params['startdate'] = startDate.toUTCString();
				params['custscript_itemfullimentid'] = recordId;	
				params['custscript_flag'] = 1;	
				params['custscript_revrecjv'] = revRecJ;
				   
				var status = nlapiScheduleScript('customscript_sch_itemfullfillmenttojv', 'customdeploy1',params);
				//nlapiLogExecution('DEBUG', 'before submit', " status ::" + status);
			  }
			  else
			  CreateForwardJV(IFObj,recordId);
	  }//if Shipp and RevRecJ Null
	 
	
}//If create and edit

	//if Item fullfillment is deleted then reverce the Journal entry
	 //nlapiLogExecution('DEBUG', 'before submit', " type before delete ::" + type);
	if(type=='delete')
	{
		var preveousIFid =nlapiGetRecordId();
		//nlapiLogExecution('DEBUG', 'before submit', " preveousIFid ::" + preveousIFid);
		
		var tranid = nlapiGetFieldValue('tranid');
		//nlapiLogExecution('DEBUG', 'before submit', " tranid ::" + tranid);
		
	  //nlapiLogExecution('DEBUG', 'before submit', " type after delete ::" + type);
	  var IFOldObj=nlapiGetOldRecord()
	  //nlapiLogExecution('DEBUG', 'after Submit', 'IFOldObj In Delete=' + IFOldObj);
	  var IFItemLineCount =IFOldObj.getLineItemCount('item');
	  //nlapiLogExecution('DEBUG', 'after Submit', 'IFItemLineCount In Delete=' + IFItemLineCount);
	  var revRecJVOld=IFOldObj.getFieldValue('custbody_revrec_journal');
	  //nlapiLogExecution('DEBUG', 'after Submit', 'revRecJVOld In Delete=' + revRecJVOld); 
	  if(IFItemLineCount>=90)
	  {
	  	var params = new Array();
		params['status'] = 'scheduled';
	    params['runasadmin'] = 'T';
		var startDate = new Date();
		params['startdate'] = startDate.toUTCString();
		params['custscript_itemfullimentid'] = IFOldObj;	
		params['custscript_flag'] = 2;	
		params['custscript_revrecjv'] = revRecJVOld;
		   
		var status = nlapiScheduleScript('customscript_sch_itemfullfillmenttojv', 'customdeploy1',params);
		//nlapiLogExecution('DEBUG', 'before submit', " status In Delete ::" + status);
	  }
	  else
	  CreateReverseJV(revRecJVOld,preveousIFid,tranid); 
	}//If Delete
	

	

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function CreateForwardJV(IFObj,IFId)
{ 
       try
	   {
	   	
		var userID = nlapiGetUser();
		//nlapiLogExecution('DEBUG', 'after Submit', 'userID=' + userID);
		
		var User = nlapiLoadRecord('employee',userID);
		//nlapiLogExecution('DEBUG', 'after Submit', 'User=' + User);
		
		var Employee = User.getFieldValue('entityid');
		//nlapiLogExecution('DEBUG', 'after Submit', 'Employee=' + Employee);
		
		var srNo=0;
		var date = IFObj.getFieldValue('trandate');
		//nlapiLogExecution('DEBUG', 'after Submit', 'date=' + date);
		
		var tranId = IFObj.getFieldValue('tranid');
		//nlapiLogExecution('DEBUG', 'after Submit', 'tranId=' + tranId);
		
		var createdDate = IFObj.getFieldValue('createddate');
		//nlapiLogExecution('DEBUG', 'after Submit', 'createdDate=' +createdDate);
		
		var createdfrom = IFObj.getFieldValue('createdfrom');
		//nlapiLogExecution('DEBUG', 'after Submit', 'createdfrom=' + createdfrom);
		
		//load sales order
		var SOObj = nlapiLoadRecord('salesorder', createdfrom);
		//nlapiLogExecution('DEBUG', 'after Submit', 'SOObj=' + SOObj);
		//get subsidary from sales order 
		var subsidary = SOObj.getFieldValue('subsidiary');
		//nlapiLogExecution('DEBUG', 'after Submit', 'subsidary=' + subsidary);
		
		var subsiObj = nlapiLoadRecord('subsidiary',subsidary);
		//nlapiLogExecution('DEBUG', 'after Submit', 'subsiObj=' + subsiObj);
		//get currency of Subsidary
		var Subsi_Currency = subsiObj.getFieldValue('currency');
		nlapiLogExecution('DEBUG', 'after Submit', 'Subsi_Currency=' + Subsi_Currency);
		//get currency from sales order
		var currency = SOObj.getFieldValue('currency');
		//nlapiLogExecution('DEBUG', 'after Submit', 'currency=' + currency);
		//get Exchange rate
		var ExchangeRate = SOObj.getFieldValue('exchangerate');
		//nlapiLogExecution('DEBUG', 'after Submit', 'ExchangeRate=' +ExchangeRate);
		
		//get value of Department from SO
		var department = SOObj.getFieldValue('department');
		//nlapiLogExecution('DEBUG', 'after Submit', 'department=' + department);
		//get item count on sales order item
		var SOitemCount = SOObj.getLineItemCount('item')
		//nlapiLogExecution('DEBUG', 'after Submit', 'SOitemCount=' + SOitemCount);		
		
		//nlapiLogExecution('DEBUG', 'after Submit', 'In status and revrej condition=');
		//create Journal Entry
		if(Subsi_Currency != currency)
		{
		nlapiLogExecution('DEBUG', 'after Submit', 'Subsidary currency and SOW currency not same=' );	
		var jvObject = nlapiCreateRecord('journalentry');
		jvObject.setFieldValue('trandate', date);
		jvObject.setFieldValue('subsidiary', subsidary);
		jvObject.setFieldValue('custbody_itemfulfillment', IFId);//IFId
		
		if (Employee != null) 
			{
				var memo = 'Journal entry created on item fulfillment. Item fulfillment # ' + tranId + ' created on ' + createdDate + ' by ' + Employee
				jvObject.setFieldValue('custbody_memo', memo);
			}
			else
			{
				var memo = 'Journal entry created on item fulfillment. Item fulfillment # ' + tranId + ' created on ' + createdDate
				jvObject.setFieldValue('custbody_memo', memo);	
			}
		
		//get item count on IF 
		var IFItemLineCount = IFObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG', 'After Submit', 'IFItemLineCount == ' + IFItemLineCount);
			
		for (var i = 1; i <= IFItemLineCount; i++) 
		{
			var IFItem = IFObj.getLineItemValue('item', 'item', i);
			nlapiLogExecution('DEBUG', 'after Submit', 'IFItem=' + IFItem);
			
			var SrNo = IFObj.getLineItemValue('item', 'custcol_srno', i);
			nlapiLogExecution('DEBUG', 'after Submit', 'SrNo=' + SrNo);
			
			var IFQty = IFObj.getLineItemValue('item', 'quantity', i);
			nlapiLogExecution('DEBUG', 'after Submit', 'IFQty=' + IFQty);
			
			var ItemType = IFObj.getLineItemValue('item', 'itemtype', i);//itemtype
			nlapiLogExecution('DEBUG', 'after Submit', 'ItemType=' + ItemType);
			
			if (ItemType == 'InvtPart') 
			{
				ItemObj = nlapiLoadRecord('inventoryitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Service') 
			{
				ItemObj = nlapiLoadRecord('serviceitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Kit') 
			{
				ItemObj = nlapiLoadRecord('kititem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'NonInvtPart') 
			{
				ItemObj = nlapiLoadRecord('noninventoryitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Assembly') //changed internal id to assemblyitem   
			{
				ItemObj = nlapiLoadRecord('assemblyitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'OthCharge') //changed internal id to otherchargeitem  
			{
				ItemObj = nlapiLoadRecord('otherchargeitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'serializedinventoryitem') //changed internal id to otherchargeitem  
			{
				ItemObj = nlapiLoadRecord('serializedinventoryitem', IFItem);
				nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			var debitAcc = ItemObj.getFieldValue('deferredrevenueaccount')
			nlapiLogExecution('DEBUG', 'after Submit', 'debitAcc=' + debitAcc);
			
			var creditAcc = ItemObj.getFieldValue('custitem_revenue_account')//incomeaccount
			nlapiLogExecution('DEBUG', 'after Submit', 'creditAcc=' + creditAcc);
			
			
			
			for (var j = 1; j <= SOitemCount; j++) 
			{
				var SOItem = SOObj.getLineItemValue('item', 'item', j);
				nlapiLogExecution('DEBUG', 'after Submit', 'SOItem=' + SOItem);
				
					var SOItemText = SOObj.getLineItemText('item', 'item', j);
					nlapiLogExecution('DEBUG', 'after Submit', 'SOItemText=' + SOItemText);
				
				var SOSrNo =SOObj.getLineItemValue('item', 'custcol_srno', j); 
				nlapiLogExecution('DEBUG', 'After Submit', 'SOSrNo == ' + SOSrNo);
				
				if (IFItem == SOItem && SrNo==SOSrNo ) 
				{
					var SORate = SOObj.getLineItemValue('item', 'rate', j);
					nlapiLogExecution('DEBUG', 'after Submit', 'SORate=' + SORate);
					
					var error='This is the Customised error message : The rate of item "'+SOItemText+'" is not present on sales order so the Journal entry is not created.Please enter the Rate for the item "'+SOItemText+'" and resubmit the fulfillment to create Journal entry.';
					if(SORate==''||SORate == null)
					{
						throw(error);
						
					}
					
					if (SORate != '' || SORate != null) 
					{
						var debitJV = parseFloat(SORate) * parseFloat(IFQty) *parseFloat(ExchangeRate);
						nlapiLogExecution('DEBUG', 'after Submit', 'debitJV==========' + debitJV);
					}
					jvObject.selectNewLineItem('line');
					jvObject.setCurrentLineItemValue('line', 'account', debitAcc); //defferedRevenue
					jvObject.setCurrentLineItemValue('line', 'debit', debitJV.toFixed(2));
					jvObject.setCurrentLineItemValue('line', 'credit', 0.0);
					jvObject.setCurrentLineItemValue('line', 'department', department);
					jvObject.commitLineItem('line');
					
					jvObject.selectNewLineItem('line');
					jvObject.setCurrentLineItemValue('line', 'account', creditAcc); //UnbilledReceivable
					jvObject.setCurrentLineItemValue('line', 'debit', 0.0);
					jvObject.setCurrentLineItemValue('line', 'credit', debitJV.toFixed(2));//department
					jvObject.setCurrentLineItemValue('line', 'department', department);
					jvObject.commitLineItem('line');
					
					break;
				}
				
			}//for SO
		}//for IF
		var JVCount = IFObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG', 'after Submit', 'JVCount=' + JVCount);
		if (JVCount >= 1) 
		{
			var submitJV = nlapiSubmitRecord(jvObject, false, false);
			nlapiLogExecution('DEBUG', 'after Submit', 'submitJV=' + submitJV);
			
		}
		if (submitJV != null) 
		{
			IFObj.setFieldValue('custbody_revrec_journal', submitJV);
			
			//memo
			//submit the IF record
			var submitIF = nlapiSubmitRecord(IFObj, false, false);
			////nlapiLogExecution('DEBUG', 'after Submit', 'submitIF=' + submitIF);
		}
	}//if for currency check
		else
		{
		var jvObject = nlapiCreateRecord('journalentry');
		jvObject.setFieldValue('trandate', date);
		jvObject.setFieldValue('subsidiary', subsidary);
		jvObject.setFieldValue('custbody_itemfulfillment', IFId);//IFId
		
		if (Employee != null) 
			{
				var memo = 'Journal entry created on item fulfillment. Item fulfillment # ' + tranId + ' created on ' + createdDate + ' by ' + Employee
				jvObject.setFieldValue('custbody_memo', memo);
			}
			else
			{
				var memo = 'Journal entry created on item fulfillment. Item fulfillment # ' + tranId + ' created on ' + createdDate
				jvObject.setFieldValue('custbody_memo', memo);	
			}
		
		//get item count on IF 
		var IFItemLineCount = IFObj.getLineItemCount('item');
		//nlapiLogExecution('DEBUG', 'After Submit', 'IFItemLineCount == ' + IFItemLineCount);
			
		for (var i = 1; i <= IFItemLineCount; i++) 
		{
			var IFItem = IFObj.getLineItemValue('item', 'item', i);
			//nlapiLogExecution('DEBUG', 'after Submit', 'IFItem=' + IFItem);
			
			var SrNo = IFObj.getLineItemValue('item', 'custcol_srno', i);
			//nlapiLogExecution('DEBUG', 'after Submit', 'SrNo=' + SrNo);
			
			var IFQty = IFObj.getLineItemValue('item', 'quantity', i);
			//nlapiLogExecution('DEBUG', 'after Submit', 'IFQty=' + IFQty);
			
			var ItemType = IFObj.getLineItemValue('item', 'itemtype', i);//itemtype
			//nlapiLogExecution('DEBUG', 'after Submit', 'ItemType=' + ItemType);
			
			if (ItemType == 'InvtPart') 
			{
				ItemObj = nlapiLoadRecord('inventoryitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Service') 
			{
				ItemObj = nlapiLoadRecord('serviceitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Kit') 
			{
				ItemObj = nlapiLoadRecord('kititem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'NonInvtPart') 
			{
				ItemObj = nlapiLoadRecord('noninventoryitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'Assembly') //changed internal id to assemblyitem   
			{
				ItemObj = nlapiLoadRecord('assemblyitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'OthCharge') //changed internal id to otherchargeitem  
			{
				ItemObj = nlapiLoadRecord('otherchargeitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			else 
			if (ItemType == 'serializedinventoryitem') //changed internal id to otherchargeitem  
			{
				ItemObj = nlapiLoadRecord('serializedinventoryitem', IFItem);
				//nlapiLogExecution('DEBUG', 'after Submit', 'ItemObj=' + ItemObj);
			}
			var debitAcc = ItemObj.getFieldValue('deferredrevenueaccount')
			//nlapiLogExecution('DEBUG', 'after Submit', 'debitAcc=' + debitAcc);
			
			var creditAcc = ItemObj.getFieldValue('custitem_revenue_account')//incomeaccount
			//nlapiLogExecution('DEBUG', 'after Submit', 'creditAcc=' + creditAcc);
			
			for (var j = 1; j <= SOitemCount; j++) 
			{
				var SOItem = SOObj.getLineItemValue('item', 'item', j);
				//nlapiLogExecution('DEBUG', 'after Submit', 'SOItem=' + SOItem);
				
					var SOItemText = SOObj.getLineItemText('item', 'item', j);
					//nlapiLogExecution('DEBUG', 'after Submit', 'SOItemText=' + SOItemText);
				
				var SOSrNo =SOObj.getLineItemValue('item', 'custcol_srno', j); 
				//nlapiLogExecution('DEBUG', 'After Submit', 'SOSrNo == ' + SOSrNo);
				
				if (IFItem == SOItem && SrNo==SOSrNo ) 
				{
					var SORate = SOObj.getLineItemValue('item', 'rate', j);
					//nlapiLogExecution('DEBUG', 'after Submit', 'SORate=' + SORate);
					
					var error='This is the Customised error message : The rate of item "'+SOItemText+'" is not present on sales order so the Journal entry is not created.Please enter the Rate for the item "'+SOItemText+'" and resubmit the fulfillment to create Journal entry.';
					if(SORate==''||SORate == null)
					{
						throw(error);
						
					}
					
					if (SORate != '' || SORate != null) 
					{
						var debitJV = parseFloat(SORate) * parseFloat(IFQty)// *parseFloat(ExchangeRate);
						//nlapiLogExecution('DEBUG', 'after Submit', 'debitJV=' + debitJV);
					}
					jvObject.selectNewLineItem('line');
					jvObject.setCurrentLineItemValue('line', 'account', debitAcc); //defferedRevenue
					jvObject.setCurrentLineItemValue('line', 'debit', debitJV);
					jvObject.setCurrentLineItemValue('line', 'credit', 0.0);
					jvObject.setCurrentLineItemValue('line', 'department', department);
					jvObject.commitLineItem('line');
					
					jvObject.selectNewLineItem('line');
					jvObject.setCurrentLineItemValue('line', 'account', creditAcc); //UnbilledReceivable
					jvObject.setCurrentLineItemValue('line', 'debit', 0.0);
					jvObject.setCurrentLineItemValue('line', 'credit', debitJV);//department
					jvObject.setCurrentLineItemValue('line', 'department', department);
					jvObject.commitLineItem('line');
					
					break;
				}
				
			}//for SO
		}//for IF
			var JVCount = IFObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG', 'after Submit', 'JVCount=' + JVCount);
		if (JVCount >= 1) 
		{
			var submitJV = nlapiSubmitRecord(jvObject, false, false);
			nlapiLogExecution('DEBUG', 'after Submit', 'submitJV=' + submitJV);
			
		}
		if (submitJV != null) 
		{
			IFObj.setFieldValue('custbody_revrec_journal', submitJV);
			
			//memo
			//submit the IF record
			var submitIF = nlapiSubmitRecord(IFObj, false, false);
			////nlapiLogExecution('DEBUG', 'after Submit', 'submitIF=' + submitIF);
		}
		}
	
	
	   }//try
	   catch(e)
	   {
	   	nlapiLogExecution('DEBUG', 'after Submit', 'try catch error=' + e);
	   }//catch

}

function CreateReverseJV(revRecJVOld,preveousIFid,tranid)
{
	 try 
	 {
	 	if (revRecJVOld != null) 
		{
			var userID = nlapiGetUser();
		 	////nlapiLogExecution('DEBUG', 'after Submit', 'userID=' + userID);
		
			var User = nlapiLoadRecord('employee',userID);
			////nlapiLogExecution('DEBUG', 'after Submit', 'User=' + User);
		
			var Employee = User.getFieldValue('entityid');
			////nlapiLogExecution('DEBUG', 'after Submit', 'Employee=' + Employee);
	 	
	 		var JVOldObj = nlapiLoadRecord('journalentry', revRecJVOld);
	 		////nlapiLogExecution('DEBUG', 'after Submit', 'JVOldObj=' + JVOldObj);
	 		
	 		var oldJVSubsidary = JVOldObj.getFieldValue('subsidiary');
	 		////nlapiLogExecution('DEBUG', 'after Submit', 'oldJVSubsidary=' + oldJVSubsidary);
	 		
	 		var oldJVCurrency = JVOldObj.getFieldValue('currency');
	 		////nlapiLogExecution('DEBUG', 'after Submit', 'oldJVCurrency=' + oldJVCurrency);
	 		
	 		var oldJVDate = JVOldObj.getFieldValue('trandate');
	 		////nlapiLogExecution('DEBUG', 'after Submit', 'oldJVDate=' + oldJVDate);
			
			
			var createdDate = JVOldObj.getFieldValue('createddate');
			////nlapiLogExecution('DEBUG', 'after Submit', 'createdDate=' +createdDate);
			
			
	 		//create new JV
				var newJVObject = nlapiCreateRecord('journalentry');
				
				if (oldJVDate != '' || oldJVDate != null) 
				{
					newJVObject.setFieldValue('trandate', oldJVDate);
				}
				if (oldJVSubsidary != '' || oldJVSubsidary != null) 
				{
					newJVObject.setFieldValue('subsidiary', oldJVSubsidary);
				}
				if (oldJVCurrency != '' || oldJVCurrency != null) 
				{
					newJVObject.setFieldValue('currency', oldJVCurrency);
				}
				
				var oldJVLineItemCount = JVOldObj.getLineItemCount('line')
				////nlapiLogExecution('DEBUG', 'after Submit', 'oldJVLineItemCount=' + oldJVLineItemCount);
				
				for (var k = 1; k <= oldJVLineItemCount; k++) 
				{
					var oldJVItem = JVOldObj.getLineItemValue('line', 'account', k)
					////nlapiLogExecution('DEBUG', 'after Submit', 'oldJVItem=' + oldJVItem);
					
					var olddebit = JVOldObj.getLineItemValue('line', 'debit', k)
					////nlapiLogExecution('DEBUG', 'after Submit', 'olddebit=' + olddebit);
					
					var oldCredit = JVOldObj.getLineItemValue('line', 'credit', k)//credit
					////nlapiLogExecution('DEBUG', 'after Submit', 'oldCredit=' + oldCredit);
					
					var olddept = JVOldObj.getLineItemValue('line', 'department', k)
					////nlapiLogExecution('DEBUG', 'after Submit', 'olddept=' + olddept);
					
					if (oldJVItem != '' || oldJVItem != null || oldJVItem != 'undefined') 
					{
						newJVObject.selectNewLineItem('line');
						newJVObject.setCurrentLineItemValue('line', 'account', oldJVItem); //defferedRevenue
						newJVObject.setCurrentLineItemValue('line', 'debit', oldCredit);
						newJVObject.setCurrentLineItemValue('line', 'credit', olddebit);
						newJVObject.setCurrentLineItemValue('line', 'department', olddept);
						newJVObject.commitLineItem('line');
					}
				}
				
				newJVObject.setFieldValue('custbody_refforwardjournal', revRecJVOld);
				
				
				
				
				var newJVSubmit = nlapiSubmitRecord(newJVObject, false, false);
				
				var LoadNewJV = nlapiLoadRecord('journalentry',newJVSubmit);
				
				var lastModified = LoadNewJV.getFieldValue('lastmodifieddate');
				////nlapiLogExecution('DEBUG', 'after Submit', 'lastModified=' +lastModified);
				if (Employee != null) 
				{
					
					var memo = 'Reversing journal entry created due to deletion of item fulfillment. Item fulfillment #'+tranid+'  deleted on ' + lastModified + ' by ' + Employee
					LoadNewJV.setFieldValue('custbody_memo', memo);
				}
				else
				{
					var memo = 'Reversing journal entry created due to deletion of item fulfillment. Item fulfillment #'+tranid+ '  deleted on' + lastModified  
					LoadNewJV.setFieldValue('custbody_memo', memo);	
				}
				
				////nlapiLogExecution('DEBUG', 'after Submit', 'newJVSubmit=' + newJVSubmit);
				if (LoadNewJV != null) 
				{
					JVOldObj.setFieldValue('custbody_refrevercejournal', newJVSubmit);
					nlapiSubmitRecord(JVOldObj);
				}
				
			var newJVSubmit = nlapiSubmitRecord(LoadNewJV, false, false);
			}//if
		}
		catch(e)
		{
			////nlapiLogExecution( 'DEBUG', 'unexpected error', e.toString())
		}
}
// END FUNCTION =====================================================

///Scheduled Script function

function ItemfullmentToJV()
{
		var contextSCH = nlapiGetContext();
		//////nlapiLogExecution('DEBUG', 'context', 'contextSCH' + contextSCH);
		var IFID=contextSCH.getSetting('SCRIPT','custscript_itemfullimentid');
		//////nlapiLogExecution('DEBUG', 'after Submit', 'IFID=' + IFID);
		var flag=contextSCH.getSetting('SCRIPT','custscript_flag');
		//////nlapiLogExecution('DEBUG', 'after Submit', 'flag=' + flag);
		var revRecJV=contextSCH.getSetting('SCRIPT','custscript_revrecjv');
		//////nlapiLogExecution('DEBUG', 'after Submit', 'revRecJV=' + revRecJV);
		
	if(flag==1)
	{
		var IFObj=nlapiLoadRecord('itemfulfillment',IFID);
		CreateForwardJV(IFObj,IFID);
	}
	else if(flag==2)
	{
		//var IFRevObj=nlapiLoadRecord('journalentry',revRecJV);
		CreateReverseJV(revRecJV,preveousIFid,tranid);
	}
	
}
