/**
 * @author Shweta
 * @modified by Supriya
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name : SCH_LocationWise_PositiveAdjustment.js
     Date        : 19 Sep 2013
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type){
    try {
        var context = nlapiGetContext();
        var usageBegin = context.getRemainingUsage();
        nlapiLogExecution('DEBUG', 'schedulerFunction', ' Usage Begin -->' + usageBegin);
        
        var counter = context.getSetting('SCRIPT', 'custscript_ad_positive_counter');
		
        
		
		if (counter != null && counter != '') {
            counter = counter;
        }//if
        else {
            counter = 0;
        }
        
        var filter = new Array();
        var coloumn = new Array();
        filter[0] = new nlobjSearchFilter('custbody_is_negative_processed', null, 'is', 'T')
        coloumn[0] = new nlobjSearchColumn('internalid')
        
        
        var search = nlapiSearchRecord('inventoryadjustment', 2295, filter, coloumn)
        
        if (search != null && search != undefined && search != '') {
            nlapiLogExecution('DEBUG', 'schedulerFunction', ' Search Length --> ' + search.length);
            for (var i = 0; i < search.length; i++) //for (var i = 0; i < search.length; i++) 
            {
            
                //******************************Begin Code For Getting Is processed Adjustmnet Records ************************************
				
                    var AfjustRecID = search[i].getValue('internalid');
					nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'AfjustRecID=> ' + AfjustRecID);
					var Flag = 0;
				
				
				    var filter1 = new Array();
			        var coloumn1 = new Array();
			    
			        coloumn1[0] = new nlobjSearchColumn('internalid')
					coloumn1[1] = new nlobjSearchColumn('custrecord_cr_negative_adjrec')
			        coloumn1[2] = new nlobjSearchColumn('custrecord_cr_positive_adjrec')
			        
			        var search1 = nlapiSearchRecord('customrecord_location_to_bin_transfer', 'customsearch2298', null, coloumn1)
					
					if(search1 != null && search1 != undefined && search1 != '' )
					{
						 for (var j = 0; j < search1.length; j++)
						 {
						 	var negativeAdjId = search1[j].getValue('custrecord_cr_negative_adjrec');
							//nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'negativeAdjId=> ' + negativeAdjId);
							
							if(AfjustRecID == negativeAdjId )
							{
								Flag = 1;
								break;
							}
						 }
					}
				
                
				if(Flag != 1)
				{
			    var InventoryAdjust = nlapiLoadRecord('inventoryadjustment', AfjustRecID)
                
                var inventory_location = InventoryAdjust.getLineItemValue('inventory', 'location', 1)
                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'inventory_location=> ' + inventory_location);
                
                var LocationOnHand = InventoryAdjust.getLineItemValue('inventory', 'adjustqtyby', 1)
                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'LocationOnHand=> ' + LocationOnHand);
                
                var item_id = InventoryAdjust.getLineItemValue('inventory', 'item', 1)
                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'item_id=> ' + item_id);
                
                var objInvItem = ReturnItemObject(item_id)
                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'objInvItem=> ' + objInvItem);
                
                var Isserilised = objInvItem.getFieldValue('isserialitem')
                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'Isserilised=> ' + Isserilised);
                
                if (Isserilised == 'F') {
                    if (inventory_location != '' && inventory_location != null && inventory_location != undefined) {
                        var LocationRec = nlapiLoadRecord('location', inventory_location)
                        
                        if (LocationRec != '' && LocationRec != null && LocationRec != undefined) {
                            var LocationSubsidiary = LocationRec.getFieldValue('custrecord_loc_subsidiary')
                            nlapiLogExecution('DEBUG', 'schedulerFunction', 'LocationSubsidiary=> ' + LocationSubsidiary);
                            
                            var ParentLocation = LocationRec.getFieldValue('parent')
                            nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentLocation=> ' + ParentLocation);
                            
                            if (ParentLocation != '' && ParentLocation != null && ParentLocation != undefined) {
                                var ParentLocRec = nlapiLoadRecord('location', ParentLocation)
                                nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentLocRec=> ' + ParentLocRec);
                                
                                if (ParentLocRec != '' && ParentLocRec != null && ParentLocRec != undefined) {
                                    var ParentBin = ParentLocRec.getFieldValue('custrecord_created_mbin_id')
                                    nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentBin=> ' + ParentBin);
                                    
                                    var ParentBinText = ParentLocRec.getFieldText('custrecord_created_mbin_id')
                                    nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentBinText=> ' + ParentBinText);
                                    
                                }//ParentBin
                                
								//****************************Code For Item Bin Updation********************************************************
								if (item_id != null && item_id != '' && item_id != undefined) {
                                
                                    var recordOBJ = ReturnItemObject(item_id)
                                    nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'recordOBJ***=> ' + recordOBJ);
									
                                    if (recordOBJ != null && recordOBJ != '' && recordOBJ != undefined) {
                                    
                                        recordOBJ.setFieldValue('usebins', 'T')
                                        recordOBJ.setFieldValue('custitem_adjust_quan', LocationOnHand)
                                        var submitID = nlapiSubmitRecord(recordOBJ, false, false);
										nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'submitID***=> ' + submitID);
										
										var is_bin_present = search_Bin_Number(submitID, ParentBin);
                    					nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'is_bin_present=> ' + is_bin_present);
                    
                    
                    					var bin_on_hand = search_Bin_Number_on_hand(submitID, ParentBin);
                   						nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'bin_on_hand=> ' + bin_on_hand);
                    
                    
                    					if (is_bin_present.indexOf(ParentBin) == -1) 	
										{
                        				nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	if(is_bin_present.indexOf(LocationBin)== -1)');
                        
                       					var updateBinNum = updateBinNumber(item_id, ParentBin, LocationOnHand)
                                        nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'Bin Updated => ' + updateBinNum);
                   					    }	

                                        
                                    }
                                    
                                }
                                
                                //****************************End of the Code For Item Bin Updation********************************************************
                                
								//****************************Code For Location Bin String Designing********************************************************
								var LocationOnHand = Math.abs(LocationOnHand)
                                
                                var LocationOnHand_plus = Math.abs(LocationOnHand)
                                var LocationBinTextFinal = ParentBinText + '(' + LocationOnHand_plus + ')'
                                nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'LocationBinTextFinal 2=> ' + LocationBinTextFinal);
                                
								//****************************Code For Location Bin String Designing********************************************************
                                
								//****************************Code For Creating Inventry Adjustment Record********************************************************
                             
  
 							try {
                                
                                
                                
                                    var invAdjRec1 = nlapiCreateRecord('inventoryadjustment')
                                    
                                    invAdjRec1.setFieldValue('subsidiary', LocationSubsidiary)
                                    invAdjRec1.setFieldValue('account', 423)
                                    invAdjRec1.setFieldValue('custbody24', 'T')
                                    invAdjRec1.setLineItemValue('inventory', 'item', 1, item_id)
                                    invAdjRec1.setLineItemValue('inventory', 'location', 1, ParentLocation)
                                    invAdjRec1.setLineItemValue('inventory', 'adjustqtyby', 1, LocationOnHand)
                                    invAdjRec1.setLineItemValue('inventory', 'binnumbers', 1, LocationBinTextFinal)
                                    
                                    var CreatedAdjustmentRec2 = nlapiSubmitRecord(invAdjRec1, true, true)
                                    nlapiLogExecution('DEBUG', 'SearchInventoryRecords', ' ***************** Positive Inventory Adjustment ID **********************=> ' + CreatedAdjustmentRec2);
                                    
									
									
                                    if (CreatedAdjustmentRec2 != null || CreatedAdjustmentRec2 != undefined || CreatedAdjustmentRec2 != '') 
									{
										
                                        objInvItem.setFieldValue('custitem60', 'T')
                                        
                                        var UpdatedInvetoryItem = nlapiSubmitRecord(objInvItem, true, true)
                                        nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'UpdatedInvetoryItem=> ' + UpdatedInvetoryItem);
										
										
										
										var locationToBinTransfer = nlapiCreateRecord('customrecord_location_to_bin_transfer')
										locationToBinTransfer.setFieldValue('custrecord_cr_negative_adjrec',AfjustRecID)
										locationToBinTransfer.setFieldValue('custrecord_cr_positive_adjrec',CreatedAdjustmentRec2)
										var locationToBinTransfer_Id = nlapiSubmitRecord(locationToBinTransfer, true, true)
										nlapiLogExecution('DEBUG', ' Inside The For Loop  ', ' locationToBinTransfer_Id => ' + locationToBinTransfer_Id);
										
                                    }
                                    
                                }//TRY
                                 catch (er) {
                                    nlapiLogExecution('DEBUG', ' ERROR ', ' Exception Caught => ' + er);
                                    
                                }


								//****************************Code For Creating Inventry Adjustment Record********************************************************
                                
                            }//ParentLocation
                        }//LocationRec
                    }//InventoryLocation
                    var usageEnd = context.getRemainingUsage();
                    nlapiLogExecution('DEBUG', 'schedulerFunction', '************ usageEnd [' + i + '] ************ ==' + usageEnd);
                    
                    if (usageEnd < 500) {
                        nlapiLogExecution('DEBUG', 'schedulerFunction', '************ Script Rescheduled for Item  ************ ==' + item_id);
                        
                        Schedulescriptafterusageexceeded(item_id);
                        break;
                    }
                }
				}// Flag End 
				
               
                
                
            }
            
            
        }//Search
    } 
    catch (exception) {
        nlapiLogExecution('DEBUG', 'ERROR', ' Exception -->' + exception);
    }
    
    
    
}

// END SCHEDULED FUNCTION ===============================================





function getParent(inventory_location, bin_number){
    if (inventory_location != null && inventory_location != '' && inventory_location != undefined) {
        if (bin_number != null && bin_number != '' && bin_number != undefined) {
            var loc_obj = nlapiLoadRecord('location', inventory_location)
            nlapiLogExecution('DEBUG', 'getParent', '  Location Object ==' + loc_obj);
            
            if (loc_obj != null && loc_obj != '' && loc_obj != undefined) {
            
                var parent = loc_obj.getFieldValue('parent')
                nlapiLogExecution('DEBUG', 'getParent', '  parent ==' + parent);
                
                
            }//loc_obj
        }
    }
    
    
    
}




function search_Bin_Number(internalid, binID){
    nlapiLogExecution('DEBUG', 'search_Bin_Number', ' search_Bin_Number ');
    
    var recordID;
    var result = new Array();
    if (internalid != null && internalid != '' && internalid != undefined) {
        if (binID != null && binID != '' && binID != undefined) {
            var filter = new Array();
            filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
            //   filter[1] = new nlobjSearchFilter('binnumber', 'binnumber', 'is', binID);
            
            var columns = new Array();
            columns[0] = new nlobjSearchColumn('internalid');
            
            var searchresults = nlapiSearchRecord('item', null, filter, columns);
            
            if (searchresults != null) {
                for (i = 0; i < searchresults.length; i++) {
                    recordID = searchresults[i].getValue('internalid');
                    nlapiLogExecution('DEBUG', 'search_Bin_Number', '  Record ID [' + i + ']==' + recordID);
                    
                    if (recordID != null && recordID != '' && recordID != undefined) {
                        var recordOBJ = ReturnItemObject(recordID)
                        nlapiLogExecution('DEBUG', 'search_Bin_Number', '  recordOBJ ==' + recordOBJ);
                        
                        if (recordOBJ != null && recordOBJ != '' && recordOBJ != undefined) {
                            var binLineCount = recordOBJ.getLineItemCount('binnumber');
                            nlapiLogExecution('DEBUG', 'search_Bin_Number', '  Bin Line Count ==' + binLineCount);
                            
                            if (binLineCount != null && binLineCount != '' && binLineCount != undefined) {
                                for (j = 1; j <= binLineCount; j++) {
                                    var bin_number = recordOBJ.getLineItemValue('binnumber', 'binnumber', j);
                                    nlapiLogExecution('DEBUG', 'search_Bin_Number', '  Bin Number ==' + bin_number);
                                    
                                    
                                    var bin_onhand = recordOBJ.getLineItemValue('binnumber', 'onhand', j);
                                    nlapiLogExecution('DEBUG', 'search_Bin_Number', '  Bin onhand ==' + bin_onhand);
                                    
                                    
                                    
                                    result.push(bin_number);
                                    
                                    
                                }//Loop							
                            }//binLineCount						
                        }//recordOBJ
                    }//recordID				
                }//Loop
            }//SErach results 
        }
        
        
    }//Validate Check	
    return result;
    
}//Search Bin Number
function search_Bin_Number_on_hand(internalid, binID){
    nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', ' search_Bin_Number ');
    
    var recordID;
    var result = new Array();
    if (internalid != null && internalid != '' && internalid != undefined) {
        if (binID != null && binID != '' && binID != undefined) {
            var filter = new Array();
            filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
            //   filter[1] = new nlobjSearchFilter('binnumber', 'binnumber', 'is', binID);
            
            var columns = new Array();
            columns[0] = new nlobjSearchColumn('internalid');
            
            var searchresults = nlapiSearchRecord('item', null, filter, columns);
            
            if (searchresults != null) {
                for (i = 0; i < searchresults.length; i++) {
                    recordID = searchresults[i].getValue('internalid');
                    nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', '  Record ID [' + i + ']==' + recordID);
                    
                    if (recordID != null && recordID != '' && recordID != undefined) {
                        var recordOBJ = ReturnItemObject(recordID)
                        nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', '  recordOBJ ==' + recordOBJ);
                        
                        if (recordOBJ != null && recordOBJ != '' && recordOBJ != undefined) {
                            var binLineCount = recordOBJ.getLineItemCount('binnumber');
                            nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', '  Bin Line Count ==' + binLineCount);
                            
                            if (binLineCount != null && binLineCount != '' && binLineCount != undefined) {
                                for (j = 1; j <= binLineCount; j++) {
                                    var bin_number = recordOBJ.getLineItemValue('binnumber', 'binnumber', j);
                                    nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', '  Bin Number ==' + bin_number);
                                    
                                    var bin_onhand = recordOBJ.getLineItemValue('binnumber', 'onhand', j);
                                    nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand', '  Bin onhand ==' + bin_onhand);
                                    
                                    
                                    if (binID == bin_number) {
                                        result.push(bin_onhand);
                                        break;
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }//Loop							
                            }//binLineCount						
                        }//recordOBJ
                    }//recordID				
                }//Loop
            }//SErach results 
        }
        
        
    }//Validate Check	
    return result;
    
}//Search Bin Number
function updateBinNumber(internalid, binID, location_on_hand)
{
    var submitID;
    if (internalid != null && internalid != '' && internalid != undefined) 
	{
        if (binID != null && binID != '' && binID != undefined) 
		{
           
            try 
			{
            
            
                var recordOBJNew = ReturnItemObject(internalid)
                
                if (recordOBJNew != null && recordOBJNew != '' && recordOBJNew != undefined) 	
				{
                
                    recordOBJNew.selectNewLineItem('binnumber');
					
				    var lineItemIndex =  recordOBJNew.getCurrentLineItemIndex('binnumber');
					nlapiLogExecution('DEBUG', 'updateBinNumber', '  lineItemIndex ==' + lineItemIndex);
                    // set the item and location values on the currently selected line
					
					recordOBJNew.setLineItemValue('binnumber','binnumber', lineItemIndex ,binID)
					
                    //recordOBJNew.setCurrentLineItemValue('binnumber', 'binnumber', binID);
                    //recordOBJ.setCurrentLineItemValue('binnumber', 'onhand', location_on_hand);
                    
                    // commit the line to the database
                    // recordOBJNew.commitLineItem('binnumber');
                    
                    var submitIDNew = nlapiSubmitRecord(recordOBJNew, true, true);
                    nlapiLogExecution('DEBUG', 'updateBinNumber', '  submitIDNew ID ==' + submitIDNew);
                    nlapiLogExecution('DEBUG', 'updateBinNumber', '  After submit Occurs');
                    
                }
                
            } 
            catch (e) 
			{
                nlapiLogExecution('DEBUG', 'updateBinNumber', ' Inside Update BIn Error ==' + e);
            }
            
            
            
        }//binID
    }//internalid
    return submitIDNew;
    
}//updateBinNumber
function ReturnItemObject(InternalId)
{
    var Itemid = InternalId;
    if (Itemid != null && Itemid != undefined && Itemid != '') 
	{
        var ItemType = getLineItemType(Itemid)
        //alert('ItemType=='+ItemType)
        
        if (ItemType == 'InvtPart') {
            objInvItem = nlapiLoadRecord('inventoryitem', Itemid);
        }
        else 
            if (ItemType == 'Service') {
                objInvItem = nlapiLoadRecord('serviceitem', Itemid);
            }
            else 
                if (ItemType == 'Kit') {
                    objInvItem = nlapiLoadRecord('kititem', Itemid);
                }
                else 
                    if (ItemType == 'NonInvtPart') {
                        objInvItem = nlapiLoadRecord('noninventoryitem', Itemid);
                    }
                    else 
                        if (ItemType == 'Group') {
                            objInvItem = nlapiLoadRecord('itemgroup', Itemid);
                        }
                        else 
                            if (ItemType == 'Assembly') //changed internal id to assemblyitem
                            {
                                objInvItem = nlapiLoadRecord('assemblyitem', Itemid);
                            }
                            else 
                                if (ItemType == 'OthCharge') //changed internal id to otherchargeitem
                                {
                                    objInvItem = nlapiLoadRecord('otherchargeitem', Itemid);
                                }
                                else 
                                    if (ItemType == 'Payment') //changed internal id to paymentitem
                                    {
                                        objInvItem = nlapiLoadRecord('paymentitem', Itemid);
                                    }
        
    }
    
    
    return objInvItem;
}

function getLineItemType(internalId){
    if (internalId != '' && internalId != null && internalId != undefined) {
        var itemType;
        
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('internalid', null, 'is', internalId);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('type');
        
        var searchresult = nlapiSearchRecord('item', null, filters, columns);
        if (searchresult != null && searchresult != '' && searchresult != 'undefined') {
            for (var i = 0; searchresult != null && i < searchresult.length; i++) {
                itemType = searchresult[i].getValue('type');
                nlapiLogExecution('DEBUG', 'In getLineItemType', " Item Type = " + itemType);
            }
            
        }
        return itemType;
        
        
    }//internalId
}




function Schedulescriptafterusageexceeded(i){
    ////Define all parameters to schedule the script for voucher generation.
    var params = new Array();
    params['status'] = 'scheduled';
    params['runasadmin'] = 'T';
    params['custscript_ad_positive_counter'] = i;
    
    var startDate = new Date();
    params['startdate'] = startDate.toUTCString();
    
    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
    nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    
    ////If script is scheduled then successfuly then check for if status=queued
    if (status == 'QUEUED') {
        nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
    }
}//fun close
