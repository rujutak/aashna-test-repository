// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_CreatingMainBin
	Author: Supriya.      
	Company:    
	Date:       
	Version:    

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY
	
	//**************Function For Searching Location for Main Bin Creation************************************
	try
	{
	SearchLocation(); 		
	}
	catch(e)
	{
	 nlapiLogExecution('DEBUG', 'SearchLocation', 'Error=> '+e);	
	}
	

   

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
//**************Function For Searching Location for Main Bin Creation************************************
function SearchLocation() 	
{
	
	var Filters = new Array();
	var Coloumn = new Array();
	
	Filters[0]= new nlobjSearchFilter('custrecord_main_bin_created',null ,'is','F')
	Filters[1]= new nlobjSearchFilter('isinactive',null ,'is','F')
	
	Coloumn[0]= new nlobjSearchColumn('internalid')
	Coloumn[1]= new nlobjSearchColumn('name')
	
	
	var search= nlapiSearchRecord('location', null, Filters, Coloumn)
	
	if(search != null && search != undefined && search != '')
	{
		 nlapiLogExecution('DEBUG', 'SearchLocation', 'search=> '+search.length);
		 
		 for (var i=0; i<2; i++)	//search.length
		  {
	       
		   var InternalId = search[i].getValue('internalid')
		   nlapiLogExecution('DEBUG', 'SearchLocation', 'InternalId=> '+InternalId);
		   
		   var Name = search[i].getValue('name')
		   nlapiLogExecution('DEBUG', 'SearchLocation', 'Name=> '+Name);
		   
		   if(InternalId != null && InternalId != undefined && InternalId != '')
		   {
		   	
			MainBinCreation(InternalId,Name) 	
		   	
		   }
			
			
		  };
		 
	
	}
	
	
}
//**************Function For Creating Main Bin************************************
function MainBinCreation(InternalId,Name) 	
{
	nlapiLogExecution('DEBUG', 'SearchLocation', '=====Inside MainBinCreation InternalId =>'+InternalId+' Name=> '+Name);	
	
    
	var binName = 'Main_'+InternalId 
	
	var binRecord = nlapiCreateRecord('bin')
	
	
	binRecord.setFieldValue('binnumber',binName)
	binRecord.setFieldValue('location',InternalId)
	binRecord.setFieldValue('memo',Name)
	
	var createdBinID = nlapiSubmitRecord(binRecord ,true,true)
	nlapiLogExecution('DEBUG', 'SearchLocation', 'createdBinID=>'+createdBinID);	
	
	if(createdBinID != null && createdBinID != undefined && createdBinID !='')
	{
		UpdateLocationRecord(createdBinID , InternalId) 
	}
	
}

//**************Update Location Record with created bin************************************
function UpdateLocationRecord(createdBinID , InternalId) 	
{
   nlapiLogExecution('DEBUG', 'SearchLocation', '=====Inside UpdateLocationRecord InternalId =>'+InternalId+' createdBinID=> '+createdBinID);	
   
   var locationRec = nlapiLoadRecord('location',InternalId)	
   
   locationRec.setFieldValue('custrecord_main_bin_created','T')
   locationRec.setFieldValue('custrecord_created_mbin_id',createdBinID)
   
   var updatedLocationId =nlapiSubmitRecord(locationRec ,true , true)
   nlapiLogExecution('DEBUG', 'SearchLocation', 'updatedLocationId=>'+updatedLocationId);
   	
}

}
// END FUNCTION =====================================================
