/**
 * @author Shweta
 * @modified by Supriya
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_LocationWise_NegativeAdjustment.js
	Date        : 19 Sep 2013


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   17 sep 2013            Supriya                       Sachin K.                       done modification fpr negative adjustment creations


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	try
	{
	    var context = nlapiGetContext();
	    var usageBegin = context.getRemainingUsage();
	    nlapiLogExecution('DEBUG', 'schedulerFunction',' Usage Begin -->' + usageBegin);
	
	    var counter=context.getSetting('SCRIPT','custscript_ad_negative_counter');
   
	    if(counter!=null && counter!='')
		{
			counter = counter;
		}//if
		else
		{
			counter=0;
		}
		
		
	    var filter = new Array();
	  //  filter[0] = new nlobjSearchFilter('internalid',null,'is', 104393)
	  			
	   var search= nlapiSearchRecord('item', '2294', null , null)
	   
	   if(search != null && search != undefined && search != '')
	   {
   	    nlapiLogExecution('DEBUG', 'schedulerFunction', ' Search Length --> '+search.length);
		
	   //for (var i = 0; i <20; i++)
	   for (var i = 0; i < search.length; i++) //search.length
	    {
			var columns = search[i].getAllColumns();
			var columnLen = columns.length;
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Column Length --> ' + columnLen);
			
			var column0 = columns[0];
			var column1 = columns[1];
			var column2 = columns[2];
			var column3 = columns[3];
			var column4 = columns[4];
			var column5 = columns[5];
			var column6 = columns[6];
			var column7 = columns[7];
			var column8 = columns[8];
			var column9 = columns[9];
			var column10 = columns[10];
			var column11 = columns[11];
			var column12 = columns[12];
			var column13 = columns[13];
			
			
			var item_id  = search[i].getValue(column0);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Item ID --> ' + item_id);
							
            var item  = search[i].getValue(column1);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Item --> ' + item);
			
			var DisplayName = search[i].getValue(column2);
			var Description = search[i].getValue(column3);
			
			var item_type = search[i].getValue(column4);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Item Type --> ' + item_type);
			
			
			var location_on_hand = search[i].getValue(column5);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Location On Hand --> ' + location_on_hand);
			
			
			var bin_number = search[i].getValue(column7);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Bin Number --> ' + bin_number);
			
			
			var inventory_location = search[i].getValue(column9);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Inventory Location --> ' + inventory_location);
			
			var adjust_quantity = search[i].getValue(column6);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Adjust Quantity --> ' + adjust_quantity);
					
		    var LocationSubsidiary;
			var inventory_location = search[i].getValue(column9);
			nlapiLogExecution('DEBUG', 'schedulerFunction', 'Inventory Location --> ' + inventory_location);
			
		    	if(inventory_location!='' &&inventory_location!=null && inventory_location!=undefined)
					{
						var LocationRec = nlapiLoadRecord('location' , inventory_location)
				
						if(LocationRec!='' && LocationRec!=null && LocationRec!=undefined)
						{
						LocationSubsidiary = LocationRec.getFieldValue('custrecord_loc_subsidiary')
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'LocationSubsidiary=> ' + LocationSubsidiary);
						
						var ParentLocation = LocationRec.getFieldValue('parent')
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'LocationSubsidiary=> ' + LocationSubsidiary);
						
						if(ParentLocation!='' &&ParentLocation!=null && ParentLocation!=undefined)
						{
							var ParentLocRec = nlapiLoadRecord('location', ParentLocation)
							nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentLocRec=> ' + ParentLocRec);
														
							if(ParentBin!='' &&ParentBin!=null && ParentBin!=undefined)
							{
								    var ParentBin = ParentLocRec.getFieldValue('custrecord_created_mbin_id')
							        nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentBin=> ' + ParentBin);
								
									var ParentBinText = ParentLocRec.getFieldText('custrecord_created_mbin_id')
							        nlapiLogExecution('DEBUG', 'schedulerFunction', 'ParentBinText=> ' + ParentBinText);
															
							}//ParentBin
							
						}//ParentLocation
											
						var LocationBin= LocationRec.getFieldValue('custrecord_created_mbin_id')
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'LocationBin=> ' + LocationBin);
										
						
						
						var LocationBinText = LocationRec.getFieldText('custrecord_created_mbin_id')
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'LocationBinText=> ' + LocationBinText);
							
						
						// ================== Search Bin Number =====================
						
						var updateItemId = UpdateItemForNegative(item_id,LocationBin)
						
						if(updateItemId != null && updateItemId != undefined && updateItemId != '')
						{
						     var recordOBJ = ReturnItemObject(updateItemId)
							 nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	recordOBJ**='+recordOBJ);
							
						}
					}//LocationRec

				}//InventoryLocation
			if(location_on_hand != 0 && location_on_hand > 0 && location_on_hand != undefined && location_on_hand != null && location_on_hand!='')
			{
					
				try
				{
				
				
				
				var LocationOnHand = '-'+location_on_hand
			    nlapiLogExecution('DEBUG', 'schedulerFunction', ' **************  LocationOnHand ****************** => ' + LocationOnHand);	
				if(LocationOnHand != null && LocationOnHand != undefined && LocationOnHand != '')
				{
					nlapiLogExecution('DEBUG', 'schedulerFunction', ' ************** Inside LocationOnHand Not Null****************** => ' );
				
					
					var invAdjRec = nlapiCreateRecord('inventoryadjustment')
					invAdjRec.setFieldValue('subsidiary',LocationSubsidiary)
					invAdjRec.setFieldValue('account',423)
					invAdjRec.setFieldValue('custbody_adj_bin_for','T')
					invAdjRec.setFieldValue('custbody_is_negative_processed','T')
					invAdjRec.setLineItemValue('inventory','item',1,item_id )
					invAdjRec.setLineItemValue('inventory','location',1,inventory_location )
					invAdjRec.setLineItemValue('inventory','adjustqtyby',1,LocationOnHand )
				    var CreatedAdjustmentRec = nlapiSubmitRecord(invAdjRec ,true ,true )
				   nlapiLogExecution('DEBUG', 'schedulerFunction', ' **************  Negative Inventory Adjustment REcord ID ****************** => ' + CreatedAdjustmentRec);	
				}
				
				
				  
            /*
			if (CreatedAdjustmentRec != null || CreatedAdjustmentRec != undefined || CreatedAdjustmentRec != '') 
				  {
				   	var objInvItem = ReturnItemObject(item_id)
				  	nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'objInvItem=> ' + objInvItem);
					
					updateBinNumber(item_id,binID,location_on_hand)		 
					
					var UpdatedInvetoryItem = nlapiSubmitRecord(objInvItem,true,true)
					nlapiLogExecution('DEBUG', 'SearchInventoryRecords', 'UpdatedInvetoryItem=> ' + UpdatedInvetoryItem);
				
					
					
					
				  	
				  }
			*/

					
				}
				catch(er)
				{
					
				  nlapiLogExecution('DEBUG', 'ERROR ', 'Exception Caught => ' + er);
								
				}
				
				
				
				
			}//LocationOnHand		
			
			
			    var usageEnd = context.getRemainingUsage();
		        nlapiLogExecution('DEBUG', 'schedulerFunction','************ usageEnd ['+i+'] ************ ==' + usageEnd);
						
				if (usageEnd < 500) 
				{
					nlapiLogExecution('DEBUG', 'schedulerFunction','************ Script Rescheduled for Item  ************ ==' + item_id);
				
					Schedulescriptafterusageexceeded(item_id);
					break;	
				}		
					
			
		}//Loop		
		
	   }//search	
	   
	}//Try
	catch(exception)
	{
		 nlapiLogExecution('DEBUG', 'ERROR',' Exception -->' + exception);
	}//Catch
}//schedulerFunction

// END SCHEDULED FUNCTION ===============================================



function Schedulescriptafterusageexceeded(i)
{
			////Define all parameters to schedule the script for voucher generation.
			 var params=new Array();
			 params['status']='scheduled';
		 	 params['runasadmin']='T';
			 params['custscript_ad_negative_counter']=i;
			 
			 var startDate = new Date();
		 	 params['startdate']=startDate.toUTCString();
			
			 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
			 nlapiLogExecution('DEBUG','After Scheduling','Script scheduled status='+ status);
			 
			 ////If script is scheduled then successfuly then check for if status=queued
			 if (status == 'QUEUED') 
		 	 {
				nlapiLogExecution('DEBUG', 'schedulecreatevoucherforCampaign', 'Campaign script exit and script scheduled for first time=');
		 	 }
}//fun close



function getParent(inventory_location,bin_number)
{
	if (inventory_location != null && inventory_location != '' && inventory_location != undefined) 
	{
		if (bin_number != null && bin_number != '' && bin_number != undefined) 
		{
			var loc_obj = nlapiLoadRecord('location',inventory_location)
			nlapiLogExecution('DEBUG', 'getParent','  Location Object ==' + loc_obj);	
			
			if (loc_obj != null && loc_obj != '' && loc_obj != undefined) 
			{
				
				var parent = loc_obj.getFieldValue('parent')
				nlapiLogExecution('DEBUG', 'getParent','  parent ==' + parent);	
				
				
			}//loc_obj
			
			
			
		}
	}
	
	
	
}




function search_Bin_Number(internalid,binID)
{
	nlapiLogExecution('DEBUG', 'search_Bin_Number',' search_Bin_Number ');	
	
	var recordID;
	var result = new Array();
	if(internalid!=null && internalid!='' && internalid!=undefined)
	{
		if(binID!=null && binID!='' && binID!=undefined)
		{
		   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
		//   filter[1] = new nlobjSearchFilter('binnumber', 'binnumber', 'is', binID);
				
		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   
		  var searchresults = nlapiSearchRecord('item',null, filter, columns);
		  
		   if(searchresults!=null)
		   {
	   		 for(i=0;i<searchresults.length;i++)
			 {			 	
				recordID = searchresults[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'search_Bin_Number','  Record ID ['+i+']==' + recordID);	
				
				if(recordID!=null && recordID!='' && recordID!=undefined)
				{
					var recordOBJ = ReturnItemObject(recordID)
					nlapiLogExecution('DEBUG', 'search_Bin_Number','  recordOBJ ==' + recordOBJ);
									
					if(recordOBJ!=null && recordOBJ!='' && recordOBJ!=undefined)
					{
						var binLineCount = recordOBJ.getLineItemCount('binnumber');
						nlapiLogExecution('DEBUG', 'search_Bin_Number','  Bin Line Count ==' + binLineCount);	
						
						if(binLineCount!=null && binLineCount!='' && binLineCount!=undefined)
						{
							 for(j=1;j<=binLineCount;j++)
							 {
							 	var bin_number = recordOBJ.getLineItemValue('binnumber','binnumber',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number','  Bin Number ==' + bin_number);
								
								
								var bin_onhand = recordOBJ.getLineItemValue('binnumber','onhand',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number','  Bin onhand ==' + bin_onhand);
								
							
																								
								result.push(bin_number);

								
							 }//Loop							
							
						}//binLineCount						
						
					}//recordOBJ
					
				}//recordID				
				
			 }//Loop
			
		   }//SErach results 
						
		}
	 		
		
	}//Validate Check	
	
	return result;
	
}//Search Bin Number



function search_Bin_Number_on_hand(internalid,binID)
{
	nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand',' search_Bin_Number ');	
	
	var recordID;
	var result = new Array();
	if(internalid!=null && internalid!='' && internalid!=undefined)
	{
		if(binID!=null && binID!='' && binID!=undefined)
		{
		   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
		//   filter[1] = new nlobjSearchFilter('binnumber', 'binnumber', 'is', binID);
				
		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   
		  var searchresults = nlapiSearchRecord('item',null, filter, columns);
		  
		   if(searchresults!=null)
		   {
	   		 for(i=0;i<searchresults.length;i++)
			 {			 	
				recordID = searchresults[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Record ID ['+i+']==' + recordID);	
				
				if(recordID!=null && recordID!='' && recordID!=undefined)
				{
					var recordOBJ = ReturnItemObject(recordID)
					nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  recordOBJ ==' + recordOBJ);
									
					if(recordOBJ!=null && recordOBJ!='' && recordOBJ!=undefined)
					{
						var binLineCount = recordOBJ.getLineItemCount('binnumber');
						nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Bin Line Count ==' + binLineCount);	
						
						if(binLineCount!=null && binLineCount!='' && binLineCount!=undefined)
						{
							 for(j=1;j<=binLineCount;j++)
							 {
							 	var bin_number = recordOBJ.getLineItemValue('binnumber','binnumber',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Bin Number ==' + bin_number);
								
								var bin_onhand = recordOBJ.getLineItemValue('binnumber','onhand',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Bin onhand ==' + bin_onhand);
								
								
								if(binID == bin_number)
								{
									result.push(bin_onhand);
									break;
								}
								
								
								
								
																								
								

								
							 }//Loop							
							
						}//binLineCount						
						
					}//recordOBJ
					
				}//recordID				
				
			 }//Loop
			
		   }//SErach results 
						
		}
	 		
		
	}//Validate Check	
	
	return result;
	
}//Search Bin Number














function updateBinNumber(internalid,binID,location_on_hand)
{
	var submitID;
	
	nlapiLogExecution('DEBUG', 'updateBinNumber','  internalid*** ==' + internalid);
	
	nlapiLogExecution('DEBUG', 'updateBinNumber','  binID**** ==' + binID);
	if (internalid != null && internalid != '' && internalid != undefined) 
	{
		
		if (binID != null && binID != '' && binID != undefined) 
		{
			var recordOBJ = ReturnItemObject(internalid)
			nlapiLogExecution('DEBUG', 'updateBinNumber','  recordOBJ**** ==' + recordOBJ);
			if(recordOBJ!=null && recordOBJ!='' && recordOBJ!=undefined)
			{
				nlapiLogExecution('DEBUG', 'updateBinNumber','  Before submit**** ==' );
				
				objInvItem.setFieldValue('custitem59','T')
					
			    objInvItem.setFieldValue('usebins','T')
                             
				objInvItem.setFieldValue('custitem_bin_for','T')
					 
				objInvItem.setFieldValue('custitem_adjust_quan',LocationOnHand)
				
				
				recordOBJ.selectNewLineItem('binnumber');
				// set the item and location values on the currently selected line
				recordOBJ.setCurrentLineItemValue('binnumber', 'binnumber', binID);
				recordOBJ.setCurrentLineItemValue('binnumber', 'onhand', location_on_hand);
				
				// commit the line to the database
				recordOBJ.commitLineItem('binnumber');
				
				submitID = nlapiSubmitRecord(recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'updateBinNumber','  Submit ID ==' + submitID);
				nlapiLogExecution('DEBUG', 'updateBinNumber','  After submit**** ==' );
				
			}//recordOBJ
			
		}//binID
		
	}//internalid
	
	return submitID;
	
}//updateBinNumber





function ReturnItemObject(InternalId)	
{
	var Itemid = InternalId ;
				if(Itemid != null && Itemid != undefined && Itemid != '')
				{
				 var ItemType = getLineItemType(Itemid)
					//alert('ItemType=='+ItemType)
		
					if (ItemType == 'InvtPart')
						{
							objInvItem = nlapiLoadRecord('inventoryitem', Itemid);
						}
						else
							if (ItemType == 'Service')
							{
								objInvItem = nlapiLoadRecord('serviceitem', Itemid);
							}
							else
								if (ItemType == 'Kit')
								{
									objInvItem = nlapiLoadRecord('kititem', Itemid);
								}
								else
									if (ItemType == 'NonInvtPart')
									{
										objInvItem = nlapiLoadRecord('noninventoryitem', Itemid);
									}
									else
										if (ItemType == 'Group')
										{
											objInvItem = nlapiLoadRecord('itemgroup', Itemid);
										}
										else
											if (ItemType == 'Assembly') //changed internal id to assemblyitem
											{
												objInvItem = nlapiLoadRecord('assemblyitem', Itemid);
											}
											else
												if (ItemType == 'OthCharge') //changed internal id to otherchargeitem
												{
													objInvItem = nlapiLoadRecord('otherchargeitem', Itemid);
												}
												else
													if (ItemType == 'Payment') //changed internal id to paymentitem
													{
														objInvItem = nlapiLoadRecord('paymentitem', Itemid);
													}
		
				            }
				
	
	return objInvItem ;
}
function getLineItemType(internalId)
{
	if(internalId!='' && internalId!=null && internalId!=undefined)
	{
		var itemType;
	
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('internalid', null, 'is', internalId);
	
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('type');
	
		var searchresult = nlapiSearchRecord('item', null, filters, columns);
		if (searchresult != null && searchresult != '' && searchresult != 'undefined') {
			for (var i = 0; searchresult != null && i < searchresult.length; i++) {
				itemType = searchresult[i].getValue('type');
				nlapiLogExecution('DEBUG', 'In getLineItemType', " Item Type = " + itemType);
			}
	
		}
	 return itemType;
			
		
	}//internalId


}


function Return_searched_Bin_Number_line(internalid,binID)
{
	nlapiLogExecution('DEBUG', 'search_Bin_Number',' Return_searched_Bin_Number_line ');
	nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	Item internalid**='+internalid);
	nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	Item binID**='+binID);	
	
	var recordID;
	var result = new Array();
	if(internalid!=null && internalid!='' && internalid!=undefined)
	{
		if(binID!=null && binID!='' && binID!=undefined)
		{
		   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
				
		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   
		  var searchresults = nlapiSearchRecord('item',null, filter, columns);
		  
		   if(searchresults!=null)
		   {
	   		 for(i=0;i<searchresults.length;i++)
			 {			 	
				recordID = searchresults[i].getValue('internalid');
				
				if(recordID!=null && recordID!='' && recordID!=undefined)
				{
					var recordOBJ = ReturnItemObject(recordID)
									
					if(recordOBJ!=null && recordOBJ!='' && recordOBJ!=undefined)
					{
						var binLineCount = recordOBJ.getLineItemCount('binnumber');
						
						if(binLineCount!=null && binLineCount!='' && binLineCount!=undefined)
						{
							 for(j=1;j<=binLineCount;j++)
							 {
							 	var bin_number = recordOBJ.getLineItemValue('binnumber','binnumber',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number','  Bin Number ==' + bin_number);
								
								
								var bin_onhand = recordOBJ.getLineItemValue('binnumber','onhand',j);
								nlapiLogExecution('DEBUG', 'search_Bin_Number','  Bin onhand ==' + bin_onhand);
								
							
																								
								if(binID == bin_number)
								{
									
									
									 nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	Item binID**='+binID);
									 nlapiLogExecution('DEBUG', 'SearchInventoryRecords', '	Item bin_number**='+bin_number);
									 return j;
									break;
								}

								
							 }//Loop							
							
						}//binLineCount						
						
					}//recordOBJ
					
				}//recordID				
				
			 }//Loop
			
		   }//SErach results 
						
		}
	 		
		
	}//Validate Check	
	
	
	
}//Search Bin Number


function UpdateItemForNegative(internalid,binID)
{
	nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand',' search_Bin_Number ');	
	
	var recordID;
	var result = new Array();
	if(internalid!=null && internalid!='' && internalid!=undefined)
	{
		if(binID!=null && binID!='' && binID!=undefined)
		{
		   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('internalid', null, 'is', internalid);
		//   filter[1] = new nlobjSearchFilter('binnumber', 'binnumber', 'is', binID);
				
		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   
		  var searchresults = nlapiSearchRecord('item',null, filter, columns);
		  
		   if(searchresults!=null)
		   {
	   		 for(i=0;i<searchresults.length;i++)
			 {			 	
				recordID = searchresults[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Record ID ['+i+']==' + recordID);	
				
				if(recordID!=null && recordID!='' && recordID!=undefined)
				{
					var recordOBJ = ReturnItemObject(recordID)
					nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  recordOBJ ==' + recordOBJ);
									
					if(recordOBJ!=null && recordOBJ!='' && recordOBJ!=undefined)
					{
						var binLineCount = recordOBJ.getLineItemCount('binnumber');
						nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  Bin Line Count ==' + binLineCount);	
						
						if(binLineCount!=null && binLineCount!='' && binLineCount!=undefined)
						{
							 for(j=1;j<=binLineCount;j++)
							 {
							 	var bin_number = recordOBJ.getLineItemValue('binnumber','binnumber',j);
								recordOBJ.removeLineItem('binnumber',1);
								
							 }//Loop							
							
						}//binLineCount	
						
						recordOBJ.setFieldValue('usebins','F')					
						var updatedItemId= nlapiSubmitRecord(recordOBJ,true,true)
								
						nlapiLogExecution('DEBUG', 'search_Bin_Number_on_hand','  updatedItemId ==' + updatedItemId);	
					}//recordOBJ
					
				}//recordID				
				
			 }//Loop
			
		   }//SErach results 
						
		}
	 		
		
	}//Validate Check	
	
	return updatedItemId;
	
}//Search Bin Number