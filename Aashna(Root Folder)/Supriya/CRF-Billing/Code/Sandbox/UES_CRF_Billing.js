/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_CRF_Billing.js
	Date        : 15 May 2013
	Description : 1. Create Invoice on save of Sales order for Phase/Task items (Setup,Localization, Maintenance and Decommissioning) and non inventory items
                  2. Create Credit Memo for the created Invoice.      

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_billing(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	
	var remove_array=new Array();
	var non_remove_array=new Array();
	
	if(type=='create'|| type=='copy')
	{
		try
		{
			var i_salesorderID=nlapiGetRecordId();
			nlapiLogExecution('DEBUG',' Sales Order ', ' ****************** Sales Order ID ****************** --> '+i_salesorderID);
							
			if(i_salesorderID!='' && i_salesorderID!=null && i_salesorderID!=undefined)
			{
				var o_salesOBJ=nlapiLoadRecord('salesorder',i_salesorderID);
				nlapiLogExecution('DEBUG',' Sales Order ', ' Sales Order Object --> '+o_salesOBJ);
								
				if(o_salesOBJ!=null && o_salesOBJ!='' && o_salesOBJ!=undefined)
				{
					
				 var o_invoice_transform=nlapiTransformRecord('salesorder',i_salesorderID,'invoice');
				 nlapiLogExecution('DEBUG',' Sales Order --> Invoice Transform  ', ' Sales Order to Invoice Transrorm --> '+o_invoice_transform);
										
					 if(o_invoice_transform!=null && o_invoice_transform!='' && o_invoice_transform!=undefined)
					 {
					 	 var i_invoiceID = nlapiSubmitRecord(o_invoice_transform, true);
						 nlapiLogExecution('DEBUG',' Transform Invoice ID  ', ' ****************** Invoice ID ****************** --> '+i_invoiceID);
						 						 
						 if(i_invoiceID!=null && i_invoiceID!='' && i_invoiceID!=undefined)
						 {
						 	var o_invoiceOBJ=nlapiLoadRecord('invoice',i_invoiceID);
							nlapiLogExecution('DEBUG',' Invoice Object  ', ' Invoice Object --> '+o_invoiceOBJ);
							
							if(o_invoiceOBJ!=null && o_invoiceOBJ!='' && o_invoiceOBJ!=undefined)
							{			
							
						
							 							   					
								var i_invoice_line_count=o_invoiceOBJ.getLineItemCount('item');
								nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Line Count --> '+i_invoice_line_count);
								
								if(i_invoice_line_count!=null && i_invoice_line_count!='' && i_invoice_line_count!=undefined)
								{
									for(var i=1;i<=i_invoice_line_count;i++)
									{
										var i_itemID=o_invoiceOBJ.getLineItemValue('item','item',i);
										nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item ID --> '+i_itemID);
								
								        var s_item_display=o_invoiceOBJ.getLineItemValue('item','item_display',i);
										nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Display --> '+s_item_display);
								         										 
										var s_item_type=o_invoiceOBJ.getLineItemValue('item','itemtype',i);
										nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Type --> '+s_item_type);
										
										
										if (s_item_display != null && s_item_display != '' && s_item_display != undefined) 
										{
											s_item_display = s_item_display.toLowerCase();
											
											//============================
											
											var s_array=new Array();
											
											//s_array.push(s_item_display);
											nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Array  --> '+s_array);
																																
											
											s_array=s_item_display.split(' ')
											
											nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Array First Element   --> '+s_array[0]);
											//===================================
																					
											
																						
											var setup = setup;
											var decommissioning = decommissioning;
											var localization = localization;
											var maintenance = maintenance;
											
											/*
                                            nlapiLogExecution('DEBUG',' Invoice ', ' Lower Case Item Display  --> '+s_item_display);
											nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(setup)  --> '+s_item_display.match(/setup/));
											nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(decommissioning)  --> '+s_item_display.match(/decommissioning/));
											nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(localization) --> '+s_item_display.match(/localization/));
											nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(maintenance)  --> '+s_item_display.match(/maintenance/));
											nlapiLogExecution('DEBUG',' Invoice ', ' s_item_type != NonInvtPart  --> '+s_item_type != 'NonInvtPart');
                                           */
											
											
                                            if ((s_item_type == 'NonInvtPart') || (s_array[0].match(/setup/) == 'setup') || (s_array[0].match(/decommissioning/) == 'decommissioning') || (s_array[0].match(/localization/) == 'localization') || (s_array[0].match(/maintenance/) == 'maintenance')) 
											{									
												non_remove_array.push(i_itemID);
											
												
											}//Remove Line Item
											else
											{
												remove_array.push(i_itemID);
												o_invoiceOBJ.removeLineItem('item',i);
												i--;
											}
											                                          
										}
										
									}//Loop Invoice Item
									
									
									nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Do Not remove Array   --> '+non_remove_array);
									nlapiLogExecution('DEBUG',' Invoice ', ' Invoice remove Array   --> '+remove_array);
									
									
									o_invoiceOBJ.setFieldValue('custbody_billing_inv_created','T');
									
									var i_invoice_submitID=nlapiSubmitRecord(o_invoiceOBJ,true);								
									nlapiLogExecution('DEBUG',' Invoice ', ' ******************* Invoice Submit ID ****************** --> '+i_invoice_submitID);
									
									if(i_invoice_submitID!=null && i_invoice_submitID!='' && i_invoice_submitID!=undefined)
									{
										 var o_creditmemo_transform=nlapiTransformRecord('invoice',i_invoice_submitID,'creditmemo');
				                         nlapiLogExecution('DEBUG',' Invoice --> Credit Memo Transform  ', ' Credit Memo Transform --> '+o_creditmemo_transform);
										
					                     if (o_creditmemo_transform != null && o_creditmemo_transform != '' && o_creditmemo_transform != undefined) 
										 {
										 	var i_creditmemoID = nlapiSubmitRecord(o_creditmemo_transform, true);
										 	nlapiLogExecution('DEBUG', ' Transform Credit Memo ID  ', ' ****************** Credit Memo ID ****************** --> ' + i_creditmemoID);
										 
											     if (i_creditmemoID != null && i_creditmemoID != '' && i_creditmemoID != undefined) 
												 {
												 	var o_creditmemoOBJ = nlapiLoadRecord('creditmemo', i_creditmemoID);
												 	nlapiLogExecution('DEBUG', ' Credit Memo   ', ' Credit Memo Object --> ' + o_creditmemoOBJ);
												 	
													 	if (o_creditmemoOBJ != null && o_creditmemoOBJ != '' && o_creditmemoOBJ != undefined) 
														{															
															o_creditmemoOBJ.setFieldValue('custbody_billing_cm_created','T');
														 
														     var i_creditmemo_submitID=nlapiSubmitRecord(o_creditmemoOBJ,true);								
									                         nlapiLogExecution('DEBUG',' Credit Memo ', ' ******************* Credit Memo Submit ID ****************** --> '+i_creditmemo_submitID);
										
														
														}//Credit Memo Object
														
											 }// Credit Memo ID
										 				 										 
										 }//Credit Memo Transform 
										
									}//Invoice Submit ID
									
									
								}//Invoice Count	
																
							}//Invoice Object							
							
						 }//Invoice ID
						
					 }//Invoice Transform Check					
					
				}//sales Order Object
								
			}//Sales Order ID 
						
		}//Try
		catch(e)
		{
			nlapiLogExecution('DEBUG',' ERROR ', ' Exception Caught --> '+e);
					
		}//Catch	
		
		
	}//Create 
	
	
	else if(type=='edit')
	{
		try
		{
			
			var add_line_array=new Array();
			var no_add_line_array=new Array();
			
			var i_salesorderID=nlapiGetRecordId();
			nlapiLogExecution('DEBUG',' Sales Order ', ' ****************** Sales Order ID ****************** --> '+i_salesorderID);
			
			
			if(i_salesorderID!='' && i_salesorderID!=null && i_salesorderID!=undefined)
			{
				var o_salesOBJ=nlapiLoadRecord('salesorder',i_salesorderID);
				nlapiLogExecution('DEBUG',' Sales Order ', ' Sales Order Object --> '+o_salesOBJ);
								
				if (o_salesOBJ != null && o_salesOBJ != '' && o_salesOBJ != undefined)
				{
				  var i_relatedRecords_salesorder = o_salesOBJ.getLineItemCount('links')
				  nlapiLogExecution('DEBUG',' Sales Order ', ' Sales Order Related Records --> '+i_relatedRecords_salesorder);
				
				  if (i_relatedRecords_salesorder != null && i_relatedRecords_salesorder != '' && i_relatedRecords_salesorder != undefined)
				  {				  	
						for(var a=1;a<=i_relatedRecords_salesorder;a++)
						{
							
						   var s_related_records_type =o_salesOBJ.getLineItemValue('links','type',a)
		    	           nlapiLogExecution('DEBUG',  ' Sales Order ', 'Sales Order Related Records Type --> ' + s_related_records_type);
				
				           var i_related_records_ID = o_salesOBJ.getLineItemValue('links','id',a)
		    	           nlapiLogExecution('DEBUG', 'Sales Order ', 'Sales Order Related Records ID --> ' + i_related_records_ID); 
							
						    if(s_related_records_type=='Invoice')
							{
								  var i_invoiceOBJ = nlapiLoadRecord('invoice',i_related_records_ID)
					              nlapiLogExecution('DEBUG', ' Invoice Object', ' Invoice OBject --> ' + i_invoiceOBJ);
								
								  if(i_invoiceOBJ!='' && i_invoiceOBJ!=null && i_invoiceOBJ!=undefined)
								  {
								  		var i_invoice_line_count=i_invoiceOBJ.getLineItemCount('item');
								        nlapiLogExecution('DEBUG', ' Invoice ', ' Invoice Item Count --> ' + i_invoice_line_count);
								         
										var b_invoice_billing_created = i_invoiceOBJ.getFieldValue('custbody_billing_inv_created');
								        nlapiLogExecution('DEBUG', ' Invoice ', ' Invoice Billing Created --> ' + b_invoice_billing_created);
																		
									    var i_relatedRecords_invoice = i_invoiceOBJ.getLineItemCount('links');
								        nlapiLogExecution('DEBUG', ' Invoice ', ' Invoice Related Records  --> ' + i_relatedRecords_invoice);
								
								        if(b_invoice_billing_created == 'T' )
										{
											if(i_relatedRecords_invoice!='' && i_relatedRecords_invoice!=null && i_relatedRecords_invoice!=undefined)
											{
												for (var y = 1; y <= i_relatedRecords_invoice; y++) 
												{
													 var s_related_records_invtype = i_invoiceOBJ.getLineItemValue('links', 'type', y)
												     nlapiLogExecution('DEBUG', ' Invoice ', ' Invoice Related Records Type --> ' + s_related_records_invtype);
												
												     var i_related_records_CM_ID = i_invoiceOBJ.getLineItemValue('links', 'id', y)
												     nlapiLogExecution('DEBUG', 'Invoice ', ' Invoice Related Records ID --> ' + i_related_records_CM_ID);
											
													  if(s_related_records_invtype=='Credit Memo')
													  {
													  	var o_creditmemoOBJ = nlapiLoadRecord('creditmemo',i_related_records_CM_ID)
					                                    nlapiLogExecution('DEBUG', ' Credit Memo Object', ' Credit Memo OBject --> ' + o_creditmemoOBJ);
														
														if (o_creditmemoOBJ != null && o_creditmemoOBJ != '' && o_creditmemoOBJ != undefined) 
														{
															var b_creditmemo_billing_created = o_creditmemoOBJ.getFieldValue('custbody_billing_cm_created');
															nlapiLogExecution('DEBUG', ' Credit Memo Object', ' Credit Memo Billing Created  --> ' + b_creditmemo_billing_created);
														
														    if(b_creditmemo_billing_created == 'T')
															{
																nlapiDeleteRecord('creditmemo',i_related_records_CM_ID);
														        nlapiLogExecution('DEBUG', 'Credit Memo ', ' ********* Credit Memo Deleted --> ' + i_related_records_CM_ID);
														
																
															}//Credit Memo Billing Created 
															
															
														}//Credit Memo Object
													  	
														
													  }//  Credit Memo Related Records Check
														
													
												}//Invoice RElated Records Loop
												
												
											}//Invoice RElated REcords Check
											
											
											
											nlapiDeleteRecord('invoice',i_related_records_ID);
											nlapiLogExecution('DEBUG', ' Invoice ', ' ********* Invoice Deleted --> ' + i_related_records_ID);
											
										}// Invoice Billing Created								
								
									
								  }// Invoice Object 						  
								
							}//Invoice Related Record Type Check
														
							
						}//Sales Order Related Records Loop
										
				  }//Sales Order Related Records Check 
				
				  	transform_record(i_salesorderID);
					nlapiLogExecution('DEBUG', ' Invoice + Credit Memo ', ' ****************  Invoice + Credit Memo Transformed ****************');
										
				 
				 		
				
				}//Sales Order Object				
				
			}//Sales Order ID
			
			
		}//Try
		catch(er)
		{
		    nlapiLogExecution('DEBUG',' ERROR ', ' Exception Caught --> '+er);
		}//Catch
		
		
		
	}
	
	
	
	
	
	return true;

}

// END AFTER SUBMIT ===============================================





function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
      {  
     for(var j=0; j<array.length;j++ )
       {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}



function transform_record(i_salesorderID)
{
	var remove_array=new Array();
	var non_remove_array=new Array();
	
	
    if(i_salesorderID!='' && i_salesorderID!=null && i_salesorderID!=undefined)
	{
		var o_salesOBJ=nlapiLoadRecord('salesorder',i_salesorderID);
		nlapiLogExecution('DEBUG',' Sales Order ', ' Sales Order Object --> '+o_salesOBJ);
						
		if(o_salesOBJ!=null && o_salesOBJ!='' && o_salesOBJ!=undefined)
		{
			
		 var o_invoice_transform=nlapiTransformRecord('salesorder',i_salesorderID,'invoice');
		 nlapiLogExecution('DEBUG',' Sales Order --> Invoice Transform  ', ' Sales Order to Invoice Transrorm --> '+o_invoice_transform);
								
			 if(o_invoice_transform!=null && o_invoice_transform!='' && o_invoice_transform!=undefined)
			 {
			 	 var i_invoiceID = nlapiSubmitRecord(o_invoice_transform, true);
				 nlapiLogExecution('DEBUG',' Transform Invoice ID  ', ' ****************** Invoice ID ****************** --> '+i_invoiceID);
				 						 
				 if(i_invoiceID!=null && i_invoiceID!='' && i_invoiceID!=undefined)
				 {
				 	var o_invoiceOBJ=nlapiLoadRecord('invoice',i_invoiceID);
					nlapiLogExecution('DEBUG',' Invoice Object  ', ' Invoice Object --> '+o_invoiceOBJ);
					
					if(o_invoiceOBJ!=null && o_invoiceOBJ!='' && o_invoiceOBJ!=undefined)
					{			
					
				
					 							   					
						var i_invoice_line_count=o_invoiceOBJ.getLineItemCount('item');
						nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Line Count --> '+i_invoice_line_count);
						
						if(i_invoice_line_count!=null && i_invoice_line_count!='' && i_invoice_line_count!=undefined)
						{
							for(var i=1;i<=i_invoice_line_count;i++)
							{
								var i_itemID=o_invoiceOBJ.getLineItemValue('item','item',i);
								nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item ID --> '+i_itemID);
						
						        var s_item_display=o_invoiceOBJ.getLineItemValue('item','item_display',i);
								nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Display --> '+s_item_display);
						         										 
								var s_item_type=o_invoiceOBJ.getLineItemValue('item','itemtype',i);
								nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Type --> '+s_item_type);
								
								
								if (s_item_display != null && s_item_display != '' && s_item_display != undefined) 
								{
									s_item_display = s_item_display.toLowerCase();
									
									//============================
									
									var s_array=new Array();
									
									//s_array.push(s_item_display);
									nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Array  --> '+s_array);
																														
									
									s_array=s_item_display.split(' ')
									
									nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Item Array First Element   --> '+s_array[0]);
									//===================================
																			
									
																				
									var setup = setup;
									var decommissioning = decommissioning;
									var localization = localization;
									var maintenance = maintenance;
									
									/*
                                    nlapiLogExecution('DEBUG',' Invoice ', ' Lower Case Item Display  --> '+s_item_display);
									nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(setup)  --> '+s_item_display.match(/setup/));
									nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(decommissioning)  --> '+s_item_display.match(/decommissioning/));
									nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(localization) --> '+s_item_display.match(/localization/));
									nlapiLogExecution('DEBUG',' Invoice ', ' s_item_display.indexOf(maintenance)  --> '+s_item_display.match(/maintenance/));
									nlapiLogExecution('DEBUG',' Invoice ', ' s_item_type != NonInvtPart  --> '+s_item_type != 'NonInvtPart');
                                   */
									
									
                                    if ((s_item_type == 'NonInvtPart') || (s_array[0].match(/setup/) == 'setup') || (s_array[0].match(/decommissioning/) == 'decommissioning') || (s_array[0].match(/localization/) == 'localization') || (s_array[0].match(/maintenance/) == 'maintenance')) 
									{									
										non_remove_array.push(i_itemID);
									
										
									}//Remove Line Item
									else
									{
										remove_array.push(i_itemID);
										o_invoiceOBJ.removeLineItem('item',i);
										i--;
									}
									                                          
								}
								
							}//Loop Invoice Item
							
							
							nlapiLogExecution('DEBUG',' Invoice ', ' Invoice Do Not remove Array   --> '+non_remove_array);
							nlapiLogExecution('DEBUG',' Invoice ', ' Invoice remove Array   --> '+remove_array);
							
							
							o_invoiceOBJ.setFieldValue('custbody_billing_inv_created','T');
							
							var i_invoice_submitID=nlapiSubmitRecord(o_invoiceOBJ,true);								
							nlapiLogExecution('DEBUG',' Invoice ', ' ******************* Invoice Submit ID ****************** --> '+i_invoice_submitID);
							
							if(i_invoice_submitID!=null && i_invoice_submitID!='' && i_invoice_submitID!=undefined)
							{
								 var o_creditmemo_transform=nlapiTransformRecord('invoice',i_invoice_submitID,'creditmemo');
		                         nlapiLogExecution('DEBUG',' Invoice --> Credit Memo Transform  ', ' Credit Memo Transform --> '+o_creditmemo_transform);
								
			                     if (o_creditmemo_transform != null && o_creditmemo_transform != '' && o_creditmemo_transform != undefined) 
								 {
								 	var i_creditmemoID = nlapiSubmitRecord(o_creditmemo_transform, true);
								 	nlapiLogExecution('DEBUG', ' Transform Credit Memo ID  ', ' ****************** Credit Memo ID ****************** --> ' + i_creditmemoID);
								 
									     if (i_creditmemoID != null && i_creditmemoID != '' && i_creditmemoID != undefined) 
										 {
										 	var o_creditmemoOBJ = nlapiLoadRecord('creditmemo', i_creditmemoID);
										 	nlapiLogExecution('DEBUG', ' Credit Memo   ', ' Credit Memo Object --> ' + o_creditmemoOBJ);
										 	
											 	if (o_creditmemoOBJ != null && o_creditmemoOBJ != '' && o_creditmemoOBJ != undefined) 
												{															
													o_creditmemoOBJ.setFieldValue('custbody_billing_cm_created','T');
												 
												     var i_creditmemo_submitID=nlapiSubmitRecord(o_creditmemoOBJ,true);								
							                         nlapiLogExecution('DEBUG',' Credit Memo ', ' ******************* Credit Memo Submit ID ****************** --> '+i_creditmemo_submitID);
								
												
												}//Credit Memo Object
												
									 }// Credit Memo ID
								 				 										 
								 }//Credit Memo Transform 
								
							}//Invoice Submit ID
							
							
						}//Invoice Count	
														
					}//Invoice Object							
					
				 }//Invoice ID
				
			 }//Invoice Transform Check					
			
		}//sales Order Object
						
	}//Sales Order ID 



	
	
}//Transform Record


