// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	
	
	
	
	
	
	


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecordJVCRf(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
	
	try
	{
		if(type == 'edit')
	{
	var RecId = nlapiGetRecordId();
	nlapiLogExecution('DEBUG', 'afterSubmitRecord ', 'RecId===>'+RecId );
	
	var OldRec = nlapiGetOldRecord();
	nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'OldRec===>'+OldRec );
	
	
	if(RecId != null && RecId != undefined && RecId != '')
	{
	var NewRec = nlapiLoadRecord('salesorder',RecId)	
	}
	
	if(NewRec != null && NewRec != undefined && NewRec != '')
	{
		if(OldRec != null && OldRec != undefined && OldRec != '')
		{
			
			
		var SoRelatedProject = NewRec.getFieldValue('custbody_customer_project_name')	
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'SoRelatedProject===>'+SoRelatedProject );
		
		if(SoRelatedProject != null && SoRelatedProject != undefined && SoRelatedProject != '')
		{
			
		var NewLineCount = NewRec.getLineItemCount('item');
		var OldLineCount = OldRec.getLineItemCount('item');
		
			for(var m= 1 ; m<= OldLineCount ; m++)
			{
				var OldItem = OldRec.getLineItemValue('item','item',m)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'OldItem===>'+OldItem );
				
				var OldItemAmount = OldRec.getLineItemValue('item','amount',m)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'OldItemAmount===>'+OldItemAmount );
				
				var OldLineItemIndex =  OldRec.getLineItemValue('item','custcol_srno',m)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'OldLineItemIndex===>'+OldLineItemIndex );
				
					for (var j=1 ; j<=NewLineCount; j++)	
					 {
					 	    var NewRecItem = NewRec.getLineItemValue('item','item',j)
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'NewRecItem===>'+NewRecItem );
							
							var NewRecItemAmount = NewRec.getLineItemValue('item','amount',j)
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'NewRecItemAmount===>'+NewRecItemAmount );
							
							var NewRecLineItemIndex =  NewRec.getLineItemValue('item','custcol_srno',j)
							nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'NewRecLineItemIndex===>'+NewRecLineItemIndex );
							
							
							     if(OldLineItemIndex == NewRecLineItemIndex)
								 {
								 	if(OldItem == NewRecItem)
								    {
										
										if(OldItemAmount != NewRecItemAmount)
										{
											
											var Filters = new Array();
											var Coloumns = new Array();
											
											Filters[0] = new nlobjSearchFilter('custbody_salesorder', null, 'is',RecId );
											Filters[1] = new nlobjSearchFilter('custbody_so_item', null, 'is', OldItem);
											
											Coloumns[0]= new nlobjSearchColumn('internalid')
											
											var results = nlapiSearchRecord('journalentry', null, Filters, Coloumns);
											
											
											
											
											if(results != null && results != undefined && results != '')
											{
												nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'results.length===>'+results.length );
												
												
												for (var q=0; q<results.length; q++) 	
												{
														
												var JournalId = results[q].getValue('internalid')
												nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'JournalId===>'+JournalId );
												
												
												var ProjectPercent = SearchProjectPercent(OldItem , SoRelatedProject)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'In Main Body ProjectPercent===>'+ ProjectPercent );
												
												if(ProjectPercent != null && ProjectPercent != undefined && ProjectPercent != '')
												{
													
												if(JournalId != null && JournalId != undefined && JournalId != '')
												{
													var JournalRec = nlapiLoadRecord('journalentry', JournalId)
													
													   if (JournalRec != null && JournalRec != undefined && JournalRec != '') 	
													   {
													   
													       var DefferedLineaccountParentAmount = JournalRec.getLineItemValue('line','debit',1) 
														   nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'DefferedLineaccountParentAmount===>'+DefferedLineaccountParentAmount );
														 
														   var DefferedLineaccountServiceAmount = JournalRec.getLineItemValue('line','credit',2) 
														   nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'DefferedLineaccountServiceAmount===>'+DefferedLineaccountServiceAmount );
														   
														   
														   var NewCalculatedAmount = (parseFloat(NewRecItemAmount)*( parseFloat(ProjectPercent) / parseFloat(100)))
														   nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'NewCalculatedAmount===>'+NewCalculatedAmount );
														   
														   
														   if(NewCalculatedAmount != undefined && NewCalculatedAmount != null && NewCalculatedAmount != '')
														   {
														   	
														   var NewUnbilledAmount = parseFloat(NewCalculatedAmount) - parseFloat(DefferedLineaccountServiceAmount)
														   nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'NewUnbilledAmount===>'+NewUnbilledAmount );
														   
														    nlapiLogExecution('DEBUG', 'afterSubmitRecord', '========Before setting values========' );
														 
														
															JournalRec.setLineItemValue('line', 'debit', 1, NewCalculatedAmount.toFixed(2))
															//JournalRec.setLineItemValue('line', 'credit', 2, NewRecItemAmount)
															
															
															if (NewUnbilledAmount != 0) 	
															{
																JournalRec.setLineItemValue('line', 'credit', 3, NewUnbilledAmount.toFixed(2))
															}
															 
															 var JVid = nlapiSubmitRecord(JournalRec, true, true);
														     nlapiLogExecution('DEBUG', 'afterSubmitRecord', '========After setting values JournalId ========>'+JVid );
														   	
														   }//  if(NewCalculatedAmount != undefined && NewCalculatedAmount != null && NewCalculatedAmount != '')
														   
														 
													   
													   }//  if (JournalRec != null && JournalRec != undefined && JournalRec != '') 	
													
													
													
												   }// if(JournalId != null && JournalId != undefined && JournalId != '')
													
												}// if(ProjectPercent != null && ProjectPercent != undefined && ProjectPercent != '')
	
													};
												
												
												
												
											}// if(results != null && results != undefined && results != '')
											
										}// if(OldItemAmount != NewRecItemAmount)
										
										
									}// if(OldItem == NewRecItem)
								 
								 
								 }// if(OldLineItemIndex == NewRecLineItemIndex)
							
					  
					 };// for (var j=1 ; j<=NewLineCount; j++)
				
				
				
			}// for(var i= 1 ; i<= OldLineCount ; i++)
			
		}// if(SoRelatedProject != null && SoRelatedProject != undefined && SoRelatedProject != '')
		
		
			
		
		
			
			
		}// if(NewRec != null && NewRec != undefined && NewRec != '')
		
	}
	
	
	
		
		
	}// if(NewRec != null && NewRec != undefined && NewRec != '')
	}
	catch(e)
	{
	nlapiLogExecution('DEBUG', 'afterSubmitRecord ', 'Error===>'+e );
	
	
	}
	
	
	
	
 
	
	
	
	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
function SearchProjectPercent(OldItem , SoRelatedProject)
{
	nlapiLogExecution('DEBUG', 'afterSubmitRecord', '============Inside SearchProjectPercent OldItem Id  ===>'+OldItem );
	
	var ItemRec = nlapiLoadRecord('serviceitem',OldItem)
	//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'ItemRec===>'+ItemRec );
	
	var ProjectRec = nlapiLoadRecord('job',SoRelatedProject)
	
	//var ProjectLineCount = ProjectRec.getLineItemCount('item')
	
	
	var ItemName = ItemRec.getFieldValue('itemid')
	//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'ItemName===>'+ItemName );
	
	
			var a = ItemName ;
			if (ItemName != null && ItemName != undefined && ItemName != '') 	
			{
				if (a.indexOf('Setup') > -1) 	
				{
					var text = 'Setup'
					//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'text==>' + text)
				}
				else 
					if (a.indexOf('Maintenance') > -1) 	
					{
						var text = 'Maintenance'
						//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'text==>' + text)
					}
					else 
						if (a.indexOf('Decommissioning') > -1) 	
						{
							var text = 'Decommissioning'
							//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'text==>' + text)
						}
						else 
							if (a.indexOf('Localization') > -1) 	
							{
								var text = 'Localization'
								//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'text==>' + text)
							}
				
				if (text != null && text != undefined && text != '')	
				{
					
					
					var s_SearchProjectRelatedTaskResult = SearchProjectRelatedTask(SoRelatedProject)
					
					
					 if (s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined && s_SearchProjectRelatedTaskResult != '') 	
					 {
					 
					 	nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 's_SearchProjectRelatedTaskResult length ==>' + s_SearchProjectRelatedTaskResult.length)
						for (var i = 0; i < s_SearchProjectRelatedTaskResult.length; i++)	
						 {
					 		var i_taskid = s_SearchProjectRelatedTaskResult[i].getValue('internalid');
							//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'i_taskid==>' + i_taskid)
					 		
							var o_TaskRecord = nlapiLoadRecord('projectTask', i_taskid)
							//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'o_TaskRecord==>' + o_TaskRecord)
					 		
					 		var TaskParent = o_TaskRecord.getFieldValue('parent')
							//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'TaskParent==>' + TaskParent)
					 		
					 		var i_taskText = o_TaskRecord.getFieldValue('title')
					 		//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'i_taskText==>' + i_taskText)
					 		
					 		var IsSummmaryTask = o_TaskRecord.getFieldValue('issummarytask')
							//nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'IsSummmaryTask==>' + IsSummmaryTask)
							
							var Parentatask = GetParentTask(i_taskText , IsSummmaryTask , i_taskid)
							
							nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'Parentatask==>' + Parentatask)
							nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'text==>' + text)
							if(Parentatask == text)
							{
							nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '============Inside Parentatask == text ===========')	
								
							var TaskRec = nlapiLoadRecord('projectTask',i_taskid)	
							
								if(TaskRec != null && TaskRec != undefined && TaskRec != null)
								{
								var TaskPercent = TaskRec.getFieldValue('percenttimecomplete')	
								nlapiLogExecution('DEBUG', 'Inside SearchProjectPercent', 'TaskPercent==>' + TaskPercent)
								
								return TaskPercent;
								
								break;	
									
								}// if(TaskRec != null )
								
							}// if(Parentatask == text)
					 		
					 	}// for (var i = 0; i < s_SearchProjectRelatedTaskResult.length; i++)
					 	
					 }//  if (s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined && s_SearchProjectRelatedTaskResult != '') 	
					
					
					
				
				}// if (text != null && text != undefined && text != '')
				
			}// if (ItemName != null && ItemName != undefined && ItemName != '') 	
}


function SearchProjectRelatedTask(i_projectid)
{
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('company', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	Coloumns[1]= new nlobjSearchColumn('parent')
	Coloumns[2]= new nlobjSearchColumn('title')
	
	
	
	var results = nlapiSearchRecord('projectTask', null, Filters, Coloumns);
	
	
	return results;
}



function GetParentTask(i_taskText , IsSummmaryTask , i_taskid) 	
{
nlapiLogExecution('DEBUG', 'Inside GetParentTask', '==============GetParentTask================' )




var a = i_taskText;
if(IsSummmaryTask == 'T')
{
		
	if (a.indexOf('Setup') > -1)	
	{
	  var text = 'Setup'
	  //nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
	  
	  
	  
	 return text;	 
	} 
		else if(a.indexOf('Maintenance') > -1)	
		{
		  var text = 'Maintenance'
		  //nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
		  
		  
		 return text;	 
		}
			else if(a.indexOf('Decommissioning') > -1)	
			{
			  var text = 'Decommissioning'
			 // nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
			  
				return text;	   
			}
				else if(a.indexOf('Localization') > -1)	
				{
				  var text = 'Localization'
				  //nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
				  return text;	 
				}

	
}// IsSummmaryTask == 'T' end


}



}
// END FUNCTION =====================================================
