// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI Inv Service Revenue Recoganisation
	Author:Supriya
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
   
  
  
  
  
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	
	
	
return true;	
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)	
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	////alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	try
	{
	
	
	if (name == 'custpage_project') 	
	{
	var k = 1;
	
	nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '=====================ON PROJECT =============================')
	var i_projectid = nlapiGetFieldValue('custpage_project');
	
	
	if (i_projectid != null && i_projectid != undefined && i_projectid != '') 	
	{
	
		var o_project = nlapiLoadRecord('job', i_projectid)
		
		var CustomerID = o_project.getFieldValue('parent')
		
		nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '=====================BEFORE SALES ORDER SEARCH ===============')
		var SalesorderId = Searchsalesorder(i_projectid)
		//alert('SalesorderId===>'+SalesorderId)
		
		if(SalesorderId != null && SalesorderId != undefined && SalesorderId != '')
		{
		 nlapiSetFieldValue('salesorder', SalesorderId)
		}
		
		
		
	}
	
 }
		
		

	
		if(name=='custpage_subsidiary')
		{
			var Subsidiary = nlapiGetFieldValue('custpage_subsidiary')	
			var mySubsidiaryListField = nlapiGetField('custpage_project');
		    populateSubsidiaryProjects('custpage_project' , Subsidiary)
		    nlapiSetFieldValue('custpage_project','')
	   
		}
	
	}
	catch(ex)
	{
	
	nlapiLogExecution('DEBUG','ON FIELD CHANGE','ERROR==>'+ex)	
	}
	
	
	
	
	
}
// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function SearchProjectRelatedTask(i_projectid)
{
	
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('company', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	Coloumns[1]= new nlobjSearchColumn('parent')
	Coloumns[2]= new nlobjSearchColumn('title')
	
	
	
	var results = nlapiSearchRecord('projectTask', null, Filters, Coloumns);
	
	
	return results;
}
//----------------------------------------------------------------------------------------------------------------
function Searchsalesorder(i_projectid) 	
{
	
	
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('custbody_customer_project_name', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	var results1 = nlapiSearchRecord('salesorder', null, Filters, Coloumns);
	
	
	if(results1 != null && results1 != undefined && results1 != '' )
	{
	var i_SOid = results1[0].getValue('internalid');
	return i_SOid;	
		
	}
		
}// Searchsalesorder
//--------------------------------------------------------------------------------------------------------------------------
function ForSearchingTaskAmountformSo(SalesorderId , i_taskText) 	
{
	var TaskAmt= 0 ;
	var o_SalesorederObj = nlapiLoadRecord('salesorder',SalesorderId)
	
	
	
	var i_Itemlinecount  = o_SalesorederObj.getLineItemCount('item')
	
	k= 1;
	do
	{
		
		var ItemText = o_SalesorederObj.getLineItemValue('item','item_display',k)
		
		var RevRecScheduleId = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
	    //alert('RevRecScheduleType=='+RevRecScheduleType)
		
		
			 
		if(ItemText != null && ItemText != undefined && ItemText != '')
		{
		    var ItemTextSplit = ItemText.split(' ')
			//alert('ItemTextSplit[0]==>'+ItemTextSplit[0])
			
			if(ItemTextSplit[0] == i_taskText)
			{
			    
						if (RevRecScheduleId != null && RevRecScheduleId != undefined && RevRecScheduleId != '') 	
				        {
						var RevRecScheduleObj = nlapiLoadRecord('revRecTemplate', RevRecScheduleId)
						
						var AmortizationType =RevRecScheduleObj.getFieldValue('amortizationtype')
						//alert('AmortizationType==>'+AmortizationType)
						
						if(AmortizationType == 'VARIABLE')
						{
						 TaskAmt = o_SalesorederObj.getLineItemValue('item','amount',k)
				
				         var RevRecScheduleType = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
	                     //alert('RevRecScheduleType=='+RevRecScheduleType)
				         break;	
						}
					    }
						else
						{
						TaskAmt ='False'	
						}
				
				
				
				
			}
		k++;	
		}
			
			
	}
	while(ItemText != null && ItemText != undefined && ItemText != '')
	
	
	return TaskAmt;
	
	
}// end ForSearchingTaskAmountformSo


function populateSubsidiaryProjects(mySubsidiaryListField , Subsidiary)
 {
  
  
   var filchk1= new Array();
   var columnsCheck1= new Array();
  filchk1[0]= new nlobjSearchFilter('subsidiary',null,'is',Subsidiary);
     
  columnsCheck1[0]= new nlobjSearchColumn('internalid');
  columnsCheck1[1]= new nlobjSearchColumn('companyname');

  var searchResultsCheck1 = nlapiSearchRecord('job', null, filchk1, columnsCheck1);
  //alert('searchResultsCheck1==>'+searchResultsCheck1)
  
  if( searchResultsCheck1 != null && searchResultsCheck1 != undefined && searchResultsCheck1 != '')
  {
  	
  	  nlapiInsertSelectOption(mySubsidiaryListField, '', '', true);
	  for(var i=0;  i < searchResultsCheck1.length; i++)
	   {
		    // mySubsidiaryListField.addSelectOption(searchResultsCheck1[i].getValue('internalid'),searchResultsCheck1[i].getValue('companyname'));
		    var counter = searchResultsCheck1[i].getValue('internalid');
		    var EMpnamestr = searchResultsCheck1[i].getValue('companyname')
		    
		    nlapiInsertSelectOption(mySubsidiaryListField, counter, EMpnamestr, true);
		  
		  
	   }
  	
  }//if( searchResultsCheck1 != null && searchResultsCheck1 != undefined && searchResultsCheck1 != '')
  
 }







	
	


// END FUNCTION =====================================================
