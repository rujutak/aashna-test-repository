// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:  
	Author:  Supriya
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
 		
		

       if (request.getMethod() == 'GET') 	
	   {
	   	
				try
				{
				
				/*
			    * Begin :Code for Creating CRF INV Service Revenue Recognisation Form
			    */
				nlapiLogExecution('DEBUG', 'IN SUITELET', '================Form Designing=============' );
				var f_form = nlapiCreateForm('Revenue Recognition');
				f_form.setScript('customscriptcli_inv_service_revenue_reco');
			    
						var s_Subsidiary = f_form.addField('custpage_subsidiary', 'select', 'Subsidiary', 'subsidiary');
						s_Subsidiary.setMandatory(true)
						
						var s_ProjectList = f_form.addField('custpage_project', 'select', 'Project');
						s_ProjectList.setMandatory(true)
						
						
						var s_Salesorder = f_form.addField('salesorder','select','Related Sales Order','salesorder')
						var s_JournalEntryDate = f_form.addField('custpage_date', 'date', 'Journal Entry Date');
						
									
						var checkBtn = f_form.addSubmitButton();
						
									
						/*
			             * end :Code for Creating CRF INV Service Revenue Recognisation Form
			             */
					
				      response.writePage(f_form);
				
			   
			
				}
				catch(ex)
				{
				
				nlapiLogExecution('DEBUG','ON Get Method ','ERROR==>'+ex)	
				}	
	    
		
			}// request.getMethod() == 'GET'
				else if (request.getMethod() == 'POST') 	
					{
								
							try
							{
								   
								    
								   
								   
								    nlapiLogExecution('DEBUG', 'IN SUITELET', '================POST=============' );
									
									var projecId = request.getParameter('custpage_project')
									
									var SalesorderId = request.getParameter('salesorder')
									
									var JornalEntryDate = request.getParameter('custpage_date')
									
									var Subsidiary = request.getParameter('custpage_subsidiary')
									
									var param = new Array();
									param['custscriptproject_id']= projecId;
									param['custscriptsales_order_id_2']= SalesorderId ;
									param['custscriptjournal_enter_date']= JornalEntryDate ; 
									param['custscript_subsidiary_id']= Subsidiary ; 
									
									nlapiSetRedirectURL('SUITELET','customscript_sut_rev_rec_taskitemdisplay', 'customdeploy_sut_rev_rec_tassoitemdispla', false,param);
									
									
									
									
									
							}
							catch(ex)
							{
							
							nlapiLogExecution('DEBUG','ON FIELD CHANGE','ERROR==>'+ex)	
							}
								
						
		                }// post end

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
function Searchsalesorder(i_projectid) 	
{
	
	
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('custbody_customer_project_name', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	var results1 = nlapiSearchRecord('salesorder', null, Filters, Coloumns);
	
	
	if(results1 != null && results1 != undefined && results1 != '' )
	{
	var i_SOid = results1[0].getValue('internalid');
	return i_SOid;	
		
	}
		
}// Searchsalesorder
//=========================================================================================

function SearchProjectRelatedTask(i_projectid)
{
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('company', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	Coloumns[1]= new nlobjSearchColumn('parent')
	Coloumns[2]= new nlobjSearchColumn('title')
	
	
	
	var results = nlapiSearchRecord('projectTask', null, Filters, Coloumns);
	
	
	return results;
}
//========================================================================================

function ForSearchingTaskAmountformSo(SalesorderId , i_taskText) 	
{
	var TaskAmt= 0 ;
	var o_SalesorederObj = nlapiLoadRecord('salesorder',SalesorderId)
	
	
	
	var i_Itemlinecount  = o_SalesorederObj.getLineItemCount('item')
	
	k= 1;
	do
	{
		
		var ItemText = o_SalesorederObj.getLineItemValue('item','item_display',k)
		
		var RevRecScheduleId = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
	    //alert('RevRecScheduleType=='+RevRecScheduleType)
		
		
			 
		if(ItemText != null && ItemText != undefined && ItemText != '')
		{
		    var ItemTextSplit = ItemText.split(' ')
			//alert('ItemTextSplit[0]==>'+ItemTextSplit[0])
			
			if(ItemTextSplit[0] == i_taskText)
			{
			    
						if (RevRecScheduleId != null && RevRecScheduleId != undefined && RevRecScheduleId != '') 	
				        {
						var RevRecScheduleObj = nlapiLoadRecord('revRecTemplate', RevRecScheduleId)
						
						var AmortizationType =RevRecScheduleObj.getFieldValue('amortizationtype')
						//alert('AmortizationType==>'+AmortizationType)
						
						if(AmortizationType == 'VARIABLE')
						{
						 TaskAmt = o_SalesorederObj.getLineItemValue('item','amount',k)
				
				         var RevRecScheduleType = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
	                     //alert('RevRecScheduleType=='+RevRecScheduleType)
				         break;	
						}
					    }
						else
						{
						TaskAmt ='False'	
						}
				
				
				
				
			}
		k++;	
		}
			
			
	}
	while(ItemText != null && ItemText != undefined && ItemText != '')
	
	
	return TaskAmt;
	
	
}// end ForSearchingTaskAmountformSo


//=======================================================================













//======================================================================
 /*
      if( i_projectid != null && i_projectid != undefined && i_projectid !='')
	   {
	   	
		 if (SalesorderIDIdGet != null && SalesorderIDIdGet != undefined && SalesorderIDIdGet != '')	
		 {
		 	
			 var o_project = nlapiLoadRecord('job',i_projectid)
	         var CustomerID =  o_project.getFieldValue('parent')
			 
			 
			 nlapiLogExecution('DEBUG','ON FIELD CHANGE','=====================BEFORE SALES ORDER SEARCH ===============')
			 var SalesorderId = 27976
			 //Searchsalesorder(i_projectid) 
			 //alert('SalesorderId='+SalesorderId)
			
			 s_Project.setDefaultValue('salesorder',SalesorderId)
			
			
             nlapiLogExecution('DEBUG','ON FIELD CHANGE','============BEFORE SearchProjectRelatedTask SEARCH ===========')
			 var s_SearchProjectRelatedTaskResult = SearchProjectRelatedTask(i_projectid)
			 
			 
				for (var i=0; i< s_SearchProjectRelatedTaskResult.length && s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined; i++) 
				{
						var i_taskid = s_SearchProjectRelatedTaskResult[i].getValue('internalid');	
						var i_taskText= s_SearchProjectRelatedTaskResult[i].getValue('title');	
						
						
						var o_TaskRecord = nlapiLoadRecord('projectTask',i_taskid)
						var TaskParent = o_TaskRecord.getFieldValue('parent')
						
								if(SalesorderId != null && SalesorderId != undefined && SalesorderId != '')
								{
											if(TaskParent == null)
											{
												
												var i_SOTaskAmount = ForSearchingTaskAmountformSo(SalesorderId,i_taskText) 	
												
												
														if(i_SOTaskAmount != 'False' && i_SOTaskAmount != undefined && i_SOTaskAmount != null && i_SOTaskAmount!='')
														{
															    var percenttimecomplete = o_TaskRecord.getFieldValue('percenttimecomplete')
												                var calculatedamount = parseFloat(i_SOTaskAmount)  * parseFloat(percenttimecomplete)  / parseFloat(100) 
																
																
																nlapiSelectLineItem('custpage_invoice_sublist',k)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','task',i_taskid)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','project',i_projectid)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','customer',CustomerID)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','recognised',percenttimecomplete)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','complete',0)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','completetoberecognised', parseFloat(percenttimecomplete)- parseFloat(0))
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','salesorderamount',i_SOTaskAmount)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','calculatedamount',calculatedamount)
															    nlapiCommitLineItem('custpage_invoice_sublist')
																
																k++;
														}
												
												
											}// if(TaskParent == null)
							
					         }//SalesorderId != null && SalesorderId != undefined && SalesorderId != ''
				
		           };// for End	

			
			
		 
		 }
	   	 
		
	   }
      */

}

//=======================================================================

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
