// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:  
	Author:  Supriya
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
 		
		

       if (request.getMethod() == 'GET') 	
	   {
	   	
				try
				{
				       
					  
			            // For getting parameter values
						
						nlapiLogExecution('DEBUG', 'IN SUITELET', 'GET method ==' );
						
						var context = nlapiGetContext();
     					usageBegin = context.getRemainingUsage();
     					nlapiLogExecution('DEBUG', 'schedulerFunction','usageBegin ==' + usageBegin);
						
						
						
						nlapiLogExecution('DEBUG', 'IN SUITELET', '================Settinng values on the Suitelet after redirecting URL=============' );
			    		var i_projectid = request.getParameter('custscriptproject_id');
					    nlapiLogExecution('DEBUG', 'IN SUITELET', 'i_projectid ==' + i_projectid);
					  
					    var SalesorderIDIdGet = request.getParameter('custscriptsales_order_id_2');
					    nlapiLogExecution('DEBUG', 'IN SUITELET', 'SalesorderIDIdGet ==' + SalesorderIDIdGet);
						
						var JornalEntryDate = request.getParameter('custscriptjournal_enter_date');
					    nlapiLogExecution('DEBUG', 'IN SUITELET', 'JornalEntryDate ==' + JornalEntryDate);
						
						var Subsidiary = request.getParameter('custscript_subsidiary_id');
					    nlapiLogExecution('DEBUG', 'IN SUITELET', 'Subsidiary ==' + Subsidiary);
						
						
						 /*
					    * Begin :Code for Creating CRF INV Service Revenue Recognisation Form
					    */
						
						nlapiLogExecution('DEBUG', 'IN SUITELET', '================Form Designing=============' );
				        var f_form = nlapiCreateForm('INV Service Revenue Recognisation Display');
						f_form.setScript('customscript_cli_sut_rev_rec_tasksoitem');
						var s_Subsidiary = f_form.addField('custpage_subsidiary', 'select', 'Subsidiary', 'subsidiary');
						s_Subsidiary.setDisplayType('hidden');
						s_Subsidiary.setDefaultValue(Subsidiary)
						
						var s_ProjectList = f_form.addField('custpage_project', 'text', 'Project');
						s_ProjectList.setDisplayType('hidden');
						s_ProjectList.setDefaultValue(i_projectid)
					
						var s_Salesorder = f_form.addField('salesorder','select','Realted SalesOrder','salesorder')
						s_Salesorder.setDisplayType('hidden');
						s_Salesorder.setDefaultValue(SalesorderIDIdGet)
						
						var s_JournalEntryDate = f_form.addField('custpage_date', 'date', 'Journal Entry Date');
						s_JournalEntryDate.setDisplayType('hidden');
						s_JournalEntryDate.setDefaultValue(JornalEntryDate)
						
						var SampleTab = f_form.addTab('custpage_sample_tab', 'Invoice');
						var InvoiceSubList = f_form.addSubList('custpage_invoice_sublist', 'list', 'Invoice', 'custpage_sample_tab');
						
						 InvoiceSubList.addField('selected', 'checkbox', 'Select ');
						 InvoiceSubList.addField('task', 'select', 'Task.', 'projectTask').setDisplayType('inline');
						 InvoiceSubList.addField('item', 'text', 'Item.').setDisplayType('inline');
						 InvoiceSubList.addField('project', 'select', 'Project', 'job').setDisplayType('inline');
						 InvoiceSubList.addField('customer', 'select', 'Customer', 'customer').setDisplayType('inline');
						 InvoiceSubList.addField('recognised', 'text', ' % Recog.').setDisplayType('inline');
						 InvoiceSubList.addField('complete', 'text', ' % Complete').setDisplayType('inline');
						 InvoiceSubList.addField('completetoberecognised', 'text', ' % Complete (To be recognized)').setDisplayType('inline');
						 InvoiceSubList.addField('recognizedamount', 'text', 'Recognized Amount ').setDisplayType('inline');
						 InvoiceSubList.addField('salesorderamount', 'text', 'Sales Order Amount. ').setDisplayType('inline');
						 InvoiceSubList.addField('calculatedamount', 'text', 'Calculated Amount. ').setDisplayType('inline');
						 InvoiceSubList.addMarkAllButtons();
					     nlapiLogExecution('DEBUG', 'IN SUITELET', '================End :Code for Creating CRF INV Service Revenue Recognisation Form=============' );
						
						
						 nlapiLogExecution('DEBUG', 'IN SUITELET', '================Begin :Code for Setting values in the sublist=============' );
						
							var k = 1;
							
							if (i_projectid != null && i_projectid != undefined && i_projectid != '') 	
							{
							
								var o_project = nlapiLoadRecord('job', i_projectid)
								
								var CustomerID = o_project.getFieldValue('parent')
								
								nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '=====================BEFORE SALES ORDER SEARCH ===============')
								var SalesorderId = SalesorderIDIdGet;
								
								nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '============BEFORE SearchProjectRelatedTask SEARCH ===========')
								
								var s_SearchProjectRelatedTaskResult = SearchProjectRelatedTask(i_projectid)
								
								if(s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined && s_SearchProjectRelatedTaskResult !='') 
								{
								
								for (var i = 0; i < s_SearchProjectRelatedTaskResult.length ; i++) 
								{
									var i_taskid = s_SearchProjectRelatedTaskResult[i].getValue('internalid');
									var o_TaskRecord = nlapiLoadRecord('projectTask', i_taskid)
									
									var TaskParent = o_TaskRecord.getFieldValue('parent')
									
									var i_taskText = o_TaskRecord.getFieldValue('title')
									nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '============i_taskText ==========='+i_taskText)
									
									var IsSummmaryTask = o_TaskRecord.getFieldValue('issummarytask')
									
									
									var Parentatask = GetParentTask(i_taskText , IsSummmaryTask , i_taskid)
									
									if (SalesorderId != null && SalesorderId != undefined && SalesorderId != '') 	
									{
										
										if (Parentatask == 'Maintenance' || Parentatask == 'Decommissioning' || Parentatask == 'Localization' || Parentatask == 'Setup') 	
										{
										
											var i_SOTaskAmount = ForSearchingTaskAmountformSo(SalesorderId, i_taskText)
											nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '============i_SOTaskAmount ==========='+i_SOTaskAmount)
											
											if (i_SOTaskAmount != 'False#null' && i_SOTaskAmount != '0#null') 	
											{
											
											
												if (i_SOTaskAmount != undefined && i_SOTaskAmount != null && i_SOTaskAmount != '') 	
												{
													var percenttimecomplete = o_TaskRecord.getFieldValue('percenttimecomplete')
													
													var i_SOTaskAmountSplit = i_SOTaskAmount.split('#')
													i_SOTaskAmount = i_SOTaskAmountSplit[0]
													var ItemNameSet = i_SOTaskAmountSplit[1]
													
													if(ItemNameSet != null && ItemNameSet != undefined && ItemNameSet != '')
													{
													var filters = new Array();
													var columns = new Array();
													filters[0] = new nlobjSearchFilter('custrecord_projectname', null, 'is', i_projectid);
													filters[1] = new nlobjSearchFilter('custrecord_task', null, 'is', i_taskid);
													filters[2] = new nlobjSearchFilter('custrecord_customer', null, 'is', CustomerID);
													
													columns[0] = new nlobjSearchColumn('internalid');
													var searchresult = nlapiSearchRecord('customrecord_crf_inv_service_revenue_rec', null, filters, columns);
													if (searchresult != null && searchresult != '' && searchresult != 'undefined') 	
													{
														var ExistingRevRecIntenalid = searchresult[0].getValue('internalid');
														var Rev_rec_custom_Rec = nlapiLoadRecord('customrecord_crf_inv_service_revenue_rec', ExistingRevRecIntenalid)
														
														var CustomPerRecog = Rev_rec_custom_Rec.getFieldValue('custrecord_per_recog')
														var RecognisedAmountC = Rev_rec_custom_Rec.getFieldValue('custrecord_recognised_amout')
														nlapiLogExecution('DEBUG', 'ON FIELD CHANGE', '============RecognisedAmountC*** ===========' + RecognisedAmountC)
														if (RecognisedAmountC == null || RecognisedAmountC == undefined || RecognisedAmountC == '') {
															RecognisedAmountC = 0;
														}
														
														
														//=========================================================================================
														if (CustomPerRecog != percenttimecomplete) {
															var Complete = percenttimecomplete;
															percenttimecomplete = CustomPerRecog;
														}
														else {
															var Complete = 0;
														}
														//=========================================================================================
														
														InvoiceSubList.setLineItemValue('task', k, i_taskid);
														InvoiceSubList.setLineItemValue('item', k, ItemNameSet);
														InvoiceSubList.setLineItemValue('project', k, i_projectid);
														InvoiceSubList.setLineItemValue('customer', k, CustomerID);
														InvoiceSubList.setLineItemValue('recognised', k, percenttimecomplete);
														InvoiceSubList.setLineItemValue('recognizedamount', k, RecognisedAmountC);
														
														InvoiceSubList.setLineItemValue('complete', k, Complete);
														
														var completetoberecognised = ((parseFloat(Complete) - parseFloat(CustomPerRecog)).toFixed(2))
														var completetoberecognisedAmtTemp = completetoberecognised.indexOf('-')
														
														if (completetoberecognisedAmtTemp != parseFloat(0)) {
														}
														else {
															completetoberecognised = parseFloat(completetoberecognised) * parseFloat(-1)
														}
														//End code to handel negative value calculation
														
														InvoiceSubList.setLineItemValue('completetoberecognised', k, completetoberecognised);
														InvoiceSubList.setLineItemValue('salesorderamount', k, i_SOTaskAmount);
														var calculatedamount = parseFloat((i_SOTaskAmount) * (parseFloat(completetoberecognised) / parseFloat(100)))
														InvoiceSubList.setLineItemValue('calculatedamount', k, calculatedamount.toFixed(2));
														
														
														
													}// end custom search result
													else {
														var RecognisedAmountC = 0;
														InvoiceSubList.setLineItemValue('task', k, i_taskid);
														InvoiceSubList.setLineItemValue('item', k, ItemNameSet);
														InvoiceSubList.setLineItemValue('project', k, i_projectid);
														InvoiceSubList.setLineItemValue('customer', k, CustomerID);
														InvoiceSubList.setLineItemValue('recognised', k, percenttimecomplete);
														InvoiceSubList.setLineItemValue('recognizedamount', k, RecognisedAmountC);
														InvoiceSubList.setLineItemValue('complete', k, 0);
														InvoiceSubList.setLineItemValue('completetoberecognised', k, parseFloat(percenttimecomplete) - parseFloat(0));
														InvoiceSubList.setLineItemValue('salesorderamount', k, i_SOTaskAmount);
														var calculatedamount = parseFloat(i_SOTaskAmount) * (((parseFloat(percenttimecomplete) - parseFloat(0)) / parseFloat(100))).toFixed(2)
														InvoiceSubList.setLineItemValue('calculatedamount', k, calculatedamount.toFixed(2));
													}
													
													k++;	
													}
													
												}// if end
											} // If end if (i_SOTaskAmount != 'False#null' && i_SOTaskAmount != '0#null') 	
										
										}// if(TaskParent == null)
									
									}//SalesorderId != null && SalesorderId != undefined && SalesorderId != ''
								
								  }// for End
									
								}// if s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined && '' end
									
							 }// end if(i_projectid != null && i_projectid != undefined && i_projectid != '')
								
						
						nlapiLogExecution('DEBUG', 'IN SUITELET', '================Begin :Code for Setting values in the sublist=============' );
									
						var checkBtn = f_form.addSubmitButton();
									
						/*
			             * end :Code for Creating CRF INV Service Revenue Recognisation Form
			             */
					
				        response.writePage(f_form);
				
			            nlapiLogExecution('DEBUG', 'IN SUITELET', 'End GET method ==' );
     					EndUsage = context.getRemainingUsage();
     					nlapiLogExecution('DEBUG', 'schedulerFunction','EndUsage ==' + EndUsage);
			
				}
				catch(ex)
				{
				
				nlapiLogExecution('DEBUG','ON Get Method ','ERROR==>'+ex)	
				}	
	    
		
			}// request.getMethod() == 'GET'
				
				else if (request.getMethod() == 'POST') 	
					{
								
							try
							{
								   nlapiLogExecution('DEBUG', 'IN SUITELET', 'Post method GET method ==' );  
								    nlapiLogExecution('DEBUG', 'IN SUITELET', '================POST=============' );
									
									var projecId = request.getParameter('custpage_project')
									
									var SalesorderId = request.getParameter('salesorder')
									
									var JournalEntryDate = request.getParameter('custpage_date')
									
									if(projecId != null && projecId != undefined && projecId != '')
									{
										
									var ProjectRec = nlapiLoadRecord('job', projecId)
									
									var RevenueRecognisationlinecount = request.getLineItemCount('custpage_invoice_sublist')
									nlapiLogExecution('DEBUG', 'IN SUITELET', 'RevenueRecognisationlinecount ==' + RevenueRecognisationlinecount);
									
									for (var k = 1; k <= RevenueRecognisationlinecount; k++) 	
									{
												var TaskSelected = request.getLineItemValue('custpage_invoice_sublist', 'selected', k)
												nlapiLogExecution('DEBUG', 'IN SUITELET', 'TaskSelected ==' + TaskSelected)
												
												
												
												if (TaskSelected == 'T') 	
												{
												
													nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Inside  TaskSelected==========' + TaskSelected)
													
													var Task = request.getLineItemValue('custpage_invoice_sublist', 'task', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'Task ==' + Task)
													
													var ItemName = request.getLineItemValue('custpage_invoice_sublist', 'item', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'Task ==' + Task)
													
													var Customer = request.getLineItemValue('custpage_invoice_sublist', 'customer', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'Customer ==' + Customer)
													
													var PerRecog = request.getLineItemValue('custpage_invoice_sublist', 'recognised', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'PerRecog ==' + PerRecog)
													
													var PerComplete = request.getLineItemValue('custpage_invoice_sublist', 'complete', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'PerComplete ==' + PerComplete)
													
													var PerCompletetoberecognised = request.getLineItemValue('custpage_invoice_sublist', 'completetoberecognised', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'PerCompletetoberecognised ==' + PerCompletetoberecognised)
													
													var SalesOrderAmount = request.getLineItemValue('custpage_invoice_sublist', 'salesorderamount', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'SalesOrderAmount ==' + SalesOrderAmount)
													
													var RecognizedAmount = request.getLineItemValue('custpage_invoice_sublist', 'recognizedamount', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'RecognizedAmount ==' + RecognizedAmount)
													
													var CalculatedAmount = request.getLineItemValue('custpage_invoice_sublist', 'calculatedamount', k)
													nlapiLogExecution('DEBUG', 'IN SUITELET', 'CalculatedAmount ==' + CalculatedAmount)
													
													//====================  Code for getting Correct payment item text of the task ========================
													var a = ItemName;
													if (ItemName != null && ItemName != undefined && ItemName != '') 								
													{
														if (a.indexOf('Setup') > -1) 	
														{
															var text = 'Setup'
															nlapiLogExecution('DEBUG', 'ON GetParentTask', 'text==>' + text)
														}
														else 
															if (a.indexOf('Maintenance') > -1) 	
															{
																var text = 'Maintenance'
																nlapiLogExecution('DEBUG', 'ON GetParentTask', 'text==>' + text)
															}
															else 
																if (a.indexOf('Decommissioning') > -1) 	
																{
																	var text = 'Decommissioning'
																	nlapiLogExecution('DEBUG', 'ON GetParentTask', 'text==>' + text)
																}
																else 
																	if (a.indexOf('Localization') > -1) 	
																	{
																		var text = 'Localization'
																		nlapiLogExecution('DEBUG', 'ON GetParentTask', 'text==>' + text)
																	}
														
														if (text != null && text != undefined && text != '') 	
														{
														
															var Filters = new Array();
															var Coloumns = new Array();
															
															Filters[0] = new nlobjSearchFilter('custrecord_task_name', null, 'is', text);
															
															Coloumns[0] = new nlobjSearchColumn('internalid')
															Coloumns[1] = new nlobjSearchColumn('custrecord_payment_item_name')
															
															var results = nlapiSearchRecord('customrecord_rev_rec_jv_cre_task_item_ma', null, Filters, Coloumns);
															
															if (results != null && results != undefined && results != '') 	
															{
																var ItemMappingRecordId = results[0].getValue('internalid')
																nlapiLogExecution('DEBUG', 'IN SUITELET', '=======ItemMappingRecordId==========' + ItemMappingRecordId)
																var PaymentItemName = results[0].getValue('custrecord_payment_item_name')
																nlapiLogExecution('DEBUG', 'IN SUITELET', '=======PaymentItemName==========' + PaymentItemName)
																
															}
															
															
														}
														
														
														
														
														//===================== Code For getting Subsidiary from Project ======================================
														
														
														var Subsidiary = ProjectRec.getFieldValue('subsidiary')
														nlapiLogExecution('DEBUG', 'IN SUITELET', 'Subsidiary ==' + Subsidiary)
														//======================Code for Updation & Creation of custom record ==================================
														
														
														var filters = new Array();
														var columns = new Array();
														
														filters[0] = new nlobjSearchFilter('custrecord_projectname', null, 'is', projecId);
														filters[1] = new nlobjSearchFilter('custrecord_task', null, 'is', Task);
														filters[2] = new nlobjSearchFilter('custrecord_customer', null, 'is', Customer);
														
														
														columns[0] = new nlobjSearchColumn('internalid');
														
														var searchresult = nlapiSearchRecord('customrecord_crf_inv_service_revenue_rec', null, filters, columns);
														if (searchresult != null && searchresult != '' && searchresult != 'undefined') {
														
															var ExistingRevRecIntenalid = searchresult[0].getValue('internalid');
															nlapiLogExecution('DEBUG', 'In getLineItemType', "  ExistingRevRecIntenalid = " + ExistingRevRecIntenalid);
															
															nlapiLogExecution('DEBUG', 'IN SUITELET', '=======ExistingRevRecIntenalid======== ==' + ExistingRevRecIntenalid)
															var Rev_rec_custom_Rec = nlapiLoadRecord('customrecord_crf_inv_service_revenue_rec', ExistingRevRecIntenalid)
															
															var RecognisedAmountCR = Rev_rec_custom_Rec.getFieldValue('custrecord_calculated_amount')
															
															if (RecognisedAmountCR == null && RecognisedAmountCR == undefined && RecognisedAmountCR == '') {
																RecognisedAmountCR = 0
															}
															
															Rev_rec_custom_Rec.setFieldValue('custrecord_projectname', projecId)
															Rev_rec_custom_Rec.setFieldValue('custrecord_task', Task)
															Rev_rec_custom_Rec.setFieldValue('custrecord_customer', Customer)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_recog', PerRecog)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_complete', PerComplete)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_complete_to_be_recog', PerCompletetoberecognised)
															Rev_rec_custom_Rec.setFieldValue('custrecord_sales_order_amount', SalesOrderAmount)
															Rev_rec_custom_Rec.setFieldValue('custrecord_recognised_amout', RecognisedAmountCR)
															Rev_rec_custom_Rec.setFieldValue('custrecord_calculated_amount', CalculatedAmount)
															
															
															
															Rev_rec_custom_Rec.setFieldValue('custrecord_projectname', projecId)
															
															var CustomRevRecOrdID = nlapiSubmitRecord(Rev_rec_custom_Rec, true, true)
															nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Updated CustomRevRecOrdID======== ==' + CustomRevRecOrdID)
															
															
														}
														else {
															nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Rev_rec_custom_Rec creation Part==========')
															var Rev_rec_custom_Rec = nlapiCreateRecord('customrecord_crf_inv_service_revenue_rec')
															
															Rev_rec_custom_Rec.setFieldValue('custrecord_projectname', projecId)
															Rev_rec_custom_Rec.setFieldValue('custrecord_task', Task)
															Rev_rec_custom_Rec.setFieldValue('custrecord_customer', Customer)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_recog', PerRecog)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_complete', PerComplete)
															Rev_rec_custom_Rec.setFieldValue('custrecord_per_complete_to_be_recog', PerCompletetoberecognised)
															Rev_rec_custom_Rec.setFieldValue('custrecord_sales_order_amount', SalesOrderAmount)
															Rev_rec_custom_Rec.setFieldValue('custrecord_recognised_amout', 0)
															Rev_rec_custom_Rec.setFieldValue('custrecord_calculated_amount', CalculatedAmount)
															
															var CustomRevRecOrdID = nlapiSubmitRecord(Rev_rec_custom_Rec, true, true)
															nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Created CustomRevRecOrdID======== ==' + CustomRevRecOrdID)
														}
														
														//======================Code for Creating Journal Record ==================================	
														
														var SalesOrderObj = nlapiLoadRecord('salesorder', SalesorderId)
														
														var Department = SalesOrderObj.getFieldValue('department')
														
														
														
														var InvoiceSearch_filters = new Array();
														var InvoiceSearch_columns = new Array();
														
														InvoiceSearch_filters[0] = new nlobjSearchFilter('createdfrom', null, 'is', SalesorderId);
														InvoiceSearch_columns[0] = new nlobjSearchColumn('internalid');
														
														var searchresult = nlapiSearchRecord('invoice', null, InvoiceSearch_filters, InvoiceSearch_columns);
														
														if (searchresult != null && searchresult != '' && searchresult != 'undefined') 	
														{
														
															for (var i = 0; i < searchresult.length; i++) 	
															{
															
																var InvoiceID = searchresult[i].getValue('internalid');
																nlapiLogExecution('DEBUG', 'IN SUITELET', '======= InvoiceID======== ==' + InvoiceID)
																var Invoicerecord = nlapiLoadRecord('invoice', InvoiceID)
																
																var REvRecJournalCreated = Invoicerecord.getFieldValue('custbody_crf_revenue_recognisation_cre')
																nlapiLogExecution('DEBUG', 'IN SUITELET', 'REvRecJournalCreated' + REvRecJournalCreated)
																var ItemLineCount = Invoicerecord.getLineItemCount('item')
																nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemLineCount' + ItemLineCount)
																if (REvRecJournalCreated == 'F') 	
																{
																
																	var invoiceTranId = Invoicerecord.getFieldValue('tranid')
																	nlapiLogExecution('DEBUG', 'IN SUITELET', 'invoiceTranId =' + invoiceTranId)
																	
																	if (ItemMappingRecordId != null && ItemMappingRecordId != undefined && ItemMappingRecordId != '') 
																	{
																		
																		var ItemMappingRec = nlapiLoadRecord('customrecord_rev_rec_jv_cre_task_item_ma', ItemMappingRecordId)
																		nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemMappingRec ===>' + ItemMappingRec)
																		var ItemTaskMappingPaymnetText = ItemMappingRec.getFieldValue('custrecord_payment_item_name')
																		nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemTaskMappingPaymnetText ===>' + ItemTaskMappingPaymnetText)
																		var ItemTaskMappingtaskText = ItemMappingRec.getFieldValue('custrecord_task_name')
																		nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemTaskMappingtaskText ===>' + ItemTaskMappingtaskText)
																		
																		if(ItemTaskMappingPaymnetText != null && ItemTaskMappingPaymnetText != undefined && ItemTaskMappingPaymnetText != '')
																		{
																			
																		for (var h = 1; h <= ItemLineCount; h++) 	
																		{
																			var ItemText = Invoicerecord.getLineItemValue('item', 'item_display', h)
																			nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemText' + ItemText)
																			
																			
																			if (ItemText != null && ItemText != undefined && ItemText != '') 	
																			{
																				//var ItemTextSplit = ItemText.split(':')
																				//nlapiLogExecution('DEBUG', 'IN SUITELET', 'ItemTextSplit[0]===>' + ItemTextSplit[0])
																				//if (ItemTextSplit[0] == ItemTaskMappingPaymnetText)
																				//var searchStr = ItemTaskMappingPaymnetText; 
                                                                                // var strRegExPattern = '/'+searchStr+'\b/'; 
                                                                                //var Mtcheddata= ItemText.match(new RegExp(strRegExPattern,'g'));
																				
																				
																				var flag = 0
																				if (ItemText.indexOf(ItemTaskMappingPaymnetText) > -1) 	
																				{
																					flag =  1;
																					nlapiLogExecution('DEBUG', 'ON GetParentTask', '============****Found***===========')
																				}
																				else
																				{
																					flag =  0;
																					nlapiLogExecution('DEBUG', 'ON GetParentTask', '============****Not Found***===========')
																					
																				}
																				
																				if(flag == 1)
																				{
																					var InvoiceAmount = Invoicerecord.getLineItemValue('item', 'amount', h)
																					nlapiLogExecution('DEBUG', 'IN SUITELET', 'InvoiceAmount' + InvoiceAmount)
																					var UnbilledAmount = parseFloat(CalculatedAmount) - parseFloat(InvoiceAmount)
																					nlapiLogExecution('DEBUG', 'IN SUITELET', 'UnbilledAmount' + UnbilledAmount)
																					
																					var Journal = nlapiCreateRecord('journalentry')
																					
																					Journal.setFieldValue('subsidiary', Subsidiary)
																					if (JournalEntryDate != null && JournalEntryDate != undefined && JournalEntryDate != '') {
																						Journal.setFieldValue('trandate', JournalEntryDate)
																					}
																					Journal.setFieldValue('custbody_invoice', InvoiceID)
																					Journal.setFieldValue('custbody_salesorder', SalesorderId)
																					Journal.setLineItemValue('line', 'account', 1, 582)
																					Journal.setLineItemValue('line', 'debit', 1, CalculatedAmount)
																					Journal.setLineItemValue('line', 'department', 1, Department)
																					Journal.setLineItemValue('line', 'memo', 1, '% complete of setup task')
																					
																					
																					Journal.setLineItemValue('line', 'account', 2, 583)
																					Journal.setLineItemValue('line', 'credit', 2, InvoiceAmount)
																					Journal.setLineItemValue('line', 'department', 2, Department)
																					var MemoText = "Invoice " + ItemText + " Amount Invoice: " + invoiceTranId
																					Journal.setLineItemValue('line', 'memo', 2, MemoText)
																					
																					
																					if (UnbilledAmount != 0) {
																						Journal.setLineItemValue('line', 'account', 3, 271)
																						Journal.setLineItemValue('line', 'credit', 3, UnbilledAmount.toFixed(2))
																						Journal.setLineItemValue('line', 'department', 3, Department)
																					}
																					var JVid = nlapiSubmitRecord(Journal, true, true);
																					nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Created Journal record ======== ==' + JVid)
																					
																					//========Custom REV REC Schedule Journal Data record creation ===========================
																					
																					if (JVid != null && JVid != '' && JVid != undefined) 	
																					{
																						var JVREC = nlapiLoadRecord('journalentry', JVid)
																						var JVDate = JVREC.getFieldValue('trandate')
																						
																						var RevRecScheduleJVDataObj = nlapiCreateRecord('customrecord_rev_rec_journal_data')
																						
																						RevRecScheduleJVDataObj.setFieldValue('custrecord_journalsales_orderid', SalesorderId)
																						RevRecScheduleJVDataObj.setFieldValue('custrecord_journal_invoiceid', InvoiceID)
																						RevRecScheduleJVDataObj.setFieldValue('custrecord_journal_date_created', JVDate)
																						RevRecScheduleJVDataObj.setFieldValue('custrecord_custom_crf_rev_rec_id', CustomRevRecOrdID)
																						RevRecScheduleJVDataObj.setFieldValue('custrecord_journal_id', JVid)
																						
																						var JVDAtaId = nlapiSubmitRecord(RevRecScheduleJVDataObj, true, true)
																						nlapiLogExecution('DEBUG', 'IN SUITELET', '=======Created JVDAtaId record ======== ==' + JVDAtaId)
																					}// if (JVid != null && JVid != '' && JVid != undefined) 	
																					
																				}// if (ItemTextSplit[0] == 'Payment')
																				
																			}// if (ItemText != null && ItemText != undefined && ItemText != '') 
																			
																			
																		}//FOR END (var h = 1; h <= ItemLineCount; h++) 
																			
																		}// if(ItemTaskMappingPaymnetText != null && ItemTaskMappingPaymnetText != undefined && ItemTaskMappingPaymnetText != '')
																		
																		
																		
																			
																		
																	}// if (ItemMappingRecordId != null && ItemMappingRecordId != undefined && ItemMappingRecordId != '')  IF END
																	
																	
																}// If item Mapping Record Null if caheck end
																Invoicerecord.setFieldValue('custbody_crf_revenue_recognisation_cre', 'T')
																var InvoiceUpdatedid = nlapiSubmitRecord(Invoicerecord, true, true);
																nlapiLogExecution('DEBUG', 'IN SUITELET', '=======InvoiceUpdatedid======== ==' + InvoiceUpdatedid)
																
															};// for (var i = 0; i < searchresult.length; i++) 
															
															
														}// if (searchresult != null && searchresult != '' && searchresult != 'undefined') 	
														
													}/// if (ItemName != null && ItemName != undefined && ItemName != '') 
													
												} // TaskSelected == 'T' end
								
							        }// for (var k = 1; k <= RevenueRecognisationlinecount; k++) 
										
								}// if(projecId != null && projecId != undefined && projecId != '')
							
							var param = new Array();
							param['custscript_projectid']= projecId;
							param['custscript_sales_order_id']= SalesorderId ;
							nlapiSetRedirectURL('SUITELET','customscript_sut_inv_crf_service_rev_rec', 'customdeploy_sut_inv_crf_invoice_revenue', false,param);	
							}
							catch(ex)
							{
							
							nlapiLogExecution('DEBUG','ON FIELD CHANGE','ERROR==>'+ex)	
							}
								
						
		                }// post end


}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
function Searchsalesorder(i_projectid) 	
{
	
	
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('custbody_customer_project_name', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	var results1 = nlapiSearchRecord('salesorder', null, Filters, Coloumns);
	
	
	if(results1 != null && results1 != undefined && results1 != '' )
	{
	var i_SOid = results1[0].getValue('internalid');
	return i_SOid;	
		
	}
		
}// Searchsalesorder
//=========================================================================================

function SearchProjectRelatedTask(i_projectid)
{
	var Filters = new Array();
	var Coloumns = new Array();
	
	Filters[0] = new nlobjSearchFilter('company', null, 'is',i_projectid );
	Coloumns[0]= new nlobjSearchColumn('internalid')
	Coloumns[1]= new nlobjSearchColumn('parent')
	Coloumns[2]= new nlobjSearchColumn('title')
	
	
	
	var results = nlapiSearchRecord('projectTask', null, Filters, Coloumns);
	
	
	return results;
}
//========================================================================================

function ForSearchingTaskAmountformSo(SalesorderId , i_taskText) 	
{
	var TaskAmt= 0 ;
	var o_SalesorederObj = nlapiLoadRecord('salesorder',SalesorderId)
	
	
	
	var i_Itemlinecount  = o_SalesorederObj.getLineItemCount('item')
	
	k= 1;
	do
	{
		
		var ItemText = o_SalesorederObj.getLineItemValue('item','item_display',k)
		
		var RevRecScheduleId = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
	    //alert('RevRecScheduleType=='+RevRecScheduleType)
		
			 
		if(ItemText != null && ItemText != undefined && ItemText != '')
		{
		    var ItemTextSplit = ItemText.split(' ')
			//alert('ItemTextSplit[0]==>'+ItemTextSplit[0])
			
			if(ItemTextSplit[0] == i_taskText)
			{
			    
						if (RevRecScheduleId != null && RevRecScheduleId != undefined && RevRecScheduleId != '') 	
				        {
						var RevRecScheduleObj = nlapiLoadRecord('revRecTemplate', RevRecScheduleId)
						
						var AmortizationType =RevRecScheduleObj.getFieldValue('amortizationtype')
						//alert('AmortizationType==>'+AmortizationType)
						
						    if(AmortizationType == 'VARIABLE')
							{
							 TaskAmt = o_SalesorederObj.getLineItemValue('item','amount',k)
					
					         var RevRecScheduleType = o_SalesorederObj.getLineItemValue('item','revrecschedule',k)
		                     //alert('RevRecScheduleType=='+RevRecScheduleType)
					         break;	
							}
							else
							{
							TaskAmt ='False'	
							}
					    }
						
						else
						{
						TaskAmt ='False'	
						}
				
				
				
				
			}
		k++;	
		}
			
			
	}
	while(ItemText != null && ItemText != undefined && ItemText != '')
	
	
	return TaskAmt+'#'+ItemText;
	
	
}// end ForSearchingTaskAmountformSo


//============================================================================
function GetParentTask(i_taskText , IsSummmaryTask , i_taskid) 	
{
nlapiLogExecution('DEBUG','ON GetParentTask','i_taskText==>'+i_taskText)
nlapiLogExecution('DEBUG','ON GetParentTask','i_taskText==>'+i_taskText)
nlapiLogExecution('DEBUG','ON GetParentTask','IsSummmaryTask==>'+IsSummmaryTask)

var a = i_taskText;
if(IsSummmaryTask == 'T')
{
		
	if (a.indexOf('Setup') > -1)	
	{
	  var text = 'Setup'
	  nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
	  
	  
	  
	 return text;	 
	} 
		else if(a.indexOf('Maintenance') > -1)	
		{
		  var text = 'Maintenance'
		  nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
		  
		  
		 return text;	 
		}
			else if(a.indexOf('Decommissioning') > -1)	
			{
			  var text = 'Decommissioning'
			  nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
			  
				return text;	   
			}
				else if(a.indexOf('Localization') > -1)	
				{
				  var text = 'Localization'
				  nlapiLogExecution('DEBUG','ON GetParentTask','text==>'+text)
				  return text;	 
				}

	
}// IsSummmaryTask == 'T' end


}

//=============================================================================









//======================================================================
 /*
      if( i_projectid != null && i_projectid != undefined && i_projectid !='')
	   {
	   	
		 if (SalesorderIDIdGet != null && SalesorderIDIdGet != undefined && SalesorderIDIdGet != '')	
		 {
		 	
			 var o_project = nlapiLoadRecord('job',i_projectid)
	         var CustomerID =  o_project.getFieldValue('parent')
			 
			 
			 nlapiLogExecution('DEBUG','ON FIELD CHANGE','=====================BEFORE SALES ORDER SEARCH ===============')
			 var SalesorderId = 27976
			 //Searchsalesorder(i_projectid) 
			 //alert('SalesorderId='+SalesorderId)
			
			 s_Project.setDefaultValue('salesorder',SalesorderId)
			
			
             nlapiLogExecution('DEBUG','ON FIELD CHANGE','============BEFORE SearchProjectRelatedTask SEARCH ===========')
			 var s_SearchProjectRelatedTaskResult = SearchProjectRelatedTask(i_projectid)
			 
			 
				for (var i=0; i< s_SearchProjectRelatedTaskResult.length && s_SearchProjectRelatedTaskResult != null && s_SearchProjectRelatedTaskResult != undefined; i++) 
				{
						var i_taskid = s_SearchProjectRelatedTaskResult[i].getValue('internalid');	
						var i_taskText= s_SearchProjectRelatedTaskResult[i].getValue('title');	
						
						
						var o_TaskRecord = nlapiLoadRecord('projectTask',i_taskid)
						var TaskParent = o_TaskRecord.getFieldValue('parent')
						
								if(SalesorderId != null && SalesorderId != undefined && SalesorderId != '')
								{
											if(TaskParent == null)
											{
												
												var i_SOTaskAmount = ForSearchingTaskAmountformSo(SalesorderId,i_taskText) 	
												
												
														if(i_SOTaskAmount != 'False' && i_SOTaskAmount != undefined && i_SOTaskAmount != null && i_SOTaskAmount!='')
														{
															    var percenttimecomplete = o_TaskRecord.getFieldValue('percenttimecomplete')
												                var calculatedamount = parseFloat(i_SOTaskAmount)  * parseFloat(percenttimecomplete)  / parseFloat(100) 
																
																
																nlapiSelectLineItem('custpage_invoice_sublist',k)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','task',i_taskid)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','project',i_projectid)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','customer',CustomerID)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','recognised',percenttimecomplete)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','complete',0)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','completetoberecognised', parseFloat(percenttimecomplete)- parseFloat(0))
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','salesorderamount',i_SOTaskAmount)
																InvoiceSubList.setCurrentLineItemValue('custpage_invoice_sublist','calculatedamount',calculatedamount)
															    nlapiCommitLineItem('custpage_invoice_sublist')
																
																k++;
														}
												
												
											}// if(TaskParent == null)
							
					         }//SalesorderId != null && SalesorderId != undefined && SalesorderId != ''
				
		           };// for End	

			
			
		 
		 }
	   	 
		
	   }
      */
	 
	 
	 /*
var TaskRecord = nlapiLoadRecord('projectTask',i_taskid)
	  var Parent = TaskRecord.getFieldValue('parent')
	  
	  if(Parent == null && Parent != undefined && Parent != '')
      {
	  	var Subrecord = nlapiLoadRecord('projectTask',Parent)
		var ParentText  = Subrecord.getFieldValue('title')
		
		if (ParentText.indexOf('Setup') > -1)
		{
		  
		}
		else
		{
		return text;		
		}
		
	  }	
	  else
		  {
		  return text;	
		  }  
*/

}

//=======================================================================

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
