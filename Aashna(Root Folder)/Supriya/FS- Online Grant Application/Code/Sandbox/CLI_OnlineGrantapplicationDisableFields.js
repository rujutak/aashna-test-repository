// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI OnlineGrantApplication Set FieldsVal
	Author:
	Company:
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
   
  
  
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
    
   
   
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateFieldSetFieldValue(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY
	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

	
	if( type == 'recmachcustrecord_grant_application_link' && name=='custrecord_fs_grant_item_salary_supp_quo')
	{
		
		
		
				var SalaryQuote = nlapiGetCurrentLineItemValue('recmachcustrecord_grant_application_link','custrecord_fs_grant_item_salary_supp_quo')
				//alert('SalaryQuote='+SalaryQuote)
				
						if(SalaryQuote == 1)
						{
						   
						   //=======================Setting Fields Hidden==============================
						   
						   
						   var Supplier = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp10")
						   //alert('Supplier='+Supplier)
						   
						   if(Supplier != null )
						   {
						   	Supplier.style.visibility = "hidden";
						   }
						   
						   
						   var Supplier1 = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp13")
						  
						    if(Supplier1 != null )
						   {
						   	Supplier1.style.visibility = "hidden";
						   }
						   
						   
						  
						  
						  
						   var QuoteDate = window.document.getElementById("custrecord_grants_item_quote_date")
						   if(QuoteDate != null )
						   {
						   QuoteDate.style.visibility = "hidden";	
						   }
						   
						  
						  
						   var QuoteReference = window.document.getElementById("custrecord_grants_item_quote_reference")
						   if(QuoteReference != null)
						   {
						   	QuoteReference.style.visibility = "hidden";
						   }
						   
						  
						   var PreferredSupplierReason= window.document.getElementById("custrecord_fs_grant_item_pref_supp_reas")
						   if(PreferredSupplierReason != null)
						   {
						   	 PreferredSupplierReason.style.visibility = "hidden";
						   }
						  
						  
						   var ItemName= window.document.getElementById("custrecord_grant_item")
						   if(ItemName != null)
						   {
						   	ItemName.style.visibility = "hidden";
						   }
						   
						  
						   var ItemCategory= window.document.getElementById("inpt_custrecord_fs_grant_item_category11")
						   if(ItemCategory != null)
						   {
						    ItemCategory.style.visibility = "hidden";	
						   }
						  
						   
						   var ItemCategory1= window.document.getElementById("inpt_custrecord_fs_grant_item_category14")
						   if (ItemCategory1 != null) 	
						   {
						   	ItemCategory1.style.visibility = "hidden";
						   }
						  
						  
						  
						   var Item= window.document.getElementById("custrecord_fs_grant_item_no_items_formattedValue")
						   if(Item != null)
						   {
						   	Item.style.visibility = "hidden";
						   }
						   
						   
						   
						   var Item1= window.document.getElementById("custrecord_fs_grant_item_no_items")
						   if(Item1 != null)
						   {
						   	 Item1.style.visibility = "hidden";
						   }
						  
						   
						   
						  
						   var ItemPrice= window.document.getElementById("custrecord_fs_grant_item_item_price_formattedValue")
						   if(ItemPrice != null)
						   {
						   ItemPrice.style.visibility = "hidden";	
						   }
						   
						   
						   
						   var ItemPrice1 = window.document.getElementById("custrecord_fs_grant_item_item_price")
						   if(ItemPrice1 != null)
						   {
						   	ItemPrice1.style.visibility = "hidden";
						   }
						   
						  
						   var GST= window.document.getElementById("custrecord_fs_grant_item_gst_formattedValue")
						   if(GST != null)
						   {
						   	GST.style.visibility = "hidden";
						   }
						   
						   
						   
						   var GST1= window.document.getElementById("custrecord_fs_grant_item_gst")
						   if (GST1 != null) 	
						   {
						   	GST1.style.visibility = "hidden";
						   }
						   
						  
						   var TotalIncl= window.document.getElementById("custrecord_fs_grant_item_total_formattedValue")
						   if (TotalIncl != null) 	
						   {
						   	 TotalIncl.style.visibility = "hidden";
						   }
						  
						   
						   
						   var TotalIncl1= window.document.getElementById("custrecord_fs_grant_item_total")
						   
						   if (TotalIncl1 != null) 	
						   {
						   	 TotalIncl1.style.visibility = "hidden";
						   }
						   
						   
						   //=======================Setting Fields visible==============================
						   
						   
						   
						   var JobTitle =  window.document.getElementById("custrecord_fs_grant_item_job_title")
						   if(JobTitle != null)
						   {
						    JobTitle.style.visibility = "visible";	
						   }
						  
						   
						   var PaymentPeriod =  window.document.getElementById("custrecord_fs_grant_item_payment_period")
						   if(PaymentPeriod != null)
						   {
						   	PaymentPeriod.style.visibility = "visible";
						   }
						   
						   
						   var Hashofpayments =  window.document.getElementById("custrecord_fs_grant_item_no_of_payments_formattedValue")
						   if(Hashofpayments != null)
						   {
						   	 Hashofpayments.style.visibility = "visible";
						   }
						  
						   
						   var  perpay = window.document.getElementById("custrecord_fs_grant_item_per_pay_formattedValue")
						   if(perpay != null)
						   {
						   	 perpay.style.visibility = "visible";
						   }
						  
						  
						  			
						}
						else if(SalaryQuote == 2)
						{
							
						   //=======================Setting Fields hidden==============================
							
						   
						   
						   var JobTitle =  window.document.getElementById("custrecord_fs_grant_item_job_title")
						   if(JobTitle != null)
						   {
						   	JobTitle.style.visibility = "hidden";
						   }
						   
						   
						   var PaymentPeriod =  window.document.getElementById("custrecord_fs_grant_item_payment_period")
						   if(PaymentPeriod != null)
						   {
						   	PaymentPeriod.style.visibility = "hidden";
						   }
						  
						   
						   var Hashofpayments =  window.document.getElementById("custrecord_fs_grant_item_no_of_payments_formattedValue")
						   if(Hashofpayments != null)
						   {
						   	  Hashofpayments.style.visibility = "hidden";
						   }
						 
						   
						   var  perpay = window.document.getElementById("custrecord_fs_grant_item_per_pay_formattedValue")
						   if(perpay != null)
						   {
						   	  perpay.style.visibility = "hidden";
						   }
						 
						   
						   
						   //=======================Setting Fields Visible==============================
						   
							
						   var Supplier = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp10")
						 
						   
						   if(Supplier != null )
						   {
						   	Supplier.style.visibility = "visible";
						   }
						   
						   
						   var Supplier1 = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp13")
						  
						    if(Supplier1 != null )
						   {
						   	Supplier1.style.visibility = "visible";
						   }
						   
						   
						  
						  
						  
						   var QuoteDate = window.document.getElementById("custrecord_grants_item_quote_date")
						   if(QuoteDate != null)
						   {
						    QuoteDate.style.visibility = "visible";	
						   }
						  
						  
						  
						   var QuoteReference = window.document.getElementById("custrecord_grants_item_quote_reference")
						   if(QuoteReference != null)
						   {
						   	QuoteReference.style.visibility = "visible";
						   }
						   
						  
						   var PreferredSupplierReason= window.document.getElementById("custrecord_fs_grant_item_pref_supp_reas")
						   if(PreferredSupplierReason != null)
						   {
						   	 PreferredSupplierReason.style.visibility = "visible";
						   }
						  
						  
						   var ItemName= window.document.getElementById("custrecord_grant_item")
						   if(ItemName != null)
						   {
						    ItemName.style.visibility = "visible";	
						   }
						  
						  
						   var ItemCategory= window.document.getElementById("inpt_custrecord_fs_grant_item_category11")
						   if(ItemCategory != null)
						   {
						    ItemCategory.style.visibility = "visible";	
						   }
						  
						   
						   var ItemCategory1= window.document.getElementById("inpt_custrecord_fs_grant_item_category14")
						   if (ItemCategory1 != null) 	
						   {
						   	ItemCategory1.style.visibility = "visible";
						   }
						  
						  
						  
						   var Item= window.document.getElementById("custrecord_fs_grant_item_no_items_formattedValue")
						   if(Item != null)
						   {
						   	Item.style.visibility = "visible";
						   }
						   
						   
						   
						   var Item1= window.document.getElementById("custrecord_fs_grant_item_no_items")
						   if(Item1 != null)
						   {
						   	 Item1.style.visibility = "visible";
						   }
						  
						   
						   
						  
						   var ItemPrice= window.document.getElementById("custrecord_fs_grant_item_item_price_formattedValue")
						   if(ItemPrice != null)
						   {
						   ItemPrice.style.visibility = "visible";	
						   }
						   
						   
						   
						   var ItemPrice1 = window.document.getElementById("custrecord_fs_grant_item_item_price")
						   if(ItemPrice1 != null)
						   {
						   	ItemPrice1.style.visibility = "visible";
						   }
						   
						  
						   var GST= window.document.getElementById("custrecord_fs_grant_item_gst_formattedValue")
						   if(GST != null)
						   {
						   	GST.style.visibility = "visible";
						   }
						   
						   
						   
						   var GST1= window.document.getElementById("custrecord_fs_grant_item_gst")
						   if (GST1 != null) 	
						   {
						   	GST1.style.visibility = "visible";
						   }
						   
						  
						   var TotalIncl= window.document.getElementById("custrecord_fs_grant_item_total_formattedValue")
						   if (TotalIncl != null) 	
						   {
						   	 TotalIncl.style.visibility = "visible";
						   }
						  
						   
						   
						   var TotalIncl1= window.document.getElementById("custrecord_fs_grant_item_total")
						   
						   if (TotalIncl1 != null) 	
						   {
						   	 TotalIncl1.style.visibility = "visible";
						   }
						   
						   
						   
							
						}
				
				
				
				
			}



	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	
	
	
	
	
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInitSetFieldValue(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY

	
	
 if( type == 'recmachcustrecord_grant_application_link' )
	{
		
		
		
				var SalaryQuote = nlapiGetCurrentLineItemValue('recmachcustrecord_grant_application_link','custrecord_fs_grant_item_salary_supp_quo')
				//alert('SalaryQuote='+SalaryQuote)
				
						if(SalaryQuote == 1)
						{
						   
						   //=======================Setting Fields Hidden==============================
						   
						   
						   var Supplier = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp10")
						   //alert('Supplier='+Supplier)
						   
						   if(Supplier != null )
						   {
						   	Supplier.style.visibility = "hidden";
						   }
						   
						   
						   var Supplier1 = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp13")
						  
						    if(Supplier1 != null )
						   {
						   	Supplier1.style.visibility = "hidden";
						   }
						   
						   
						  
						  
						  
						   var QuoteDate = window.document.getElementById("custrecord_grants_item_quote_date")
						   if(QuoteDate != null )
						   {
						   QuoteDate.style.visibility = "hidden";	
						   }
						   
						  
						  
						   var QuoteReference = window.document.getElementById("custrecord_grants_item_quote_reference")
						   if(QuoteReference != null)
						   {
						   	QuoteReference.style.visibility = "hidden";
						   }
						   
						  
						   var PreferredSupplierReason= window.document.getElementById("custrecord_fs_grant_item_pref_supp_reas")
						   if(PreferredSupplierReason != null)
						   {
						   	 PreferredSupplierReason.style.visibility = "hidden";
						   }
						  
						  
						   var ItemName= window.document.getElementById("custrecord_grant_item")
						   if(ItemName != null)
						   {
						   	ItemName.style.visibility = "hidden";
						   }
						   
						  
						   var ItemCategory= window.document.getElementById("inpt_custrecord_fs_grant_item_category11")
						   if(ItemCategory != null)
						   {
						    ItemCategory.style.visibility = "hidden";	
						   }
						  
						   
						   var ItemCategory1= window.document.getElementById("inpt_custrecord_fs_grant_item_category14")
						   if (ItemCategory1 != null) 	
						   {
						   	ItemCategory1.style.visibility = "hidden";
						   }
						  
						  
						  
						   var Item= window.document.getElementById("custrecord_fs_grant_item_no_items_formattedValue")
						   if(Item != null)
						   {
						   	Item.style.visibility = "hidden";
						   }
						   
						   
						   
						   var Item1= window.document.getElementById("custrecord_fs_grant_item_no_items")
						   if(Item1 != null)
						   {
						   	 Item1.style.visibility = "hidden";
						   }
						  
						   
						   
						  
						   var ItemPrice= window.document.getElementById("custrecord_fs_grant_item_item_price_formattedValue")
						   if(ItemPrice != null)
						   {
						   ItemPrice.style.visibility = "hidden";	
						   }
						   
						   
						   
						   var ItemPrice1 = window.document.getElementById("custrecord_fs_grant_item_item_price")
						   if(ItemPrice1 != null)
						   {
						   	ItemPrice1.style.visibility = "hidden";
						   }
						   
						  
						   var GST= window.document.getElementById("custrecord_fs_grant_item_gst_formattedValue")
						   if(GST != null)
						   {
						   	GST.style.visibility = "hidden";
						   }
						   
						   
						   
						   var GST1= window.document.getElementById("custrecord_fs_grant_item_gst")
						   if (GST1 != null) 	
						   {
						   	GST1.style.visibility = "hidden";
						   }
						   
						  
						   var TotalIncl= window.document.getElementById("custrecord_fs_grant_item_total_formattedValue")
						   if (TotalIncl != null) 	
						   {
						   	 TotalIncl.style.visibility = "hidden";
						   }
						  
						   
						   
						   var TotalIncl1= window.document.getElementById("custrecord_fs_grant_item_total")
						   
						   if (TotalIncl1 != null) 	
						   {
						   	 TotalIncl1.style.visibility = "hidden";
						   }
						   
						   
						   //=======================Setting Fields visible==============================
						   
						   
						   
						   var JobTitle =  window.document.getElementById("custrecord_fs_grant_item_job_title")
						   if(JobTitle != null)
						   {
						    JobTitle.style.visibility = "visible";	
						   }
						  
						   
						   var PaymentPeriod =  window.document.getElementById("custrecord_fs_grant_item_payment_period")
						   if(PaymentPeriod != null)
						   {
						   	PaymentPeriod.style.visibility = "visible";
						   }
						   
						   
						   var Hashofpayments =  window.document.getElementById("custrecord_fs_grant_item_no_of_payments_formattedValue")
						   if(Hashofpayments != null)
						   {
						   	 Hashofpayments.style.visibility = "visible";
						   }
						  
						   
						   var  perpay = window.document.getElementById("custrecord_fs_grant_item_per_pay_formattedValue")
						   if(perpay != null)
						   {
						   	 perpay.style.visibility = "visible";
						   }
						  
						  
						  			
						}
						else if(SalaryQuote == 2)
						{
							
						   //=======================Setting Fields hidden==============================
							
						   
						   
						   var JobTitle =  window.document.getElementById("custrecord_fs_grant_item_job_title")
						   if(JobTitle != null)
						   {
						   	JobTitle.style.visibility = "hidden";
						   }
						   
						   
						   var PaymentPeriod =  window.document.getElementById("custrecord_fs_grant_item_payment_period")
						   if(PaymentPeriod != null)
						   {
						   	PaymentPeriod.style.visibility = "hidden";
						   }
						  
						   
						   var Hashofpayments =  window.document.getElementById("custrecord_fs_grant_item_no_of_payments_formattedValue")
						   if(Hashofpayments != null)
						   {
						   	  Hashofpayments.style.visibility = "hidden";
						   }
						 
						   
						   var  perpay = window.document.getElementById("custrecord_fs_grant_item_per_pay_formattedValue")
						   if(perpay != null)
						   {
						   	  perpay.style.visibility = "hidden";
						   }
						 
						   
						   
						   //=======================Setting Fields Visible==============================
						   
							
						   var Supplier = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp10")
						 
						   
						   if(Supplier != null )
						   {
						   	Supplier.style.visibility = "visible";
						   }
						   
						   
						   var Supplier1 = window.document.getElementById("inpt_custrecord_grant_item_supplier_emp13")
						  
						    if(Supplier1 != null )
						   {
						   	Supplier1.style.visibility = "visible";
						   }
						   
						   
						  
						  
						  
						   var QuoteDate = window.document.getElementById("custrecord_grants_item_quote_date")
						   if(QuoteDate != null)
						   {
						    QuoteDate.style.visibility = "visible";	
						   }
						  
						  
						  
						   var QuoteReference = window.document.getElementById("custrecord_grants_item_quote_reference")
						   if(QuoteReference != null)
						   {
						   	QuoteReference.style.visibility = "visible";
						   }
						   
						  
						   var PreferredSupplierReason= window.document.getElementById("custrecord_fs_grant_item_pref_supp_reas")
						   if(PreferredSupplierReason != null)
						   {
						   	 PreferredSupplierReason.style.visibility = "visible";
						   }
						  
						  
						   var ItemName= window.document.getElementById("custrecord_grant_item")
						   if(ItemName != null)
						   {
						    ItemName.style.visibility = "visible";	
						   }
						  
						  
						   var ItemCategory= window.document.getElementById("inpt_custrecord_fs_grant_item_category11")
						   if(ItemCategory != null)
						   {
						    ItemCategory.style.visibility = "visible";	
						   }
						  
						   
						   var ItemCategory1= window.document.getElementById("inpt_custrecord_fs_grant_item_category14")
						   if (ItemCategory1 != null) 	
						   {
						   	ItemCategory1.style.visibility = "visible";
						   }
						  
						  
						  
						   var Item= window.document.getElementById("custrecord_fs_grant_item_no_items_formattedValue")
						   if(Item != null)
						   {
						   	Item.style.visibility = "visible";
						   }
						   
						   
						   
						   var Item1= window.document.getElementById("custrecord_fs_grant_item_no_items")
						   if(Item1 != null)
						   {
						   	 Item1.style.visibility = "visible";
						   }
						  
						   
						   
						  
						   var ItemPrice= window.document.getElementById("custrecord_fs_grant_item_item_price_formattedValue")
						   if(ItemPrice != null)
						   {
						   ItemPrice.style.visibility = "visible";	
						   }
						   
						   
						   
						   var ItemPrice1 = window.document.getElementById("custrecord_fs_grant_item_item_price")
						   if(ItemPrice1 != null)
						   {
						   	ItemPrice1.style.visibility = "visible";
						   }
						   
						  
						   var GST= window.document.getElementById("custrecord_fs_grant_item_gst_formattedValue")
						   if(GST != null)
						   {
						   	GST.style.visibility = "visible";
						   }
						   
						   
						   
						   var GST1= window.document.getElementById("custrecord_fs_grant_item_gst")
						   if (GST1 != null) 	
						   {
						   	GST1.style.visibility = "visible";
						   }
						   
						  
						   var TotalIncl= window.document.getElementById("custrecord_fs_grant_item_total_formattedValue")
						   if (TotalIncl != null) 	
						   {
						   	 TotalIncl.style.visibility = "visible";
						   }
						  
						   
						   
						   var TotalIncl1= window.document.getElementById("custrecord_fs_grant_item_total")
						   
						   if (TotalIncl1 != null) 	
						   {
						   	 TotalIncl1.style.visibility = "visible";
						   }
						   
						   
						   
							
						}
				
				
				
				
			}









}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
